<?php

namespace App\Gateway;

use App\Gateway\Enum;
use Carbon\Carbon;
use Illuminate\Support\Facades\Request;

abstract class PortAbstract
{
    /**
     * Transaction id
     *
     * @var null|int
     */
//    protected $transactionType = 0;
//    protected $transactionFkID;

    protected $transactionId = null;

    /**
     * Transaction row in database
     */
    public $transaction = null;

    /**
     * Customer card number
     *
     * @var string
     */
    protected $cardNumber = '';

    /**
     * @var Config
     */
    protected $config;

    /**
     * Port id
     *
     * @var int
     */
    protected $portName;

    /**
     * Reference id
     *
     * @var string
     */
    protected $refId;

    /**
     * Amount in Rial
     *
     * @var int
     */
    protected $amount;

    /**
     * Description of transaction
     *
     * @var string
     */
    protected $description;

    /**
     * callback URL
     *
     * @var url
     */
    protected $callbackUrl;

    /**
     * Tracking code payment
     *
     * @var string
     */
    protected $trackingCode;


    protected $retrivalRefNo;

    /**
     * Initialize of class
     *
     * @param Config $config
     * @param DataBaseManager $db
     * @param int $port
     */
    function __construct()
    {
        $this->db = app('db');
    }

    /** bootstraper */
    function boot()
    {

    }

    function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * @return mixed
     */
    function getTable()
    {
        return $this->db->table($this->config->get('gateway.table'));
    }

    /**
     * @return mixed
     */
    function getLogTable()
    {
        return $this->db->table($this->config->get('gateway.table') . '_log');
    }

    /**
     * Get port id, $this->port
     *
     * @return int
     */
    function getPortName()
    {
        return $this->portName;
    }

    /**
     * Get port id, $this->port
     *
     * @return int
     */
    function setPortName($name)
    {
        $this->portName = $name;
    }

    /**
     * Set custom description on current transaction
     *
     * @param string $description
     *
     * @return void
     */
    function setCustomDesc($description)
    {
        $this->description = $description;
    }

    /**
     * Get custom description of current transaction
     *
     * @return string | null
     */
    function getCustomDesc()
    {
        return $this->description;
    }

    /**
     * Return card number
     *
     * @return string
     */
    function cardNumber()
    {
        return $this->cardNumber;
    }

    /**
     * Return tracking code
     */
    function trackingCode()
    {
        return $this->trackingCode;
    }

    function retrivalRefNo()
    {
        return $this->retrivalRefNo;
    }

    /**
     * Get transaction id
     *
     * @return int|null
     */
    function transactionId()
    {
        return $this->transactionId;
    }

    /**
     * Return reference id
     */
    function refId()
    {
        return $this->refId;
    }

    /**
     * Sets price
     * @param $price
     * @return mixed
     */
    function price($price)
    {
        return $this->set($price);
    }

    /**
     * get price
     */
    function getPrice()
    {
        return $this->amount;
    }

    /**
     * Return result of payment
     * If result is done, return true, otherwise throws an related exception
     *
     * This method must be implements in child class
     *
     * @param object $transaction row of transaction in database
     *
     * @return $this
     */
    function verify($transaction)
    {
        $this->transaction = $transaction;
        $this->transactionId = $transaction->transact_id;
        $this->amount = intval($transaction->transact_amount);
        $this->refId = $transaction->transact_refId;
    }

    function getTimeId()
    {
        $genuid = function () {
            if ($this->portName == Enum::PARSIAN)
                return substr(str_pad(str_replace('.', '', microtime(true)), 12, 0), 5, 12);
            return substr(str_pad(str_replace('.', '', microtime(true)), 12, 0), 0, 12);
        };
        $uid = $genuid();
        while ($this->getTable()->whereTransactId($uid)->first())
            $uid = $genuid();
        return $uid;
    }

    /**
     * Insert new transaction to poolport_transactions table
     *
     * @return int last inserted id
     */
    protected function newTransaction()
    {
//        $uid = $this->getTimeId();
        $this->transactionId = $this->getTable()->insertGetId([
            'transact_created_at' => time(),
            'transact_port' => $this->getPortName(),
            'transact_amount' => $this->amount,
            'transact_status' => Enum::TRANSACTION_INIT,
            'transact_ip' => Request::getClientIp(),
            'transact_description' => $this->description,
            'transact_type' => null,
            'transact_fk_id' => null,
//            'transact_type' => $this->transactionType,
//            'transact_fk_id' => $this->transactionFkID,
        ]);
        return $this->transactionId;
    }

    /**
     * Commit transaction
     * Set status field to success status
     *
     * @return bool
     */
    protected function transactionSucceed()
    {
        $this->getTable()->whereTransactId($this->transactionId)->update([
            'transact_status' => Enum::TRANSACTION_SUCCEED,
            'transact_tracking_code' => $this->trackingCode,
            'transact_retrival_ref_no' => $this->retrivalRefNo,
            //'card_number' => $this->cardNumber,
            'transact_paymented_at' => time(),
            'transact_updated_at' => time(),
        ]);
    }

    /**
     * Failed transaction
     * Set status field to error status
     *
     * @return bool
     */
    protected function transactionFailed()
    {
        return $this->getTable()->whereTransactId($this->transactionId)->update([
            'transact_status' => Enum::TRANSACTION_FAILED,
            'transact_updated_at' => time(),
        ]);
    }

    /**
     * Update transaction refId
     *
     * @return void
     */
    protected function transactionSetRefId()
    {
        return $this->getTable()->whereTransactId($this->transactionId)->update([
            'transact_refId' => $this->refId,
            'transact_updated_at' => time(),
        ]);

    }

    /**
     * New log
     *
     * @param string|int $statusCode
     * @param string $statusMessage
     */
    protected function newLog($statusCode, $statusMessage)
    {
        return $this->getLogTable()->insert([
            'gtl_transact_id' => $this->transactionId,
            'gtl_result_code' => $statusCode,
            'gtl_result_message' => $statusMessage,
            'gtl_log_time' => time(),
        ]);
    }

    /**
     * Add query string to a url
     *
     * @param string $url
     * @param array $query
     * @return string
     */
    protected function makeCallback($url, array $query)
    {
        return $this->url_modify(array_merge($query, ['_token' => csrf_token()]), url($url));
    }

    /**
     * manipulate the Current/Given URL with the given parameters
     * @param $changes
     * @param  $url
     * @return string
     */
    protected function url_modify($changes, $url)
    {
        // Parse the url into pieces
        $url_array = parse_url($url);

        // The original URL had a query string, modify it.
        if (!empty($url_array['query'])) {
            parse_str($url_array['query'], $query_array);
            $query_array = array_merge($query_array, $changes);
        } // The original URL didn't have a query string, add it.
        else {
            $query_array = $changes;
        }

        return (!empty($url_array['scheme']) ? $url_array['scheme'] . '://' : null) .
            (!empty($url_array['host']) ? $url_array['host'] : null) .
            (!empty($url_array['port']) ? ':' . $url_array['port'] : null) .
            $url_array['path'] . '?' . http_build_query($query_array);
    }
}
