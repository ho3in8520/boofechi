<?php

namespace App\Gateway;

class Enum
{
    const MELLAT = 'MELLAT';
    const SADAD = 'SADAD';
    const ZARINPAL = 'ZARINPAL';
    const PAYLINE = 'PAYLINE';
    const JAHANPAY = 'JAHANPAY';
    const PARSIAN = 'PARSIAN';
    const PASARGAD = 'PASARGAD';
    const SAMAN = 'SAMAN';
    const ASANPARDAKHT = 'ASANPARDAKHT';
    const PAYPAL = 'PAYPAL';
    const PAYIR = 'PAYIR';

    /**
     * Status code for status field in poolport_transactions table
     */
    const TRANSACTION_INIT = 0;
    const TRANSACTION_INIT_TEXT = 'تراکنش ایجاد شد.';

    /**
     * Status code for status field in poolport_transactions table
     */
    const TRANSACTION_SUCCEED = 2;
    const TRANSACTION_SUCCEED_TEXT = 'پرداخت با موفقیت انجام شد.';

    /**
     * Status code for status field in poolport_transactions table
     */
    const TRANSACTION_FAILED = 1;
    const TRANSACTION_FAILED_TEXT = 'عملیات پرداخت با خطا مواجه شد.';

    const TRANSACTTION_BAG_CHARGE = 0;

    const TRANSACTTION_COLLECTION_FACTOR = 1;
    const TRANSACTTION_SYSTEM_FACTOR = 2;
    const TRANSACTTION_SYSTEM_CHARGE = 3;

}
