<?php

namespace App\Gateway\Pasargad;

use App\Gateway\Exceptions\BankException;

class PasargadErrorException extends BankException {}
