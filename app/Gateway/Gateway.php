<?php



namespace App\Gateway;

use Illuminate\Support\Facades\Facade;

/**
 * @see https://github.com/akoSalman/payment
 */
class Gateway extends Facade
{
    /**
     * The name of the binding in the IoC container.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'gateway';
    }
}
