<?php

namespace App\Gateway\Sadad;

class SadadResult
{
    const ERROR_CONNECT = -2542;
    const ERROR_CONNECT_MESSAGE = 'unable_to_connect';

    const INVALID_RESPONSE_CODE = -2541;
    const INVALID_RESPONSE_MESSAGE = 'invalid_response';

    const UNKNOWN_CODE = -2540;
    const UNKNOWN_MESSAGE = 'unknown';

    private static $results = array(
        array(
            'code' => SadadResult::ERROR_CONNECT,
            'fa' => 'خطا در اتصال به درگاه سداد',
            'en' => 'Error in connect to sadad',
            'retry' => false
        ),
        array(
            'code' => SadadResult::INVALID_RESPONSE_CODE,
            'fa' => 'جواب نامعتبر',
            'en' => 'Invalid Response',
            'retry' => false
        ),
        array(
            'code' => 0,
            'fa' => 'تراکنش با موفقیت انجام شد!',
            'en' => 'Invalid Response',
            'retry' => false
        ),
        array(
            'code' => 61,
            'fa' => 'مبلغ تراکنش از حد مجاز بالاتر است',
            'en' => 'Invalid Response',
            'retry' => false
        ),
        array(
            'code' => 1002,
            'fa' => 'خطا در سیستم- تراکنش ناموفق',
            'en' => 'Invalid Response',
            'retry' => false
        ),
        array(
            'code' => 1006,
            'fa' => 'خطا در سیستم',
            'en' => 'Invalid Response',
            'retry' => false
        ),
        array(
            'code' => 1011,
            'fa' => 'درخواست تکراری- شماره سفارش تکراری می باشد',
            'en' => 'Invalid Response',
            'retry' => false
        ),
        array(
            'code' => 1015,
            'fa' => 'پاسخ خطای نامشخص از سمت مرکز',
            'en' => 'Invalid Response',
            'retry' => false
        ),
        array(
            'code' => 1017,
            'fa' => 'مبلغ درخواستی شما جهت پرداخت از حد مجاز تعريف شده برای اين پذیرنده بیشتر است',
            'en' => 'Invalid Response',
            'retry' => false
        ),
        array(
            'code' => 1031,
            'fa' => 'مهلت زمانی شما جهت پرداخت به پايان رسیده است.لطفا مجددا سعی تلاش فرمایید',
            'en' => 'Invalid Response',
            'retry' => false
        ),
        array(
            'code' => 1056,
            'fa' => 'سیستم موقتا قطع میباشد.لطفا بعدا تلاش فرمايید.',
            'en' => 'Invalid Response',
            'retry' => false
        ),
        array(
            'code' => 1061,
            'fa' => 'اشکال در تولید کد يکتا. لطفا مرورگر خود را بسته و با اجرای مجدد مرورگر عملیات پرداخت را انجام دهید  (احتمال استفاده از دکمه   >Back< مرورگر)',
            'en' => 'Invalid Response',
            'retry' => false
        ),
        array(
            'code' => 1064,
            'fa' => 'لطفا مجددا سعی بفرمايید',
            'en' => 'Invalid Response',
            'retry' => false
        ),
        array(
            'code' => 1065,
            'fa' => 'ارتباط ناموفق .لطفا چند لحظه ديگر مجددا سعی کنید',
            'en' => 'Invalid Response',
            'retry' => false
        ),
        array(
            'code' => 1068,
            'fa' => 'با عرض پوزش به علت بروزرسانی . سیستم موقتا قطع میباشد',
            'en' => 'Invalid Response',
            'retry' => false
        ),
        array(
            'code' => 1068,
            'fa' => 'با عرض پوزش به علت بروزرسانی . سیستم موقتا قطع میباشد',
            'en' => 'Invalid Response',
            'retry' => false
        ),
        /*array(
            'code' => -1,
            'fa' => 'نتیجه استعلام نامعلوم و یا کاربر از انجام تراکنش منصرف شده است.',
            'en' => 'Result of the Inquiry Unknown or Customer Cancellation',
            'retry' => true,
        ),
        array(
            'code' => -1,
            'fa' => 'پارامترهای ارسالی صحیح نیست و یا تراکنش در سیستم وجود ندارد.',
            'en' => 'Parameters passed is incorrect or there is no transaction in the system.',
            'retry' => false,
        ),
        array(
            'code' => 9000,
            'fa' => 'درخواست تراکنش در سیستم ثبت شده ولی تراکنش هنوز شروع نشده است.',
            'en' => 'Transaction request registered in the system, but the transaction has not started yet.',
            'retry' => true,
        ),
        array(
            'code' => 9001,
            'fa' => 'تراکنش در سیستم ثبت شده و پیام مالی به سیستم بانک ارسال شده است اما هنوز پاسخی دریافت نشده است.',
            'retry' => true,
        ),
        array(
            'code' => 9004,
            'fa' => 'سیستم در حال تلاش جهت برگشت تراکنش خرید است.',
            'en' => 'The system is trying to buy back transaction.',
            'retry' => true,
        ),
        array(
            'code' => 9005,
            'fa' => 'سیستم در حال تلاش جهت برگشت تراکنش خرید است.',
            'en' => 'The system is trying to buy back transaction.',
            'retry' => true,
        ),
        array(
            'code' => 9006,
            'fa' => 'تراکنش ناموفق بوده و مبلغ با موفقیت به حساب مشتری برگشت خورده است',
            'en' => 'Purchase operation was unsuccessful and The amount is returned successfully to the client`s account',
            'retry' => false,
        ),
        array(
            'code' => 0,
            'fa' => 'عملیات خرید نا موفق بوده است',
            'en' => 'Purchase operation was unsuccessful',
            'retry' => false,
        ),
        array(
            'code' => 56,
            'fa' => 'کارت نامعتبر است.',
            'en' => 'Card Not Effective',
            'retry' => false,
        ),
        array(
            'code' => 51,
            'fa' => 'مبلغ درخواستی از موجودی حساب شما, بیشتر است.حداقل مانده حساب شما پس از عملیات پرداخت باید 100,000 ریال باشد.',
            'en' => 'Inventory shortage',
            'retry' => false,
        ),
        array(
            'code' => 55,
            'fa' => 'رمز کارت صحیح نمی باشد لطفا مجددا, سعی کنید.',
            'en' => 'Incorrect Pin',
            'retry' => false,
        ),
        array(
            'code' => 9008,
            'fa' => 'در هنگام انجام عملیات CheckStatus مشکلی رخ داده است.',
            'en' => 'CheckStatus operations when a problem has occurred.',
            'retry' => true,
        ),
        array(
            'code' => 9009,
            'fa' => 'سیستم در حال تلاش برای دریافت جواب عملیات بانکی است.',
            'en' => 'The system is trying to Get answers to banking operations.',
            'retry' => true,
        ),
        array(
            'code' => 9010,
            'fa' => 'تراکنش به لیست تراکنش های برگشت خودکار اضافه شده است و در انتظار برگشت می باشد.',
            'en' => 'The transaction is automatically added to the list of back transactions and is expected to return.',
            'retry' => true,
        ),
        array(
            'code' => 9011,
            'fa' => 'تراکنش برگشت خرید ارسال شده است اما پاسخ دریافت شده قطعی نیست و تراکنش در لیست تراکنش های معوق می باشد و نتیجه عملیات بانکی ظرف 24 ساعت آینده مشخص خواهد شد.',
            'en' => 'Back purchase transaction has been sent but not conclusive answer has been received and the transaction is on the list of pending transactions and banking operations will be determined within the next 24 hours.',
            'retry' => false,
        ),
        array(
            'code' => 9012,
            'fa' => 'تراکنش ناموفق - نتیجه تراکنش معوق بعد از 24 ساعت مشخص می شود',
            'en' => 'Failed Transaction - result of outstanding transactions will be determined after 24 hours',
            'retry' => false,
        ),*/
        array(
            'code' => 0,
            'fa' => 'تراکنش موفق',
            'en' => 'Successful Transaction',
            'retry' => false,
        ),
    );

    /**
     * return response
     *
     * @param int $code
     * @param string $message
     * @return null
     */
    public static function codeResponse($code, $message)
    {
        $code = intval($code);

        foreach (self::$results as $v) {
            //if ($v['message'] == $message && $v['code'] == $code) ========> old
            if ($v['code'] == $code)
                return $v;
        }

        return null;
    }
}
