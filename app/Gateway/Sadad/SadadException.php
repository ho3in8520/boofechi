<?php

namespace App\Gateway\Sadad;


use App\Gateway\Exceptions\BankException;

class SadadException extends BankException {}
