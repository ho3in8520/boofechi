<?php

namespace App\Gateway\Parsian;

use App\Gateway\Exceptions\BankException;

class ParsianErrorException extends BankException {}
