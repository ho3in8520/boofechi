<?php
namespace App\Export;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class BladeExport implements FromView
{

    private $title;
    private $headers;
    private $records;

    public function __construct($headers,$records,$title)
    {
        $this->title = $title;
        $this->headers = $headers;
        $this->records = $records;
    }

    public function view(): View
    {
        return view('exports.export', [
            'title' => $this->title,
            'headers' => $this->headers,
            'records' => $this->records
        ]);
    }
}