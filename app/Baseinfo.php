<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Baseinfo extends Model
{
    protected $table = "baseinfos";
    protected $primaryKey = "bas_id";
    public $timestamps = false;
//    protected $fillable = [
//        'bas_parent_id',
//        'bas_value',
//        'bas_type',
//        'bas_status',
//        'bas_extra_value',
//        'bas_extra_value1',
//        'bas_can_user_add',
//    ];
}
