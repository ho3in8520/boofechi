<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class RegisterResume
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        if($request->user()->panel_type == 1111){
//            echo "<pre>";
//            print_r(user()->permissions());
//            exit();
//            if(isset($request->route()->getAction()['permissions'])) {
//                $need_permissions = $request->route()->getAction()['permissions'];
//
////                if(count(array_intersect($haystack, $target)) == count($target)){
////                    // all of $target is in $haystack
////                }
//
//                if (can('menu_collectionfs')) {
//                    echo "<pre>";
//                    print_r($request->route()->getAction()['permissions']);
//                    exit();
//                }
//            }
//            return abort(404);
//        }

        $permit_routes = [
            "terms",
            "authentication",
            'logout',
            'get-cities',
            'visitor/business-form',
            'shopkeeper/business-form',
            'upload_route',
            'upload_route/create',
            'dashboard'
        ];
        $permit_routes_terms = [
            "terms",
            'logout',
        ];

        $permit_force_factor = [
            "list-factor-system",
            'verify-payment',
            'view-system-factor/{id}',
            'payment-factor-system',
            'logout'
        ];

        if (Auth::check()) {
            //if not admin
            if (user('panel_type') != 39 && user('panel_type') != 40) {
                if (user('accept_terms') == 0)//agar ghabol nakarde terms
                    if (!in_array($request->route()->uri, $permit_routes_terms))
                        return redirect()->route('terms');
            }

            //register sessions
            if (user('panel_type') == 44 || user('panel_type') == 45) {
                if (user('profile_complete') == 0)//agar takmil nashode  profilesh
                    if (!in_array($request->route()->uri, $permit_routes))
                        return redirect()->route('authentication');
            }

            //if not admin
            if (user('panel_type') != 39) {
                if (!in_array($request->route()->uri, $permit_force_factor)) {
                    $factor = DB::table('system_factor_master')
                        ->where('sfm_collection_id', collection_id())
                        ->where('sfm_is_payed', 0)
                        ->where('sfm_mode', 1)
                        ->where('sfm_is_force', 1)
                        ->first();
                    if ($factor)//agar factore force darad
                    {
                        return redirect()->route('list-factor-system')->with("error", "به نظر میرسد، فاکتور پرداخت نشده ای با وضعیت فوری دارید، لطفا نسبت به پرداخت آن اقدام نمایید!");
                    }
                }
            }
        }
        return $next($request);
    }

}
