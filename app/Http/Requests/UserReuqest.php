<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserReuqest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rule = [
            'prsn_id'=>['required'],
            'user_id'=>['required'],
            'users.username'=>['required','unique_string:users,username,'.request('user_id').',user_id'],
            'users.email'=>['nullable','email','unique:users,email,'.request('user_id').',user_id'],
        ];
        if(request('password') == "" && request()->method('patch')){
            return $rule;
        } else{
            $rule = array_merge($rule,[
                'password'=>['required','confirmed'],
                'password_confirmation'=>['required']
            ]);
            return $rule;
        }

    }
}
