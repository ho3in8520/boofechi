<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\DB;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'product.prod_name' => ['required', 'max:255',function($attr,$value,$fail){
            $collection_product = DB::table('products')
                ->select([
                    'cp_id',
                    "prod_name"
                ])
                ->where('prod_name',$value)
                ->leftJoin('collection_products','cp_product_id','prod_id')
                ->where('cp_collection_id',collection_id())->first();
            if (isset($collection_product->prod_name) && $collection_product->cp_id != null){
                return $fail("محصول وارد شده تکراری میباشد");
            }
        }],
            'file'=>['image','mimes:jpeg,png,jpg'],
            'product.prod_category_id' => ['required'],
//            'product.cp_price' => ['required'],
//            'collProduct.cp_second_code' => ['required'],
//            'collProduct.cp_default_price_buy' => ['required'],
//            'collProduct.cp_default_price_consumer' => ['required'],
            'collProduct.cp_arzesh_afzoodeh' => ['nullable'],
            'collProduct.cp_measurement_unit_id' => ['required', 'exist_value_baseinfos:measure_unit'],
            'collProduct.cp_packaging_type' => ['required', 'exist_value_baseinfos:packaging_type'],
            'collProduct.cp_weight_of_each' => ['nullable', 'numeric'],
            'collProduct.cp_dimension_length' => ['nullable', 'numeric'],
            'collProduct.cp_dimension_width' => ['nullable', 'numeric'],
            'pc_discount_percent.*' => ['nullable','regex:/^[0-9]+$/'],
//            'pc_date_from.*' => ['nullable', 'required_if:type,==,2', "regex:/^[1-4]\d{3}\/((0[1-6]\/((3[0-1])|([1-2][0-9])|(0[1-9])))|((1[0-2]|(0[7-9]))\/(30|([1-2][0-9])|(0[1-9]))))$/"],
//            'pc_date_to.*' => ['nullable', 'required_if:type,==,2', "regex:/^[1-4]\d{3}\/((0[1-6]\/((3[0-1])|([1-2][0-9])|(0[1-9])))|((1[0-2]|(0[7-9]))\/(30|([1-2][0-9])|(0[1-9]))))$/"],
//            'pc_date_from_dis.*' => ['nullable', 'required_if:type,==,1', "regex:/^[1-4]\d{3}\/((0[1-6]\/((3[0-1])|([1-2][0-9])|(0[1-9])))|((1[0-2]|(0[7-9]))\/(30|([1-2][0-9])|(0[1-9]))))$/"],
//            'pc_date_to_dis.*' => ['nullable', 'required_if:type,==,1', "regex:/^[1-4]\d{3}\/((0[1-6]\/((3[0-1])|([1-2][0-9])|(0[1-9])))|((1[0-2]|(0[7-9]))\/(30|([1-2][0-9])|(0[1-9]))))$/"],
            'pc_buy_count.*' => ['nullable', 'required_if:type,==,2', 'numeric'],
            'numner_collections' => ['nullable', 'numeric'],
            'cp_measurement_unit_id.*' => ['nullable', 'exist_value_baseinfos:measure_unit'],
            'pc_free_count.*' => ['nullable', 'required_if:type,==,2', 'numeric'],
            'pc_free_collection_product_id.*' => ['required_if:pc_method_select_product.*,2'],
            'collProduct.cp_count_per' => ['required','numeric'],
        ];
    }

    public function messages()
    {
        return [
            'pc_date_from.*.required_if' => ["فیلد :attribute الزامی است"],
            'pc_date_to.*' => ["فیلد :attribute الزامی است"],
            'pc_date_from_dis.*' => ["فیلد :attribute الزامی است"],
            'pc_date_to_dis.*' => ["فیلد :attribute الزامی است"],
            'pc_buy_count.*' => ["فیلد :attribute الزامی است"],
            'pc_free_count.*' => ["فیلد :attribute الزامی است"],
            'pc_free_collection_product_id.*' => ["فیلد :attribute الزامی است"],
        ];
    }
}
