<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FactoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //extends rule exitst : for check value exist in  table
        return [
            'prsn_id'=>['required'],
            'coll_id'=>['required'],
            'persons.prsn_name' => ['required', 'max:30'],
            'persons.prsn_family' => ['required', 'max:30'],
            'persons.prsn_national_code' => ['required', 'max:10', 'regex:/([0-9]+){10}/'],
            'persons.prsn_mobile1' => ['required', 'max:11', 'regex:/([0-9]+){11}/'],
            'persons.prsn_phone1' => ['required', 'max:11', 'regex:/([0-9]+){11}/'],
            'collections.coll_name' => ['required', 'max:100'],
            'user.reagent_id' => ['max:26', 'exists:persons,prsn_national_code','nullable'],
            'collections.coll_economic_code' => ['required', 'unique:collections,coll_economic_code,'.request('coll_id').',coll_id','max:30'],
            'collections.coll_national_number' => ['required', 'max:30'],
//            'collections.coll_commission' => ['required', 'max:30', 'regex:/^-?(?:\d+|\d*\.\d+)$/', 'max_float_character:5'],
            'collections.coll_post_code' => ['required', 'max:10', 'regex:/([0-9]+){10}/'],
            'collections.coll_site_address' => ['nullable', 'max:190'],
            'collections.coll_address' => ['required', 'max:1000'],
            'collections.coll_state_id' => ['required', 'exist_value:cities,c_id'],
            'collections.coll_city_id' => ['required', 'exist_value:cities,c_id'],
            'typeClass'=>[function($attr,$value,$fail){
                if(empty(request('classType')))
                    return $fail("حداقل یک مورد از صنف ها باید انتخاب شود");
            }]
        ];
    }
}
