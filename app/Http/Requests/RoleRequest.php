<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\DB;


class RoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->isMethod('post')) {
            return [
                'rol_id' => ['required'],
                'roles.rol_name' => ['required', 'max:255', function ($attr, $value, $fail) {
                    $role = DB::table('roles')
                        ->where('rol_creator', user('user_id'))
                        ->where('rol_name', $value)->first();
                    if (isset($role->rol_name) && $role->rol_id != null) {
                        return $fail("نقش وارد شده تکراری میباشد");
                    }
                }],
//            'roles.rol_name'=>['required','unique_string:roles,rol_name,'.request('rol_id').',rol_id'],
                'roles.rol_label' => ['required', 'max:255', function ($attr, $value, $fail) {
                    $role = DB::table('roles')
                        ->where('rol_creator', user('user_id'))
                        ->where('rol_label', $value)->first();
                    if (isset($role->rol_label) && $role->rol_id != null) {
                        return $fail("نقش وارد شده تکراری میباشد");
                    }
                }],
//            'roles.rol_label'=>['required','unique_string:roles,rol_label,'.request('rol_id').',rol_id'],
            ];
        } elseif ($this->isMethod('patch')) {
            return [
                'rol_id' => ['required'],
                'roles.rol_name' => ['required', 'max:255'],
                'roles.rol_label' => ['required', 'max:255'],
            ];
        }
    }
}
