<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ShopkeeperRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'shop_keeper.name'=>['required','max:50'],
            'shop_keeper.last_name'=>['required','max:50'],
            'shop_keeper.name_father'=>['required','max:50'],
            'shop_keeper.date_of_birth'=>['required'],
            'shop_keeper.national_code'=>['required', 'max:10', 'regex:/([0-9]+){10}/'],
            'shop_keeper.postal_code'=>['required', 'max:10', 'regex:/([0-9]+){10}/'],
            'shop_keeper.state'=>['required'],
            'shop_keeper.city'=>['required'],
            'shop_keeper.register_type'=>['required'],
            'shop_keeper.address'=>['required','max:255'],
            'shop_keeper.class'=>['required'],
            'shop_keeper.job'=>['required'],
            'shop_keeper.store'=>['required'],
        ];
    }
}
