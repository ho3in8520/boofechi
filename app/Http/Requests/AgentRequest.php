<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AgentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "persons.prsn_name"=>["required","max:30"],
            "persons.prsn_family"=>["required","max:30"],
            "persons.prsn_national_code"=>["required"],
            "persons.prsn_father_name"=>["required","max:30"],
            "persons.prsn_birthday"=>["required","/^[1-4]\d{3}\/((0[1-6]\/((3[0-1])|([1-2][0-9])|(0[1-9])))|((1[0-2]|(0[7-9]))\/(30|([1-2][0-9])|(0[1-9]))))$/"],
            "persons.prsn_certificate_code"=>["required","between_digit:1,10","numeric"],
            "persons.prsn_serial_certificate_code"=>["required","max:15"],
            "persons.prsn_phone1"=>["required","max:11"],
            "persons.prsn_mobile1"=>["required","max:11"],
            "persons.prsn_gender_id"=>["required","exist_value_baseinfos:gender"],
            "persons.prsn_post_code"=>["required","max:20"],
            "persons.prsn_address"=>["required","max:1000"],
            "collections.coll_email"=>["required","max:190"],
            "collections.coll_name"=>["required","max:50"],
            "collections.coll_registeration_code"=>["required","max:30"],
            "collections.coll_economic_code"=>["required","max:30"],
            "collections.coll_national_number"=>["required","max:30"],
            "collections.coll_type_id"=>["required","exist_value_baseinfos:coll_type_id"],
            "collections.coll_type_class_id"=>["required","exist_value_baseinfos:coll_type_class_id"],
            'collections.coll_state_id'=>['required','exist_value:cities,c_id'],
            'collections.coll_city_id'=>['required','exist_value:cities,c_id'],
            "collections.coll_post_code"=>["required","max:15"],
            "collections.coll_address"=>["required","max:1000"],
        ];
    }
}
