<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateCollectionUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'create-user.image' => ['mimes:jpeg,jpg,png,gif', 'max:1024'],
            'create-user.name' => ['required', 'max:30'],
            'create-user.last-name' => ['required', 'max:30'],
            'create-user.national-code' => ['required', 'max:10', 'regex:/([0-9]+){10}/'],
            'create-user.mobail' => ['required', 'max:11', 'regex:/([0-9]+){11}/'],
            'create-user.username' => ['required', 'max:30', 'regex:/^[A-Za-z][A-Za-z0-9]{4,31}$/'],
            'create-user.password' => ['required', 'regex:/^[A-Za-z]|[A-Za-z0-9]|[#?!@$%^&*-].{5}$/'],
            'create-user.roll' => ['required'],
            'selected_cities' => ['required'],
            'categories' => [function ($attr, $value, $fail) {
                if (!request()->has('cats') || empty(request('cats')))
                    return $fail("حداقل یک مورد از صنف ها باید انتخاب شود");
            }],
            'selected_type' => function ($attribute, $value, $fail) {
                if (!request()->has('role_person')) {
                    $fail('هیچ دریافت کننده ای انتخاب نشده است.');
                }
            }
        ];
    }
}
