<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\DB;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category.cat_name' => ['required', 'max:255'/*,'unique_string:categories,cat_name'*/],
            'category.cat_parent_id' => ['nullable', function ($attr, $value, $fail) {
                if ($value == "")
                    return true;
                else {
                    $result = DB::table('categories')->whereCatId($value)->get();
                    if($result)
                        return true;
                    else
                        $fail('دسته مورد نظر نامعتبر است!');
                }
            }],
            'category.cat_senf_id' => ['required', function ($attr, $value, $fail) {
                if ($value == "")
                    return true;
                else {
                    $result = DB::table('baseinfos')->whereBasId($value)->get();
                    if($result)
                        return true;
                    else
                        $fail('صنف وارد شده نامعتبر است!');
                }
            }],
            'category.cat_type' => ['boolean'],
        ];
    }
}
