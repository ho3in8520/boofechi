<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AnnoucementRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'announcement.ann_title' => ['required'],
//            'announcement.ann_body' => ['required'],
            'selected_cities' => ['required'],
            'selected_type' => function ($attribute, $value, $fail) {
                if (!request()->has('role_person')) {
                    $fail('هیچ دریافت کننده ای انتخاب نشده است.');
                }
            },
            'type' => function ($attribute, $value, $fail) {
                if (user('panel_type')==39 && !$value) {
                    $fail('نوع ارسال مشخص نشده است');
                }
            }
        ];
    }
}
