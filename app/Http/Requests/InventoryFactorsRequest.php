<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InventoryFactorsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_id.*'=>['required'],
            'product_count.*'=>['required'],
            'product_price_buy.*'=>['required'],
            'product_price_consumer.*'=>['required'],
//            'roles.rol_name'=>['required','unique_string:roles,rol_name,'.request('rol_id').',rol_id'],
//            'roles.rol_label'=>['required','unique_string:roles,rol_label,'.request('rol_id').',rol_id'],
        ];
    }
}
