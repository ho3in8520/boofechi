<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\CategoryRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:list_category', ['only' => ['index']]);
        $this->middleware('permission:delete_category', ['only' => ['destroy']]);
        $this->middleware('permission:create_category', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit_category', ['only' => ['update', 'edit']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $senfs = json_decode(json_encode(DB::table('baseinfos')
            ->select([
                'bas_id as id',
                'bas_value as name'
            ])
            ->where('bas_parent_id', '!=', 0)
            ->where(['bas_type' => 'type_class'])
            ->get()->toArray()), true);
        $categories = DB::table("categories")->select([
            "cat_id as id",
            "cat_parent_id as parent_id",
            "cat_name as name"
        ])->where('cat_status', 1)->orderBy("cat_parent_id", "asc")->get()->toArray();
        $categories = convertToHierarchy(json_decode(json_encode($categories), true));
        $data = DB::table("categories as cat")
            ->select([
                "cat.cat_id",
                'bas_value as senf',
                "cat.cat_name",
                DB::raw("(case when cat.cat_status = 1 then N'فعال' else N'غیر فعال' end) as cat_status"),
                DB::raw("(case when cat.cat_type != 0 then N'لاین' else N'دسته بندی' end) as cat_type"),
                DB::raw("(case when cat.cat_parent_id != 0 then parent.cat_name else '---' end) as cat_parent_name")
            ])
            ->leftjoin('baseinfos', 'bas_id', '=', 'cat.cat_senf_id')
            ->leftjoin('categories as parent', 'parent.cat_id', '=', 'cat.cat_parent_id');

        if (request()->has('cat_name') && request('cat_name') != "")
            $data->where('cat.cat_name', 'like', '%' . request('cat_name') . '%');

        if (request()->has('cat_type') && request('cat_type') != "")
            $data->where('cat.cat_type', request('cat_type'));

        if (request()->has('cat_parent_id') && request('cat_parent_id') != 0)
            $data->where('cat.cat_parent_id', request('cat_parent_id'));

        if (request()->has('cat_senf_id') && request('cat_senf_id') != 0)
            $data->where('cat.cat_senf_id', request('cat_senf_id'));

        if (user("panel_type") != 39)//not is admin
        {
            $data->leftJoin("collection_categories", "cc_category_id", "=", "cat.cat_id")
                ->where("cc_collection_id", collection_id());
            if (user("panel_type") == 40) {//is user under collection
                $data->where("cc_user_id", user("user_id"));
            }
            else{
                $data->where("cc_user_id", 0);
            }
        }

        $data = $data->orderBy("cat.cat_name")
            ->distinct("cat.cat_id")
            ->paginate(20);
//        dd($data);
        return view('category.index', compact('data', 'categories', 'senfs'));
    }

    public function getCatBySenfId($id)
    {
        $categories = DB::table("categories")->select([
            "cat_id as id",
            "cat_parent_id as parent_id",
            "cat_name as name"
        ])
            ->where('cat_status', 1)
            ->where('cat_senf_id', $id)
            ->orderBy("cat_parent_id", "asc")
            ->get()->toArray();
        $categories = convertToHierarchy(json_decode(json_encode($categories), true));

        createCombobox($categories, 0, null, true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $senfs = json_decode(json_encode(DB::table('baseinfos')
            ->where('bas_parent_id', '!=', 0)
            ->where(['bas_type' => 'type_class'])
            ->get()->toArray()), true);
//        $categories = DB::table("categories")->select([
//            "cat_id as id",
//            "cat_parent_id as parent_id",
//            "cat_name as name"
//        ])->where('cat_status', 1)->orderBy("cat_parent_id", "asc")->get()->toArray();
//        $categories = convertToHierarchy(json_decode(json_encode($categories), true));
        $categories = [];
        return view('category.form', compact('categories', 'senfs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
//        DB::beginTransaction();
//        try {
        Category::create(array_merge($request->category, ['cat_status' => ($request->has('category.cat_status') ? 1 : 0)]));
        session()->flash('Success', config('first_config.message.success_message'));
//            DB::commit();
        return redirect(route('category.create'));
//        } catch (\Exception $exception) {
//            session()->flash('Error', config('first_config.message.error_message'));
//            DB::rollBack();
//            return redirect()->back();
////        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $old_cat = Category::where('cat_id', $id)->first();
        $senfs = json_decode(json_encode(DB::table('baseinfos')
            ->where('bas_parent_id', '!=', 0)
            ->where(['bas_type' => 'type_class'])
            ->get()->toArray()), true);
        $categories = DB::table("categories")->select([
            "cat_id as id",
            "cat_parent_id as parent_id",
            "cat_name as name"
        ])
            ->where('cat_status', 1)
            ->where('cat_senf_id', $old_cat->cat_senf_id)
            ->orderBy("cat_parent_id", "asc")->get()->toArray();
        $categories = convertToHierarchy(json_decode(json_encode($categories), true));
        return view('category.form', compact('categories', 'senfs', 'old_cat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, $id)
    {
        $cat = Category::where('cat_id', $id)->first();
        $cat->cat_name = $request->category['cat_name'];
        $cat->cat_senf_id = $request->category['cat_senf_id'];
        $cat->cat_type = $request->category['cat_type'];
        $cat->cat_parent_id = $request->category['cat_parent_id'];
        $cat->cat_status = ($request->has('category.cat_status') ? true : false);
        $cat->save();
        session()->flash('Success', config('first_config.message.success_message'));
        return redirect(route('category.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
