<?php

namespace App\Http\Controllers;

use App\PanelWizard;
use App\PanelWizardCompleted;
use Illuminate\Support\Facades\DB;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $result = [];
        $msg_complet = null;
        if (user('panel_type') == 44 || user('panel_type') == 45) {//shopkeeper or visitor
            $pending = DB::table('panel_wizard_completed')
                ->where('pwc_user_id', user('user_id'))
                ->where('pwc_accepted', 0)
                ->count();
            if ($pending > 0) {//agar formi darad ke dar entezare taeed ast
                $msg_complet = "<div class=\"alert alert-warning\">
                                <strong>" . config('first_config.message.user-waiting-wizard') . "</strong>
                            </div>";

            }
            $result ['msg_complet'] = $msg_complet;
            $rejected = DB::table('panel_wizard_completed')
                ->where('pwc_user_id', user('user_id'))
                ->where('pwc_accepted', 2)
                ->count();
            if ($rejected > 0)//agar formi darad ke taeed nashode
                return redirect()->route('authentication');
            $panel_wizard = DB::table('panel_wizards')
                ->where('pw_panel_type_id', user('panel_type'))
                ->count();
            $panel_wizard_comp = DB::table('panel_wizard_completed')
                ->where('pwc_user_id', user('user_id'))
                ->whereIN('pwc_accepted', [0, 1])
                ->count();
            if ($panel_wizard == $panel_wizard_comp) {
                $array = [];
                foreach (findUser(user('user_id'), 'type_class') as $class) {
                    array_push($array, [
                        'TC' => $class['coc_typeClass_id'],
                    ]);
                }
                if (user('panel_type') == 44) {
                    $sum_week = call_sp("get_sales_report_dashboard", 2, [
                        'collection_id' => collection_id(),
                        'date_from' => \Morilog\Jalali\Jalalian::forge('last week')->format('%Y/%m/%d'),
                        'date_to' => jdate_from_gregorian(date('Y/m/d')),
                        'mode' => 1,
                        'type' => 0,
                    ]);
                    $sum_month = call_sp("get_sales_report_dashboard", 2, [
                        'collection_id' => collection_id(),
                        'date_from' => \Morilog\Jalali\Jalalian::forge('last month')->format('%Y/%m/%d'),
                        'date_to' => jdate_from_gregorian(date('Y/m/d')),
                        'mode' => 1,
                        'type' => 0,
                    ]);
                    $sum_year = call_sp("get_sales_report_dashboard", 2, [
                        'collection_id' => collection_id(),
                        'date_from' => \Morilog\Jalali\Jalalian::forge('last year')->format('%Y/01/01'),
                        'date_to' => jdate_from_gregorian(date('Y/m/d')),
                        'mode' => 1,
                        'type' => 0,
                    ]);
                    $sum_by_date = call_sp("get_sales_report_dashboard", 2, [
                        'collection_id' => collection_id(),
                        'date_from' => \Morilog\Jalali\Jalalian::forge('now - 5 month')->format('%Y/%m/%d'),
                        'date_to' => jdate_from_gregorian(date('Y/m/d')),
                        'mode' => 1,
                        'type' => 1,
                    ]);
                    $bartarinha = call_sp("get_top_shopkeeper_buies", 2, [
                        'collection_id' => collection_id(),
                        'mode' => 1,
                        'type' => 0,
                        'city_id' => user()->person->prsn_city_id
                    ]);

                    $haraji_products = call_sp("get_haraji_products", 2, [
                        'collection_id' => collection_id(),
                        "mode"=>1
                    ]);

                    $today_activities = call_sp("get_today_activities", 2, [
                        'collection_id' => collection_id(),
                        "mode"=>1,
                        "date"=> jdate_from_gregorian(date('Y/m/d'),"Y/m/d")
                    ]);

                    $result ['sum_week'] = $sum_week;
                    $result ['sum_month'] = $sum_month;
                    $result ['sum_year'] = $sum_year;
                    $result ['sum_by_date'] = $sum_by_date;
                    $result ['bartarinha'] = $bartarinha;
                    $result ['haraji_products'] = $haraji_products;
                    $result ['today_activities'] = $today_activities;


                } elseif (user('panel_type') == 45) {

                    $sum_week = call_sp("visitor_dashboard", 2, [
                        'user_id' => user('user_id'),
                        'date_from' => jalali_to_timestamp(\Morilog\Jalali\Jalalian::forge('last week')->format('%Y/%m/%d')),
                        'date_to' => jalali_to_timestamp(jdate_from_gregorian(date('Y/m/d'),'Y/m/d'),'/',false),
                        'mode' => 0,
                    ]);
                    $sum_month = call_sp("visitor_dashboard", 2, [
                        'user_id' => user('user_id'),
                        'date_from' => jalali_to_timestamp(\Morilog\Jalali\Jalalian::forge('last month')->format('%Y/%m/%d')),
                        'date_to' => jalali_to_timestamp(jdate_from_gregorian(date('Y/m/d'),'Y/m/d'),'/',false),
                        'mode' => 0,
                    ]);
                    $sum_year = call_sp("visitor_dashboard", 2, [
                        'user_id' => user('user_id'),
                        'date_from' => jalali_to_timestamp(\Morilog\Jalali\Jalalian::forge('last year')->format('%Y/01/01')),
                        'date_to' => jalali_to_timestamp(jdate_from_gregorian(date('Y/m/d'),'Y/m/d'),'/',false),
                        'mode' => 0,
                    ]);

                    $sum_by_date = call_sp("visitor_dashboard", 2, [
                        'user_id' => user('user_id'),
                        'date_from' => jalali_to_timestamp(\Morilog\Jalali\Jalalian::forge('now - 5 month')->format('%Y/%m/%d')),
                        'date_to' => jalali_to_timestamp(jdate_from_gregorian(date('Y/m/d'),'Y/m/d'),'/',false),
                        'mode' => 1,
                    ]);

                    $bartarinha = call_sp("get_top_shopkeeper_buies", 2, [
                        'collection_id' => collection_id(),
                        'mode' => 1,
                        'type' => 0,
                        'city_id' => user()->person->prsn_city_id
                    ]);

                    $haraji_products = call_sp("get_haraji_products", 2, [
                        'collection_id' => collection_id(),
                        "mode"=>1
                    ]);

                    $result ['sum_week'] = $sum_week;
                    $result ['sum_month'] = $sum_month;
                    $result ['sum_year'] = $sum_year;
                    $result ['sum_by_date'] = $sum_by_date;
                    $result ['bartarinha'] = $bartarinha;
                    $result ['haraji_products'] = $haraji_products;

                }
                return view('dashboard', $result, compact('result'));
            } else {
                return redirect()->route('authentication');
            }
        } else if (user('panel_type') == 41 || user('panel_type') == 42 || user('panel_type') == 43 || user('panel_type') == 40) {

            $mode = 0;

            $top_shopkeeper = call_sp("get_top_shopkeeper_buies", 2, [
                'collection_id' => collection_id(),
                'mode' => $mode,
                'type' => 0,
            ]);

            $bartarinha = call_sp("get_top_shopkeeper_buies", 2, [
                'collection_id' => collection_id(),
                'mode' => $mode,
                'type' => 0,
            ]);
            $top_by_city = call_sp("get_top_shopkeeper_buies", 2, [
                'collection_id' => collection_id(),
                'mode' => 0,
                'type' => 1,
            ]);

            $top_products = call_sp("get_sales_report_dashboard", 2, [
                'collection_id' => collection_id(),
                'mode' => $mode,
                'type' => 2,
            ]);
            $sum_week = call_sp("get_sales_report_dashboard", 2, [
                'collection_id' => collection_id(),
                'date_from' => \Morilog\Jalali\Jalalian::forge('last week')->format('%Y/%m/%d'),
                'date_to' => jdate_from_gregorian(date('Y/m/d')),
                'mode' => $mode,
                'type' => 0,
            ]);
            $sum_month = call_sp("get_sales_report_dashboard", 2, [
                'collection_id' => collection_id(),
                'date_from' => \Morilog\Jalali\Jalalian::forge('last month')->format('%Y/%m/%d'),
                'date_to' => jdate_from_gregorian(date('Y/m/d')),
                'mode' => $mode,
                'type' => 0,
            ]);
            $sum_year = call_sp("get_sales_report_dashboard", 2, [
                'collection_id' => collection_id(),
                'date_from' => \Morilog\Jalali\Jalalian::forge('last year')->format('%Y/01/01'),
                'date_to' => jdate_from_gregorian(date('Y/m/d')),
                'mode' => $mode,
                'type' => 0,
            ]);

            $sum_by_date = call_sp("get_sales_report_dashboard", 2, [
                'collection_id' => collection_id(),
                'date_from' => \Morilog\Jalali\Jalalian::forge('now - 5 month')->format('%Y/%m/%d'),
                'date_to' => jdate_from_gregorian(date('Y/m/d')),
                'mode' => $mode,
                'type' => 1,
            ]);

            $haraji_products = call_sp("get_haraji_products", 2, [
                'collection_id' => collection_id(),
                "mode"=>0
            ]);

            $today_activities = call_sp("get_today_activities", 2, [
                'collection_id' => collection_id(),
                "mode"=>0,
                "date"=> jdate_from_gregorian(date('Y/m/d'),"Y/m/d")
            ]);

            $result ['top_shopkeeper_sales'] = $top_shopkeeper;
            $result ['top_products'] = $top_products;
            $result ['sum_week'] = $sum_week;
            $result ['sum_month'] = $sum_month;
            $result ['sum_year'] = $sum_year;
            $result ['sum_by_date'] = $sum_by_date;
            $result ['top_by_city'] = $top_by_city;
            $result ['bartarinha'] = $bartarinha;
            $result ['haraji_products'] = $haraji_products;
            $result ['today_activities'] = $today_activities;
        }

        return view('dashboard', $result);
    }
}
