<?php

namespace App\Http\Controllers;

use App\CollectionProducts;
use App\Http\Requests\ProductRequest;
use App\Product;
use App\ProductComplimentary;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:list_product', ['only' => ['index']]);
        $this->middleware('permission:order_product', ['only' => ['productList']]);
        $this->middleware('permission:delete_product', ['only' => ['destroy']]);
        $this->middleware('permission:create_product', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit_product', ['only' => ['update', 'edit']]);
        $this->middleware('permission:product_eshant', ['only' => ['eshant', 'deleteEshant', 'editEshant']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = DB::table('products')->select([
            "prod_name",
            "prod_id",
            "cat_name",
            "cp_default_count",
            'b.bas_value',
            'p.bas_value as packaging_type',
            'cp_id',
            'cp_default_price_consumer',
            'cp_default_price_buy',
            'cp_count_per',
            DB::raw("dbo.check_discount_or_eshant(cp_id, " . time() . ") as have_discount"),
            DB::raw("dbo.check_discount_or_eshant(cp_id, " . time() . ") as have_eshant")
        ])
            ->join('collection_products', 'prod_id', '=', 'cp_product_id')
            ->join('categories', 'prod_category_id', '=', 'cat_id')
            ->join('baseinfos as b', 'b.bas_id', '=', 'cp_measurement_unit_id')
            ->join('baseinfos as p', 'p.bas_id', '=', 'cp_packaging_type')
            ->where("cp_collection_id", collection_id());
        if (request()->has('prod_name') && request('prod_name') != "")
            $products = $products->where('prod_name', 'like', '%' . request('prod_name') . '%');
        if (request()->has('cat_name') && request('cat_name') != "")
            $products = $products->where('cat_name', 'like', '%' . request('cat_name') . '%');

        if (request()->has('cp_default_count_from') && request('cp_default_count_from') != "")
            $products = $products->where('cp_default_count', '>=', request('cp_default_count_from'));

        if (request()->has('cp_default_count_to') && request('cp_default_count_to') != "")
            $products = $products->where('cp_default_count', '<=', request('cp_default_count_to'));

        if (request()->has('have_discount') && request('have_discount') != "")
            $products = $products->whereRaw("dbo.check_discount_or_eshant(cp_id, " . time() . ") = ?", [request('have_discount')]);
        if (request()->has('have_eshant') && request('have_eshant') != "") {
            $arr = [0 => -2, 1 => -1];
            $products = $products->whereRaw("dbo.check_discount_or_eshant(cp_id, " . time() . ") = ?", [$arr[request('have_eshant')]]);
        }
        $products = $products->paginate(20);

        $closestDate = CollectionProducts::orderBy('updated_at', 'desc')
            ->first();

        return showData(view('product.index', compact('products','closestDate')));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $product = null;
        //$collection_id = user()->person->collections->first()->coll_id;
        $categories = DB::table('categories')
            ->select([
                'cat_id  as id',
                'cat_parent_id  as parent_id',
                'cat_name  as name',
            ])
            ->join("collection_categories", "cc_category_id", "=", "cat_id")
            ->where("cc_collection_id", collection_id())->get()->toArray();
        $categories = convertToHierarchy(json_decode(json_encode($categories), true));
        $weight_units = json_decode(json_encode(DB::table('baseinfos')->where('bas_type', 'weight_unit')->where('bas_parent_id', '!=', 0)->get()->toArray()), true);
        $width_unit = json_decode(json_encode(DB::table('baseinfos')->where('bas_type', 'width_unit')->where('bas_parent_id', '!=', 0)->get()->toArray()), true);
        return view('product.form', compact('categories', 'weight_units', 'width_unit', 'product'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        $file = null;
        if (request()->has('file')) {
            $crop= dimensionsImageCrop(  explode(',',$request->imageCrop));
            $file = uploadFile($request->file('file'), 'collection_products',1,$crop);
        }
        $result = call_sp("add_product", null, [
            'name' => "{$request->product['prod_name']}",
            'description' => /*$request->product['prod_description']*/
                null,
            'category_id' => $request->product['prod_category_id'],
            'arzesh_afzoodeh' => isset($request->collProduct['cp_arzesh_afzoodeh']) ? 1 : 0,
            'measurement_unit' => $request->collProduct['cp_measurement_unit_id'],
            'weight' => ($request->collProduct['cp_weight_of_each'] != "") ? $request->collProduct['cp_weight_of_each'] : 0,
            'tool' => ($request->collProduct['cp_dimension_length'] != "") ? $request->collProduct['cp_dimension_length'] : 0,
            'arz' => ($request->collProduct['cp_dimension_width'] != "") ? $request->collProduct['cp_dimension_width'] : 0,
            'user_id' => user('user_id'),
            'timestmp' => date("Y-m-d H:i:s"),
            'price_buy' => ($request->collProduct['cp_default_price_buy'] != "") ? $request->collProduct['cp_default_price_buy'] : 0,
            'price_consumer' => ($request->collProduct['cp_default_price_consumer'] != "") ? $request->collProduct['cp_default_price_consumer'] : 0,
            'file_path' => $file,
            'cp_weight_unit' => ($request->collProduct['cp_weight_unit']) ? $request->collProduct['cp_weight_unit'] : 0,
            'cp_width_unit' => ($request->collProduct['cp_width_unit']) ? $request->collProduct['cp_width_unit'] : 0,
            'packaging_type' => $request->collProduct['cp_packaging_type'],
            'sale_once' => isset($request->collProduct['cp_sale_once']) ? 1 : 0,
            'count_per' => $request->collProduct['cp_count_per'],
            'code_hesabdari_api' => $request->collProduct['cp_code_hesabdari_api'],
        ]);
        if ($result[0]['result'] > 0) {
            if ($request->type == 1) {//have_discount
                $model = new ProductComplimentary();
                $model->pc_type = 0;
                $model->pc_collection_product_id = $result[0]['result'];
                $model->pc_discount_percent = $request->pc_discount_percent[0];
                $model->pc_date_from = jalali_to_timestamp($request->pc_date_from_dis[0]);
                $model->pc_date_to = jalali_to_timestamp($request->pc_date_to_dis[0],'/',false);
                $model->save();
            } else if ($request->type == 2) {//have_eshant
                for ($i = 0; $i < count($request->pc_buy_count); $i++) {
                    $model = new ProductComplimentary();
                    $model->pc_type = 1;
                    $model->pc_collection_product_id = $result[0]['result'];
                    $model->pc_buy_count = $request->pc_buy_count[$i];
                    $model->pc_free_count = $request->pc_free_count[$i];

                    //check what product_id is for eshant
                    if ($request->pc_method_select_product[$i] == 1)//khode mahsol
                        $model->pc_free_collection_product_id = $result[0]['result'];
                    else // mahsole digar
                        $model->pc_free_collection_product_id = $request->pc_free_collection_product_id[$i];

                    $model->pc_date_from = jalali_to_timestamp($request->pc_date_from[$i]);
                    $model->pc_date_to = jalali_to_timestamp($request->pc_date_to[$i],'/',false);
                    $model->pc_overflow = isset($request->pc_overflow[$i]) ? 1 : 0;
                    $model->save();
                }
            }
            $array = ["status" => "100", "msg" => "محصول با موفقیت ثبت شد.",'url'=>asset('/product/create')];
        } else
            $array = ["status" => "500", "msg" => "خطا در ثبت محصول، مجددا تلاش فرمائید!"];
        return json_encode($array);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = DB::table('collection_products')->select([
            'prod_name',
            'prod_description',
            DB::raw('dbo.get_full_path(cat_id) as category'),
            DB::raw('dbo.check_discount_or_eshant(cp_id,' . time() . ') as have_discount'),
            'cp_default_count as count',
            DB::raw("(case when cp_default_count > 0 then cp_default_price_buy else 0 end) as price_buy"),
            DB::raw("(case when cp_default_count > 0 then cp_default_price_consumer else 0 end) as price_consumer"),
            'p.bas_value',
            'b.bas_value as packaging_type',
            'cp_arzesh_afzoodeh',
            'cp_weight_of_each',
            'cp_dimension_length',
            'cp_dimension_width',
            'cp_count_per',
            DB::raw("(select top 1 ur_path from upload_route where ur_table_name='collection_products' and ur_fk_id=cp_id) as imgProduct")
        ])
            ->join('products', 'prod_id', '=', 'cp_product_id')
            ->join('categories', 'prod_category_id', '=', 'cat_id')
            ->join('baseinfos as p', 'p.bas_id', '=', 'cp_measurement_unit_id')
            ->join('baseinfos as b', 'b.bas_id', '=', 'cp_packaging_type')
            ->where('cp_id', $id)
            ->first();

        $eshants = [];
        $str = "";
        if ($product->have_discount == -1) {
            $eshants = DB::table('product_complimentary')
                ->select([
                    'pc_buy_count',
                    'pc_free_count',
                    'prod_name',
                    'pc_date_from',
                    'pc_date_to'
                ])
                ->join('collection_products', 'cp_id', '=', 'pc_free_collection_product_id')
                ->join('products', 'cp_product_id', '=', 'prod_id')
                ->where('pc_type', '1')
                ->where('pc_collection_product_id', $id)
                ->get()->toArray();
            $eshants = json_decode(json_encode($eshants), true);
            $str = "<table class='table table-hover table-bordered table-stripped table-eshants'>" . tableHeader([
                    'ردیف',
                    'تعداد خرید',
                    'تعداد اشانت',
                    'محصول',
                    'بازه تاریخ'
                ]);
            $i = 1;
            $str .= "<tbody>";
            foreach ($eshants as $p) {
                if ($p['pc_date_from'] == 0 && $p['pc_date_to'] == 0)
                    $time = "نامحدود";
                else if ($p['pc_date_from'] == 0)
                    $time = "از " . '~' . ' تا ' . jdate($p['pc_date_to'])->format('Y/m/d');
                else if ($p['pc_date_to'] == 0)
                    $time = "از " . jdate($p['pc_date_from'])->format('Y/m/d') . ' تا ' . "~";
                else
                    $time = "از " . jdate($p['pc_date_from'])->format('Y/m/d') . ' تا ' . jdate($p['pc_date_to'])->format('Y/m/d');

                $str .= "<tr data-id='$id' data-count='{$p['pc_buy_count']}' data-href='" . route('add-to-basket') . "'>
                        <td>{$i}</td>
                        <td>{$p['pc_buy_count']}</td>
                        <td>" . withoutZeros($p['pc_free_count']) . "</td>
                        <td>{$p['prod_name']}</td>
                        <td>{$time}</td>
                    </tr>";
                $i++;
            }
            $str .= "</tbody></table>";
        }
        return showData(view('product.show', compact('product', 'str')));
    }

    public function eshantDetail($id)
    {
        $eshants = DB::table('product_complimentary')
            ->select([
                'pc_buy_count',
                'pc_free_count',
                'prod_name',
                'pc_date_from',
                'pc_date_to'
            ])
            ->join('collection_products', 'cp_id', '=', 'pc_free_collection_product_id')
            ->join('products', 'cp_product_id', '=', 'prod_id')
            ->where('pc_type', '1')
            ->where('pc_collection_product_id', $id)
            ->get()->toArray();
        $eshants = json_decode(json_encode($eshants), true);

        $str = "<table class='table table-hover table-bordered table-stripped table-eshants'>" . tableHeader([
                'ردیف',
                'تعداد خرید',
                'تعداد اشانت',
                'محصول',
                'بازه تاریخ'
            ]);

        $i = 1;
        foreach ($eshants as $product) {
            if ($product['pc_date_from'] == 0 && $product['pc_date_to'] == 0)
                $time = "نامحدود";
            else if ($product['pc_date_from'] == 0)
                $time = "از " . '~' . ' تا ' . jdate($product['pc_date_to'])->format('Y/m/d');
            else if ($product['pc_date_to'] == 0)
                $time = "از " . jdate($product['pc_date_from'])->format('Y/m/d') . ' تا ' . "~";
            else
                $time = "از " . jdate($product['pc_date_from'])->format('Y/m/d') . ' تا ' . jdate($product['pc_date_to'])->format('Y/m/d');

            $str .= "<tr data-id='$id' data-count='{$product['pc_buy_count']}' data-href='" . route('add-to-basket') . "'>
                        <td>{$i}</td>
                        <td>{$product['pc_buy_count']}</td>
                        <td>" . withoutZeros($product['pc_free_count']) . "</td>
                        <td>{$product['prod_name']}</td>
                        <td>{$time}</td>
                    </tr>";
            $i++;
        }

        $str .= "</table>";
        return json_encode(['status' => 100, 'FormatHtml' => $str]);
    }

    public function productList($senf = null, $cat = null)
    {
        if (user("panel_type") == 45 && !session()->has('purchasing_user_id')) //is visitor va karbar baraye kharid entekhab nashode
            abort(403);

        $max_price = DB::select("select isnull(max(cp_default_price_consumer),0) as mx_price from collection_products ");


        //get products
        $array_params = [];
        if (\request()->has('price_range') && \request('price_range') != '') {
            list($array_params['price_from'], $array_params['price_to']) = explode(',', \request('price_range'));
        }

        if (\request()->has('mojod') && \request('mojod') == 1) {
            $array_params['mojod'] = 1;
        }
        if (\request()->has('takhfif') && \request('takhfif') == 1) {
            $array_params['takhfif'] = 1;
        }
        if (\request()->has('eshant') && \request('eshant') == 1) {
            $array_params['eshant'] = 1;
        }

        $array_params ['senf'] = deSlg($senf);
        $array_params ['cat_name'] = deSlg($cat);
        $array_params ['sort'] = 'prod_id';//optional
        $array_params ['direction'] = 'desc';//optional
        $array_params ['per_page'] = \request('per_page', 20);//optional
        $array_params ['page'] = \request('page', 1);//optional
        $tmp = findUser(user("user_id"), "collection",'all');
        $array_params ['city_id'] = $tmp->coll_city_id;
        $array_params ['collection_id'] = collection_id();

        if (request()->has('q') && \request('q') != '')
            $array_params ['q'] = request('q');//optional

        if (request()->has('sub_cats')) {
            $category_array = [];
            foreach ($_GET['sub_cats'] as $com) {
                array_push($category_array, [
                    'ID' => $com
                ]);
            }
            $array_params ['sub_cats'] = createRootData($category_array);//optional
        }
        if (request()->has('companies')) {
            $company_array = [];
            foreach (\request()->get('companies') as $c) {
                array_push($company_array, [
                    'ID' => $c
                ]);
            }
            $array_params ['companies'] = createRootData($company_array);//optional
        }

        $count = call_sp('show_products_product_list_count', null, $array_params);
        $pagination = new \Illuminate\Pagination\LengthAwarePaginator(@$count[0]['cnt'] / $array_params['per_page'], @$count[0]['cnt'], $array_params['per_page'], $array_params['page'], [
            'path' => ''
        ]);
        $products = call_sp('show_products_product_list_rows', null, $array_params);

        if (\request()->ajax()) {
            $string = "";
            if (!empty($products))
                foreach ($products as $product) {
                    $string .= '<div class="col-6 col-lg-4 col-md-6">
                                <div class="card">
                                    <div class="card-body">
                                     <div class="thumbnail">
                                        <img class="card-img-top img-fluid" src="' . getFile(($product['file_path'] != "") ? $product['file_path'] : 'default.jpg') . '"
                                             alt="Card image cap">';
                    if ($product['cp_default_count'] == 0)
                        $string .= '<div class="caption-wrapper">
                                        <div class="caption">ناموجود</div>
                                    </div>';
                    else if ($product['have_discount'] > 0)
                        $string .= '<div class="percent-wrapper">
                                                                    <div class="percent">%' . withoutZeros($product['have_discount']) . '</div>
                                                                </div>';

                    else if ($product['have_discount'] == -1)
                        $string .= '<div class="eshant-wrapper">
                                                                    <div class="eshant">اشانت</div>
                                                                </div>';
                    $string .= '</div>
                                        <div class="card-block">
                                            <p class="card-title">' . $product['prod_name'];

                    $string .= '</p>
                                    <p><span>موجودی:</span> ' . number_format(withoutZeros($product['cp_default_count'])) . '</p>
                                            <p class="price">
                                                <span class="tag tag-default">قیمت:</span>';

                    if ($product['have_discount'] > 0) {
                        $price = "<span class='danger' style='text-decoration: line-through'>" . number_format($product['price'])
                            . "</span> <span class='success'>" . number_format($product['price'] * (100 - $product['have_discount']) / 100) . "</span>";
                    } else
                        $price = number_format($product['price']);

                    if ($product['cp_default_count'] > 0)
                        $string .= $price . ' ' . setting('financial_unit');
                    else
                        $string .= " نامشخص ";

                    $string .= '</p>';
                    if ($product['cp_default_count'] > 0) {
                        $string .= ' <div class="input-group">
                                            <select name="measure_unit" class="form-control">
                                            <option value = "1" > ' . $product["bas_value"] . '</option>';

                        if ($product['cp_sale_once']==1)
                            $string .= '<option value="0">تکی</option>';
                        $string .= '</select>';
                        $string .= '<input class="form-control" name="count_product" placeholder="تعداد">';

                        $string .= '<a class="btn btn-success btn-add-to-basket" data-href="' . url('add-to-basket') . '" data-id="' . $product['prod_id'] . '" title="افزودن به سبد خرید"><i class="fa fa-shopping-basket"></i></a> ';
                    } else
                        $string .= '<a class="btn btn-danger" title="ناموجود"><i class="fa fa-shopping-basket"></i></a> ';

                    $string .= '<a class="btn btn-primary product-detail" data-href="' . route('product.show', $product['prod_id']) . '" title="جزئیات محصول"><i class="fa fa-eye"></i></a> ';
                    if ($product['have_discount'] == -1)
                        $string .= '<a class="btn btn-info eshant-detail" data-href="' . route('eshant-detail', $product['prod_id']) . '" data-id="' . $product['prod_id'] . '" title="جزئیات اشانتیون"><i class="fa fa-info"></i></a> ';
                    $string .= '</div>
                                    </div>
                                </div>
                                </div>
                            </div>';
                }
            else
                $string .= '<div class="alert alert-danger col-sm-12"
                                 style="margin-right: -10px;color:#FFFFFF !important;">موردی یافت نشد.
                            </div>';
            return json_encode(['status' => 100, 'data' => $string, 'links' => '<div>' . $pagination->appends(request()->query())->links() . '</div>']);
        }

        //get menu and submenus and filters items
        $senfs = call_sp('show_senf_and_catergory_product_list', null, [
            'user_id' => user('user_id')
        ]);
        $sub_cats = [];
        $temp = [];
        foreach ($senfs as $snf) {
            if ($cat != null)
                if (strstr($snf['full_path'], deSlg($cat)))
                    $sub_cats [] = $snf;
            $temp[$snf['bas_value']][] = $snf;
        }
        $senfs = $temp;

        //get companies for filters
        if ($cat != null) {
            $companies = DB::table('collections')
                ->select([
                    DB::raw('distinct(coll_id)'),
                    'coll_name'
                ])
                ->join('area_support', 'as_collection_id', '=', 'coll_id')
                ->join('collection_persons', 'cp_coll_id', '=', 'coll_id')
                ->join('collection_classes', 'coc_coll_id', '=', 'coll_id')
                ->join('persons', 'prsn_id', '=', 'cp_prsn_id')
                ->join('users', 'user_id', '=', 'prsn_user_id')
                ->where('as_city_id', $tmp->coll_city_id)
                ->where('coll_hide', 0)
                ->whereRaw('coll_id != '. collection_id())
                ->whereRaw('coc_typeClass_id in(select coc_typeClass_id from collection_classes where coc_coll_id = '.$tmp->coll_id.')')
                ->whereIn('panel_type', [42, 43])//only companies and wholesalers
                ->get()
                ->toArray();
        } else
            $companies = [];
        $max_price = withoutZeros($max_price[0]->mx_price);
        return view('product.product-list', compact('products', 'senfs', 'companies', 'sub_cats', 'pagination', 'senf', 'cat','max_price'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function edit(Request $request, $id)
    {
        $categories = DB::table('categories')
            ->select([
                'cat_id  as id',
                'cat_parent_id  as parent_id',
                'cat_name  as name',
            ])
            ->join("collection_categories", "cc_category_id", "=", "cat_id")
            ->where("cc_collection_id", collection_id())->get()->toArray();
        $categories = convertToHierarchy(json_decode(json_encode($categories), true));
        $weight_units = json_decode(json_encode(DB::table('baseinfos')->where('bas_type', 'weight_unit')->where('bas_parent_id', '!=', 0)->get()->toArray()), true);
        $width_unit = json_decode(json_encode(DB::table('baseinfos')->where('bas_type', 'width_unit')->where('bas_parent_id', '!=', 0)->get()->toArray()), true);
        if ($product = DB::table('collection_products')
            ->select([
                'collection_products.*',
                'prod_name',
                'prod_category_id',
                DB::raw("dbo.check_discount_or_eshant(cp_id, " . time() . ") as have_discount"),
                DB::raw("dbo.check_discount_or_eshant(cp_id, " . time() . ") as have_eshant"),
                DB::raw("(select top 1 ur_path from upload_route where ur_table_name= 'collection_products' and ur_fk_id= collection_products.cp_id )as image"),
            ])
            ->where('cp_id', $id)
            ->where('cp_collection_id', collection_id())
            ->leftJoin('products', 'prod_id', 'cp_product_id')
            ->first()) {

            $eshant = DB::table('product_complimentary')
                ->select([
                    'product_complimentary.*',
                    DB::raw("(select top 1 products.prod_name from collection_products as b
            LEFT JOIN collection_products as p ON p.cp_id=product_complimentary.pc_free_collection_product_id
			LEFT JOIN products ON p.cp_product_id=products.prod_id) as eshant")
                ])
                ->where('pc_collection_product_id', $product->cp_id)
                ->first();
            $backUrl=redirect()->back()->getTargetUrl();
            return view('product.edit_form', compact('product', 'categories', 'weight_units', 'width_unit', 'eshant','backUrl'));
        } else
            abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $file = null;
        if (request()->has('file')) {
            $crop= dimensionsImageCrop(  explode(',',$request->imageCrop));
            DB::table('upload_route')->where('ur_table_name', 'collection_products')
                ->where('ur_fk_id', $id)->delete();
            $file = uploadFile($request->file('file'), 'collection_products', 111,$crop);
        }
        $result = call_sp("add_product", null, [
            'cp_id' => $id,
            'mode' => 2,
            'name' => "{$request->product['prod_name']}",
            'description' => /*$request->product['prod_description']*/
                null,
            'category_id' => $request->product['prod_category_id'],
            'arzesh_afzoodeh' => isset($request->collProduct['cp_arzesh_afzoodeh']) ? 1 : 0,
            'measurement_unit' => $request->collProduct['cp_measurement_unit_id'],
            'weight' => ($request->collProduct['cp_weight_of_each'] != "") ? $request->collProduct['cp_weight_of_each'] : 0,
            'tool' => ($request->collProduct['cp_dimension_length'] != "") ? $request->collProduct['cp_dimension_length'] : 0,
            'arz' => ($request->collProduct['cp_dimension_width'] != "") ? $request->collProduct['cp_dimension_width'] : 0,
            'user_id' => user('user_id'),
            'timestmp' => date("Y-m-d H:i:s"),
            'price_buy' => ($request->collProduct['cp_default_price_buy'] != "") ? $request->collProduct['cp_default_price_buy'] : 0,
            'price_consumer' => ($request->collProduct['cp_default_price_consumer'] != "") ? $request->collProduct['cp_default_price_consumer'] : 0,
            'file_path' => $file,
            'cp_weight_unit' => ($request->collProduct['cp_weight_unit']) ? $request->collProduct['cp_weight_unit'] : 0,
            'cp_width_unit' => ($request->collProduct['cp_width_unit']) ? $request->collProduct['cp_width_unit'] : 0,
            'packaging_type' => $request->collProduct['cp_packaging_type'],
            'sale_once' => isset($request->collProduct['cp_sale_once']) ? 1 : 0,
            'count_per' => ($request->collProduct['cp_count_per']) ? $request->collProduct['cp_count_per'] : 0,
            'code_hesabdari_api' => $request->collProduct['cp_code_hesabdari_api'],
        ]);
        if ($result[0]['result'] == 0) {
            $pc_id = DB::table('product_complimentary')->where('pc_collection_product_id', $id)->first();

            if ($pc_id) {  //update ProductComplimentary

                if ($request->type == 1) {//have_discount
                    $model = ProductComplimentary::find($pc_id->pc_id);
                    $model->pc_type = 0;
                    $model->pc_buy_count = 0;
                    $model->pc_free_count = 0;
                    $model->pc_free_collection_product_id = 0;
                    $model->pc_discount_percent = $request->pc_discount_percent[0];
                    $model->pc_date_from = jalali_to_timestamp($request->pc_date_from_dis[0]);
                    $model->pc_date_to = jalali_to_timestamp($request->pc_date_to_dis[0],'/',false);
                    $model->save();
                } else if ($request->type == 2) {//have_eshant
                    for ($i = 0; $i < count($request->pc_buy_count); $i++) {
                        $model = ProductComplimentary::find($pc_id->pc_id);
                        $model->pc_type = 1;
                        $model->pc_discount_percent = 0;
                        $model->pc_buy_count = $request->pc_buy_count[$i];
                        $model->pc_free_count = $request->pc_free_count[$i];

                        //check what product_id is for eshant
                        if ($request->pc_method_select_product[$i] == 1)//khode mahsol
                            $model->pc_free_collection_product_id = $id;
                        else // mahsole digar
                            $model->pc_free_collection_product_id = $request->pc_free_collection_product_id[$i];

                        $model->pc_date_from = jalali_to_timestamp($request->pc_date_from[$i]);
                        $model->pc_date_to = jalali_to_timestamp($request->pc_date_to[$i],'/',false);
                        $model->pc_overflow = isset($request->pc_overflow[$i]) ? 1 : 0;
                        $model->save();
                    }
                } else if ($request->type == 3) { //delete ProductComplimentary
//                    dd($request->type);
                    ProductComplimentary::where('pc_collection_product_id', $id)->delete();
                }
            } else { //create ProductComplimentary
                if ($request->type == 1) {//have_discount
                    $model = new ProductComplimentary();
                    $model->pc_type = 0;
                    $model->pc_collection_product_id = $id;
                    $model->pc_discount_percent = $request->pc_discount_percent[0];
                    $model->pc_date_from = jalali_to_timestamp($request->pc_date_from_dis[0]);
                    $model->pc_date_to = jalali_to_timestamp($request->pc_date_to_dis[0],'/',false);
                    $model->save();
                } else if ($request->type == 2) {//have_eshant
                    for ($i = 0; $i < count($request->pc_buy_count); $i++) {
                        $model = new ProductComplimentary();
                        $model->pc_type = 1;
                        $model->pc_collection_product_id = $id;
                        $model->pc_buy_count = $request->pc_buy_count[$i];
                        $model->pc_free_count = $request->pc_free_count[$i];

                        //check what product_id is for eshant
                        if ($request->pc_method_select_product[$i] == 1)//khode mahsol
                            $model->pc_free_collection_product_id = $id;
                        else // mahsole digar
                            $model->pc_free_collection_product_id = $request->pc_free_collection_product_id[$i];

                        $model->pc_date_from = jalali_to_timestamp($request->pc_date_from[$i]);
                        $model->pc_date_to = jalali_to_timestamp($request->pc_date_to[$i],'/',false);
                        $model->pc_overflow = isset($request->pc_overflow[$i]) ? 1 : 0;
                        $model->save();
                    }
                }

            }
            $array = ["status" => "100", "msg" => "محصول با موفقیت ثبت شد.", 'url'=>$request->backUrl];
        }
        else

            $array = ["status" => "500", "msg" => "خطا در ثبت محصول، مجددا تلاش فرمائید!"];
        return json_encode($array);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $result = checkRelation('collection_products', $id);
        if (empty($result) || $result[0]['ID'] == 0) {
            $check_eshants = DB::table("product_complimentary")
                ->where("pc_free_collection_product_id", $id)
                ->first();
            //agar in mahsol eshante mahsole digari ast
            if ($check_eshants)
                if ($check_eshants->pc_collection_product_id != $check_eshants->pc_free_collection_product_id)
                    return \redirect()->back()->with("error", config('first_config.message.relation_delete'));

            DB::delete('delete from product_complimentary where pc_collection_product_id = ?', [$id]);
            DB::delete('delete from product_complimentary where pc_free_collection_product_id = ?', [$id]);
            DB::delete('delete from inventory_factor_detail where ifd_collection_product_id = ?', [$id]);
            DB::delete('delete from collection_products where cp_id = ?', [$id]);
            return \redirect()->back()->with("success", "عملیات باموفقیت انجام شد!");
        } else {
            return \redirect()->back()->with("error", config('first_config.message.relation_delete'));
        }
    }

    public function deleteEshant($id)
    {
        $res = ProductComplimentary::where('pc_id', $id);
        if ($res)
            $res->delete();
        return redirect()->back();
    }

    public function eshant(Request $request, $id)
    {
        $prod_name = CollectionProducts::where("cp_id", $id)
            ->select('prod_name')->where('cp_collection_id', collection_id())
            ->join("products", 'prod_id', 'cp_product_id')->first();
        if (!$prod_name)
            abort(404);
        $prod_name = $prod_name->prod_name;
        if ($request->isMethod('post')) {
            $request->validate([
                'pc_buy_count' => 'required',
                'pc_free_count' => 'required',
//                'pc_date_from' => 'jdate',
//                'pc_date_to' => 'jdate',
                'pc_free_collection_product_id' => ['required_if:pc_method_select_product,2'],
            ], [
                'pc_free_collection_product_id.required_if' => 'فیلد محصول الزامی است'
            ]);
            $model = new ProductComplimentary();
            $model->pc_collection_product_id = $id;
            $model->pc_buy_count = request()->get('pc_buy_count');
            $model->pc_free_count = request()->get('pc_free_count');
            $model->pc_type = true;
            $model->pc_overflow = request()->has('pc_overflow') ? true : false;
            if (request()->get('pc_method_select_product') == 2)
                $model->pc_free_collection_product_id = request()->get('pc_free_collection_product_id');
            else
                $model->pc_free_collection_product_id = $id;
            $model->pc_date_from = jalali_to_timestamp(request()->get('pc_date_from'));
            $model->pc_date_to = jalali_to_timestamp(request()->get('pc_date_to'));
            if ($model->save())
                return json_encode(['status' => 100, 'msg' => 'با موفقیت ثبت شد.']);
            else
                return json_encode(['status' => 500, 'msg' => 'خطا در افزودن اشانت، مجددا تلاش فرمائید.']);
        }
        $eshants = ProductComplimentary::where('pc_collection_product_id', $id)
            ->select([
                'prod_name',
                'product_complimentary.*'
            ])
            ->join('collection_products', 'pc_free_collection_product_id', '=', 'cp_id')
            ->join('products', 'prod_id', '=', 'cp_product_id')
            ->where('pc_type', true)
            ->orderBy('pc_buy_count')
            ->get()->toArray();
        return view("product.eshants", compact('eshants', 'prod_name', 'id'));
    }

    public function editEshant(Request $request, $id)
    {
        $prod = ProductComplimentary::where("pc_id", request()->get('id'))
            ->join('collection_products', 'pc_collection_product_id', '=', 'cp_id')
            ->where('cp_collection_id', collection_id())->first();
        if (!$prod)
            abort(404);
        if ($request->isMethod('post')) {
            $request->validate([
                'pc_buy_count' => 'required',
                'pc_free_count' => 'required',
//                'pc_date_from' => 'jdate',
//                'pc_date_to' => 'jdate',
                'pc_free_collection_product_id' => ['required_if:pc_method_select_product,2'],
            ], [
                'pc_free_collection_product_id.required_if' => 'در حالت محصول دیگر، انتخاب محصول الزامیست'
            ]);
            $model = ProductComplimentary::where('pc_id', request()->get('id'));
            $array = [
                'pc_buy_count' => request()->get('pc_buy_count'),
                'pc_free_count' => request()->get('pc_free_count'),
                'pc_overflow' => request()->has('pc_overflow') ? true : false,
                'pc_date_from' => jalali_to_timestamp(request()->get('pc_date_from')),
                'pc_date_to' => jalali_to_timestamp(request()->get('pc_date_to')),
            ];
            $array['pc_free_collection_product_id'] = request()->get('pc_free_collection_product_id');

            if ($model->update($array))
                return json_encode(['status' => 100, 'msg' => 'با موفقیت ثبت شد.']);
            else
                return json_encode(['status' => 500, 'msg' => 'خطا در افزودن اشانت، مجددا تلاش فرمائید.']);
        }
    }
}
