<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BasketController extends Controller
{
    public function basketItems()
    {
        $result = getBasketItems();
        return view('basket.items', compact('result'));
    }

    public function basketItemDetails(Request $request, $id)
    {
        if (request()->isMethod("post")) {

            //validate inputs
            $array = [
                'ret_fd_id.*' => ['required'],
                'ret_count.*' => ['required'],
                'ret_reason.*' => ['required'],
            ];
            $request->validate($array);

            //create returns array and save
            $array = [];
            if (request()->ret_fd_id)
                foreach (request()->ret_fd_id as $key => $value) {
                    if ($value && request()->ret_count[$key] && request()->ret_reason[$key]) {
                        array_push($array, [
                            'PR' => $value,
                            'CN' => request()->ret_count[$key],
                            'RS' => request()->ret_reason[$key]
                        ]);
                    }
                }
            //if not selected check invalid item

            $user_id = getUserIdForAddFactor();
            $result = call_sp('factor_master_sp', null,
                [
                    'mode' => 1,
                    'self_collection_id' => getCollectionForAddFactor($user_id),
                    'user_id' => $user_id,
                    'fm_porsant_user_id' => ($user_id == user('user_id')) ? user("reagent_id") : user('user_id'),
                    'description' => (request()->has('description') && request()->description != '') ? request()->description : null,
                    'shamsi_date' => jdate()->format('Y/m/d'),
                    'payment_method' => 0,
                    'created_at' => time(),
                    'return_reasons' => createRootData($array),
                    'fm_id' => $id
                ]
            );
            if ($result[0]['result'] == 0)
                return json_encode(['status' => 100, 'msg' => 'با موفقیت ثبت شد.', 'url' => asset('factor/payment') . '/' . $id]);
            else
                return json_encode(['status' => 500, 'msg' => 'خطا در ثبت فاکتور، مجددا تلاش فرمائید.']);
        }
        $factor_returns = DB::table('factor_returns')
            ->select([
                'prod_name',
                'fd_id',
                'fr_count',
                'fd_price_buy',
                'fr_reason_id'
            ])
            ->leftJoin('factor_detail', 'fd_id', '=', 'fr_fd_id')
            ->leftJoin('collection_products', 'cp_id', '=', 'fd_product_id')
            ->leftJoin('products', 'cp_product_id', '=', 'prod_id')
            ->where('fr_master_id', $id)
            ->get();
        $result = getBasketItems($id);
        if (!$result)
            return \redirect()->route('basket-items');
        $tax = setting('tax');
        extract(getFactorDetailsDependencies($id, $result));
        return view('basket.item-details', compact('result', 'id', 'person_collection', 'product_return_permit', 'return_reasons', 'talab', 'tax', 'factor_returns'));
    }

    public
    function deleteFactor($id)
    {
        $result = deleteFactorFromBasket($id);
        if ($result)
            return \redirect()->back()->with('success', 'با موفقیت حذف شد!');
        return \redirect()->back()->with('error', 'خطا در انجام حذف، مجددا تلاش فرمائید.');
    }

    public
    function deleteProductFromFactor($id)
    {
        $result = removeFromBasket($id);
        if ($result)
            return \redirect()->back();
        return \redirect()->back()->with('error', 'خطا در انجام حذف، مجددا تلاش فرمائید.');
    }

    public
    function updateCount()
    {
        $result = addToBasket(request('id'), request('count'));
        if ($result)
            return json_encode(['status' => 100, 'msg' => 'با موفقیت انجام شد!']);
        return json_encode(['status' => 500, 'msg' => 'متاسفانه درخواست شما انجام نشد!']);
    }

    public
    function loadBasketItems($id)
    {
        $result = getBasketItems($id);
        if (!$result)
            return \redirect()->route('basket-items');
        extract(getFactorDetailsDependencies($id, $result));
        return showData(view('basket.item-details', compact('result', 'id', 'talab', 'return_reasons', 'person_collection', 'product_return_permit')));
    }
}
