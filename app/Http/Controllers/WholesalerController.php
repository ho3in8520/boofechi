<?php

namespace App\Http\Controllers;

use App\Collection;
use App\Component\Tools;
use App\Http\Requests\WholesalerRequest;
use App\Person;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WholesalerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = Tools::getList($request, 43);
//        dd($data);
        return showData(view('wholesalers.index', compact('data')));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('wholesalers.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(WholesalerRequest $request)
    {
        $result = Tools::collection_store($request, 43);
        return redirect('/user/create?prsn_id=' . $result);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Collection $wholesaler)
    {
        $collections = $wholesaler;
        $persons = $wholesaler->persons->first();
        $collectionClass = DB::table('collection_classes')->whereCocCollId($wholesaler->coll_id)->pluck('coc_typeClass_id')->toArray();
        return view("wholesalers.form", compact('collections', 'persons', 'collectionClass'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(WholesalerRequest $request, Collection $wholesaler)
    {
        $result = Tools::collection_update($request, $wholesaler);
        $person = $wholesaler->persons->first();
        if ($person->prsn_user_id == 1)
            return redirect('/user/create?prsn_id=' . $person->prsn_id);
        return redirect('/user/' . $person->prsn_user_id . '/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



}
