<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    public function username()
    {
        return 'username';
    }

    protected function credentials(Request $request)
    {
//        return array_merge($request->only($this->username(), 'password'), ['is_active' => 1]);
        return array_merge($request->only($this->username(), 'password'));
    }

    public function login(\Illuminate\Http\Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }

        // This section is the only change
        if ($this->guard()->validate($this->credentials($request))) {
            $user = $this->guard()->getLastAttempted();
            // Make sure the user is active
            if ($user->is_active && $this->attemptLogin($request)) {
                // Send the normal successful login response
                return $this->sendLoginResponse($request);
            } else {
                // Increment the failed login attempts and redirect back to the
                // login form with an error message.
                try {
                    $this->incrementLoginAttempts($request);
                    $message = DB::table("violation_log")->select(['vl_reason'])->where('vl_user_id', $user->user_id)->where('vl_type', 1)->latest("vl_id")->first();
                    if ($message)
                        $message = "متاسفانه پنل شما به علت " . "\"<b>" . $message->vl_reason . "</b>\"" . " ، به حالت تعلیق درآمده است.";
                    else
                        $message = "متاسفانه پنل شما، به حالت تعلیق درآمده است.";
                    return redirect()
                        ->back()
                        ->withInput($request->only($this->username(), 'remember'))
                        ->with('active', $message);
                } catch (\Exception $e) {
                    return redirect()
                        ->back()
                        ->withInput($request->only($this->username(), 'remember'))
                        ->with('active', "مشکل در ورود به سامانه");
                }
            }
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function validateLogin(Request $request)
    {
        $request->validate([
            $this->username() => 'required|string',
            'password' => 'required|string',
            // "g-recaptcha-response"=>["required",function($attr,$value,$fail){
            //     if(isset($value)){
            //         $ip = $_SERVER['REMOTE_ADDR'];
            //         $secretkey = "6LccxuYUAAAAAJWhAD5TiNOa1byxf79iyvuxq9gY";
            //         $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secretkey."&response=".$value."&remoteip=".$ip);
            //         $responseKeys = json_decode($response,true);
            //         if(intval($responseKeys["success"]) !== 1) {
            //             $fail("خطا کد امنیتی");
            //         } else {
            //             return true;
            //         }
            //     }
            // }],
        ]);
    }

    protected function authenticated(Request $request, $user)
    {
        //delete all expired factors from basket
        emptyBasket();

        //send sms
        sendMessage("ورود به سامانه بوفه چی در تاریخ " . jdate()->format('Y/m/d ساعت H:i:s '), [user("user_id")]);
    }

    public function logout(Request $request)
    {
        if(Session::exists('is_admin')){
            Auth::loginUsingId(Session::get('is_admin'));
            Session::remove('is_admin');
            return redirect('/');
        }else {
            $this->guard()->logout();

            $request->session()->invalidate();

            return $this->loggedOut($request) ?: redirect('/');
        }
    }
}
