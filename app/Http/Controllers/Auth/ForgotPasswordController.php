<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\DB;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

//    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function forget_password(Request $request)
    {
        if ($request->isMethod('post')) {
            $request->validate([
                'mobile' => ['required', 'regex: /^(0)?9\d{9}$/'],
            ]);
            if ($find_user = DB::table('persons')
                ->select([
                    'user_id',
                    'username',
                    'time_forget',
                ])
                ->where('prsn_mobile1', $request->mobile)
                ->leftJoin('users', 'user_id', 'prsn_user_id')
                ->first()) {
                if ($find_user->time_forget > time()) {
                    return json_encode(['status' => 210, 'time' => $find_user->time_forget - time()]);
                } else {
                    $password = str_random(8);
                    $time = time() + 60;
                    DB::table('users')
                        ->where('user_id', $find_user->user_id)
                        ->update([
                            'time_forget' => $time,
                            'password' => Hash::make($password)
                        ]);
                    sendMessage(generateMessage("sms_forgetPassword", ["%username%" => $find_user->username, "%code%" => $password]), [$find_user->user_id]);
                    return json_encode(['status' => 100, 'time' => 60]);
                }
            } else {
                return json_encode(['status' => 220, 'msg' => 'کاربری با این شماره یافت نشد']);
            }
        }
        return showData(view('auth.forget-password'));
    }
}
