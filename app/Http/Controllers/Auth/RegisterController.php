<?php

namespace App\Http\Controllers\Auth;

use App\Collection;
use App\Http\Controllers\Controller;
use App\Person;
use App\User;
use http\Env\Request;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/login';


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        return Validator::make($data, [
            'username' => ['required', 'string', 'max:255', 'unique:users,username'],
            'mobile' => ['required', 'string', 'max:255', 'unique:persons,prsn_mobile1', 'unique:persons,prsn_mobile2'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email'],
            'class' => function ($attribute, $value, $fail) {
                $array = [1, 2];
                if (!in_array(request()->get('class'), $array)) {
                    $fail('نوع پنل نامعتبر است!');
                }
            }
            //'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\User
     */
    public function register(\Illuminate\Http\Request $request)
    {
        $data = $request->all();
        $request->validate([
            'username' => ['required', 'string', 'max:255', 'unique:users,username'],
            'mobile' => ['required', 'string', 'max:255', 'unique:persons,prsn_mobile1', 'unique:persons,prsn_mobile2'],
            'reagent' => ['nullable', 'exists:persons,prsn_national_code'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email'],
            'class' => function ($attribute, $value, $fail) {
                $array = [1, 2];
                if (!in_array(request()->get('class'), $array)) {
                    $fail('نوع پنل نامعتبر است!');
                }
            }
        ]);

        $pass = generateRandomNumber(5);
        $code = generateRandomString(10);
        $array = [1 => 44, 2 => 45];
        $panel_type = $array[$data['class']];
        DB::beginTransaction();
        try {
            $agent = 0;
            if ($data['reagent'] != '') {
                $agent = DB::table('users')
                    ->select([
                        'user_id'
                    ])
                    ->leftJoin('persons', 'prsn_user_id', '=', 'user_id')
                    ->where('prsn_national_code', $data['reagent'])
                    ->first();
                if ($agent)
                    $agent = $agent->user_id;
                else
                    $agent = 0;
            }

            if ($user = User::create([
                'username' => $data['username'],
                //'code' => $code,
                'email' => $data['email'],
                'password' => Hash::make($pass),
                'reagent_id' => $agent,
                'panel_type' => $panel_type,
                'is_active' => 1,
                'agent_id' => getAgent()->ag_id
            ])
            ) {
                $collection = Collection::create([
                    'coll_name' => $data['username'],
                ]);
                $person = Person::create([
                    'prsn_mobile1' => $data['mobile'],
                    'prsn_user_id' => $user->user_id,
                    'prsn_gender_id' => 12,
                ]);
                DB::table('collection_persons')->insert([
                    'cp_prsn_id' => $person->prsn_id,
                    'cp_coll_id' => $collection->coll_id
                ]);
                DB::commit();
                //send $pass with sms
                sendMessage(generateMessage("sms_sendPassword", ["%code%" => $pass]), [$user->user_id]);
                return json_encode(['status' => 100, 'msg' => 'اطلاعات با موفقیت ثبت شد! ', 'url' => route('login')]);
            }
        } catch (\Exception $e) {
//            echo $e->getMessage();
//            exit();
            return json_encode(['status' => 500, 'msg' => 'خطا در ثبت اطلاعات، مجددا تلاش فرمائید!']);

        }
    }

    protected function create(array $data)
    {

    }
}
