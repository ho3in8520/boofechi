<?php

namespace App\Http\Controllers;

use App\Export;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class ReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:reports_factors', ['only' => ['reportSaleFactors']]);
        $this->middleware('permission:reports_products', ['only' => ['reportSaleProducts']]);
        $this->middleware('permission:reports_totally', ['only' => ['reportSaleTotally']]);
    }

    public function reportSaleFactors(Request $request)
    {
        //sendMessage(generateMessage("sms_factorBuy", ["%number%" => 133]), [2]);
        $params = $request->all();
        $params = array_filter($params, function ($value) {
            return !is_null($value) && $value !== '';
        });
        $array = [
            'fm_collection_id' => isset($params['fm_collection_id']) ? $request->get('fm_collection_id') : 0,
            'fm_self_collection_id' => isset($params['fm_self_collection_id']) ? $request->get('fm_self_collection_id') : 0,
            'fm_no' => isset($params['fm_no']) ? $request->get('fm_no') : 0,
            'fm_date_from' => isset($params['fm_date_from']) ? $request->get('fm_date_from') : '____/__/__',
            'fm_date_to' => isset($params['fm_date_to']) ? $request->get('fm_date_to') : '____/__/__',
            'fm_description' => isset($params['fm_description']) ? $request->get('fm_description') : '',
            'fm_porsant_user_id' => isset($params['fm_porsant_user_id']) ? $request->get('fm_porsant_user_id') : 0,
            'fm_creator_id' => isset($params['fm_creator_id']) ? $request->get('fm_creator_id') : 0,
            'cp_id' => isset($params['product_id']) ? $request->get('product_id') : 0,
            'city_id' => isset($params['city']) ? $request->get('city') : 0,
            'state_id' => isset($params['state']) ? $request->get('state') : 0
        ];
        if (in_array(user('panel_type'), [40, 41, 42, 43])) {
            $array ["fm_collection_id"] = collection_id();
        } else if (in_array(user('panel_type'), [44])) {
            $array ["fm_self_collection_id"] = collection_id();
        }

        $records = call_sp('report_sale_factors_by_factor', null, $array);
        $headers = [
            "ردیف",
            "شماره فاکتور",
            "تاریخ",
            "مبلغ",
            "تخفیف جشنواره ای",
            "تخفیف روش پرداخت",
           // "طلب از قبل",
            "فروشنده",
            "خریدار",
        ];
        $temp = [];
        $i = 0;
        foreach ($records as $record) {
            $record["fm_id"] = ++$i;
            $record["fm_amount"] = number_format($record["fm_amount"]);
            $record["fm_festival_discount"] = number_format($record["fm_festival_discount"]);
            $record["fm_payment_discount"] = number_format($record["fm_payment_discount"]);
            //$record["fm_talab_az_ghabl"] = number_format($record["fm_talab_az_ghabl"]);
            $temp[] = $record;
        }
        $records = $temp;
        $sum = [
            'fm_amount',
            'fm_festival_discount',
            'fm_payment_discount',
           // 'fm_talab_az_ghabl',
        ];

        $array['report_type'] = "factors";
        $title = createPrintTitleFromFilters($array);
        if (isset($params['export-type'])) {
            if ($params['export-type'] == "print") {
                return view('print.print', compact('records', 'headers', 'title', 'sum'));
            } else if ($params['export-type'] == "excel") {
                $export = new Export\BladeExport($headers, $records, $title);
                return Excel::download($export, 'invoices.xlsx');
            }
        }
        return view('report.report_sale_factors', compact('factors'));
    }

    public
    function reportSaleProducts(Request $request)
    {
        $params = $request->all();
        $params = array_filter($params, function ($value) {
            return !is_null($value) && $value !== '';
        });
        $array = [
            'fm_collection_id' => isset($params['fm_collection_id']) ? $request->get('fm_collection_id') : 0,
            'fm_self_collection_id' => isset($params['fm_self_collection_id']) ? $request->get('fm_self_collection_id') : 0,
            'fm_no' => isset($params['fm_no']) ? $request->get('fm_no') : 0,
            'fm_date_from' => isset($params['fm_date_from']) ? $request->get('fm_date_from') : '____/__/__',
            'fm_date_to' => isset($params['fm_date_to']) ? $request->get('fm_date_to') : '____/__/__',
            'fm_description' => isset($params['fm_description']) ? $request->get('fm_description') : '',
            'fm_porsant_user_id' => isset($params['fm_porsant_user_id']) ? $request->get('fm_porsant_user_id') : 0,
            'fm_creator_id' => isset($params['fm_creator_id']) ? $request->get('fm_creator_id') : 0,
            'cp_id' => isset($params['product_id']) ? $request->get('product_id') : 0,
            'city_id' => isset($params['city']) ? $request->get('city') : 0,
            'state_id' => isset($params['state']) ? $request->get('state') : 0,
            'report_type' => isset($params['report-type']) ? $params["report-type"] : "totally"
        ];
        if (in_array(user('panel_type'), [40, 41, 42, 43])) {
            $array ["fm_collection_id"] = collection_id();
        } else if (in_array(user('panel_type'), [44])) {
            $array ["fm_self_collection_id"] = collection_id();
        }
        $records = call_sp('report_sale_products_by_product', null, $array);
        if (isset($params["report-type"])) {
            switch ($params["report-type"]) {
                case "totally":
                    $headers = [
                        "ردیف",
                        "محصول",
                        "تعداد",
                        "مبلغ"
                    ];
                    $temp = [];
                    $i = 0;
                    foreach ($records as $record) {
                        array_unshift($record, ++$i);
                        $record["total_price"] = number_format($record["total_price"]);
                        $temp[] = $record;
                    }
                    $records = $temp;
                    $sum = [
                        'total_price'
                    ];
                    break;
                case "by-factor":
                    $headers = [
                        "ردیف",
                        "تاریخ فاکتور",
                        "شماره فاکتور",
                        "محصول",
                        "تعداد",
                        "مبلغ"
                    ];
                    $temp = [];
                    $i = 0;
                    foreach ($records as $record) {
                        array_unshift($record, ++$i);
                        $record["total_price"] = number_format($record["total_price"]);
                        $temp[] = $record;
                    }
                    $records = $temp;
                    $sum = [
                        'total_price'
                    ];
                    break;
                case "by-date":
                    $headers = [
                        "ردیف",
                        "تاریخ",
                        "محصول",
                        "تعداد",
                        "مبلغ"
                    ];
                    $temp = [];
                    $i = 0;
                    foreach ($records as $record) {
                        array_unshift($record, ++$i);
                        $record["total_price"] = number_format($record["total_price"]);
                        $temp[] = $record;
                    }
                    $records = $temp;
                    $sum = [
                        'total_price'
                    ];
                    break;
                case "by-seller":
                    $headers = [
                        "ردیف",
                        "فروشنده",
                        "محصول",
                        "تعداد",
                        "مبلغ"
                    ];
                    $temp = [];
                    $i = 0;
                    foreach ($records as $record) {
                        array_unshift($record, ++$i);
                        $record["total_price"] = number_format($record["total_price"]);
                        $temp[] = $record;
                    }
                    $records = $temp;
                    $sum = [
                        'total_price'
                    ];
                    break;
                case "by-buyer":
                    $headers = [
                        "ردیف",
                        "خریدار",
                        "محصول",
                        "تعداد",
                        "مبلغ"
                    ];
                    $temp = [];
                    $i = 0;
                    foreach ($records as $record) {
                        array_unshift($record, ++$i);
                        $record["total_price"] = number_format($record["total_price"]);
                        $temp[] = $record;
                    }
                    $records = $temp;
                    $sum = [
                        'total_price'
                    ];
                    break;
                case "by-state":
                    $headers = [
                        "ردیف",
                        "استان",
                        "محصول",
                        "تعداد",
                        "مبلغ"
                    ];
                    $temp = [];
                    $i = 0;
                    foreach ($records as $record) {
                        array_unshift($record, ++$i);
                        $record["total_price"] = number_format($record["total_price"]);
                        $temp[] = $record;
                    }
                    $records = $temp;
                    $sum = [
                        'total_price'
                    ];
                    break;
                case "by-city":
                    $headers = [
                        "ردیف",
                        "شهر",
                        "محصول",
                        "تعداد",
                        "مبلغ"
                    ];
                    $temp = [];
                    $i = 0;
                    foreach ($records as $record) {
                        array_unshift($record, ++$i);
                        $record["total_price"] = number_format($record["total_price"]);
                        $temp[] = $record;
                    }
                    $records = $temp;
                    $sum = [
                        'total_price'
                    ];
                    break;
            }
        }
        $title = createPrintTitleFromFilters($array);
        if (isset($params['export-type'])) {
            if ($params['export-type'] == "print") {
                return view('print.print', compact('records', 'headers', 'title', 'sum'));
            } else if ($params['export-type'] == "excel") {
                $export = new Export\BladeExport($headers, $records, $title);
                return Excel::download($export, 'invoices.xlsx');
            }
        }
        return view('report.report_sale_products', compact('factors'));
    }

    public
    function reportSaleTotally(Request $request)
    {
        $params = $request->all();
        $params = array_filter($params, function ($value) {
            return !is_null($value) && $value !== '';
        });
        $array = [
            'fm_collection_id' => isset($params['fm_collection_id']) ? $request->get('fm_collection_id') : 0,
            'fm_self_collection_id' => isset($params['fm_self_collection_id']) ? $request->get('fm_self_collection_id') : 0,
            'fm_no' => isset($params['fm_no']) ? $request->get('fm_no') : 0,
            'fm_date_from' => isset($params['fm_date_from']) ? $request->get('fm_date_from') : '____/__/__',
            'fm_date_to' => isset($params['fm_date_to']) ? $request->get('fm_date_to') : '____/__/__',
            'fm_description' => isset($params['fm_description']) ? $request->get('fm_description') : '',
            'fm_porsant_user_id' => isset($params['fm_porsant_user_id']) ? $request->get('fm_porsant_user_id') : 0,
            'fm_creator_id' => isset($params['fm_creator_id']) ? $request->get('fm_creator_id') : 0,
            'cp_id' => isset($params['product_id']) ? $request->get('product_id') : 0,
            'city_id' => isset($params['city']) ? $request->get('city') : 0,
            'state_id' => isset($params['state']) ? $request->get('state') : 0,
            'report_type' => isset($params['report-type']) ? $params["report-type"] : "by-date"
        ];

        if (in_array(user('panel_type'), [40, 41, 42, 43])) {
            $array ["fm_collection_id"] = collection_id();
        } else if (in_array(user('panel_type'), [44])) {
            $array ["fm_self_collection_id"] = collection_id();
        }
        if (isset($params['report-type'])) {
            switch ($params['report-type']) {
                case "by-factor":
                    $records = call_sp('report_sale_factors_totally', null, $array);
                    $headers = [
                        "ردیف",
                        "شماره فاکتور",
                        "تاریخ",
                        "مبلغ فاکتور",
                        "تخفیف جشنواره ای",
                        "تخفیف روش پرداخت",
                       // "طلب از قبل",
                        "مبلغ دریافتی",
                        "فروشنده",
                        "خریدار",
                    ];
                    $temp = [];
                    $i = 0;
                    foreach ($records as $record) {
                        $record["fm_id"] = ++$i;
                        $record["fm_amount"] = number_format($record["fm_amount"]);
                        $record["fm_festival_discount"] = number_format($record["fm_festival_discount"]);
                        $record["fm_payment_discount"] = number_format($record["fm_payment_discount"]);
                        //$record["fm_talab_az_ghabl"] = number_format($record["fm_talab_az_ghabl"]);
                        $record["last_amount"] = number_format($record["last_amount"]);
                        $temp[] = $record;
                    }
                    $records = $temp;
                    $sum = [
                        'fm_amount',
                        'fm_festival_discount',
                        'fm_payment_discount',
                       // 'fm_talab_az_ghabl',
                        'last_amount',
                    ];
                    break;
                case "by-date":
                    $records = call_sp('report_sale_factors_totally', null, $array);
                    $headers = [
                        "ردیف",
                        "تاریخ",
                        "تعداد فاکتور",
                        "مبلغ فاکتور",
                        "تخفیف جشنواره ای",
                        "تخفیف روش پرداخت",
                       // "طلب از قبل",
                        "مبلغ دریافتی",
                    ];
                    $temp = [];
                    $i = 0;
                    foreach ($records as $record) {
                        array_unshift($record, ++$i);
                        $record["total_price"] = number_format($record["total_price"]);
                        $record["fm_festival_discount"] = number_format($record["fm_festival_discount"]);
                        $record["fm_payment_discount"] = number_format($record["fm_payment_discount"]);
                        //$record["fm_talab_az_ghabl"] = number_format($record["fm_talab_az_ghabl"]);
                        $record["last_amount"] = number_format($record["last_amount"]);
                        $temp[] = $record;
                    }
                    $records = $temp;
                    $sum = [
                        'cnt',
                        'total_price',
                        'fm_festival_discount',
                        'fm_payment_discount',
                        //'fm_talab_az_ghabl',
                        'last_amount'
                    ];
                    break;
                case "by-seller":
                    $records = call_sp('report_sale_factors_totally', null, $array);
                    $headers = [
                        "ردیف",
                        "فروشنده",
                        "تعداد فاکتور",
                        "مبلغ فاکتور",
                        "تخفیف جشنواره ای",
                        "تخفیف روش پرداخت",
                       // "طلب از قبل",
                        "مبلغ دریافتی",
                    ];
                    $temp = [];
                    $i = 0;
                    foreach ($records as $record) {
                        array_unshift($record, ++$i);
                        $record["total_price"] = number_format($record["total_price"]);
                        $record["fm_festival_discount"] = number_format($record["fm_festival_discount"]);
                        $record["fm_payment_discount"] = number_format($record["fm_payment_discount"]);
                        //$record["fm_talab_az_ghabl"] = number_format($record["fm_talab_az_ghabl"]);
                        $record["last_amount"] = number_format($record["last_amount"]);
                        $temp[] = $record;
                    }
                    $records = $temp;
                    $sum = [
                        'cnt',
                        'total_price',
                        'fm_festival_discount',
                        'fm_payment_discount',
                       // 'fm_talab_az_ghabl',
                        'last_amount'
                    ];
                    break;
                case "by-buyer":
                    $records = call_sp('report_sale_factors_totally', null, $array);
                    $headers = [
                        "ردیف",
                        "خریدار",
                        "تعداد فاکتور",
                        "مبلغ فاکتور",
                        "تخفیف جشنواره ای",
                        "تخفیف روش پرداخت",
                       // "طلب از قبل",
                        "مبلغ دریافتی",
                    ];
                    $temp = [];
                    $i = 0;
                    foreach ($records as $record) {
                        array_unshift($record, ++$i);
                        $record["total_price"] = number_format($record["total_price"]);
                        $record["fm_festival_discount"] = number_format($record["fm_festival_discount"]);
                        $record["fm_payment_discount"] = number_format($record["fm_payment_discount"]);
                       // $record["fm_talab_az_ghabl"] = number_format($record["fm_talab_az_ghabl"]);
                        $record["last_amount"] = number_format($record["last_amount"]);
                        $temp[] = $record;
                    }
                    $records = $temp;
                    $sum = [
                        'cnt',
                        'total_price',
                        'fm_festival_discount',
                        'fm_payment_discount',
                       // 'fm_talab_az_ghabl',
                        'last_amount'
                    ];
                    break;
                case "by-state":
                    $records = call_sp('report_sale_factors_totally', null, $array);
                    $headers = [
                        "ردیف",
                        "استان",
                        "تعداد فاکتور",
                        "مبلغ فاکتور",
                        "تخفیف جشنواره ای",
                        "تخفیف روش پرداخت",
                        //"طلب از قبل",
                        "مبلغ دریافتی",
                    ];
                    $temp = [];
                    $i = 0;
                    foreach ($records as $record) {
                        array_unshift($record, ++$i);
                        $record["total_price"] = number_format($record["total_price"]);
                        $record["fm_festival_discount"] = number_format($record["fm_festival_discount"]);
                        $record["fm_payment_discount"] = number_format($record["fm_payment_discount"]);
                        //$record["fm_talab_az_ghabl"] = number_format($record["fm_talab_az_ghabl"]);
                        $record["last_amount"] = number_format($record["last_amount"]);
                        $temp[] = $record;
                    }
                    $records = $temp;
                    $sum = [
                        'cnt',
                        'total_price',
                        'fm_festival_discount',
                        'fm_payment_discount',
                       // 'fm_talab_az_ghabl',
                        'last_amount'
                    ];
                    break;
                case "by-city":
                    $records = call_sp('report_sale_factors_totally', null, $array);
                    $headers = [
                        "ردیف",
                        "شهر",
                        "تعداد فاکتور",
                        "مبلغ فاکتور",
                        "تخفیف جشنواره ای",
                        "تخفیف روش پرداخت",
                        //"طلب از قبل",
                        "مبلغ دریافتی",
                    ];
                    $temp = [];
                    $i = 0;
                    foreach ($records as $record) {
                        array_unshift($record, ++$i);
                        $record["total_price"] = number_format($record["total_price"]);
                        $record["fm_festival_discount"] = number_format($record["fm_festival_discount"]);
                        $record["fm_payment_discount"] = number_format($record["fm_payment_discount"]);
                       // $record["fm_talab_az_ghabl"] = number_format($record["fm_talab_az_ghabl"]);
                        $record["last_amount"] = number_format($record["last_amount"]);
                        $temp[] = $record;
                    }
                    $records = $temp;
                    $sum = [
                        'cnt',
                        'total_price',
                        'fm_festival_discount',
                        'fm_payment_discount',
                       // 'fm_talab_az_ghabl',
                        'last_amount'
                    ];
                    break;
                case "by-agent":
                    $records = call_sp('report_sale_factors_totally', null, $array);
                    $headers = [
                        "ردیف",
                        "دامنه",
                        "تعداد فاکتور",
                        "مبلغ فاکتور",
                        "تخفیف جشنواره ای",
                        "تخفیف روش پرداخت",
                        //"طلب از قبل",
                        "مبلغ دریافتی",
                    ];
                    $temp = [];
                    $i = 0;
                    foreach ($records as $record) {
                        array_unshift($record, ++$i);
                        $record["total_price"] = number_format($record["total_price"]);
                        $record["fm_festival_discount"] = number_format($record["fm_festival_discount"]);
                        $record["fm_payment_discount"] = number_format($record["fm_payment_discount"]);
                        //$record["fm_talab_az_ghabl"] = number_format($record["fm_talab_az_ghabl"]);
                        $record["last_amount"] = number_format($record["last_amount"]);
                        $temp[] = $record;
                    }
                    $records = $temp;
                    $sum = [
                        'cnt',
                        'total_price',
                        'fm_festival_discount',
                        'fm_payment_discount',
                       // 'fm_talab_az_ghabl',
                        'last_amount'
                    ];
                    break;
            }


            $title = createPrintTitleFromFilters($array);
            if ($params['export-type'] == "print") {
                return view('print.print', compact('records', 'headers', 'title', 'sum'));
            } else if ($params['export-type'] == "excel") {
                $export = new Export\BladeExport($headers, $records, $title);
                return Excel::download($export, 'invoices.xlsx');
            }
        }
        return view('report.report_sale_totally', compact('factors'));
    }
}
