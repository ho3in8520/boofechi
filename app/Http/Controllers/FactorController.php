<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class FactorController extends Controller
{
    public function __construct()
    {
//        $this->middleware('permission:menu_company');
//        $this->middleware('permission:output-factors', ['only' => ['index']]);
//        $this->middleware('permission:input-factors', ['only' => ['input']]);
        $this->middleware('permission:delete_factor', ['only' => ['destroy']]);
        $this->middleware('permission:output-factors', ['only' => ['output']]);
        $this->middleware('permission:input-factor', ['only' => ['input']]);
        $this->middleware('permission:archive-factors', ['only' => ['archivedFactors']]);
    }

    public function dontRead(Request $request)
    {
        $factor = DB::table("factor_master")->where("fm_id", $request->get("id"))->where("fm_collection_id", collection_id())->first();
        if ($factor) {
            $sql = "insert into factor_routing(far_role_id,far_factor_id,far_created_at) select top 1 far_role_id,far_factor_id," . time() . " from factor_routing where far_factor_id = {$factor->fm_id} order by far_id desc";
            DB::insert($sql);
        }
        return json_encode(["status" => 100, "route" => asset("input-factors")]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function output(Request $request, $id = null)
    {
        $user_id = getUserIdForAddFactor();
        //if admin no need to check collection_id
        $factors = DB::table('factor_master')->select([
            'fm_date',
            'fm_no',
            'coll_name',
            'bas_value as payment_method',
            DB::raw('(case when fm_is_payed = 1 then dbo.get_factor_amount(fm_id,2) else dbo.get_factor_amount(fm_id,1) end) as fm_amount'),
            'fm_is_payed',
            'fm_id'
        ])
            ->leftJoin('collection_payment_methods', 'cpm_id', '=', 'fm_payment_method');

//            if (in_array(user("panel_type"), [43, 44]))
//                $factors->join('collections', 'fm_self_collection_id', 'coll_id');
//            else
        $factors->join('collections', 'fm_collection_id', 'coll_id');

        $factors->leftJoin('baseinfos as pm', 'pm.bas_id', '=', 'cpm_bas_id');
        if ($request->has('factor_date') && $request->get('factor_date') != '')
            $factors->where('fm_date', $request->get('factor_date'));
        if ($request->has('collection') && $request->get('collection') != '')
            $factors->where('coll_name', 'like', '%' . $request->get('collection') . '%');
        if ($request->has('factor_no') && $request->get('factor_no') != '')
            $factors->where('fm_no', $request->get('factor_no'));
        if ($request->has('factor_payment') && $request->get('factor_payment') != '') {
            if (in_array($request->get('factor_payment'), [2, 3])) {
                $arr_status_payment = [2 => 1, 3 => 0];
                $factors->where('fm_is_payed', $arr_status_payment[$request->get('factor_payment')]);
            }
        }

        //if user can manage all factors in collection
        if (!can('manage-all-factors'))
            $factors->where('fm_creator_id', $user_id);//show only own factors
        else
            $factors->whereIn('fm_creator_id', getAllUserByCollectionId());//show all factors from this collection

        //marjooeeha ro az form sabte faktor mikhad bezane
//        if ($request->ajax()) {
//            $factors->where('fm_is_payed', 1)->where('fm_status', 2);
//        }

        $factors = $factors
            ->where('fm_mode', 1)
            ->latest("fm_id")
            ->paginate(20);
        $mode = 1;
        return showData(view('factor.output', compact('factors', 'mode')));
    }

    public function archivedFactors(Request $request, $id = null)
    {
        //if admin no need to check collection_id
        $factors = DB::table('factor_master')->select([
            'fm_date',
            'fm_no',
            DB::raw('dbo.get_factor_amount(fm_id,2) as fm_amount'),
            'fm_is_payed',
            'fm_id',
            "fm_status",
            "coll_name",
            "username"
        ])
            ->join('users', 'fm_end_work_user_id', 'user_id')
            ->join('collections', 'fm_self_collection_id', 'coll_id');
        if ($request->has('factor_date') && $request->get('factor_date') != '')
            $factors->where('fm_date', $request->get('factor_date'));
        if ($request->has('collection') && $request->get('collection') != '')
            $factors->where('coll_name', 'like', '%' . $request->get('collection') . '%');
        if ($request->has('agent') && $request->get('agent') != '')
            $factors->where('users.username', 'like', '%' . $request->get('agent') . '%');
        if ($request->has('factor_no') && $request->get('factor_no') != '')
            $factors->where('fm_no', $request->get('factor_no'));
        if ($request->has('factor_status') && $request->get('factor_status') != '') {
            if (in_array($request->get('factor_status'), array_keys(config('first_config.factor-statuses'))))
                $factors->where('fm_status', $request->get('factor_status'));
        }
        //$factors->whereRaw("(select top 1 far_is_viewed from factor_routing where far_factor_id = fm_id and far_user_viewed = " . user("user_id") . ") = 1");

        //if user can manage all factors in collection
//        if (!can('manage-all-factors'))
//            $factors->where('fm_creator_id', user('user_id'));//show only own factors
//        else
//            $factors->whereIn('fm_creator_id', getAllUserByCollectionId());//show all factors from this collection

        //marjooeeha ro az form sabte faktor mikhad bezane
//        if ($request->ajax()) {
//            $factors->where('fm_is_payed', 1)->where('fm_status', 2);
//        }

        if (in_array(user("panel_type"), [43, 44])) {
            $factors->where("fm_self_collection_id", collection_id());
        } else {
            $factors->where("fm_collection_id", collection_id());
        }

        $factors = $factors
            ->where('fm_mode', 1)
            ->whereIn('fm_status', [3, 2])//accepted or returned factors
            ->orderBy("fm_id", "desc")
            ->paginate(20);
        $mode = 1;
        return showData(view('factor.archived', compact('factors', 'mode')));
    }

    public function popupFactorsForReturnProducts(Request $request, $id)
    {
        //if admin no need to check collection_id
        $factors = DB::table('factor_master')->select([
            'fm_date',
            'fm_no',
            'coll_name',
            'fm_amount',
            'bas_value as payment_method',
            'fm_is_payed',
            'fm_id'
        ])
            ->join('collection_payment_methods', 'cpm_id', '=', 'fm_payment_method')
            ->join('collections', 'fm_collection_id', 'coll_id')
            ->join('baseinfos as pm', 'pm.bas_id', '=', 'cpm_bas_id');
        if ($request->has('factor_date') && $request->get('factor_date') != '')
            $factors->where('fm_date', $request->get('factor_date'));
        if ($request->has('factor_no') && $request->get('factor_no') != '')
            $factors->where('fm_no', $request->get('factor_no'));
        if ($request->has('factor_payment') && $request->get('factor_payment') != '')
            $factors->where('fm_is_payed', $request->get('factor_payment'));

        //if user can manage all factors in collection
        $factors->where('fm_creator_id', getUserIdForAddFactor());//show only own factors

        $factors = $factors
            ->where('fm_collection_id', $id)
            ->where('fm_mode', 1)
            ->where('fm_is_payed', 1)
            ->where('fm_status', 2)
            ->paginate(20);
        $mode = 2;
        return showData(view('factor.output', compact('factors', 'mode')));
    }

    public function input(Request $request, $id = null)
    {
        $result = DB::table('factor_master')
            ->select([
                'fm_id',
                'fm_date',
                'fm_no',
                'fm_status',
                DB::raw('(case when fm_is_payed = 1 then dbo.get_factor_amount(fm_id,2) else dbo.get_factor_amount(fm_id,1) end) as fm_amount'),
                'prsn_phone1',
                'coll_name',
                'bas_value as payment_method',
                'state.c_name as state',
                'city.c_name as city',
                DB::raw("(select top 1 far_is_viewed from factor_routing where far_factor_id = fm_id order by far_id desc) as far_is_viewed")
            ])
            ->join('collections', 'coll_id', '=', 'fm_self_collection_id')
            ->join('collection_payment_methods', 'cpm_id', '=', 'fm_payment_method')
            ->join('baseinfos as pm', 'pm.bas_id', '=', 'cpm_bas_id')
            ->join('persons', 'prsn_user_id', '=', 'fm_creator_id')
            ->join('cities as state', 'state.c_id', '=', 'prsn_state_id')
            ->join('cities as city', 'city.c_id', '=', 'prsn_city_id');
        $is_owner = DB::table('persons')->select('prsn_user_id')
            ->join('collection_persons', 'cp_prsn_id', 'prsn_id')
            ->where('cp_coll_id', collection_id())->where('prsn_is_owner', true)->first();
        if ($is_owner->prsn_user_id != user('user_id')) {
            $result->whereRaw("
                    fm_id in (
                        select
                            fm_id
                        from [factor_master]
                        join [persons] on [prsn_user_id] = [fm_creator_id]
                        join [factor_detail] on [fd_master_id] = [fm_id]
                        join [collection_products] on [collection_products].[cp_id] = [fd_product_id]
                        join [products] on [prod_id] = [cp_product_id]
                        where prod_category_id in (select cc_category_id from collection_categories where cc_user_id = " . user('user_id') . " group by cc_category_id)
                        and persons.prsn_city_id in (select as_city_id from area_support where as_user_id = " . user('user_id') . " group by as_city_id)
                        and [fm_collection_id] = " . collection_id() . "
                        and fm_status not in(2,3) and fm_is_payed = 1 and fm_mode = 1
                        group by fm_id
                    )
                    and fm_assigned_role_id in (select ur_rol_id from user_roles where ur_user_id = " . user('user_id') . ")
            ");
        } else {
            $result->where('fm_collection_id', collection_id());
        }/*->orWhereRaw("
                    fm_id in (
                        select
                            fm_id
                        from [factor_master]
                        where [fm_collection_id] = " . collection_id() . "
                        --and ".user('panel_type')." in (41,42,43)
                        group by fm_id
                    )
            ")*/;

        if ($request->has('factor_date') && $request->get('factor_date') != '') {
            $result->where('fm_date', $request->get('factor_date'));
        }

        if ($request->has('collection') && $request->get('collection') != '') {
            $result->where('coll_name', 'like', '%' . $request->get('collection') . '%');
        }

        if ($request->has('factor_no') && $request->get('factor_no') != '') {
            $result->where('fm_no', $request->get('factor_no'));
        }

        $factors = $result
            ->where('fm_mode', 1)
            ->where('fm_is_payed', 1)
            ->whereNotIn('fm_status', [2, 3])
            ->latest('fm_id')
            ->paginate(20);
        $factor_statuses = config('first_config.factor-statuses');
        return showData(view('factor.input', compact('factors', 'factor_statuses')));
    }

    public function operationViewInputFactor(Request $request, $id)
    {
        $sql = "select cp_id,prod_name,fd_id,fd_count,bas_value,bas_id,fd_ooe,fd_ooe_id,fm_status,fm_id,fm_created_at,fm_no,fm_amount,fm_talab_az_ghabl,fm_description
                ,fd_price_buy,fd_price_consumer,fd_arzesh_afzoodeh,fm_self_collection_id,fm_collection_id,fm_is_payed,fm_payment_method_percent,
                (select top 1 ur_path from upload_route where ur_collection_id = fm_self_collection_id and ur_table_name = 'logo' order by ur_id desc) as logo
                ,coll_name,coll_address,coll_id,fd_discount,fm_calculated,fm_festival_discount from factor_detail
                left join collection_products on cp_id = fd_product_id
                left join baseinfos on bas_id = cp_measurement_unit_id
                left join factor_master on fd_master_id = fm_id
                left join collections on coll_id = fm_self_collection_id
                left join products on cp_product_id = prod_id
                where fd_master_id = ?";
        if (in_array(user("panel_type"), [44]))
            $sql .= " and fm_self_collection_id = ? ";
        else
            $sql .= " and fm_collection_id = ? ";
        $sql .= " and fm_mode = 1
                and fm_is_payed = 1
                order by cp_id,fd_id,fd_ooe_id";
        $result = json_decode(json_encode(DB::select($sql, [$id, collection_id()])), true);
        if (!$result)
            abort(404);

        //check if already viewed redirect to input-factors
        $already_viewed = DB::table('factor_routing')->where('far_factor_id', $id)
            ->join('factor_master', 'fm_id', '=', 'far_factor_id')
            ->join('user_roles', function ($join) {
                $join->on("ur_rol_id", "=", "far_role_id");
//                $join->where("ur_user_id", user('user_id'));
                return $join->whereRaw("far_id = (select max(far_id) from factor_routing where far_factor_id = fm_id)");
            })->select([
                'far_id',
                'far_is_viewed',
                'far_user_viewed'
            ])->latest('far_id')->first();

        if (isset($already_viewed->far_is_viewed) && !$already_viewed->far_is_viewed) {
            DB::table('factor_routing')->where('far_id', $already_viewed->far_id)
                ->update([
                    'far_is_viewed' => 1,
                    'far_user_viewed' => user('user_id'),
                    'far_viewed_at' => time()
                ]);
            $is_viewed_user_id = user('user_id');
        } else {
//            if (!isset($already_viewed->far_user_viewed) || $already_viewed->far_user_viewed != user('user_id'))
//                return \redirect()->route('input-factors');
            $is_viewed_user_id = $already_viewed->far_user_viewed;
        }

        $number_factor = $result[0]['fm_no'];

        if ($request->isMethod('post')) {
            $request->validate([
                'operation' => [function ($attr, $value, $fail) {
                    if ($value == "") {
                        $fail("فیلد عملیات الزامی است");
                    }
//                    elseif ($value == 1)
//                        $fail("فیلد ارسال به الزامی است");
                }],
            ]);
            $user_id = DB::table('collections')
                ->where('coll_id', $result[0]['coll_id'])
                ->leftJoin('collection_persons', 'cp_coll_id', 'coll_id')
                ->leftJoin('persons', 'prsn_id', 'cp_prsn_id')
                ->first()->prsn_user_id;
            $array = [];
            if (request()->ret_fd_id)
                foreach (request()->ret_fd_id as $key => $value) {
                    if ($value && request()->ret_count[$key] && request()->ret_reason[$key]) {
                        array_push($array, [
                            'PR' => $value,
                            'CN' => request()->ret_count[$key],
                            'RS' => request()->ret_reason[$key]
                        ]);
                    }
                }
            $result = call_sp('factor_change_status', null, [
                'fm_id' => $id,
                'created_at' => time(),
                'miladi_date' => date('Y/m/d H:i:s'),
                'return_reasons' => createRootData($array),
                'status' => $request->get('operation'),
                'user_id' => user('user_id'),
                'shamsi_date' => jdate()->format('Y/m/d'),
                'role_id' => ($request->has('send_to') && $request->get('send_to') != '') ? $request->get('send_to') : null
            ]);
            if ($result) {
                if ($result[0]['result'] == -5)
                    return json_encode(['status' => 500, 'msg' => 'مبلغ مرجوعی بیشتر از مبلغ فاکتور می باشد!']);
                else if ($request->operation == 3 && $result[0]['result'] == 0) {
                    sendMessage(generateMessage("sms_cancel_factor", ["%number%" => $number_factor, "%date%" => jdate_from_gregorian(date('Y/m/d'), 'Y/m/d'), "%company%" => findUser(user('user_id'), 'collection', 'name')]), [$user_id]);
                    return json_encode(['status' => 100, 'msg' => 'عملیات باموفقیت انجام شد!', 'url' => asset('input-factors')]);
                } else if ($result[0]['result'] == 0)
                    return json_encode(['status' => 100, 'msg' => 'عملیات باموفقیت انجام شد!', 'url' => asset('input-factors')]);
                else
                    return json_encode(['status' => 500, 'msg' => 'خطا در انجام عملیات، مجددا تلاش فرمائید!']);
            }
            return json_encode(['status' => 500, 'msg' => 'عملیات باموفقیت انجام شد!']);
        }

        $person_collection = DB::table('persons')
            ->leftJoin('collection_persons', 'cp_prsn_id', '=', 'prsn_id')
            ->where('cp_coll_id', $result[array_keys($result)[0]]['fm_self_collection_id'])
            ->first();

        $sql = "select cp_id,prod_name,fr_id,fr_type,
                 (case when fr_is_accepted = 1 then fr_accepted_count else fr_count end) as fr_count
                 ,fr_is_accepted
                ,mu.bas_value as measurement_unit,rs.bas_value as reason
                ,fd_price_buy,fd_price_consumer,fd_arzesh_afzoodeh
                fd_discount
                from factor_returns
                left join factor_detail on fd_id = fr_fd_id
                left join collection_products on cp_id = fd_product_id
                left join baseinfos mu on mu.bas_id = cp_measurement_unit_id
                left join collection_return_reasons on crr_id = fr_reason_id
                left join baseinfos rs on rs.bas_id = crr_bas_id
                left join products on cp_product_id = prod_id
                where fr_master_id = ?";
        $factor_returns = json_decode(json_encode(DB::select($sql, [$id])), true);
        //dd($factor_returns);
        //$talab = $result[array_keys($result)[0]]['fm_talab_az_ghabl'];
        $factor_actions = config('first_config.factor-statuses');
        $collection_roles = json_decode(json_encode(DB::table('roles')->where('rol_creator', getUserByCollectionId())->get()), true);

        $return_reasons = returnReasons(collection_id());
        $tax = setting('tax');
        return view('factor.view-input', compact('result', 'id', 'person_collection', 'factor_returns', 'talab', 'factor_actions', 'collection_roles', 'return_reasons', 'tax', 'is_viewed_user_id'));
    }

    public function updateAmount(Request $request)
    {
        if (is_numeric($request->amount)){
            if ($request->basId == 34) {
                $result = call_sp('updateAmountOfFactorDetailProduct', null, [
                    'fd_id' => $request->id,
                    'fd_amount' => $request->amount,
                ]);
                if ($result[0]['result'] == 1)
                    return json_encode(['status' => 100, 'msg' => 'با موفقیت انجام شد!']);
                return json_encode(['status' => 500, 'msg' => 'خطا در ثبت، مجددا تلاش فرمائید!']);
            }else
                return json_encode(['status' => 500, 'msg' => 'خطا در ثبت!']);
        }else
            return json_encode(['status' => 500, 'msg' => 'خطا در ثبت! مقدار ورودی باید عدد اعشار باشد']);
    }


    public function operationViewOutputFactor($id)
    {
        $array_params = [];
        $sql = "select cp_id,prod_name,fd_id,fd_count,bas_value,fd_ooe,fd_ooe_id,fm_status,fm_id,fm_created_at,fm_no,fm_amount,fm_talab_az_ghabl,fm_description
                ,fd_price_buy,fd_price_consumer,fd_arzesh_afzoodeh,fm_self_collection_id,fm_collection_id,fm_is_payed,fm_payment_method_percent,
                (select top 1 ur_path from upload_route where ur_collection_id = fm_collection_id and ur_table_name = 'logo' order by ur_id desc) as logo
                ,coll_name,coll_address,fd_discount,fm_calculated,fm_festival_discount from factor_detail
                left join collection_products on cp_id = fd_product_id
                left join baseinfos on bas_id = cp_measurement_unit_id
                left join factor_master on fd_master_id = fm_id
                left join collections on coll_id = fm_collection_id
                left join products on cp_product_id = prod_id
                where fd_master_id = ? ";

        array_push($array_params, $id);
        if (user("panel_type") != 39) //not is admin
        {
            array_push($array_params, getCollectionForAddFactor(getUserIdForAddFactor()));
            $sql .= " and fm_self_collection_id = ? ";
        }
        $sql .= " and fm_mode = 1
                order by cp_id,fd_id,fd_ooe_id";
        $result = json_decode(json_encode(DB::select($sql, $array_params)), true);
        if (!$result)
            abort(404);

        $person_collection = DB::table('persons')
            ->leftJoin('collection_persons', 'cp_prsn_id', '=', 'prsn_id')
            ->where('cp_coll_id', $result[array_keys($result)[0]]['fm_collection_id'])
            ->first();

        $sql = "select cp_id,prod_name,fr_id,fr_type,
                 (case when fr_is_accepted = 1 then fr_accepted_count else fr_count end) as fr_count
                 ,fr_is_accepted
                ,mu.bas_value as measurement_unit,rs.bas_value as reason
                ,fd_price_buy,fd_price_consumer,fd_arzesh_afzoodeh
                fd_discount
                from factor_returns
                left join factor_detail on fd_id = fr_fd_id
                left join collection_products on cp_id = fd_product_id
                left join baseinfos mu on mu.bas_id = cp_measurement_unit_id
                left join collection_return_reasons on crr_id = fr_reason_id
                left join baseinfos rs on rs.bas_id = crr_bas_id
                left join products on cp_product_id = prod_id
                where fr_master_id = ?";
        $factor_returns = json_decode(json_encode(DB::select($sql, [$id])), true);
        //dd($factor_returns);
        //$talab = $result[array_keys($result)[0]]['fm_talab_az_ghabl'];

        $tax = setting('tax');
        return view('factor.view-output', compact('result', 'id', 'person_collection', 'factor_returns', 'talab', 'tax'));
    }

    public function factorDetail($id)
    {
        $details = DB::table('factor_detail')
            ->select([
                'prod_name',
                'fm_no',
                DB::raw('((100-fd_discount) * fd_price_buy)/100 as fd_price_buy'),
                'fd_count',
                'fd_id',
                'bas_value',
                'cp_count_per',
            ])
            ->leftJoin('factor_returns', 'fd_id', '=', 'fr_fd_id')
            ->leftJoin('factor_master', 'fd_master_id', '=', 'fm_id')
            ->leftJoin('collection_products', 'fd_product_id', '=', 'cp_id')
            ->leftJoin("baseinfos", "bas_id", "cp_measurement_unit_id")
            ->leftJoin('products', 'prod_id', '=', 'cp_product_id')
            ->where('fd_ooe', false)
            ->whereRaw('(fr_is_accepted is null or fr_is_accepted = 0)')//baraye inke agar gablan marjoo shode dige nayare to list
            ->where('fd_master_id', $id);
        if (!user()->hasRole('administrator')) {
//            $details->where('fm_self_collection_id', collection_id());
            $user_id = getUserIdForAddFactor();
            $details->where('fm_creator_id', $user_id);
        }
        $details = $details->groupBy("prod_name", "fm_no", "fd_count", "fd_id", "fd_discount", "fd_price_buy", "bas_value", "cp_count_per")
            ->get()->toArray();
        return showData(view('factor.detail', compact('details')), [
            'factor_number' => @$details[0]->fm_no
        ]);
    }

    public function acceptReturnItem(Request $request)
    {
        if ($request->has('id')) {
            if (DB::table('factor_returns')
                ->where('fr_id', $request->get('id'))->update([
                    'fr_accepted_count' => $request->get('count'),
                    'fr_is_accepted' => 1
                ])
            ) {
                return json_encode(['status' => 100, 'msg' => 'با موفقیت انجام شد!']);
            }
            return json_encode(['status' => 500, 'msg' => 'با موفقیت انجام شد!']);
        }
    }

    public function deacceptReturnItem(Request $request)
    {
        if ($request->has('id')) {
            if (DB::table('factor_returns')
                ->where('fr_id', $request->get('id'))->update([
                    'fr_accepted_count' => 0,
                    'fr_is_accepted' => 0
                ])
            ) {
                return json_encode(['status' => 100, 'msg' => 'با موفقیت انجام شد!']);
            }
            return json_encode(['status' => 500, 'msg' => 'با موفقیت انجام شد!']);
        }
    }

    public function paymentFactor(Request $request, $id)
    {
        $user_id = getUserIdForAddFactor();
        $factor = DB::table('factor_master')
            ->select([
                'factor_master.*',
                DB::raw('dbo.get_factor_amount(fm_id,1) as fm_amount'),
                DB::raw('(select top 1 cd_amount from collection_debt where cd_source_collection_id = fm_collection_id and cd_destination_collection_id = fm_self_collection_id) as cd_amount'),
                //DB::raw('fm_amount - floor(dbo.calcPercent(fm_amount,fm_festival_discount))')
            ])
            ->where('fm_id', $id)
            ->where('fm_self_collection_id', getCollectionForAddFactor($user_id))
            ->where('fm_mode', 1)
            ->first();
        if (!$factor || $factor->fm_is_payed == true)
            abort(404);

        if (request()->isMethod("post")) {
            //validate inputs
            $array = [
                'selected_payment_method' => [
                    'bail',
                    function ($attribute, $value, $fail) {
                        if (!request()->has('payment_type')) {
                            $fail('لطفا یک روش پرداخت انتخاب کنید.');
                        }
                    },
                    //check is payment_method valid?????
                    function ($attribute, $value, $fail) use ($factor) {
                        $result = DB::table('collection_payment_methods')
                            ->where('cpm_id', request('payment_type'))
                            ->where('cpm_collection_id', $factor->fm_collection_id)
                            ->where('cpm_status', 1)
                            ->first();
                        if (!$result)
                            $fail('روش پرداخت انتخاب شده نامعتبر است!');
                    },
                ],
                'online_gate' => function ($attribute, $value, $fail) use ($factor) {
                    $result = DB::table('collection_payment_methods')
                        ->leftJoin('baseinfos', 'cpm_bas_id', '=', 'bas_id')
                        ->where('cpm_id', request('payment_type'))
                        ->where('cpm_collection_id', $factor->fm_collection_id)
                        ->where('cpm_status', 1)
                        ->where('bas_id', 50)
                        ->first();
                    if ($result)
                        if (!request()->has('online_gate') || request()->get('online_gate') == 0)
                            $fail('درگاه بانکی را انتخاب کنید!');
                },
            ];
            $request->validate($array);
            $result = call_sp('factor_payment', null, [
                'fm_id' => $id,
                'mode' => 1,
                'payment_method' => request('payment_type')
            ]);
            if ($result) {
                if ($result[0]['method'] == 50) {//online
                    //get online discount_percent
                    $dis_percent = $payment_methods = DB::table('collection_payment_methods')
                        ->select([
                            'cpm_percent',
                        ])
                        ->leftJoin('baseinfos', 'cpm_bas_id', '=', 'bas_id')
                        ->where('cpm_collection_id', $factor->fm_collection_id)
                        ->where('bas_id', 50)->first()->cpm_percent;

                    $gateway = paymentClass(request('online_gate'));
                    //$gateway->setCallback(url('/path/to/callback/route'));
                    $gateway
                        ->price($factor->fm_amount - floor(calcPercent($factor->fm_amount, $dis_percent)))
                        ->ready();
                    if (DB::table('gateway_transact')->where("transact_id", $gateway->transactionId())->update([
                        "transact_type" => \App\Gateway\Enum::TRANSACTTION_COLLECTION_FACTOR,
                        "transact_fk_id" => $factor->fm_id, //id factor_or_user
                        "transact_refId" => $gateway->refId()
                    ])
                    ) {
                        return json_encode(['status' => 100, 'url' => $gateway->redirect(), 'msg' => 'در حال انتقال به بانک...']);
                    } else {
                        return json_encode(['status' => 500, 'msg' => 'خطا در اتصال به بانک، لطفا مجددا تلاش فرمائید!']);
                    }
                    exit();
                } else {
                    $res = call_sp('factor_payment', null, [
                        'fm_id' => $id,
                        'mode' => 2,
                        'shamsi_date' => jdate()->format("Y/m/d"),
                        'created_at' => date('Y/m/d H:i:s'),
                        'timestamp' => time(),
                        'user_id' => $user_id,
                        'payment_method' => request('payment_type')
                    ]);
                    if ($res) {
                        //mojoodi product kame
                        if (isset($res[0]['prod_name'])) {
                            return json_encode(['status' => 500, 'pin' => true, 'msg' => 'عدم موجودی انبار برای محصول ' . implode(' و ', array_column($res, 'prod_name')) . '<br>' . 'لطفا فاکتور خود را ویرایش کنید.']);
                        } //mojoodi kife poole user kame
                        else if ($res[0]['result'] == -5) {
                            return json_encode(['status' => 500, 'msg' => 'موجودی کیف پول شما برای پرداخت فاکتور جاری کافی نیست!']);
                        } else /*if ($res[0]['result'] == 0)*/ {
                            $company = DB::table('collections')->select('coll_name')->join('factor_master', 'coll_id', 'fm_collection_id')->where('fm_id', $id)->first();
                            sendMessage(generateMessage("sms_factorBuy", ["%number%" => $res[0]['result'], "%company%" => $company->coll_name]), [$user_id]);
                            return json_encode(['status' => 100, 'msg' => 'فاکتور شما با موفقیت پرداخت شد!', 'url' => asset('output-factors')]);
                        }
                    }
                }
            }
            return json_encode(['status' => 500, 'msg' => 'خطا در پرداخت فاکتور، مجددا تلاش فرمائید!']);
        }

        $payment_methods = DB::table('collection_payment_methods')
            ->select([
                'cpm_id',
                'bas_value',
                'bas_id',
                'cpm_percent',
                'cpm_description'
            ])
            ->leftJoin('baseinfos', 'cpm_bas_id', '=', 'bas_id')
            ->where('bas_id', '!=', 50)
            ->where('cpm_collection_id', $factor->fm_collection_id);

        $collection_online_gates = DB::table("collection_online_gates")
            ->select([
                "og_title",
                "og_id",
            ])
            ->join("online_gates", 'og_id', '=', 'cog_gate_id')
            ->where('og_status', 1)
            ->where('cog_collection_id', $factor->fm_collection_id)
            ->get();

        if (user('panel_type') == 45)//is visitor
        {
            if (session()->has('purchasing_user_id') && session()->get('purchasing_user_id') == user("otp")) {
                $payment_methods->whereNotIn('bas_id', [50, 53]);
            }
        }
        $payment_methods = $payment_methods->where('cpm_status', 1)
            ->orderBy('cpm_percent', 'desc')
            ->get()->toArray();
        return view('factor.payment', compact('payment_methods', 'factor', 'id', 'collection_online_gates'));
    }

    public function openFactorForEdit($id)
    {
        $res = call_sp('openFactorForEdit', false, [
            'fm_id' => $id,
            'self_collection_id' => getCollectionForAddFactor(getUserIdForAddFactor())
        ]);
        if (!isset($res) || $res[0]['result'] == -2)
            abort(404);
        else if ((!isset($res) || $res[0]['result'] == -1))
            abort(500);

        return redirect(asset('basket-item-details') . '/' . $id);
    }

    public function printFactor($id)
    {
        $sql = "select cp_id,prod_name,fd_id,fd_count,bas_value,fd_ooe,fd_ooe_id,fm_status,fm_id,fm_created_at,fm_no,fm_amount,fm_talab_az_ghabl,fm_description
                ,fd_price_buy,fd_price_consumer,fd_arzesh_afzoodeh,fm_self_collection_id,fm_collection_id,fm_payment_method_percent,fm_is_payed
                ,fd_discount,fm_calculated,fm_festival_discount from factor_detail
                left join collection_products on cp_id = fd_product_id
                left join baseinfos on bas_id = cp_measurement_unit_id
                left join factor_master on fd_master_id = fm_id
                left join collections on coll_id = fm_collection_id
                left join products on cp_product_id = prod_id
                where fd_master_id = ?
                and fm_collection_id = ?
                and fm_mode = 1
                order by cp_id,fd_id,fd_ooe_id";

        $result = json_decode(json_encode(DB::select($sql, [$id, getCollectionForAddFactor(getUserIdForAddFactor())])), true);
        if (!$result)
            abort(404);

        $seller_details = DB::table('persons')
            ->select([
                "persons.*",
                "collections.*",
                DB::raw("(select top 1 ur_path from upload_route where ur_collection_id = coll_id and ur_table_name = 'logo' order by ur_id desc) as logo")
            ])
            ->leftJoin('collection_persons', 'cp_prsn_id', '=', 'prsn_id')
            ->leftJoin('collections', 'cp_coll_id', '=', 'coll_id')
            ->where('cp_coll_id', $result[array_keys($result)[0]]['fm_collection_id'])
            ->first();

        $buyer_details = DB::table('persons')
            ->select([
                "persons.*",
                "collections.*"
            ])
            ->leftJoin('collection_persons', 'cp_prsn_id', '=', 'prsn_id')
            ->leftJoin('collections', 'cp_coll_id', '=', 'coll_id')
            ->where('cp_coll_id', $result[array_keys($result)[0]]['fm_self_collection_id'])
            ->first();

        $sql = "select cp_id,prod_name,fr_id,fr_type,
                 (case when fr_is_accepted = 1 then fr_accepted_count else fr_count end) as fr_count
                 ,fr_is_accepted
                ,mu.bas_value as measurement_unit,rs.bas_value as reason
                ,fd_price_buy,fd_price_consumer,fd_arzesh_afzoodeh
                fd_discount
                from factor_returns
                left join factor_detail on fd_id = fr_fd_id
                left join collection_products on cp_id = fd_product_id
                left join baseinfos mu on mu.bas_id = cp_measurement_unit_id
                left join collection_return_reasons on crr_id = fr_reason_id
                left join baseinfos rs on rs.bas_id = crr_bas_id
                left join products on cp_product_id = prod_id
                where fr_master_id = ?";
        $factor_returns = json_decode(json_encode(DB::select($sql, [$id])), true);
        //dd($factor_returns);
        //$talab = $result[array_keys($result)[0]]['fm_talab_az_ghabl'];


        $i = $kham = $sum_one = $sum_total = $sum_discount = $sum_total_menhaye_discount = $sum_maliat = $sum_total_bealave_maliat = 0;
        $products = '<table style="text-align: center !important" class="table table-striped table-inverse table-bordered table-hover">
                <thead>
                <th>ردیف</th>
                <th>محصول</th>
                <th>مقدار</th>
                <th>واحد</th>
                <th>قیمت واحد</th>
                <th>قیمت کل</th>
                <th>تخفیف</th>
                <th>مبلغ کل پس از کسر تخفیف</th>
                <th>مالیات بر ارزش افزوده</th>
                <th>قیمت کل + مالیات بر ارزش افزوده</th>
                </thead><tbody>';
        if ($result) {
            foreach ($result as $item) {
                $one = $total = $discount = $total_menhaye_discount = $maliat = $total_bealave_maliat = 0;
                $i++;

                if ($item['fd_ooe'] == 0) {
                    $sum_one += $one = $item['fd_price_consumer'];
                    $sum_total += $total = $one * $item['fd_count'];
                    $sum_discount += $discount = floor(calcPercent($total, $item['fd_discount']));
                    $sum_total_menhaye_discount += $total_menhaye_discount = $total - $discount;
                    $sum_maliat += $maliat = ($item['fd_arzesh_afzoodeh'] == 1) ? floor(calcPercent($total_menhaye_discount, setting("tax"))) : 0;
                    $sum_total_bealave_maliat += $total_bealave_maliat = $total_menhaye_discount + $maliat;
                } else
                    $one = "اشانتیون";
                $factor_discount = withoutZeros(floor(calcPercent($sum_total_bealave_maliat, $result[array_keys($result)[0]]['fm_festival_discount'])));
                $financial_unit = setting("financial_unit");
                $products .= '<tr>
                    <td>' . $i . '</td>';
                if ($item['fd_ooe'] == 0)
                    $txt = $item['prod_name'];
                else if ($item['fd_ooe_id'] == -1)
                    $txt = "اشانتیون-فاکتور";
                else
                    $txt = "اشانتیون-محصول";

                $products .= '<td><b>' . $txt . '</b></td>
                    <td style="width: 200px">
                        <div class="input-group prod-counter col-sm-offset-2">
                            <lable class="form-control prod-count input-sm"
                                   style="width: 20px">' . $item['fd_count'] . '</lable>
                        </div>
                    </td>
                    <td>' . $item['bas_value'] . '</td>
                    <td>
                        ' . (is_numeric($one) ? number_format($one) : $one) . '
                    </td>
                    <td>' . number_format($total) . '</td>
                    <td>' . number_format($discount) . '</td>
                    <td>' . number_format($total_menhaye_discount) . '</td>
                    <td>' . number_format($maliat) . '</td>
                    <td>' . number_format($total_bealave_maliat) . '</td>
                </tr>
                ';
            }
            $products .= '<tr>
                            <td colspan="4">مجموع</td>
                            <td>' . number_format($sum_one) . '</td>
                            <td>' . number_format($sum_total) . '</td>
                            <td>' . number_format($sum_discount) . '</td>
                            <td>' . number_format($sum_total_menhaye_discount) . '</td>
                            <td>' . number_format($sum_maliat) . '</td>
                            <td>' . number_format($sum_total_bealave_maliat) . '</td>
                        </tr>';
            $products .= "</tbody></table>";
        }

        $returns = '<table class="table table-bordered table-marjooee" style="text-align: center">';
        if (count($factor_returns) > 0) {
            if (in_array(0, array_column($factor_returns, 'fr_type'))) {
                $returns .= '<tr>
                <th colspan = "5" ><h4 class="pull-right"> کالاهای مرجوعی از طرف خریدار </h4>
                </th>
            </tr>
            <tr>
                <th> محصول</th>
                <th> تعداد</th>
                <th> قیمت</th>
                <th> علت</th>
            </tr>';
            }
            foreach ($factor_returns as $return) {
                if ($return['fr_type'] == 0) {
                    $returns .= '<tr data-id="' . $return['fr_id'] . '">
                <td class="selectProduct">
                    ' . $return['prod_name'] . '
                </td>
                <td>
                    <input type="text" class="form-control" name="ret_count"
                           value="' . withoutZeros($return['fr_count']) . '">
                </td>
                <td>
                    <label class="form-control price_buy"
                           style="min-width: 150px">' . number_format(withoutZeros($return['fd_price_buy'])) . '</label>
                </td>
                <td>
                    <lable class="frm-control">' . $return['reason'] . '</lable>
                </td>
            </tr>';
                }
            }
        }
        $returns .= '</table>';

        if ($result[array_keys($result)[0]]['fm_is_payed'] == 1) {
            $kham = floor(calcPercent(($sum_total_bealave_maliat - $factor_discount), $result[array_keys($result)[0]]['fm_payment_method_percent']));
        }

        $html = setting('print_html', $result[array_keys($result)[0]]['fm_collection_id']);

        $keys = [
            "%seller_corporate%",
            "%seller_tel%",
            "%seller_logo%",
            "%seller_address%",
            "%buyer_corporate%",
            "%buyer_tel%",
            "%buyer_address%",
            "%factor_number%",
            "%time%",
            "%description%",
            "%products%",
            "%discount_percent%",
            "%discount_amount%",
            "%payable%",
            "%payable_string%",
            "%payment_method_percent%",
            "%payment_method_discount_amount%",
            "%payed_amount%",
            "%returns%"
        ];
        $financial_unit = setting("financial_unit");
        $values = [
            $seller_details->coll_name,
            $seller_details->prsn_phone1,
            getFile(($seller_details->logo ? $seller_details->logo : 'nologo.png')),
            $seller_details->coll_address,
            $buyer_details->coll_name,
            $buyer_details->prsn_phone1,
            $buyer_details->coll_address,
            $result[array_keys($result)[0]]['fm_no'],
            jdate($result[array_keys($result)[0]]['fm_created_at'])->format('Y/m/d ساعت H:i:s'),
            $result[array_keys($result)[0]]['fm_description'],
            $products,
            withoutZeros($result[array_keys($result)[0]]['fm_festival_discount']),
            number_format($factor_discount),
            number_format($sum_total_bealave_maliat - $factor_discount),
            numtoword($sum_total_bealave_maliat - $factor_discount - $kham) . ' ' . $financial_unit,
            number_format($result[array_keys($result)[0]]['fm_payment_method_percent']),
            number_format($kham),
            number_format($sum_total_bealave_maliat - $factor_discount - $kham) . ' ' . $financial_unit,
            $returns
        ];

        $html = str_replace($keys, $values, $html);

        $fm_no = $result[array_keys($result)[0]]['fm_no'];
        return view('factor.print', compact('html', 'fm_no'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public
    function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public
    function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public
    function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public
    function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public
    function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public
    function destroy($id)
    {
        //
    }

}
