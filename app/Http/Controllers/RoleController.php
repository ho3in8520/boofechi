<?php

namespace App\Http\Controllers;

use App\Baseinfo;
use App\Http\Requests\RoleRequest;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:list_role', ['only' => ['index']]);
        $this->middleware('permission:delete_role', ['only' => ['destroy']]);
        $this->middleware('permission:create_role', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit_role', ['only' => ['update', 'edit']]);
        $this->middleware('permission:set_role_to_class', ['only' => ['setRoleToClass']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = DB::table('roles')->select([
            'rol_id',
            'rol_label',
            'rol_name'
        ]);
        if (request()->has('rol_label'))
            $roles->where('rol_label', 'like', '%' . request('rol_label') . '%');
        if (request()->has('rol_label'))
            $roles->where('rol_name', 'like', '%' . request('rol_name') . '%');

        $roles->where('rol_creator', user('user_id'));
//        if (!can('view-all-roles')) {
//            $roles = $roles->join('user_roles', 'ur_rol_id', '=', 'rol_id');
//        }
        $data = $roles->paginate(20);
        return view('roles.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //i can set only my permissions to users that i create
        $my_permissions = getPermissionIds();
        $perms = \App\Permission::where("perm_status", "=", "1")->orderByRaw("perm_parent_id asc,perm_fm_id asc")
            ->whereIn("perm_id", $my_permissions)
            ->get()->toArray();
        $perms = convertToHierarchy($perms, 'perm_id', 'perm_parent_id');
        return view('roles.form', compact('perms'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleRequest $request)
    {
        $permissions = [];
        if (request()->has('permissions'))
            $permissions = $request->permissions;
        $array = [];
        foreach ($permissions as $permission) {
            array_push($array, [
                'RL' => $permission,
            ]);
        }

        $result = call_sp("add_role", 5, [
            'mode' => 1,
            'title_persian' => $request->roles['rol_label'],
            'title_english' => $request->roles['rol_name'],
            'values' => createRootData($array),
            'creator' => user('user_id'),
            'receive_tickets' => isset($request->roles['receive_tickets']) ? 1 : 0,
        ]);
        if ($result[0]['result'] == 0)
            return json_encode(['status' => 100, 'msg' => 'نقش مورد نظر با موفقیت ثبت شد!']);
        else
            return json_encode(['status' => 500, 'msg' => 'خطا در ثبت اطلاعات، لطفا مجددا تلاش فرمائید.']);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function setRoleToClass(Request $request)
    {
        if (request()->isMethod('post')) {
            $request->validate([
                'senf' => function ($attribute, $value, $fail) {
                    if ($value == "") {
                        $fail('هیچ صنفی انتخاب نشده است!');
                    }
                },
                'panel.*' => function ($attribute, $value, $fail) {
                    if ($value == "") {
                        $fail('لطفا برای تمامی پنل ها نقش انتخاب کنید.');
                    }
                }
            ]);
            $panel = [];
            if (request()->has('panel'))
                $panel = request()->panel;
            $array = [];
            foreach ($panel as $key => $value) {
                array_push($array, [
                    'TP' => $key,
                    'RL' => $value,
                ]);
            }
            $result = call_sp("set_role_to_senf", 2, [
                'senf_id' => request()->senf,
                'values' => createRootData($array)
            ]);
            if ($result[0]['result'] == 0)
                return json_encode(['status' => 100, 'msg' => 'نقش مورد نظر با موفقیت ثبت شد!']);
            else
                return json_encode(['status' => 500, 'msg' => 'خطا در ثبت اطلاعات، لطفا مجددا تلاش فرمائید.']);
        }
        $senfs = Baseinfo::where('bas_type', 'type_class')
            ->where('bas_parent_id', '!=', 0)->get()->toArray();
        $panel_types = Baseinfo::where('bas_type', 'panel_type')
            ->whereNotIn('bas_id', [39, 40, 46])
            ->where('bas_parent_id', '!=', 0)->get()->toArray();
        $roles = Role::where('rol_creator', user('user_id'))->get()->toArray();
        return view('roles.set-role-to-class', compact('senfs', 'panel_types', 'roles'));
    }


    public function getRoleToClass(Request $request)
    {

            $senfs = Baseinfo::where('bas_type', 'type_class')
                ->where('bas_parent_id', '!=', 0)->get()->toArray();
            $panel_types = Baseinfo::where('bas_type', 'panel_type')
                ->whereNotIn('bas_id', [39, 40, 46])
                ->where('bas_parent_id', '!=', 0)->get()->toArray();
            $roles = Role::where('rol_creator', user('user_id'))->get()->toArray();

            $result = DB::table('baseinfos')
                ->select([
                    'bas_id',
                    'clr_type_id',
                    'rol_id'
                ])
                ->where('bas_id', $request->q)
                ->leftJoin('class_roles', 'clr_class_id', 'bas_id')
                ->leftJoin('roles', 'rol_id', 'clr_role_id')
                ->get();
//        $result= json_encode($result);
//dd($result);
         return response()->json(array('response'=> $result), 200);
    }

    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        if ($role['rol_id'] == 1 && !user()->hasRole('programmer'))
            return redirect()->back();

        //i can set only my permissions to users that i create
        $my_permissions = getPermissionIds();
        $perms = \App\Permission::where("perm_status", "=", "1")->orderByRaw("perm_parent_id asc,perm_fm_id asc")
            ->whereIn("perm_id", $my_permissions)
            ->get()->toArray();
        $perms = convertToHierarchy($perms, 'perm_id', 'perm_parent_id');
        $rolPermissions = $role->permissions()->get()->pluck('perm_id')->toArray();
        return view('roles.form', compact('role', 'rolPermissions', 'perms'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param object $role
     * @return \Illuminate\Http\Response
     */
    public function update(RoleRequest $request, Role $role)
    {
        $permissions = [];
        if (request()->has('permissions'))
            $permissions = $request->permissions;
        $array = [];
        foreach ($permissions as $permission) {
            array_push($array, [
                'RL' => $permission,
            ]);
        }

        $result = call_sp("add_role", 5, [
            'mode' => 2,
            'title_persian' => $request->roles['rol_label'],
            'title_english' => $request->roles['rol_name'],
            'values' => createRootData($array),
            'id' => $role->rol_id,
            'receive_tickets' => isset($request->roles['receive_tickets']) ? 1 : 0,
        ]);
        if ($result[0]['result'] == 0)
            return json_encode(['status' => 100, 'msg' => 'نقش مورد نظر با موفقیت ثبت شد!']);
        else
            return json_encode(['status' => 500, 'msg' => 'خطا در ثبت اطلاعات، لطفا مجددا تلاش فرمائید.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        if ($id == 1 && !user()->hasRole('programmer'))
            return json_encode(['status' => 500, 'msg' => config('first_config.message.relation_delete')]);

        $result = checkRelation('roles', $id);
        if (empty($result) || $result[0]['ID'] == 0) {
            DB::delete('delete from class_roles where clr_role_id = ?', [$id]);
            DB::delete('delete from user_roles where ur_rol_id = ?', [$id]);
            Role::where('rol_id', $id)->delete();
            return \redirect()->back()->with("success", "عملیات باموفقیت انجام شد!");
        } else {
            return \redirect()->back()->with("error", config('first_config.message.relation_delete'));
        }
    }

    public function destroy($id)
    {
        
    }
}
