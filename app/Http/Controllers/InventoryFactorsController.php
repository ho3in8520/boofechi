<?php

namespace App\Http\Controllers;

use App\Http\Requests\InventoryFactorsRequest;
use App\Inventories;
use App\FinancialCycle;
use Illuminate\Http\Request;

class InventoryFactorsController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:increase_inventory', ['only' => ['create','store']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('inventory-factors.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(InventoryFactorsRequest $request)
    {
        $inv_id = Inventories::where("inv_collection_id", $collection_id = collection_id())->first()->inv_id;
        $fc_id = FinancialCycle::where("fc_collection_id", $collection_id = collection_id())->first()->fc_id;
        $array = [];
        for ($i = 0; $i < count($request->product_id); $i++) {
            if ($request->product_id[$i])
                array_push($array, [
                    'PRD' => $request->product_id[$i],
                    'PRB' => $request->product_price_buy[$i],
                    'PRC' => $request->product_price_consumer[$i],
                    'CNT' => $request->count_product[$i],
                ]);
        }
        if (!empty($array)) {
            $result = call_sp("inventory_factor", null, [
                'inv_id' => $inv_id,
                'user_id' => user("user_id"),
                'fc_id' => $fc_id,
                'user_session_id' => session()->getId(),
                'type' => 0, //in
                'collection_id' => user()->person->collections->first()->coll_id,
                'shamsi_date' => jdate()->format('Y/m/d'),
                'created_at' => date("Y-m-d H:i:s"),
                'values' => createRootData($array)
            ]);
            if ($result[0]['result'] == 0)
                $array = ["status" => "100", "msg" => "ثبت موجودی با موفقیت انجام شد!"];
            else
                $array = ["status" => "500", "msg" => "خطا در ثبت موجودی، مجددا تلاش فرمائید!"];
        } else
            $array = ["status" => "500", "msg" => "خطا در ثبت موجودی، مجددا تلاش فرمائید!"];
        return json_encode($array);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
