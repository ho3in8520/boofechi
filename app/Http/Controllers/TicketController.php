<?php

namespace App\Http\Controllers;

use Egulias\EmailValidator\Exception\ExpectingCTEXT;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class TicketController extends Controller
{
    public function __construct()
    {
//        $this->middleware('permission:menu_user');
        $this->middleware('permission:list_tickets', ['only' => ['show', 'index']]);
        $this->middleware('permission:close_ticket', ['only' => ['close']]);
        $this->middleware('permission:create_ticket', ['only' => ['create', 'store']]);
        $this->middleware('permission:answer_ticket', ['only' => ['answer']]);
        $this->middleware('permission:view_ticket', ['only' => ['show-ticket']]);
        $this->middleware('permission:referral_ticket', ['only' => ['referral']]);
    }

    public
    function index(Request $request)
    {
        $statuses = config('first_config.ticket-statuses');
//        $sql = "select tickets.t_id,tickets.t_assigned_role_id,parent.t_subject from tickets
//                left join tickets parent on parent.t_id = tickets.t_parent_id
//                where tickets.t_id in(
//                select max(t_id) from tickets
//                where  t_parent_id != 0)
//                union
//                select t_id,t_assigned_role_id,t_subject from tickets
//                where t_parent_id = 0
//                and t_id not in(select t_parent_id from tickets)";

       /* $ticketsA = DB::table('tickets as last_answer')
            ->select([
                'parent.t_subject',
                'parent.t_id as t_id',
                'rol_label',
                'last_answer.t_created_at',
                'last_answer.t_status',
                DB::raw('(select top 1 tr_is_viewed from ticket_routing where tr_ticket_id = last_answer.t_id and (tr_is_viewed = 0 or tr_viewed_user_id = ' . user('user_id') . ') order by tr_id desc) is_viewed')
            ])
            ->leftJoin('roles', 'last_answer.t_assigned_role_id', '=', 'rol_id')
            ->leftJoin('tickets as parent', 'parent.t_id', '=', 'last_answer.t_parent_id')
            ->whereRaw('last_answer.t_assigned_role_id in (select ur_rol_id from user_roles where ur_user_id = ' . user('user_id') . ')')
            ->whereRaw("last_answer.t_id in(select max(t_id) from tickets where t_parent_id != 0 group by t_parent_id)")
            ->where('last_answer.t_mode', 0);
        if ($request->has('ticket_no') && $request->get('ticket_no') != '')
            $ticketsA->where('last_answer.t_id', $request->get('ticket_no'));
        if ($request->has('ticket_subject') && $request->get('ticket_subject') != '')
            $ticketsA->where('parent.t_subject', 'like', '%' . $request->get('ticket_subject') . '%');
        if ($request->has('ticket_department') && $request->get('ticket_department') != '')
            $ticketsA->where('last_answer.t_assigned_role_id', $request->get('ticket_department'));*/

        $tickets = DB::table('tickets')
            ->select([
                't_id',
                't_subject',
                'rol_label',
                't_created_at',
                't_assigned_user_id',
                't_status',
                DB::raw('(select top 1 tr_is_viewed from ticket_routing where tr_ticket_id = t_id and (tr_is_viewed = 0 or tr_viewed_user_id = ' . user('user_id') . ') order by tr_id desc) is_viewed')
            ])
            //->join('ticket_routing', 'tr_ticket_id', '=', 't_id')
            ->join('ticket_routing', 'tr_ticket_id', '=', 't_id')
            ->join('roles', 't_assigned_role_id', '=', 'rol_id')
            ->where('t_parent_id', 0)
            ->where('t_mode', 0)
            ->whereRaw('((select top 1 tr_viewed_user_id from ticket_routing where tr_ticket_id = t_id order by tr_id desc) = '.user('user_id').' or t_assigned_user_id = '.user('user_id').' or (t_assigned_role_id in (select ur_rol_id from user_roles where ur_user_id = ' . user('user_id') . ') and t_assigned_user_id = 0 and (select top 1 tr_is_viewed from ticket_routing where tr_ticket_id = t_id and (tr_is_viewed = 0 or tr_viewed_user_id = ' . user('user_id') . ') order by tr_id desc) = 0))');
        if ($request->has('ticket_no') && $request->get('ticket_no') != '')
            $tickets->where('t_id', $request->get('ticket_no'));
        if ($request->has('ticket_subject') && $request->get('ticket_subject') != '')
            $tickets->where('t_subject', 'like', '%' . $request->get('ticket_subject') . '%');
        if ($request->has('ticket_department') && $request->get('ticket_department') != '')
            $tickets->where('t_assigned_role_id', $request->get('ticket_department'));
        $tickets->distinct('t_id');
       $tickets = $tickets->orderBy("t_id","desc")->paginate(20);

        $departments = /*array_merge()*/
            getRoles();//rolhaye admin
        return showData(view('ticket.index', compact('tickets', 'statuses', 'departments')));
    }

    public
    function myTickets(Request $request)
    {
        //close all open tickets
        $time = 48 * 60 * 60;
        DB::table('tickets')
            ->where('t_mode', 0)
            ->whereRaw("t_created_at + $time < " . time())
            ->update([
                't_mode' => 1,
                't_status' => 4
            ]);

        $statuses = config('first_config.ticket-statuses');
        $tickets = DB::table('tickets as parent')
            ->select([
                'parent.t_subject',
                'parent.t_id',
                'rol_label',
                DB::raw('(case when last_answer.t_created_at is not null then last_answer.t_created_at else parent.t_created_at end) as t_created_at'),
                'parent.t_status'
            ])
            ->join('roles', 'parent.t_assigned_role_id', '=', 'rol_id')
            ->leftJoin(DB::raw('(select top 1 * from tickets order by t_id desc) as last_answer'), 'parent.t_id', '=', 'last_answer.t_parent_id');

        if ($request->has('ticket_no') && $request->get('ticket_no') != '')
            $tickets->where('parent.t_id', $request->get('ticket_no'));
        if ($request->has('ticket_subject') && $request->get('ticket_subject') != '')
            $tickets->where('parent.t_subject', 'like', '%' . $request->get('ticket_subject') . '%');
        if ($request->has('ticket_department') && $request->get('ticket_department') != '')
            $tickets->where('last_answer.t_assigned_role_id', $request->get('ticket_department'))
                ->orWhere('parent.t_assigned_role_id', $request->get('ticket_department'));

        $tickets->orderBy("t_id","desc");

        $tickets->where('parent.t_creator_user_id', user('user_id'))->where('parent.t_parent_id',0);

        $tickets = $tickets->paginate(20);

        $departments = getRoles("self-domain");
        return showData(view('ticket.index', compact('tickets', 'statuses', 'departments')));
    }

    public function referral(Request $request, $id)
    {
        $ticket = DB::table('tickets')
            ->where('t_id', $id)
            ->first();
        if (!$ticket)
            abort(404);

        $request->validate([
            'ticket.section' => 'required'
        ]);
        $result = DB::table('tickets')->where('t_id', $id)->update([
            't_assigned_role_id' => $request->ticket['section'],
            't_status' => 3
        ]);
        DB::table('ticket_routing')->insert([
            'tr_ticket_id' => $id,
            'tr_role_id' => $request->ticket['section'],
            'tr_created_at' => time()
        ]);
        if ($result)
            return json_encode(['status' => 100, 'msg' => 'با موفقیت ارجاع شد','url'=>asset('/ticket')]);
        return json_encode(['status' => 500, 'msg' => 'خطا در ارجاع تیکت، مجددا تلاش فرمائید!']);
    }

    public function close(Request $request)
    {
        $id = $request->get('id');
        $ticket = DB::table('tickets')
            ->where('t_id', $id)
            ->first();
        if (!$ticket)
            abort(404);

        $request->validate([
            'ticket.section' => 'required'
        ]);
        $last_answer = DB::table('tickets')
            ->where('t_parent_id', $id)
            ->latest('t_id')
            ->first();
        if ($last_answer)
            $id = $last_answer->t_id;
        $result = DB::table('tickets')->where('t_id', $id)->update([
            't_mode' => 1,
            't_status' => 4
        ]);
        if ($result)
            return json_encode(['status' => 100, 'msg' => 'با موفقیت بسته شد']);
        return json_encode(['status' => 500, 'msg' => 'خطا در بستن تیکت، مجددا تلاش فرمائید!']);
    }

    public
    function create(Request $request)
    {
        $departments = getRoles("self-domain");
        return view('ticket.form', compact('departments'));
    }

    public function show(Request $request, $id)
    {
        $ticket = DB::table('tickets')
            ->where('t_id', $id)
            ->where('t_parent_id',0)
            ->first();
        if (!$ticket)
            abort(404);

        $already_viewed = DB::table('ticket_routing')->where('tr_ticket_id', $id)
            ->join('tickets', 't_id', '=', 'tr_ticket_id')
            ->join('user_roles', function ($join) {
                $join->on("ur_rol_id", "=", "tr_role_id");
                $join->where("ur_user_id", user('user_id'));
                return $join->whereRaw("tr_id = (select max(tr_id) from ticket_routing where tr_ticket_id = t_id)");
            })->select([
                'tr_id',
                'tr_is_viewed',
                'tr_viewed_user_id'
            ])->latest('tr_id')->first();
        if ($already_viewed->tr_viewed_user_id == 0) {
            DB::table('ticket_routing')->where('tr_id', $already_viewed->tr_id)
                ->update([
                    'tr_is_viewed' => 1,
                    'tr_viewed_user_id' => user('user_id'),
                    'tr_viewed_at' => time()
                ]);
        } else {
            if (!in_array($already_viewed->tr_viewed_user_id, [0, user('user_id')]))
                return \redirect(route('ticket.index'));
        }

        $sql = "
            ;with cte as
            (
                select t_id, t_subject,t_parent_id
                from tickets
                where t_parent_id = 0 and t_id = ?
                union all
                select tik.t_id,tik.t_subject,tik.t_parent_id
                from cte join tickets tik
                    on tik.t_parent_id = cte.t_id
            )
            select tickets.*,concat(prsn_name,' ',prsn_family) as prsn_name,prsn_user_id,
            (select top 1 ur_path from upload_route where ur_table_name = 'tickets' and ur_fk_id = t_id order by ur_id desc) as t_file
            from tickets
             left join persons on prsn_user_id = t_creator_user_id
             where t_id in
            (
                select t_id from cte
            )
            order by t_id asc
        ";
        $result = DB::select($sql, [$id]);
        $departments = getRoles();
        return view('ticket.show', compact('result', 'departments', 'id', 'ticket'));
    }

    public function showTicketCustomer(Request $request, $id)
    {
        $ticket = DB::table('tickets')
            ->where('t_id', $id)
            ->where('t_creator_user_id', user('user_id'))
            ->first();
        if (!$ticket)
            abort(404);

        $sql = "
            ;with cte as
            (
                select t_id, t_subject,t_parent_id
                from tickets
                where t_parent_id = 0 and t_id = ?
                union all
                select tik.t_id,tik.t_subject,tik.t_parent_id
                from cte join tickets tik
                    on tik.t_parent_id = cte.t_id
            )
            select tickets.*,concat(prsn_name,' ',prsn_family) as prsn_name,prsn_user_id,
            (select top 1 ur_path from upload_route where ur_table_name = 'tickets' and ur_fk_id = t_id order by ur_id desc) as t_file
            from tickets
             left join persons on prsn_user_id = t_creator_user_id
             where t_id in
            (
                select t_id from cte
            )
            order by t_id asc
        ";
        $result = DB::select($sql, [$id]);
        $departments = getRoles();
//        dd($result);
        return showData(view('ticket.show', compact('result', 'departments', 'id', 'ticket')));
    }

    public function answer(Request $request, $id)
    {
        $ticket = DB::table('tickets')
            ->where('t_id', $id)
            ->first();
        if (!$ticket)
            abort(404);
        $request->validate([
            'ticket.text' => 'required',
            'ticket.file' => 'max:6024',
        ]);
        $status = 1;
        if ($ticket->t_creator_user_id == user('user_id'))
            $status = 2;

        $ticket_id = DB::table('tickets')->insertGetId([
            't_parent_id' => $id,
            't_subject' => null,
            't_description' => $request->ticket['text'],
            't_created_at' => time(),
            't_creator_user_id' => user('user_id'),
            't_assigned_role_id' => $ticket->t_assigned_role_id, //role administrator
//                't_assigned_user_id' => 2, //user admin
            't_status' => 0
        ]);
        DB::table("tickets")->where('t_id',$ticket->t_id)->update([
            't_status'=>$status
        ]);
        if ($request->file('file'))
            DB::table('upload_route')->insert([
                'ur_collection_id' => collection_id(),
                'ur_fk_id' => $ticket_id,
                'ur_table_name' => 'tickets',
                'ur_is_deleted' => 0,
                'ur_status' => 0,
                'ur_path' => uploadFile($request->file('file'), 'ticket'),
                'created_at' => date("Y/m/d H:i:s"),
            ]);

        $result = DB::table('ticket_routing')->insert([
            'tr_ticket_id' => $ticket_id,
            'tr_role_id' => $ticket->t_assigned_role_id,
            'tr_created_at' => time()
        ]);
        if ($result)
            return json_encode(['status' => 100, 'msg' => 'پاسخ با موفقیت ثبت شد']);
        return json_encode(['status' => 500, 'msg' => 'خطا در ثبت پاسخ، مجددا تلاش فرمائید!']);
    }

    public function store(Request $request)
    {
        if ($request->isMethod('post')) {
            $request->validate([
                'ticket.section' => 'required | exists:roles,rol_id',
                'ticket.title' => 'required',
                'ticket.text' => 'required',
                'ticket.file' => 'max:6024',
            ]);
            $ticket_id = DB::table('tickets')->insertGetId([
                't_parent_id' => 0,
                't_subject' => $request->ticket['title'],
                't_description' => $request->ticket['text'],
                't_created_at' => time(),
                't_creator_user_id' => user('user_id'),
                't_assigned_role_id' => $request->ticket['section'], //role administrator
//                't_assigned_user_id' => 2, //user admin
                't_status' => 0
            ]);
            if ($request->file('file'))
                DB::table('upload_route')->insert([
                    'ur_collection_id' => collection_id(),
                    'ur_fk_id' => $ticket_id,
                    'ur_table_name' => 'tickets',
                    'ur_is_deleted' => 0,
                    'ur_status' => 0,
                    'ur_path' => uploadFile($request->file('file'), 'ticket'),
                    'created_at' => date("Y/m/d H:i:s"),
                ]);

            $result = DB::table('ticket_routing')->insert([
                'tr_ticket_id' => $ticket_id,
                'tr_role_id' => $request->ticket['section'],
                'tr_created_at' => time()
            ]);

            if ($result)
                return json_encode(['status' => 100, 'msg' => 'تیکت با موفقیت ایجاد شد، شماره تیکت ' . $ticket_id, 'pin' => true]);
            return json_encode(['status' => 500, 'msg' => 'خطا در ایجاد تیکت، مجددا تلاش فرمائید!']);

        }
    }

}
