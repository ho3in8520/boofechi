<?php

namespace App\Http\Controllers;

use App\Collection;
use App\Http\Requests\UserReuqest;
use App\Person;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:list_user', ['only' => ['index']]);
        $this->middleware('permission:delete_user', ['only' => ['destroy']]);
        $this->middleware('permission:create_user', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit_user', ['only' => ['update', 'edit']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $collections = \App\Baseinfo::whereBasType('role_collection')->where('bas_parent_id', '<>', '0')->get();
        $users = DB::table('users')
            ->selectRaw("
            users.*,concat(p.prsn_name,' ',p.prsn_family) as full_name,
            b1.bas_value as gender,coll.coll_name,b2.bas_value as typeCollection
            ")
            ->leftjoin('persons as p', 'p.prsn_user_id', '=', 'users.user_id')
            ->leftjoin('baseinfos as b1', 'b1.bas_id', '=', 'p.prsn_gender_id')
            ->leftjoin('collection_persons as copr', 'copr.cp_prsn_id', '=', 'p.prsn_id')
            ->leftjoin('collections as coll', 'coll.coll_id', '=', 'copr.cp_coll_id')
            ->leftjoin('baseinfos as b2', 'b2.bas_id', '=', 'coll.coll_role_id');
        if (request()->has('full_name') && request('full_name') != "")
            $users = $users->whereRaw("concat(p.prsn_name,' ',p.prsn_family) like ?", ['%' . request('full_name') . '%']);
        if (request()->has('username') && request('username') != "")
            $users = $users->whereRaw("username like ?", ['%' . request('username') . '%']);
        if (request()->has('coll_name') && request('coll_name') != "")
            $users = $users->whereRaw("coll_name like ?", ['%' . request('coll_name') . '%']);
        if (request()->has('typeCollection') && request('typeCollection') != "")
            $users = $users->whereRaw("coll.coll_role_id =  ?", [request('typeCollection')]);
        $users = $users->whereNotIn('user_id', [1, 2])->groupBy("users.user_id")->paginate(20);
        return view('users.index', compact('users', 'collections'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (request()->has('prsn_id') && request('prsn_id') != "") {
            $person = Person::find(request('prsn_id'));
            if (empty($person))
                return abort(404);
            $collection = $person->collections->first();
            if (empty($collection))
                return abort(404);
            $url = getTypeCollection($collection);
            $url .= $collection->coll_id . '/edit';
            return view('users.form', compact('url'));
        } else
            return redirect()->back();
    }

    public function bagLog()
    {
        $sql = "select * from(
                        select bt_time,bt_amount,bt_description,bt_status,bt_type from bag_transacts where bt_user_id = " . user("user_id") . " 
                        union 
                        select fm_created_at as bt_time,dbo.get_factor_amount(fm_id,2) as bt_amount,fm_description,0,
                        cpm_bas_id from factor_master
                        join collection_payment_methods on cpm_id = fm_payment_method
                        where fm_status = 2 and fm_creator_id = " . user("user_id") . " and cpm_bas_id in(51,52)
                )x where  1 = 1 ";
        $params = [];

        if (\request()->has('typeTransacts') && request()->get('typeTransacts') != 0) {
            $sql .= " and x.bt_type = :bt_type";
            $params [':bt_type'] = \request()->get("typeTransacts");
        }
        if (\request()->has('statusTransacts') && request()->get('statusTransacts') != 0) {
            $status_array = [2 => 0, 1 => 1];
            $sql .= " and x.bt_status = :bt_status";
            $params [':bt_status'] = $status_array[\request()->get("statusTransacts")];
        }
        if (\request()->has('description') && request()->get('description') != "") {
            $sql .= " and x.bt_description like :description";
            $params [':description'] = "%".\request()->get("description")."%";
        }

        $sql .= " order by bt_time desc";
        $result = DB::select($sql,$params);

        return view('users.bag', compact('result'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request\UserRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserReuqest $request)
    {
        DB::beginTransaction();
        try {
            $person = Person::find($request->prsn_id);
            $user = User::updateOrCreate([
                'user_id' => $person->prsn_user_id
            ], array_merge($request->users, [
                //'code' => rand(1, 500000),
                'password' => bcrypt($request->password),
                'users.is_active' => $request->has('users.is_active') ? 1 : 0
            ]));
            //$user->roles()->sync($request->roles['rol_id']);
            Person::wherePrsnId($request->prsn_id)->update(["prsn_user_id" => $user->user_id]);
            $class = $person->collections->first()->collectionClass->pluck('coc_typeClass_id')->toArray();
            $role = DB::table('class_roles')->whereIn('clr_class_id', $class)
                ->whereClrTypeId($person->collections->first()->coll_role_id)
                ->pluck('clr_role_id')->toArray();
            DB::commit();
            return redirect("/company/area-support/" . $person->collections->first()->coll_id);
        } catch (\Exception $exception) {
            DB::rollBack();
            session()->flash('Error', config('first_config.message.error_message'));
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param App\User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $rol = $user->roles->first();
        $person = Person::wherePrsnUserId($user->user_id)->first();
        $collection = $person->collections->first();
        $result = getTypeCollection($collection);
        $url = $result . $collection->coll_id . '/edit';
        return view('users.form', compact('user', 'rol', 'url'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request\UserRequest $request
     * @param App\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserReuqest $request, User $user)
    {
        $password = [];
        if ($request->password != "")
            $password = ['password' => bcrypt($request->password),];
        DB::beginTransaction();
        try {
            User::whereUserId($user->user_id)->update(array_merge(($request->users), $password, [
                'is_active' => 1
            ]));
            DB::commit();
            $person = Person::wherePrsnUserId($user->user_id)->first();
            return redirect("/company/area-support/" . $person->collections->first()->coll_id);
        } catch (\Exception $exception) {
            DB::rollBack();
            echo $exception->getMessage();
            session()->flash('Error', config('first_config.message.error_message'));
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function updateRole(Request $request, User $user)
    {
        DB::beginTransaction();
        try {
            $user->roles()->detach();
            $user->roles()->sync($request->rol);
            DB::commit();
            return json_encode(["status" => "100", "msg" => "اطلاعات با موفقیت ثبت شد"]);
        } catch (\Exception $exception) {
            DB::rollBack();
            return json_encode(["status" => "500", "msg" => "اطلاعات ثبت نشد خطای سیستمی رخ داده لطفا با واحد پشتیبانی تماس حاصل فرمایید"]);
        }

    }

    public function chargingWallet(Request $request)
    {
        try {
            $amount = $request->get('amount');

            if ($amount < app()->config->get('gateway.sadad.min-value')) {
                return json_encode(['status' => 500, 'msg' => 'مبلغ تراکنش کمتر از حد مجاز است!']);
            }
            $gateway = paymentClass(5); //sadad
            $gateway
                ->price($amount)
                ->ready();
            if (DB::table('gateway_transact')->where("transact_id", $gateway->transactionId())->update([
                "transact_type" => \App\Gateway\Enum::TRANSACTTION_SYSTEM_CHARGE,
                "transact_fk_id" => user('user_id'), //id factor_or_user
                "transact_refId" => $gateway->refId(),
                "transact_temp" => $request->get('factor_id') ?? 0
            ])
            ) {
                return json_encode(['status' => 100, 'url' => $gateway->redirect(), 'msg' => 'در حال انتقال به بانک...']);
            } else {
                return json_encode(['status' => 500, 'msg' => $gateway]);
            }
            exit();
        } catch (\Exception $e) {
            return json_encode(['status' => 500, 'msg' => 'خطا در اتصال به بانک، لطفا مجددا تلاش فرمائید!']);
        }
    }
}
