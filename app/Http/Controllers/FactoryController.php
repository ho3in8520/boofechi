<?php

namespace App\Http\Controllers;

use App\City;
use App\Collection;
use App\Component\Tools;
use App\Http\Requests\FactoryRequest;
use App\Person;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FactoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = Tools::getList($request, 41);
        return showData(view('factories.index', compact('data')));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view("factories.form");
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(FactoryRequest $request)
    {
        $result = Tools::collection_store($request, 41);

        return redirect('/user/create?prsn_id=' . $result);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Collection $factory)
    {
        if($factory->coll_role_id <> 14)
            return redirect()->back();
        $collections = $factory;
        $persons = $factory->persons->first();
        $collectionClass = DB::table('collection_classes')->whereCocCollId($factory->coll_id)->pluck('coc_typeClass_id')->toArray();
        return view("factories.form", compact('collections', 'persons','collectionClass'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(FactoryRequest $request, Collection $factory)
    {
        $result = Tools::collection_update($request, $factory);
        $person = $factory->persons->first();
        if ($person->prsn_user_id == 1)
            return redirect('/user/create?prsn_id=' . $person->prsn_id);
        return redirect('/user/' . $person->prsn_user_id . '/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function payment_methods()
    {
        return view('factories.payment_methods');
    }
}
