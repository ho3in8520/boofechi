<?php

namespace App\Http\Controllers;

use App\Announcement;
use App\Http\Requests\AnnoucementRequest;
use Illuminate\Support\Facades\DB;

class AnnouncementController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:list_announcements', ['only' => ['index']]);
        $this->middleware('permission:delete_announcements', ['only' => ['destroy']]);
        $this->middleware('permission:create_announcements', ['only' => ['create','store']]);
        $this->middleware('permission:edit_announcements', ['only' => ['update','edit']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Announcement::where("ann_user_id", user('user_id'))
            ->where("ann_collection_id", collection_id());

        if (\request('title') && \request('title') != "")
            $data->where('ann_title', 'like', '%' . \request('title') . '%');

        if (\request('user_id') && \request('user_id') != "")
            $data->where('ann_user_id', \request('user_id'));

        $data = $data->with('user')
            ->paginate(20);
        return view('announcements.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $selected_types = [];
        $temp = '{}';
        $types = getTypesForAnnouncements();
        return view('announcements.form', compact("types", 'temp', 'selected_types'));
    }

//    /**
//     * Store a newly created resource in storage.
//     *
//     * @param  \Illuminate\Http\Request $request
//     * @return \Illuminate\Http\Response
//     */
    public function store(AnnoucementRequest $request)
    {
        $cities = explode(',', $request->selected_cities);
        $array = [];
        foreach ($cities as $city) {
            foreach (array_keys($request->role_person) as $type) {
                array_push($array, [
                    'CT' => $city,
                    'TP' => $type
                ]);
            }
        }
        if ($request->has('type') && $request->type=='news')
            $radio_type=1;
        else
            $radio_type=0;
        $result = call_sp("add_announcement", 6, [
            'mode' => 1,
            'title' => $request->announcement['ann_title'],
            'body' => $request->ann_body,
            'user_id' => user('user_id'),
            'collection_id' => user()->person->collections->first()->coll_id,
            'values' => createRootData($array),
            'timestmp' => date("Y-m-d H:i:s"),
            'type'=>$radio_type
        ]);
        if ($result[0]['result'] == 0)
            return json_encode(['status' => 100, 'msg' => 'اطلاعیه مورد نظر با موفقیت ثبت شد!']);
        else if ($result[0]['result'] == -1)
            return json_encode(['status' => 500, 'msg' => 'در هر روز امکان ارسال یک اطلاعیه وجود دارد!']);
        else
            return json_encode(['status' => 500, 'msg' => 'خطا در ثبت اطلاعیه، لطفا مجددا تلاش فرمائید.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $types = getTypesForAnnouncements();
        $announcement = Announcement::where('ann_id', $id)->firstOrFail();
        $selected_receivers = DB::table('announcement_receivers')->where('anr_announcement_id', $id)->get();
        $selected_receivers = json_decode(json_encode($selected_receivers), true);
        $selected_types = array_unique(array_column($selected_receivers, 'anr_type_id'));

        $areas = DB::table('announcement_receivers')
            ->select([
                "cities.c_id",
                "cities.c_name",
                "parent.c_name as parent"
            ])
            ->join("cities", "c_id", "=", "anr_city_id")
            ->join("cities as parent", "parent.c_id", "=", "cities.c_parent_id")
            ->where("anr_announcement_id", $id)
            ->groupBy('cities.c_id', 'cities.c_name', 'parent.c_name')
            ->get()
            ->toArray();
        $arr = json_decode(json_encode($areas), true);
        $temp = [];
        foreach ($arr as $row) {
            $temp[$row['parent']][] = [
                "id" => $row['c_id'],
                "name" => $row['c_name']
            ];
        }
        foreach ($temp as $key => $value) {
            $temp[$key] = [
                'text' => array_column($value, 'name'),
                'ids' => array_column($value, 'id'),
            ];
        }
        $temp = empty($temp) ? "{}" : json_encode($temp, JSON_UNESCAPED_UNICODE);
        return view('announcements.form', compact("types", 'announcement', 'temp', 'selected_types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(AnnoucementRequest $request, $id)
    {
        $cities = explode(',', $request->selected_cities);
        $array = [];
        foreach ($cities as $city) {
            foreach (array_keys($request->role_person) as $type) {
                array_push($array, [
                    'CT' => $city,
                    'TP' => $type
                ]);
            }
        }
        if ($request->has('type') && $request->type=='news')
            $radio_type=1;
        else
            $radio_type=0;
        $result = call_sp("add_announcement", 6, [
            'mode' => 2,
            'id' => $id,
            'title' => $request->announcement['ann_title'],
            'body' => $request->announcement['ann_body'],
            'values' => createRootData($array),
            'timestmp' => date("Y-m-d H:i:s"),
            'type'=>$radio_type
        ]);
        if ($result[0]['result'] == 0)
            return json_encode(['status' => 100, 'msg' => 'اطلاعیه مورد نظر با موفقیت ویرایش شد!']);
        else
            return json_encode(['status' => 101, 'msg' => 'خطا در ویرایش اطلاعیه، لطفا مجددا تلاش فرمائید.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = checkRelation('announcements', $id);
        if (empty($result) || $result[0]['ID'] == 0) {
            DB::delete('delete from announcement_receivers where anr_announcement_id = ?', [$id]);
            Announcement::where('ann_id', $id)->delete();
            return json_encode(['status' => 100, 'msg' => 'عملیات حذف با موفقیت انجام شد!']);
        } else {
            return json_encode(['status' => 101, 'msg' => config('first_config.message.relation_delete')]);
        }
    }
}
