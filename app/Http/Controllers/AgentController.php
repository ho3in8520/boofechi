<?php

namespace App\Http\Controllers;

use App\Baseinfo;
use App\Collection;
use App\Component\Tools;
use App\Http\Requests\AgentRequest;
use App\Person;
use App\UploadRoute;
use App\User;
use App\Role;
use App\Agents;
use Illuminate\Support\Facades\Hash;
use App\CollectionPersons;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\DocBlock\Tags\Uses;

class AgentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(AgentRequest $request)
    {
        Tools::enroll_visitore_supermarket_store($request);
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function registerAgent(Request $request)
    {
        $collections=null;
        $upload_find=null;
        $company = Baseinfo::where('bas_type', 'company-type')
            ->where('bas_parent_id', '!=', '0')
            ->get();
        if ($request->isMethod('post')) {
            $request->validate([
                'persons.prsn_name' => ['required'],
                'persons.prsn_family' => ['required'],
                'persons.prsn_national_code' => ['required', 'max:10', 'regex:/([0-9]+){10}/'],
                'persons.prsn_father_name' => ['required'],
                'persons.prsn_birthday' => ["required", "regex:/^[1-4]\d{3}\/((0[1-6]\/((3[0-1])|([1-2][0-9])|(0[1-9])))|((1[0-2]|(0[7-9]))\/(30|([1-2][0-9])|(0[1-9]))))$/"],
                'persons.prsn_certificate_code' => ['required', 'regex:/([0-9]+)/'],
                'persons.prsn_serial_certificate_code' => ['required', 'regex:/([0-9]+)/'],
                'persons.prsn_phone1' => ['required', 'regex:/([0-9]+){11}/'],
                'persons.prsn_mobile1' => ['required', 'regex:/([0-9]+){11}/'],
                'collections.coll_email' => ['required', 'email'],
                'persons.prsn_gender_id' => ['required'],
                'persons.prsn_post_code' => ['required', 'max:10', 'regex:/([0-9]+){10}/'],
                'persons.prsn_address' => ['required'],
                'collections.coll_name' => ['required'],
//                'collections.coll_type_class_id' => ['required'],
                'collections.coll_state_id' => ['required'],
                'collections.coll_city_id' => ['required'],
                'collections.coll_post_code' => ['required', 'max:10', 'regex:/([0-9]+){10}/'],
                'collections.domain' => ['required'],
                'collections.username' => ['required', 'unique_string:users,username'],
                'collections.password' => ['required'],
                'collections.coll_address' => ['required'],
                'collections.role' => function ($attribute, $value, $fail) {
                    if ($value == "") {
                        $fail('لطفا یک نقش را انتخاب کنید.');
                    }
                }

            ]);

            $create_user = new User();
            $create_user->username = strtolower($request->collections['username']);
            $create_user->password = Hash::make($request->collections['password']);
            $create_user->code = 125;
            $create_user->email = $request->collections['coll_email'];
            $create_user->is_active = 1;
            $create_user->panel_type = 46;
            $create_user->created_at = date('Y-m-d');
            $create_user->creator = user('user_id');
            $create_user->agent_id = 1;
            $create_user->accept_terms = 0;
            $create_user->profile_complete = 1;
            $create_user->save();

            $create_person = new Person();
            $create_person->prsn_name = $request->persons['prsn_name'];
            $create_person->prsn_family = $request->persons['prsn_family'];
            $create_person->prsn_user_id = $create_user->user_id;
            $create_person->prsn_gender_id = $request->persons['prsn_gender_id'];
            $create_person->prsn_father_name = $request->persons['prsn_father_name'];
            $create_person->prsn_birthday = $request->persons['prsn_birthday'];
            $create_person->prsn_national_code = $request->persons['prsn_national_code'];
            $create_person->prsn_certificate_code = $request->persons['prsn_certificate_code'];
            $create_person->prsn_serial_certificate_code = $request->persons['prsn_serial_certificate_code'];
            $create_person->prsn_mobile1 = $request->persons['prsn_mobile1'];
            $create_person->prsn_phone1 = $request->persons['prsn_phone1'];
            $create_person->prsn_post_code = $request->persons['prsn_post_code'];
            $create_person->prsn_address = $request->persons['prsn_address'];
            $create_person->save();

            $create_coll = new Collection();
            $create_coll->coll_name = $request->collections['coll_name'];
            $create_coll->coll_economic_code = $request->collections['coll_economic_code'];
            $create_coll->coll_national_number = $request->collections['coll_national_number'];
            $create_coll->coll_is_deleted = 0;
            $create_coll->coll_post_code = $request->collections['coll_post_code'];
            $create_coll->coll_address = $request->collections['coll_address'];
            $create_coll->coll_state_id = $request->collections['coll_state_id'];
            $create_coll->coll_city_id = $request->collections['coll_city_id'];
            $create_coll->save();

            $coll_pers = new CollectionPersons();
            $coll_pers->cp_prsn_id = $create_person->prsn_id;
            $coll_pers->cp_coll_id = $create_coll->coll_id;
            $coll_pers->save();

            $domain = getDomain();
            $agent = getAgent($domain);
            $agent->ag_id;

            $create_agent = new Agents();
            $create_agent->ag_domain = $request->collections['domain'];
            $create_agent->ag_status = 1;
            $create_agent->ag_parent_id = $agent;
            $create_agent->save();

            $update_user = User::find($create_user->user_id);
            $update_user->agent_id = $create_agent->ag_id;
            $update_user->save();

            DB::table('user_roles')
                ->insert([
                    'ur_user_id'=>$create_user->user_id,
                    'ur_role_id'=>$request->collections['role'],
                ]);


            if (!$request->has('ur_url') || count($request->ur_url) != count($request->ur_type_id))
                return json_encode(["status" => "422", "msg" => " فایل آپلودی خود را انتخاب نمایید"]);
            for ($i = 0; $i < count($request->ur_url); $i++) {
                $route = uploadFile($request->file('ur_url')[$i], 'collection', $create_user->user_id);
                $person = Person::find($create_person->prsn_id);
                UploadRoute::create([
                    'ur_path' => $route,
                    'ur_fk_id' => $create_user->user_id,
                    'ur_collection_id' => $person->collections()->first()->toArray()['coll_id'],
                    'ur_description' => $request->ur_description[$i],
                    'ur_type_id' => $request->ur_type_id[$i],
                    'ur_table_name' => 'collections',
                ]);
            }

            return json_encode(['status' => '100', 'msg' => 'اطلاعات با موفقیت ثبت شد']);
        }
        $roles = Role::where('rol_creator', user('user_id'))->get()->toArray();

        return view('agents.form', compact('company','collections','upload_find','roles'));

    }

    public function listAgents()
    {
        $result = DB::table('users')
            ->select([
                'user_id',
                'username',
                'prsn_name',
                'prsn_family',
                'ag_domain',
                'created_at',
                'is_active',
            ])
            ->where('panel_type', 46);

        if (\request()->has('date_from') && \request('date_from') != "")
            $result->where('created_at', '>=', date("Y-m-d H:i:s", jalali_to_timestamp(\request('date_from'))));

        if (\request()->has('status') && \request('status') != "")
            $result->where("is_active", "like", '%' . \request('status') . '%');

        if (\request()->has('username') && \request('username') != "")
            $result->where("username", "like", '%' . \request('username') . '%');

        $result = $result->leftJoin('persons', 'prsn_user_id', 'user_id');

        if (\request()->has('name') && \request('name') != "")
            $result->where("prsn_name", "like", '%' . \request('name') . '%');

        $result = $result->leftJoin('agents', 'ag_id', 'agent_id');

        if (\request()->has('domain') && \request('domain') != "")
            $result->where("ag_domain", "like", '%' . \request('domain') . '%');

        $result = $result->paginate(20);
        return showData(view('agents.index', compact('result')));
    }

    public function editAgent(Request $request, $id)
    {
        $company = Baseinfo::where('bas_type', 'company-type')
            ->where('bas_parent_id', '!=', '0')
            ->get();
        $collections = DB::table('users')
            ->where('user_id', $id)
            ->leftJoin('persons', 'prsn_user_id', 'user_id')
            ->leftJoin('collection_persons', 'cp_prsn_id', 'prsn_id')
            ->leftJoin('collections', 'coll_id', 'cp_coll_id')
            ->first();
        $person = DB::table('persons')->where('prsn_user_id', $id)->first();

        $upload_find = DB::table('upload_route')
            ->select([
                'ur_id',
                'ur_path',
                'ur_description',
                'ur_table_name',
                'created_at',
                'bas_id',
                'bas_value',
            ])
            ->where('ur_collection_id', $collections->coll_id)
            ->where('ur_table_name', 'collections')
            ->leftJoin('baseinfos', 'bas_id', 'ur_type_id')
            ->get();
        $domain = DB::table('users')
            ->where('user_id', $id)
            ->leftJoin('agents', 'ag_id', 'agent_id')
            ->select([
                'ag_domain'
            ])->first();

        if ($request->isMethod('post')) {

            $request->validate([
                'persons.prsn_name' => ['required'],
                'persons.prsn_family' => ['required'],
                'persons.prsn_national_code' => ['required', 'max:10', 'regex:/([0-9]+){10}/'],
                'persons.prsn_father_name' => ['required'],
                'persons.prsn_birthday' => ["required", "regex:/^[1-4]\d{3}\/((0[1-6]\/((3[0-1])|([1-2][0-9])|(0[1-9])))|((1[0-2]|(0[7-9]))\/(30|([1-2][0-9])|(0[1-9]))))$/"],
                'persons.prsn_certificate_code' => ['required', 'regex:/([0-9]+)/'],
                'persons.prsn_serial_certificate_code' => ['required', 'regex:/([0-9]+)/'],
                'persons.prsn_phone1' => ['required', 'regex:/([0-9]+){11}/'],
                'persons.prsn_mobile1' => ['required', 'regex:/([0-9]+){11}/'],
                'collections.coll_email' => ['required', 'email'],
                'persons.prsn_gender_id' => ['required'],
                'persons.prsn_post_code' => ['required', 'max:10', 'regex:/([0-9]+){10}/'],
                'persons.prsn_address' => ['required'],
                'collections.coll_name' => ['required'],
//                'collections.coll_type_class_id' => ['required'],
                'collections.coll_state_id' => ['required'],
                'collections.coll_city_id' => ['required'],
                'collections.coll_post_code' => ['required', 'max:10', 'regex:/([0-9]+){10}/'],
                'collections.domain' => ['required'],
                'collections.username' => ['required', 'unique_string:users,username'],
//                'collections.password' => ['required'],
                'collections.coll_address' => ['required'],
                'collections.role' => function ($attribute, $value, $fail) {
                    if ($value == "") {
                        $fail('لطفا یک نقش را انتخاب کنید.');
                    }
                }

            ]);

            $create_user = User::find($id);
            $create_user->username = $request->collections['username'];
//            $create_user->password = Hash::make($request->collections['password']);
            $create_user->email = $request->collections['coll_email'];
            $create_user->created_at = date('Y-m-d');
            $create_user->creator = user('user_id');
            $create_user->save();

            $find_person = Person::where('prsn_user_id', $id)->first('prsn_id');
//            dd($find_person->prsn_id);
            $create_person = Person::find($find_person->prsn_id);
            $create_person->prsn_name = $request->persons['prsn_name'];
            $create_person->prsn_family = $request->persons['prsn_family'];
            $create_person->prsn_gender_id = $request->persons['prsn_gender_id'];
            $create_person->prsn_father_name = $request->persons['prsn_father_name'];
            $create_person->prsn_birthday = $request->persons['prsn_birthday'];
            $create_person->prsn_national_code = $request->persons['prsn_national_code'];
            $create_person->prsn_certificate_code = $request->persons['prsn_certificate_code'];
            $create_person->prsn_serial_certificate_code = $request->persons['prsn_serial_certificate_code'];
            $create_person->prsn_mobile1 = $request->persons['prsn_mobile1'];
            $create_person->prsn_phone1 = $request->persons['prsn_phone1'];
            $create_person->prsn_post_code = $request->persons['prsn_post_code'];
            $create_person->prsn_address = $request->persons['prsn_address'];
            $create_person->save();

            $create_coll = Collection::find($collections->coll_id);
            $create_coll->coll_name = $request->collections['coll_name'];
            $create_coll->coll_economic_code = $request->collections['coll_economic_code'];
            $create_coll->coll_national_number = $request->collections['coll_national_number'];
            $create_coll->coll_is_deleted = 0;
            $create_coll->coll_post_code = $request->collections['coll_post_code'];
            $create_coll->coll_address = $request->collections['coll_address'];
            $create_coll->coll_state_id = $request->collections['coll_state_id'];
            $create_coll->coll_city_id = $request->collections['coll_city_id'];
            $create_coll->save();

            DB::table('agents')
                ->where('ag_id', $create_user->agent_id)
                ->update([
                    'ag_domain' => $request->collections['domain']
                ]);

            DB::table('user_roles')
                ->where('ur_user_id',$create_user->user_id)
                ->update([
                    'ur_role_id'=>$request->collections['role'],
                ]);

            if (!$request->has('ur_url') || count($request->ur_url) != count($request->ur_type_id)) {
            } else {
                for ($i = 0; $i < count($request->ur_url); $i++) {
                    $route = uploadFile($request->file('ur_url')[$i], 'collection', $create_user->user_id);
                    $person = Person::find($create_person->prsn_id);
                    UploadRoute::create([
                        'ur_path' => $route,
                        'ur_fk_id' => $create_user->user_id,
                        'ur_collection_id' => $person->collections()->first()->toArray()['coll_id'],
                        'ur_description' => $request->ur_description[$i],
                        'ur_type_id' => $request->ur_type_id[$i],
                        'ur_table_name' => 'collections',
                    ]);
                }
            }

            return json_encode(['status' => '100', 'msg' => 'اطلاعات با موفقیت ثبت شد']);
        }


        return view('agents.form', compact('company', 'collections', 'person','upload_find', 'domain'));

    }

    public function delete_agentUpload($id)
    {
        UploadRoute::find($id)->delete();
        return json_encode(['status' => '100', 'msg' => 'با موفقیت حذف گردید']);
    }

    public function change_agentPass(Request $request)
    {
        $pass=User::find($request->id);
        $pass->password=Hash::make($request->password);
        if ($pass->save())
            return json_encode(['status' => '100', 'msg' => 'با موفقیت رمز عبور تغییر گردید']);
            return json_encode(['status' => '500', 'msg' => 'خطا در ثبت اطلاعات']);
    }
}
