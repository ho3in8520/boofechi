<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use MongoDB\Driver\Session;

class siteController extends Controller
{
    /*public function shopkeeper_dashboard()
    {
        return view('site.dashboard-shopkeeper');
    }*/

    public function paymentVerify()
    {
        $message = "";
        try {
            \Gateway::verify();
            $gateway = DB::table('gateway_transact')->where('transact_id', request('transaction_id'))->first();
            if ($gateway->transact_status == 2) {
                /* if ($gateway->transact_type == 1)//collection_factor
                 {
                     DB::table('factor_master')->where('fm_id', $gateway->transact_fk_id)->update([
                         'fm_is_payed' => 1
                     ]);
                 } else if ($gateway->transact_type == 2)//system_factor
                 {
                     DB::table('system_factor_master')->where('sfm_id', $gateway->transact_fk_id)->update([
                         'sfm_is_payed' => 1
                     ]);
                 } else */
                if ($gateway->transact_type == 3)//charge
                {
                    DB::table('users')->where('user_id', $gateway->transact_fk_id)->update([
                        'amount' => DB::raw('amount + ' . $gateway->transact_amount)
                    ]);
                    DB::table('bag_transacts')->insert([
                        'bt_user_id' => $gateway->transact_fk_id,
                        'bt_time' => time(),
                        'bt_status' => 1, //increase
                        'bt_type' => 2,
                        'bt_description' => 'پرداخت آنلاین',
                        'bt_amount' => $gateway->transact_amount
                    ]);
                }
            }
            $message = "پرداخت با موفقیت انجام شد!";
            $status = "success";
            if ($gateway->transact_temp != 0)
                return redirect()->route('factor-payment', ['id' => $gateway->transact_temp]);
            return showData(view('site.payment-details', compact('gateway', 'message', 'status')));
            // تراکنش با موفقیت سمت بانک تایید گردید
            // در این مرحله عملیات خرید کاربر را تکمیل میکنیم

        } catch (\App\Gateway\Exceptions\RetryException $e) {
            $gateway = DB::table('gateway_transact')->where('transact_id', request('transaction_id'))->first();
            if ($gateway->transact_status == 2) {
                $status = "success";
                $message = 'تراکنش با موفقیت انجام شد!';
            } else {
                $status = "danger";
                $message = 'خطا در انجام تراکنش';
            }
            if ($gateway->transact_temp != 0)
                return redirect()->route('factor-payment', ['id' => $gateway->transact_temp]);
            return showData(view('site.payment-details', compact('gateway', 'message', 'status')));

        } catch (\Exception $e) {
            //$gateway = DB::table('gateway_transact')->where('transact_id',request('transaction_id'))->first();
            $status = "danger";
            $message = $e->getMessage();
            $gateway = DB::table('gateway_transact')->where('transact_id', request('transaction_id'))->first();
            if ($gateway->transact_temp != 0)
                return redirect()->route('factor-payment', ['id' => $gateway->transact_temp]);
            return showData(view('site.payment-details', compact('gateway', 'message', 'status')));
        }

    }
}
