<?php

namespace App\Http\Controllers;

use App\AreaSupport;
use App\Baseinfo;
use App\Collection;
use App\UploadRoute;
use App\CollectionCategories;
use App\Component\Tools;
use App\Http\Requests\CompanyRequest;
use App\Http\Requests\CreateCollectionUserRequest;
use App\Person;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = Tools::getList($request, 42);
        return showData(view('companies.index', compact('data')));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('companies.form');
    }

    public function saveCreateUser(CreateCollectionUserRequest $request)
    {

    }

    public function manageCategories(Request $request, $id)
    {
        $collection_id=$id;
        $collection = Collection::findOrFail($id);
//        $collection_id = user()->person->collections->first()->coll_id;
        $selected = DB::table('categories')
            ->select([
                'cat_id'
            ])
            ->join("collection_categories", "cc_category_id", "=", "cat_id")
            ->where("cc_collection_id", $id)->get()->toArray();
        $selected = array_column(json_decode(json_encode($selected), true), 'cat_id');
        if (request()->method() == "POST") {
            $ids = request()->get('categories');
            if (($key = array_search(0, $ids)) !== false) {//delete onsore 0
                unset($ids[$key]);
            }
            DB::delete("delete from collection_categories where cc_collection_id = :id", [":id" => $id]);
            foreach ($ids as $city_id) {
                $model = new CollectionCategories();
                $model->cc_collection_id = $id;
                $model->cc_category_id = $city_id;
                $model->save();
            };
            return json_encode(["status" => 100, 'msg' => 'دسته های انتخاب شده با موفقیت ثبت شد!', 'url' => url("/upload_route/create") . "?prsn_id=" . $collection->persons->first()->prsn_id]);
        }
        $url = "area-support/" . $id;
        return view('companies.manage_categories', compact('selected', 'url','collection_id'));
    }

    public function area(Request $request, $id)
    {
        $collection = Collection::findOrFail($id);

        $this->validate(request(), [
            'selected_cities' => function ($attribute, $value, $fail) {
                if (!request()->has('selected_cities')) {
                    $fail('شهرهای انتخاب شده الزامی است');
                }
            }
        ]);

        $first_user_id_in_collection = DB::table('users')
            ->join('persons', 'prsn_user_id', '=', 'user_id')
            ->join('collection_persons', 'prsn_id', '=', 'cp_prsn_id')
            ->join('collections', 'cp_coll_id', '=', 'coll_id')
            ->where('coll_id', $id)
            ->first()
            ->user_id;
        if (request()->method() == "POST") {
            $request->validate([
                'selected_cities' => function ($attribute, $value, $fail) {
                    if (!request()->has('selected_cities') || \request('selected_cities') == '') {
                        $fail('هیچ شهری انتخاب نشده است.');
                    }
                }
            ]);
            $ids = explode(',', request()->get('selected_cities'));
            AreaSupport::where("as_collection_id", $id)->where("as_user_id", $first_user_id_in_collection)->delete();
            foreach ($ids as $city_id) {
                $model = new AreaSupport();
                $model->as_collection_id = $id;
                $model->as_city_id = $city_id;
                $model->as_user_id = $first_user_id_in_collection;
                $model->save();
            };
            return json_encode(['status' => 100, 'msg' => 'نقاط تحت پوشش با موفقیت ذخیره شد!', 'url' => url("/company/manage-categories") . "/" . $id]);
        }
        $areas = DB::table('area_support')
            ->select([
                "cities.c_id",
                "cities.c_name",
                "parent.c_name as parent"
            ])
            ->join("cities", "c_id", "=", "as_city_id")
            ->join("cities as parent", "parent.c_id", "=", "cities.c_parent_id")
            ->where("as_collection_id", $id)
            ->where("as_user_id", $first_user_id_in_collection)
            ->get()
            ->toArray();
        $arr = json_decode(json_encode($areas), true);
        $temp = [];
        foreach ($arr as $row) {
            $temp[$row['parent']][] = [
                "id" => $row['c_id'],
                "name" => $row['c_name']
            ];
        }
        foreach ($temp as $key => $value) {
            $temp[$key] = [
                'text' => array_column($value, 'name'),
                'ids' => array_column($value, 'id'),
            ];
        }
        $temp = empty($temp) ? "{}" : json_encode($temp, JSON_UNESCAPED_UNICODE);
        $url = "/user/" . $collection->persons()->first()->prsn_user_id . "/edit";
        return view('collection.area_support', compact('temp', 'url'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyRequest $request)
    {
        $result = Tools::collection_store($request, 42);
        return redirect('/user/create?prsn_id=' . $result);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param object $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Collection $company)
    {
        $collections = $company;
        $persons = $company->persons->first();
        $logo = UploadRoute::where('ur_collection_id', $collections->coll_id)->where('ur_table_name', 'logo')->orderBy('ur_id', 'DESC')->first();
        $collectionClass = DB::table('collection_classes')->whereCocCollId($company->coll_id)->pluck('coc_typeClass_id')->toArray();
        return view('companies.form', compact('collections', 'persons', 'collectionClass', 'logo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param object $company
     * @return \Illuminate\Http\Response
     */
    public function update(CompanyRequest $request, Collection $company)
    {
        $result = Tools::collection_update($request, $company);
        $user=DB::table('persons')
            ->where('prsn_id',$request->prsn_id)
            ->first()->prsn_user_id;
        if (getDefaultRoleFactorReceiver($user)==true){
            DB::table('user_roles')
                ->insert([
                    'ur_rol_id' => getDefaultFactorReceiver(),
                    'ur_user_id' => $user
                ]);
        }
        $person = $company->persons->first();
        if ($person->prsn_user_id == 1)
            return redirect('/user/create?prsn_id=' . $person->prsn_id);
        return redirect('/user/' . $person->prsn_user_id . '/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
