<?php

namespace App\Http\Controllers\Admin;

use App\Baseinfo;
use App\Component\Tools;
use App\Http\Controllers\Controller;
use App\Http\Requests\BasicInfoRequest;
use App\Permission;
use App\Setting;
use App\Terms;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class SettingController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:manage_registration', ['only' => ['acceptRegistration', 'acceptForm', 'deacceptForm', 'getForm']]);
        $this->middleware('permission:menu_setting', ['only' => ['menuSetting', 'saveMenu']]);
        $this->middleware('permission:manage_baseinfos', ['only' => ['basicInfo']]);
        $this->middleware('permission:manage_terms', ['only' => ['create_terms']]);
        $this->middleware('permission:manage_avatars', ['only' => ['acceptUploadedFiles', 'acceptFile', 'deacceptFile']]);
        $this->middleware('permission:admin-setting', ['only' => ['systemSetting']]);
        $this->middleware('permission:mange_menus', ['only' => ['menuSetting']]);
    }

    public function acceptRegistration()
    {
        $registrations = DB::table('panel_wizard_completed')->select([
            DB::raw("concat(prsn_name,' ',prsn_family,'(',username,')') as name"),
            'panel_wizard_completed.created_at',
            'pwc_reason',
            'pwc_accepted',
            'pw_form_address',
            'pwc_id',
            'bas_value',
            'user_id'
        ])
            ->join("panel_wizards", 'pw_id', '=', 'pwc_form_id')
            ->join("baseinfos", 'bas_id', '=', 'pw_panel_type_id')
            ->join("users", 'user_id', '=', 'pwc_user_id')
            ->join("persons", 'prsn_user_id', '=', 'user_id');

        if (request()->has('panel_type') && request('panel_type') != 0)
            $registrations->where("pw_panel_type_id", request('panel_type'));

        if (request()->has('user') && request('user') != "") {
            $user = request('user');
            $registrations->where(function ($query) use ($user) {
                return $query->where('prsn_name', 'like', '%' . $user . '%')->orWhere('prsn_family', 'like', '%' . $user . '%');
            });
        }
        if (request()->has('form') && request('form') != "0") {
            $registrations->where('pw_form_address', request('form'));
        }
        if (request()->has('status') && request('status') != 0) {
            $status_array = [1 => 0, 2 => 1, 3 => 2];
            $registrations->where('pwc_accepted', $status_array[request('status')]);
        }


        $registrations = $registrations
            ->orderBy("user_id", "desc")
//            ->orderBy("panel_wizard_completed.created_at", "desc")
            ->orderBy("pw_id", "asc")
            ->paginate(20);
        return showData(view('admin.setting.registrations', compact('registrations')));
    }

    public function acceptForm()
    {
        $form = DB::table('panel_wizard_completed')->where('pwc_id', request()->get('id'))->first();
        if (!$form)
            abort(404);
        if (DB::table("panel_wizard_completed")->where("pwc_id", $form->pwc_id)->update(['pwc_accepted' => 1])) {
            $panel_type = findUser($form->pwc_user_id, "panel_type", "id");
            $count_forms = DB::table("panel_wizards")->where('pw_panel_type_id', $panel_type)->get()->count();
            $count_form_accepted = DB::table("panel_wizard_completed")
                ->where('pwc_accepted', 1)
                ->where('pwc_user_id', $form->pwc_user_id)
                ->get()->count();
            if ($count_forms == $count_form_accepted) {
                DB::table("users")->where("user_id", $form->pwc_user_id)->update(
                    [
                        'profile_complete' => 1,
                    ]
                );
            }
            return json_encode(['status' => 100, 'msg' => "با موفقیت انجام شد!"]);
        }
        return json_encode(['status' => 500, 'msg' => "عملیات با خطا مواجه شد، مججدا تلاش فرمائید"]);
    }

    public function deacceptForm()
    {
        $form = DB::table('panel_wizard_completed')->where('pwc_id', request()->get('id'))->first();
        if (!$form)
            abort(404);
        if (DB::table("panel_wizard_completed")->where("pwc_id", $form->pwc_id)->update(
            [
                'pwc_accepted' => 2,
                'pwc_reason' => request()->get('reason')
            ]
        )
        ) {
            DB::table("users")->where("user_id", $form->pwc_user_id)->update(
                [
                    'profile_complete' => 0,
                ]
            );
            return json_encode(['status' => 100, 'msg' => "با موفقیت انجام شد!"]);
        }
        return json_encode(['status' => 500, 'msg' => "عملیات با خطا مواجه شد، مججدا تلاش فرمائید"]);
    }

    public function getForm()
    {
        $form = DB::table('panel_wizard_completed')
            ->select([
                'pwc_user_id',
                "pw_form_address",
                'pw_panel_type_id'
            ])
            ->join("panel_wizards", 'pw_id', '=', 'pwc_form_id')
            ->where('pwc_id', \request()->get('id'))
            ->first();
        if (!$form)
            abort(404);

        switch ($form->pw_form_address) {
            case "authentication":
                $oldForm = DB::table('users')
                    ->select([
                        'reagent_id',
                        'persons.*'
                    ])
                    ->where('user_id',$form->pwc_user_id)
                    ->leftJoin('persons','prsn_user_id', 'user_id')->first();
                return showData(view('collection.authentication-form', compact('oldForm')));
                break;
            case "business-form":
                $collection_id = DB::table("persons")->select("cp_coll_id")
                    ->join("collection_persons", 'cp_prsn_id', '=', 'prsn_id')
                    ->where("prsn_user_id", $form->pwc_user_id)
                    ->first()->cp_coll_id;
                $types = json_decode(json_encode(DB::table('baseinfos')
                    ->select([
                        'bas_id',
                        'bas_value'
                    ])
                    ->where('bas_parent_id', 19)
                    ->where('bas_id', '!=', 1)
                    ->get()->toArray()), true);
                $typeCollections = \App\Baseinfo::whereBasType('type_class')->where('bas_parent_id', '<>', '0')->get();
                if ($form->pw_panel_type_id == 44) //shopkeeper
                {
                    $old_senfs = array_column(json_decode(json_encode(DB::table('collection_classes')->select([
                        'coc_typeClass_id'
                    ])
                        ->where('coc_coll_id', $collection_id)
                        ->get()), true), 'coc_typeClass_id');

                    $shopKepper = DB::table('persons')
                        ->select([
                            'prsn_id',
                            'coll_name',
                            'coll_address',
                            'prsn_job',
                            'coll_state_id',
                            'coll_city_id'
                        ])
                        ->join('collection_persons', 'cp_prsn_id', '=', 'prsn_id')
                        ->join('collections', 'coll_id', '=', 'cp_coll_id')
                        ->where('prsn_user_id', $form->pwc_user_id)
                        ->first();
                    return showData(view('collection.business_shopkeeper_form', compact('types', 'typeCollections', 'old_senfs', 'shopKepper')));
                } else {
                    $old_senfs = array_column(json_decode(json_encode(DB::table('collection_classes')->select([
                        'coc_typeClass_id'
                    ])
                        ->where('coc_coll_id', $collection_id)
                        ->get()), true), 'coc_typeClass_id');

                    $visitor = DB::table('persons')
                        ->select([
                            'prsn_id',
                            'coll_name',
                            'coll_address',
                            'prsn_job',
                            'coll_state_id',
                            'coll_city_id'
                        ])
                        ->join('collection_persons', 'cp_prsn_id', '=', 'prsn_id')
                        ->join('collections', 'coll_id', '=', 'cp_coll_id')
                        ->where('prsn_user_id', $form->pwc_user_id)
                        ->first();
                    return showData(view('collection.business_visitor_form', compact('types', 'visitor', 'old_senfs', 'typeCollections')));
                }
                break;
            case "upload_route":
                $files = DB::table("upload_route")
                    ->select([
                        'upload_route.*',
                        DB::raw("concat(prsn_name,' ',prsn_family) as name"),
                        'bas_value',
                        'ur_description'
                    ])
                    ->leftJoin('users', 'user_id', '=', 'ur_fk_id')
                    ->leftJoin('persons', 'user_id', '=', 'prsn_user_id')
                    ->leftJoin('baseinfos', 'bas_id', '=', 'ur_type_id')
                    ->where("ur_table_name", 'collections')
                    ->where('ur_fk_id', $form->pwc_user_id)
                    ->latest('ur_id')
                    ->paginate(20);

                return showData(view('admin.setting.register_uploaded_files', compact('files')));
                break;
        }
    }

    public function menuSetting()
    {
        $menus = DB::table("permissions")->select([
            "perm_parent_id as parent_id",
            "perm_id as id",
            "perm_name as name",
            "perm_link as href",
            "perm_label as title",
            "perm_label as text",
            "perm_icon as icon",
            DB::raw("'_self' as target"),
        ])->whereNotNull("perm_link")->where("perm_link", "!=", "")->get()->toArray();
        $menus = json_decode(json_encode($menus), true);
        $menus = convertToHierarchy($menus);

        return view('admin.setting.menu', compact('menus'));

    }

    public function acceptUploadedFiles()
    {
        $files = DB::table("upload_route")
            ->select([
                'upload_route.*',
                DB::raw("concat(prsn_name,' ',prsn_family) as name")
            ])
            ->leftJoin('users', 'user_id', '=', 'ur_fk_id')
            ->leftJoin('persons', 'user_id', '=', 'prsn_user_id')
            ->where("ur_table_name", 'avatar')
            ->latest('ur_id')
            ->paginate(20);
        return view('admin.setting.uploaded_files', compact('files'));
    }

    public function saveMenu()
    {
        $path = '../menu-config/';
        @mkdir($path, $mode = 0777);
        file_put_contents($path . "menu.json", \request('mnujson'));
        return json_encode(["status" => 100]);
    }

    public function basicInfo(Request $request, $id = null)
    {
        $types = json_decode(json_encode(DB::table('baseinfos')
            ->select([
                'bas_id',
                'bas_value',
                'bas_type'
            ])
            ->where('bas_parent_id', 0)
            ->where('bas_id', '!=', 1)
            ->get()->toArray()), true);
        $baseinfos = DB::table('baseinfos as b')
            ->select([
                'b.bas_id',
                'b.bas_status',
                'b.bas_value',
                DB::raw('(case when p.bas_value is null then \'-\' else p.bas_value end) as parent')
            ])
            ->leftJoin('baseinfos as p', 'b.bas_parent_id', '=', 'p.bas_id')
            ->where('b.bas_parent_id', '!=', 0)
            ->orderBy('b.bas_type')
            ->paginate(20);
        if (is_null($id)) {
            //is create
            $old_base = null;
            $category = null;
            if ($request->isMethod('post')) {
                $request->validate([

                    'BasicInfo.title' => ['required', 'max:200'],
                    'BasicInfo.category' => ['required', 'exists:baseinfos,bas_id'],

                ]);
                //insert
                $findb_type = DB::table('baseinfos')->where("bas_id", $request->BasicInfo['category'])->select('bas_type')->first();
                $create_base = new Baseinfo();
                $create_base->bas_parent_id = $request->BasicInfo['category'];
                $create_base->bas_value = $request->BasicInfo['title'];
                $create_base->bas_type = $findb_type->bas_type;
                $create_base->bas_status = $request->BasicInfo['status'];
                $create_base->bas_extra_value = '0';
                $create_base->bas_extra_value1 = '0';
                $create_base->bas_can_user_add = '0';
                if ($create_base->save()) {
                    return json_encode(['status' => 100, "msg" => "با موفقیت ثبت شد."]);
                } else {
                    return json_encode(['status' => 500, "msg" => "خطا در ثبت"]);
                }
            }
            return view('admin.setting.basicinfo', compact('types', 'baseinfos', 'old_base', 'category'));
        } else {
            //is update
            $old_base = Baseinfo::where("bas_id", $id)->first();
            $category = json_decode(json_encode(DB::table('baseinfos')
                ->select(
                    'bas_id',
                    'bas_value'

                )
                ->where('bas_id', $old_base->bas_parent_id)
                ->first()), true);
//
            if (!$old_base)
                abort(404);
            if ($request->isMethod('patch')) {
                $request->validate([

                    'BasicInfo.title' => ['required', 'max:200'],
                    'BasicInfo.category' => ['required', 'max:200'],

                ]);
                $findb_type = DB::table('baseinfos')->where("bas_id", $request->BasicInfo['category'])->select('bas_type')->first();
                $update_base = Baseinfo::find($id);
                $update_base->bas_parent_id = $request->BasicInfo['category'];
                $update_base->bas_value = $request->BasicInfo['title'];
                $update_base->bas_status = $request->BasicInfo['status'];
                $update_base->bas_type = $findb_type->bas_type;
                if ($update_base->save()) {
                    return json_encode(['status' => 100, "msg" => "با موفقیت ثبت شد."]);
                } else {
                    return json_encode(['status' => 500, "msg" => "خطا در ثبت"]);
                }
                //update
            }
            return view('admin.setting.basicinfo', compact('types', 'old_base', 'baseinfos', 'category'));
        }

    }

    public function delete_basicInfo($id)
    {
        $result = checkRelation('baseinfos', $id);
        if (empty($result) || $result[0]['ID'] == 0) {
            DB::beginTransaction();
            try {
                DB::delete('delete from baseinfos where bas_id = ?', [$id]);
                DB::commit();
                return redirect()->back()->with("success","عملیات حذف با موفقیت انجام شد!");
            } catch (\Exception $e) {
                DB::rollback();
            }
        }
        return json_encode(['status' => 101, 'msg' => config('first_config.message.relation_delete')]);
    }

    public function create_terms(Request $request, $id = null)
    {
        $bas_info = json_decode(json_encode(DB::table('baseinfos')
            ->select([
                'bas_id',
                'bas_value',
            ])
            ->where('bas_type', 'panel_type')
            ->whereNotIn('bas_id', [39])
            ->get()->toArray()), true);

        $olds = Terms::where('pt_id', $id)->first();
        $fin_terms = DB::table('panel_terms as b')
            ->leftJoin('baseinfos as p', 'b.pt_panel_id', 'p.bas_id')
            ->select([
                'p.bas_value',
                'b.pt_id'
            ])->get();

        if ($request->isMethod('post')) {
            $request->validate([
//                'panel_type' => ['required', 'exists:baseinfos,bas_id'],
            ]);
            $result = Terms::where('pt_panel_id', $request->panel_type)->first();
            if ($result) {//update
                if ($request->forced == true) {
                    DB::table('users')
                        ->where('panel_type', $request->panel_type)
                        ->update([
                            'accept_terms' => 0
                        ]);
                }
                $update = Terms::find($result->pt_id);
                $update->pt_terms = $request->terms;
                if ($update->save()) {
                    return json_encode(['status' => 100, 'msg' => 'اطلاعات با موفقیت ویرایش گردید']);
                } else {
                    return json_encode(['status' => 500, 'msg' => 'خطا در ثبت اطلاعات']);
                }
            } else {//insert
                if ($request->forced == true) {
                    DB::table('users')
                        ->where('panel_type', $request->panel_type)
                        ->update([
                            'accept_terms' => 0
                        ]);
                }
                $create = new Terms();
                $create->pt_panel_id = $request->panel_type;
                $create->pt_terms = $request->terms;
                if ($create->save()) {
                    return json_encode(['status' => 100, 'msg' => 'اطلاعات با موفقیت ثبت گردید']);
                } else {
                    return json_encode(['status' => 500, 'msg' => 'خطا در ثبت اطلاعات']);
                }
            }
        }

        if ($request->isMethod('delete')) {
            Terms::where('pt_id', $id)->delete();
            return json_encode(['status' => 100, 'msg' => 'با موفقیت حذف گردید']);
        }

        return view('admin.setting.create_terms', compact('bas_info', 'fin_terms', 'olds'));
    }

    public function acceptFile()
    {
        $file = DB::table('upload_route')->where('ur_id', request()->get('id'))->first();
        if (!$file)
            abort(404);
        if (DB::table("upload_route")->where("ur_id", $file->ur_id)->update(['ur_status' => 1]))
            return json_encode(['status' => 100, 'msg' => "با موفقیت انجام شد!"]);
        return json_encode(['status' => 500, 'msg' => "عملیات با خطا مواجه شد، مججدا تلاش فرمائید"]);
    }

    public function deacceptFile()
    {
        $file = DB::table('upload_route')->where('ur_id', request()->get('id'))->first();
        if (!$file)
            abort(404);
        if (DB::table("upload_route")->where("ur_id", $file->ur_id)->update(['ur_status' => 0]))
            return json_encode(['status' => 100, 'msg' => "با موفقیت انجام شد!"]);
        return json_encode(['status' => 500, 'msg' => "عملیات با خطا مواجه شد، مججدا تلاش فرمائید"]);
    }

    public function systemSetting(Request $request)
    {
        $financial_unit = setting('financial_unit');
        $tax = setting('tax');
        $basket_life_time = setting('basket_life_time');
        $purchase_for_subscription_life_time = setting('purchase_for_subscription_life_time');
        $sms_sendPassword = setting('sms_sendPassword');
        $sms_welcome = setting('sms_welcome');
        $sms_forgetPassword = setting('sms_forgetPassword');
        $sms_factorBuy = setting('sms_factorBuy');
        $sms_visitorBuy = setting('sms_visitorBuy');
        $sms_cancel_factor = setting('sms_cancel_factor');
        $email_sendPassword = setting('email_sendPassword');
        $email_welcome = setting('email_welcome');
        $email_forgetPassword = setting('email_forgetPassword');
        $email_factorBuy = setting('email_factorBuy');
        $forge_calc_porsant = setting('forge_calc_porsant');
        $send_sms = setting('send_sms');
        $send_email = setting('send_email');
        $expire_time_visitor = setting('otp_expire_time');
        $checkout_request = setting('checkout_permitted_amout');
        $sms_changeMobile = setting('sms_changeMobile');
        $email_changeEmail = setting('email_changeEmail');

        $old_role = DB::table('setting')
            ->select([
                'rol_id',
            ])
            ->where('st_collection_id', 0)
            ->where('st_user_id', 0)
            ->where('st_key', 'factor_receiver')
            ->leftJoin('roles','rol_id','st_value')
            ->first()->rol_id;
        $financial = Baseinfo::where('bas_type', 'financial_unit')
            ->where('bas_parent_id', '!=', 0)->get();
        $role = json_decode(json_encode(DB::table('roles')
            ->select([
                'rol_id',
                'rol_label',
            ])
            ->where('rol_creator', user('user_id'))
            ->get()->toArray()), true);
        if ($request->isMethod('post')) {
            $request->validate([
                'sms.*' => ['required'],
                'email.*' => ['required'],
                'timeFactor' => ['required'],
                'timeFactorVisitor' => ['required'],
                'financial' => ['required'],
                'taxes' => ['required'],
                'forgeCalcPorsant' => ['required'],
                'role_factor_receiver' => ['required'],
                'expireTimeVisitor' => ['required'],
            ]);
            if ($financial_unit == null) {//mode insert
                DB::table('setting')
                    ->insert([
                        'st_key' => 'financial_unit',
                        'st_value' => $request->financial,
                        'st_collection_id' => 0,
                        'st_user_id' => 0
                    ]);
            } else {//mode update
                DB::table('setting')
                    ->where('st_key', 'financial_unit')
                    ->update([
                        'st_value' => $request->financial,
                    ]);
            }

            if ($tax == null) {//mode insert
                DB::table('setting')
                    ->insert([
                        'st_key' => 'tax',
                        'st_value' => $request->taxes,
                        'st_collection_id' => 0,
                        'st_user_id' => 0
                    ]);
            } else {//mode update
                DB::table('setting')
                    ->where('st_key', 'tax')
                    ->update([
                        'st_value' => $request->taxes,
                    ]);
            }

            if ($basket_life_time == null) {//mode insert
                DB::table('setting')
                    ->insert([
                        'st_key' => 'basket_life_time',
                        'st_value' => $request->timeFactor,
                        'st_collection_id' => 0,
                        'st_user_id' => 0
                    ]);
            } else {//mode update
                DB::table('setting')
                    ->where('st_key', 'basket_life_time')
                    ->update([
                        'st_value' => $request->timeFactor,
                    ]);
            }

            if ($purchase_for_subscription_life_time == null) {//mode insert
                DB::table('setting')
                    ->insert([
                        'st_key' => 'purchase_for_subscription_life_time',
                        'st_value' => $request->timeFactorVisitor,
                        'st_collection_id' => 0,
                        'st_user_id' => 0
                    ]);
            } else {//mode update
                DB::table('setting')
                    ->where('st_key', 'purchase_for_subscription_life_time')
                    ->update([
                        'st_value' => $request->timeFactorVisitor,
                    ]);
            }

            if ($sms_sendPassword == null) {//mode insert
                DB::table('setting')
                    ->insert([
                        'st_key' => 'sms_sendPassword',
                        'st_value' => $request->sms['sendPassword'],
                        'st_collection_id' => 0,
                        'st_user_id' => 0
                    ]);
            } else {//mode update
                DB::table('setting')
                    ->where('st_key', 'sms_sendPassword')
                    ->update([
                        'st_value' => $request->sms['sendPassword'],
                    ]);
            }

            if ($sms_welcome == null) {//mode insert
                DB::table('setting')
                    ->insert([
                        'st_key' => 'sms_welcome',
                        'st_value' => $request->sms['welcome'],
                        'st_collection_id' => 0,
                        'st_user_id' => 0
                    ]);
            } else {//mode update
                DB::table('setting')
                    ->where('st_key', 'sms_welcome')
                    ->update([
                        'st_value' => $request->sms['welcome'],
                    ]);
            }

            if ($sms_forgetPassword == null) {//mode insert
                DB::table('setting')
                    ->insert([
                        'st_key' => 'sms_forgetPassword',
                        'st_value' => $request->sms['forgetPassword'],
                        'st_collection_id' => 0,
                        'st_user_id' => 0
                    ]);
            } else {//mode update
                DB::table('setting')
                    ->where('st_key', 'sms_forgetPassword')
                    ->update([
                        'st_value' => $request->sms['forgetPassword'],
                    ]);
            }

            if ($sms_factorBuy == null) {//mode insert
                DB::table('setting')
                    ->insert([
                        'st_key' => 'sms_factorBuy',
                        'st_value' => $request->sms['factorBuy'],
                        'st_collection_id' => 0,
                        'st_user_id' => 0
                    ]);
            } else {//mode update
                DB::table('setting')
                    ->where('st_key', 'sms_factorBuy')
                    ->update([
                        'st_value' => $request->sms['factorBuy'],
                    ]);
            }

            if ($sms_visitorBuy == null) {//mode insert
                DB::table('setting')
                    ->insert([
                        'st_key' => 'sms_visitorBuy',
                        'st_value' => $request->sms['sms_visitorBuy'],
                        'st_collection_id' => 0,
                        'st_user_id' => 0
                    ]);
            } else {//mode update
                DB::table('setting')
                    ->where('st_key', 'sms_visitorBuy')
                    ->update([
                        'st_value' => $request->sms['sms_visitorBuy'],
                    ]);
            }

            if ($sms_changeMobile == null) {//mode insert
                DB::table('setting')
                    ->insert([
                        'st_key' => 'sms_changeMobile',
                        'st_value' => $request->sms['sms_changeMobile'],
                        'st_collection_id' => 0,
                        'st_user_id' => 0
                    ]);
            } else {//mode update
                DB::table('setting')
                    ->where('st_key', 'sms_changeMobile')
                    ->update([
                        'st_value' => $request->sms['sms_changeMobile'],
                    ]);
            }

 if ($sms_cancel_factor == null) {//mode insert
                DB::table('setting')
                    ->insert([
                        'st_key' => 'sms_cancel_factor',
                        'st_value' => $request->sms['sms_cancel_factor'],
                        'st_collection_id' => 0,
                        'st_user_id' => 0
                    ]);
            } else {//mode update
                DB::table('setting')
                    ->where('st_key', 'sms_cancel_factor')
                    ->update([
                        'st_value' => $request->sms['sms_cancel_factor'],
                    ]);
            }

            if ($email_sendPassword == null) {//mode insert
                DB::table('setting')
                    ->insert([
                        'st_key' => 'email_sendPassword',
                        'st_value' => $request->email['sendPassword'],
                        'st_collection_id' => 0,
                        'st_user_id' => 0
                    ]);
            } else {//mode update
                DB::table('setting')
                    ->where('st_key', 'email_sendPassword')
                    ->update([
                        'st_value' => $request->email['sendPassword'],
                    ]);
            }

            if ($email_welcome == null) {//mode insert
                DB::table('setting')
                    ->insert([
                        'st_key' => 'email_welcome',
                        'st_value' => $request->email['welcome'],
                        'st_collection_id' => 0,
                        'st_user_id' => 0
                    ]);
            } else {//mode update
                DB::table('setting')
                    ->where('st_key', 'email_welcome')
                    ->update([
                        'st_value' => $request->email['welcome'],
                    ]);
            }

            if ($email_forgetPassword == null) {//mode insert
                DB::table('setting')
                    ->insert([
                        'st_key' => 'email_forgetPassword',
                        'st_value' => $request->email['forgetPassword'],
                        'st_collection_id' => 0,
                        'st_user_id' => 0
                    ]);
            } else {//mode update
                DB::table('setting')
                    ->where('st_key', 'email_forgetPassword')
                    ->update([
                        'st_value' => $request->email['forgetPassword'],
                    ]);
            }

            if ($email_factorBuy == null) {//mode insert
                DB::table('setting')
                    ->insert([
                        'st_key' => 'email_factorBuy',
                        'st_value' => $request->email['factorBuy'],
                        'st_collection_id' => 0,
                        'st_user_id' => 0
                    ]);
            } else {//mode update
                DB::table('setting')
                    ->where('st_key', 'email_factorBuy')
                    ->update([
                        'st_value' => $request->email['factorBuy'],
                    ]);
            }

            if ($email_changeEmail == null) {//mode insert
                DB::table('setting')
                    ->insert([
                        'st_key' => 'email_changeEmail',
                        'st_value' => $request->email['email_changeEmail'],
                        'st_collection_id' => 0,
                        'st_user_id' => 0
                    ]);
            } else {//mode update
                DB::table('setting')
                    ->where('st_key', 'email_changeEmail')
                    ->update([
                        'st_value' => $request->email['email_changeEmail'],
                    ]);
            }

            if ($forge_calc_porsant == null) {//mode insert
                DB::table('setting')
                    ->insert([
                        'st_key' => 'forge_calc_porsant',
                        'st_value' => $request->forgeCalcPorsant,
                        'st_collection_id' => 0,
                        'st_user_id' => 0
                    ]);
            } else {//mode update
                DB::table('setting')
                    ->where('st_key', 'forge_calc_porsant')
                    ->update([
                        'st_value' => $request->forgeCalcPorsant,
                    ]);
            }

            if ($send_sms == null) {//mode insert
                DB::table('setting')
                    ->insert([
                        'st_key' => 'send_sms',
                        'st_value' => $request->has("checkActive.sms"),
                        'st_collection_id' => 0,
                        'st_user_id' => 0
                    ]);
            } else {//mode update
                DB::table('setting')
                    ->where('st_key', 'send_sms')
                    ->update([
                        'st_value' => $request->has("checkActive.sms"),
                    ]);
            }
            if ($send_email == null) {//mode insert
                DB::table('setting')
                    ->insert([
                        'st_key' => 'send_email',
                        'st_value' => $request->has("checkActive.email"),
                        'st_collection_id' => 0,
                        'st_user_id' => 0
                    ]);
            } else {//mode update
                DB::table('setting')
                    ->where('st_key', 'send_email')
                    ->update([
                        'st_value' => $request->has("checkActive.email"),
                    ]);
            }

            if ($old_role == null) {//mode insert
                DB::table('setting')
                    ->insert([
                        'st_key' => 'factor_receiver',
                        'st_value' => $request->role_factor_receiver,
                        'st_collection_id' => 0,
                        'st_user_id' => 0
                    ]);
            } else {//mode update
                DB::table('setting')
                    ->where('st_key', 'factor_receiver')
                    ->where('st_value',$old_role)
                    ->update([
                        'st_value' => $request->role_factor_receiver,
                    ]);
            }

            if ($expire_time_visitor == null) {//mode insert
                DB::table('setting')
                    ->insert([
                        'st_key' => 'otp_expire_time',
                        'st_value' => $request->expireTimeVisitor,
                        'st_collection_id' => 0,
                        'st_user_id' => 0
                    ]);
            } else {//mode update
                DB::table('setting')
                    ->where('st_key', 'otp_expire_time')
                    ->update([
                        'st_value' => $request->expireTimeVisitor,
                    ]);
            }
            if ($checkout_request == null) {//mode insert
                DB::table('setting')
                    ->insert([
                        'st_key' => 'checkout_permitted_amout',
                        'st_value' => $request->checkoutRequest,
                        'st_collection_id' => 0,
                        'st_user_id' => 0
                    ]);
            } else {//mode update
                DB::table('setting')
                    ->where('st_key', 'checkout_permitted_amout')
                    ->update([
                        'st_value' => $request->checkoutRequest,
                    ]);
            }

            return json_encode(['status' => 100, 'msg' => 'با موفقیت انجام گردید']);

        }
        return view('admin.setting.system_setting', compact('financial', 'financial_unit', 'tax', 'basket_life_time', 'purchase_for_subscription_life_time', 'sms_sendPassword', 'sms_welcome', 'sms_forgetPassword', 'sms_factorBuy', 'email_sendPassword', 'email_welcome', 'email_forgetPassword', 'email_factorBuy', 'forge_calc_porsant', 'send_sms', 'send_email', 'sms_visitorBuy', 'role', 'old_role','expire_time_visitor','checkout_request','sms_changeMobile','email_changeEmail','sms_cancel_factor'));
    }
}
