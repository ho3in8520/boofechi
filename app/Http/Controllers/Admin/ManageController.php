<?php

namespace App\Http\Controllers\Admin;

use App\Baseinfo;
use App\Collection;
use App\Component\Tools;
use App\Http\Controllers\Controller;
use App\Http\Requests\BasicInfoRequest;
use App\Permission;
use App\Person;
use App\Role;
use App\SystemFactorDetail;
use App\SystemFactorMaster;
use App\Terms;
use App\UploadRoute;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use phpDocumentor\Reflection\Types\Compound;


class ManageController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:menu_setting', ['only' => ['menuSetting', 'saveMenu']]);
        $this->middleware('permission:manage_baseinfos', ['only' => ['basicInfo']]);
        $this->middleware('permission:manage_terms', ['only' => ['create_terms']]);
        $this->middleware('permission:manage_avatars', ['only' => ['acceptUploadedFiles', 'acceptFile', 'deacceptFile']]);
        $this->middleware('permission:create_factor', ['only' => ['systemFactor', 'listSystemFactor', 'editSystemFactor', 'returnSystemFactor']]);
        $this->middleware('permission:list_collection', ['only' => ['list_collection', 'listCollectionAjax']]);
        $this->middleware('permission:support_form', ['only' => ['supportForm']]);
        $this->middleware('permission:edit_porsant', ['only' => ['editPorsant']]);
        $this->middleware('permission:create_advertising', ['only' => ['createAdvertising']]);
        $this->middleware('permission:list_advertising', ['only' => ['listAdvertising']]);
        $this->middleware('permission:edit_advertising', ['only' => ['editAdvertising']]);
        $this->middleware('permission:change_password_user', ['only' => ['changePasswordUser']]);
        $this->middleware('permission:manage_wallet', ['only' => ['wallet']]);
        $this->middleware('permission:manage_Transaction', ['only' => ['createTransaction', 'editTransaction', 'searchUserTransaction']]);
        $this->middleware('permission:list_transaction_system', ['only' => ['listTransaction']]);
        $this->middleware('permission:create_transaction_system', ['only' => ['createTransactionSystem']]);
        $this->middleware('permission:porsant_collection', ['only' => ['porsantCollection']]);
        $this->middleware('permission:wallet_history', ['only' => ['walletHistory']]);
        $this->middleware('permission:checkout_request_list', ['only' => ['checkoutRequestList']]);
        $this->middleware('permission:login_panel_user', ['only' => ['loginPanelUser']]);
    }

    public function blockCollection($id)
    {
        if (blockCollectionAndItUsers($id, "block")) {
            return redirect()->back()->with('success', 'با موفقیت انجام شد!');
        }
        return redirect()->back()->with('error', 'خطا در بلاک کردن مجموعه');
    }

    public function unblockCollection($id)
    {
        if (blockCollectionAndItUsers($id, "unblock")) {
            return redirect()->back()->with('success', 'با موفقیت انجام شد!');
        }
        return redirect()->back()->with('error', 'خطا در بلاک کردن مجموعه');
    }

    public function designPrint($id)
    {
        $collection = DB::table('collections')->where('coll_id', $id)->first();
        if (!$collection)
            abort(404);

        if (request()->isMethod("post")) {
            $setting = DB::table('setting')->where('st_collection_id', $id)->where('st_key', 'print_html')->first();
            if ($setting)
                DB::table('setting')->where('st_id', $setting->st_id)->update([
                    'st_value' => request()->get('print_html'),
                ]);
            else
                DB::table('setting')->insert([
                    'st_value' => request()->get('print_html'),
                    'st_collection_id' => $id,
                    'st_key' => 'print_html'
                ]);
            return \redirect(route('design-print', $id));
        }
        $factor_html = setting("print_html", $collection->coll_id);
        return view('admin.manage.design_print', compact('collection', 'factor_html'));
    }

    public function supportForm($id)
    {
        $user = findUser($id);
        if (!$user)
            abort(404);
        return view('admin.manage.support_form', compact('user', 'id'));
    }

    public function Tickets(Request $request, $id)
    {
        $time = 48 * 60 * 60;
        DB::table('tickets')
            ->where('t_mode', 0)
            ->whereRaw("t_created_at + $time < " . time())
            ->update([
                't_mode' => 1
            ]);
        $statuses = config('first_config.ticket-statuses');
        $ticketsA = DB::table('tickets as last_answer')
            ->select([
                'parent.t_subject',
                'parent.t_id as t_id',
                'rol_label',
                'last_answer.t_created_at',
                'last_answer.t_status',
                DB::raw('(select top 1 tr_is_viewed from ticket_routing where tr_ticket_id = last_answer.t_id order by tr_id desc) is_viewed')
            ])
            ->leftJoin('roles', 'last_answer.t_assigned_role_id', '=', 'rol_id')
            ->leftJoin('tickets as parent', 'parent.t_id', '=', 'last_answer.t_parent_id')
            //->whereRaw('last_answer.t_assigned_role_id in (select ur_rol_id from user_roles where ur_user_id = ' . user('user_id') . ')')
            ->whereRaw("last_answer.t_id in(select max(t_id) from tickets where t_parent_id != 0)");
        if ($request->has('ticket_no') && $request->get('ticket_no') != '') {
            $ticketsA->where('parent.t_id', $request->get('ticket_no'));
        }
        if ($request->has('ticket_subject') && $request->get('ticket_subject') != '')
            $ticketsA->where('parent.t_subject', 'like', '%' . $request->get('ticket_subject') . '%');
        if ($request->has('ticket_department') && $request->get('ticket_department') != '')
            $ticketsA->where('last_answer.t_assigned_role_id', $request->get('ticket_department'));

        if ($request->has('date_from') && $request->get('date_from') != '')
            $ticketsA->where('parent.t_created_at', '>=', jalali_to_timestamp($request->get('date_from')));
        if ($request->has('date_to') && $request->get('date_to') != '')
            $ticketsA->where('parent.t_created_at', '<=', jalali_to_timestamp($request->get('date_to'), '/', false));

        $ticketsA->where('parent.t_creator_user_id', user('user_id'));


        $ticketsB = DB::table('tickets')
            ->select([
                't_subject',
                't_id',
                'rol_label',
                't_created_at',
                't_status',
                DB::raw('(select top 1 tr_is_viewed from ticket_routing where tr_ticket_id = t_id order by tr_id desc) is_viewed')
            ])
            ->leftJoin('roles', 't_assigned_role_id', '=', 'rol_id')
            ->where('t_parent_id', 0)
            //->whereRaw('t_assigned_role_id in (select ur_rol_id from user_roles where ur_user_id = ' . user('user_id') . ')')
            ->whereRaw('t_id not in(select t_parent_id from tickets)');
        if ($request->has('ticket_no') && $request->get('ticket_no') != '')
            $ticketsB->where('t_id', $request->get('ticket_no'));
        if ($request->has('ticket_subject') && $request->get('ticket_subject') != '')
            $ticketsB->where('t_subject', 'like', '%' . $request->get('ticket_subject') . '%');
        if ($request->has('ticket_department') && $request->get('ticket_department') != '')
            $ticketsB->where('t_assigned_role_id', $request->get('ticket_department'));
        if ($request->has('date_from') && $request->get('date_from') != '')
            $ticketsB->where('t_created_at', '>=', jalali_to_timestamp($request->get('date_from')));
        if ($request->has('date_to') && $request->get('date_to') != '')
            $ticketsB->where('t_created_at', '<=', jalali_to_timestamp($request->get('date_to'), '/', false));

        $ticketsB->where('t_creator_user_id', $id);

        $tickets = $ticketsA->union($ticketsB)->paginate(20);

        $departments = getRoles("self-domain");
        return showData(view('admin.manage.tickets', compact('tickets', 'statuses', 'departments', 'id')));
    }

    public function subscription(Request $request, $id)
    {
        $states = \App\City::where('c_parent_id', 0)->get()->toArray();
        if (request()->has('state') && request()->get('state') != "")
            $cities = \App\City::where('c_parent_id', request()->get('state'))->get();
        else
            $cities = [];

        $subscription = DB::table("users")
            ->select([
                DB::raw("concat(coll_name,' ',code) as name"),
                'is_active',
                'state.c_name as state',
                'users.user_id',
                'city.c_name as city',
                'users.created_at',
                'code',
                DB::raw("(select top 1 ur_path from upload_route where ur_collection_id = coll_id and ur_fk_id = user_id and ur_table_name = 'avatar' order by ur_id desc) as avatar")
            ])
            ->join("persons", "prsn_user_id", "=", "user_id")
            ->join("collection_persons", "prsn_id", "=", "cp_prsn_id")
            ->join("collections", "coll_id", "=", "cp_coll_id")
            ->join("cities as city", "prsn_city_id", "=", "city.c_id")
            ->join("cities as state", "prsn_state_id", "=", "state.c_id");

        if (\request()->has('name') && \request('name') != "") {
            $name = \request('name');
            $subscription->where(function ($query) use ($name) {
                return $query->where("coll_name", "like", '%' . \request('name') . '%')
                    ->orWhere("code", "like", '%' . \request('name') . '%');
            });
        }

        if (\request()->has('date_from') && \request('date_from') != "")
            $subscription->where('users.created_at', '>=', date("Y-m-d H:i:s", jalali_to_timestamp(\request('date_from'))));

        if (\request()->has('date_to') && \request('date_to') != "")
            $subscription->where('users.created_at', '<=', date("Y-m-d H:i:s", jalali_to_timestamp(\request('date_to'), '/', false)));

        if (\request()->has('state') && \request('state') != "")
            $subscription->where('state.c_id', \request('state'));

        if (\request()->has('city') && \request('city') != "")
            $subscription->where('city.c_id', \request('city'));

        $subscription = $subscription->where("reagent_id", $id)
            ->paginate(20);

        return showData(view('admin.manage.subscriptions', compact('states', 'cities', 'subscription', 'id')));
    }

    public function userDetails(Request $request)
    {
        $details = DB::table("users")
            ->select([
                'coll_name',
                'is_active',
                'state.c_name as state',
                'city.c_name as city',
                'users.user_id',
                'users.created_at',
                'users.panel_type',
                'code',
                DB::raw("concat(prsn_name,' ',prsn_family) as prsn_name"),
                'prsn_mobile1',
                'prsn_address',
                'email',
                DB::raw("(select top 1 ur_path from upload_route where ur_collection_id = coll_id and ur_fk_id = user_id and ur_table_name = 'avatar' order by ur_id desc) as avatar")
            ])
            ->join("persons", "prsn_user_id", "=", "user_id")
            ->join("collection_persons", "prsn_id", "=", "cp_prsn_id")
            ->join("collections", "coll_id", "=", "cp_coll_id")
            ->join("cities as city", "prsn_city_id", "=", "city.c_id")
            ->join("cities as state", "prsn_state_id", "=", "state.c_id")
            ->where("user_id", $request->get('id'))
            ->first();
        $id = $request->get('id');
        return showData(view('admin.manage.user_details', compact('details', 'id')));
    }

    public function userPermissions(Request $request)
    {
        $permissions = getPermissions($request->get('id'));
        return showData(view('admin.manage.user_permissions', compact('permissions')));
    }

    public function userBlock(Request $request)
    {
        if (DB::table('users')->where('user_id', $request->get('id'))->update([
            'is_active' => (($request->get('status') == 0) ? 1 : 0)
        ])
        ) {
            if ($request->get('status') == 1) {
                DB::table('violation_log')->insert([
                    'vl_user_id' => $request->get('id'),
                    'vl_time' => time(),
                    'vl_type' => 1,
                    'vl_reason' => ($request->get('status') == 1) ? $request->get('reason') : null
                ]);
            }
            return json_encode(['status' => 100, 'msg' => 'با موفقیت انجام شد!']);
        }
        return json_encode(['status' => 500, 'msg' => 'خطا در انجام عملیات، مجددا تلاش فرمائید.']);
    }

    public function factors(Request $request, $id)
    {
        $factors = DB::table('factor_master')->select([
            'fm_date',
            'fm_no',
            DB::raw('dbo.get_factor_amount(fm_amount,2) as fm_amount'),
            'fm_is_payed',
            'fm_id'
        ]);
        if ($request->has('factor_date') && $request->get('factor_date') != '')
            $factors->where('fm_date', $request->get('factor_date'));
        if ($request->has('factor_no') && $request->get('factor_no') != '')
            $factors->where('fm_no', $request->get('factor_no'));
        if ($request->has('factor_payment') && $request->get('factor_payment') != '') {
            if (in_array($request->get('factor_payment'), [0, 1]))
                $factors->where('fm_is_payed', $request->get('factor_payment'));
        }

        $factors = $factors
            ->where('fm_mode', 1)
            ->where('fm_creator_id', $id)
            ->latest("fm_id")
            ->paginate(20);
        $mode = 1;
        return showData(view('admin.manage.factors', compact('factors', 'mode')));
    }

    public function violationsLog(Request $request, $id)
    {
        $reports = DB::table('violation_log')->where('vl_user_id', $id);

        if (\request()->has('date_from') && \request('date_from') != "")
            $reports->where('vl_time', '>=', jalali_to_timestamp(\request('date_from')));

        if (\request()->has('date_to') && \request('date_to') != "")
            $reports->where('vl_time', '<=', jalali_to_timestamp(\request('date_to'), '/', false));

        if (\request()->has('reason') && \request('reason') != "")
            $reports->where('vl_reason', 'like', '%' . \request('reason') . '%');

        $reports = $reports->paginate(20);
        return showData(view('admin.manage.violations_log', compact('reports', 'id')));
    }

    public function userRoles(Request $request, $id)
    {
        $user = DB::table("users")->where("user_id", $id)->first();
        if (!$user)
            abort(404);

        if (\request()->isMethod("post")) {
            $check_role = DB::table("roles")->where("rol_id", request("role"))->where("rol_creator", user("user_id"))->first();
            if (!$check_role)
                return json_encode(["status" => 500, 'msg' => "نقش انتخاب شده، فاقد اعتبار است!"]);

            if (DB::table("user_roles")->insert(["ur_rol_id" => $check_role->rol_id, "ur_user_id" => $id])) {
                return json_encode(["status" => 100, 'msg' => "ثبت نقش با موفقیت انجام شد!"]);
            }
            return json_encode(["status" => 500, 'msg' => "خطا در ثبت نقش، مجددا تلاش فرمائید!"]);

        }

        $roles = DB::table("user_roles")
            ->select([
                "rol_name",
                "rol_label",
                "ur_rol_id",
            ])
            ->join("roles", "ur_rol_id", "=", "rol_id")
            ->where("ur_user_id", $id)
            ->get();
        $array_roles = array_column(json_decode(json_encode($roles->toArray()), true), 'ur_rol_id');
        $my_roles = Role::where('rol_creator', user('user_id'))
            ->whereNotIn("rol_id", $array_roles)
            ->where('rol_name','!=','admin')
            ->get()->toArray();
        return showData(view("admin.manage.user-roles", compact('roles', 'my_roles', 'id')));
    }

    public function porsant($id)
    {
        $porsant = DB::table("factor_master")
            ->select([
                DB::raw("floor(dbo.calcPercent(fm_amount," . user('porsant_percent') . ")) as porsant"),
                DB::raw("concat(coll_name,' ',code) as name"),
                'fm_created_at',
                'fm_no',
                DB::raw('floor(dbo.get_factor_amount(fm_id,2)) as fm_amount')
            ])
            ->join("users", "user_id", "=", "fm_creator_id")
            ->join("collections", "coll_id", "=", "fm_self_collection_id");
        if (\request()->has('name') && \request('name') != "") {
            $name = \request('name');
            $porsant->where(function ($query) use ($name) {
                return $query->where("coll_name", "like", '%' . \request('name') . '%')
                    ->orWhere("code", "like", '%' . \request('name') . '%');
            });
        }
        if (\request()->has('factor_number') && \request('factor_number') != "")
            $porsant->where('fm_no', \request('factor_number'));

        if (\request()->has('date_from') && \request('date_from') != "")
            $porsant->where('fm_created_at', '>=', jalali_to_timestamp(\request('date_from')));

        if (\request()->has('date_to') && \request('date_to') != "")
            $porsant->where('fm_created_at', '<=', jalali_to_timestamp(\request('date_to'), '/', false));

        $porsant = $porsant->where("fm_porsant_user_id", $id)
            ->where("fm_status", 2)
            ->latest("fm_id")
            ->paginate(20);

        return showData(view('admin.manage.porsants', compact('porsant', 'id')));
    }

    public function userAnnouncements(Request $request, $id)
    {
        $result = DB::table('announcements')
            ->leftJoin('announcement_receivers', 'anr_announcement_id', 'ann_id')
            ->where('anr_type_id', findUser($id, 'panel_type', 'id'))
            ->where('anr_city_id', findUser($id, 'city', 'id'))
            ->leftJoin('collections', 'ann_collection_id', 'coll_id')
            ->select([
                'ann_id',
                'ann_title',
                'ann_body',
                'coll_name',
                'announcements.created_at',
                DB::raw("(select top 1 count(1) from announcement_viewed where av_ann_id = ann_id and av_user_id = $id) as is_viewed")

            ]);

        if (request()->has('companyName') && request('companyName') != "")
            $result->where("coll_name", "like", '%' . request('companyName') . '%');
        if (request()->has('status') && request('status') != "")
            if (request('status') != 1)
                $result->whereRaw('(select top 1 count(1) from announcement_viewed where av_ann_id = ann_id) = 0');
            else
                $result->whereRaw('(select top 1 count(1) from announcement_viewed where av_ann_id = ann_id) > 0');

        if (request()->has('no_date_from') && request('no_date_from') != "") {
            $from = date("Y-m-d 00:00:00", jalali_to_timestamp(request('no_date_from')));
            $result->where('created_at', '>=', $from);
        }

        if (request()->has('no_date_to') && request('no_date_to') != "") {
            $to = date("Y-m-d 23:59:59", jalali_to_timestamp(request('no_date_to')));
            $result->where('created_at', '<=', $to);
        }
        $result = $result->paginate(20);

        return showData(view('admin.manage.announcements', compact('result')));
    }

    public function userProfile($id)
    {
        $details = DB::table("users")
            ->select([
                'coll_id',
                'coll_name',
                'is_active',
                'state.c_name as state',
                'city.c_name as city',
                'users.user_id',
                'users.created_at',
                'users.panel_type',
                'code',
                DB::raw("concat(prsn_name,' ',prsn_family) as prsn_name"),
                'prsn_mobile1',
                'prsn_address',
                'email',
                DB::raw("(select top 1 ur_path from upload_route where ur_collection_id = coll_id and ur_fk_id = user_id and ur_table_name = 'avatar' order by ur_id desc) as avatar")
            ])
            ->join("persons", "prsn_user_id", "=", "user_id")
            ->join("collection_persons", "prsn_id", "=", "cp_prsn_id")
            ->join("collections", "coll_id", "=", "cp_coll_id")
            ->join("cities as city", "prsn_city_id", "=", "city.c_id")
            ->join("cities as state", "prsn_state_id", "=", "state.c_id")
            ->where("user_id", $id)
            ->first();

        $is_asli = DB::table('persons')
            ->select(['prsn_is_owner'])
            ->where('prsn_user_id', $details->user_id)
            ->first()->prsn_is_owner;

        $collection = DB::table('collections')
            ->select([
                'coll_name',
                'coll_economic_code',
                'coll_national_number',
                'coll_site_address',
                'coll_post_code',
                'coll_address',
                'coll_state_id',
                'coll_city_id',
                DB::raw("(select top 1 c_name from cities where c_id=coll_state_id) as state"),
                DB::raw("(select top 1 c_name from cities where c_id=coll_city_id) as city"),
            ])
            ->where('coll_id', findUser($id, 'collection', 'id'))
            ->first();

        $classes = DB::table('collection_classes')
            ->select([
                'bas_value'
            ])
            ->where('coc_coll_id', findUser($id, 'collection', 'id'))
            ->leftJoin('baseinfos', 'coc_typeClass_id', 'bas_id')
            ->get();

        $role = DB::table('user_roles')
            ->select([
                'rol_label'
            ])
            ->where('ur_user_id', $id)
            ->leftJoin('roles', 'ur_rol_id', 'rol_id')
            ->get();

        $area_support_city = DB::table('area_support')
            ->select([
                'c_name'
            ])
            ->where('as_collection_id', $details->coll_id)
            ->where('as_user_id', $id)
            ->where('c_parent_id', '!=', '0')
            ->leftJoin('cities', 'as_city_id', 'c_id')
            ->get();

        $area_support_state = DB::table('area_support')
            ->select([
                'p.c_name'
            ])
            ->where('as_collection_id', $details->coll_id)
            ->where('as_user_id', $id)
            ->leftJoin('cities as b', 'as_city_id', 'b.c_id')
            ->leftJoin('cities as p', 'b.c_parent_id', 'p.c_id')
            ->groupBy('p.c_name')
            ->get();

        $category = DB::table('collection_categories')
            ->where('cc_collection_id', $details->coll_id)
            ->where('cc_user_id', ($is_asli) ? 0 : $details->user_id)
            ->leftJoin('categories', 'cc_category_id', 'cat_id')
            ->get();

        return showData(view('admin.manage.personality', compact('details', 'id', 'role', 'area_support_state', 'area_support_city', 'category', 'collection', 'classes')));
    }

    public function userDocuments($id)
    {
        $files = DB::table("upload_route")
            ->select([
                'upload_route.*',
                'bas_value',
                'ur_description'
            ])
            ->leftJoin('baseinfos', 'bas_id', '=', 'ur_type_id')
            ->where('ur_fk_id', $id)
            ->latest('ur_id')
            ->paginate(20);
        return showData(view('admin.manage.documents', compact('files')));
    }

    public function userSetting(Request $request, $id)
    {
        if (request()->isMethod("post")) {
            $request->validate([
                'factor_receiver' => ['required'],
            ]);
            if ($request->has('role') && $request->get('role') != '')
                DB::table('user_roles')->insert([
                    'ur_user_id' => $id,
                    'ur_rol_id' => request()->get('role')
                ]);
            if (DB::table('setting')->insert([
                'st_key' => 'factor_receiver',
                'st_value' => request()->get('factor_receiver'),
                'st_collection_id' => findUser($id, 'collection', 'id'),
            ])
            )
                return json_encode(['status' => 100, 'msg' => 'با موفقیت انجام شد!']);
            return json_encode(['status' => 500, 'msg' => 'خطا در به روز رسانی تنظیمات، مجددا تلاش فرمائید.']);
        }
        $array_setting = [];
        $array_setting['factor_receiver'] = setting('factor_receiver', findUser($id, 'collection', 'id'));
        $self_roles = Role::where('rol_creator', $id)->get()->toArray();
        $roles = Role::where('rol_creator', user('user_id'))->get()->toArray();
        $default_role_admin_set = DB::table('user_roles')->join('roles', 'ur_rol_id', '=', 'rol_id')->whereIn('rol_id', array_column($roles, 'rol_id'))->first();
        return showData(view('admin.manage.setting', compact('roles', 'self_roles', 'array_setting', 'default_role_admin_set', 'id')));
    }

    public function reports()
    {
        $reports = DB::table("messages")
            ->select([
                "messages.*",
                "coll_name",
                DB::raw("concat(prsn_name,' ',prsn_family) as prsn_name"),
            ])
            ->join("persons", "prsn_user_id", "=", "msg_sender_user_id")
            ->join("collections", "coll_id", "=", "msg_receiver_collection_id")
            ->where("msg_type", 1)->latest("msg_id")->paginate(20);
        return view('admin.manage.reports', compact('reports'));
    }

    public function viewReport($id)
    {
        $report = DB::table("messages")
            ->select([
                "messages.*",
                "coll_name",
                DB::raw("concat(prsn_name,' ',prsn_family) as prsn_name"),
                'c.c_name as city',
                's.c_name as state',
                'bas_value',
                DB::raw("(select top 1 concat(prsn_name,' ',prsn_family,'(',code,')') from persons
                        left join collection_persons on cp_prsn_id = prsn_id
                        left join users on prsn_user_id = user_id
                        where cp_coll_id = coll_id
                        order by prsn_id asc)as receiver_name"),
                DB::raw("(select top 1 ur_path from upload_route where ur_fk_id = msg_id and ur_table_name = 'reports') as report_file")
            ])
            ->join("persons", "prsn_user_id", "=", "msg_sender_user_id")
            ->join("users as usender", "prsn_user_id", "=", "usender.user_id")
            ->join("baseinfos as bsender", "bsender.bas_id", "=", "usender.panel_type")
            ->join("cities as c", "prsn_city_id", "=", "c.c_id")
            ->join("cities as s", "prsn_state_id", "=", "s.c_id")
            ->join("collections", "coll_id", "=", "msg_receiver_collection_id")
            ->where("msg_type", 1)
            ->where("msg_id", $id)
            ->first();
        return showData(view('admin.manage.view_report', compact('report')));
    }

    public function addViolation($id)
    {
        $report = DB::table("messages")
            ->select([
                DB::raw("(select top 1 user_id from persons
                        left join collection_persons on cp_prsn_id = prsn_id
                        left join users on prsn_user_id = user_id
                        where cp_coll_id = coll_id
                        order by prsn_id asc)as user_id")
            ])
            ->join("collections", "coll_id", "=", "msg_receiver_collection_id")
            ->where("msg_type", 1)
            ->where("msg_id", $id)
            ->first();
        if (!$report)
            abort(404);
        if (DB::table("violation_log")->insert([
            'vl_user_id' => $report->user_id,
            'vl_type' => 0,
            'vl_time' => time(),
        ])
        )
            return json_encode(["status" => 100, 'msg' => 'با موفقیت انجام شد!']);
        return json_encode(["status" => 500, 'msg' => 'خطا در ثبت اخطار، مجددا تلاش فرمائید!']);
    }

    public function systemFactor(Request $request, $id)
    {
        $findUser = DB::table('tickets')
            ->where('t_id', $id)
            ->select([
                't_creator_user_id',
            ])
            ->first();
        if (!$findUser)
            abort(404);

        $collection = findUser($findUser->t_creator_user_id, 'collection', 'all');

        if (!$collection)
            abort(404);

        $img = DB::table('upload_route')
            ->where('ur_collection_id', $collection->coll_id)
            ->where('ur_table_name', "logo")
            ->orderBy('ur_collection_id', 'asc')
            ->select([
                "ur_path",
            ])
            ->first();
        if ($request->isMethod('post')) {
            $request->validate([
                'title.*' => ['required'],
                'price.*' => ['required', 'regex:/([0-9]+)/'],
            ]);
            $array = [];
            foreach ($request->price as $key => $price) {
                array_push($array, [
                    'TI' => $request->title[$key],
                    'PR' => $price
                ]);
            }

            $result = call_sp("system_factor", null, [
                'mode' => 0,
                'values' => createRootData($array),
                'collection_id' => $collection->coll_id,
                'user_id' => user("user_id"),
                'created_at' => time(),
                'description' => $request->description,
                'discount' => $request->dicount ?? 0,
            ]);
            if ($result[0]['result'] == 0)
                return json_encode(['status' => 100, 'msg' => 'با موفقیت انجام شد!']);
            return json_encode(['status' => 500, 'msg' => 'خطا در ثبت فاکتور، مجددا تلاش فرمائید!']);


        }
        return view('admin.manage.system-factor', compact('collection', 'img'));
    }

    public function listSystemFactor(Request $request, $id = null)
    {
        if ($request->isMethod("post")) {
            if (DB::table('system_factor_master')->where('sfm_id', $id)->where('sfm_mode', 0)->first()) {
                $result = call_sp("system_factor", null, [
                    'mode' => 1,
                    'sfm_id' => $id
                ]);
                if ($result[0]['result'] != 0)
                    session()->flash('error', 'خطا در صدور فاکتور، مجددا تلاش فرمائید!');
                else
                    return redirect(route("list-system-factor"));

            } else
                abort(404);
        }
        $result = DB::table('system_factor_master')
            ->leftJoin('collections', 'coll_id', 'sfm_collection_id');
        if (request()->has('name') && request('name') != "") {
            $result->where("coll_name", "like", '%' . request('name') . '%');
        }
        if (request()->has('amount') && request('amount') != "")
            $result->where("sfm_amount", "like", '%' . request('amount') . '%');
        if (\request()->has('date') && \request('date') != "")
            $result->where('sfm_created_at', '>=', date("Y-m-d H:i:s", jalali_to_timestamp(\request('date'))));
        $result = $result->leftJoin('users', 'user_id', 'sfm_creator_id');
        if (request()->has('userCreate') && request('userCreate') != "")
            $result->where("username", "like", '%' . request('userCreate') . '%');
        if (request()->has('mode') && request('mode') != 0) {
            $mode_array = [1 => 0, 2 => 1];
            $result->where("sfm_mode", $mode_array[request('mode')]);
        }
        if (request()->has('is_payed') && request('is_payed') != 0) {
            $mode_array = [1 => 0, 2 => 1];
            $result->where("sfm_is_payed", $mode_array[request('is_payed')]);
        }
        $result = $result->select([
            'coll_name',
            'username',
            'sfm_created_at',
            'sfm_amount',
            'sfm_mode',
            'sfm_is_payed',
            'sfm_id',
            'sfm_discount',
            'sfm_is_force'
        ])
            ->paginate(20);
//        dd($result);
        return showData(view('admin.manage.list-factor-system', compact('result')));
    }

    public function editSystemFactor(Request $request, $id)
    {
        $find_user = SystemFactorMaster::where('sfm_id', $id)->first();
        if ($find_user->sfm_mode == 0) {
            $collection = DB::table('collections')
                ->where('coll_id', $find_user->sfm_collection_id)
                ->leftJoin('collection_persons', 'cp_coll_id', 'coll_id')
                ->leftJoin('persons', 'prsn_id', 'cp_prsn_id')
                ->first();

            $img = DB::table('upload_route')
                ->where('ur_collection_id', $collection->coll_id)
                ->where('ur_table_name', "logo")
                ->orderBy('ur_collection_id', 'asc')
                ->select([
                    "ur_path",
                ])
                ->first();
            $detail = SystemFactorDetail::where('sfd_master_id', $find_user->sfm_id)->get();
            if ($request->isMethod('post')) {
                $request->validate([
                    'title.*' => ['required'],
                    'price.*' => ['required', 'regex:/([0-9]+)/'],
                ]);
                $array = [];
                foreach ($request->price as $key => $price) {
                    array_push($array, [
                        'TI' => $request->title[$key],
                        'PR' => $price
                    ]);
                }

                $result = call_sp("system_factor", null, [
                    'mode' => 2,
                    'values' => createRootData($array),
                    'sfm_id' => $id,
                    'description' => $request->description,
                    'discount' => $request->dicount,
                ]);

                if ($result[0]['result'] == 0)
                    return json_encode(['status' => 100, 'msg' => 'با موفقیت انجام شد!', 'url' => route('list-system-factor')]);
                return json_encode(['status' => 500, 'msg' => 'خطا در ثبت فاکتور، مجددا تلاش فرمائید!']);
            }
            return view('admin.manage.edit_factors', compact('collection', 'img', 'detail', 'find_user'));
        } else {
            abort(404);
        }
    }

    public function returnSystemFactor(Request $request, $id)
    {
        if ($request->isMethod("post")) {
            if (DB::table('system_factor_master')->where('sfm_id', $id)->where('sfm_mode', 1)->first()) {
                $result = call_sp("system_factor", null, [
                    'mode' => 3,
                    'sfm_id' => $id
                ]);
                if ($result[0]['result'] != 0)
                    session()->flash('error', 'خطا در صدور فاکتور، مجددا تلاش فرمائید!');
                else
                    return redirect(route("list-system-factor"));

            } else
                abort(404);
        }
    }

    public function list_collection(Request $request)
    {
        $result = DB::table('collections')
            ->select([
                "coll_id",
                "coll_hide",
                "users.*",
                "persons.*",
                DB::raw("concat(coll_name,' ','(',username,')') as coll_name")
            ]);
        if (request()->has('name') && request('name') != "") {
            $result->where("coll_name", "like", '%' . request('name') . '%');
        }
        $result = $result->leftJoin('collection_persons', 'cp_coll_id', 'coll_id')
            ->leftJoin('persons', 'prsn_id', 'cp_prsn_id');
        if (request()->has('phone') && request('phone') != "") {
            $result->where("prsn_phone1", "like", '%' . request('phone') . '%');
        }
        $result = $result->leftJoin('users', 'user_id', 'prsn_user_id');
        if (request()->has('type') && request('type') != "0") {
            $result->where("panel_type", "like", request('type'));
        }
        $result = $result->where('panel_type', '!=', 40);
        if (\request()->has('date_from') && \request('date_from') != "")
            $result->where('created_at', '>=', date("Y-m-d H:i:s", jalali_to_timestamp(\request('date_from'))));

        if (\request()->has('date_to') && \request('date_to') != "")
            $result->where('created_at', '<=', date("Y-m-d H:i:s", jalali_to_timestamp(\request('date_to'), '/', false)));
        if (request()->has('status') && request('status') != "0") {
            $mode_array = [2 => 0, 1 => 1];
            $result->where("is_active", $mode_array[request('status')]);
        }
        $result = $result->paginate(20);
        return showData(view('admin.manage.collections', compact('result')));
    }

    public function isActive(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            $result = DB::table('users')
                ->where('user_id', $id)->first();
            if ($result->is_active == 1) {
                DB::table('users')
                    ->where('user_id', $id)
                    ->update(['is_active' => 0]);
            } else {
                DB::table('users')
                    ->where('user_id', $id)
                    ->update(['is_active' => 1]);
            }
            if ($request->ajax())
                return json_encode(["status" => 100, 'msg' => 'با موفقیت انجام شد!']);
            return \redirect()->back()->with("success", 'با موفقیت انجام شد!');
        }
    }

    public function editPorsant(Request $request)
    {
        if (DB::table('users')
            ->where('user_id', $request->id)
            ->update(['porsant_percent' => $request->percent])
        )
            return json_encode(["status" => 100, 'msg' => 'با موفقیت انجام شد!']);
        return json_encode(["status" => 500, 'msg' => 'خطا در ثبت اخطار، مجددا تلاش فرمائید!']);
    }

    public function searchUser()
    {
        $panel_type = Baseinfo::where('bas_type', 'panel_type')->where('bas_parent_id', '!=', 0)
            ->where('bas_id', '!=', 40)->where('bas_id', '!=', 39)->get();

        $result = DB::table('users');
        $result->leftJoin('persons', 'prsn_user_id', 'user_id');
        $result->leftJoin('collection_persons', 'cp_prsn_id', 'prsn_id')
            ->leftJoin('collections', 'coll_id', 'cp_coll_id');
        $is_any_filter_exist = false;

        if (request()->has('username') && request('username') != "") {
            $result->where("username", "like", '%' . request('username') . '%');
            $is_any_filter_exist = true;
        }
        if (request()->has('panel_type') && request('panel_type') != "0") {
            $result->where("panel_type", "like", request('panel_type'));
            $is_any_filter_exist = true;
        }
        if (request()->has('post') && request('post') != "") {
            $result->where("prsn_post_code", "like", '%' . request('post') . '%');
            $is_any_filter_exist = true;
        }
        if (request()->has('nationalCode') && request('nationalCode') != "") {
            $result->where("prsn_national_code", "like", '%' . request('nationalCode') . '%');
            $is_any_filter_exist = true;
        }
        if (request()->has('mobile') && request('mobile') != "") {
            $result->where("prsn_mobile1", "like", '%' . request('mobile') . '%');
            $is_any_filter_exist = true;
        }
        if (request()->has('name') && request('name') != "") {
            $result->where("coll_name", "like", '%' . request('name') . '%');
            $is_any_filter_exist = true;
        }

        if ($is_any_filter_exist == false)
            $result->whereRaw("0 = 1");

        $result = $result->paginate(10);
        return showData(view('admin.manage.search_user', compact('panel_type', 'result')));
    }

    public function createAdvertising(Request $request)
    {
        $panel_type = Baseinfo::where('bas_type', 'panel_type')->where('bas_id', '!=', 39)->where('bas_id', '!=', 40)->get();
        $advertising = Baseinfo::where('bas_type', 'advertising')->where('bas_parent_id', '!=', 0)->get();
        $rol = Baseinfo::where('bas_type', 'type_class')->where('bas_parent_id', '!=', 0)->get();

        $temp = "{}";
        if ($request->isMethod('post')) {
            $request->validate([
                'date_from' => ['required'],
                'date_to' => ['required'],
                'collection_id' => ['required'],
                'station' => ['required'],
                'panel_type' => function ($attribute, $value, $fail) {
                    if (empty(\request('panel'))) {
                        $fail('هیچ پنلی انتخاب نشده است');
                    }
                },
                'senf' => function ($attribute, $value, $fail) {
                    if (empty(\request('rol'))) {
                        $fail('هیچ صنفی انتخاب نشده است');
                    }
                },
                'image' => ['required'],
                'selected_cities' => [function ($attribute, $value, $fail) {
                    if (!request()->has('selected_cities') || \request('selected_cities') == '') {
                        $fail('هیچ شهری انتخاب نشده است.');
                    }
                }]
            ]);
            $path = uploadFile($request->file('image'), 'advertising', user("user_id"));

            UploadRoute::create([
                'ur_path' => $path,
                'ur_fk_id' => user("user_id"),
                'ur_collection_id' => collection_id(),
                'ur_description' => null,
                'ur_type_id' => 1,
                'ur_table_name' => 'advertising',
            ]);

            $ids = explode(',', request()->get('selected_cities'));

            $panel = [];
            foreach ($request->panel as $key => $panel_type) {
                foreach ($ids as $city) {
                    foreach ($request->rol as $rol) {
                        array_push($panel, [
                            'PT' => $panel_type,
                            'CT' => $city,
                            'CL' => $rol,
                        ]);
                    }
                }
            }
            $result = call_sp("add_advertising", null, [
                'mode' => 1,
                'values' => createRootData($panel),
                'path' => $path,
                'creator' => user('user_id'),
                'created_at' => time(),
                'start_at' => jalali_to_timestamp($request->date_from),
                'expire_at' => strtotime(date("Y-m-d " . ((request()->has('time') && request()->get('time') != '') ? request()->get('time') : '23:59') . ":59", jalali_to_timestamp(request('date_to')))),
                'collection_id' => $request->collection_id,
                'station_id' => $request->station,
                'link' => $request->link,
                'description' => $request->description,
            ]);
            if ($result[0]['result'] == 0)
                return json_encode(['status' => 100, 'msg' => 'با موفقیت انجام شد!']);
            return json_encode(['status' => 500, 'msg' => 'خطا در ثبت مجددا تلاش فرمائید!']);
        }
        return view('admin.manage.create_advertising', compact('temp', 'panel_type', 'advertising', 'rol'));
    }

    public function listAdvertising()
    {
        $result = DB::table('advertising_master')
            ->select([
                'coll_name',
                'adm_id',
                'adm_start_at',
                'adm_expire_at',
                'adm_creator',
                'adm_station_id',
                'prsn_name',
                'prsn_family',
                'bas_value',
            ]);
        if (\request()->has('date_from') && \request('date_from') != "")
            $result = $result->where('adm_start_at', '>=', jalali_to_timestamp(\request('date_from')));
        if (\request()->has('date_to') && \request('date_to') != "")
            $result = $result->where('adm_expire_at', '>=', jalali_to_timestamp(\request('date_to')));
        $result = $result->leftJoin('collections', 'coll_id', 'adm_collection_id');
        if (request()->has('name') && request('name') != "") {
            $result = $result->where("coll_name", "like", '%' . request('name') . '%');
        }
        $result = $result->leftJoin('persons', 'prsn_user_id', 'adm_creator');
        if (request()->has('nameCreator') && request('nameCreator') != "") {
            $result = $result->where(function ($query) {
                $query->where("prsn_name", "like", '%' . request('nameCreator') . '%')
                    ->orWhere("prsn_family", "like", '%' . request('nameCreator') . '%');
            });
        }
        $result = $result->leftJoin('baseinfos', 'bas_id', 'adm_station_id');
        if (request()->has('station') && request('station') != "") {
            $result = $result->where("bas_id", "like", '%' . request('station') . '%');
        }
        $result = $result->paginate(10);

        return showData(view('admin.manage.list_advertising', compact('result')));
    }

    public function editAdvertising(Request $request, $id)
    {
        $panel_type = Baseinfo::where('bas_type', 'panel_type')->where('bas_id', '!=', 39)->get();
        $advertising = Baseinfo::where('bas_type', 'advertising')->where('bas_parent_id', '!=', 0)->get();
        $rol = Baseinfo::where('bas_type', 'type_class')->where('bas_parent_id', '!=', 0)->get();
        $master = DB::table('advertising_master')
            ->where('adm_id', $id)
            ->leftJoin('collections', 'coll_id', 'adm_collection_id')
            ->first();

        $city = DB::table('advertising_detail')
            ->where('add_master_id', $id)
            ->leftJoin('cities as c', 'c.c_id', 'add_city_id')
            ->leftJoin('cities as p', 'p.c_id', 'c.c_parent_id')
            ->select(["c.c_id", 'c.c_name as city', 'p.c_name as state'])
            ->groupBy("c.c_id", "c.c_name", 'p.c_name')
            ->get()->toArray();
        $panel_type_old = DB::table('advertising_detail')
            ->where('add_master_id', $id)
            ->leftJoin('baseinfos', 'add_panel_type', 'bas_id')
            ->select(["bas_id"])
            ->groupBy("bas_id")
            ->get()->toArray();
        $senf = DB::table('advertising_detail')
            ->where('add_master_id', $id)
            ->leftJoin('baseinfos', 'add_senf_id', 'bas_id')
            ->select(["bas_id", "bas_value"])
            ->groupBy("bas_id", "bas_value")
            ->get()->toArray();
        //$result = $result->get()->toArray();


        $city = json_decode(json_encode($city), true);
        $temp = [];
        foreach ($city as $row) {
            $temp[$row['state']][] = [
                "id" => $row['c_id'],
                "name" => $row['city']
            ];
        }
        foreach ($temp as $key => $value) {
            $temp[$key] = [
                'text' => array_column($value, 'name'),
                'ids' => array_column($value, 'id'),
            ];
        }

        $temp = empty($temp) ? "{}" : json_encode($temp, JSON_UNESCAPED_UNICODE);
        $panel_type_old = array_column(json_decode(json_encode($panel_type_old), true), 'bas_id');
        $senf = array_column(json_decode(json_encode($senf), true), 'bas_id');
//        echo "<pre>";
//        print_r($panel_type_old);
//        exit();
        if ($request->isMethod('post')) {
            $request->validate([
                'date_from' => ['required'],
                'date_to' => ['required'],
                'collection_id' => ['required'],
                'station' => ['required'],
                'selected_cities' => [function ($attribute, $value, $fail) {
                    if (!request()->has('selected_cities') || \request('selected_cities') == '') {
                        $fail('هیچ شهری انتخاب نشده است.');
                    }
                }]
            ]);
            if (request()->has('image')) {
                $path = uploadFile($request->file('image'), 'advertising', user("user_id"));

                UploadRoute::create([
                    'ur_path' => $path,
                    'ur_fk_id' => user("user_id"),
                    'ur_collection_id' => collection_id(),
                    'ur_description' => null,
                    'ur_type_id' => 1,
                    'ur_table_name' => 'advertising',
                ]);
            } else
                $path = null;
            $ids = explode(',', request()->get('selected_cities'));

            $panel = [];
            foreach ($request->panel as $key => $panel_type) {
                foreach ($ids as $city) {
                    foreach ($request->rol as $rol) {
                        array_push($panel, [
                            'PT' => $panel_type,
                            'CT' => $city,
                            'CL' => $rol,
                        ]);
                    }
                }
            }

            $result = call_sp("add_advertising", null, [
                'mode' => 2,
                'id' => $id,
                'values' => createRootData($panel),
                'path' => $path,
                'creator' => user('user_id'),
                'created_at' => time(),
                'start_at' => jalali_to_timestamp($request->date_from),
                'expire_at' => strtotime(date("Y-m-d " . ((request()->has('time') && request()->get('time') != '') ? request()->get('time') : '23:59') . ":59", jalali_to_timestamp(request('date_to')))),
                'collection_id' => $request->collection_id,
                'station_id' => $request->station,
                'link' => $request->link,
                'description' => $request->description,
            ]);
            if ($result[0]['result'] == 0)
                return json_encode(['status' => 100, 'msg' => 'با موفقیت انجام شد!']);
            return json_encode(['status' => 500, 'msg' => 'خطا در ثبت مجددا تلاش فرمائید!']);
        }
        return view('admin.manage.create_advertising', compact('panel_type', 'advertising', 'temp', 'id', 'rol', 'master', 'senf', 'panel_type_old'));
    }

    public function changePasswordUser(Request $request)
    {
        if (DB::table('users')
            ->where('user_id', $request->id)
            ->update([
                'password' => Hash::make($request->password)
            ]))
            return json_encode(["status" => 100, 'msg' => 'با موفقیت انجام شد!']);
        return json_encode(["status" => 500, 'msg' => 'خطا در ثبت ، مجددا تلاش فرمائید!']);
    }

    public function wallet($id)
    {
        $result = DB::table('bag_transacts')
            ->select([
                'prsn_name',
                'prsn_family',
                'bt_time',
                'bt_amount',
                'bt_description',
                'username',
                'bt_status'
            ])
            ->where('bt_user_id', $id)->where('bt_type', 2)
            ->leftJoin('persons', 'prsn_user_id', 'bt_creator')
            ->leftJoin('users', 'prsn_user_id', 'user_id')
            ->latest('bt_id')
            ->paginate(10);
        $find_amount = User::find($id);
        return showData(view('admin.manage.wallet', compact('result', 'id', 'find_amount')));
    }

    public function updateWallet(Request $request)
    {
        $request->validate([
            'amount' => ['required', 'regex:/^[0-9]+$/'],
        ]);
        $result = call_sp("bag_charge_by_another_user", null, [
            'mode' => $request->type,
            'charger_user_id' => user('user_id'),
            'user_id' => $request->id,
            'amount' => $request->amount,
            'created_at' => time()
        ]);
        if (isset($result))
            if ($result[0]['result'] == 0)
                return json_encode(['status' => 100, 'msg' => 'با موفقیت ثبت گردید']);
        return json_encode(['status' => 500, 'msg' => 'خطا در ثبت']);

    }

    public function listCollectionAjax()
    {
        $result = DB::table('collections')
            ->select([
                "coll_id",
                "coll_hide",
                "users.*",
                "persons.*",
                DB::raw("concat(coll_name,' ','(',username,')') as coll_name")
            ]);
        if (request()->has('name') && request('name') != "") {
            $result->where("coll_name", "like", '%' . request('name') . '%');
        }
        $result = $result->leftJoin('collection_persons', 'cp_coll_id', 'coll_id')
            ->leftJoin('persons', 'prsn_id', 'cp_prsn_id');
        if (request()->has('phone') && request('phone') != "") {
            $result->where("prsn_phone1", "like", '%' . request('phone') . '%');
        }
        $result = $result->leftJoin('users', 'user_id', 'prsn_user_id');
        if (request()->has('type') && request('type') != "0") {
            $result->where("panel_type", "like", request('type'));
        }
        $result = $result->where('panel_type', '!=', 40);
        if (\request()->has('date_from') && \request('date_from') != "")
            $result->where('created_at', '>=', date("Y-m-d H:i:s", jalali_to_timestamp(\request('date_from'))));

        if (\request()->has('date_to') && \request('date_to') != "")
            $result->where('created_at', '<=', date("Y-m-d H:i:s", jalali_to_timestamp(\request('date_to'), '/', false)));
        if (request()->has('status') && request('status') != "0") {
            $mode_array = [2 => 0, 1 => 1];
            $result->where("is_active", $mode_array[request('status')]);
        }
        $result = $result->paginate(20);
        return showData(view('admin.manage.collections_ajax', compact('result')));
    }

    public function searchUserTransaction()
    {
        $panel_type = Baseinfo::where('bas_type', 'panel_type')->where('bas_parent_id', '!=', 0)
            ->where('bas_id', '!=', 40)->where('bas_id', '!=', 39)->get();

        $result = DB::table('users');
        $result->leftJoin('persons', 'prsn_user_id', 'user_id');
        $result->leftJoin('collection_persons', 'cp_prsn_id', 'prsn_id')
            ->leftJoin('collections', 'coll_id', 'cp_coll_id');
        $is_any_filter_exist = false;

        if (request()->has('username') && request('username') != "") {
            $result->where("username", "like", '%' . request('username') . '%');
            $is_any_filter_exist = true;
        }
        if (request()->has('panel_type') && request('panel_type') != "0") {
            $result->where("panel_type", "like", request('panel_type'));
            $is_any_filter_exist = true;
        }
        if (request()->has('post') && request('post') != "") {
            $result->where("prsn_post_code", "like", '%' . request('post') . '%');
            $is_any_filter_exist = true;
        }
        if (request()->has('nationalCode') && request('nationalCode') != "") {
            $result->where("prsn_national_code", "like", '%' . request('nationalCode') . '%');
            $is_any_filter_exist = true;
        }
        if (request()->has('mobile') && request('mobile') != "") {
            $result->where("prsn_mobile1", "like", '%' . request('mobile') . '%');
            $is_any_filter_exist = true;
        }
        if (request()->has('name') && request('name') != "") {
            $result->where("coll_name", "like", '%' . request('name') . '%');
            $is_any_filter_exist = true;
        }

        if ($is_any_filter_exist == false)
            $result->whereRaw("0 = 1");

        $result = $result->paginate(10);
        return showData(view('admin.manage.search_user_transaction', compact('panel_type', 'result')));
    }

    public function deleteRoleSupportForm(Request $request, $id)
    {
        if (DB::table('user_roles')
            ->where('ur_rol_id', $id)
            ->where('ur_user_id', $request->id)
            ->delete())
            return \redirect()->back()->with("success", "عملیات باموفقیت انجام شد!");
        return \redirect()->back()->with("error", config('first_config.message.relation_delete'));
    }

    public function createTransactionSystem(Request $request)
    {
        $type_payment = Baseinfo::where('bas_type', 'type_payment')->where('bas_parent_id', '!=', 0)->get();
        $type_request = Baseinfo::where('bas_type', 'transaction_type')->where('bas_parent_id', '!=', 0)->get();
        if ($request->isMethod('post')) {
            $request->validate([
                'type_request' => ['required'],
                'type_payment' => ['required'],
                'collection_id' => ['required'],
                'amount' => ['required'],
                'date' => ['required'],
                'number_follow' => ['required'],
            ]);
            if (DB::table('deposited_porsants')
                ->insert([
                    'dp_received_collection_id' => $request->collection_id,
                    'dp_amount' => $request->amount,
                    'dp_date' => strtotime(date("Y-m-d " . ((request()->has('time') && request()->get('time') != '') ? request()->get('time') : '23:59') . ":59", jalali_to_timestamp(request('date')))),
                    'dp_payment_type' => $request->type_payment,
                    'dp_tracking_code' => $request->number_follow,
                    'dp_creator_id' => user('user_id'),
                    'dp_created_at' => date("Y/m/d H:i:s"),
                    'dp_type' => $request->type_request,
                    'dp_description' => $request->description,
                ]))
                return json_encode(['status' => 100, 'msg' => 'با موفقیت انجام شد!']);
            return json_encode(['status' => 500, 'msg' => 'خطا در ثبت اطلاعات!']);
        }
        return view('admin.manage.transaction', compact('type_payment', 'type_request'));
    }

    public function listTransaction()
    {
        $type_request = Baseinfo::where('bas_type', 'transaction_type')->where('bas_parent_id', '!=', 0)->get();
        $result = DB::table('deposited_porsants');
        if (request()->has('amount') && request('amount') != "") {
            $result = $result->where("dp_amount", ">=", request('amount'));
        }

        if (request()->has('issueTracking') && request('issueTracking') != "") {
            $result = $result->where("dp_tracking_code", "like", '%' . request('issueTracking') . '%');
        }

        if (\request()->has('date') && \request('date') != "")
            $result = $result->where('dp_date', '>=', jalali_to_timestamp(\request('date')));


        $result->leftJoin('collections', 'dp_received_collection_id', 'coll_id');
        if (request()->has('collection') && request('collection') != "") {
            $result = $result->where("coll_name", "like", '%' . request('collection') . '%');
        }

        $result->leftJoin('baseinfos', 'dp_type', 'bas_id');

        if (request()->has('mode') && request('mode') != "") {
            $result = $result->where("bas_id", request('mode'));
        }
        $result->leftJoin('persons', 'dp_creator_id', 'prsn_user_id');
        if (request()->has('nameCreator') && request('nameCreator') != "") {
            $result = $result->where(function ($query) {
                $query->where("prsn_name", "like", '%' . request('nameCreator') . '%')
                    ->orWhere("prsn_family", "like", '%' . request('nameCreator') . '%');
            });
        }
        $result = $result->paginate(10);

        return showData(view('admin.manage.list_transaction', compact('result', 'type_request')));
    }

    public function porsantCollection(Request $request, $id)
    {
        $return = DB::table('collection_management_porsant')
            ->where("cmp_collection_id", $id)
            ->get()->toArray();
        $return = json_decode(json_encode($return), true
        );
        if ($request->isMethod('post')) {

            $array_price = [];
            foreach (request()->get('price_from') as $key => $value) {
                $array_price[$key]["from"] = $value;
            }
            foreach (request()->get('price_to') as $key => $value) {
                $array_price[$key]["to"] = $value;
            }
            $array = [
                'percentage.*' => ['required', 'regex:/^[1-9]*[.]*[0-9]+$/'],
                'price_from.*' => ['required', 'regex:/^[0-9]+$/',],
                'price_to.*' => ['required', 'regex:/^[0-9]+$/'],
            ];
            $messages = [];
            foreach (request()->get('price_from') as $key => $value) {
                $array['price_from.' . $key] = [];
                $array['price_to.' . $key] = [];
                array_push($array['price_from.' . $key], function ($attribute, $val, $fail) use ($array_price, $key, $value) {
                    foreach ($array_price as $k => $v) {
                        if ($key != $k) {
                            if ($value > $v['from'] && $value < $v['to'])
                                $fail('تداخل در بازه مورد نظر');
                        }
                    }
                });
                if (request()->get('price_from')[$key] == "") {
                    array_push($array['price_from.' . $key], function ($attribute, $val, $fail) {
                        if ($val == "") {
                            $fail('مقدار را وارد کنید');
                        }
                    });
                } else if (request()->get('price_to')[$key] == "" || request()->get('price_to')[$key] == 0) {
                    array_push($array['price_to.' . $key], function ($attribute, $val, $fail) {
                        if ($val == "") {
                            $fail('مقدار را وارد کنید');
                        }
                    });
                }
            }
            $request->validate($array, $messages);
            try {
                DB::table('collection_management_porsant')
                    ->where('cmp_collection_id', $id)
                    ->delete();
                foreach ($request->percentage as $key => $percent) {
                    if ($percent != '') {
                        $result = DB::table('collection_management_porsant')
                            ->insert([
                                'cmp_collection_id' => $id,
                                'cmp_price_from' => $request->price_from[$key],
                                'cmp_price_to' => $request->price_to[$key],
                                'cmp_percent' => $percent,
                            ]);
                    }
                }
                if ($result)
                    return json_encode(['status' => 100, 'msg' => 'با موفقیت انجام شد!']);
                return json_encode(['status' => 500, 'msg' => 'خطا در ثبت اطلاعات!']);
            } catch (Exception $e) {
                return json_encode(['status' => 500, 'msg' => 'خطا در ثبت اطلاعات!']);
            }
        }
        return view("admin.manage.porsant_collection", compact('return'));
    }

    public function walletHistory()
    {
        return view("admin.manage.wallet_history");
    }

    public function checkoutRequestList(Request $request)
    {
        $result = DB::table('checkout_request')
            ->select([
                'rc_id',
                'prsn_name',
                'prsn_family',
                'coll_name',
                'panel_type',
                'rc_created_at',
                'rc_price',
            ])
            ->where('rc_is_viewed', '0')
            ->leftJoin('users', 'user_id', 'rc_user_id')
            ->leftJoin('persons', 'prsn_user_id', 'user_id')
            ->leftJoin('collection_persons', 'cp_prsn_id', 'prsn_id')
            ->leftJoin('collections', 'coll_id', 'cp_coll_id');
        if (request()->has('name') && request('name') != "") {
            $result = $result->where(function ($query) {
                $query->where("prsn_name", "like", '%' . request('name') . '%')
                    ->orWhere("prsn_family", "like", '%' . request('name') . '%');
            });
        }
        if (request()->has('coll_name') && request('coll_name') != "") {
            $result->where("coll_name", "like", '%' . request('coll_name') . '%');
        }
        if (request()->has('panel_type') && request('panel_type') != "") {
            $result->where("panel_type", "like", '%' . request('panel_type') . '%');
        }
        if (\request()->has('date') && \request('date') != "")
            $result->where('rc_created_at', '<=', jalali_to_timestamp(request('date'), '/', false));

        if (request()->has('amount') && request('amount') != "") {
            $result->where("rc_price", '>=', request('amount'));
        }
        $result = $result->paginate(10);

        if ($request->isMethod('post')) {
            foreach ($request->view as $key => $view) {
                if (DB::table('checkout_request')
                    ->where('rc_id', $key)
                    ->update([
                        "rc_is_viewed" => 1
                    ]))
                    return json_encode(['status' => 100, 'msg' => 'با موفقیت انجام شد!']);
                return json_encode(['status' => 500, 'msg' => 'خطا در ثبت اطلاعات!']);
            }
        }
        return showData(view("admin.manage.checkout_request_list", compact('result')));
    }

    public function editShopkeeper(Request $request,$id)
    {
        if (findUser($id)->panel_type == 44) {
            $persons = findUser($id, 'persons');
            $collection = findUser($id, 'collection', 'all');
            $avatar = DB::table('upload_route')
                ->select(['ur_path'])
                ->where('ur_table_name', 'avatar')
                ->where('ur_collection_id', $collection->coll_id)
                ->first();
            $old_senfs = array_column(json_decode(json_encode(DB::table('collection_classes')->select([
                'coc_typeClass_id'
            ])
                ->where('coc_coll_id', findUser($id, 'collection', 'id'))
                ->get()), true), 'coc_typeClass_id');
            if ($request->isMethod('post')) {
                $request->validate([
                    'collection.*'=>'required',
                    'persons.*'=>'required',
                    'class' => function ($attribute, $value, $fail) {
                        if (\request('shop_keeper_class')=="")
                            $fail('حداقل یک صنف باید وارد شود');
                    }
                ]);
                try {
                    Person::find($persons->prsn_id)->update($request->persons);
                    Collection::find($collection->coll_id)->update($request->collection);
                    if ($request->has('file')) {
                        $route = uploadFile($request->file('file'), 'avatar', $id);
                        $up = UploadRoute::create([
                            'ur_path' => $route,
                            'ur_fk_id' => $id,
                            'ur_collection_id' => $collection->coll_id,
                            'ur_type_id' => 1,
                            'ur_table_name' => 'avatar',
                        ]);
                    }
                    DB::table('collection_classes')
                        ->where('coc_coll_id', $collection->coll_id)->delete();
                    foreach ($request->shop_keeper_class as $row) {
                        DB::table('collection_classes')
                            ->insert([
                                'coc_coll_id' => $collection->coll_id,
                                'coc_typeClass_id' => $row
                            ]);
                    }
                    return json_encode(['status' => 100, 'msg' => 'با موفقیت انجام شد!']);
                } catch (\Exception $e) {
                    return json_encode(['status' => 500, 'msg' => 'خطا در ثبت اطلاعات!']);
                }
            }
            return view('admin.manage.edit_shopkeeper', compact('persons', 'collection', 'old_senfs', 'states', 'avatar'));
        }else
            abort(404);
    }

    public function editVisitor(Request $request,$id)
    {
        if (findUser($id)->panel_type == 45) {
        $persons=findUser($id,'persons');
        $collection=findUser($id,'collection','all');
        $avatar=DB::table('upload_route')
            ->select(['ur_path'])
            ->where('ur_table_name','avatar')
            ->where('ur_collection_id',$collection->coll_id)
            ->first();
        $old_senfs= array_column(json_decode(json_encode(DB::table('collection_classes')->select([
            'coc_typeClass_id'
        ])
            ->where('coc_coll_id', findUser($id, 'collection', 'id'))
            ->get()), true), 'coc_typeClass_id');
        if($request->isMethod('post')){
            $request->validate([
                'collection.*'=>'required',
                'persons.*'=>'required',
                'class' => function ($attribute, $value, $fail) {
                if (\request('shop_keeper_class')=="")
                        $fail('حداقل یک صنف باید وارد شود');
                }
            ]);
            try {
                Person::find($persons->prsn_id)->update($request->persons);
                Collection::find($collection->coll_id)->update($request->collection);
                if($request->has('file')){
                    $route = uploadFile($request->file('file'), 'avatar', $id);
                    $up= UploadRoute::create([
                        'ur_path' => $route,
                        'ur_fk_id' => $id,
                        'ur_collection_id' => $collection->coll_id,
                        'ur_type_id' => 1,
                        'ur_table_name' => 'avatar',
                    ]);
                }
                DB::table('collection_classes')
                    ->where('coc_coll_id',$collection->coll_id)->delete();
                foreach ($request->shop_keeper_class as $row) {
                    DB::table('collection_classes')
                        ->insert([
                            'coc_coll_id'=>$collection->coll_id,
                            'coc_typeClass_id'=>$row
                        ]);
                }
                return json_encode(['status' => 100, 'msg' => 'با موفقیت انجام شد!']);
            }catch (\Exception $e){
                return json_encode(['status' => 500, 'msg' => 'خطا در ثبت اطلاعات!']);
            }
        }
        return view('admin.manage.edit_visitor',compact('persons','collection','old_senfs','states','avatar'));
    }else
        abort(404);
        }

    public function loginPanelUser(Request $request)
    {
        Session::put('is_admin',Auth::user()->user_id);
        Auth::loginUsingId($request->id);
        return json_encode(['status'=>100,'url'=>'/']);
    }
}
