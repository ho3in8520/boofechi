<?php

namespace App\Http\Controllers;

use App\AreaSupport;
use App\Baseinfo;
use App\Collection;
use App\PanelWizard;
use App\PanelWizardCompleted;
use App\Person;
use App\Terms;
use App\SystemFactorMaster;
use App\SystemFactorDetail;
use App\UploadRoute;
use App\User;
use Egulias\EmailValidator\Exception\ExpectingCTEXT;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;


class CollectionController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:list_user', ['only' => ['usersList']]);
        $this->middleware('permission:create_user', ['only' => ['createUser']]);
        $this->middleware('permission:edit_user', ['only' => ['updateUser']]);
        $this->middleware('permission:delete_user', ['only' => ['deleteUser']]);
        $this->middleware('permission:return-reasons', ['only' => ['return_reasons']]);
        $this->middleware('permission:payment-methods', ['only' => ['paymentMethods']]);
        $this->middleware('permission:online-gates', ['only' => ['onlineGates']]);
        $this->middleware('permission:festival_discounts', ['only' => ['festival_discounts']]);
        $this->middleware('permission:factor_eshant', ['only' => ['factor_eshant']]);
        $this->middleware('permission:system-factors', ['only' => ['listFactorSystem']]);
        $this->middleware('permission:list_massage', ['only' => ['listMassage']]);
        $this->middleware('permission:view_massage', ['only' => ['viewMassage']]);
        $this->middleware('permission:setting_user', ['only' => ['settingUser']]);
        $this->middleware('permission:edit_area_support_company', ['only' => ['editAreaSupportCompany']]);
    }

    public function onlineGates(Request $request)
    {
        if (\request()->isMethod("post")) {
            dd($request->has('active'));
            if ($request->has('active')) {
                $selected_methods = array_keys($request->active);

                foreach ($selected_methods as $key) {
                    if (DB::table("collection_online_gates")->where("cog_gate_id", $key)->update([
                        'cog_status' => 1,
                    ])
                    )
                        continue;
                    else
                        if (DB::table("collection_online_gates")->insert([
                            'cog_status' => 1,
                            'cog_collection_id' => collection_id(),
                            'cog_gate_id' => $key,
                        ])
                        ) ;
                }
                DB::table('collection_online_gates')
                    ->whereNotIn('cog_gate_id', $selected_methods)
                    ->update([
                        'cog_status' => 0,
                    ]);
            } else {
                DB::table('collection_online_gates')
                    ->where('cog_collection_id', collection_id())
                    ->update([
                        'cog_status' => 0,
                    ]);
            }
            return json_encode(["status" => 100, 'msg' => "با موفقیت ثبت شد!"]);
            return json_encode(["status" => 500, 'msg' => "خطا در ثبت روش پرداخت، مجددا تلاش فرمائید!"]);

        }

        $online_gates = DB::table("online_gates")->where('og_status', 1)
            ->leftJoin('collection_online_gates', 'og_id', 'cog_gate_id')
            ->select([
                "og_title",
                "cog_status",
                "cog_id",
                "og_id",
            ])->get()->toArray();
        $online_gates = json_decode(json_encode($online_gates), true);
        $collection_online_gates = DB::table("collection_online_gates")
            ->select([
                "og_title",
                "cog_status",
                "cog_id",
                "og_id",

            ])
            ->join("online_gates", 'og_id', '=', 'cog_gate_id')
            ->where('og_status', 1)
            ->where('cog_collection_id', collection_id())->get();
        return showData(view('collection.online_gates', compact('online_gates', 'collection_online_gates')));
    }

    public function changeStatus($id)
    {
        if (DB::table("collection_online_gates")->where("cog_id", $id)->update([
            'cog_status' => request()->get('status'),
        ])
        )
            return json_encode(["status" => 100, 'msg' => "با موفقیت ثبت شد!"]);
        return json_encode(["status" => 500, 'msg' => "خطا در تغییر وضعیت، مجددا تلاش فرمائید!"]);
    }

    public function festival_discounts(Request $request, $id = null)
    {
        $return = DB::table('collection_factor_complimentary')
            ->where('cfc_collection_id', collection_id())
            ->where('cfc_type', '0')
            ->get();
        $return = json_decode(json_encode($return), true);

        if ($request->isMethod('post')) {
            $array_price = [];
            foreach (request()->get('price_from') as $key => $value) {
                $array_price[$key]["from"] = $value;
            }
            foreach (request()->get('price_to') as $key => $value) {
                $array_price[$key]["to"] = $value;
            }
            $array_date = [];
            foreach (request()->get('date_from') as $key => $value) {
                $array_date[$key]["from"] = $value;
            }
            foreach (request()->get('date_to') as $key => $value) {
                $array_date[$key]["to"] = $value;
            }
            $array = [
                'percentage.*' => ['required', 'regex:/^[1-9]*[.]*[0-9]+$/'],
                'price_from.*' => ['required', 'regex:/^[0-9]+$/',],
                'price_to.*' => ['required', 'regex:/^[0-9]+$/'],
                'date_from.*' => ['regex:/^[1-9][0-9]{3}\/(0[1-9]|1[0-2])\/(0[1-9]|[1-2][0-9]|3[0-1])$/'],
                'date_to.*' => ['regex:/^[1-9][0-9]{3}\/(0[1-9]|1[0-2])\/(0[1-9]|[1-2][0-9]|3[0-1])$/',],
            ];
            $messages = [];
            foreach (request()->get('price_from') as $key => $value) {
                $array['price_from.' . $key] = [];
                $array['price_to.' . $key] = [];
                array_push($array['price_from.' . $key], function ($attribute, $val, $fail) use ($array_price, $key, $value) {
                    foreach ($array_price as $k => $v) {
                        if ($key != $k) {
                            if ($value > $v['from'] && $value < $v['to'])
                                $fail('تداخل در بازه مورد نظر');
                        }
                    }
                });
                if (request()->get('price_from')[$key] == "") {
                    array_push($array['price_from.' . $key], function ($attribute, $val, $fail) {
                        if ($val == "") {
                            $fail('مقدار را وارد کنید');
                        }
                    });
                } else if (request()->get('price_to')[$key] == "" || request()->get('price_to')[$key] == 0) {
                    array_push($array['price_to.' . $key], function ($attribute, $val, $fail) {
                        if ($val == "") {
                            $fail('مقدار را وارد کنید');
                        }
                    });
                }
            }

            foreach (request()->get('date_from') as $key => $value) {
                $array['date_from.' . $key] = [];
                $array['date_to.' . $key] = [];
                array_push($array['date_from.' . $key], function ($attribute, $val, $fail) use ($array_date, $key, $value) {
                    foreach ($array_date as $k => $v) {
                        if ($key != $k) {
                            if ($value > $v['from'] && $value < $v['to'])
                                $fail('تداخل در بازه مورد نظر');
                        }
                    }
                });
                if (request()->get('date_from')[$key] == "") {
                    array_push($array['date_from.' . $key], function ($attribute, $val, $fail) {
                        if ($val == "") {
                            $fail('مقدار را وارد کنید');
                        }
                    });
                } else if (request()->get('date_to')[$key] == "" || request()->get('date_to')[$key] == 0) {
                    array_push($array['date_to.' . $key], function ($attribute, $val, $fail) {
                        if ($val == "") {
                            $fail('مقدار را وارد کنید');
                        }
                    });
                }
            }

            $request->validate($array, $messages);

            if (!$return) {
                $array = [];
                foreach ($request->percentage as $key => $percent) {
                    if ($percent != '') {
                        array_push($array, [
                            'DTF' => jalali_to_timestamp($request->date_from[$key]),
                            'DTT' => jalali_to_timestamp($request->date_to[$key], '/', false),
                            'BUF' => $request->price_from[$key],
                            'BUT' => $request->price_to[$key],
                            'PRC' => $percent,
                        ]);
                    }
                }
                $result = call_sp("collection_factor_complimentary_sp", null, [
                    'mode' => 0,
                    'type' => 0,
                    'values' => createRootData($array),
                    'collection_id' => collection_id(),
                ]);
                if (isset($result))
                    if ($result[0]['result'] == 0)
                        return json_encode(['status' => 100, 'msg' => 'اطلاعات با موفقیت ثبت شد']);
                return json_encode(['status' => 500, 'msg' => 'خطا در ثبت اطلاعات']);
            } else {
                $array = [];
                foreach ($request->percentage as $key => $percent) {
                    if ($percent != '') {
                        array_push($array, [
                            'DTF' => jalali_to_timestamp($request->date_from[$key]),
                            'DTT' => jalali_to_timestamp($request->date_to[$key], '/', false),
                            'BUF' => $request->price_from[$key],
                            'BUT' => $request->price_to[$key],
                            'PRC' => $percent,
                        ]);
                    }
                }
                $result = call_sp("collection_factor_complimentary_sp", null, [
                    'mode' => 1,
                    'type' => 0,
                    'values' => createRootData($array),
                    'collection_id' => collection_id(),
                ]);
                if (isset($result))
                    if ($result[0]['result'] == 0)
                        return json_encode(['status' => 100, 'msg' => 'اطلاعات با موفقیت ثبت شد']);
                return json_encode(['status' => 500, 'msg' => 'خطا در ثبت اطلاعات']);

            }
        }

        if ($request->isMethod('delete')) {
            DB::table('collection_factor_complimentary')
                ->where("cfc_id", $id)
                ->delete();
            return redirect()->back()->with(session()->flash('success', 'با موفقیت حذف گردید'));


        }
        return view('collection.festival_discounts', compact('return'));
    }

    public function paymentMethods(Request $request)
    {
        //##??#?##??#?##??##??##??##??##??##??##??##??##??##?
        //delete nakonim method haro                        ?
        //agar khast update kone har record ro update konim ?
        //##??#?##??#?##??##??##??##??##??##??##??##??##??##?
        $old_pay = null;
        $methods = \Illuminate\Support\Facades\DB::table("baseinfos")->select([
            'bas_id',
            'bas_value'
        ])
            ->where('bas_type', 'type_payment')
            ->where('bas_parent_id', '!=', 0)
            ->where('bas_id', '!=', 50)
            ->get()->toArray();

        $old_pays = DB::table('collection_payment_methods')
            ->where('cpm_collection_id', collection_id())
            ->get();
        $old_pays = json_decode(json_encode($old_pays), true);
        $methods = json_decode(json_encode($methods), true);
        if ($request->isMethod('post')) {
            if ($request->has('is_active')) {
                $selected_methods = array_keys($request->is_active);
                foreach ($selected_methods as $key) {
                    $request->validate([
                        'percent.*' => ['nullable', 'regex:/^[1-9]*[.]*[0-9]+$/']
                    ]);
                    if ($request->percent == '') {
                        $percent = 0;
                    } else {
                        $percent = $request->percent;
                    }
                    if (DB::table('collection_payment_methods')
                        ->where('cpm_bas_id', $key)
                        ->where('cpm_collection_id', collection_id())
                        ->update([
                            'cpm_status' => 1,
                            'cpm_percent' => $percent[$key],
                            'cpm_description' => $request->get('description')[$key]
                        ])
                    )
                        continue;
                    else
                        DB::table('collection_payment_methods')
                            ->insert([
                                'cpm_collection_id' => collection_id(),
                                'cpm_bas_id' => $key,
                                'cpm_status' => 1,
                                'cpm_percent' => $percent[$key],
                                'cpm_description' => $request->get('description')[$key]
                            ]);
                }
                DB::table('collection_payment_methods')
                    ->whereNotIn('cpm_bas_id', $selected_methods)
                    ->update([
                        'cpm_status' => 0,
                        'cpm_percent' => 0,
                        'cpm_description' => null
                    ]);
            } else {

                DB::table('collection_payment_methods')
                    ->where('cpm_collection_id', collection_id())
                    ->update([
                        'cpm_status' => 0,
                        'cpm_percent' => 0,
                    ]);

            }
            return json_encode(['status' => 100, 'msg' => 'اطلاعات با موفقیت ثبت شد']);
        }
        return view('collection.payment_methods', compact('methods', 'old_pays'));
    }

    public function factor_eshant(Request $request, $id = null)
    {
        $return = DB::table('collection_factor_complimentary')
            ->where('cfc_collection_id', collection_id())
            ->where('cfc_type', '1')
            ->leftJoin('collection_products', 'cp_id', 'cfc_free_collection_product_id')
            ->leftJoin('products', 'cp_product_id', 'prod_id')
            ->leftJoin('baseinfos', 'bas_id', 'cp_measurement_unit_id')
            ->select([
                'cfc_id',
                'cfc_buy_from',
                'cfc_buy_to',
                'cfc_date_from',
                'cfc_date_to',
                'cfc_overflow',
                'cfc_free_count',
                'cfc_free_collection_product_id',
                'prod_name',
                'cp_count_per',
                'bas_value',
            ])->get();
        $return = json_decode(json_encode($return), true);

        if ($request->isMethod('post')) {
            $array_price = [];
            foreach (request()->get('price_from') as $key => $value) {
                $array_price[$key]["from"] = $value;
            }
            foreach (request()->get('price_to') as $key => $value) {
                $array_price[$key]["to"] = $value;
            }
            $array = [
                'pc_free_collection_product_id.*' => ['required'],
                'count.*' => ['required'],
            ];
            $messages = [];
            foreach (request()->get('price_from') as $key => $value) {
                $array['price_from.' . $key] = [];
                $array['price_to.' . $key] = [];
                array_push($array['price_from.' . $key], function ($attribute, $val, $fail) use ($array_price, $key, $value) {
                    foreach ($array_price as $k => $v) {
                        if ($key != $k) {
                            if ($value > $v['from'] && $value < $v['to'])
                                $fail('تداخل در بازه مورد نظر');
                        }
                    }
                });
                if (request()->get('price_from')[$key] == "") {
                    array_push($array['price_from.' . $key], function ($attribute, $val, $fail) {
                        if ($val == "") {
                            $fail('مقدار را وارد کنید');
                        }
                    });
                } else if (request()->get('price_to')[$key] == "" || request()->get('price_to')[$key] == 0) {
                    array_push($array['price_to.' . $key], function ($attribute, $val, $fail) {
                        if ($val == "") {
                            $fail('مقدار را وارد کنید');
                        }
                    });
                }
            }
            $request->validate($array, $messages);
            DB::table('collection_factor_complimentary')
                ->where('cfc_collection_id', collection_id())
                ->where('cfc_type', 1)
                ->delete();

            $create = [];
            foreach ($request->pc_free_collection_product_id as $key => $eshant) {
                if ($request->type[$key] == 0)
                    $count[$key] = $request->count[$key];
                else
                    $count[$key] = $request->count[$key] * $request->count_per[$key];
                array_push($create, [
                    'cfc_collection_id' => collection_id(),
                    'cfc_type' => 1,
                    'cfc_free_count' => $count[$key],
                    'cfc_free_collection_product_id' => $request->pc_free_collection_product_id[$key],
                    'cfc_buy_from' => $request->price_from[$key],
                    'cfc_buy_to' => $request->price_to[$key],
                    'cfc_date_from' => jalali_to_timestamp($request->pc_date_from[$key]),
                    'cfc_date_to' => jalali_to_timestamp($request->pc_date_to[$key], '/', false),
                    'cfc_overflow' => (isset($request->pc_overflow[$key])) ? 1 : 0,
                ]);
            }
            if (DB::table('collection_factor_complimentary')
                ->insert($create)
            ) {
                return json_encode(['status' => 100, 'msg' => 'اطلاعات با موفقیت ثبت گردید']);
            } else {
                return json_encode(['status' => 500, 'msg' => 'خطا در ثبت اطلاعات']);
            }
        }
        if ($request->isMethod('delete')) {
            DB::table('collection_factor_complimentary')
                ->where("cfc_id", $id)
                ->delete();
            return redirect()->back()->with(session()->flash('success', 'با موفقیت حذف گردید'));

        }
        return view('collection.factor_eshant', compact('return'));
    }

    public function commission(Request $request)
    {
        $old = DB::table('collection_management_porsant')->where('cmp_collection_id', collection_id())->get();
        if ($request->isMethod('post')) {
            $array_price = [];
            foreach (request()->get('price_from') as $key => $value) {
                $array_price[$key]["from"] = $value;
            }
            foreach (request()->get('price_to') as $key => $value) {
                $array_price[$key]["to"] = $value;
            }
            $array = [
                'percentage.*' => ['required'],
            ];
            $messages = [];
            foreach (request()->get('price_from') as $key => $value) {
                $array['price_from.' . $key] = [];
                $array['price_to.' . $key] = [];
                array_push($array['price_from.' . $key], function ($attribute, $val, $fail) use ($array_price, $key, $value) {
                    foreach ($array_price as $k => $v) {
                        if ($key != $k) {
                            if ($value > $v['from'] && $value < $v['to'])
                                $fail('تداخل در بازه مورد نظر');
                        }
                    }
                });
                if (request()->get('price_from')[$key] == "") {
                    array_push($array['price_from.' . $key], function ($attribute, $val, $fail) {
                        if ($val == "") {
                            $fail('مقدار را وارد کنید');
                        }
                    });
                } else if (request()->get('price_to')[$key] == "" || request()->get('price_to')[$key] == 0) {
                    array_push($array['price_to.' . $key], function ($attribute, $val, $fail) {
                        if ($val == "") {
                            $fail('مقدار را وارد کنید');
                        }
                    });
                }
            }
            $request->validate($array, $messages);
            if ($old) {
                $sp = [];
                foreach ($request->percentage as $key => $price) {
                    array_push($sp, [
                        'PRF' => $request->price_from[$key],
                        'PRT' => $request->price_to[$key],
                        'PRC' => $price
                    ]);
                }

                $result = call_sp("collection_management_porsant_sp", null, [
                    'mode' => 1,
                    'values' => createRootData($sp),
                    'collection_id' => collection_id(),
                ]);
                if ($result[0]['result'] == 0)
                    return json_encode(['status' => 100, 'msg' => 'با موفقیت انجام شد!']);
                return json_encode(['status' => 500, 'msg' => 'خطا در ثبت فاکتور، مجددا تلاش فرمائید!']);
            } else {
                $sp = [];
                foreach ($request->percentage as $key => $price) {
                    array_push($sp, [
                        'PRF' => $request->price_from[$key],
                        'PRT' => $request->price_to[$key],
                        'PRC' => $price
                    ]);
                }

                $result = call_sp("collection_management_porsant_sp", null, [
                    'mode' => 0,
                    'values' => createRootData($sp),
                    'collection_id' => collection_id(),
                ]);
                if ($result[0]['result'] == 0)
                    return json_encode(['status' => 100, 'msg' => 'با موفقیت انجام شد!']);
                return json_encode(['status' => 500, 'msg' => 'خطا در ثبت فاکتور، مجددا تلاش فرمائید!']);
            }
        }
        return view('collection.commission', compact('old'));
    }

    public
    function shopkeeper_notification_list(Request $request)
    {
        $result = DB::table('announcements')
            ->leftJoin('announcement_receivers', 'anr_announcement_id', 'ann_id')
            ->where('anr_type_id', user('panel_type'))
            ->where('anr_city_id', user()->person->prsn_city_id)
            ->where('ann_type', 0)
            ->leftJoin('collections', 'ann_collection_id', 'coll_id')
            ->select([
                'ann_id',
                'ann_title',
                'ann_body',
                'coll_name',
                'announcements.created_at',
                DB::raw("(select top 1 count(1) from announcement_viewed where av_ann_id = ann_id and av_user_id = " . user('user_id') . ") as is_viewed")

            ]);

        if (request()->has('companyName') && request('companyName') != "")
            $result->where("coll_name", "like", '%' . request('companyName') . '%');
        if (request()->has('status') && request('status') != "")
            if (request('status') != 1)
                $result->whereRaw('(select top 1 count(1) from announcement_viewed where av_ann_id = ann_id) = 0');
            else
                $result->whereRaw('(select top 1 count(1) from announcement_viewed where av_ann_id = ann_id) > 0');

        if (request()->has('no_date_from') && request('no_date_from') != "") {
            $from = date("Y-m-d 00:00:00", jalali_to_timestamp(request('no_date_from')));
            $result->where('created_at', '>=', $from);
        }

        if (request()->has('no_date_to') && request('no_date_to') != "") {
            $to = date("Y-m-d 23:59:59", jalali_to_timestamp(request('no_date_to')));
            $result->where('created_at', '<=', $to);
        }
        $result->orderBy('ann_id', 'desc');
        $result = $result->paginate(10);
        if ($request->isMethod('post')) {
            if ($request->view == null) {
                return json_encode(['status' => 500, 'msg' => 'موردی انتخاب نشده است']);
            }
            $view = array_keys($request->view);
            foreach ($view as $row) {
                DB::table('announcement_viewed')
                    ->insert([
                        'av_ann_id' => $row,
                        'av_user_id' => user('user_id'),
                        'av_time' => time()
                    ]);
            }
            return json_encode(['status' => 100, 'msg' => 'اطلاعات با موفقیت ثبت شد']);
        }

        return showData(view('collection.notifications_form_list', compact('result')));
    }

    public
    function shopkeeper_notification_form($id)
    {
        $user_id=user('user_id');
        $count = DB::table('announcements')
            ->select([
                "count(1)"
            ])
            ->where('ann_type', 0)
            ->leftJoin('announcement_receivers', 'anr_announcement_id', 'ann_id')
            ->where('anr_type_id', user('panel_type'))
            ->where('anr_city_id', user()->person->prsn_city_id)
            ->whereRaw("(select count(1) from announcement_viewed where av_ann_id = ann_id and av_user_id=$user_id) = 0")
            ->count();

        if ($count > 0) { // baraye sabt view
            $row = DB::table('announcements')
                ->where('ann_id', $id)
                ->select([
                    'ann_view_count'
                ])->first();

            DB::table('announcements')
                ->where('ann_id', $id)
                ->update([
                    'ann_view_count' => $row->ann_view_count + 1
                ]);
            DB::table('announcement_viewed')
                ->insert([
                    'av_ann_id' => $id,
                    'av_user_id' => user('user_id'),
                    'av_time' => time()
                ]);
        }
        $result = DB::table('announcements')
            ->where('ann_id', $id)
            ->select([
                "coll_name",
                'ann_title',
                'ann_body',
                'ann_id',
                'created_at',
                DB::raw("(select top 1 ur_path from upload_route where ur_table_name = 'collections' and ur_collection_id = coll_id order by ur_id desc) as logo")
            ])
            ->leftJoin('announcement_receivers', 'anr_announcement_id', 'ann_id')
            ->where('anr_type_id', user('panel_type'))
            ->where('anr_city_id', user()->person->prsn_city_id)
            ->leftJoin('collections', 'ann_collection_id', 'coll_id')
            ->first();


        return view('collection.notifications_form', compact('result'));
    }

    public
    function createUser(Request $request)
    {
        $roles = DB::table('roles')->select([
            'rol_id',
            'rol_label'
        ]);
        $roles = $roles->where('rol_creator', user('user_id'))->get()->toArray();
        $roles = json_decode(json_encode($roles), true);
        $creator = findUser(user('user_id'), 'persons');

        if ($request->isMethod('post')) {
            $request->validate([
                'create-user.image' => ['mimes:jpeg,jpg,png,gif', 'max:1024'],
                'create-user.name' => ['bail', 'required', 'max:30'],
                'create-user.last-name' => ['required', 'max:30'],
                'create-user.national-code' => ['required', 'max:10', 'regex:/([0-9]+){10}/', 'unique:persons,prsn_national_code'],
                'create-user.mobile' => ['required', 'unique:persons,prsn_mobile1', 'max:11', 'unique:persons,prsn_mobile2', 'max:11', 'regex:/([0-9]+){11}/'],
                'create-user.username' => ['required', 'unique:users,username', 'max:30', 'regex:/^[A-Za-z][A-Za-z0-9]{4,31}$/', ''],
                'create-user.password' => ['required', 'regex:/^[A-Za-z]|[A-Za-z0-9]|[#?!@$%^&*-].{5}$/'],
                'create-user.role' => ['required', 'exists:roles,rol_id'],
                'selected_cities' => ['required'],
                'category' => function ($attribute, $value, $fail) {
                    if (empty(\request('categories'))) {
                        $fail('هیچ دسته بندی انتخاب نشده است');
                    }
                },
                'selected_type' => function ($attribute, $value, $fail) {
                    if (!request()->has('role_person')) {
                        $fail('هیچ دریافت کننده ای انتخاب نشده است.');
                    }
                }
            ]);
            DB::beginTransaction();
            try {
                //save user
                $data = $request->get('create-user');
                $user = new \App\User();
                $user->username = $data['username'];
                $user->password = bcrypt($data['password']);
                $user->panel_type = 40;
                $user->creator = user('user_id');
                $user->code = generateRandomString(10);
                $user->agent_id = getAgent()->ag_id;
                $user->is_active = 1;
                if ($user->save()) {
                    if ($request->has('create-user.image')) {
                        \Illuminate\Support\Facades\DB::table('upload_route')->insert([
                            [
                                'ur_collection_id' => collection_id(),
                                'ur_fk_id' => $user->user_id,
                                'ur_table_name' => "users",
                                'ur_path' => uploadFile($request->file('create-user.image'), 'logo', $user->user_id)
                            ],
                        ]);
                    }
                }

                //save person
                $person = new \App\Person;
                $person->prsn_user_id = $user->user_id;
                $person->prsn_name = $data['name'];
                $person->prsn_family = $data['last-name'];
                $person->prsn_national_code = $data['national-code'];
                $person->prsn_mobile1 = $data['mobile'];
                $person->prsn_state_id = $creator->prsn_state_id;
                $person->prsn_city_id = $creator->prsn_city_id;
                $person->save();

                //save role
                \Illuminate\Support\Facades\DB::table('user_roles')->insert([
                    [
                        'ur_user_id' => $user->user_id,
                        'ur_rol_id' => $data['role']
                    ],
                ]);

                //save collection_person
                \Illuminate\Support\Facades\DB::table('collection_persons')->insert([
                    [
                        'cp_prsn_id' => $person->prsn_id,
                        'cp_coll_id' => collection_id()
                    ],
                ]);

                //save area_support
                $array = [];
                $cities = explode(',', $request->selected_cities);
                foreach ($cities as $city) {
                    array_push($array, [
                        'as_collection_id' => collection_id(),
                        'as_user_id' => $user->user_id,
                        'as_city_id' => $city
                    ]);
                }
                \App\AreaSupport::insert($array);

                //save categories
                $array = [];
                $categories = array_keys($request->categories);
                if (($key = array_search(0, $categories)) !== false) {//delete onsore 0
                    unset($categories[$key]);
                }
                foreach ($categories as $category) {
                    array_push($array, [
                        'cc_collection_id' => collection_id(),
                        'cc_user_id' => $user->user_id,
                        'cc_category_id' => $request->categories[$category]
                    ]);
                }
                \App\CollectionCategories::insert($array);
                DB::commit();
                return json_encode(['status' => 100, 'msg' => 'اطلاعات با موفقیت ثبت شد.']);
            } catch (\Exception $e) {
                echo $e->getMessage();
                exit();
                DB::rollback();
                return json_encode(['status' => 500, 'msg' => 'خطا در ثبت اطلاعات، مجددا تلاش فرمائید.']);
            }

        }
        $temp = '{}';
        $selected = null;
        $path = "/company/save";
        return view('collection.create-user', compact("temp", "selected", 'path', 'roles'));

    }

    public
    function updateUser(Request $request, $id)
    {
        $old_user = \App\User::where('user_id', $id)->first();
        if (!$old_user)
            abort(404);
        $old_person = \App\Person::where('prsn_user_id', $old_user->user_id)->first();
        $creator = findUser(user('user_id'), 'persons');
        if ($request->isMethod('patch')) {
            $data = $request->get('create-user');
            $request->validate([
                'create-user.image' => ['mimes:jpeg,jpg,png,gif', 'max:1024'],
                'create-user.name' => ['bail', 'required', 'max:50'],
                'create-user.last-name' => ['required', 'max:30'],
                'create-user.national-code' => ['required', 'max:10', 'regex:/([0-9]+){10}/', 'unique:persons,prsn_national_code,' . $old_person->prsn_id . ',prsn_id'],
                'create-user.mobile' => ['required', 'unique:persons,prsn_mobile1,' . $old_person->prsn_id . ',prsn_id', 'max:11', 'unique:persons,prsn_mobile2,' . $old_person->prsn_id . ',prsn_id', 'max:11', 'regex:/([0-9]+){11}/'],
                'create-user.role' => ['required', 'exists:roles,rol_id'],
                'selected_cities' => ['required'],
                'category' => function ($attribute, $value, $fail) {
                    if (empty(\request('categories'))) {
                        $fail('هیچ دسته بندی انتخاب نشده است');
                    }
                },
                'selected_type' => function ($attribute, $value, $fail) {
                    if (!request()->has('role_person')) {
                        $fail('هیچ دریافت کننده ای انتخاب نشده است.');
                    }
                }
            ]);
            DB::beginTransaction();
            try {
                //save user
                if ($data['password'] != "")
                    \App\User::where('user_id', $id)->update([
                        'password' => bcrypt($data['password'])
                    ]);

                if ($request->has('create-user.image')) {
                    \Illuminate\Support\Facades\DB::table('upload_route')->insert([
                        [
                            'ur_collection_id' => collection_id(),
                            'ur_fk_id' => $old_user->user_id,
                            'ur_table_name' => "users",
                            'ur_path' => uploadFile($request->file('create-user.image'), 'logo', $old_user->user_id)
                        ],
                    ]);
                }

                //save person
                \App\Person::where('prsn_user_id', $old_user->user_id)->update([
                    'prsn_name' => $data['name'],
                    'prsn_family' => $data['last-name'],
                    'prsn_national_code' => $data['national-code'],
                    'prsn_mobile1' => $data['mobile'],
                    'prsn_state_id' => $creator->prsn_state_id,
                    'prsn_city_id' => $creator->prsn_city_id,
                ]);

                //save role
                \Illuminate\Support\Facades\DB::table('user_roles')->where('ur_user_id', $old_user->user_id)->delete();
                \Illuminate\Support\Facades\DB::table('user_roles')->insert([
                    [
                        'ur_user_id' => $old_user->user_id,
                        'ur_rol_id' => $data['role']
                    ],
                ]);

                //save collection_person
                \Illuminate\Support\Facades\DB::table('collection_persons')->where('cp_prsn_id', $old_person->prsn_id)->delete();
                \Illuminate\Support\Facades\DB::table('collection_persons')->insert([
                    [
                        'cp_prsn_id' => $old_person->prsn_id,
                        'cp_coll_id' => collection_id()
                    ],
                ]);

                //save area_support
                $array = [];
                $cities = explode(',', $request->selected_cities);
                foreach ($cities as $city) {
                    array_push($array, [
                        'as_collection_id' => collection_id(),
                        'as_user_id' => $old_user->user_id,
                        'as_city_id' => $city
                    ]);
                }
                \Illuminate\Support\Facades\DB::table('area_support')->where('as_user_id', $old_user->user_id)->delete();
                \App\AreaSupport::insert($array);

                //save categories
                $array = [];
                $categories = array_keys($request->categories);
                foreach ($categories as $category) {
                    array_push($array, [
                        'cc_collection_id' => collection_id(),
                        'cc_user_id' => $old_user->user_id,
                        'cc_category_id' => $request->categories[$category]
                    ]);
                }
                \Illuminate\Support\Facades\DB::table('collection_categories')->where('cc_user_id', $old_user->user_id)->delete();
                \App\CollectionCategories::insert($array);
                DB::commit();
                return json_encode(['status' => 100, 'msg' => 'اطلاعات با موفقیت ثبت شد.']);
            } catch (\Exception $e) {
                DB::rollback();
                return json_encode(['status' => 500, 'msg' => $e->getMessage()]);
            }
        }
        $old_role = DB::table('user_roles')->where('ur_user_id', $old_user->user_id)->first();
        $old_categories = \App\CollectionCategories::where('cc_user_id', $old_user->user_id)->get()->toArray();
        $selected = array_column(json_decode(json_encode($old_categories), true), 'cc_category_id');
        $selected_cities = DB::table('area_support')
            ->select([
                "cities.c_id",
                "cities.c_name",
                "parent.c_name as parent"
            ])
            ->join("cities", "c_id", "=", "as_city_id")
            ->join("cities as parent", "parent.c_id", "=", "cities.c_parent_id")
            ->where("as_user_id", $old_user->user_id)
            ->groupBy('cities.c_id', 'cities.c_name', 'parent.c_name')
            ->get()
            ->toArray();
        $selected_cities = json_decode(json_encode($selected_cities), true);

        $temp = [];
        foreach ($selected_cities as $row) {
            $temp[$row['parent']][] = [
                "id" => $row['c_id'],
                "name" => $row['c_name']
            ];
        }
        foreach ($temp as $key => $value) {
            $temp[$key] = [
                'text' => array_column($value, 'name'),
                'ids' => array_column($value, 'id'),
            ];
        }
        $temp = empty($temp) ? "{}" : json_encode($temp, JSON_UNESCAPED_UNICODE);

        $roles = DB::table('roles')->select([
            'rol_id',
            'rol_label'
        ]);

        $roles = $roles->where('rol_creator', user('user_id'))->get()->toArray();
        $roles = json_decode(json_encode($roles), true);
        $path = "/company/save";
        return view('collection.create-user', compact("temp", "selected", 'path', 'roles', 'old_user', 'selected', 'old_person', 'old_role', 'temp'));
    }

    public
    function usersList(Request $request)
    {
        $users = DB::table('users')
            ->select([
                'prsn_name',
                'prsn_family',
                'prsn_mobile1',
                'username',
                'user_id',
                'amount',
                'rol_label',
                DB::raw("(select top 1 ur_path from upload_route where ur_table_name = 'users' and ur_fk_id = prsn_user_id order by ur_id desc) as avatar")
            ]);

        if ($request->has('username') && $request->get('username') != '')
            $users->where('username', 'like', '%' . $request->get('username') . '%');
        if ($request->has('name') && $request->get('name') != '')
            $users->where('prsn_name', 'like', '%' . $request->get('name') . '%');
        if ($request->has('family') && $request->get('family') != '')
            $users->where('prsn_family', 'like', '%' . $request->get('family') . '%');
        if ($request->has('mobile') && $request->get('mobile') != '') {
            $users->where('prsn_mobile1', 'like', '%' . $request->get('mobile') . '%');
            $users->orWhere('prsn_mobile2', 'like', '%' . $request->get('mobile') . '%');
        }
        if ($request->has('rol') && $request->get('rol') != '')
            $users->where('rol_label', 'like', '%' . $request->get('rol') . '%');

        $users = $users->leftJoin('persons', 'prsn_user_id', '=', 'user_id')
            ->where('user_id', '!=', user('user_id'))
            ->leftJoin('user_roles', 'ur_user_id', '=', 'user_id')
            ->leftJoin('roles', 'rol_id', '=', 'ur_rol_id')
            ->leftJoin('collection_persons', 'cp_prsn_id', '=', 'prsn_id')
            ->where('cp_coll_id', collection_id())
            ->paginate(30);
        return showData(view('collection.users-list', compact('users')));
    }

    public
    function deleteUser($id)
    {
        $result = checkRelation('users', $id);
        if ($result[0]['ID'] == 0 || empty($result)) {
            DB::beginTransaction();
            try {
                $person = DB::table('persons')->where('prsn_user_id', $id)->first();
                DB::delete('delete from collection_persons where cp_prsn_id = ?', [$person->prsn_id]);
                DB::delete('delete from persons where prsn_user_id = ?', [$id]);
                DB::delete('delete from factor_master where fm_creator_id = ?', [$id]);
                DB::delete('delete from collection_categories where cc_user_id = ?', [$id]);
                DB::delete('delete from area_support where as_user_id = ?', [$id]);
                DB::delete('delete from user_roles where ur_user_id = ?', [$id]);
                DB::delete('delete from bag_transacts where bt_user_id = ?', [$id]);
                DB::delete('delete from users where user_id = ?', [$id]);
                DB::delete('delete from upload_route where ur_fk_id = ? and ur_table_name = ?', [$id, 'users']);
                DB::commit();
                //return json_encode(['status' => 100, 'msg' => 'عملیات حذف با موفقیت انجام شد!']);
                return redirect()->back()->with('success', "عملیات حذف با موفقیت انجام شد!");
            } catch (\Exception $e) {
                DB::rollback();
            }
        }
//        return json_encode(['status' => 101, 'msg' => config('first_config.message.relation_delete')]);
        return redirect()->back()->with('error', config('first_config.message.relation_delete'));
    }

    public
    function return_reasons(Request $request)
    {
        $return_reasons = DB::table('baseinfos')
            ->select([
                "bas_id",
                "bas_value",
//                "crr_status"
            ])
//            ->leftJoin("collection_return_reasons", 'crr_bas_id', 'bas_id')
            ->where('bas_type', 'return_reason')
            ->where('bas_parent_id', '!=', 0)
//            ->where("crr_collection_id", collection_id())
            ->get()
            ->toArray();
        $old_reasons = DB::table('collection_return_reasons')
            ->select([
                "bas_id",
                "bas_value",
                "crr_bas_id",
                "crr_status",
                "crr_return_to_inventory",
            ])
            ->where("crr_collection_id", collection_id())
            ->leftJoin("baseinfos", 'bas_id','crr_bas_id')
            ->get();
        $old_reasons = json_decode(json_encode($old_reasons), true);
        $return_reasons = json_decode(json_encode($return_reasons), true);

        if ($request->isMethod('post')) {
            if ($request->has('active') || $request->has('is_return')) {
                $selected_methods = array_values($request->active);

                foreach ($selected_methods as $value) {
                    if (DB::table('collection_return_reasons')
                        ->where('crr_collection_id', collection_id())
                        ->where('crr_bas_id', $value)->update([
                            'crr_bas_id' => $value,
                            'crr_return_to_inventory' => isset($request->is_return[$value]) ? 1 : 0,
                            'crr_status' => isset($value) ? 1 : 0,
                        ])
                    )
                        continue;
                    else
                        DB::table('collection_return_reasons')
                            ->insert([
                                'crr_collection_id' => collection_id(),
                                'crr_bas_id' => $value,
                                'crr_return_to_inventory' => isset($request->is_return[$value]) ? 1 : 0,
                                'crr_status' => isset($value) ? 1 : 0,
                            ]);
                }
                DB::table('collection_return_reasons')
                    ->where('crr_collection_id', collection_id())
                    ->whereNotIn('crr_bas_id', $selected_methods)
                    ->update([
                        'crr_return_to_inventory' => 0,
                        'crr_status' => 0
                    ]);
            } else {
                DB::table('collection_return_reasons')
                    ->where('crr_collection_id', collection_id())
                    ->update([
                        'crr_return_to_inventory' => 0,
                        'crr_status' => 0
                    ]);
            }
            return json_encode(['status' => 100, 'msg' => 'اطلاعات با موفقیت ثبت شد']);

        }
        return view('collection.return_reasons', compact('return_reasons', 'old_reasons'));
    }

    public
    function area_support(Request $request, $id)
    {
        $collection = Collection::findOrFail($id);

        $this->validate(request(), [
            'selected_cities' => function ($attribute, $value, $fail) {
                if (!request()->has('selected_cities')) {
                    $fail('شهرهای انتخاب شده الزامی است');
                }
            }
        ]);

        $first_user_id_in_collection = DB::table('users')
            ->join('persons', 'prsn_user_id', '=', 'user_id')
            ->join('collection_persons', 'prsn_id', '=', 'cp_prsn_id')
            ->join('collections', 'cp_coll_id', '=', 'coll_id')
            ->where('coll_id', $id)
            ->first()
            ->user_id;
        if (request()->method() == "POST") {
            $request->validate([
                'selected_cities' => function ($attribute, $value, $fail) {
                    if (!request()->has('selected_cities') || \request('selected_cities') == '') {
                        $fail('هیچ شهری انتخاب نشده است.');
                    }
                }
            ]);
            $ids = explode(',', request()->get('selected_cities'));
            AreaSupport::where("as_collection_id", $id)->where("as_user_id", $first_user_id_in_collection)->delete();
            foreach ($ids as $city_id) {
                $model = new AreaSupport();
                $model->as_collection_id = $id;
                $model->as_city_id = $city_id;
                $model->as_user_id = $first_user_id_in_collection;
                $model->save();
            };
            return json_encode(['status' => 100, 'msg' => 'نقاط تحت پوشش با موفقیت ذخیره شد!', 'url' => url("/company/manage-categories") . "/" . $id]);
        }
        $areas = DB::table('area_support')
            ->select([
                "cities.c_id",
                "cities.c_name",
                "parent.c_name as parent"
            ])
            ->join("cities", "c_id", "=", "as_city_id")
            ->join("cities as parent", "parent.c_id", "=", "cities.c_parent_id")
            ->where("as_collection_id", $id)
            ->where("as_user_id", $first_user_id_in_collection)
            ->get()
            ->toArray();
        $arr = json_decode(json_encode($areas), true);
        $temp = [];
        foreach ($arr as $row) {
            $temp[$row['parent']][] = [
                "id" => $row['c_id'],
                "name" => $row['c_name']
            ];
        }
        foreach ($temp as $key => $value) {
            $temp[$key] = [
                'text' => array_column($value, 'name'),
                'ids' => array_column($value, 'id'),
            ];
        }
        $temp = empty($temp) ? "{}" : json_encode($temp, JSON_UNESCAPED_UNICODE);
        $url = "/user/" . $collection->persons()->first()->prsn_user_id . "/edit";
        return view('collection.area_support', compact('temp', 'url'));
    }

    public
    function register_authentication(Request $request)
    {
        $oldForm = null;
        $result = DB::table('panel_wizard_completed')->select([
            'pwc_accepted',
            'pwc_form_id',
            'pwc_reason'
        ])
            ->join('panel_wizards', 'pw_id', '=', 'pwc_form_id')
            ->where('pwc_user_id', user('user_id'))
            ->where('pw_form_address', 'authentication')
            ->first();
        if ($result) {
            //form ghablan sabt shode
            if ($result->pwc_accepted == 1 || $result->pwc_accepted == 0) {
                //taeed shode boro be address badi
                $next_form = DB::table('panel_wizards')->select([
                    'pw_form_address'
                ])
                    ->where('pw_id', '>', $result->pwc_form_id)->first();

                if ($next_form) {
                    //check panel_type visitor and shopkeeper
                    if (user('panel_type') == 44) {
                        $url = '/shopkeeper/';
                        //visitor
                    } elseif (user('panel_type') == 45) {
                        $url = '/visitor/';
                    }
                    //go to next form
                    return redirect(asset($url . $next_form->pw_form_address));
                } else {
                    //go to dashboard
                    return redirect(asset('dashboard'));
                }

            } else if ($result->pwc_accepted == 2) {
                //mode update
                session()->flash('error', config('first_config.message.user-waiting-error') . $result->pwc_reason);
                $oldForm = DB::table('users')
                    ->select([
                        'reagent_id',
                        'persons.*'
                    ])
                    ->where('user_id', user('user_id'))
                    ->leftJoin('persons','prsn_user_id','user_id')->first();

            }
        }
        if ($request->isMethod('post')) {
            $request->validate([

                'shop_keeper.name' => ['required', 'max:50'],
                'shop_keeper.last_name' => ['required', 'max:50'],
                'shop_keeper.name_father' => ['required', 'max:50'],
                'shop_keeper.date_of_birth' => ['required', 'regex:/^[1-9][0-9]{3}\/(0[1-9]|1[0-2])\/(0[1-9]|[1-2][0-9]|3[0-1])$/', function ($attr, $value, $fail) {
                    if ($value > jdate_from_gregorian(time(), 'Y/m/d')) {
                        return $fail('تاریخ معتبر نیست');
                    }
                }
                ],
                'shop_keeper.national_code' => ['required', 'max:10', 'regex:/([0-9]+){10}/'],
                'shop_keeper.postal_code' => ['required', 'max:10', 'regex:/([0-9]+){10}/'],
                'shop_keeper.state' => ['required'],
                'shop_keeper.city' => ['required'],
                'shop_keeper.address' => ['required', 'max:255'],
            ]);
            DB::beginTransaction();
            try {
                //update person
                $user_id = user("user_id");
                if ($request->id) {//agar az panel admin dare edit mishe
                    $form = DB::table('panel_wizard_completed')->select([
                        'pwc_user_id'
                    ])
                        ->where('pwc_id', $request->id)
                        ->first();
                    if (!$form)
                        abort(404);
                    $user_id = $form->pwc_user_id;
                }

                DB::table('users')
                    ->where('user_id', $user_id)
                    ->update([
                        'reagent_id' => ($request->shop_keeper['reagent_id']) ? $request->shop_keeper['reagent_id'] : 0,
                    ]);

                DB::table('persons')
                    ->where('prsn_user_id', $user_id)
                    ->update([
                        'prsn_name' => $request->shop_keeper['name'],
                        'prsn_family' => $request->shop_keeper['last_name'],
                        'prsn_father_name' => $request->shop_keeper['name_father'],
                        'prsn_birthday' => $request->shop_keeper['date_of_birth'],
                        'prsn_national_code' => $request->shop_keeper['national_code'],
                        'prsn_post_code' => $request->shop_keeper['postal_code'],
                        'prsn_state_id' => $request->shop_keeper['state'],
                        'prsn_city_id' => $request->shop_keeper['city'],
                        'prsn_address' => $request->shop_keeper['address'],]);
                if ($request->id) {
                    DB::commit();
                    return json_encode(['status' => 100, 'msg' => 'با موفقیت آپدیت شد']);
                }
                if ($result) {
                    $fin_id = DB::table('panel_wizards as b')
                        ->select([
                            'p.pwc_id',
                            'b.pw_id',
                        ])
                        ->where('pw_panel_type_id', user('panel_type'))
                        ->leftJoin('panel_wizard_completed as p', 'b.pw_id', '=', 'p.pwc_form_id')
                        ->where('pwc_user_id', $user_id)
                        ->where('pw_form_address', 'authentication')
                        ->first();
                    DB::table('panel_wizard_completed')
                        ->where('pwc_id', '=', $fin_id->pwc_id)
                        ->update([
                            'pwc_form_id' => $fin_id->pw_id,
                            'pwc_accepted' => 0,
                            'updated_at' => date("Y/m/d H:i:s"),
                            'updatetor_user_id' => user('user_id')
                        ]);

                } else if ($result == null) {
                    //insert panel_wizard_completed
                    $fin_type = DB::table('panel_wizards as b')
                        ->select([
                            'b.pw_panel_type_id'
                        ])
                        ->where('b.pw_panel_type_id', '=', user('panel_type'))
                        ->where('pw_form_address', 'authentication')
                        ->select([
                            'b.pw_id'
                        ])->first();
                    DB::table('panel_wizard_completed')
                        ->insert([
                            'pwc_form_id' => $fin_type->pw_id,
                            'pwc_user_id' => user('user_id'),
                            'pwc_accepted' => '0',
                            'created_at' => date("Y/m/d H:i:s"),
                        ]);
                }
                DB::commit();
                return json_encode(['status' => 100, 'msg' => 'اطلاعات با موفقیت ثبت شد']);
            } catch (\Expecting $e) {
                DB::rollBack();
                return json_encode(['status' => 500, 'msg' => 'متسفانه عملیات با خطا مواجه شد!']);
            }
        }
        return view('collection.authentication-form', compact('oldForm'));
    }

    public
    function register_businessForm_shopkeeper(Request $request)
    {
        $old_senfs = [];
        $shopKepper = null;
        $typeCollections = \App\Baseinfo::whereBasType('type_class')->where('bas_parent_id', '<>', '0')->get();
        $oldForm = null;
        $result = DB::table('panel_wizard_completed')->select([
            'pwc_accepted',
            'pwc_form_id',
            'pwc_reason'
        ])
            ->join('panel_wizards', 'pw_id', '=', 'pwc_form_id')
            ->where('pwc_user_id', user('user_id'))
            ->where('pw_form_address', 'business-form')
            ->first();
        if ($result) {
            //form ghablan sabt shode
            if ($result->pwc_accepted == 1 || $result->pwc_accepted == 0) {
                //taeed shode boro be address badi
                $next_form = DB::table('panel_wizards')->select([
                    'pw_form_address'
                ])
                    ->where('pw_id', '>', $result->pwc_form_id)->first();

                if ($next_form) {
                    //find prrsn_id
                    $fin_prsn_id = Person::where('prsn_user_id', user('user_id'))->first();
                    //go to next form
                    return redirect(asset($next_form->pw_form_address . '/create?prsn_id=' . $fin_prsn_id->prsn_id));
                } else {
                    //go to dashboard
                    return redirect(asset('dashboard'));
                }

            } else if ($result->pwc_accepted == 2) {
                //mode update
                session()->flash('error', config('first_config.message.user-waiting-error') . $result->pwc_reason);
                $old_senfs = array_column(json_decode(json_encode(DB::table('collection_classes')->select([
                    'coc_typeClass_id'
                ])
                    ->where('coc_coll_id', collection_id())
                    ->get()), true), 'coc_typeClass_id');

                $shopKepper = DB::table('persons')
                    ->select([
                        'prsn_id',
                        'coll_name',
                        'coll_address',
                        'prsn_job',
                        'coll_state_id',
                        'coll_city_id'
                    ])
                    ->join('collection_persons', 'cp_prsn_id', '=', 'prsn_id')
                    ->join('collections', 'coll_id', '=', 'cp_coll_id')
                    ->where('prsn_user_id', user('user_id'))
                    ->first();
            }
        }
        if ($request->isMethod('post')) {
            $request->validate([
                'typeClass' => function ($attribute, $value, $fail) {
                    if (empty(\request('shop_keeper_class'))) {
                        $fail('هیچ صنفی انتخاب نشده است');
                    }
                },
                'shop_keeper.job' => ['required'],
                'shop_keeper.store' => ['required'],
                'shop_keeper.state' => ['required'],
                'shop_keeper.city' => ['required'],
                'shop_keeper.address' => ['required'],
            ]);
            DB::beginTransaction();
            try {
                $user_id = user("user_id");
                $collection_id = collection_id();
                if ($request->id) {//agar az panel admin dare edit mishe
                    $form = DB::table('panel_wizard_completed')->select([
                        'pwc_user_id'
                    ])
                        ->where('pwc_id', $request->id)
                        ->first();
                    if (!$form)
                        abort(404);
                    $user_id = $form->pwc_user_id;
                    $collection_id = DB::table("persons")->select("cp_coll_id")
                        ->join("collection_persons", 'cp_prsn_id', '=', 'prsn_id')
                        ->where("prsn_user_id", $user_id)
                        ->first()->cp_coll_id;
                }
                DB::table('persons')
                    ->where('prsn_user_id', $user_id)
                    ->update([
                        'prsn_job' => $request->shop_keeper['job']
                    ]);

                //set roles
                $roles = DB::table('class_roles')
                    ->select([
                        'clr_role_id'
                    ])
                    ->whereIn('clr_class_id', $request->shop_keeper_class)
                    ->where('clr_type_id', findUser($user_id, "panel_type", "id"))
                    ->get();
                //save roles
                $array = [];
                foreach ($roles as $role) {
                    array_push($array, [
                        'ur_rol_id' => $role->clr_role_id,
                        'ur_user_id' => $user_id
                    ]);
                }
                DB::table('user_roles')->where("ur_user_id", $user_id)->delete();
                DB::table('user_roles')->insert($array);
                //save classes
                $array = [];
                foreach ($request->shop_keeper_class as $class) {
                    array_push($array, [
                        'coc_coll_id' => $collection_id,
                        'coc_typeClass_id' => $class
                    ]);
                }

                DB::table('collection_classes')->where('coc_coll_id', $collection_id)->delete();
                DB::table('collection_classes')->insert($array);

                //end set roles

                DB::table('collections')
                    ->where('coll_id', $collection_id)
                    ->update([
                        'coll_name' => $request->shop_keeper['store'],
                        'coll_state_id' => $request->shop_keeper['state'],
                        'coll_city_id' => $request->shop_keeper['city'],
                        'coll_address' => $request->shop_keeper['address'],
                    ]);
                DB::commit();
                if ($request->id) {
                    return json_encode(['status' => 100, 'msg' => 'با موفقیت آپدیت شد']);
                }
                //update panel_wizard_completed
                if ($result) {
                    $fin_id = DB::table('panel_wizards as b')
                        ->select([
                            'p.pwc_id',
                            'b.pw_id',
                        ])
                        ->where('pw_panel_type_id', user('panel_type'))
                        ->leftJoin('panel_wizard_completed as p', 'b.pw_id', '=', 'p.pwc_form_id')
                        ->where('pwc_user_id', user('user_id'))
                        ->where('pw_form_address', 'business-form')
                        ->first();
                    DB::table('panel_wizard_completed')
                        ->where('pwc_id', '=', $fin_id->pwc_id)
                        ->update([
                            'pwc_form_id' => $fin_id->pw_id,
                            'pwc_user_id' => user('user_id'),
                            'pwc_accepted' => 0,
                            'updated_at' => date("Y/m/d H:i:s"),
                            'updatetor_user_id' => user('user_id'),
                        ]);

                } else if ($result == null) {
                    //insert panel_wizard_completed
                    $fin_type = DB::table('panel_wizards as b')
                        ->select([
                            'b.pw_panel_type_id'
                        ])
                        ->where('b.pw_panel_type_id', '=', user('panel_type'))
                        ->where('pw_form_address', 'business-form')
                        ->select([
                            'b.pw_id'
                        ])->first();
                    DB::table('panel_wizard_completed')
                        ->insert([
                            'pwc_form_id' => $fin_type->pw_id,
                            'pwc_user_id' => user('user_id'),
                            'pwc_accepted' => '0',
                            'created_at' => date("Y/m/d H:i:s"),
                        ]);
                }
                DB::commit();
                return json_encode(['status' => 100, 'msg' => 'اطلاعات با موفقیت ثبت شد']);
            } catch (\Expecting $e) {
                DB::rollBack();
                return json_encode(['status' => 500, 'msg' => 'متسفانه عملیات با خطا مواجه شد!']);
            }
        }


        $types = json_decode(json_encode(DB::table('baseinfos')
            ->select([
                'bas_id',
                'bas_value'
            ])
            ->where('bas_parent_id', 19)
            ->where('bas_id', '!=', 1)
            ->get()->toArray()), true);
        return view('collection.business_shopkeeper_form', compact('types', 'typeCollections', 'old_senfs', 'shopKepper'));
    }

    public
    function register_businessForm_visitor(Request $request)
    {
        $old_senfs = [];
        $visitor = null;
        $family = DB::table('persons')
            ->select(['prsn_family'])
            ->where('prsn_user_id', user('user_id'))
            ->first();
        $typeCollections = \App\Baseinfo::whereBasType('type_class')->where('bas_parent_id', '<>', '0')->get();
        $oldForm = null;
        $result = DB::table('panel_wizard_completed')->select([
            'pwc_accepted',
            'pwc_form_id',
            'pwc_reason'
        ])
            ->join('panel_wizards', 'pw_id', '=', 'pwc_form_id')
            ->where('pwc_user_id', user('user_id'))
            ->where('pw_form_address', 'business-form')
            ->first();
        if ($result) {
            //form ghablan sabt shode
            if ($result->pwc_accepted == 1 || $result->pwc_accepted == 0) {
                //taeed shode boro be address badi
                $next_form = DB::table('panel_wizards')->select([
                    'pw_form_address'
                ])
                    ->where('pw_id', '>', $result->pwc_form_id)->first();

                if ($next_form) {
                    //find prrsn_id
                    $fin_prsn_id = Person::where('prsn_user_id', user('user_id'))->first();
                    //go to next form
                    return redirect(asset($next_form->pw_form_address . '/create?prsn_id=' . $fin_prsn_id->prsn_id));
                } else {
                    //go to dashboard
                    return redirect(asset('dashboard'));
                }

            } else if ($result->pwc_accepted == 2) {
                //mode update
                session()->flash('error', config('first_config.message.user-waiting-error') . $result->pwc_reason);

                $old_senfs = array_column(json_decode(json_encode(DB::table('collection_classes')->select([
                    'coc_typeClass_id'
                ])
                    ->where('coc_coll_id', collection_id())
                    ->get()), true), 'coc_typeClass_id');

                $visitor = DB::table('persons')
                    ->select([
                        'prsn_id',
                        'prsn_family',
                        'coll_name',
                        'coll_address',
                        'prsn_job',
                        'coll_state_id',
                        'coll_city_id'
                    ])
                    ->join('collection_persons', 'cp_prsn_id', '=', 'prsn_id')
                    ->join('collections', 'coll_id', '=', 'cp_coll_id')
                    ->where('prsn_user_id', user('user_id'))
                    ->first();
            }
        }
        if ($request->isMethod('post')) {
            $request->validate([

                'typeClass' => function ($attribute, $value, $fail) {
                    if (empty(\request('visitor_class'))) {
                        $fail('هیچ صنفی انتخاب نشده است');
                    }
                },
                'visitor.job' => ['required'],
                'visitor.state' => ['required'],
                'visitor.city' => ['required'],
                'visitor.address' => ['required'],
            ]);
            DB::beginTransaction();
            try {
                $user_id = user("user_id");
                $collection_id = collection_id();
                if ($request->id) {//agar az panel admin dare edit mishe
                    $form = DB::table('panel_wizard_completed')->select([
                        'pwc_user_id'
                    ])
                        ->where('pwc_id', $request->id)
                        ->first();
                    if (!$form)
                        abort(404);
                    $user_id = $form->pwc_user_id;
                    $collection_id = DB::table("persons")->select("cp_coll_id")
                        ->join("collection_persons", 'cp_prsn_id', '=', 'prsn_id')
                        ->where("prsn_user_id", $user_id)
                        ->first()->cp_coll_id;
                }

                //set roles
                $roles = DB::table('class_roles')
                    ->select([
                        'clr_role_id'
                    ])
                    ->whereIn('clr_class_id', $request->visitor_class)
                    ->where('clr_type_id', findUser($user_id, "panel_type", "id"))
                    ->get();
                //save roles
                $array = [];
                foreach ($roles as $role) {
                    array_push($array, [
                        'ur_rol_id' => $role->clr_role_id,
                        'ur_user_id' => $user_id
                    ]);
                }
                DB::table('user_roles')->where("ur_user_id", $user_id)->delete();
                DB::table('user_roles')->insert($array);
                //save classes
                $array = [];
                foreach ($request->visitor_class as $class) {
                    array_push($array, [
                        'coc_coll_id' => $collection_id,
                        'coc_typeClass_id' => $class
                    ]);
                }
                DB::table('collection_classes')->where('coc_coll_id', $collection_id)->delete();
                DB::table('collection_classes')->insert($array);
                //end set roles

                DB::table('collections')
                    ->where('coll_id', $collection_id)
                    ->update([
                        'coll_state_id' => $request->visitor['state'],
                        'coll_city_id' => $request->visitor['city'],
                        'coll_address' => $request->visitor['address'],
                    ]);
                DB::table('persons')
                    ->where('prsn_user_id', $user_id)
                    ->update([
                        'prsn_job' => $family->prsn_family
                    ]);
                if ($request->id) {
                    DB::commit();
                    return json_encode(['status' => 100, 'msg' => 'با موفقیت آپدیت شد']);
                }
                //update panel_wizard_completed
                if ($result) {
                    $fin_id = DB::table('panel_wizards as b')
                        ->select([
                            'p.pwc_id',
                            'b.pw_id',
                        ])
                        ->where('pw_panel_type_id', user('panel_type'))
                        ->leftJoin('panel_wizard_completed as p', 'b.pw_id', '=', 'p.pwc_form_id')
                        ->where('pwc_user_id', user('user_id'))
                        ->where('pw_form_address', 'business-form')
                        ->first();
                    DB::table('panel_wizard_completed')
                        ->where('pwc_id', '=', $fin_id->pwc_id)
                        ->update([
                            'pwc_form_id' => $fin_id->pw_id,
                            'pwc_user_id' => user('user_id'),
                            'pwc_accepted' => 0,
                            'updated_at' => date("Y/m/d H:i:s"),
                            'updatetor_user_id' => user('user_id'),
                        ]);

                } else if ($result == null) {
                    //insert panel_wizard_completed
                    $fin_type = DB::table('panel_wizards as b')
                        ->select([
                            'b.pw_panel_type_id'
                        ])
                        ->where('b.pw_panel_type_id', '=', user('panel_type'))
                        ->where('pw_form_address', 'business-form')
                        ->select([
                            'b.pw_id'
                        ])->first();
                    DB::table('panel_wizard_completed')
                        ->insert([
                            'pwc_form_id' => $fin_type->pw_id,
                            'pwc_user_id' => user('user_id'),
                            'pwc_accepted' => '0',
                            'created_at' => date("Y/m/d H:i:s"),
                        ]);
                }
                DB::commit();
                return json_encode(['status' => 100, 'msg' => 'اطلاعات با موفقیت ثبت شد']);
            } catch (\Expecting $e) {
                DB::rollBack();
                return json_encode(['status' => 500, 'msg' => 'متسفانه عملیات با خطا مواجه شد!']);
            }
        }

        $types = json_decode(json_encode(DB::table('baseinfos')
            ->select([
                'bas_id',
                'bas_value'
            ])
            ->where('bas_parent_id', 19)
            ->where('bas_id', '!=', 1)
            ->get()->toArray()), true);

        return view('collection.business_visitor_form', compact('types', 'visitor', 'old_senfs', 'typeCollections'));
    }

    public
    function register_uploadDocument(Request $request)
    {
        if ($request->isMethod('post')) {
            $request->validate([
                'prsn_id' => ['required'],
                'ur_type_id.*' => ['required'],
            ]);
            DB::beginTransaction();
            try {
                //update panel_wizard_completed
                if (DB::table('Panel_wizard_completed')->select([
                    'pwc_user_id'
                ])->where('pwc_user_id', user('user_id'))
                    ->where('pwc_accepted', '!=', 0)
                    ->first()
                ) {
                    $fin_id = DB::table('panel_wizards as b')
                        ->select([
                            'b.pw_id',
                        ])
                        ->leftJoin('panel_wizard_completed as p', 'b.pw_id', '=', 'p.pwc_form_id')
                        ->where('p.pwc_accepted', '!=', 0)
                        ->first();
//                    dd($fin_id);
                    DB::table('panel_wizard_completed')
                        ->update([
                            'pwc_form_id' => $fin_id->pw_id,
                            'pwc_user_id' => user('user_id'),
                            'pwc_accepted' => '0',
                        ]);

                } else {
                    //insert panel_wizard_completed
                    $fin_type = DB::table('panel_wizards as b')
                        ->select([
                            'b.pw_panel_type_id'
                        ])
                        ->where('b.pw_panel_type_id', '=', user('panel_type'))
                        ->leftJoin('panel_wizard_completed as p', 'p.pwc_form_id', '<', 'b.pw_id')
                        ->select([
                            'b.pw_id'
                        ])->first();
//                    dd($fin_type);
                    DB::table('panel_wizard_completed')
                        ->insert([
                            'pwc_form_id' => $fin_type->pw_id,
                            'pwc_user_id' => user('user_id'),
                            'pwc_accepted' => '0',
                        ]);
                }
                DB::commit();
                return json_encode(['status' => 100, 'msg' => 'اطلاعات با موفقیت ثبت شد!', 'url' => asset('upload-document') . '?prsn_id=' . Person::where('prsn_user_id', user('user_id'))->first()->prsn_id]);
            } catch (\Expecting $e) {
                DB::rollBack();
                return json_encode(['status' => 500, 'msg' => 'متسفانه عملیات با خطا مواجه شد!']);
            }

        }

        $url = "/shopkeeper/business-form";
        return view('upload_route.form', compact("url"));

    }

    public
    function view_profile($id)
    {
        $result = DB::table('collections')
            ->select([
                'email',
                'panel_type',
                'prsn_address',
                'prsn_user_id',
                'prsn_phone1',
                'coll_name',
                'coll_id',
                DB::raw("(select top 1 ur_path from upload_route where ur_collection_id = coll_id and ur_table_name = 'logo' order by ur_id desc) as logo")
            ])
            ->leftJoin('collection_persons', 'cp_coll_id', 'coll_id')
            ->leftJoin('persons', 'cp_prsn_id', 'prsn_id')
            ->leftJoin('users', 'prsn_user_id', 'user_id')
            ->where('coll_id', $id)
            ->first();

        return view('collection.view_profile', compact('id', 'result'));
    }

    public
    function sendMessage(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            $request->validate([
                'title_sendMessage' => ['required'],
                'text_sendMessage' => ['required'],
            ]);

            $msg_id = DB::table("messages")->insertGetId([
                'msg_title' => $request->get('title_sendMessage'),
                'msg_message' => $request->get('text_sendMessage'),
                'msg_sender_user_id' => user("user_id"),
                'msg_receiver_collection_id' => $id,
                'msg_created_at' => time()
            ]);

            if ($request->file('file_sendMessage'))
                $result = DB::table('upload_route')->insert([
                    'ur_collection_id' => collection_id(),
                    'ur_fk_id' => $msg_id,
                    'ur_table_name' => 'messages',
                    'ur_is_deleted' => 0,
                    'ur_status' => 0,
                    'ur_path' => uploadFile($request->file('file_sendMessage'), 'message'),
                    'created_at' => date("Y/m/d H:i:s"),
                ]);

            if ($msg_id)
                return json_encode(['status' => 100, 'msg' => 'پیام شما با موفقیت ارسال شد!']);
            return json_encode(['status' => 500, 'msg' => 'خطا در ارسال پیام، مجددا تلاش فرمائید!']);
        }
    }

    public
    function reportMessage(Request $request, $id)
    {
        $collection_id = findUser($id, 'collection', 'id');
        if ($request->isMethod('post')) {
            $request->validate([
                'text_report' => ['required'],
            ]);

            $msg_id = DB::table("messages")->insertGetId([
                'msg_message' => $request->get('text_report'),
                'msg_sender_user_id' => user("user_id"),
                'msg_receiver_collection_id' => $collection_id,
                'msg_type' => 1,
                'msg_created_at' => time()
            ]);

            if ($request->file('file_report'))
                $result = DB::table('upload_route')->insert([
                    'ur_collection_id' => collection_id(),
                    'ur_fk_id' => $msg_id,
                    'ur_table_name' => 'reports',
                    'ur_is_deleted' => 0,
                    'ur_status' => 0,
                    'ur_path' => uploadFile($request->file('file_report'), 'report'),
                    'created_at' => date("Y/m/d H:i:s"),
                ]);
//            DB::table("violation_log")->insertGetId([
//                'vl_user_id' => $user_id,
//                'vl_type' => 0,
//                'vl_time' => time(),
//            ]);

            if ($msg_id)
                return json_encode(['status' => 100, 'msg' => 'پیام شما با موفقیت ارسال شد!']);
            return json_encode(['status' => 500, 'msg' => 'خطا در ارسال پیام، مجددا تلاش فرمائید!']);
        }
    }

    public
    function edit_profile(Request $request)
    {
        $sheba = Person::where('prsn_user_id', \user('user_id'))->first()->prsn_sheba;
        $result = DB::table('users as b')
            ->where('b.user_id', user("user_id"))
            ->leftJoin('persons as p', 'p.prsn_user_id', 'b.user_id')
            ->leftJoin('collection_persons as c', 'c.cp_prsn_id', 'p.prsn_id')
            ->select([
                'b.email',
                'b.password',
                'p.prsn_name',
                'p.prsn_family',
                'p.prsn_mobile1',
                'p.prsn_national_code',
            ])->first();
        $avatar = DB::table('upload_route')
            ->select([
                'ur_path'
            ])
            ->where('ur_collection_id', collection_id())
            ->where('ur_table_name', 'avatar')
            ->where('ur_status', '1')
            ->orderBy('ur_id', 'DESC')
            ->first();


        if ($request->isMethod('post')) {

            if ($request->avatar) {
                $path = uploadFile($request->file('avatar'), 'avatar', user("user_id"));
                if (UploadRoute::create([
                    'ur_path' => $path,
                    'ur_fk_id' => user("user_id"),
                    'ur_collection_id' => collection_id(),
                    'ur_description' => null,
                    'ur_type_id' => 1,
                    'ur_table_name' => 'avatar',
                ])
                )
                    return json_encode(['status' => 100, 'msg' => 'عملیات با موفقیت انجام شد']);
                return json_encode(['status' => 500, 'msg' => 'متسفانه عملیات با خطا مواجه شد!']);
            }
            if ($request->old_password != null || $request->password != null || $request->verifyPassword != null) {
                $request->validate([
                    'old_password' => ['required'],
                    'password' => ['required'],
                    'verifyPassword' => ['required']
                ]);
                if ($request->password != $request->verifyPassword) {
                    return json_encode(['status' => 500, 'msg' => 'رمز عبور ها با هم مطابقت ندارند']);
                }
                if (Hash::check($request->old_password, $result->password)) {
                    if ($request->old_password == $request->password) {
                        return json_encode(['status' => 500, 'msg' => 'رمزعبور جدید شما نمی تواند رمزعبور فعلی باشد']);
                    }
                    DB::table('users')
                        ->where('user_id', user('user_id'))
                        ->update([
                            'password' => Hash::make($request->password)
                        ]);
                } else {
                    return json_encode(['status' => 500, 'msg' => 'رمز عبور فعلی اشتباه است']);
                }
            }
            if ($request->has('sheba')) {
                DB::table('persons')
                    ->where('prsn_user_id', \user('user_id'))
                    ->update([
                        'prsn_sheba' => $request->sheba
                    ]);
            }
            return json_encode(['status' => 100, 'msg' => 'عملیات با موفقیت انجام شد']);
        }
        return view('collection.edit_profile', compact('result', 'avatar', 'sheba'));
    }

    public function editAreaSupportCompany(Request $request)
    {

        $first_user_id_in_collection = DB::table('users')
            ->join('persons', 'prsn_user_id', '=', 'user_id')
            ->join('collection_persons', 'prsn_id', '=', 'cp_prsn_id')
            ->join('collections', 'cp_coll_id', '=', 'coll_id')
            ->where('coll_id', collection_id())
            ->first()
            ->user_id;
        if (request()->method() == "POST") {
            $request->validate([
                'selected_cities' => function ($attribute, $value, $fail) {
                    if (!request()->has('selected_cities') || \request('selected_cities') == '') {
                        $fail('هیچ شهری انتخاب نشده است.');
                    }
                }
            ]);
            $ids = explode(',', request()->get('selected_cities'));
            AreaSupport::where("as_collection_id", collection_id())->where("as_user_id", $first_user_id_in_collection)->delete();
            foreach ($ids as $city_id) {
                $model = new AreaSupport();
                $model->as_collection_id = collection_id();
                $model->as_city_id = $city_id;
                $model->as_user_id = $first_user_id_in_collection;
                $model->save();
            };
            return json_encode(['status' => 100, 'msg' => 'نقاط تحت پوشش با موفقیت ذخیره شد!']);
        }
        $areas = DB::table('area_support')
            ->select([
                "cities.c_id",
                "cities.c_name",
                "parent.c_name as parent"
            ])
            ->join("cities", "c_id", "=", "as_city_id")
            ->join("cities as parent", "parent.c_id", "=", "cities.c_parent_id")
            ->where("as_collection_id", collection_id())
            ->where("as_user_id", $first_user_id_in_collection)
            ->get()
            ->toArray();
        $arr = json_decode(json_encode($areas), true);
        $temp = [];
        foreach ($arr as $row) {
            $temp[$row['parent']][] = [
                "id" => $row['c_id'],
                "name" => $row['c_name']
            ];
        }
        foreach ($temp as $key => $value) {
            $temp[$key] = [
                'text' => array_column($value, 'name'),
                'ids' => array_column($value, 'id'),
            ];
        }
        $temp = empty($temp) ? "{}" : json_encode($temp, JSON_UNESCAPED_UNICODE);
        return view('collection.edit_company_area_support', compact('temp'));
    }

    public
    function invoice_list()
    {
        return view('collection.invoice_list');
    }

    public
    function wizard_setting(Request $request, $id = null)
    {
        $type = null;
        $update_wiz = null;
        $bas_info = json_decode(json_encode(DB::table('baseinfos')
            ->select([
                'bas_id',
                'bas_value',
            ])
            ->where('bas_type', 'panel_type')
            ->get()->toArray()), true);
        $wizards = DB::table('panel_wizards as b')
            ->select([
                'b.pw_panel_type_id',
            ])
            ->leftJoin('baseinfos as p', 'b.pw_panel_type_id', '=', 'p.bas_id')
            ->select([
                'p.bas_value',
                'b.pw_id',
                'b.pw_form_address',
                'b.pw_status',
            ])
            ->paginate(20);
        if ($id == null) {
            if ($request->isMethod('post')) {
                $request->validate([
                    'wizard.type' => ['required'],
                    'wizard.address' => ['required'],
                ]);
                //is create
                $create = new PanelWizard();
                $create->pw_panel_type_id = $request->wizard['type'];
                $create->pw_form_address = $request->wizard['address'];
                $create->pw_status = $request->wizard['status'];
                if ($create->save()) {
                    return json_encode(['status' => 100, 'msg' => 'اطلاعات با موفقیت ثبت شد!']);
                } else {
                    return json_encode(['status' => 500, 'msg' => 'خطا در ثبت اطلاعات!']);
                }
            }
        } else {
            //echo update
            $update_wiz = PanelWizard::where('pw_id', $id)->first();
            $type = json_decode(json_encode(DB::table('baseinfos')
                ->select(
                    'bas_id',
                    'bas_value'

                )
                ->where('bas_id', $update_wiz->pw_panel_type_id)
                ->first()), true);
            if ($request->isMethod('patch')) {
                $request->validate([
                    'wizard.type' => ['required'],
                    'wizard.address' => ['required'],
                ]);
                //is create update
                $create = PanelWizard::find($id);
                $create->pw_panel_type_id = $request->wizard['type'];
                $create->pw_form_address = $request->wizard['address'];
                $create->pw_status = $request->wizard['status'];
                if ($create->save()) {
                    return json_encode(['status' => 100, 'msg' => 'اطلاعات با موفقیت ثبت شد!']);
                } else {
                    return json_encode(['status' => 500, 'msg' => 'خطا در ثبت اطلاعات!']);
                }
            }
            if ($request->isMethod('delete')) {

                $result = checkRelation('panel_wizards', $id);
                if (empty($result) || $result[0]['ID'] == 0) {
                    DB::beginTransaction();
                    try {
                        DB::delete('delete from panel_wizards where pw_id = ?', [$id]);
                        DB::commit();
                        return json_encode(['status' => 100, 'msg' => 'عملیات حذف با موفقیت انجام شد!']);
                    } catch (\Exception $e) {
                        DB::rollback();
                    }
                }
                return json_encode(['status' => 101, 'msg' => config('first_config.message.relation_delete')]);

            }
        }


        return view('collection.wizard_settings', compact('bas_info', 'wizards', 'update_wiz', 'type'));
    }

    public
    function terms(Request $request)
    {
        $check = User::where('user_id', user('user_id'))->first('accept_terms');
        if ($check->accept_terms == 1) {
            return redirect(route('dashboard'));
        } else {
            $result = Terms::where('pt_panel_id', user('panel_type'))->first();
            if ($request->isMethod('post')) {
                $update = User::find(user('user_id'));
                $update->accept_terms = 1;
                if ($update->save()) {
                    return json_encode(['status' => 100, 'msg' => 'اطلاعات با موفقیت ثبت گردید', 'url' => route('dashboard')]);
                } else {
                    return json_encode(['status' => 500, 'msg' => 'خطا در ثبت اطلاعات']);
                }
            }
            return view('collection.terms', compact('result'));
        }
    }

    public function viewSystemFactor(Request $request, $id)
    {
        $master = SystemFactorMaster::where('sfm_id', $id)->first();
        if ($master->sfm_mode == 1) {
            $detail = SystemFactorDetail::where('sfd_master_id', $master->sfm_id)->get();
            $seller = findUser($master->sfm_creator_id, 'collection', 'all');
            $logo = UploadRoute::where('ur_collection_id', $seller->coll_id)
                ->where('ur_table_name', "logo")
                ->orderBy('ur_collection_id', 'desc')
                ->select([
                    "ur_path",
                ])
                ->first();
            $collection_online_gates = DB::table("collection_online_gates")
                ->select([
                    "og_title",
                    "og_id",
                ])
                ->join("online_gates", 'og_id', '=', 'cog_gate_id')
                ->where('og_status', 1)
                ->get();
            if ($request->isMethod('post')) {

                $result = call_sp("system_factor_payment", null, [
                    'factor_id' => $request->factor_id,
                    'user_id' => \user('user_id'),
                    'created_at' => time(),
                ]);
                if (isset($result))
                    if ($result[0]['result'] == 0)
                        return json_encode(['status' => 100, 'msg' => 'پرداخت با موفقیت انجام شد', 'url' => asset('/list-factor-system')]);
                return json_encode(['status' => 500, 'msg' => 'خطا در پرداخت']);
            }
            return view('collection.factor-system', compact('detail', 'master', 'seller', 'logo', 'collection_online_gates', 'id'));
        } else {
            abort(404);
        }

    }

    public function listFactorSystem()
    {
        $result = DB::table('system_factor_master')
            ->where('sfm_collection_id', collection_id());
        if (request()->has('amount') && request('amount') != "")
            $result->where("sfm_amount", "like", '%' . request('amount') . '%');
        if (\request()->has('date') && \request('date') != "")
            $result->where('sfm_created_at', '>=', date("Y-m-d H:i:s", jalali_to_timestamp(\request('date'))));
        if (request()->has('is_payed') && request('is_payed') != 0) {
            $mode_array = [1 => 0, 2 => 1];
            $result->where("sfm_is_payed", $mode_array[request('is_payed')]);
        }
        $result = $result->leftJoin('persons', 'prsn_user_id', 'sfm_creator_id');
        if (request()->has('user-create') && request('user-create') != "") {
            $result->where(function ($query) {
                $query->where("prsn_name", "like", '%' . request('user-create') . '%')
                    ->orWhere("prsn_family", "like", '%' . request('user-create') . '%');
            });
        }

        $result = $result->leftJoin('collection_persons', 'cp_prsn_id', 'prsn_id')
            ->leftJoin('collections', 'coll_id', 'cp_coll_id')
            ->where('sfm_mode', 1)
            ->latest('sfm_id')
            ->paginate(20);

        $collection_online_gates = DB::table("collection_online_gates")
            ->select([
                "og_title",
                "og_id",
            ])
            ->join("online_gates", 'og_id', '=', 'cog_gate_id')
            ->where('og_status', 1)
            ->get();
        return showData(view('collection.list-factor-system', compact('result', 'collection_online_gates')));
    }

    public function paymentFactorSystem(Request $request)
    {
        $factor = DB::table('system_factor_master')->where('sfm_id', $request->get('factor_id'))->first();
        if (!$factor)
            abort(404);

        $gateway = paymentClass(request('gate_id'));
        //$gateway->setCallback(url('/path/to/callback/route'));
        $gateway
            ->price($factor->sfm_amount - $factor->sfm_discount)
            ->ready();
        if (DB::table('gateway_transact')->where("transact_id", $gateway->transactionId())->update([
            "transact_type" => \App\Gateway\Enum::TRANSACTTION_SYSTEM_FACTOR,
            "transact_fk_id" => $factor->sfm_id, //id factor_or_user
            "transact_refId" => $gateway->refId() //id factor_or_user
        ])
        ) {
            return json_encode(['status' => 100, 'url' => $gateway->redirect(), 'msg' => 'در حال انتقال به بانک...']);
        }
        return json_encode(['status' => 500, 'msg' => 'خطا در اتصال به درگاه، مجددا تلاش فرمائید!']);
    }

    public function listMassage(Request $request)
    {
        $result = DB::table('messages')
            ->select([
                'msg_title',
                'coll_name',
                'msg_created_at',
                'msg_id',
                'msg_is_read',
            ])
            ->where('msg_receiver_collection_id', collection_id())
            ->where('msg_type', 0);
        if (request()->has('date_from') && request('date_from') != "") {
            $from = jalali_to_timestamp(request('date_from'));
            $result->where('msg_created_at', '>=', $from);
        }

        if (request()->has('date_to') && request('date_to') != "") {
            $to = jalali_to_timestamp(request('date_to'));
            $result->where('msg_created_at', '<=', $to);
        }
        if (request()->has('status') && request('status') != 0) {
            $mode_array = [1 => 0, 2 => 1];
            $result->where("msg_is_read", $mode_array[request('status')]);
        }
        $result = $result->leftJoin('persons', 'prsn_user_id', 'msg_sender_user_id')
            ->leftJoin('collection_persons', 'cp_prsn_id', 'prsn_id')
            ->leftJoin('collections', 'coll_id', 'cp_coll_id');
        if (request()->has('senderName') && request('senderName') != "")
            $result->where("coll_name", "like", '%' . request('senderName') . '%');

        $result = $result->paginate(10);
        if ($request->isMethod('post')) {
            if ($request->view == null) {
                return json_encode(['status' => 500, 'msg' => 'موردی انتخاب نشده است']);
            }
            $view = array_keys($request->view);
            foreach ($view as $row) {
                DB::table('messages')
                    ->where('msg_id', $row)
                    ->update([
                        'msg_is_read' => 1,
                        'msg_read_at' => time()
                    ]);
            }
            return json_encode(['status' => 100, 'msg' => 'اطلاعات با موفقیت ثبت شد']);
        }

        return showData(view('collection.list_massage', compact('result')));
    }

    public function viewMassage($id)
    {

        if (DB::table('messages')
                ->select([
                    'msg_is_read'
                ])
                ->where('msg_id', $id)
                ->first()->msg_is_read == 0) { // baraye sabt view
            DB::table('messages')
                ->where('msg_id', $id)
                ->update([
                    'msg_is_read' => 1
                ]);
        }
        $result = DB::table('messages')
            ->where('msg_id', $id)
            ->where('msg_type', 0)
            ->leftJoin('persons', 'prsn_user_id', 'msg_reader_user_id')
            ->leftJoin('collection_persons', 'cp_prsn_id', 'prsn_id')
            ->leftJoin('collections', 'coll_id', 'cp_coll_id')
            ->select([
                'msg_title',
                'msg_message',
                'coll_name',
                'msg_created_at',
                'msg_id',
                'msg_is_read',
                DB::raw("(select top 1 ur_path from upload_route where ur_table_name = 'logo' and ur_collection_id = coll_id order by ur_id desc) as logo")
            ])
            ->first();

        return view('collection.view_massage', compact('result'));
    }

    public function settingUser(Request $request)
    {
//        dd($request->factor_receiver);
        $role = json_decode(json_encode(DB::table('roles')
            ->select([
                'rol_id',
                'rol_label',
            ])
            ->where('rol_creator', user('user_id'))
            ->get()->toArray()), true);

        $factor = DB::table('setting')
            ->where('st_key', 'factor_receiver')
            ->where('st_collection_id', collection_id())
            ->first();
        $returned = DB::table('setting')
            ->where('st_key', 'product_return_permit')
            ->where('st_collection_id', collection_id())
            ->first();

        $min_factor_amount = DB::table('setting')
            ->where('st_key', 'min_factor_amount')
            ->where('st_collection_id', collection_id())
            ->first();
        $token=findUser(\user('user_id'),'collection','all')->coll_api_token;
        if ($request->isMethod('post')) {
            $request->validate([
                'min_factor_amount' => ['regex:/^[0-9]+$/']
            ]);
            if ($request->factor_receiver != null) {
                if ($factor) {
                    DB::table('setting')
                        ->where('st_key', 'factor_receiver')
                        ->where('st_collection_id', collection_id())
                        ->update([
                            'st_value' => $request->factor_receiver,
                        ]);

                } else {
                    DB::table('setting')
                        ->insert([
                            'st_key' => 'factor_receiver',
                            'st_value' => $request->factor_receiver,
                            'st_collection_id' => collection_id(),
                            'st_user_id' => 0
                        ]);
                }
            }
            if ($min_factor_amount) {
                DB::table('setting')
                    ->where('st_key', 'min_factor_amount')
                    ->where('st_collection_id', collection_id())
                    ->update([
                        'st_value' => $request->min_factor_amount,
                    ]);

            } else {
                DB::table('setting')
                    ->insert([
                        'st_key' => 'min_factor_amount',
                        'st_value' => $request->min_factor_amount,
                        'st_collection_id' => collection_id(),
                        'st_user_id' => 0
                    ]);
            }

            if ($returned) {
                DB::table('setting')
                    ->where('st_key', 'product_return_permit')
                    ->where('st_collection_id', collection_id())
                    ->update([
                        'st_value' => (request()->has('returned'))?1:0,
                    ]);

            } else {
                DB::table('setting')
                    ->insert([
                        'st_key' => 'product_return_permit',
                        'st_value' => (request()->has('returned'))?1:0,
                        'st_collection_id' => collection_id(),
                        'st_user_id' => 0
                    ]);
            }


            return json_encode(['status' => 100, 'msg' => 'اطلاعات با موفقیت ثبت گردید']);

        }
        return view('collection.setting', compact('role', 'factor', 'returned', 'min_factor_amount','token'));
    }

    public function notesDashboard(Request $request)
    {
        if ($request->value == '') {
            DB::table('setting')
                ->where('st_key', 'notes_dashboard')
                ->where('st_collection_id', collection_id())
                ->where('st_user_id', user('user_id'))
                ->delete();
        } else {
            if (getNotesDashboard()) {
                DB::table('setting')
                    ->where('st_key', 'notes_dashboard')
                    ->where('st_collection_id', collection_id())
                    ->where('st_user_id', user('user_id'))
                    ->update([
                        'st_key' => 'notes_dashboard',
                        'st_value' => $request->value,
                        'st_collection_id' => collection_id(),
                        'st_user_id' => user('user_id'),
                    ]);
            } else {
                DB::table('setting')
                    ->insert([
                        'st_key' => 'notes_dashboard',
                        'st_value' => $request->value,
                        'st_collection_id' => collection_id(),
                        'st_user_id' => user('user_id'),
                    ]);
            }
        }
        return json_encode(['status' => 100, 'msg' => 'یادداشت شما موفقیت ثبت گردید']);
    }

    public function updateWallet(Request $request)
    {
        $user = findUser($request->user_id);
        if ($user->creator == user('user_id')) {////check ke karbar khod hamin sherkat bashe
            $request->validate([
                'amount' => ['required', 'regex:/^[0-9]+$/'],
                'type' => ['required', 'exists:baseinfos,bas_id'],
            ]);
            $creator = DB::table('persons')
                ->select([
                    'prsn_name',
                    'prsn_family'
                ])
                ->where('prsn_user_id', user('user_id'))
                ->first();
            $find_amount = User::find($request->user_id);
            if ($request->type == 1) {
                if (\user('amount') >= $request->amount) { ///check wallet creator
                    $amount = $find_amount->amount + $request->amount;

                    if ($request->description)
                        $description = $request->description;
                    else
                        $description = " مبلغ افزایش گردیده است (پیام سیستم)";
                } else {
                    return json_encode(['status' => 500, 'msg' => 'موجودی کیف پول شما کمتر از مبلغ وارد شده است']);
                }
            } elseif ($request->type == 2) {
                if ($find_amount->amount >= $request->amount) {
                    $amount = $find_amount->amount - $request->amount;
                } else
                    return json_encode(['status' => 300]);
                if ($request->description)
                    $description = $request->description;
                else
                    $description = " مبلغ کسر گردیده است (پیام سیستم)";
            }
            if (DB::table('users')
                ->where('user_id', $request->user_id)
                ->update([
                    'amount' => $amount
                ])) {
                DB::table('bag_transacts')
                    ->insert([
                        'bt_user_id' => $request->user_id,
                        'bt_creator' => user('user_id'),
                        'bt_time' => time(),
                        'bt_amount' => $request->amount,
                        'bt_description' => $description,
                    ]);
                $amount_creator = findUser(user('user_id'), 'amount');
                if ($request->type == 1) {
                    DB::table('users')->where('user_id', \user('user_id'))
                        ->update([
                            'amount' => $amount_creator - $request->amount
                        ]);
                } elseif ($request->type == 2) {
                    DB::table('users')->where('user_id', \user('user_id'))
                        ->update([
                            'amount' => $request->amount + $amount_creator
                        ]);
                }
                return json_encode(['status' => 100, 'msg' => 'با موفقیت انجام شد!']);
            } else
                return json_encode(['status' => 500, 'msg' => 'خطا در ثبت اطلاعات!']);
        } else
            return json_encode(['status' => 500, 'msg' => 'خطا در ثبت اطلاعات!']);
    }

    public function checkoutPermitted(Request $request)
    {
        $result = call_sp("checkout_request_sp", null, [
            'user_id' => user('user_id'),
            'created_at' => time(),
            'price' => $request->amount,
            'description ' => null,

        ]);

        if (isset($result))
            if ($result[0]['result'] == 0)
                return json_encode(['status' => 100, 'msg' => 'اطلاعات با موفقیت ثبت شد']);
            elseif ($result[0]['result'] == -3)
                return json_encode(['status' => 500, 'msg' => 'مبلغ درخواست شده کمتر از حد مجاز تسویه است']);
        return json_encode(['status' => 500, 'msg' => 'خطا در ثبت اطلاعات']);
    }

    public function changeMobile(Request $request)
    {
        $time = \user('otp_expire_time');
        if ($request->type == 1) {///send code mobile feli
            if (isset($time) && $time > time()) {
                return json_encode(['status' => 300, 'msg' => 'برای درخواست مجدد باید 3 دقیقه صبر کنید']);
            } else {
                $code = generateRandomNumber(5);
                DB::table('users')
                    ->where('user_id', \user('user_id'))
                    ->update([
                        'temp' => findUser(user('user_id'), 'persons')->prsn_mobile1,
                        'otp' => $code,
                        'otp_expire_time' => time() + 120,
                    ]);
                sendSMS(generateMessage("sms_changeMobile", ["%code%" => $code]), [findUser(user('user_id'), 'persons')->prsn_mobile1]);

                return json_encode(['status' => 100, 'msg' => 'کد تاییدیه برای شماره تلفن فعلی ارسال گردید']);
            }
        } elseif ($request->type == 2) { //check code
            if (user('otp') == $request->code) {
                if (isset($time) && $time > time()) {
                    return json_encode(['status' => 100, 'msg' => 'کد صحیح است']);
                } else
                    return json_encode(['status' => 500, 'msg' => 'کد غیرفعال شده است']);
            } else
                return json_encode(['status' => 500, 'msg' => 'کد اشتباه است']);
        } elseif ($request->type == 3) {///send code mobile jadid
            $request->validate([
                'mobile' => ['max:11', 'regex:/([0-9]+){11}/']
            ]);
            $check_mobile_new = DB::table('persons')
                ->select('count(1)')
                ->where('prsn_mobile1', $request->mobileNew)
                ->orWhere('prsn_mobile2', $request->mobileNew)->count();
            if ($check_mobile_new == 0) {
                $code = generateRandomNumber(5);
                DB::table('users')
                    ->where('user_id', \user('user_id'))
                    ->update([
                        'temp' => $request->mobileNew,
                        'otp' => $code,
                        'otp_expire_time' => time() + 120,
                    ]);
                sendSMS(generateMessage("sms_changeMobile", ["%code%" => $code]), [$request->mobileNew]);

                return json_encode(['status' => 100, 'msg' => 'کد تاییدیه برای شماره تلفن جدید ارسال گردید']);
            } else
                return json_encode(['status' => 500, 'msg' => 'شماره در سامانه موجود است']);
        } elseif ($request->type == 4) { //check code
            if (\user('otp') == $request->codeNew) {
                if (isset($time) && $time > time()) {
                    DB::table('persons')
                        ->where('prsn_user_id', \user('user_id'))
                        ->update([
                            'prsn_mobile1' => \user('temp')
                        ]);
                    return json_encode(['status' => 100, 'msg' => 'کد صحیح است']);
                } else
                    return json_encode(['status' => 500, 'msg' => 'کد غیرفعال شده است']);
            } else
                return json_encode(['status' => 500, 'msg' => 'کد اشتباه است']);
        }
    }

    public function changeEmail(Request $request)
    {
        $time = \user('otp_expire_time');
        if ($request->type == 1) {///send code email feli
            if (isset($time) && $time > time()) {
                return json_encode(['status' => 300, 'msg' => 'برای درخواست مجدد باید 3 دقیقه صبر کنید']);
            } else {
                $code = generateRandomNumber(5);
                DB::table('users')
                    ->where('user_id', \user('user_id'))
                    ->update([
                        'temp' => \user('email'),
                        'otp' => $code,
                        'otp_expire_time' => time() + 120,
                    ]);
                sendEmail(generateMessage("email_changeEmail", ["%code%" => $code]), [\user('email')]);

                return json_encode(['status' => 100, 'msg' => 'کد تاییدیه برای ایمیل فعلی ارسال گردید']);
            }
        } elseif ($request->type == 2) { //check code
            if (user('otp') == $request->code) {
                if (isset($time) && $time > time()) {
                    return json_encode(['status' => 100, 'msg' => 'کد صحیح است']);
                } else
                    return json_encode(['status' => 500, 'msg' => 'کد غیرفعال شده است']);
            } else
                return json_encode(['status' => 500, 'msg' => 'کد اشتباه است']);
        } elseif ($request->type == 3) {///send code email jadid
            $request->validate([
                'mobile' => ['email']
            ]);
            $check_email_new = DB::table('users')
                ->select('count(1)')
                ->where('email', $request->emailNew)->count();
            if ($check_email_new == 0) {
                $code = generateRandomNumber(5);
                DB::table('users')
                    ->where('user_id', \user('user_id'))
                    ->update([
                        'temp' => $request->emailNew,
                        'otp' => $code,
                        'otp_expire_time' => time() + 120,
                    ]);
                sendEmail(generateMessage("email_changeEmail", ["%code%" => $code]), [$request->emailNew]);

                return json_encode(['status' => 100, 'msg' => 'کد تاییدیه برای ایمیل جدید ارسال گردید']);
            } else
                return json_encode(['status' => 500, 'msg' => 'ایمیل در سامانه موجود است']);
        } elseif ($request->type == 4) { //check code
            if (\user('otp') == $request->codeNew) {
                if (isset($time) && $time > time()) {
                    DB::table('users')
                        ->where('user_id', \user('user_id'))
                        ->update([
                            'email' => \user('temp')
                        ]);
                    return json_encode(['status' => 100, 'msg' => 'کد صحیح است']);
                } else
                    return json_encode(['status' => 500, 'msg' => 'کد غیرفعال شده است']);
            } else
                return json_encode(['status' => 500, 'msg' => 'کد اشتباه است']);
        }
    }

    public function createToken()
    {
        $token = findUser(\user('user_id'), 'collection', 'all');
        try {
            DB::table('collections')
                ->where('coll_id', $token->coll_id)
                ->update([
                    'coll_api_token' => Str::random(100)
                ]);
            return json_encode(['status' => 100, 'msg' => 'توکن جدید ایجاد گردید']);
        }catch(\Exception $e){
            return json_encode(['status' => 500, 'msg' => 'خطا در انجام عملیات']);
        }
    }
}
