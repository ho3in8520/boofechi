<?php

namespace App\Http\Controllers;

use App\Component\Tools;
use App\Http\Requests\UploadRequest;
use App\Person;
use App\UploadRoute;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class UploadRouteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $oldForm = null;
        $result = DB::table('panel_wizard_completed')->select([
            'pwc_accepted',
            'pwc_form_id',
            'pwc_reason'
        ])
            ->join('panel_wizards', 'pw_id', '=', 'pwc_form_id')
            ->where('pwc_user_id', user('user_id'))
            ->where('pw_form_address', 'upload_route')
            ->first();
        if ($result) {
            //form ghablan sabt shode
            if ($result->pwc_accepted == 1 || $result->pwc_accepted == 0) {
                //taeed shode boro be address badi
                //go to dashboard
                return redirect(asset('/dashboard'));


            } else if ($result->pwc_accepted == 2) {
                //mode update
                session()->flash('error', config('first_config.message.user-waiting-error') . $result->pwc_reason);
            }
        }
        if (request()->has('prsn_id') && request('prsn_id')) {
            $person = Person::find(request('prsn_id'));
            $url = "/company/manage-categories/" . $person->collections->first()->coll_id;
            return view('upload_route.form', compact('url'));
        } else
            return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(UploadRequest $request)
    {
        $result = DB::table('panel_wizard_completed')->select([
            'pwc_accepted',
            'pwc_form_id',
            'pwc_reason'
        ])
            ->join('panel_wizards', 'pw_id', '=', 'pwc_form_id')
            ->where('pwc_user_id', user('user_id'))
            ->where('pw_form_address', 'upload_route')
            ->first();
        try {
            if (!$request->has('ur_url') || count($request->ur_url) != count($request->ur_type_id))
                return json_encode(["status" => "422", "msg" => " فایل آپلودی خود را انتخاب نمایید"]);
            UploadRoute::where('ur_table_name', 'collections')
                ->where('ur_collection_id', collection_id())
                ->delete();
            for ($i = 0; $i < count($request->ur_url); $i++) {
                $route = uploadFile($request->file('ur_url')[$i], 'collection', user("user_id"));
                $person = Person::find($request->prsn_id);
               $up= UploadRoute::create([
                    'ur_path' => $route,
                    'ur_fk_id' => user("user_id"),
                    'ur_collection_id' => $person->collections()->first()->toArray()['coll_id'],
                    'ur_description' => $request->ur_description[$i],
                    'ur_type_id' => $request->ur_type_id[$i],
                    'ur_table_name' => 'collections',
                ]);
            }
            if (user('panel_type') == 44 || user('panel_type') == 45) {
                //update panel_wizard_completed
                if ($result) {
                    $fin_id = DB::table('panel_wizards as b')
                        ->select([
                            'p.pwc_id',
                            'b.pw_id',
                        ])
                        ->where('pw_panel_type_id', user('panel_type'))
                        ->leftJoin('panel_wizard_completed as p', 'b.pw_id', '=', 'p.pwc_form_id')
                        ->where('pwc_user_id', user('user_id'))
                        ->where('pw_form_address', 'upload_route')
                        ->first();
                    DB::table('panel_wizard_completed')
                        ->where('pwc_id', '=', $fin_id->pwc_id)
                        ->update([
                            'pwc_form_id' => $fin_id->pw_id,
                            'pwc_user_id' => user('user_id'),
                            'pwc_accepted' => 0,
                        ]);

                } else if ($result == null) {
                    //insert panel_wizard_completed
                    $fin_type = DB::table('panel_wizards as b')
                        ->select([
                            'b.pw_panel_type_id'
                        ])
                        ->where('b.pw_panel_type_id', '=', user('panel_type'))
                        ->where('pw_form_address', 'upload_route')
                        ->select([
                            'b.pw_id'
                        ])->first();
                    DB::table('panel_wizard_completed')
                        ->insert([
                            'pwc_form_id' => $fin_type->pw_id,
                            'pwc_user_id' => user('user_id'),
                            'pwc_accepted' => '0',
                        ]);
                }
            }
            DB::commit();
            $url = getUrlByTypeCollection($person->collections->first());
            if (user('panel_type') == 44 || user('panel_type') == 45) {
                return json_encode(['status' => '100', 'msg' => 'اطلاعات با موفقیت ثبت شد']);
            } else {
                return json_encode(['status' => '100', 'msg' => config('first_config.message.success_message'), 'url' => $url]);
            }
        } catch
        (\Exception $exception) {
            return json_encode(["status" => "500", 'msg' => $exception->getMessage()]);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
