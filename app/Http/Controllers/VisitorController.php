<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VisitorController extends Controller
{
    public function __construct()
    {
//        $this->middleware('permission:menu_user');
        $this->middleware('permission:list_visitors', ['only' => ['visitorList']]);
        $this->middleware('permission:purchase_for_subscription', ['only' => ['purchaseForSubscription', 'purchaseCheckOtp','exitPurchaseForSubscription']]);
        $this->middleware('permission:view_porsants', ['only' => ['porsant']]);
        $this->middleware('permission:view_subscription', ['only' => ['subscription']]);
    }

    public function exitPurchaseForSubscription(Request $request)
    {
        if (request()->isMethod("post")) {
            exitFromPurchasingMode();
        }
    }

    public function purchaseCheckOtp(Request $request, $id)
    {
        if (session()->has('purchasing_user_id'))
            return redirect(asset('/product-list'));

        $user = DB::Table("users")->where("code", $id)->first();
        if (!$user || user('otp') != $id)
            abort(404);

        session()->remove('purchasing_user_id');

        if (request()->isMethod("post")) {
            $otp = request()->get('otp');
            $request->validate([
                'otp' => ['required', function ($attribute, $value, $fail) use ($otp, $user) {
                    if ($user->otp != request()->get('otp') || $user->otp_expire_time < time())
                        $fail("کد وارد شده نامعتبر است!");
                }]
            ]);
            session()->put('purchasing_user_id', $user->user_id);
            DB::Table("users")->where("user_id", user("user_id"))->update([
                'otp' => $user->user_id,
                'temp' => time() + (setting('purchase_for_subscription_life_time') * 60)
            ]);
            $url = asset('/product-list');
            $factor_master = DB::table("factor_master")->where("fm_creator_id", $user->user_id)
                ->where(function ($query) {
                    return $query->where('fm_mode', 0)->orWhere("fm_is_payed", 0);
                })
                ->first();
            if ($factor_master) {
                if ($factor_master->fm_mode == 0)
                    $url = asset("basket-items") . "/" . $factor_master->fm_id;
                else if ($factor_master->fm_is_payed == 0)
                    $url = asset("factor/payment/") . "/" . $factor_master->fm_id;
            }
            return json_encode(['status' => 100, 'msg' => 'کد تایید پذیرفته شد!', 'url' => $url]);
        }

        return view('visitor.purchase_check_otp');
    }

    public function purchaseForSubscription(Request $request)
    {
        if (session()->has('purchasing_user_id')) {
            return redirect(asset('/product-list'));
        }

        if (request()->isMethod("post")) {
            $request->validate([
                'code' => 'required|exists:users,code'
            ]);
            $code = generateRandomNumber(6);
            $u = DB::Table("users")->where("code", $request->get('code'))->first();
            if (DB::Table("users")->where("code", $request->get('code'))->update([
                'otp' => $code,
                'otp_expire_time' => (setting('otp_expire_time') * 60) + time()
            ])
            ) {
                DB::Table("users")->where("user_id", user("user_id"))->update([
                    'otp' => $request->get('code')
                ]);
                sendMessage(generateMessage("sms_visitorBuy", ["%code%" => $code]), [$u->user_id]);
                return json_encode(["status" => 100, 'msg' => "کد تایید ارسال شد!", 'url' => asset('purchase-check-otp') . '/' . $request->get('code')]);
            }
            return json_encode(["status" => 500, 'msg' => "خطا در ارسال کد تایید"]);
        }
        return view('visitor.purchase_for_subscription');
    }

    public function porsant()
    {
        $porsant = DB::table("factor_master")
            ->select([
                DB::raw("floor(dbo.calcPercent(floor(dbo.get_factor_amount(fm_id,2))," . user('porsant_percent') . ")) as porsant"),
                DB::raw("concat(coll_name,' ',code) as name"),
                'fm_created_at',
                'fm_no',
                DB::raw('floor(dbo.get_factor_amount(fm_id,2)) as fm_amount')
            ])
            ->join("users", "user_id", "=", "fm_creator_id")
            ->join("collections", "coll_id", "=", "fm_self_collection_id");
        if (\request()->has('name') && \request('name') != "") {
            $name = \request('name');
            $porsant->where(function ($query) use ($name) {
                return $query->where("coll_name", "like", '%' . \request('name') . '%')
                    ->orWhere("code", "like", '%' . \request('name') . '%');
            });
        }
        if (\request()->has('factor_number') && \request('factor_number') != "")
            $porsant->where('fm_no', \request('factor_number'));

        if (\request()->has('date_from') && \request('date_from') != "")
            $porsant->where('fm_created_at', '>=', jalali_to_timestamp(\request('date_from')));

        if (\request()->has('date_to') && \request('date_to') != "")
            $porsant->where('fm_created_at', '<=', jalali_to_timestamp(\request('date_to'), '/', false));

        $porsant = $porsant->where("fm_porsant_user_id", user("user_id"))
            ->where("fm_status", 2)
            ->latest("fm_id")
            ->paginate(20);

        return showData(view('visitor.porsants', compact('porsant')));
    }

    public function poursant_cleanse(Request $request)
    {
        if ($request->isMethod('post')) {
            $request->validate([
                'amount' => ['required', 'regex:([0-9]+)']
            ]);
        }
        return view('visitor.poursant_cleanse');
    }

    public function subscription()
    {
        $states = \App\City::where('c_parent_id', 0)->get()->toArray();
        if (request()->has('state') && request()->get('state') != "")
            $cities = \App\City::where('c_parent_id', request()->get('state'))->get();
        else
            $cities = [];

        $subscription = DB::table("users")
            ->select([
                DB::raw("concat(coll_name,' ',code) as name"),
                'state.c_name as state',
                'city.c_name as city',
                'users.created_at',
                'code'
            ])
            ->join("persons", "prsn_user_id", "=", "user_id")
            ->join("collection_persons", "prsn_id", "=", "cp_prsn_id")
            ->join("collections", "coll_id", "=", "cp_coll_id")
            ->join("cities as city", "prsn_city_id", "=", "city.c_id")
            ->join("cities as state", "prsn_state_id", "=", "state.c_id");

        if (\request()->has('name') && \request('name') != "") {
            $name = \request('name');
            $subscription->where(function ($query) use ($name) {
                return $query->where("coll_name", "like", '%' . \request('name') . '%')
                    ->orWhere("code", "like", '%' . \request('name') . '%');
            });
        }

        if (\request()->has('date_from') && \request('date_from') != "")
            $subscription->where('users.created_at', '>=', date("Y-m-d H:i:s", jalali_to_timestamp(\request('date_from'))));

        if (\request()->has('date_to') && \request('date_to') != "")
            $subscription->where('users.created_at', '<=', date("Y-m-d H:i:s", jalali_to_timestamp(\request('date_to'), '/', false)));

        if (\request()->has('state') && \request('state') != "")
            $subscription->where('state.c_id', \request('state'));

        if (\request()->has('city') && \request('city') != "")
            $subscription->where('city.c_id', \request('city'));

        $subscription = $subscription->where("reagent_id", user("user_id"))
            ->paginate(20);
        return showData(view('visitor.subscriptions', compact('states', 'cities', 'subscription')));
    }

    public function visitorList(Request $request)
    {
        $result = DB::table('users as us')
            ->select([
                'user_id',
                'porsant_percent',
                'prsn_name',
                'created_at',
                DB::raw('(select count(1) from users where users.reagent_id = us.user_id) as sub_users')
            ])
            ->where('panel_type', 45);

        //porsant
        if (\request()->has('porsant_from') && \request('porsant_from') != "")
            $result->where('porsant_percent', '>=', request('porsant_from'));

        if (\request()->has('porsant_to') && \request('porsant_to') != "")
            $result->where('porsant_percent', '<=', request('porsant_to'));

        //date
        if (\request()->has('date_from') && \request('date_from') != "")
            $result->where('created_at', '>=', date("Y-m-d H:i:s", jalali_to_timestamp(\request('date_from'))));

        if (\request()->has('date_to') && \request('date_to') != "")
            $result->where('created_at', '<=', date("Y-m-d H:i:s", jalali_to_timestamp(\request('date_to'), '/', false)));

        $result = $result->leftJoin('persons', 'user_id', 'prsn_user_id');
        // name visitor
        if (\request()->has('nameVisitor') && \request('nameVisitor') != "")
            $result->where("prsn_name", "like", '%' . request('nameVisitor') . '%');

        if (\request()->has('subset') && \request('subset') != "") {
            $result->where(DB::raw("(select count(1) from users where users.reagent_id = us.user_id)"), request('subset'));
        }

        $result = $result->paginate(10);

        if ($request->isMethod('post')) {
            $update = User::find($request->id);
            $update->porsant_percent = $request->percent;
            if ($update->save())
                return json_encode(['status' => 100, 'msg' => 'اطلاعات با موفقیت ثبت شد']);
            return json_encode(['status' => 500, 'msg' => 'خطا در ثبت اطلاعات']);
        }
        return showData(view('visitor.index', compact('result')));
    }

}
