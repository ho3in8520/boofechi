<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PanelWizardCompleted extends Model
{
    protected $fillable='panel_wizard_completed';
    protected $primaryKey='pwc_id';
}
