<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductComplimentary extends Model
{
    protected $table = "product_complimentary";
    protected $primaryKey = "pc_id";
    public $timestamps = false;
    protected $guarded = [];
}
