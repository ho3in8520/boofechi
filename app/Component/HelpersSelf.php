<?php

use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

function emptyBasket()
{
    //convert to second
    $time = setting('basket_life_time') * 60;

    $result = DB::table('factor_master')
        ->select([
            'fm_id'
        ])
        ->where('fm_creator_id', user('user_id'))
        ->where('fm_mode', 0)
        ->whereRaw('fm_created_at + ' . $time . ' < ' . time())
        ->get();
    if ($result) {
        foreach ($result as $factor) {
            DB::table('factor_returns')->where('fr_master_id', $factor->fm_id)->delete();
            DB::table('factor_routing')->where('far_factor_id', $factor->fm_id)->delete();
            DB::table('factor_detail')->where('fd_master_id', $factor->fm_id)->delete();
            DB::table('factor_master')->where('fm_id', $factor->fm_id)->delete();
        }
    }
}

function createPrintTitleFromFilters($array)
{
    echo "";
    $reportType = [
        "totally" => "سرجمع محصولات",
        "by-factor" => "به تفکیک فاکتور",
        "by-date" => "به تفکیک تاریخ",
        "by-seller" => "به تفکیک فروشنده",
        "by-buyer" => "به تفکیک خریدار",
        "by-state" => "به تفکیک استان",
        "by-city" => "به تفکیک شهر",
        "factors" => "گزارش فاکتورها",
        "by-agent" => "به تفکیک نمایندگی",
    ];

    $title = "";
    try {
        if ($array['fm_collection_id'] != 0)
            $title .= "فروشنده: " . DB::table("collections")->where("coll_id", $array['fm_collection_id'])->first()->coll_name . "/";
        if ($array['fm_self_collection_id'] != 0)
            $title .= "خریدار: " . DB::table("collections")->where("coll_id", $array['fm_self_collection_id'])->first()->coll_name . "/";
        if ($array['fm_no'] != 0)
            $title .= "شماره فاکتور: " . $array['fm_no'] . "/";
        if ($array['fm_date_from'] != '____/__/__')
            $title .= "تاریخ از: " . $array['fm_date_from'] . "/";
        if ($array['fm_date_to'] != '____/__/__')
            $title .= "تاریخ تا: " . $array['fm_date_to'] . "/";
        if ($array['fm_description'] != '')
            $title .= "توضیحات: " . $array['fm_description'] . "/";
        if ($array['fm_porsant_user_id'] != '')
            $title .= "کاربر دریافت کننده پورسانت: " . DB::table("users")->where("user_id", $array['fm_porsant_user_id'])->first()->username . "/";
        if ($array['fm_creator_id'] != '')
            $title .= "کاربر ثبت کننده: " . DB::table("users")->where("user_id", $array['fm_creator_id'])->first()->username . "/";
        if ($array['cp_id'] != '')
            $title .= "محصول: " . DB::table("collection_products")->join("products", "cp_product_id", "prod_id")->where("cp_id", $array['cp_id'])->first()->prod_name . "/";
        if ($array['state_id'] != '')
            $title .= "استان: " . DB::table("cities")->where("c_id", $array['state_id'])->first()->c_name . "/";
        if ($array['city_id'] != '')
            $title .= "شهر: " . DB::table("cities")->where("c_id", $array['city_id'])->first()->c_name . "/";
        if (isset($array['report_type']) && $array['report_type'] != '') {
            if (\Request::route()->getName() == 'report-sale-factors')
                $type = "گزارش فروش فاکتورها";
            if (\Request::route()->getName() == 'report-sale-products')
                $type = "گزارش فروش محصولات";
            if (\Request::route()->getName() == 'report-sale-totally')
                $type = "گزارش فروش سرجمع";
            $title .= "نوع گزارش: " . $type . '-' . $reportType[$array["report_type"]];
        }

        $title = trim($title, "/");

    } catch (Exception $e) {
        return "خطا در تولید عنوان گزارش";
    }
    return $title;
}

function paymentClass($method)
{
    try {
        $method = DB::table('online_gates')->where('og_id', $method)->first();
        $class = "\\App\\Gateway\\{$method->og_class}\\{$method->og_class}";
        return \Gateway::make(new $class());
    } catch (Exception $e) {
        return false;
    }
}

function blockCollectionAndItUsers($collection_id, $type)
{
    if ($type == "block") {
        DB::beginTransaction();
        try {
            DB::table('collections')->where('coll_id', $collection_id)->update([
                'coll_hide' => 1
            ]);
            $user_ids = DB::table('persons')->select([
                'prsn_user_id'
            ])
                ->leftJoin('collection_persons', 'cp_prsn_id', '=', 'prsn_id')
                ->where('cp_coll_id', $collection_id)
                ->get()->toArray();
            $user_ids = array_column($user_ids, 'prsn_user_id');
            DB::table('users')->whereIn('user_id', $user_ids)->update([
                'is_active' => 0
            ]);

            $array_for_violation_log = [];
            foreach ($user_ids as $id) {
                array_push($array_for_violation_log, [
                    "vl_user_id" => $id,
                    "vl_type" => 1,
                    "vl_time" => time(),
                    "vl_reason" => null
                ]);
            }
            DB::table('violation_log')->insert($array_for_violation_log);
            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollback();
            return false;
        }
    } else if ($type == "unblock") {
        DB::beginTransaction();
        try {
            DB::table('collections')->where('coll_id', $collection_id)->update([
                'coll_hide' => 0
            ]);
            $user_ids = DB::table('persons')->select([
                'prsn_user_id'
            ])
                ->leftJoin('collection_persons', 'cp_prsn_id', '=', 'prsn_id')
                ->where('cp_coll_id', $collection_id)
                ->get()->toArray();
            $user_ids = array_column($user_ids, 'prsn_user_id');
            DB::table('users')->whereIn('user_id', $user_ids)->update([
                'is_active' => 1
            ]);

            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollback();
            return false;
        }
    }
}

function getPanelColor()
{
    $array = [
        39 => 'sublime-vivid',
        40 => 'crystal-clear',
        41 => 'aqua-marine',
        42 => 'danger',
        43 => 'primary',
        44 => 'timber',
        45 => 'timber',
        46 => 'white',
    ];
    return $array[user('panel_type')];
}

function getBarColor($panel_type)
{
    $array = [
        39 => '#F44336',
        40 => '#F44336',
        41 => '#9C27B0',
        42 => '#3F51B5',
        43 => '#CDDC39',
        44 => '#00E676',
        45 => '#FF5722',
        46 => '#E91E63',
    ];
    return $array[$panel_type];
}

function getPanelBg()
{
    $array = [
        39 => '/theme/img/sidebar-bg/04.jpg',
        40 => '/theme/img/sidebar-bg/08.jpg',
        41 => '/theme/img/sidebar-bg/06.jpg',
        42 => '/theme/img/sidebar-bg/05.jpg',
        43 => '/theme/img/sidebar-bg/02.jpg',
        44 => '/theme/img/sidebar-bg/01.jpg',
        45 => '/theme/img/sidebar-bg/01.jpg',
        46 => '/theme/img/sidebar-bg/07.jpg',
    ];
    return $array[user('panel_type')];
}

function getPanelName()
{
    return DB::table('baseinfos')->select('bas_value')->where('bas_id', user('panel_type'))->first()->bas_value;
}

function findUser($id, $field = null, $type = 'name')
{
    $user = DB::table("users")->where('user_id', $id)->first();
    if ($field == null)
        return $user;

    if ($field == "panel_type") {
        $panel_type = DB::table('baseinfos')->select(["bas_value", "bas_id"])->where('bas_id', $user->panel_type)->first();
        if ($type == 'id')
            return $panel_type->bas_id;
        return $panel_type->bas_value;
    } else if ($field == "agent") {
        $ag_domain = DB::table('agents')->select(["ag_domain", "ag_id"])->where('ag_id', $user->agent_id)->first();
        if ($type == 'id')
            return $ag_domain->ag_id;
        return $ag_domain->ag_domain;
    } else if ($field == "collection") {
        $collection = DB::table('persons')
            ->select(['persons.*', 'collections.*'])
            ->join('collection_persons', 'cp_prsn_id', '=', 'prsn_id')
            ->join('collections', 'cp_coll_id', '=', 'coll_id')
            ->where('prsn_user_id', $user->user_id)->first();
        if ($type == 'id')
            return $collection->coll_id;
        else if ($type == "all")
            return $collection;
        return $collection->coll_name;
    } else if ($field == "city") {
        $city = DB::table('persons')
            ->select(['c_id', 'c_name'])
            ->join('cities', 'c_id', '=', 'prsn_city_id')
            ->where('prsn_user_id', $user->user_id)->first();
        if ($type == 'id')
            return $city->c_id;
        return $city->c_name;
    } else if ($field == "type_class") {
        $class = DB::table('persons')
            ->select(['coc_typeClass_id'])
            ->where('prsn_user_id', $id)
            ->leftJoin('collection_persons', 'prsn_id', 'cp_prsn_id')
            ->leftJoin('collection_classes', 'cp_coll_id', 'coc_coll_id')
            ->get();
        $class = json_decode(json_encode($class), true);
        return $class;
    } else if ($field == "persons") {
        $persons = DB::table('persons')
            ->where('prsn_user_id', $id)
            ->first();
        return $persons;
    } else
        return $user->{$field};
}

function getDomain()
{
    return str_replace('www.', '', \Request::server("SERVER_NAME"));
}

function getAgent($domain = null)
{
    if ($domain == null)
        $domain = getDomain();
    return DB::table('agents')->where('ag_domain', $domain)->first();
}

function getTopAgent($domain = null)
{
    $agent = getAgent($domain);
    $top_agent = 0;
    if ($agent->ag_parent_id != 0) {
        $top_domain = DB::table('agents')->where('ag_id', $agent->ag_parent_id)->one();
        if ($top_domain)
            $top_agent = $top_domain->ag_id;
    }
    return $top_agent;
}

function getRoles($type = null)
{
    $domain = getDomain();
    $agent = getAgent($domain);
    $top_agent = getTopAgent($domain);
    $sql = "
        select
            rol_id,
            (
            case
               agent_id
            when
                {$agent->ag_id}
            then
                concat('$domain','/',rol_label)
            else
                rol_label
            end
            ) as rol_label
        from users
        join user_roles on ur_user_id = user_id
        join roles on ur_rol_id = rol_id
        where rol_receive_tickets = 1
        ";

    if ($type != "self-domain") {
        $sql .= " and agent_id in ($agent->ag_id, $top_agent)";
    } else
        $sql .= " and agent_id = $agent->ag_id";


    $sql .= " group by rol_id , rol_label,agent_id";

    $result = DB::select($sql);
    $result = json_decode(json_encode($result), true);
    return $result;
}

function getTopRoles()
{
    $user_id = user('creator_id');
}

function getCategories($collection_id = null, $user_id = null, $parent_id = null)
{
    if ($collection_id == null)
        $collection_id = collection_id();

    $categories = DB::table("categories")->select([
        "cat_id as id",
        "cat_parent_id as parent_id",
        "cat_name as name"
    ])
        ->where('cat_status', 1)
        ->orderBy("cat_parent_id", "asc");

    if (user('panel_type') != 39) {//not is admin
        $categories->leftJoin('collection_categories', 'cat_id', '=', 'cc_category_id');
        $categories->where('cc_collection_id', $collection_id);
    }
    if ($parent_id != null) {
        $categories->where('cat_parent_id', $parent_id);
    }
    $categories = $categories->get()->toArray();
    $categories = convertToHierarchy(json_decode(json_encode($categories), true));
    return $categories;
}

function getAreaSupport($type = 2, $collection_id = null, $user_id = null, $parent_id = null)
{
    //type = 1 => get states & type = 2 get cities
    if ($collection_id == null)
        $collection_id = collection_id();
    if ($user_id == null)
        $user_id = user('user_id');
    $person = \App\Person::where('prsn_user_id', $user_id)->first();
    if ($type == 1) {//get states
        if (user("panel_type") == 39)//is admin
            $area_supports = \App\City::where("c_parent_id", 0)->get();
        else {
            $area_supports = \Illuminate\Support\Facades\DB::table('area_support')
                ->select([
                    'parent.c_id',
                    'parent.c_name'
                ])
                ->where('as_collection_id', $collection_id)
                ->join('cities', 'as_city_id', '=', 'cities.c_id')
                ->join('cities as parent', 'cities.c_parent_id', '=', 'parent.c_id')
                ->where('as_user_id', $user_id)
                ->groupBy('parent.c_id', 'parent.c_name')
                ->get()
                ->toArray();
        }
    } else {//get cities
        if (user("panel_type") == 39)//is admin
            $area_supports = \App\City::where("c_parent_id", $parent_id)->get();
        else {
            $area_supports = \Illuminate\Support\Facades\DB::table('area_support')
                ->select([
                    'c_id',
                    'c_name'
                ])
                ->where('as_collection_id', $collection_id)
                ->join('cities', 'as_city_id', '=', 'cities.c_id')
                ->where('as_user_id', user('user_id'))
                ->where('cities.c_parent_id', ($parent_id == null) ? -101 : $parent_id)
                ->groupBy('cities.c_id', 'cities.c_name')
                ->get()
                ->toArray();
        }
    }
    $area_supports = json_decode(json_encode($area_supports), true);
    return $area_supports;
}

function getFile($full_path, $item = null)
{
    if (isset($full_path)) {
        $full_path = str_replace("/", '_-', $full_path);
        return route('files', $full_path);
    } else {
        switch ($item) {
            case 'avatar';
                $row = 'avatar.jpg';
                break;
            case 'logo';
                $row = 'logo.jpg';
                break;
            case 'product';
                $row = 'product.jpg';
                break;
        }
        return asset('default-image/' . $row);
    }
}

function uploadFile($file, $type, $user_id = null,$crop=null)
{
    if ($user_id == null)
        $user_id = user('user_id');

    if (!in_array($type, [
        "collection",
        "product",
        "avatar",
        "factor",
        "logo",
        "collection_products",
        "ticket",
        "message",
        "report",
        "advertising",
    ])
    )
        return false;
    $path = "uploads/" . $user_id . "/" . $type . "/";

    if (!file_exists($path)) {
        mkdir(/*public_path()*/
            $path, 0755, true);
        $f = @fopen($path . '/.htaccess', 'w');
        fwrite($f, "Order allow,deny\nDeny from all");
        fclose($f);
    }
    do {
        $name = generateRandomString(20) . "." . $file->getClientOriginalExtension();
    } while (file_exists($name));

//    dd($name)
    @chdir('../../project/');

    if ($file->move($path, $name)) {
        $route = $path . $name;
        //water mark
        if ($type == "collection_products") {
            watermark($route,$crop);
        }
        $route = ltrim($route, 'uploads/');
        return $route;
    } else return false;
}

function watermark($route,$crop)
{
    $img = Image::make($route);
    if (!empty($crop)){
        $img->crop(round($crop['width']),round($crop['height']),round($crop['x']),round($crop['y']));
    }
    $img->resize(400, 370);
//    $img->insert("uploads/watermark/watermark.png", 'bottom-right', 20, 20); //insert watermark in (also from public_directory)
    $img->save($route); //save created image (will override old image)
    return true;
}

function getUrlByTypeCollection($collection)
{
    $url = "";
    $panel_type = DB::table("collections")->select('panel_type')
        ->leftJoin('collection_persons', 'cp_coll_id', '=', 'coll_id')
        ->leftJoin('persons', 'prsn_id', '=', 'cp_prsn_id')
        ->leftJoin('users', 'user_id', '=', 'prsn_user_id')
        ->where('coll_id', $collection->coll_id)
        ->first()->panel_type;
    switch ($panel_type) {
        case '41':
            $url = asset("/factory/");
            break;
        case '42':
            $url = asset("/company/");
            break;
        case '43':
            $url = asset("/wholesaler/");
            break;
    }
    return $url;
}

function getRole()
{
    user()->person->type;
}

function collection()
{
    if ((user()->person->collections->first()->coll_id))
        return 1;

}

function collection_id()
{
    return (user()->person->collections->first()->coll_id);
}

function getTypeCollection($collection)
{
    $url = "";
    $panel_type = DB::table("collections")->select('panel_type')
        ->leftJoin('collection_persons', 'cp_coll_id', '=', 'coll_id')
        ->leftJoin('persons', 'prsn_id', '=', 'cp_prsn_id')
        ->leftJoin('users', 'user_id', '=', 'prsn_user_id')
        ->where('coll_id', $collection->coll_id)
        ->first()->panel_type;
    switch ($panel_type) {
        case '41':
            $url = "/factory/";
            break;
        case '42':
            $url = "/company/";
            break;
        case '43':
            $url = "/wholesaler/";
            break;
    }
    return $url;
}

function getUserByCollectionId($collection_id = null)
{
    if ($collection_id == null)
        $collection_id = collection_id();
    $result = DB::table("users")
        ->select("user_id")
        ->join("persons", 'prsn_user_id', '=', 'user_id')
        ->join("collection_persons", 'cp_prsn_id', '=', 'prsn_id')
        ->where("cp_coll_id", $collection_id)
        ->orderBy('prsn_id', 'asc')
        ->limit(1)
        ->get()
        ->toArray();
    return $result[0]->user_id;
}

function getAllUserByCollectionId($collection_id = null)
{
    if ($collection_id == null)
        $collection_id = collection_id();

    $users = DB::table('users')->select('user_id')
        ->leftJoin('persons', 'prsn_user_id', '=', 'user_id')
        ->leftJoin('collection_persons', 'cp_prsn_id', '=', 'prsn_id')
        ->where('cp_coll_id', $collection_id)
        ->orderBy('user_id asc')
        ->get();
    $array = [];
    foreach ($users as $user) {
        array_push($array, $user->user_id);
    }
    return $array;
}

function collectionUsers()
{
    $collection_id = collection_id();
    $users = DB::table('users')
        ->select([
            'user_id',
            'username'
        ])
        ->join('persons', 'user_id', '=', 'prsn_user_id')
        ->join('collection_persons', 'cp_prsn_id', '=', 'prsn_id')
        ->join('collections', 'cp_coll_id', '=', 'coll_id')
        ->where('coll_id', $collection_id)->get()->toArray();
    $users = json_decode(json_encode($users), true);
    return $users;
}

function getFactorDetailsDependencies($id, $result)
{
    //agar mohasebat anjam nashode
//    if ($result[array_keys($result)[0]]['fm_calculated'] == 0) {
    //mohasebat ra anjam bede (discount va eshant haye khode factor)
    $result = call_sp('factor_calculates', null, [
        'fm_id' => $id,
        'timestamp' => time()
    ]);
//    echo "<pre>";
//    print_r($result);
//    exit();
//    }

    $result = getBasketItems($id);
//    $talab = talabAzGhabl($result[array_keys($result)[0]]['coll_id'], $result[array_keys($result)[0]]['fm_self_collection_id']);
//    if ($talab > $result[array_keys($result)[0]]['fm_amount'])
//        $talab = $result[array_keys($result)[0]]['fm_amount'];
    $talab = 0;
    $return_reasons = returnReasons($result[array_keys($result)[0]]['coll_id']);

    $person_collection = DB::table('persons')
        ->leftJoin('collection_persons', 'cp_prsn_id', '=', 'prsn_id')
        ->where('cp_coll_id', $result[array_keys($result)[0]]['coll_id'])
        ->first();

    $product_return_permit = setting('product_return_permit', $result[array_keys($result)[0]]['coll_id']);

//    $result = convertToHierarchy($result, 'cp_id', 'fd_ooe_id');
    return [
        'talab' => $talab,
        'return_reasons' => $return_reasons,
        'person_collection' => $person_collection,
        'product_return_permit' => $product_return_permit,
        'result' => $result,
    ];
}

function talabAzGhabl($source_collection_id, $destination_collection_id)
{
    $result = DB::table('collection_debt')
        ->select('cd_amount')
        ->where('cd_source_collection_id', $source_collection_id)
        ->where('cd_destination_collection_id', $destination_collection_id)
        ->first();
    if ($result)
        return $result->cd_amount;
    return 0;
}

function returnReasons($collection_id)
{
    $return_reasons = DB::table('collection_return_reasons')
        ->select([
            "crr_id",
            "bas_value"
        ])
        ->leftJoin('baseinfos', 'bas_id', '=', 'crr_bas_id')
        ->where('bas_type', 'return_reason')
        ->where('crr_collection_id', $collection_id)
        ->where('bas_parent_id', '!=', 0)
        ->where('crr_status', 1)
        ->get()
        ->toArray();
    return $return_reasons;
}

//working with basket
function addToBasket($item, $count = 1, $add_type = 1, $mode = 2, $type = 0)
{
    $user_id = getUserIdForAddFactor();
    $collection_id = getCollectionForAddFactor($user_id);
    $result = call_sp('add_to_basket', null, [
        'add_type' => $add_type,
        'type' => $type,
        'mode' => $mode,
        'user_id' => $user_id,
        'prod_id' => $item,
        'count' => $count,
        'self_collection_id' => $collection_id,
        'timestamp' => time()
    ]);
    if ($result[0]['result'] == 0) {
        return true;
    }
    return false;
}

function removeFromBasket($item)
{
//    session()->remove('basket.' . $item);
    call_sp('delete_from_basket', null, [
        'user_id' => getUserIdForAddFactor(),
        'prod_id' => $item
    ]);
    return true;
}

function getBasketItems($id = null, $mode = 0)
{
    $user_id = getUserIdForAddFactor();
    $result = call_sp('get_basket_items', null, [
        'user_id' => $user_id,
        'id' => $id,
        'mode' => $mode
    ]);
    if (!empty($result))
        return $result;
    return false;
}

function deleteFactorFromBasket($id)
{
    $result = call_sp('delete_factor_from_basket', null, [
        'user_id' => getUserIdForAddFactor(),
        'id' => $id
    ]);
    if ($result[0]['result'] == 0)
        return true;
    return false;
}

function basketCount()
{
    $user_id = getUserIdForAddFactor();

    $sql = "select count(1) as cnt from factor_master
            where fm_creator_id = :user
            and fm_mode = 0";
    $result = DB::select($sql, [":user" => $user_id]);
    if (isset($result[0]->cnt))
        return $result[0]->cnt;
    return 0;
}

function generateCode()
{
    $code = rand(1, 500000);
    $check = \App\User::whereCode($code)->first();
    return empty($code) ? $code : generateCode();

}

//working with announcements
function announcementCount()
{
    $result = DB::table('announcements')
        ->select([
            "count(1)"
        ])
        ->where('ann_type', 0)
        ->leftJoin('announcement_receivers', 'anr_announcement_id', 'ann_id')
        ->where('anr_type_id', user('panel_type'))
        ->where('anr_city_id', user()->person->prsn_city_id)
        ->whereRaw("(select top 1 count(1) from announcement_viewed where av_ann_id = ann_id and av_user_id = " . user('user_id') . ") = 0")
        ->count();

    return $result;
}

function getAnnouncementItems()
{
    $user_id = user('user_id');
    $result = DB::table('announcements')
        ->select([
            "coll_name",
            'ann_title',
            'ann_id',
            'created_at',
            DB::raw("(select top 1 ur_path from upload_route where ur_table_name = 'collections' and ur_collection_id = coll_id order by ur_id desc) as logo")
        ])
        ->where('ann_type', 0)
        ->leftJoin('announcement_receivers', 'anr_announcement_id', 'ann_id')
        ->where('anr_type_id', user('panel_type'))
        ->where('anr_city_id', user()->person->prsn_city_id)
        ->whereRaw("(select count(1) from announcement_viewed where av_ann_id = ann_id and av_user_id=$user_id) = 0")
        ->leftJoin('collections', 'ann_collection_id', 'coll_id')
        ->latest("ann_id")
        ->limit(5)
        ->get();
    return $result;
}

//working with visitor purchase
function getPurchasingUserId()
{
    if (user('panel_type') == 45 && session()->has('purchasing_user_id')) {
        if (session()->get('purchasing_user_id') == user("otp")) {
            if ((int)user('temp') < time()) {
                session()->flash('error', 'مدت زمان خرید، به پایان رسید! فاکتور های باز بعد از 12 ساعت حذف خواهد شد!');
                exitFromPurchasingMode();
            }
            $user = DB::table("users")
                ->select([
                    DB::raw("concat(prsn_name,' ',prsn_family) as name"),
                    "code"
                ])
                ->join("persons", 'prsn_user_id', '=', 'user_id')
                ->where('user_id', user("otp"))->first();
            $username = $user->name . ' با کد کاربری ' . $user->code;
            $link = "<a class='' href='' data-action='" . asset('/exit-purchasing-for-subscription') . "' data-method='post' data-confirm='اطمینان از خروج دارید؟'>خروج</a>";
            return "<b class='text-danger' style='font-size: 14px'>" . "شما در حال خرید برای " . $username . " می باشید " . $link . "</b>";
        }
    }
    return '';
}

function getUserIdForAddFactor()
{
    $user_id = user('user_id');
    if (user('panel_type') == 45)//is visitor
    {
        if (session()->has('purchasing_user_id') && session()->get('purchasing_user_id') == user("otp")) {
            $user_id = user("otp");
        } else
            return false;
    }
    return $user_id;
}

function getCollectionForAddFactor($user_id)
{
    $collection_id = collection_id();
    if (user('panel_type') == 45)//is visitor
    {
        if (session()->has('purchasing_user_id') && session()->get('purchasing_user_id') == user("otp")) {
            $collection_id = DB::table('persons')
                ->select(['cp_coll_id'])
                ->join('collection_persons', 'cp_prsn_id', '=', 'prsn_id')
                ->where('prsn_user_id', $user_id)->first()->cp_coll_id;
        } else
            return false;
    }
    return $collection_id;
}

function exitFromPurchasingMode()
{
    \Illuminate\Support\Facades\Artisan::call('cache:clear');
    session()->forget('purchasing_user_id');
    Session::save();
    DB::table('users')->where('user_id', user('user_id'))->update([
        'otp' => null
    ]);
    header("location: " . asset('purchase-for-subscription'));
    exit();
}

function messages()
{
    $result = DB::table('messages')
        ->select([
            "count(1)"
        ])
        ->where('msg_receiver_collection_id', collection_id())
        ->where('msg_is_read', 0)
        ->where('msg_type', 0)
        ->count();
    return $result;
}

function messagesItems()
{
    $result = DB::table('messages')
        ->where('msg_receiver_collection_id', collection_id())
        ->where('msg_is_read', 0)
        ->where('msg_type', 0)
        ->leftJoin('persons', 'prsn_user_id', 'msg_sender_user_id')
        ->leftJoin('collection_persons', 'cp_prsn_id', 'prsn_id')
        ->leftJoin('collections', 'coll_id', 'cp_coll_id')
        ->select([
            'msg_title',
            'coll_name',
            'msg_created_at',
        ])
        ->limit(5)
        ->get();
    return $result;
}

function amount()
{
    $result = DB::table('users')
        ->where('user_id', user('user_id'))
        ->select([
            'amount',
        ])
        ->first();
    return $result;
}

function getAvatar()
{
    if ($image = DB::table('upload_route')
        ->where('ur_collection_id', collection_id())
        ->where('ur_table_name', 'avatar')
        ->orderBy('ur_id', 'desc')
        ->first()
    )
        return $image->ur_path;
    else
        return $image = null;
}

function announcementSystem()
{
    $paymant_method = DB::table('collection_payment_methods')
        ->where('cpm_collection_id', collection_id())
        ->where('cpm_status', 1)
        ->count();
    $factor_receiver = DB::table('setting')
        ->where('st_key', 'factor_receiver')
        ->where('st_collection_id', collection_id())
        ->count();

    $array = ["payment" => $paymant_method, "factor" => $factor_receiver];

    return $array;
}

function newUserCount()
{
    $result = DB::table('users')
        ->select([
            "count(1)"
        ])
        ->whereIN('panel_type', [44, 45])
        ->where('profile_complete', 0)
        ->count();
    return $result;
}

function getNewUserItems()
{
    $result = DB::table('users')
        ->select([
            'username',
            'created_at',
            'prsn_name',
            'prsn_family',
        ])
        ->whereIn('panel_type', ['44', '45'])
        ->where('profile_complete', 0)
        ->leftJoin('persons', 'prsn_user_id', 'user_id')
        ->orderBy('user_id', 'desc')
        ->get();

    return $result;
}

function getAdvertising($station)
{
    $result = DB::table('advertising_master')
        ->select([
            'ur_path',
            'adm_station_id',
            'adm_link',
        ])
        ->where('adm_expire_at', '>=', time())
        ->where('adm_start_at', '<=', time())
        ->where('adm_station_id', $station)
        ->leftJoin('advertising_detail', 'adm_id', 'add_master_id')
        ->where('add_panel_type', user('panel_type'))
        ->where('add_city_id', user()->person->prsn_city_id)
        ->whereIn('add_senf_id', findUser(user('user_id'), 'type_class'))
        ->leftJoin('upload_route', 'ur_fk_id', 'adm_id')
        ->where('ur_table_name', 'advertising')
        ->orderBy("adm_expire_at", "asc")
        ->get();
    return $result;
}

function getNewsItems()
{
    $result = DB::table('announcements')
        ->select([
            "ann_title",
            "ann_body",
            "created_at",
        ])
        ->where('ann_type', 1)
        ->leftJoin('announcement_receivers', 'anr_announcement_id', 'ann_id')
        ->where('anr_type_id', user('panel_type'))
        ->where('anr_city_id', user()->person->prsn_city_id)
        ->whereRaw("(select count(1) from announcement_viewed where av_ann_id = ann_id) = 0")
        ->leftJoin('collections', 'ann_collection_id', 'coll_id')
        ->latest("ann_id")
        ->limit(3)
        ->get();
    return $result;
}

function countNewFactor()
{
    try {
        $result = call_sp('get_new_factors_count', null, [
            'user_id' => user('user_id'),
            'collection_id' => collection_id()
        ]);
        return $result[0]['cnt'];
    } catch (Exception $e) {
        return 0;
    }
}

function getItemNewFactor()
{
    $result = DB::table('factor_master')
        ->select([
            'fm_no',
            'fm_date',
        ])->where('fm_collection_id', collection_id())
        ->where('fm_status', 0)
        ->leftJoin('factor_routing', 'far_factor_id', 'fm_id')
        ->leftJoin('user_roles', 'far_role_id', 'ur_rol_id')
        ->where('ur_user_id', user('user_id'))
        ->limit(5)
        ->get();
    return $result;
}

function editCompanyAreaSupport($type = 2, $collection_id = null, $user_id = null, $parent_id = null)
{
    if ($collection_id == null)
        $collection_id = collection_id();
    if ($user_id == null)
        $user_id = user('user_id');
    $person = \App\Person::where('prsn_user_id', $user_id)->first();
    if ($type == 1) {//get states
        $area_supports = \App\City::where("c_parent_id", 0)->get();
    } else {//get cities
        $area_supports = \App\City::where("c_parent_id", $parent_id)->get();
    }
    $area_supports = json_decode(json_encode($area_supports), true);
    return $area_supports;
}

function getNotesDashboard()
{
    $result = DB::table('setting')
        ->select([
            'st_value',
        ])
        ->where('st_key', 'notes_dashboard')
        ->where('st_collection_id', collection_id())
        ->where('st_user_id', user('user_id'))
        ->first();
    return $result;
}

function countUsers()
{
    $result = DB::table('users')
        ->select([
            'count(1)'
        ])->whereIn('panel_type', [44, 45])
        ->count();
    return $result;
}

function countCompany()
{
    $result = DB::table('users')
        ->select([
            'count(1)'
        ])->whereIn('panel_type', [42, 43])
        ->count();
    return $result;
}

function countFactor()
{
    $result = DB::table('factor_master')
        ->select([
            'count(1)'
        ])->count();
    return $result;
}

function countNewTicket()
{
    $result = call_sp('get_new_ticket_count', null, [
        'user_id' => user('user_id'),
    ]);
    return $result[0]['cnt'];
}

function countNewCheckoutRequest()
{
    $result = DB::table("checkout_request")->where('rc_is_viewed', 0)->count("rc_id");
    return $result;
}

function getDefaultFactorReceiver()
{
    $factor = DB::table('setting')
        ->select([
            'rol_id',
        ])
        ->where('st_collection_id', 0)
        ->where('st_user_id', 0)
        ->where('st_key', 'factor_receiver')
        ->leftJoin('roles', 'rol_id', 'st_value')
        ->first()->rol_id;
    return $factor;
}

function getDefaultRoleFactorReceiver($user_id)
{
    $check = DB::table('user_roles')
        ->where('ur_user_id', $user_id)
        ->where('ur_rol_id', getDefaultFactorReceiver())
        ->first();
    if ($check)
        return false;
    return true;
}

function permitted_amout()
{
    return DB::table('setting')
        ->where('st_key', 'checkout_permitted_amout')
        ->first()->st_value;
}

function check_set_sheba()
{
    $result = DB::table('persons')
        ->select([
            'prsn_sheba'
        ])
        ->where('prsn_user_id', user('user_id'))
        ->first();
    return $result;
}

function getCategoriesToSenf($collection_id = null)
{
    $categories = DB::table("collection_classes")->select([
        "cat_id as id",
        "cat_parent_id as parent_id",
        "cat_name as name"
    ])
        ->where('coc_coll_id', $collection_id)
        ->leftJoin('categories', 'coc_typeClass_id', 'cat_senf_id')
        ->where('cat_status', 1)
        ->orderBy("cat_parent_id", "asc")->get()->toArray();
    return $categories = convertToHierarchy(json_decode(json_encode($categories), true));
}


function dimensionsImageCrop($data)
{
    $array = [
        $data[0]=>$data[1],
        $data[2]=>$data[3],
        $data[4]=>$data[5],
        $data[6]=>$data[7],
    ];
    return $array;
}

?>
