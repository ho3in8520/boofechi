<?php

use Tzsk\Sms\Facade\Sms;

//function number_format($number){
//    return (int)($number);
//}

function generateMessage($type, array $params)
{
    $str = setting($type);
    $str = str_replace(array_keys($params), array_values($params), $str);
    return $str;
}

function sendEmail($message, array $emails)
{
    //send email
    foreach ($emails as $email) {
        $subject = 'پیام جدید از boofechi.com';
        $headers = 'From: info@boofechi.com' . "\r\n" .
            'Reply-To: info@boofechi.com' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
        mail($email, $subject, $message, $headers);
    }
}

function sendSMS($message,$mobiles)
{
    Sms::send($message, function ($sms) use ($mobiles) {
        $sms->to($mobiles);
    });
}

function sendMessage($message, array $users)
{
    try {
        //if sms is active
        if (setting('send_sms') == 1) {
            $mobiles = DB::table("persons")->select("prsn_mobile1")->whereIn("prsn_user_id", $users)->groupBy('prsn_mobile1')->get();
            $mobiles = array_column(json_decode(json_encode($mobiles, true)), 'prsn_mobile1');
            sendSMS($message,$mobiles);
        }

        //if email is active
        if (setting('send_email') == 1) {
            $emails = DB::table("users")->select("email")->whereIn("user_id", $users)->groupBy('email')->get();
            $emails = array_column(json_decode(json_encode($emails), true), 'email');
            sendEmail($message,$emails);
        }

        return true;
    } catch (Exception $e) {
//        echo "<pre>";
//        print_r($e->getMessage());
//        exit();
        return false;
    }
}

function numtoword($input)
{
    $obj = new App\Component\NumToWords();
    return $obj->numberToWords($input);
}

function calcPercent($number, $percent)
{
    return withoutZeros((($percent) * $number) / 100);
}

function formValue($name, $old, $field = null)
{
//    if($name == 'role') {
//        echo "<pre>";
//        print_r($old->{$field});
//        exit();
//    }
    return old($name, isset($old) ? (is_null($field) ? $old->{$name} : $old->{$field}) : '');
}

function SeederHierarchySaveMenu($permission, $parent_id = 0)
{
    foreach ($permission as $perm) {
        $model = new \App\Permission();
        $model->perm_name = $perm['perm_name'];
        $model->perm_label = $perm['perm_label'];
        $model->perm_status = $perm['perm_status'];
        $model->perm_parent_id = $parent_id;
        $model->perm_fm_id = $perm['perm_fm_id'];
        $model->perm_link = $perm['perm_link'];
        $model->perm_icon = $perm['perm_icon'];
        $model->save();
        if (isset($perm['childs'])) {
            SeederHierarchySaveMenu($perm['childs'], $model->perm_id);
        }
    }
}

function showAllPermissions($perms, $rolPermissions, $level = 0)
{
    $level++;
    $array = [
        1 => '#E91E63',
        2 => '#00BCD4',
        3 => '#CDDC39',
        4 => '#9C27B0',
        5 => '#FFC107',
        6 => '#607D8B',
        7 => '#666EE8',
    ];
    foreach ($perms as $prm) {
        if (isset($prm['children'])) {
            ?>
            <div id="headingCollapse<?= $prm['perm_id'] ?>" class="card-header"
                 style="border: 1px solid <?= $array[$level] ?>;background-color: <?= $array[$level] ?>;margin-top:15px">
                <a data-toggle="collapse" style="color: #FFFFFF !important;" href="#collapse<?= $prm['perm_id'] ?>"
                   aria-expanded="false" aria-controls="collapse21"
                   class="card-title lead collapsed"><?= $prm['perm_label'] ?>
                </a>
            </div>
            <div id="collapse<?= $prm['perm_id'] ?>" role="tabpanel" aria-labelledby="headingCollapse21"
                 class="collapse"
                 style="border-style: solid;border-width: 0 1px 1px 1px;border-color: #cfcfcf;padding: 15px">
                <input data-size="xs" data-color="<?= $array[$level] ?>" type="checkbox" class="switchery"
                       name="permissions[]"
                    <?php
                    if (!empty(old("permissions"))) {
                        if (is_array(old("permissions")) && in_array($prm['perm_id'], old("permissions")))
                            echo "checked";
                    } else {
                        if (!empty($rolPermissions) && is_array($rolPermissions) && in_array($prm['perm_id'], $rolPermissions))
                            echo "checked";
                    }
                    ?>
                       value="<?= $prm['perm_id'] ?>" style="margin-left: 10px">
                <?php
                echo $prm['perm_label'];
                if (isset($prm['children'])) {
                    showAllPermissions($prm['children'], $rolPermissions, $level);
                }
                ?>
            </div>
            <?php
        } else {
            ?>
            <input data-size="xs" data-color="<?= $array[$level] ?>" type="checkbox" class="switchery"
                   name="permissions[]"
                <?php
                if (!empty(old("permissions"))) {
                    if (is_array(old("permissions")) && in_array($prm['perm_id'], old("permissions")))
                        echo "checked";
                } else {
                    if (!empty($rolPermissions) && is_array($rolPermissions) && in_array($prm['perm_id'], $rolPermissions))
                        echo "checked";
                }
                ?>
                   value="<?= $prm['perm_id'] ?>" style="margin-left: 10px">
            <?php
            echo $prm['perm_label'];
            ?>
            <?php
        }
    }
}

function convertToHierarchy($results, $id_field = 'id', $parent_id_field = 'parent_id', $childrenField = 'children')
{
    $hierarchy = array();
    $itemReferences = array();
    foreach ($results as $item) {
        $id = $item[$id_field];
        $parentId = $item[$parent_id_field];
        if (isset($itemReferences[$parentId])) {
            $itemReferences[$parentId][$childrenField][$id] = $item;
            $itemReferences[$id] =& $itemReferences[$parentId][$childrenField][$id];
        } elseif (!$parentId || !isset($hierarchy[$parentId])) {
            $hierarchy[$id] = $item;
            $itemReferences[$id] =& $hierarchy[$id];
        }
    }
    unset($results, $item, $id, $parentId);
    foreach ($hierarchy as $id => &$item) {
        $parentId = $item[$parent_id_field];
        if (isset($itemReferences[$parentId])) {
            $itemReferences[$parentId][$childrenField][$id] = $item;
            unset($hierarchy[$id]);
        }
    }
    unset($itemReferences, $id, $item, $parentId);
    return $hierarchy;
}

function createCombobox($array, $level = 0, $selected = null, $entekhab_konid = false)
{
    if ($level == 0 && $entekhab_konid) {
        ?>
        <option value="0">انتخاب کنید...</option>
        <?php
    }
    $dash = "";
    foreach ($array as $row) {
        $dash = str_repeat('__', $level);
        if (!is_array($selected)) {
            ?>
            <option
                value='<?= $row['id'] ?>' <?= ($row['id'] == $selected) ? "selected" : "" ?>
                <?= ((isset($row['parent_id'])) ? "data-parent='{$row['parent_id']}'" : '') ?>><?= $dash . " " . $row['name'] ?></option>
            <?php
        } else {
            ?>
            <option
                value='<?= $row['id'] ?>' <?= (in_array($row['id'], $selected) ? "selected" : "") ?>
                <?= ((isset($row['parent_id'])) ? "data-parent='{$row['parent_id']}'" : '') ?>><?= $dash . " " . $row['name'] ?></option>
            <?php
        }
        if (isset($row['children'])) {
            createCombobox($row['children'], $level + 1, $selected);
        }
        echo "</optgroup>";
    }
}

/*function createComboboxMultipleSelect($array, $level = 0, $selected = [])
{
    $dash = "";
    foreach ($array as $row) {
        $dash = str_repeat('__', $level);
        ?>
        <option value='<?= $row['id'] ?>' <?= (in_array($row['id'], $selected) ? "selected" : "") ?>><?= $dash . " " . $row['name'] ?></option>
        <?php
        if (isset($row['children'])) {
            createCombobox($row['children'], $level + 1, $selected);
        }
        echo "</optgroup>";
    }
}*/

function customForeach($items, $key, $value, $default = null, $show_select = true)
{
    $str = "";
    if ($show_select)
        $str = "<option value>انتخاب کنید...</option>";
    foreach ($items as $item) {
        if (is_array($default)) {
            $str .= "<option value='{$item["$key"]}' " . ((in_array($item["$key"], $default)) ? 'selected' : '') . ">{$item["$value"]}</option>";
        } else {
            $str .= "<option value='{$item["$key"]}' " . (($item["$key"] == $default) ? 'selected' : '') . ">{$item["$value"]}</option>";
        }
    }
    return $str;
}

function getSummary($data)
{
    if (count($data) > 0) {
        return "نمایش " . "<b>" . ($first = (($data->currentPage() - 1) * $data->perPage() + 1)) . "</b>  تا " . "<b>" . ($first + count($data) - 1) . "</b>" . " مورد از کل " . "<b>" . $data->total() . "</b>" . " مورد.";
    } else
        return "نمایش <b>0</b> مورد.";
}

function getPermissions($user_id = null)
{
    if ($user_id == null)
        $user_id = user('user_id');
    $roles = \Illuminate\Support\Facades\DB::table("user_roles")->whereUrUserId($user_id)->get()->toArray();
    $permissions = [];
    foreach ($roles as $role) {
        $permission = \Illuminate\Support\Facades\DB::table('role_permissions')->select("perm_label")
            ->leftJoin('permissions', 'rp_perm_id', '=', 'perm_id')->where("rp_rol_id", $role->ur_rol_id)
            ->get()->toArray();

        foreach ($permission as $perm) {
            array_push($permissions, $perm->perm_label);
        }
    }
    return array_unique($permissions);
}

function getPermissionIds($user_id = null)
{
    if ($user_id == null)
        $user_id = user('user_id');
    $roles = \Illuminate\Support\Facades\DB::table("user_roles")->whereUrUserId($user_id)->get()->toArray();
    $permissions = [];
    foreach ($roles as $role) {
        $permission = \Illuminate\Support\Facades\DB::table('role_permissions')
            ->select("rp_perm_id")
            ->where('rp_rol_id', $role->ur_rol_id)
            ->get()
            ->toArray();

        foreach ($permission as $perm) {
            array_push($permissions, $perm->rp_perm_id);
        }
    }
    return array_unique($permissions);
}

function getMenuItems($menus)
{
    $json = [];
    foreach ($menus as $menu) {
        $json[] = [
            "name" => $menu['name'],
            "href" => $menu['href'],
            "title" => $menu['title'],
            "text" => $menu['text'],
            "icon" => $menu['icon'],
            "target" => $menu['target'],
            "children" => (isset($menu['children']) ? getMenuItems($menu['children']) : "")
        ];
    }
    return $json;
}

function getMenuLinks($menus)
{
    foreach ($menus as $menu) {
        if (isset($menu['name'])) {
            if (can($menu['name'])) {
                ?>
                <li class="<?= (isset($menu['children']) && !empty($menu['children'])) ? "has-sub" : "" ?> nav-item">
                    <a href="<?= (isset($menu['children']) && !empty($menu['children'])) ? "#" : URL::to('/') . $menu['href'] ?>"
                       title="<?= $menu['text'] ?>">
                        <i class="<?= $menu['icon'] ?>">
                        </i>
                        <span data-i18n="" class="menu-title"><?= $menu['text'] ?></span>
                    </a>
                    <?php
                    if (isset($menu['children']) && !empty($menu['children'])) {
                        ?>
                        <ul class="menu-content">
                            <?php
                            getMenuLinks($menu['children']);
                            ?>
                        </ul>
                        <?php
                    }
                    ?>
                </li>
                <?php
            }
        } else {
            ?>
            <li class="<?= (isset($menu['children']) && !empty($menu['children'])) ? "has-sub" : "" ?> nav-item">
                <a href="<?= (isset($menu['children']) && !empty($menu['children'])) ? "#" : URL::to('/') . $menu['href'] ?>"
                   title="<?= $menu['text'] ?>">
                    <i class="<?= $menu['icon'] ?>">
                    </i>
                    <span data-i18n="" class="menu-title"><?= $menu['text'] ?></span>
                </a>
                <?php
                if (isset($menu['children']) && !empty($menu['children'])) {
                    ?>
                    <ul class="menu-content">
                        <?php
                        getMenuLinks($menu['children']);
                        ?>
                    </ul>
                    <?php
                }
                ?>
            </li>
            <?php
        }
    }
}

function createHtmlMenu($menus)
{
    $path = "../menu-config" . "/" . "menu.json";
    if (!file_exists($path)) {
        $json = getMenuItems($menus);
        return json_encode($json, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
    } else
        return file_get_contents($path);
}

function deleteEmpties($array)
{
    return array_filter($array);
}

function generateRandomString($length = 10)
{
    $chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charLength = strlen($chars);
    $str = null;
    for ($i = 0; $i < $length; $i++) {
        $str .= $chars[rand(0, $charLength - 1)];
    }
    return $str;
}

function generateRandomNumber($length = 10)
{
    $chars = '0123456789';
    $charLength = strlen($chars);
    $str = null;
    for ($i = 0; $i < $length; $i++) {
        $str .= $chars[rand(0, $charLength - 1)];
    }
    return $str;
}

function index($data, $loop)
{
    return ($data->currentPage() - 1) * $data->perPage() + 1 + $loop->index;
}

function can($perm)
{
    return auth()->user()->can($perm);
}

function user($item = "")
{
    if (Auth::check()) {
        if ($item == "")
            return auth()->user();
        else
            return auth()->user()->{$item};
    } else return false;
}

function call_sp($sp_name, $param_count, $params = [])
{
    $sql = "SET NOCOUNT ON;exec {$sp_name} ";

    $i = 0;
    $sp_parameters = [];
    foreach ($params as $key => $value) {
        $sql .= "@{$key} = ?, ";
        array_push($sp_parameters, $value);
        $i++;
    }
    if ($i > 0)
        $sql = rtrim($sql, ', ');
    $result = \Illuminate\Support\Facades\DB::select($sql, $sp_parameters);
//    DB::enableQueryLog(); // Enable query log
//
//// Your Eloquent query
//
//    dd(DB::getQueryLog()); // Show results of log
    return json_decode(json_encode($result), true);
}

function tableHeader(array $arr)
{
    $str = "<thead>";
    foreach ($arr as $th) {
        $str .= "<th>$th</th>";
    }
    $str .= "</thead>";
    return $str;
}

function tableBody(array $arr)
{
    $str = "<tbody>";
    foreach ($arr as $row) {
        $str .= "<tr>";
        foreach ($row as $column) {
            $str .= "<td>$column</td>";
        }
        $str .= "</tr>";
    }
    $str .= "</tbody>";
    return $str;
}

function tableFooter(array $arr)
{
    $str = "<tfoot>";
    foreach ($arr as $tf) {
        $str .= "<td>$tf</td>";
    }
    $str .= "</tfoot>";
    return $str;
}

function getPrevKeyInArray($array = array(), $key)
{
    $keys = array_keys($array);
    $found_index = array_search($key, $keys);
    if ($found_index === false || $found_index === 0)
        return false;
    return $keys[$found_index - 1];
}

function showData($view, array $array = [])
{
    if (request()->ajax()) {
        $temp = ["status" => "100", "FormatHtml" => $view->renderSections()['main_content']];
        $temp = array_merge($temp, $array);
        return json_encode($temp);
    } else
        return $view;
}

function getTypesForAnnouncements()
{
    $types = \App\Baseinfo::where("bas_type", "panel_type")
        ->where("bas_parent_id", "!=", 0);
    if (user('panel_type') == 42)
        $types = $types->whereIn("bas_id", [43, 44, 45]);
    elseif (user('panel_type') == 43)
        $types = $types->whereIn("bas_id", [44, 45]);
    elseif (user('panel_type') == 40) {
        $creator = \Illuminate\Support\Facades\DB::table('users')
            ->select(['u.user_id'])
            ->where('user_id', user('user_id'))
            ->leftJoin('users as u', 'u.user_id', 'creator')->first();
        if ($creator->user_id == 42)
            $types = $types->whereIn("bas_id", [43, 44, 45]);
        if ($creator->user_id == 43)
            $types = $types->whereIn("bas_id", [44, 45]);
        if ($creator->user_id == 41)
            $types = $types->whereIn("bas_id", [42, 43, 44, 45]);
        if ($creator->user_id == 46)
            $types = $types->whereIn("bas_id", [41, 42, 43, 44, 45]);
    } elseif (user('panel_type') == 41)
        $types = $types->whereIn("bas_id", [42, 43, 44, 45]);
    elseif (user('panel_type') == 46)
        $types = $types->whereIn("bas_id", [41, 42, 43, 44, 45]);
    elseif (user('panel_type') == 39)
        $types = $types->whereNotIn("bas_id", [39]);

    $types = $types->get()->toArray();
    return $types;
}

function jalali_to_timestamp($date, $spliter = '/', $first = true)
{
    if ($first == true) {
        $h = "00";
        $i = "00";
        $s = "00";
    } else {
        $h = "23";
        $i = "59";
        $s = "59";
    }

    list($y, $m, $d) = explode($spliter, $date);
    return (new \Morilog\Jalali\Jalalian($y, $m, $d, $h, $i, $s))->toArray()['timestamp'];
}

function jdate_from_gregorian($input, $format = '%A, %d %B %Y | H:i:s')
{
    return \Morilog\Jalali\Jalalian::fromDateTime($input)->format($format);
}

function createRootData($rows)
{
    $values = "<Root>";
    foreach ($rows as $items) {
        $values .= "<Data ";
        foreach ($items as $key => $value) {
            $values .= "{$key}=\"{$value}\" ";
        }
        $values .= "/>";
    }
    $values = rtrim($values, " ") . "</Root>";
    return $values;
}

function checkRelation($table, $id)
{
    $result = call_sp("check_relation", 2, [
        'table_name' => $table,
        'id' => $id
    ]);
    return $result;
}

function setting($item, $collection_id = null, $user_id = null)
{
    //if logged in
    if (\Auth::check()) {
        $collection_id = ($collection_id == null) ? collection_id() : $collection_id;
        $user_id = ($user_id == null) ? user('user_id') : $user_id;
    }
    $sql = "
        select dbo.get_setting(?,?,?) as st_value
    ";
    $result = \Illuminate\Support\Facades\DB::select($sql, [$item, $collection_id, $user_id]);
    if (empty($result))
        exit('setting ' . $item . ' not exists!');
    else
        return $result[0]->st_value;
}

function slg($text)
{
    return str_replace(' ', '-', trim($text));
}

function deSlg($text)
{
    return trim(str_replace('-', ' ', $text));
}

function withoutZeros($number)
{
    return $number + 0;
}

?>
