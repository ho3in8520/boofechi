<?php

namespace App\Component;

use App\Collection;
use App\Person;
use Illuminate\Support\Facades\DB;

class Tools
{
    public function session_message($type, $key)
    {
        if (session()->has($type)) {
            $message = session()->get($type);
            switch ($type) {
                case 'success':
                    return "<div class='alert alert-success text-white'><strong></strong>{$message}</div>";
                case 'error':
                    return "<div class='alert alert-danger text-white'><strong></strong>{$message}</div>";
                case 'warning':
                    return "<div class='alert alert-warning text-white'><strong></strong>{$message}</div>";
                case 'info':
                    return "<div class='alert alert-info text-white'><strong></strong>{$message}</div>";
            }
        }

    }

    public static function nationalCodeCheck(&$input)
    {
        $input = self::convertToEnglishNumbers($input);
        if (!preg_match("/^\d{10}$/", $input)) {
            return false;
        }

        $check = (int)$input[9];
        $sum = array_sum(array_map(function ($x) use ($input) {
                return ((int)$input[$x]) * (10 - $x);
            }, range(0, 8))) % 11;

        return ($sum < 2 && $check == $sum) || ($sum >= 2 && $check + $sum == 11);
    }

    public static function mobileCheck(&$mobile)
    {
        $mobile = self::convertToEnglishNumbers(self::normalMobile($mobile));
        if ((bool)preg_match('/^(((98)|(\+98)|(0098)|0)(9){1}[0-9]{9})+$/', $mobile) || (bool)preg_match('/^(9){1}[0-9]{9}+$/', $mobile)) {
            $pattern = '/^[0-9]{11}+$/';
            return preg_split($pattern, $mobile);
        } else
            return false;
    }

    public static function convertToEnglishNumbers($string)
    {
        $persian = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
        $arabic = ['٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩'];
        $num = range(0, 9);
        $convertedPersianNums = str_replace($persian, $num, $string);
        $englishNumbersOnly = str_replace($arabic, $num, $convertedPersianNums);
        return $englishNumbersOnly;
    }

    public static function normalMobile(&$uNumber, $prefix = '')
    {
        $uNumber = trim($uNumber);
        $ret = &$uNumber;
        if (substr($uNumber, 0, 3) == '%2B') {
            $ret = substr($uNumber, 3);
            $uNumber = $ret;
        }
        if (substr($uNumber, 0, 3) == '%2b') {
            $ret = substr($uNumber, 3);
            $uNumber = $ret;
        }
        if (substr($uNumber, 0, 4) == '0098') {
            $ret = substr($uNumber, 4);
            $uNumber = $ret;
        }
        if (substr($uNumber, 0, 3) == '098') {
            $ret = substr($uNumber, 3);
            $uNumber = $ret;
        }
        if (substr($uNumber, 0, 3) == '+98') {
            $ret = substr($uNumber, 3);
            $uNumber = $ret;
        }
        if (substr($uNumber, 0, 2) == '98') {
            $ret = substr($uNumber, 2);
            $uNumber = $ret;
        }
        if (substr($uNumber, 0, 1) == '0') {
            $ret = substr($uNumber, 1);
            $uNumber = $ret;
        }
        $ret = $prefix . $ret;
        return $ret;
    }

    public static function collection_store($request, $panel_type)
    {
        DB::beginTransaction();
        try {
            $person = Person::create(array_merge($request->persons, [
                'prsn_state_id' => ($request->has('collections.coll_state_id') && $request->collections['coll_state_id'] != "") ? $request->collections['coll_state_id'] : 100000,
                'prsn_city_id' => ($request->has('collections.coll_city_id') && $request->collections['coll_city_id']) ? $request->collections['coll_city_id'] : 100000
            ]));
            $collection = Collection::create(array_merge($request->collections, [
                'coll_state_id' => ($request->has('collections.coll_state_id') && $request->collections['coll_state_id'] != "") ? $request->collections['coll_state_id'] : 100000,
                'coll_city_id' => ($request->has('collections.coll_city_id') && $request->collections['coll_city_id']) ? $request->collections['coll_city_id'] : 100000
            ]));
            $person->collections()->sync($collection->coll_id);

            $agent = 0;
            if ($request->user['reagent_id'] != '') {
                $agent = DB::table('users')
                    ->select([
                        'user_id'
                    ])
                    ->leftJoin('persons', 'prsn_user_id', '=', 'user_id')
                    ->where('prsn_national_code', $request->user['reagent_id'])
                    ->first();
                if ($agent)
                    $agent = $agent->user_id;
                else
                    $agent = null;
            }
            $user = \App\User::create([
                'username' => "coll_" . $collection->coll_id,
                //'code' => "prsn_" . $person->prsn_id,
                'panel_type' => $panel_type,
                'password' => bcrypt('123456'),
                'reagent_id' => $agent,
                'creator' => user('user_id'),
                'is_active' => 1,
                'agent_id' => getAgent()->ag_id
            ]);
            DB::table("persons")->where("prsn_id", $person->prsn_id)->update([
                "prsn_user_id" => $user->user_id,
                'prsn_is_owner'=>1
            ]);

            if ($panel_type == 42 || $panel_type == 43) {
                DB::table('collection_porsant_period')->insert([
                    'cpp_collection_id' => $collection->coll_id,
                    'cpp_from_time' => jalali_to_timestamp(jdate()->format("Y/m/d")),
                    'cpp_to_time' => jalali_to_timestamp(\Morilog\Jalali\Jalalian::now()->addMonths(1)->format("Y/m/d"), "/", false)
                ]);
            }

            if ($request->file('file')) {
                \Illuminate\Support\Facades\DB::table('upload_route')->insert([
                    [
                        'ur_collection_id' => $collection->coll_id,
                        'ur_fk_id' => $collection->coll_id,
                        'ur_table_name' => "logo",
                        'ur_path' => uploadFile($request->file('file'), 'logo', $user->user_id)
                    ],
                ]);
            }

            $inv = new \App\Inventories();
            $inv->inv_name = "انبار پیش فرض";
            $inv->inv_collection_id = $collection->coll_id;
            $inv->save();

            $fc = new \App\FinancialCycle();
            $fc->fc_name = "دوره مالی پیش فرض";
            $fc->fc_collection_id = $collection->coll_id;
            $fc->fc_date_from = jdate()->format("Y/m/d");
            $fc->fc_date_to = 0;
            $fc->fc_status = 1;
            $fc->save();


            $roles = DB::table('class_roles')
                ->select([
                    'clr_role_id'
                ])
                ->whereIn('clr_class_id', $request->classType)
                ->where('clr_type_id', findUser($user->user_id, "panel_type", "id"))
                ->get();

            $array = [];
            foreach ($roles as $role) {
                array_push($array, [
                    'ur_rol_id' => $role->clr_role_id,
                    'ur_user_id' => $user->user_id
                ]);
            }

            DB::table('user_roles')->insert($array);

            ////DefaultRoleFactorReceiver
            if (getDefaultRoleFactorReceiver($user->user_id)==true){
                DB::table('user_roles')
                    ->insert([
                        'ur_rol_id' => getDefaultFactorReceiver(),
                        'ur_user_id' => $user->user_id
                    ]);
            }
            $array = [];
            foreach ($request->classType as $type) {
                array_push($array, [
                    'coc_typeClass_id' => $type,
                    'coc_coll_id' => $collection->coll_id,
                ]);
            }
            DB::table('collection_classes')->insert($array);
            DB::commit();
            return $person->prsn_id;
        } catch (\Exception $exception) {
            echo $exception->getMessage();
            exit();
            session()->flash('Error', config('first_config.message.error_message'));
            DB::rollBack();
        }
    }

    public static function enroll_visitore_supermarket_store($request)
    {
        DB::beginTransaction();
        try {
            $person = Person::create(array_merge($request->persons, [
                'prsn_gender_id' => ($request->has('persons.prsn_gender_id') && $request->persons['prsn_gender_id'] != "") ? $request->persons['prsn_gender_id'] : 1
            ]));
            $collection = Collection::create(array_merge($request->collections, [
                'coll_type_id' => ($request->has('collections.coll_type_id') && $request->collections['coll_type_id'] != "") ? $request->collections['coll_type_id'] : 1,
                'coll_type_class_id' => $request->has('collections.coll_type_class_id') && $request->collections['coll_type_class_id'] ? $request->collections['coll_type_class_id'] : 1,
                'coll_state_id' => $request->has('collections.coll_state_id') && $request->collections['coll_state_id'] ? $request->collections['coll_state_id'] : 100000,
                'coll_city_id' => $request->has('collections.coll_city_id') && $request->collections['coll_city_id'] ? $request->collections['coll_city_id'] : 100000,
            ]));
            $person->collections()->sync($collection->coll_id);
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            session()->flash('Error', config('first_config.message.error_message'));
            echo $exception->getMessage();
        }
    }

    public static function collection_update($request, $collec)
    {

        DB::beginTransaction();
        try {
            Person::where('prsn_id', $collec->persons->first()->prsn_id)->update($request->persons);
            Collection::where('coll_id', $collec->coll_id)->update(array_merge($request->collections, [
                'coll_state_id' => ($request->has('collections.coll_state_id') && $request->collections['coll_state_id'] != "") ? $request->collections['coll_state_id'] : 100000,
                'coll_city_id' => ($request->has('collections.coll_city_id') && $request->collections['coll_city_id']) ? $request->collections['coll_city_id'] : 100000
            ]));

            if ($request->file('file')) {
                \Illuminate\Support\Facades\DB::table('upload_route')->insert([
                    [
                        'ur_collection_id' => $collec->coll_id,
                        'ur_fk_id' => $collec->coll_id,
                        'ur_table_name' => "logo",
                        'ur_path' => uploadFile($request->file('file'), 'logo', getUserByCollectionId($collec->coll_id))
                    ],
                ]);
            }

            $user = DB::table("collection_persons")->select([
                "prsn_user_id"
            ])
                ->join("persons", "cp_prsn_id", "=", "prsn_id")
                ->where("cp_coll_id", $collec->coll_id)
                ->first();
            $roles = DB::table('class_roles')
                ->select([
                    'clr_role_id'
                ])
                ->whereIn('clr_class_id', $request->classType)
                ->where('clr_type_id', findUser($user->prsn_user_id, "panel_type", "id"))
                ->get();

            $array = [];
            foreach ($roles as $role) {
                array_push($array, [
                    'ur_rol_id' => $role->clr_role_id,
                    'ur_user_id' => $user->prsn_user_id
                ]);
            }
            DB::table('user_roles')->where("ur_user_id", $user->prsn_user_id)->delete();
            DB::table('user_roles')->insert($array);

            DB::table('collection_classes')->whereCocCollId($collec->coll_id)->delete();
            foreach ($request->classType as $type) {
                DB::table('collection_classes')->insert([
                    'coc_typeClass_id' => $type,
                    'coc_coll_id' => $collec->coll_id,
                ]);
            }
            DB::commit();
        } catch (\Exception $exception) {
            echo $exception->getMessage();
            exit();
            session()->flash('Error', config('first_config.message.error_message'));
            DB::rollBack();
        }

    }

    public static function getList($request, $type, $perPage = 20)
    {
        $filters = @array_filter($request->all());
        $collections = DB::table('collections as coll')
            ->select([
                "coll.*",
                "state.c_name as state_name",
                "city.c_name as city_name",
                "prs.*"
            ])
            ->leftjoin('collection_persons as coll_prs', 'coll_prs.cp_coll_id', '=', 'coll_id')
            ->leftjoin('persons as prs', 'prs.prsn_id', '=', 'coll_prs.cp_prsn_id')
            ->leftjoin('users', 'prsn_user_id', '=', 'user_id')
            ->leftjoin('cities as state', 'state.c_id', '=', 'coll.coll_state_id')
            ->leftjoin('cities as city', 'city.c_id', '=', 'coll.coll_city_id')
            ->where('panel_type', $type);

        if (isset($filters['name']))
            $collections->where("coll_name", 'like', "%{$filters['name']}%");
        if (isset($filters['id']))
            $collections->where("coll_id", '=', "{$filters['id']}");
        if (isset($filters['state']))
            $collections->where("coll.coll_state_id", '=', $filters['state']);
        if (isset($filters['city']))
            $collections->where("coll.coll_city_id", '=', $filters['city']);
        //get raw sql
        //return self::rawSql($collections);
        $collections = $collections
            ->orderBy("coll.coll_id", "desc")
            ->paginate(20);
        return $collections;
    }

    public static function rawSql($query)
    {
        return vsprintf(str_replace('?', '%s', $query->toSql()), collect($query->getBindings())->map(function ($binding) {
            return is_numeric($binding) ? $binding : "'{$binding}'";
        })->toArray());
    }
}
