<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventories extends Model
{
    public $timestamps = false;
    protected $table = "inventories";
    //
}
