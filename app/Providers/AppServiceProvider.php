<?php

namespace App\Providers;

use App\Baseinfo;
use App\City;
use App\Role;
use \Morilog\Jalali\Jalalian;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extendImplicit('min_array_size', function ($attribute, $value, $parameters) {
            $data = $value;
            if (!is_array($data)) {
                return false;
            }
            return count($data) >= $parameters[0];
        }, "هیچ موردی انتخاب نشده است");

        Validator::extend('jalali', function ($attribute, $value, $parameters) {
            if (!is_string($value) || strlen($value) != 10) {
                return false;
            }
            $delimiter = count($parameters) ? $parameters[0] : '/';
            list($y,$m,$d) = explode($delimiter,$value);
            return \Morilog\Jalali\CalendarUtils::checkDate($y, $m, $d, true);
        },'تاریخ وارد شده نامعتبر است');

        Validator::extend('jalali_before', function ($attribute, $value, $parameters,$validator) {
            if (!is_string($value) || strlen($value) != 10) {
                return false;
            }
            $delimiter = count($parameters) ? $parameters[0] : '/';
            list($y,$m,$d) = explode($delimiter,$value);
            list($yd,$md,$dd) = explode($delimiter,$parameters[1]);
            $validator->addReplacer('jalali_before', function($message, $attribute, $rule, $parameters){
                return str_replace([':min'], $parameters[1], $message);
            });
            return (new Jalalian($y, $m, $d))->lessThan((new Jalalian($yd, $md, $dd)));
        },'تاریخ وارد شده باید کوچکتر از :min باشد');

        Validator::extend('jalali_after', function ($attribute, $value, $parameters,$validator) {
            if (!is_string($value) || strlen($value) != 10) {
                return false;
            }
            $delimiter = count($parameters) ? $parameters[0] : '/';
            list($y,$m,$d) = explode($delimiter,$value);
            list($yd,$md,$dd) = explode($delimiter,$parameters[1]);
            $validator->addReplacer('jalali_after', function($message, $attribute, $rule, $parameters){
                return str_replace([':max'], $parameters[1], $message);
            });
            return (new Jalalian($y, $m, $d))->greaterThan((new Jalalian($yd, $md, $dd)));
        },'تاریخ وارد شده باید بزرگتر از :max باشد');

        Validator::extend('exist_value', function ($attribute, $value, $parameters, $validator) {
            $check = DB::table($parameters[0])->where($parameters[1], $value)->first();
            if (!empty($check))
                return true;
            else
                return false;
        });
        Validator::extend('max_float_character', function ($attribute, $value, $parameters, $validator) {
            if (mb_strlen($value) > $parameters[0])
                return false;
            else
                return true;
        });

        Validator::extend('exist_value_baseinfos', function ($attribute, $value, $parameters, $validator) {
            $check = DB::table('baseinfos')->where('bas_type', $parameters[0])->where('bas_id', $value)->first();
            if (!empty($check))
                return true;
            else
                return false;
        });
        Validator::extend('unique_string', function ($attribute, $value, $parameters, $validator) {

            $check = DB::table($parameters[0])->whereRaw("replace({$parameters[1]},' ','') = ?", [str_replace(' ', '', $value)]);
            if (!empty($parameters[2]) && !empty($parameters[3]))
                $check = $check->where($parameters[3], '<>', $parameters[2]);
            $check = $check->first();
            if (empty($check))
                return true;
            else
                return false;
        }, 'این مورد تکراری میباشد');
        Validator::extend('not_exists', function ($attribute, $value, $parameters, $validator) {

            $check = DB::table($parameters[0])->whereRaw("replace({$parameters[1]},' ','') = ?", [str_replace(' ', '', $value)]);
            if (!empty($parameters[2]) && !empty($parameters[3]))
                $check = $check->where($parameters[3], '<>', $parameters[2]);
            $check = $check->first();
            if (empty($check))
                return true;
            else
                return false;
        }, 'این مورد قبلا ثبت شده است!');
        View::composer(['factories.form', 'companies.form', 'wholesalers.form', 'agents.form', 'users.form'], function ($view) {
            $states = City::where('c_parent_id', '=', '0')->get();
            $gender = Baseinfo::where('bas_type', 'gender')->where('bas_parent_id', '<>', '0')->get();
            $type = Baseinfo::where('bas_type', 'type_collection')->where('bas_parent_id', '<>', '0')->get();
            $typeClass = Baseinfo::where('bas_type', 'type_class_collection')->where('bas_parent_id', '<>', '0')->get();
            $roles = Role::where('rol_id', '<>', '1')->get();
            $view->with([
                'states' => $states,
                'gender' => $gender,
                'type' => $type,
                'typeClass' => $typeClass,
                'roles' => $roles
            ]);
        });
    }
}
