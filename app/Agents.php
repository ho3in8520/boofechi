<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class agents extends Model
{
    protected $table='agents';
    protected $primaryKey='ag_id';
    public $timestamps=false;
}
