<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PanelWizard extends Model
{
    protected $table = 'panel_wizards';
    protected $primaryKey = 'pw_id';
    public $timestamps = false;
}
