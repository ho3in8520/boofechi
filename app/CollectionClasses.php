<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CollectionClasses extends Model
{
    protected $table = "collection_classes";
    protected $primaryKey = "coc_id";
    protected $guarded = [];

    public function collections()
    {
        return $this->belongsTo(Collection::class,'coc_coll_id');
    }
}
