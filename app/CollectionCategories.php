<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CollectionCategories extends Model
{
    public $timestamps = false;
    public $table = "collection_categories";
}
