<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UploadRoute extends Model
{
    protected $table = "upload_route";
    protected $primaryKey = "ur_id";
    protected $guarded = [];

    public function baseinfos()
    {
        return $this->belongsTo(Baseinfo::class,'ur_type_id');
    }

}
