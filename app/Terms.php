<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Terms extends Model
{
    protected $table='panel_terms';
    protected $primaryKey='pt_id';
    public $timestamps=false;
}
