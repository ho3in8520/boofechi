<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table='setting';
    protected $primaryKey='st_id';
    public $timestamps=false;
}
