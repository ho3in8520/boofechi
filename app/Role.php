<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = "roles";
    protected $primaryKey = "rol_id";
    protected $guarded = [];

    public $timestamps = false;

    public function users()
    {
        return $this->belongsToMany(User::class,'user_roles','ur_rol_id','ur_user_id');
    }
    public function permissions()
    {
        return $this->belongsToMany(Permission::class,'role_permissions','rp_rol_id','rp_perm_id');
    }
}
