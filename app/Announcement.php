<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{
    protected $table = "announcements";

    public function user(){
        return $this->hasOne(User::class,'user_id','ann_user_id');
    }
}
