<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Collection extends Model
{
    protected $table = "collections";
    protected $primaryKey = "coll_id";
    public $timestamps = false;
    protected $guarded = [];

    public function persons()
    {
        return $this->belongsToMany(Person::class, 'collection_persons', 'cp_coll_id', 'cp_prsn_id');
    }

    public function state()
    {
        return $this->belongsTo(City::class, 'coll_state_id');
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'coll_city_id');
    }

    public function products()
    {
        return $this->belongsToMany(Product::Class, 'collection_products', 'cp_collection_id', 'cp_product_id');
    }

    public function collectionClass()
    {
        return $this->hasMany(CollectionClasses::class,'coc_coll_id');
    }
}
