<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    protected $table = "persons";
    protected $primaryKey = "prsn_id";
    protected $guarded = [];
    public $timestamps = false;

    public function collections()
    {
        return $this->belongsToMany(Collection::class,'collection_persons','cp_prsn_id','cp_coll_id')->withPivot('cp_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'prsn_user_id');
    }

    public function type(){
        return $this->hasOne(Baseinfo::class,'bas_id','prsn_role_id');
    }
}
