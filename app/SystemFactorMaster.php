<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SystemFactorMaster extends Model
{
    protected $table='system_factor_master';
    protected $primaryKey='sfm_id';
    public $timestamps=false;
}
