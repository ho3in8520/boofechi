<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FinancialCycle extends Model
{
    public $timestamps = false;
    protected $table = "financial_cycles";
}
