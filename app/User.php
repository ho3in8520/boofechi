<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "users";
    protected $primaryKey = "user_id";
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'user_roles', 'ur_user_id', 'ur_rol_id');
    }

    public function hasRole($role)
    {
        if (is_string($role)) {
            return $this->roles->contains('rol_name', $role);
        }
        return !!$role->intersect($this->roles)->count();
    }

    public function person()
    {
        return $this->hasOne(Person::class,'prsn_user_id','user_id');
    }
}
