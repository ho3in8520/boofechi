<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = "cities";
    protected $primaryKey = "c_id";
    protected $guarded = [];

    public function collections()
    {
        return $this->hasMany('App\Collection');
    }
}
