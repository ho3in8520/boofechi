<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AreaSupport extends Model
{
    public $table = "area_support";
    protected $primaryKey = "as_id";
    public $timestamps = false;
    protected $guarded = [];
}
