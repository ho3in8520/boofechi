<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $primaryKey = "prod_id";
    protected $guarded = [];

    public function category()
    {
        return $this->belongsTo(Category::class, 'prod_category_id');
    }

    public function collections()
    {
        return $this->belongsToMany(Collection::class, 'collection_products', 'cp_product_id', 'cp_collection_id');
    }

    //che mahsoli eshant dare...
    public function complimentary()
    {
        return $this->belongsTo(ProductComplimentary::class, 'pc_product_id');
    }

    //kodom maksol ro mikhan eshant bedan...
    public function complimentaryProduct()
    {
        return $this->belongsTo(ProductComplimentary::class, 'pc_free_product_id');
    }
}
