<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CollectionPersons extends Model
{
    protected $table='collection_persons';
    protected $primaryKey='cp_id';
    public $timestamps=false;
}
