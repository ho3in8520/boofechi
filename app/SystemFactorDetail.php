<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SystemFactorDetail extends Model
{
    protected $table='system_factor_detail';
    protected $primaryKey='sfd_id';
    public $timestamps=false;
}
