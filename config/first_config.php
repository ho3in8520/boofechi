<?php
return [
    'panel_name' => 'سامانه مدیریت فروش',
    'factor-statuses' => [
        0 => ['value' => 'در حال بررسی', 'permission' => 'first-accept-factor', 'color' => '#9E9E9E'],
        1 => ['value' => 'تایید موقت', 'permission' => 'first-accept-factor', 'color' => '#666EE8'],
        2 => ['value' => 'تایید نهایی', 'permission' => 'last-accept-factor', 'color' => '#28D094'],
        3 => ['value' => 'لغو', 'permission' => 'cancel-factor', 'color' => '#FF4961'],
       // 4 => ['value' => 'معلق', 'permission' => 'suspend-factor', 'color' => '#FF9149']
    ],
    'ticket-statuses' => [
        0 => ['value' => 'در حال بررسی', 'permission' => 'first-accept-factor', 'color' => '#9E9E9E'],
        1 => ['value' => 'پاسخ داده شده', 'permission' => 'first-accept-factor', 'color' => '#666EE8'],
        2 => ['value' => 'پاسخ مشتری', 'permission' => 'last-accept-factor', 'color' => '#28D094'],
        3 => ['value' => 'ارجاع داده شده', 'permission' => 'cancel-factor', 'color' => '#FF4961'],
        4 => ['value' => 'بسته شده', 'permission' => 'cancel-factor', 'color' => '#000000'],
    ],
    'message' => [
        'success_message' => 'اطلاعات با موفقیت ثبت شد.',
        'error_message' => 'اطلاعات ثبت نشد. خطای سیستمی رخ داده لطفا با واحد پشتیبانی تماس حاصل فرمایید!',
        'empty_table' => 'موردی برای نمایش یافت نشد!',
        'empty_table_return_products' => 'کلیه کالاهای فاکتور جاری قبلا برگشت شده اند!',
        'relation_delete' => 'به دلیل وجود اطلاعات وابسته، عملیات حذف امکان پذیر نمی باشد!',
        'confirm-delete' => 'اطمینان از حذف این مورد دارید؟',
        'user-waiting-wizard' => 'اطلاعات شما با موفقیت ثبت گردید، لطفا تا تایید اطلاعات توسط سامانه منتظر بمانید. با تشکر',
        'user-waiting-error' => 'متاسفانه پنل شما تایید نگردید به دلیل : ',
    ],
];