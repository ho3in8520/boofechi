<?php

use Illuminate\Support\Facades\File;

Auth::routes();

Route::get('/calculate-porsants', function () {
    call_sp("check_period_porsant", null, [
        'current_time' => jalali_to_timestamp(\Morilog\Jalali\Jalalian::now()->subDays(setting('forge_calc_porsant'))->format("Y/m/d"), "/", false),
        'next_month_time' => jalali_to_timestamp(\Morilog\Jalali\Jalalian::now()->subDays(setting('forge_calc_porsant'))->addMonths(1)->format("Y/m/d"), "/", false),
        'created_at' => time()
    ]);
});

Route::match(["post", "get"], '/forget-password', 'Auth\ForgotPasswordController@forget_password')->name('forget-password');

Route::group(['namespace' => 'Admin', 'middleware' => ['auth'], 'prefix' => 'admin'], function () {
    Route::get('/accept-uploaded-files', 'SettingController@acceptUploadedFiles')->name('accept-uploaded-files');
    Route::get('/menu-setting', 'SettingController@menuSetting')->name('menu-setting');
    Route::post('/save-menu', 'SettingController@saveMenu')->name('menu-save');
    Route::match(["post", "get", "delete"], '/create-terms/{id?}', 'SettingController@create_terms')->name('create-terms');
    Route::match(['get', 'post', 'patch'], '/basic-info/{id?}', 'SettingController@basicInfo')->name('basic-info');
    Route::match(['delete'], '/delete-basic/{id}', 'SettingController@delete_basicInfo')->name('basic-info-delete');
    Route::post('/files/accept', 'SettingController@acceptFile');
    Route::post('/files/deaccept', 'SettingController@deacceptFile');
    Route::get('/accept-registration', 'SettingController@acceptRegistration')->name('accept-registration');
    Route::get('/get-form', 'SettingController@getForm');
    Route::post('/form/accept', 'SettingController@acceptForm');
    Route::post('/form/deaccept', 'SettingController@deacceptForm');
    Route::get('support-form/{id}', 'ManageController@supportForm')->name("support-form");
    Route::get('support-tickets/{id}', 'ManageController@tickets')->name("support-tickets");
    Route::get('support-subscriptions/{id}', 'ManageController@subscription')->name("support-subscriptions");
    Route::get('user/details', 'ManageController@userDetails');
    Route::get('user/permissions', 'ManageController@userPermissions');
    Route::post('user/block', 'ManageController@userBlock');
    Route::get('factors/{id}', 'ManageController@factors');
    Route::match(['post', 'get'], 'user-roles/{id}', 'ManageController@userRoles');
    Route::get('violations-log/{id}', 'ManageController@violationsLog');
    Route::get('porsant/{id}', 'ManageController@porsant');
    Route::get('user-announcements/{id}', 'ManageController@userAnnouncements');
    Route::get('user-profile/{id}', 'ManageController@userProfile');
    Route::get('user-documents/{id}', 'ManageController@userDocuments');
    Route::match(['get', 'post'], 'user-setting/{id}', 'ManageController@userSetting');
    Route::get('reports', 'ManageController@reports');
    Route::get('view-report/{id}', 'ManageController@viewReport');
    Route::post('add-violation/{id}', 'ManageController@addViolation');
    Route::match(['get', 'post'], 'system-factor/{id}', 'ManageController@systemFactor');
    Route::match(['get', 'post'], 'list-system-factor/{id?}', 'ManageController@listSystemFactor')->name("list-system-factor");
    Route::match(['get', 'post'], 'edit-system-factor/{id}', 'ManageController@editSystemFactor')->name("edit-system-factor");
    Route::post('return-system-factor/{id}', 'ManageController@returnSystemFactor');
    Route::match(['get', 'post'], 'design-print/{id}', 'ManageController@designPrint')->name('design-print');
    Route::match(['get', 'post'], 'collections', 'ManageController@list_collection')->name('collections');
    Route::post('is-active/{id}', 'ManageController@isActive');
    Route::post('edit-porsant', 'ManageController@editPorsant');
    Route::match(['get', 'post'], 'system-setting', 'SettingController@systemSetting')->name('system-setting');
    Route::match(['post'], 'block-collection/{id}', 'ManageController@blockCollection');
    Route::match(['post'], 'unblock-collection/{id}', 'ManageController@unblockCollection');
    Route::match(['get', 'post'], 'search-user', 'ManageController@searchUser')->name('search-user');
    Route::match(['get', 'post'], 'create-advertising', 'ManageController@createAdvertising')->name('create-advertising');
    Route::match(['get', 'post'], 'list-advertising', 'ManageController@listAdvertising')->name('list-advertising');
    Route::match(['get', 'post'], 'edit-advertising/{id}', 'ManageController@editAdvertising')->name('edit-advertising');
    Route::post('change-password', 'ManageController@changePasswordUser');
    Route::match(['get', 'post'], 'wallet/{id}', 'ManageController@wallet')->name('wallet');
    Route::post('update-wallet', 'ManageController@updateWallet');
    Route::get('collections-ajax', 'ManageController@listCollectionAjax');
    Route::get('search_user_transaction', 'ManageController@searchUserTransaction');
    Route::post('delete-role/{id}', 'ManageController@deleteRoleSupportForm');
    Route::match(['post', 'get'], 'create-transaction', 'ManageController@createTransactionSystem')->name('transaction');
    Route::match(['post', 'get'], 'list-transaction', 'ManageController@listTransaction')->name('list-transaction');
    Route::match(['post', 'get'], 'porsant-collection/{id}', 'ManageController@porsantCollection');
    Route::match(['post', 'get'], 'wallet-history', 'ManageController@walletHistory');
    Route::match(['post', 'get'], 'checkout-request-list', 'ManageController@checkoutRequestList');
    Route::match(['post', 'get'], 'edit-shopkeeper/{id}', 'ManageController@editShopkeeper');
    Route::match(['post', 'get'], 'edit-visitor/{id}', 'ManageController@editVisitor');
    Route::post('login-panel', 'ManageController@loginPanelUser')->name('admin.login.panel.user');
});

Route::middleware(['auth'])->group(function () {
    Route::match(["post", "get"], '/role/set-role-to-class', 'RoleController@setRoleToClass')->name('set-role-to-class');
    Route::match(["post", "get"], '/role/get-role-to-class', 'RoleController@getRoleToClass');
    Route::get('/product-list/{senf?}/{cat?}', 'ProductController@productList')->name('product-list');
    Route::get('/product/eshant-detail/{id}', 'ProductController@eshantDetail')->name('eshant-detail');
    Route::post('/product/delete/{id}', 'ProductController@delete');
    Route::post('/role/delete/{id}', 'RoleController@delete');
    Route::resources([
        '/category' => 'CategoryController',
        '/inventory-factors' => 'InventoryFactorsController',
        '/announcement' => 'AnnouncementController',
        '/upload_route' => 'UploadRouteController',
        '/ticket' => 'TicketController'
    ]);

    //notes dashboard
    Route::post('/notes-dashboard', 'CollectionController@notesDashboard');
    Route::post('/checkout-permitted', 'CollectionController@checkoutPermitted');

    //profile
    Route::post('/change-mobile', 'CollectionController@changeMobile');
    Route::match(["post", "get"], '/change-email/{code?}', 'CollectionController@changeEmail');


    //company
    Route::match(["post", "get"], '/area-support', 'CollectionController@editAreaSupportCompany')->name('area-support-company');
    Route::post('/update-wallet', 'CollectionController@updateWallet');

    //category
    Route::post('category/get-categories-by-senf-id/{id}', 'CategoryController@getCatBySenfId');

    Route::get('files/{full_path}', function ($full_path) {
        try {
            $full_path = str_replace('_-', '/', $full_path);
            $path = "../" . DIRECTORY_SEPARATOR . "uploads" . DIRECTORY_SEPARATOR . $full_path;
            $file = File::get($path);
            $type = File::mimeType($path);
            $response = \Illuminate\Support\Facades\Response::make($file, 200);
            $response->header("Content-Type", $type);
            return $response;
        } catch (Exception $e) {
            abort(404);
        }

    })->name('files');

    //verrify-payment
    Route::match(['get', 'post'], '/payment-verify', 'SiteController@paymentVerify')->name('payment-verify');


    //user
    Route::match(['get', 'post'], '/create-user', 'CollectionController@createUser')->name('create-user');
    Route::match(['get', 'patch'], '/update-user/{id}', 'CollectionController@updateUser')->name('update-user');
    Route::match(['get', 'patch'], '/users-list', 'CollectionController@usersList')->name('users-list');
    Route::match(['delete'], '/delete-user/{id}', 'CollectionController@deleteUser')->name('delete-list');

    Route::match(['get', 'post'], '/authentication', 'CollectionController@register_authentication')->name('authentication');
    Route::match(["post", "get"], '/shopkeeper/business-form', 'CollectionController@register_businessForm_shopkeeper')->name('business-form_shopkeeper');
    Route::match(["post", "get"], '/visitor/business-form', 'CollectionController@register_businessForm_visitor')->name('business-form_visitor');
    Route::match(["post", "get"], '/bag-log', 'UserController@bagLog')->name('business-form_visitor');
//    Route::match(["post", "get"], '/upload-document', 'CollectionController@register_uploadDocument')->name('upload-document');
    Route::match(["post", "get"], '/product/eshant/{id}', 'ProductController@eshant')->name('eshant');
    Route::post('/product/delete-eshant/{id}', 'ProductController@deleteEshant')->name('delete-eshant');
    Route::post('/product/edit-eshant/{id}', 'ProductController@editEshant')->name('edit-eshant');
//    Route::match(["post", "get"], '/shopkeeper/dashboard', 'SiteController@shopkeeper_dashboard')->name('dashboard');
    Route::match(["post", "get"], '/announcement-list', 'CollectionController@shopkeeper_notification_list')->name('announcement-list');
    Route::match(["post", "get"], '/announcement-form/{id}', 'CollectionController@shopkeeper_notification_form')->name('announcement-form');
    Route::match(["post", "get"], '/payment-methods', 'CollectionController@paymentMethods')->name('payment-methods');
    Route::match(["post", "get", "delete"], '/festival-discounts/{id?}', 'CollectionController@festival_discounts')->name('festival-discounts');
    Route::match(["post", "get", "delete"], '/factor-eshant/{id?}', 'CollectionController@factor_eshant')->name('factor-eshant');
    Route::match(["post", "get"], '/commission', 'CollectionController@commission')->name('commission');
    Route::match(["post", "get"], '/return-reasons', 'CollectionController@return_reasons')->name('return-reasons');
    Route::match(["post", "get"], '/area-support/{id}', 'CollectionController@area_support')->name('area-support');
    Route::match(["post", "get"], '/profile/{id}', 'CollectionController@view_profile')->name('view-profile');
    Route::post('/send-message/{id}', 'CollectionController@sendMessage')->name('send-message');
    Route::post('/report-to-admin/{id}', 'CollectionController@reportMessage')->name('report-to-admin');
    Route::match(["patch", "get", "post"], '/edit-profile', 'CollectionController@edit_profile')->name('edit-profile');
    Route::match(["post", "get"], 'invoice-list', 'CollectionController@invoice_list')->name('invoice-list');
    Route::match(["post", "get"], 'online-gates', 'CollectionController@onlineGates')->name('online-gates');
    Route::match(["post"], 'change-status-gate/{id}', 'CollectionController@changeStatus')->name('change-status-gate');

    Route::match(["post", "get", "patch", "delete"], 'wizard-setting/{id?}', 'CollectionController@wizard_setting')->name('wizard-setting');
    Route::match(["post", "get"], '/terms', 'CollectionController@terms')->name('terms');
//
    //visitor
    Route::match(["post", "get"], 'visitor-list', 'VisitorController@visitorList');
    Route::post('exit-purchasing-for-subscription', 'VisitorController@exitPurchaseForSubscription')->name("exit-purchasing-for-subscription");
    Route::match(["post", "get"], 'purchase-check-otp/{id}', 'VisitorController@purchaseCheckOtp');
    Route::match(["post", "get"], 'subscription', 'VisitorController@subscription')->name('subscription');
    Route::match(["post", "get"], 'porsant-cleanse', 'VisitorController@poursant_cleanse')->name('poursant-cleanse');
    Route::match(["post", "get"], 'porsant', 'VisitorController@porsant')->name('porsant');
    Route::match(["post", "get"], 'purchase-for-subscription', 'VisitorController@purchaseForSubscription')->name("purchase-for-subscription");

    //agent
    Route::match(["post", "get"], 'agent/register-agent', 'AgentController@registerAgent')->name('register-agent');
    Route::match(["post", "get"], 'list-agent', 'AgentController@listAgents')->name('list-agent');
    Route::match(["post", "get"], 'edit-agent/{id}', 'AgentController@editAgent')->name('edit-agent');
    Route::match(["delete"], 'delete-agentUpload/{id}', 'AgentController@delete_agentUpload')->name('delete-agentUpload');
    Route::match(["post"], 'change-agentpass', 'AgentController@change_agentPass')->name('change-agentpass');

    //request
    Route::match(["get", "post"], 'request', 'CollectionController@requests')->name('request');

    //massage
    Route::match(["get", "post"], 'list-massage', 'CollectionController@listMassage')->name('list-massage');
    Route::match(["get", "post"], 'view-massage/{id}', 'CollectionController@viewMassage')->name('view-massage');

    //setting company
    Route::match(["get", "post"], 'setting', 'CollectionController@settingUser')->name('setting-user');

    //ticket
    Route::post('ticket/close', 'TicketController@close')->name('close-ticket');
    Route::post('ticket/answer/{id}', 'TicketController@answer')->name('answer-ticket');
    Route::get('/show-ticket/{id}', 'TicketController@showTicketCustomer')->name('show-ticket');
    Route::get('my-tickets', 'TicketController@myTickets')->name('my-tickets');
    Route::post('ticket/referral/{id}', 'TicketController@referral')->name('referral-ticket');

    //factor routes
    Route::get('/popup-factors/{id}', 'FactorController@popupFactorsForReturnProducts')->name('popup-factors');
    Route::get('/output-factors/{id?}', 'FactorController@output')->name('output-factors');
    Route::get('/input-factors', 'FactorController@input')->name('input-factors');
    Route::get('/factor/detail-list/{id?}', 'FactorController@factorDetail')->name('factor-details');
    Route::match(['get', 'post'], '/factor/view-input/{id}', 'FactorController@operationViewInputFactor')->name('view');
    Route::match(['get', 'post'], '/factor/view-output/{id}', 'FactorController@operationViewOutputFactor')->name('view');
    Route::match(['get', 'post'], '/factor/payment/{id}', 'FactorController@paymentFactor')->name('factor-payment');
    Route::get('/factor/update-factor/{id}', 'FactorController@openFactorForEdit');
    Route::post('/factor/accept-return', 'FactorController@acceptReturnItem');
    Route::post('/factor/deaccept-return', 'FactorController@deacceptReturnItem');
    Route::get('/archived-factor', 'FactorController@archivedFactors')->name('archived-factors');
    Route::match(['get', 'post'], 'view-system-factor/{id}', 'CollectionController@viewSystemFactor');
    Route::match(['get', 'post'], 'list-factor-system', 'CollectionController@listFactorSystem')->name('list-factor-system');
    Route::post('payment-factor-system', 'CollectionController@paymentFactorSystem')->name('payment-factor-system');
    Route::get('factor/print-factor/{id}', 'FactorController@printFactor');
    Route::post('factor/dont-read', 'FactorController@dontRead');
    Route::post('factor/update-amount', 'FactorController@updateAmount')->name('update-amount');
    //Route::post('/calc-talab-before-payment', 'FactorController@calcTalabBeforePayment');

    //reports
    Route::get('/report/sale/factors', 'ReportController@reportSaleFactors')->name("report-sale-factors");
    Route::get('/report/sale/products', 'ReportController@reportSaleProducts')->name("report-sale-products");
    Route::get('/report/sale/totally', 'ReportController@reportSaleTotally')->name("report-sale-totally");

    //basket routes
    Route::get('/add-to-basket', function () {
        if (addToBasket(request()->get('id'), request()->get('count', 1),1,request()->get('mode', 2),request()->get('type', 0))) {
            return json_encode(['status' => 100, 'msg' => 'با موفقیت اضافه شد!', 'count' => basketCount()]);
        }
        return json_encode(['status' => 101, 'msg' => 'خطا در افزودن به سبد خرید']);
    })->name('add-to-basket');
    Route::get('/get-basket-top-menu', function () {
        $items = getBasketItems();
        $sum = $count = $i = 0;
        $str = '<thead>
                  <th>#</th>
                  <th>لوگو</th>
                  <th>شرکت</th>
                  <th>حداقل مبلغ</th>
                  <th>تعداد</th>
                  <th>مبلغ</th>
                  </thead>
                    <tbody>';
        if ($items) {
            foreach ($items as $item) {
                $i++;
                $sum += withoutZeros($item['price']);
                $count += $item['cnt'];
                $str .= '<tr>
                        <td>' . $i . '</td>
                        <td><img class="rounded-circle" style="width: 30px;height: 30px"
                        src="' . getFile(($item["logo"] != "") ? $item["logo"] : "nologo.png") . '"></td>
                        <td><a href="' . asset('basket-item-details') . '/' . $item['fm_id'] . '"><b>' . $item['coll_name'] . '</b></a>
                        </td>
                        <td>'.number_format($item['min_amount']).'</td>
                        <td>' . $item['cnt'] . '</td>
                        <td>' . number_format(withoutZeros($item['price'])) . '</td>
                    </tr>';
            }
        } else {
            $str .= '<tr>
                    <td colspan="6">سبد خرید شما خالی است</td>
                </tr>';
        }
        $str .= '</tbody>
                    <tfoot>
                          <tr>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td><b>جمع</b></td>
                            <td>' . $count . '</td>
                            <td>' . number_format($sum) . '</td>
                          </tr>
                    </tfoot>';
        return json_encode(['status' => 100, 'basket' => $str, 'count' => count($items)]);
    })->name('get-basket-top-menu');
    Route::get('/delete-from-basket', function () {
        if (removeFromBasket(request()->get('id')))
            return json_encode(['status' => 100, 'msg' => 'با موفقیت حذف شد!', 'count' => basketCount()]);
        return json_encode(['status' => 101, 'msg' => 'خطا در حذف از سبد خرید']);
    });
    Route::get('/basket-items', 'BasketController@basketItems')->name('basket-items');
    Route::match(['post', 'get'], '/basket-item-details/{id}', 'BasketController@basketItemDetails')->name('basket-item-details');
    Route::post('/delete-factor-from-basket/{id}', 'BasketController@deleteFactor')->name('delete-factor-from-basket');
    Route::post('/delete-product-from-factor/{id}', 'BasketController@deleteProductFromFactor')->name('delete-product-from-factor');
    Route::post('/update-count', 'BasketController@updateCount')->name('update-count');
    Route::get('/load-basket-items/{id}', 'BasketController@loadBasketItems')->name('load-basket-items');

// wallet
    Route::post('/charging-wallet', 'UserController@chargingWallet');
    Route::post('/create-token', 'CollectionController@createToken');
});
