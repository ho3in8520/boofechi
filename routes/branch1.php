<?php

Auth::routes();
//for get city by state_id
Route::get('/get-cities', function () {
    if (user("panel_type") == 39 || (user('profile_complete') == 0 && in_array(user("panel_type"),[44,45])))//is admin
        $cities = \App\City::where('c_parent_id', request('id'))->get();
    else
        $cities = getAreaSupport(2, null, null, request('id', null));
    $str = customForeach($cities, 'c_id', 'c_name');
    return json_encode($str);
})->name('get-cities');
Route::get('/get-cities-edit-company', function () {
        $cities = \App\City::where('c_parent_id', request('id'))->get();
    $str = customForeach($cities, 'c_id', 'c_name');
    return json_encode($str);
})->name('get-cities-edit-company');
Route::resource("/agent", "AgentController");
Route::middleware(['auth'])->group(function () {
    Route::get('/dashboard',  'HomeController@index')->name('dashboard');
    Route::get('/',  'HomeController@index')->name('dashboard');

    Route::post('/save-menu', 'SettingController@saveMenu')->name('menu-save');
    Route::match(["post", "get"], '/company/area-support/{id}', 'CompanyController@area')->name('area-support');
    Route::match(["post", "get"], '/company/manage-categories/{id}', 'CompanyController@manageCategories')->name('manage-categories');
    Route::resources([
        '/factory' => 'FactoryController',
        '/company' => 'CompanyController',
        '/wholesaler' => 'WholesalerController',
        '/product' => 'ProductController',
        '/upload_route' => 'UploadRouteController',
        '/user' => 'UserController',
        '/role' => 'RoleController'
    ]);
    Route::get('/getRole', function () {
        if (request()->ajax() && request()->has('id') && request('id') != "") {
            $user = new \App\User();
            $role = ($user->find(request('id'))->roles->pluck('rol_id')->toArray());
            return json_encode(["roleHtml" => customForeach(\App\Role::get(), 'rol_id', 'rol_name',$role)]);
        }else{
            return redirect()->back();
        }
    });
    Route::post('/updateRole/{user}','UserController@updateRole')->name('updateRole');
});
