$(document).on("change", '.methodSelectProduct', function () {
    var row = $(this).closest("div.selectProduct");
    if ($(this).val() == 2) {
        $(row).find('.showProduct').show();
    } else {
        $(row).find('.showProduct').hide();
    }
});


$("input[name='collProduct[cp_arzesh_afzoodeh]']").change(function () {
    $("input[name='collProduct[cp_default_price_buy]']").keyup();
});

$("input[name='collProduct[cp_default_price_buy]']").keyup(function () {
    var maliat = "{{setting('tax')}}";
    var price = parseInt($(this).val());
    if (price == 0)
        return false;
    var aa = $("input[name='collProduct[cp_arzesh_afzoodeh]']").is(":checked");
    if (aa)
        $("#with_arzesh_afzoodeh").html(numberFormat(((maliat * price) / 100) + price));
    else
        $("#with_arzesh_afzoodeh").html(numberFormat(price));
});


$(document).on('change', '.discount', function () {
    var elementCheck = $(this);
    if ($(elementCheck).is(':checked')) {
        $(".DiscountForm").hide();
        $(".DiscountForm").find("input[type=text]").val("");
        $(".eshantionForm").hide();
        $(".eshantionForm").find("input[type=text],input[type=hidden]").val("");
        if ($(elementCheck).val() == 1)
            $(".DiscountForm").show();
        if ($(elementCheck).val() == 2)
            $(".eshantionForm").show();
    }
});
$(document).on('click', '.deleteForm', function () {
    var elementClick = $(this);
    if (confirm('آیا از حذف فرم اطمینان دارید ؟'))
        if ($(elementClick).closest('.eshantionForm')) {
            $(elementClick).closest('.eshantionForm').remove();
        } else
            alert('حذف امکان پذیر نمی باشد');
});
$(document).on('click', '.addForm', function () {
    var formHtml = $(this).closest('.eshantionForm').clone();
    formHtml.find(".showProduct").css('display', 'none');
    formHtml.counter('محصول');
    $(this).closest('.eshantionForm').after("<div class='row eshantionForm'>" + formHtml.html() + "</div>");
});

$(document).on('click', '.showProduct', function () {
    showData($(this).data('url'));
    trClick = $(this);
});
$(document).on('change', '#uni_measurement', function () {
    if ($(this).val() != "")
        $("#counter").html("تعداد در هر " + ($("#uni_measurement option:selected").html()));
    else
        $("#counter").html("تعداد");
});

$(document).on('click', '#createProduct', function () {
    var arr = [];
    $('input[name^=pc_date_from]').each(function (k, v) {
        $(this).css('border-color', '#A6A9AE');
        arr[k] = {'from': $(this).val(), 'to': ''};
    });
    $('input[name^=pc_date_to]').each(function (k, v) {
        $(this).css('border-color', '#A6A9AE');
        arr[k]['to'] = $(this).val();
    });
    $('input[name^=pc_date_from]').each(function (index) {
        var elem = $(this);
        var date = $(elem).val();
        $(arr).each(function (key, value) {
            if (index != key) {
                if (date >= value.from && date < value.to) {
                    $(elem).css('border-color', 'red');
                }
            }
        });
    });
    $('input[name^=pc_date_to]').each(function (index) {
        var elem = $(this);
        var date = $(elem).val();
        $(arr).each(function (key, value) {
            if (index != key) {
                if (date > value.from && date <= value.to) {
                    $(elem).css('border-color', 'red');
                }
            }
        });
    });
});

$(document).on('click', '#blah', function () {
    $("#imgInp").click();
});

var $modal = $('#modal');
var image = document.getElementById('image');
var cropper;
var crop;
var imageCrop;
$("body").on("change", "#imgInp", function (e) {
    var files = e.target.files;
    var done = function (url) {
        image.src = url;
        $modal.modal('show');
    };
    var reader;
    var file;

    if (files && files.length > 0) {
        file = files[0];
        if (URL) {
            done(URL.createObjectURL(file));
        } else if (FileReader) {
            reader = new FileReader();
            reader.onload = function (e) {
                done(reader.result);
            };
            reader.readAsDataURL(file);
        }
    }
});
$modal.on('shown.bs.modal', function () {
    cropper = new Cropper(image, {
        aspectRatio: 1,
        viewMode: 1,
        preview: '.preview',
        crop: function(event) {
            imageCrop={
                x:event.detail.x,
                y:event.detail.y,
                width:event.detail.width,
                height:event.detail.height,
            }
        }
    });
}).on('hidden.bs.modal', function () {
    cropper.destroy();
    cropper = null;
});
$("#crop").click(function () {
    $('#modal').modal('hide');
    console.log(cropper.initialCropBoxData);
    canvas = cropper.getCroppedCanvas({
        width: 160,
        height: 160,
    });
    canvas.toBlob(function (blob) {

        url = URL.createObjectURL(blob);

        var reader = new FileReader();
        reader.onload = function (e) {
            $('#blah').attr('src', e.target.result);
        }
        reader.readAsDataURL(blob);
    });
    if ($("#imgInp").val() != "")
        $("#deleteImage").html("<i id='deleteImageUpload' title=\"حذف عکس\" class=\"fa fa-trash text-danger\"></i>")
});


$(document).on('click', '#deleteImageUpload', function () {
    $("#imgInp").val('');
    $("#blah").attr('src', '/images/image.jpg');
    $("#deleteImage").html("");
    imageCrop="";
});
$(document).on('click', '#createProduct', function () {
    var elementClick=$(this);
    var form= new FormData($(elementClick).closest('form')[0]);

    if(imageCrop) {
        form.append("imageCrop", Object.entries(imageCrop));
    }
    $.ajax({
        url: $(elementClick).closest('form').attr('action'),
        type: 'POST',
        data: form,
        cache: false,
        async: true,
        contentType: false,
        processData: false,
        success: function (response) {
            resultResponse(JSON.parse(response), false, 2000, true);
            $(elementClick).html(captionButton)
        },
        error: function (xhr) {
            defaultProgress();
            errorForms(xhr);
            $(elementClick).html("ثبت")
        }
    });
});
