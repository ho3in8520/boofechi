/**
 * Created by hh on 6/24/2019.
 */
$(document).ready(function () {
//            $('.selectpicker').selectpicker();
//            $(".selectpicker").select2({closeOnSelect:false});

    $(".select2WithCheckbox").select2({
        dir: 'rtl',
        closeOnSelect: false,
        allowHtml: true,
        language: {
            errorLoading: function () {
                return "خطا در بارگذاری داده ها"
            },
            inputTooLong: function (e) {
                var t = e.input.length - e.maximum;

                n = "طول متن " + t + " بیش از حد مجاز است!";

                return t != 1 && (n += "es"), n
            },
            inputTooShort: function (e) {
                var t = e.minimum - e.input.length;

                n = "طول متن " + t + " بیش از حد کوتاه است!";

                return n
            },
            loadingMore: function () {
                return "در حال بارگذاری..."
            },
            maximumSelected: function (e) {
                var t = "انتخاب بیش از حد " + e.maximum;

                return e.maximum == 1 ? t += "m" : t += "ns", t
            },
            noResults: function () {
                return "فاقد داده"
            },
            searching: function () {
                return " در حال جستجو..."
            }
        },
//                allowClear: true,
//         tags: true // создает новые опции на лету
    });
});