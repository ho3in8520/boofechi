function deleteAjax(elementClick, url, dataId) {
    if (confirm('اطمینان از حذف این مورد دارید؟') === false)
        return false;
    var old_html = $(elementClick).html();
    $(elementClick).html("<i class='fa fa-spinner fa-spin'></i>");
    $.ajax({
        url: url,
        type: 'delete',
        success: function (response) {
            var res = JSON.parse(response);
            if (res.status == 100) {
                elementClick.closest('tr').fadeOut(500);
                elementClick.closest('tr').remove();
            }
            else {
                $(elementClick).html(old_html);
                alert(res.msg);
            }
        },
        error: function (xhr) {
            if (xhr.status === 419) {
                $(elementClick).html(old_html);
                alert("خطای 419 رخ داده");
                window.location.reload();
            }
        }
    });
}