/////////////////////////////////////////////////////////////////////////////super global variable for find class select2
classNameErrorSelect2 = "select2-selection";

//////////////////////////////////////////////////////////////////////////////main function store ajax
function storeAjax(elementClick, method, captionButton = "ثبت", progress = false, reloadPage = 1, TimeOutActionNextStore = 2000, show_message = true) {
    startConfig();
    if (progress && $("#progressShow").length == 0) {
        $(elementClick).closest("form").append(htmlProgress);
        storeAjax(elementClick, method, captionButton, progress, reloadPage, TimeOutActionNextStore)
    } else {
        $.ajax({
            url: $(elementClick).closest('form').attr('action'),
            type: method,
            data: new FormData($(elementClick).closest('form')[0]),
            cache: false,
            async: true,
            contentType: false,
            processData: false,
            success: function (response) {
                resultResponse(JSON.parse(response), reloadPage, TimeOutActionNextStore, show_message)
                $(elementClick).html(captionButton)
            },
            xhr: function () {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        $("#progressBar").css('width', (evt.loaded / evt.total) * 100 + '%');
                        $("#progressBar").html(Math.floor((evt.loaded / evt.total) * 100) + '%');
                        var percentComplete = (evt.loaded / evt.total) * 100;
                    }
                }, false);
                return xhr;
            },
            error: function (xhr) {
                defaultProgress();
                errorForms(xhr);
                $(elementClick).html("ثبت")
            }
        });
    }
}

///////////////////////////////////////////////////////////////////////////////show message
function alertMessage(typeMessage, message, mode) {
    var messageShow = "";
    switch (typeMessage) {
        case 'success' :
            toastr.success(message, "موفق", {showMethod: "slideDown", hideMethod: "slideUp", timeOut: mode});
            // messageShow = "<div style='text-align: center' class='alert alert-success'><strong>تبریک! </strong>" + message + "</div>";
            break;
        case 'error' :
            toastr.error(message, "خطا", {showMethod: "slideDown", hideMethod: "slideUp", timeOut: mode});
            // messageShow = "<div style='text-align: center' class='alert alert-danger'><strong>خطر! </strong>" + message + "</div>";
            break;
    }
    // $(".ajaxMessage").html(messageShow);
}

////////////////////////////////////////////////////////////////////////////////startConfig form progress bar
function startConfig(showProgress = false) {
    $('.help-block').remove();
    $("input,select,textarea").css(normalCss);
    $("#progressBar").css('width', 0 + '%');
    $("#progressBar").css('background-color', '#3399ff');
    $("input,select,textarea").css(normalCss);
    (showProgress) ? $("#progressShow").show() : false;

}

////////////////////////////////////////////////////////////////////////////////default progrees
function defaultProgress() {
    $("#progressBar").css('width', '0');
}

///////////////////////////////////////////////////////////////////////////////result response
function resultResponse(result, reloadPage, time, show_message = true) {
    var mode = 5e3;
    if (typeof (result.pin) != "undefined")
        mode = 0;
    switch (parseInt(result.status)) {
        case 100:
            $("#progressBar").css('background-color', "green");
            if (show_message)
                alertMessage('success', result.msg, mode);
            if (reloadPage) {
                setTimeout(function () {
                    window.location.reload();
                }, time)
            } else {
                setTimeout(function () {
                    window.location.href = result.url;
                }, time)
            }
            break;
        case 422:
            if (show_message)
                alertMessage('error', result.msg, mode);
            defaultProgress();
            break;
        case 500:
            if (show_message)
                alertMessage('error', result.msg, mode);
            defaultProgress();
            break;
    }

}

/////////////////////////////////////////////////////////////////////////////////error validation form
function errorForms(xhr) {
    if (xhr.status === 422) {
        toastr.error("خطاهای فرم را برطرف نمائید!", "خطا", {
            showMethod: "slideDown",
            hideMethod: "slideUp",
            timeOut: 5e3
        });
        var errorsValidation = JSON.parse(xhr.responseText).errors;
        $.each(errorsValidation, function (key, value) {
            // var errArray = key.split('.');
            key = createKey(key);
            getElement = "[name='" + key + "']";
            var errorHtml = "<div class='help-block text-danger'>" + value + "</div>";
            showHtmlValidation(getElement, errorHtml);
        });
    }
}

function createKey(key) {
    // var temp = key.substring(key.lastIndexOf('.')+1);
    var array = key.split('.');
    var str = "";
    var i = 0;
    $.each(array, function (key, value) {
        if (value == 0) {
            str += "[]";
        } else {
            if (i > 0)
                str += "[" + value + "]";
            else
                str += value;
        }
        i++;
    });
    return str;
}

////////////////////////////////////////////////////////////////////////////////show html error validation
function showHtmlValidation(getElement, errorHtml, indexArr = "") {
    if ($(getElement).parent().find(".help-block").length == 0) {
        if (!$(getElement).hasClass('select2')) {
            if (indexArr != "") {
                if ($(getElement).parent().hasClass('input-group')) {
                    $(getElement).parent().eq(indexArr).after(errorHtml);
                    $(getElement).parent().eq(indexArr).css(errorCss)
                } else {
                    $(getElement).eq(indexArr).after(errorHtml);
                    $(getElement).eq(indexArr).css(errorCss)
                }
            } else {
                if ($(getElement).parent().hasClass('input-group')) {
                    $(getElement).parent().after(errorHtml);
                    $(getElement).parent().css(errorCss)
                } else {
                    $(getElement).after(errorHtml);
                    $(getElement).css(errorCss)
                }
            }
        } else {
            $(getElement).closest('div').append(errorHtml);
            $(getElement).closest('div').find('.' + classNameErrorSelect2).css(errorCss);
        }
    }
}

/////////////////////////////////////////////////////////////////////////////////css validation form and create div show error
var errorCss = {"border-width": "1px", "border-color": "#d2322d", "border-style": "solid"}
var normalCss = {"border-width": "1px", "border-color": "#75787D", "border-style": "solid"}
var htmlProgress = '  <div class="row" id="progressShow"><div class="col-md-12"><div class="progress"><div id="progressBar" class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:0%">0%</div></div></div></div>';





