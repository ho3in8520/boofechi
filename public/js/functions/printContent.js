function printContent(url, data, element, title,beforeHtml) {

    data.push({name: "title", value: title});
    $.ajax({
        type: "GET",
        url: url,
        data: $.param(data),
        success: function (response) {
            $(element).html(beforeHtml);
            var contents = response;
            var idname = name;
            var frame1 = document.createElement('iframe');
            frame1.name = "frame1";
            document.body.appendChild(frame1);
            var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
            frameDoc.document.open();
            frameDoc.document.write('<html><head><title></title>');
            frameDoc.document.write('<style>table{direction:rtl}</style>');

            // your title
            frameDoc.document.title = title;


            frameDoc.document.write('</head><body class="print-body">');
            frameDoc.document.write(contents);
            frameDoc.document.write('</body></html>');
            frameDoc.document.close();
            setTimeout(function () {
                window.frames["frame1"].focus();
                window.frames["frame1"].print();
                setTimeout(function () {
                    document.body.removeChild(frame1);
                },500);
            }, 500);
            return false;
        }
    });

}

$(document).on("click", "a.btn-print,button.btn-print", function (e) {
    e.preventDefault();
    var beforeHtml = $(this).html();
    $(this).html("<i class='fa fa-spinner fa-spin'></i> لطفا منتظر بمانید...");
    var elem = $(this);
    var data = $(elem).closest("form").serializeArray();
    printContent($(elem).data('action'), data, elem, $(elem).data('title'),beforeHtml);
});