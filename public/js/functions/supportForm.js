$(document).ready(function () {
    $(document).on("click", ".report-tabs li a", function () {
        var href = $(this).attr("href");
        href = href.substring(1);
        var address = $("#" + href).find("table").closest("form").attr("action");
        if (address.indexOf('?') != -1)
            address = address.substring(0, address.indexOf('?'));
        var form = $("#" + href).find("table").closest("form");
        $('<input>').attr({
            type: 'hidden',
            id: 'tab',
            name: 'tab',
            value: href
        }).appendTo(form);
    });

    $(document).on('click', '#save-user-setting', function () {
        var elem = $(this);
        var data = $(elem).closest("form").serializeArray();
        var id = $(elem).data("id");
        data.push({
            'name': 'id',
            'value': id
        });
        $.post($(elem).closest("form").attr("action"), data, function (data) {
            var result = JSON.parse(data);
            if (result.status == 100)
                toastr.success(result.msg, "موفق");
            else
                toastr.error(result.msg, "ناموفق");
        });
    });

    $(document).on('click', '.viewTicket', function () {
        trClick = $(this);
        var id = $(this).closest("tr").data("id");
        var buttons = {
            0: {
                title: 'بستن تیکت',
                data: {
                    id: id
                },
                class: 'btn btn-warning closeTicket'
            },
        };
        showData(APP_URL + '/show-ticket/' + id, {}, true, "#basic-form-layouts", buttons);
    });

    $(document).on("click", ".closeTicket", function (e) {
        if (confirm('اطمینان از بستن این تیکت دارید؟') == false)
            return false;
        var element = $(this);
        var data = [];
        data.push({
            'name': "id",
            'value': $(element).data('id')
        });
        $.post(APP_URL + '/ticket/close', data, function (data) {
            var result = JSON.parse(data);
            if (result.status == 100)
                toastr.success(result.msg, "موفق");
            else
                toastr.error(result.msg, "ناموفق");
        });
    });

    $(document).on("click", ".addRoleToUser", function (e) {
        var element = $(this);
        var data = [];
        data.push({
            'name': "role",
            'value': $(element).closest('form').find("select[name=role]").val()
        });
        $.post($(element).closest('form').attr("action"), data, function (data) {
            var result = JSON.parse(data);
            if (result.status == 100)
                toastr.success(result.msg, "موفق");
            else
                toastr.error(result.msg, "ناموفق");
        });
    });

    $(document).on("click", ".blockUser", function (e) {
        var element = $(this);
        if (confirm($(element).data('status') == 1 ? 'اطمینان از بلاک کردن این کاربر دارید؟' : 'اطمینان از باز کردن این کاربر دارید؟') == false)
            return false;
        var after_status = ($(element).data('status') == 1) ? 0 : 1;
        var data = [];
        data.push({
            'name': "id",
            'value': $(element).data('id')
        });
        data.push({
            'name': "status",
            'value': $(element).data('status')
        });
        if ($(element).data('status') == 1) {
            swal({
                title: "علت",
                text: "علت بلاک شدن:",
                input: "textarea",
                showCancelButton: true,
                animation: "slide-from-top",
                inputPlaceholder: "",
                confirmButtonText: 'ثبت',
                cancelButtonText: "انصراف",
            }).then(function (inputValue) {
                if (inputValue === false) return false;
                data.push({
                    'name': "reason",
                    'value': inputValue
                });
                $.post(APP_URL + '/admin/user/block', data, function (data) {
                    var result = JSON.parse(data);
                    if (result.status == 100) {
                        if ($(element).data('status') == 1) {
                            $(document).find(".user-panel-status").html('<strong>وضعیت پنل : </strong><span class="badge badge-danger text-white">غیر فعال</span>');
                        }
                        else {
                            $(document).find(".user-panel-status").html('<strong>وضعیت پنل : </strong><span class="badge badge-success text-white">فعال</span>');
                        }
                        toastr.success(result.msg, "موفق");
                        $(element).data('status', after_status);
                    }
                    else
                        toastr.error(result.msg, "ناموفق");
                });
            }).done();
        }
        else
            $.post(APP_URL + '/admin/user/block', data, function (data) {
                var result = JSON.parse(data);
                if (result.status == 100) {
                    if ($(element).data('status') == 1) {
                        $(document).find(".user-panel-status").html('<strong>وضعیت پنل : </strong><span class="badge badge-danger text-white">غیر فعال</span>');
                    }
                    else {
                        $(document).find(".user-panel-status").html('<strong>وضعیت پنل : </strong><span class="badge badge-success text-white">فعال</span>');
                    }
                    toastr.success(result.msg, "موفق");
                    $(element).data('status', after_status);
                }
                else
                    toastr.error(result.msg, "ناموفق");
            });
    });

    $(document).on("click", ".showUser", function (e) {
        var element = $(this);
        var buttons = {
            0: {
                title: 'بلاک کردن/ باز کردن',
                data: {
                    id: $(element).closest("tr").data('id'),
                    status: $(element).closest("tr").data('status'),
                },
                class: 'btn btn-primary blockUser'
            },
        };
        showData(APP_URL + '/admin/user/details', {id: $(element).closest("tr").data('id')}, true, "#basic-form-layouts", buttons);
        trClick = $(this);
        return false;
    });

    $(document).on("click", ".showPermissions", function (e) {
        var element = $(this);
        showData(APP_URL + '/admin/user/permissions', {id: $(element).closest("tr").data('id')}, true, "#basic-form-layouts");
        trClick = $(this);
        return false;
    });

    $('input[type="file"]').change(function (e) {
        var fileName = e.target.files[0].name;
        $('#show-name-upload').html(fileName)
    });
    $(document).on('click', '#createTicket', function (e) {
        storeAjax($(this), 'POST')

    });
});