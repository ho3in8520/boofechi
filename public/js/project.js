$(document).on('keypress', '[chars]', function (event) {
    var $elem = $(this);
    var chars = $elem.attr('chars');
    var arr = chars.split("|");
    if (typeof arr[1] != "undefined")
        if ($elem.val().length >= parseInt(arr[1])) {
            return false;
            event.preventDefault();
        }
    //$elem.val(value.replace(regReplace, ''));

});
$(".english").bind('keypress', function (e) {
    var regex = new RegExp("^[a-zA-Z0-9 ]+$");
    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (regex.test(str)) return true;
    e.preventDefault();
    return false;
});
$(document).on('blur change', '[chars]', function (event) {
    var $elem = $(this);
    if ($elem.val() == '')
        return false;
    var chars = $elem.attr('chars');
    var arr = chars.split("|");
    var value = $elem.val(),
        regReplace,
        preset = {
            'int': {
                "regex": /[0-9 -()+]+$/,
                "label": "عدد صحیح"
            },
            'float': {
                "regex": '[-+]?([0-9]*.[0-9]+|[0-9]+)',
                "label": "عدد اعشار"
            },
            'ip': {
                "regex": 'bd{1,3}.d{1,3}.d{1,3}.d{1,3}b',
                "label": "آی پی"
            },
            'url': {
                "regex": /^(http(s)?:\/\/)?(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/,
                "label": "url"
            },
            'string': {
                "regex": /^([a-z0-9]{5,})$/,
                "label": "رشته"
            },
            'email': {
                "regex": /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                "label": "ایمیل"
            },
            'jdate': {
                "regex": /^[1][1-4][0-9]{2}\/((0[1-6]\/(0[1-9]|[1-2][0-9]|3[0-1]))|(0[7-9]\/(0[1-9]|[1-2][0-9]|30))|(1[0-1]\/(0[1-9]|[1-2][0-9]|30))|(12\/(0[1-9]|[1-2][0-9])))/,
                "label": "تاریخ"
            },
            'time': {
                "regex": /(?:2[0-3]|[01][0-9]):[0-5][0-9]/,
                "label": "ساعت"
            },
            'mobile': {
                "regex": /^(0)?9\d{9}$/,
                "label": "شماره موبایل"
            },
            'nationalcode': {
                "regex": '',
                "label": "کد ملی"
            },
        },
        filter = preset[arr[0]].regex || arr[0];
    var checked = false;
    if (arr[0] == "nationalcode") {
        checked = checkCodeMelli($elem.val());
    } else {
        regReplace = new RegExp(filter, 'ig');
        checked = regReplace.test($elem.val());
    }
    if (!checked) {
        $elem.css({"border-width": "1px", "border-color": "rgb(210, 50, 45)", "border-style": "solid"});
        if (!$elem.parent().find(".help-block").length && !$elem.parent().parent().find(".help-block").length) {
            if ($elem.parent().hasClass('input-group'))
                $elem.parent().after("<div class='help-block text-danger'>ورودی یک " + preset[arr[0]].label + " نامعتبر است!</div>");
            else
                $elem.after("<div class='help-block text-danger'>ورودی یک " + preset[arr[0]].label + " نامعتبر است!</div>");
        }
    } else {
        // if ($elem.parent().hasClass('input-group')){
        //     if($elem.prev("input").length)
        //         $elem.next("input").blur(function(){
        //         }).blur();
        //     if($elem.next("input").length)
        //         $elem.next("input").blur(function(){
        //         }).blur();
        // }


        $elem.css({"border-width": "1px", "border-color": "#A6A9AE", "border-style": "solid"});
        if ($elem.parent().hasClass('input-group'))
            $elem.parent().parent().find(".help-block").remove();
        else
            $elem.parent().find(".help-block").remove();
    }

});
$("input[chars^='time']").inputmask("99:99");

function checkCodeMelli(value) {
    var r = 0;
    var n = 0;
    var c = 0;
    if (value.length < 10) {
        return false;
    } else if (value.length == 10) {
        if (value == '1111111111' || value == '0000000000' || value == '2222222222' || value == '3333333333' || value == '4444444444' || value == '5555555555' || value == '7777777777' || value == '8888888888' || value == '9999999999')
            return false;
    } else if (value.charAt(0) == '0' && value.charAt(1) == '0' && value.charAt(2) == '0' && value.charAt(3) == '0' && value.charAt(4) == '0' && value.charAt(5) == '0' && value.charAt(6) == '0')
        return false;

    c = parseInt(value.charAt(9));
    for (i = 1; i <= 9; i++) {
        n = n + parseInt(value.charAt(i - 1) * (11 - i));
    }
    r = parseInt(n % 11);
    if ((r < 2 && r == c) || (r > 2 && c == 11 - r)) {
        return true;
    } else
        return false;
}

function getDataAttributes(node) {
    var d = {},
        re_dataAttr = /^data\-(.+)$/;

    $.each(node.get(0).attributes, function (index, attr) {
        if (re_dataAttr.test(attr.nodeName)) {
            var key = attr.nodeName.match(re_dataAttr)[1];
            d[key] = attr.nodeValue;
        }
    });

    return d;
}

$(document).on("click", "button,a", function () {
    var elem = $(this);
    $(elem).css("pointer-events", "none");
    setTimeout(function () {
        $(elem).css("pointer-events", "auto");
    }, 1000);
});

function number_format(Number) {
    Number += '';
    Number = Number.replace(',', '');
    Number = Number.replace(',', '');
    Number = Number.replace(',', '');
    Number = Number.replace(',', '');
    Number = Number.replace(',', '');
    Number = Number.replace(',', '');
    x = Number.split('.');
    y = x[0];
    z = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(y))
        y = y.replace(rgx, '$1' + ',' + '$2');
    return y + z;
}

function previewImage(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

(function ($) {
    $.fn.counter = function (title) {
        if(counter < 0)
            counter = 0;
        counter = counter + 1;
        var cln_element = $(this);
        $(this).find("a,button").each(function () {
            $(this).css("pointer-events", "auto");
        });
        $(this).find("input,select,textarea").each(function () {
            if ($(this).hasClass("datePicker"))
                $(this).removeAttr("id");
            $(this).removeClass("hasDatepicker");
            var name = $(this).attr('name');
            if (name.indexOf('[')) {
                name = name.substring(0, name.indexOf('[')) + '[' + counter + ']';
                // name = name.replace('[]', '[' + counter + ']');
                $(this).attr('name', name);
            }
        });
        $(cln_element).find("input").attr('value', '');
        $(cln_element).find("textarea").html('');
        if (title != '')
            $(cln_element).find('.showProduct').html(title);
        $(cln_element).find(".help-block").remove();
        $(cln_element).find("input").css(normalCss);
    };

    $.fn.beginLoading = function () {
        $this = $(this);
        $this.html("<i class='fa fa-spin fa-spinner'></i>");
    };
    $.fn.endLoading = function (html) {
        $this = $(this);
        $this.html(html);
    };
}(jQuery));
!function (a, r, o) {
    "use strict";
    var t = (o("html"), 0);
    if (Array.prototype.forEach) {
        var c = o(".switchery");
        o.each(c, function (a, r) {
            var t = "", c = "", e = "", i = "", s = "", F = "", l = "", d = "", h = "", n = "";
            t = o(this).data("size");
            var y = {lg: "large", sm: "small", xs: "xsmall"};
            F = void 0 !== o(this).data("size") ? "switchery switchery-" + y[t] : "switchery", c = o(this).data("color"), e = o(this).data("color-secondary"), i = o(this).data("jack-color"), s = o(this).data("jack-color-secondary");
            var w = {
                primary: "#666EE8",
                success: "#28D094",
                danger: "#FF4961",
                warning: "#FF9149",
                info: "#1E9FF2",
                white: "#FFFFFF"
            };
            l = void 0 !== c ? w[c] : "#28D094", d = void 0 !== e ? w[e] : "#FFFFFF", h = void 0 !== i ? w[i] : "#FFFFFF", n = void 0 !== s ? w[s] : "#FFFFFF";
            new Switchery(o(this)[0], {className: F, color: l, secondaryColor: d, jackColor: h, jackSecondaryColor: n})
        })
    } else {
        var e = r.querySelectorAll(".switchery");
        for (t = 0; t < e.length; t++) {
            e[t].data("size"), e[t].data("color"), new Switchery(e[t], {color: "#28D094"})
        }
    }
}(window, document, jQuery);
$(document).ready(function () {

    $(document).on("click", "a[data-confirm]", function (e) {
        e.preventDefault();
        var elem = $(this);
        if (confirm($(elem).data('confirm')) == false)
            return false;
        if ($(elem).data('method')) {
            var options = {
                'action': $(elem).data('action'),
                'target': '_self'
            };
            if ($(elem).data('method').toLowerCase() != "delete")
                $.extend(options, {
                    method: $(elem).data('method')
                });
            else
                $.extend(options, {
                    method: "post"
                });

            var form = $('<form>', options);
            if ($(elem).data('method').toLowerCase() == "delete") {
                form.append($('<input>', {
                    'name': '_method',
                    'value': "delete",
                    'type': 'hidden'
                }));
            }
            form.append($('<input>', {
                'name': '_token',
                'value': $('meta[name=csrf]').attr('content'),
                'type': 'hidden'
            }));
            if ($(elem).data('id')) {
                form.append($('<input>', {
                    'name': 'id',
                    'value': $(elem).data('id'),
                    'type': 'hidden'
                }));
            }
            form.appendTo(document.body);
            $(form).submit();
        }
    });

    $("input[type=text]").attr("autocomplete", 'off');
    $(document).on('click', '.btn-loading', function () {
        $(this).html("<i class='fa fa-spinner fa-spin'></i> لطفا منتظر بمانید...")
    });
    $(document).on('click', '.btn-loading-sm', function () {
        $(this).html("<i class='fa fa-spinner fa-spin'></i>")
    });
    $('.select2').select2({
        dir: 'rtl',

        // // theme: "bootstrap",
        width: '100%',
        // closeOnSelect: false
    });
    var opts = {
        dir: 'rtl',
        // theme: "bootstrap",
        openOnEnter: false,
        width: '100%',
        // placeholder: 'معین',
        // let our custom formatter work
        // minimumInputLength: 2,
        language: {
            inputTooShort: function (args) {
                return "لطفا بیشتر از دو کارکتر وارد کنید";
            },
            inputTooLong: function (args) {
                return "متن ورودی بیشتر از حد مجاز است";
            },
            errorLoading: function () {
                return "نتیجه ای در بر نداشت";
            },
            loadingMore: function () {
                return "در حال بارگذاری";
            },
            noResults: function () {
                return "نتیجه ای در بر نداشت";
            },
            searching: function () {
                return "در حال جستجو...";
            },
            maximumSelected: function (args) {
                // args.maximum is the maximum number of items the user may select
                return "نتیجه ای در بر نداشت";
            }
        },
        templateResult: formatOutput
    };

    $('.category').select2(opts);

    $("select[name='categories[]'").on('select2:select', function (e) {
        var element = $(this);
        var val = e.params.data.id;

        var alreadySelected = $(element).val();

        var selectedValues = new Array();
        selectedValues = getChilds(element, val);

        if ($(element).find("option[data-parent='" + val + "']").length) {
            selectedValues[selectedValues.length] = val;
            for (var j = 0, len = selectedValues.length; j < len; j++) {
                alreadySelected[alreadySelected.length] = selectedValues[j];
            }
            $(element).val(alreadySelected).change();
            return false;
        }
    });

    $("select[name='categories[]'").on('select2:unselect', function (e) {
        var element = $(this);
        var alreadySelected = $(element).val();
        var val = e.params.data.id;

        var selectedValues = new Array();
        selectedValues = getChilds(element, val);

        console.log(alreadySelected);

        selectedValues[selectedValues.length] = val;
        clearItems(element, selectedValues,alreadySelected);
        $(element).val(clearItems(element, selectedValues,alreadySelected)).change();
    });

    function clearItems(element, selectedElements,alreadySelected) {
        for (var j = 0, len = selectedElements.length; j < len; j++) {
            alreadySelected = jQuery.grep(alreadySelected, function(value) {
                return value != selectedElements[j];
            });
        }
        return alreadySelected;
    }

    function getChilds(element, id) {
        var array = new Array();
        var i = 0;
        $(element).find("option[data-parent='" + id + "']").each(function (key, elem) {
            array[i] = $(elem).val();
            i++;
            if ($(element).find("option[data-parent='" + $(elem).val() + "']")) {
                var array_child = new Array();
                array_child = getChilds(element, $(elem).val());
                if (array_child.length) {
                    for (var j = 0, len = array_child.length; j < len; j++) {
                        array[i] = array_child[j];
                        i++;
                    }
                }
            }
        });
        return array;
    }

    $(".selectAllSelect2").click(function () {
        var element = $(this);
        if ($(element).prop('checked')) {
            $(element).parent().parent().find("select > option").prop("selected", "selected");
            $(element).parent().parent().find("select").trigger("change");
        } else {
            $(element).parent().parent().find("select").val([]);
            $(element).parent().parent().find("select").trigger("change");
        }
    });


    $(".help-block").closest('.form-group').find('.select2-selection').css({
        "border-color": "#FF4961",
        "border-width": "1px",
        "border-style": "solid"
    });

    $(document).on('click', '.show', function () {
        $(".show-permission").slideToggle();
    });
    $('.show').css('cursor', 'pointer');
    $(function () {
        $('.reset').click(function () {
            var element = $(this);
            element.closest("form")[0].reset();
        });
    });

    (function ($) {
        $.fn.popupLoadData = function (options) {
            $this = $(this);
            $this.html("در حال دریافت اطلاعات...");
            jQuery.ajax({
                type: (options.hasOwnProperty('type')) ? options.type : "POST",
                url: options.url,
                dataType: (options.hasOwnProperty('dataType')) ? options.dataType : "text",
                data: (options.hasOwnProperty('data')) ? options.data : {},
                success: function (response) {
                    var result = JSON.parse(response);
                    if (result.status == 100) {
                        $this.html(result.data);
                    }
                },
                error: function (request, status, error) {
                    return this.html("<span class='popup-load-data-error'>" + request.responseText + "</span>");
                }
            });
        };
    }(jQuery));
});

function mdl(heading, formContent, strSubmitFunc, btnText) {
    var elId = crDyElId();
    var html = '<div id="' + elId + '" class="modal hide fade in" style="display:none;"><div class="modal-dialog">';
    html += '<div class="modal-content">';
    html += '<div class="modal-header">';
    html += '<h4 class="modal-title">' + heading + '</h4>';
    html += '<button type="button" class="close" data-dismiss="modal">×</button>';
    html += '</div>'; // header
    html += '<div class="modal-body">';
    html += formContent;
    html += '</div>';// body
    html += '<div class="modal-footer">';
    if (btnText != '') {
        html += '<button class="btn btn-success"';
        html += ' onClick="' + strSubmitFunc + '">' + btnText;
        html += '</button>';
    }
    html += '<button type="button" class="btn btn-danger" data-dismiss="modal">';
    html += 'بستن';
    html += '</button>'; // close button
    html += '</div>';  // footer
    html += '</div>';  // content
    html += '</div>';  // dialog
    html += '</div>';  // modalWindow
    $(".mdl").append(html);
    $("#" + elId).modal();
    return $("#" + elId);
}

$(document).on('focus', ".datePicker", function () {
    $(this).datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        isRTL: true,
        changeMonth: true,
        changeYear: true,
        dateFormat: "yy/mm/dd",
        showAnim: 'slideDown',
        showButtonPanel: true,
        yearRange: "-100:+10",
    });
});

function formatOutput(optionElement) {
    if (!optionElement.id) {
        var state = $(
            '<span class="catOriginal"><i class="fa fa-folder-open"></i> ' + optionElement.text + '</span>'
        );
        return $(state);
        return optionElement.text;
    }
    var state = $(
        '<span>' + optionElement.text + '</span>'
    );
    return $(state);
};

function crDyElId() {
    do {
        var n = randomString(20);
    }
    while ($("#" + n).length != 0);
    return n;
}

function randomString(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

function toEnglishNumber(strNum) {
    var pn = ["۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"];
    var en = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
    var an = ["٠", "١", "٢", "٣", "٤", "٥", "٦", "٧", "٨", "٩"];
    var cache = strNum;
    for (var i = 0; i < 10; i++) {
        var regex_fa = new RegExp(pn[i], 'g');
        var regex_ar = new RegExp(an[i], 'g');
        cache = cache.replace(regex_fa, en[i]);
        cache = cache.replace(regex_ar, en[i]);
    }
    return cache;
}


function copyToClipboard(text) {

    var textArea = document.createElement( "textarea" );
    textArea.value = text;
    document.body.appendChild( textArea );
    textArea.select();

    try {
        var successful = document.execCommand( 'copy' );
    } catch (err) {
        console.log('Oops, unable to copy',err);
    }
    document.body.removeChild( textArea );
}
