<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="apple-touch-icon" sizes="60x60" href="{{ asset('/theme/img/ico/apple-icon-60.html') }}">
<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('/theme/img/ico/apple-icon-76.html') }}">
<link rel="apple-touch-icon" sizes="120x120" href="{{ asset('/theme/img/ico/apple-icon-120.html') }}">
<link rel="apple-touch-icon" sizes="152x152" href="{{ asset('/theme/img/ico/apple-icon-152.html') }}">
<link rel="shortcut icon" type="image/x-icon" href="{{ asset('favicon.ico') }}">
<link rel="shortcut icon" type="image/png" href="{{ asset('favicon.png') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('/theme/fonts/feather/style.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('/theme/fonts/simple-line-icons/style.css') }}">
{{--<link rel="stylesheet" type="text/css" href="{{ asset('/theme/fonts/font-awesome/css/font-awesome.min.css') }}">--}}
<link rel="stylesheet" type="text/css" href="{{ asset('/theme/vendors/css/perfect-scrollbar.min.css') }}">
{{--<link rel="stylesheet" type="text/css" href="{{ asset('/theme/vendors/css/prism.min.css') }}">--}}
<link rel="stylesheet" type="text/css" href="{{ asset('/theme/css/app.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('/css/theme.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('/css/select2.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('/css/toastr.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('/css/switchery.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('/css/datepicker/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('/theme/vendors/css/katex.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('/theme/vendors/css/monokai-sublime.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('/theme/vendors/css/quill.snow.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('/theme/vendors/css/quill.bubble.css') }}">
<link rel="stylesheet" type="text/css" href="{{asset("/theme/vendors/css/sweetalert2.min.css")}}">
<link rel="stylesheet" href="{{asset('/css/menu/all.css')}}">
<link rel="stylesheet" href="{{asset('/css/menu/fontawesome.min.css')}}">
<link rel="stylesheet" href="{{asset('/css/menu/bootstrap-iconpicker.min.css')}}">
