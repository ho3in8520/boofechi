<div class="navbar-container">
    <div id="navbarSupportedContent" class="collapse navbar-collapse">
        <ul class="navbar-nav">

            <li class="dropdown nav-item mt-1 dropdown_basket"
                style="display: {{basketCount() == 0 ? 'none' : 'block' }};"><a
                    id="dropdownBasket" href="#" data-toggle="dropdown"
                    class="nav-link position-relative dropdown-toggle"><i
                        class="ft-basket blue-grey darken-4"></i><span
                        class="notification badge badge-pill white badge-success basket_count">{{basketCount()}}</span>
                    <p class="d-none">سبد خرید</p></a>
                <div class="notification-dropdown dropdown-menu dropdown-menu-left" style="width: 400px">
                    <div class="arrow_box_right" style="padding: 10px">
                        <div class="noti-list">
                            <span>
                                <table style="text-align: center !important;font-size: 12px"
                                       class="table table-striped table-inverse table-hover table-sm table-basket-items-top-menu">
                                  <thead>
                                  <th>#</th>
                                  <th>لوگو</th>
                                  <th>شرکت</th>
                                  <th>حداقل قیمت</th>
                                  <th>تعداد</th>
                                  <th>مبلغ</th>
                                  </thead>
                                    <tbody>
                                        <?php
                                        $items = getBasketItems();
                                        $sum = $count = $i = 0;
                                        if($items)
                                        {
                                        foreach ($items as $item)
                                        {
                                        $i++;
                                        $sum += withoutZeros($item['price']);
                                        $count += $item['cnt'];
                                        ?>
                                                  <tr class='clickable-row' data-href='{{asset('basket-item-details').'/'.$item['fm_id']}}'>
                                                    <td>{{$i}}</td>
                                                    <td><img class="rounded-circle"
                                                             style="width: 30px;height: 30px"
                                                             src="{{ getFile($item['logo']  ? $item['logo'] : 'nologo.png') }}"></td>
                                                    <td><a href="{{asset('basket-item-details').'/'.$item['fm_id']}}"><b>{{$item['coll_name']}}</b></a></td>
                                                    <td>{{number_format($item['min_amount'])}}</td>
                                                    <td>{{$item['cnt']}}</td>
                                                    <td>{{number_format(withoutZeros($item['price']))}}</td>
                                                  </tr>
                                                <?php
                                                }
                                                }
                                                else{
                                                ?>
                                              <tr>
                                                <td colspan="6">سبد خرید شما خالی است</td>
                                              </tr>
                                            <?php
                                            }
                                            ?>
                                </tbody>
                                <tfoot>
                                      <tr>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td><b>جمع</b></td>
                                        <td>{{$count}}</td>
                                        <td>{{number_format($sum)}}</td>
                                      </tr>
                                </tfoot>
                             </table>
                            </span>
                        </div>
                        <a href="{{route('basket-items')}}"
                           class="noti-footer primary text-center d-block border-top border-top-blue-grey border-top-lighten-4 text-bold-400 py-1">نمایش
                            سبد خرید</a>
                    </div>
                </div>
            </li>
            <li class="dropdown nav-item mt-1 delay"><a id="dropdownBasic2" href="#" data-toggle="dropdown"
                                                  class="nav-link position-relative dropdown-toggle"><i
                        class="ft-bell blue-grey darken-4" id="hide-method" data-toggle="tooltip" data-original-title="اطلاعیه" data-delay="500"></i>
                    @if(announcementCount() > 0)
                        <span
                            class="notification badge badge-pill badge-info">{{announcementCount()}}</span>
                    @endif
                    <p class="d-none">اطلاعیه</p></a>
                <div class="notification-dropdown dropdown-menu dropdown-menu-left">
                    <div class="arrow_box_right">
                        <div class="noti-list">
                            @if(!announcementCount()==0)
                                @foreach(getAnnouncementItems() as $announcement)
                                    <a class="dropdown-item noti-container py-2"
                                       href="{{asset("announcement-form".'/'.$announcement->ann_id)}}"><img
                                            src="{{getFile($announcement->logo,'logo')}}"
                                            class="rounded-circle ml-1 ft-save warning float-right" style="height: 40px
                                          ; width: 40px; margin-left: 10px"><span
                                            class="noti-wrapper"><p
                                                class="float-left">{{jdate_from_gregorian($announcement->created_at,"Y/m/d | H:i")}}</p><span
                                                class="noti-title line-height-1 d-block text-bold-400 info">{{$announcement->coll_name}}</span><span
                                                class="noti-text">{{$announcement->ann_title}}</span></span></a>
                                @endforeach
                            @else
                                <a class="dropdown-item noti-container py-2 text-center">موردی برای نمایش یافت نشد!</a>
                            @endif
                        </div>
                        <a class="noti-footer primary text-center d-block border-top border-top-blue-grey border-top-lighten-4 text-bold-400 py-1"
                           href="{{route("announcement-list")}}">لیست
                            همه اعلان ها</a>
                    </div>
                </div>
            </li>
            @can('icon_massage')
                <li class="dropdown nav-item mt-1"><a id="dropdownBasic3" href="#" data-toggle="dropdown"
                                                      class="nav-link position-relative dropdown-toggle"><i
                            class="icon-envelope blue-grey darken-4" id="hide-method" data-toggle="tooltip" data-original-title="پیام ها" data-delay="500"></i>
                        @if(messages() > 0)
                            <span class="notification badge badge-pill badge-danger">{{messages()}}</span>
                        @endif
                        <p class="d-none">اطلاعیه</p></a>
                    <div class="notification-dropdown dropdown-menu dropdown-menu-left">
                        <div class="arrow_box_right">
                            <div class="noti-list">
                                @if(!messages()==0)
                                    @foreach(messagesItems() as $messagesItems)
                                        <a class="dropdown-item noti-container py-2"><span
                                                class="noti-wrapper"><p
                                                    class="float-left">{{jdate_from_gregorian($messagesItems->msg_created_at,"Y/d/m")}}</p><span
                                                    class="noti-title line-height-1 d-block text-bold-400 info">{{$messagesItems->coll_name}}</span></span></a>
                                    @endforeach
                                @else
                                    <a class="dropdown-item noti-container py-2 text-center">موردی برای نمایش یافت
                                        نشد!</a>
                                @endif
                            </div>
                            <a class="noti-footer primary text-center d-block border-top border-top-blue-grey border-top-lighten-4 text-bold-400 py-1"
                               href="{{route("list-massage")}}">لیست
                                همه پیام ها</a>
                        </div>
                    </div>
                </li>
            @endcan

            @can('icon_ticket')

                <a href="{{asset('ticket')}}">  <li class="dropdown nav-item mt-1"> <span id="dropdownBasic3"
                                                      class="nav-link position-relative"><i
                                class="icon-speech blue-grey darken-4" id="hide-method" data-toggle="tooltip" data-original-title="تیکت ها" data-delay="500"></i></span></a>
@if(countNewTicket()>0)
                        <span class="notification badge badge-pill badge-danger">{{countNewTicket()}}</span>
@endif
                        <p class="d-none">تیکت ها</p>

                </li>
            @endcan
            @can("checkout_request_icon")
            <a href="{{asset('/admin/checkout-request-list')}}">  <li class="dropdown nav-item mt-1"> <span id="dropdownBasic3"
                                                                                   class="nav-link position-relative"><i
                            class="fa fa-dollar-sign blue-grey darken-4" id="hide-method" data-toggle="tooltip" data-original-title="درخواست های تسویه" data-delay="500"></i></span>
                    @if(countNewCheckoutRequest()>0)
                        <span class="notification badge badge-pill badge-danger">{{countNewCheckoutRequest()}}</span>
                    @endif
                    <p class="d-none">درخواست های تسویه</p></a>
            </li>
            @endcan
            @can('icon_factor')
                @if(user('panel_type')!=39)
              <a href="{{asset('/input-factors')}}">  <li class="dropdown nav-item mt-1"><span id="dropdownBasic3"
                                                      class="nav-link position-relative"><i
                              class="icon-doc blue-grey darken-4" id="hide-method" data-toggle="tooltip" data-original-title="فاکتور ها" data-delay="500"></i></span></a>
                        @if(countNewFactor() > 0)
                            <span class="notification badge badge-pill badge-danger">{{countNewFactor()}}</span>
                        @endif
                        <p class="d-none">فاکتور ها</p></a>
                    <div class="notification-dropdown dropdown-menu dropdown-menu-left">
                    </div>
                </li></a>
                    @endif
            @endcan
            @can('icon_user_new')
                <li class="dropdown nav-item mt-1"><a id="dropdownBasic3" href="#" data-toggle="dropdown"
                                                      class="nav-link position-relative dropdown-toggle"><i
                            class="icon-user-follow blue-grey darken-4" id="hide-method" data-toggle="tooltip" data-original-title="کاربران جدید" data-delay="500"></i>
                        @if(newUserCount() > 0)
                            <span class="notification badge badge-pill badge-danger">{{newUserCount()}}</span>
                        @endif
                        <p class="d-none">کاربران جدیدا</p></a>
                    <div class="notification-dropdown dropdown-menu dropdown-menu-left">
                        <div class="arrow_box_right">
                            @if(!newUserCount()==0)
                                @foreach(getNewUserItems() as $newUser)
                                    <div class="noti-list">
                                        <a class="dropdown-item noti-container py-2"
                                           href="{{route('accept-registration')}}"><span
                                                class="noti-wrapper"><p
                                                    class="float-left">{{jdate_from_gregorian($newUser->created_at,'Y/m/d')}}</p><span
                                                    class="noti-title line-height-1 d-block text-bold-400 info">{{(empty(!$newUser->prsn_name)) ?$newUser->prsn_name.' '.$newUser->prsn_family:' ثبت نام تکمیل نشده است'.('('.$newUser->username.')')}}</span></span></a>

                                    </div>
                                @endforeach
                            @else
                                <a class="dropdown-item noti-container py-2 text-center">موردی برای نمایش یافت
                                    نشد!</a>
                            @endif
                            <a class="noti-footer primary text-center d-block border-top border-top-blue-grey border-top-lighten-4 text-bold-400 py-1"
                               href="{{route("accept-registration")}}">لیست
                                همه کاربران جدید</a>
                        </div>
                    </div>
                </li>
            @endcan


            <li class="dropdown nav-item mr-0"><a id="dropdownBasic3" href="#" data-toggle="dropdown"
                                                  class="nav-link position-relative dropdown-user-link dropdown-toggle"><span
                        class="avatar avatar-online"><img id="navbar-avatar"
                                                          src="{{getFile(getAvatar(),'avatar')}}"
                                                          alt="avatar"/></span> {{user('username')}}
                    <p class="d-none">تنظیمات کاربر</p></a>
                <div aria-labelledby="dropdownBasic3" class="dropdown-menu dropdown-menu-left">
                    <div class="arrow_box_right">


                        <a class="dropdown-item wallet text-white" style="background-color: orange">
                            <span>اعتبار : {{number_format(WithoutZeros(user("amount")))}}</span></a>
                        @if(user('amount')>permitted_amout())
                        <a class="dropdown-item py-1 text-white primary checkout"
                              style="background-color: #66bb6a;text-align: center">
                            <span>تسویه کیف پول</span></a>
                        @endif
                        <a href="{{asset("edit-profile")}}" class="dropdown-item py-1"><i class="ft-edit ml-2"></i><span>پروفایل من</span></a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            <i class="fa fa-power-off"></i>
                            <span>خروج</span>
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>
