<ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
    <?php
    $path = "../menu-config" . "/" . "menu.json";
    $menus = [];
    if (file_exists($path)) {
        $menus = json_decode(file_get_contents($path), true);
        getMenuLinks($menus);
    }
    ?>
</ul>