<script src="{{ asset('/theme/vendors/js/core/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('/theme/vendors/js/core/popper.min.js') }}"></script>
<script src="{{ asset('/theme/vendors/js/core/bootstrap.min.js') }}"></script>
<script src="{{ asset('/theme/vendors/js/perfect-scrollbar.jquery.min.js') }}"></script>
<script src="{{ asset('/theme/vendors/js/prism.min.js') }}"></script>
<script src="{{ asset('/theme/vendors/js/jquery.matchHeight-min.js') }}"></script>
<script src="{{ asset('/theme/vendors/js/screenfull.min.js') }}"></script>
<script src="{{ asset('/theme/vendors/js/pace/pace.min.js') }}"></script>
<script src="{{ asset('/theme/js/app-sidebar.js') }}"></script>
<script src="{{ asset('/theme/js/notification-sidebar.js') }}"></script>
<script src="{{ asset('/theme/js/customizer.js') }}"></script>
<script src="{{ asset('/js/select2.full.min.js') }}"></script>
<script src="{{ asset('/js/functions/numberFormat.js') }}"></script>
<script src="{{ asset('/js/datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('/js/datepicker/bootstrap-datepicker.fa.min.js') }}"></script>
<script src="{{ asset('/js/functions/popup.js') }}"></script>
<script src="{{ asset('/js/toastr.min.js') }}"></script>
<script src="{{ asset('/js/switchery.min.js') }}"></script>
<script src="{{asset('/js/inputmask/inputmask.js')}}"></script>
<script src="{{asset('/js/inputmask/inputmask.extension.js')}}"></script>
<script src="{{asset('/js/inputmask/inputmask.numeric.js')}}"></script>
<script src="{{asset('/js/inputmask/jquery.inputmask.js')}}"></script>
<script src="{{ asset('/js/project.js') }}"></script>
<script src="{{ asset('/js/functions/printContent.js') }}"></script>
{{--<script src="{{ asset('/theme/js/chartjs.js') }}"></script>--}}
{{--<script src="{{ asset('/theme/vendors/js/chart.min.js') }}"></script>--}}
<script src="{{ asset('/theme/js/components-modal.min.js') }}"></script>
<script src="{{asset("/theme/vendors/js/sweetalert2.min.js")}}"></script>
<script src="{{asset('theme/js/tooltip.js')}}"></script>
<script src="https://panel.boofechi.com/js/menu/fontawesome5-3-1.min.js"></script>

<script>
    $(document).on("click", ".wallet", function () {
        var element = $(this);
       var amount=$(element).closest("button").data("amount");
       var factor_id=$(element).data("factorid");
       if(!amount){
           amount=''
       }
        swal({
            title: "شارژ کیف پول",
            text: "مبلغ را وارد کنید:",
            input: "text",
            inputValue: amount,
            showCancelButton: true,
            animation: "slide-from-top",
            confirmButtonText: 'ثبت',
            cancelButtonText: "انصراف"
        }).then(function (inputValue) {
            if (inputValue === false) return false;
            var data = new Array();
            data.push({
                name:'amount',
                value: inputValue
            });
            if(factor_id > 0) {
                data.push({
                    name: 'factor_id',
                    value: factor_id
                });
            }
            var r = confirm(" اطمینان از پرداخت مبلغ " + numberFormat(inputValue) + " دارید؟ ");
            if (r == true) {
                $.post("{{asset('/charging-wallet')}}", data, function (data) {
                    var result = JSON.parse(data);
                    if (result.status == 100) {
                        toastr.success(result.msg, "موفق");
                        setTimeout(function () {
                            location.replace(result.url);
                        }, 2000);
                    } else if (result.status == 500){
                        toastr.error(result.msg, "خطا");
                    }
                });
            }
        }).done();
    });

@if(!empty(check_set_sheba()->prsn_sheba))
    $(document).on("click", ".checkout", function () {
        var element = $(this);
        var amount=$(element).closest("button").data("amount");
        swal({
            title: "تسویه کیف پول",
            text: "مبلغ را وارد کنید:",
            input: "text",
            showCancelButton: true,
            animation: "slide-from-top",
            confirmButtonText: 'ثبت',
            cancelButtonText: "انصراف",
            html:'<h6 style="color: green">موجودی کیف پول شما : {{number_format(user('amount'))}}</h6>' +
                '<p style="color:red">توجه داشته باشید مبلغ {{number_format(permitted_amout())}} باید در کیف پولتان باشد</p>'
        }).then(function (inputValue) {
            if (inputValue === false) return false;
                $.post("{{asset('/checkout-permitted')}}", {amount: inputValue}, function (data) {
                    var result = JSON.parse(data);
                    if (result.status == 100) {
                        toastr.success(result.msg, "موفق");
                        setTimeout(function () {
                            location.replace(result.url);
                        }, 2000);
                    } else if (result.status == 500){
                        toastr.error(result.msg, "خطا");
                    }
                });
        }).done();
    });
    @else
    $(document).on("click", ".checkout", function () {
        var element = $(this);
        swal({
            title: 'شماره شبا',
            text: "شماره شبا شما در سامانه ثبت نشده است لطفا از طریق دکمه زیر شماره شبا خود را ثبت کنید",
            icon: 'warning',
            showCancelButton: true,
            cancelButtonText: "انصراف",
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ثبت شماره شبا'
        }).then((confirmButtonText) => {
            if (confirmButtonText) {
                window.location.href = 'edit-profile';
            }
        }).done();
    });
    @endif

    $(document).ready(function($) {
        $(".clickable-row").click(function() {
            window.location = $(this).data("href");
        });
    });
</script>

