<table>
    <thead>
    <tr>
        <th colspan="<?=count($headers)?>">{{$title}}</th>
    </tr>
    <tr>
        @foreach($headers as $header)
            <th>{{$header}}</th>
        @endforeach
    </tr>
    </thead>
    <tbody>
    @if(count($records))
        @foreach($records as $record)
            <tr>
                @foreach($record as $item)
                    <td>{{ $item }}</td>
                @endforeach
            </tr>
        @endforeach
    @else
        <tr>
            <td colspan="<?=count($headers)?>" style="text-align: center;">
                {{ config('first_config.message.empty_table') }}
            </td>
        </tr>
    @endif
    </tbody>
</table>