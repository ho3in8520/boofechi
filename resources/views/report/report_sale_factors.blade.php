@extends('master_page')
<?php
$title = "گزارش فروش فاکتورها";
?>
@section('title_browser',$title)
@section('style')@endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header"><?=$title?></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form method="GET" action="{{url()->current()}}">
                        <div class="card-body">
                            <div class="card">
                                <div class="col-md-12">
                                    <div class="card-body">
                                        @include('report.filter_form')
                                    </div>
                                </div>
                                <hr>
                                <div class="col-md-12">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>نوع خروجی</label>
                                                    <select class="form-control" name="export-type">
                                                        <option value="print">پرینت</option>
                                                        <option value="excel">اکسل</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <button  type="submit" data-action="{{url()->current()}}" class="btn btn-print btn-success btn-do-action btn-block"
                                                   style="margin-top:32px">انجام
                                                    عملیات</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            $("select[name=export-type]").change(function () {
                if($(this).val() == "print")
                {
                    $(".btn-do-action").addClass("btn-print");
                }
                else {
                    $(".btn-do-action").removeClass("btn-print");
                }
            });
        });
    </script>
@endsection