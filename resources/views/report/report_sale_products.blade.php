@extends('master_page')
<?php
$title = "گزارش فروش محصولات";
?>
@section('title_browser',$title)
@section('style')@endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header"><?=$title?></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form method="GET" action="{{url()->current()}}">
                        <div class="card-body">
                            <div class="card">
                                <div class="col-md-12">
                                    <div class="card-body">
                                        @include('report.filter_form')
                                    </div>
                                </div>
                                <hr>
                                <div class="col-md-12">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label>نوع گزارش</label>
                                                    <select class="form-control" name="report-type">
                                                        <option value="totally">سرجمع</option>
                                                        <option value="by-factor">به تفکیک فاکتور</option>
                                                        <option value="by-date">به تفکیک تاریخ</option>
                                                        @if(in_array(user("panel_type"),[39,44]))
                                                            <option value="by-seller">به تفکیک فروشنده</option>
                                                        @endif
                                                        @if(in_array(user("panel_type"),[39,41, 42, 43]))
                                                            <option value="by-buyer">به تفکیک خریدار</option>
                                                        @endif
                                                        @if(user("panel_type") != 44)
                                                            <option value="by-state">به تفکیک استان</option>
                                                            <option value="by-city">به تفکیک شهر</option>
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>نوع خروجی</label>
                                                    <select class="form-control" name="export-type">
                                                        <option value="print">پرینت</option>
                                                        <option value="excel">اکسل</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <button  type="submit" data-action="{{url()->current()}}" class="btn btn-print btn-success btn-do-action btn-block"
                                                         style="margin-top:32px">انجام
                                                    عملیات</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            $("select[name=export-type]").change(function () {
                if($(this).val() == "print")
                {
                    $(".btn-do-action").addClass("btn-print");
                }
                else {
                    $(".btn-do-action").removeClass("btn-print");
                }
            });
        });
    </script>
@endsection