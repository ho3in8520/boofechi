<?php
$states = getAreaSupport(1);
$cities = getAreaSupport(2, null, null, request('state', null));
$seller_collections = [];
if (user("panel_type") == 39) {//admin
    $seller_collections = DB::table('users')
        ->join('persons', 'prsn_user_id', 'user_id')
        ->join('collection_persons', 'cp_prsn_id', 'prsn_id')
        ->join('collections', 'cp_coll_id', 'coll_id')
        ->select([
            'coll_id',
            'coll_name'
        ])->whereIn('panel_type', [41, 42, 43])->get()->toArray();
} else if (user("panel_type") == 44) {//maghazedar
    $seller_collections = DB::table('users')
        ->join('persons', 'prsn_user_id', 'user_id')
        ->join('collection_persons', 'cp_prsn_id', 'prsn_id')
        ->join('collections', 'cp_coll_id', 'coll_id')
        ->join('factor_master', 'fm_collection_id', 'coll_id')
        ->select([
            'coll_id',
            'coll_name'
        ])
        ->where('fm_self_collection_id', collection_id())
        ->where('fm_mode', 1)
        ->distinct('coll_id')
        ->get()->toArray();
}
$seller_collections = json_decode(json_encode($seller_collections), true);

$products = [];
if (in_array(user("panel_type"), [41, 42, 43])) {
    $products = DB::table('factor_master')
        ->join('factor_detail', 'fm_id', 'fd_master_id')
        ->join('collection_products', 'cp_id', 'fd_product_id')
        ->join('products', 'prod_id', 'cp_product_id')
        ->select([
            'cp_id',
            'prod_name'
        ])
        ->where('fm_collection_id', collection_id())
        ->where('fm_mode', 1)
        ->distinct('coll_id')
        ->get()->toArray();
} else if (user("panel_type") == 44) {//maghazedar
    $products = DB::table('factor_master')
        ->join('factor_detail', 'fm_id', 'fd_master_id')
        ->join('collection_products', 'cp_id', 'fd_product_id')
        ->join('products', 'prod_id', 'cp_product_id')
        ->select([
            'cp_id',
            'prod_name'
        ])
        ->where('fm_self_collection_id', collection_id())
        ->where('fm_mode', 1)
        ->distinct('coll_id')
        ->get()->toArray();
}

$products = json_decode(json_encode($products), true);

$buyer_collections = [];
if (user("panel_type") == 39) {//admin
    $buyer_collections = DB::table('users')
        ->join('persons', 'prsn_user_id', 'user_id')
        ->join('collection_persons', 'cp_prsn_id', 'prsn_id')
        ->join('collections', 'cp_coll_id', 'coll_id')
        ->select([
            'coll_id',
            'coll_name'
        ])->whereIn('panel_type', [44])->get()->toArray();
} else if (in_array(user("panel_type"), [40, 41, 42, 43])) {//sherkat or karkhane or omdeforosh
    $buyer_collections = DB::table('users')
        ->join('persons', 'prsn_user_id', 'user_id')
        ->join('collection_persons', 'cp_prsn_id', 'prsn_id')
        ->join('collections', 'cp_coll_id', 'coll_id')
        ->join('factor_master', 'fm_self_collection_id', 'coll_id')
        ->select([
            'coll_id',
            'coll_name'
        ])
        ->where('fm_collection_id', collection_id())
        ->where('fm_mode', 1)
        ->distinct('coll_id')
        ->get()->toArray();
}
$buyer_collections = json_decode(json_encode($buyer_collections), true);
?>
<div class="row">
    <div class="col-sm-3">
        <div class="form-group">
            <label>شماره فاکتور</label>
            <input type="text" name="fm_no" class="form-control"
                   value="{{request()->query('fm_no')}}">
        </div>
    </div>
    <div class="col-sm-3">
        <label>تاریخ ثبت فاکتور</label>
        <div class="input-group">
            <input type="text" name="fm_date_from" class="form-control datePicker"
                   value="{{request()->query('fm_date_from')}}" placeholder="از">
            <input type="text" name="fm_date_to" class="form-control datePicker"
                   value="{{request()->query('fm_date_to')}}" placeholder="تا">
        </div>
    </div>
    @if(!in_array(user('panel_type'),[39]) && \Request::route()->getName() == "report-sale-products")
        <div class="col-sm-3">
            <div class="form-group">
                <label>محصول</label>
                <select name="product_id" class="form-control state">
                    {!!customForeach($products, "cp_id", "prod_name",request()->query('product_id')) !!}
                </select>
            </div>
        </div>
    @endif
    @if(in_array(user('panel_type'),[39,44]))
        <div class="col-sm-3">
            <div class="form-group">
                <label>فروشنده</label>
                <select name="fm_collection_id" class="form-control state">
                    {!!customForeach($seller_collections, "coll_id", "coll_name",request()->query('fm_collection_id')) !!}
                </select>
            </div>
        </div>
    @endif
    @if(!in_array(user('panel_type'),[44]))
        <div class="col-sm-3">
            <div class="form-group">
                <label>خریدار</label>
                <select name="fm_self_collection_id" class="form-control state">
                    {!!customForeach($buyer_collections, "coll_id", "coll_name",request()->query('fm_self_collection_id')) !!}
                </select>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group">
                <label>استان</label>
                <select name="state" class="form-control state">
                    {!!customForeach($states, "c_id", "c_name",request()->query('state')) !!}
                </select>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group">
                <label>شهر</label>
                <select name="city" class="form-control city">
                    {!!customForeach($cities, "c_id", "c_name",request()->query('city'))!!}
                </select>
            </div>
        </div>
        @if(\Request::route()->getName() == "report-sale-factors")
            <div class="col-sm-3">
                <div class="form-group">
                    <label>توضیحات</label>
                    <textarea class="form-control" name="fm_description">
                    {{request()->query('fm_description')}}
                </textarea>
                </div>
            </div>
        @endif
    @endif
</div>