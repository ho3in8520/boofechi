@extends('login')
@section('title','فراموشی رمز عبور')
@section('title_header','فراموشی رمز عبور')
@section('content')
    <form action="" method="post">
        @csrf
        <div class="form-group">
            <h6 class="info" id="alert-text" style="display: none; text-align: center">ارسال مجدد بعد از گذشت زمان</h6>
            <div class="col-md-12">
                <input type="text" id="phone" class="form-control form-control-lg"
                       name="mobile" value="" placeholder="شماره تماس" autofocus>
                <div class='help-block text-danger'></div>
            </div>
        </div>
        <h5 style="text-align: center"></h5>
        <a id="forgetPassword" class="icon">
            <div id="test" style="text-align: center; margin: 10px" data-toggle="tooltip" data-placement="top"
                 title="ارسال مجدد رمز عبور"></div>
        </a>
        <div class="form-group">
            <div class="text-center col-md-12">
                <button type="button" id="forgetPassword"
                        class="btn btn-danger px-4 py-2 text-uppercase white font-small-4 box-shadow-2 border-0 btn-block">
                    ارسال رمزعبور
                </button>
            </div>
        </div>
    </form>
    <div class="card-footer grey darken-1">
        <div class="text-center"><a href="{{asset('login')}}"><b>ورود</b></a></div>
        <div class="text-center">آیا حساب کاربری ندارید؟ <a href="{{asset('register')}}"><b>ثبت نام</b></a></div>
    </div>
@endsection
@section('script')
    <script src="{{ asset('/js/functions/storeAjax.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('#phone').change(function () {
                $(".help-block").html("");
                $('span.error-mobile').remove();
                $("#phone").css({'border-color': '#A6A9AE'});
                var inputVal = $(this).val();
                var characterReg = /^(0)?9\d{9}$/;
                if (!characterReg.test(inputVal)) {
                    $(this).after('<span class="danger error-mobile">فرمت شماره موبایل صحیح نیست</span>');
                    $("#phone").css({'border-color': '#bc0005'});

                }
            });
            $(document).on('click', '#forgetPassword', function (e) {
                $.ajax({
                    url: "{{asset('forget-password')}}",
                    type: "POST",
                    data: {
                        mobile: $("#phone").val(),
                        _token: '{{ csrf_token() }}'

                    },
                    success: function (response) {
                        var result = JSON.parse(response);

                        if (result.status == 100) {
                            $(".help-block").html("");
                            toastr.success(result.msg, "پیامک حاوی رمز عبور جدید برای شما ارسال گردید");
                            var minutes = 1;
                            var seconds = minutes * result.time;

                            function convertIntToTime(num) {
                                var mins = Math.floor(num / 60);
                                var secs = num % 60;
                                var timerOutput = (mins < 10 ? "0" : "") + mins + ":" + (secs < 10 ? "0" : "") + secs;
                                return (timerOutput);
                            }

                            var countdown = setInterval(function () {
                                var current = convertIntToTime(seconds);
                                $("#phone").removeAttr("style").hide();
                                $(".btn-block").remove();
                                $('#alert-text').show();
                                $('#test').hide();
                                $('h5').show();
                                $('h5').html(current);

                                if (seconds == 0) {
                                    clearInterval(countdown);
                                }
                                seconds--;
                                if (seconds >= 0) {
                                } else {
                                    $('h5').hide();
                                    $('#test').show();
                                    $('#test').html('<span class="ft-refresh-ccw"</span>');
                                }
                            }, 1000);

                            $(function () {
                                $('[data-toggle="tooltip"]').tooltip()
                            });
                            setTimeout(function () {
                            });
                        }
                        if (result.status == 210) {

                            toastr.error("رمز عبور جدید قبلا برای شما ارسال شده");
                            var minutes = 1;
                            var seconds = minutes * result.time;

                            function convertIntToTime(num) {
                                var mins = Math.floor(num / 60);
                                var secs = num % 60;
                                var timerOutput = (mins < 10 ? "0" : "") + mins + ":" + (secs < 10 ? "0" : "") + secs;
                                return (timerOutput);
                            }

                            var countdown = setInterval(function () {
                                var current = convertIntToTime(seconds);
                                $("#phone").removeAttr("style").hide();
                                $(".btn-block").removeAttr("style").hide();
                                $('#alert-text').show();
                                $('#test').hide();
                                $('h5').show();
                                $('h5').html(current);

                                if (seconds == 0) {
                                    clearInterval(countdown);
                                }
                                seconds--;
                                if (seconds >= 0) {
                                } else {
                                    $('h5').hide();
                                    $('#test').show();
                                    $('#test').html('<span class="ft-refresh-ccw"</span>');
                                }
                            }, 1000);

                            $(function () {
                                $('[data-toggle="tooltip"]').tooltip()
                            });
                            setTimeout(function () {
                            });
                        }

                        if (result.status == 220) {
                            toastr.error(result.msg);
                            setTimeout(function () {
                                $("#phone").css({'border-color': '#bc0005'});
                                $(".help-block").html("کاربری با این شماره یافت نشد");

                            });
                        }

                        if (result.status == 422) {
                            toastr.success(result.message);
                        }
                    },
                    error: function (response) {
                        setTimeout(function () {
                            $("#phone").css({'border-color': '#bc0005'});
                            if ($("#phone").val() == '') {
                                $('span.error-mobile').remove();
                                toastr.error("فیلد شماره تماس الزامی است");
                                $(".help-block").html("فیلد شماره تماس الزامی است");
                            } else {
                                toastr.error("فرمت شماره موبایل صحیح نیست");
                            }
                        });
                    }
                });
            });
        });
    </script>
@endsection
