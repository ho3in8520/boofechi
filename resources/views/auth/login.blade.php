@extends('login')
@section('title','ورود به سامانه')
@section('title_header','ورود به سامانه')
@section('content')
    <form action="{{ route('login') }}" method="POST">
        @csrf
        @php
            if (session()->has("active")) {
                $message = session()->get("active");
                echo "<div class='alert alert-danger text-white'><strong></strong>{$message}</div>";
            }
        @endphp
        <div class="form-group">
            <div class="col-md-12">
                <input type="username" class="form-control form-control-lg "
                       name="username" value="{{ old('username') }}" id="inputEmail" placeholder="نام کاربری"
                       autocomplete="off" autofocus>

                @if($errors->first('username'))
                    <div class="help-block text-danger">
                        {{ $errors->first('username') }}
                    </div>
                @endif
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-12">
                <input type="password" class="form-control form-control-lg" name="password"
                       id="inputPass" placeholder="رمز عبور">

                @if($errors->first('password'))
                    <div class="help-block text-danger">
                        {{ $errors->first('password') }}
                    </div>
                @endif
            </div>
        </div>
        <!--<div class="form-group">-->
        <!--    <div class="col-md-12">-->
        <!--        <div class="g-recaptcha" data-sitekey="6LccxuYUAAAAADXeFtN88T0fDU2iwMU0Dt4hte4C"></div>-->
        <!--        @if($errors->first('g-recaptcha-response'))-->
        <!--            <div class="help-block text-danger">-->
        <!--                {{ $errors->first('g-recaptcha-response') }}-->
        <!--            </div>-->
        <!--        @endif-->
        <!--    </div>-->
        <!--</div>-->
        <div class="text-center col-md-12">
                <button type="submit"
                        class="btn btn-loading btn-danger px-4 py-2 text-uppercase white font-small-4 box-shadow-2 border-0 btn-block">
                    ورود به پنل
                </button>
            </div>
        </div>
{{--        <div class="form-group">--}}
{{--            <div class="row">--}}
{{--                <div class="col-md-12">--}}
{{--                    <div class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0 ml-5">--}}
{{--                        <input name="remeberme" type="checkbox" class="custom-control-input" checked--}}
{{--                               id="rememberme">--}}
{{--                        <label class="custom-control-label" for="rememberme">مرا--}}
{{--                            به خاطر بسپار</label>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
    </form>
    <div class="card-footer grey darken-1">
        <div class="text-center mb-1">رمز عبور را فراموش کرده اید؟ <a href="{{asset('forget-password')}}"><b>بازیابی</b></a>
        </div>
        <div class="text-center">آیا حساب کاربری ندارید؟ <a href="{{asset('register')}}"><b>ثبت نام</b></a></div>
    </div>
@endsection
@section("script")

    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script type="text/javascript">
        var onloadCallback = function() {
            alert("grecaptcha is ready!");
        };
    </script>

@endsection
