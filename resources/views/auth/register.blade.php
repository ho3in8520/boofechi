@extends('login')
@section('title','ثبت نام در سامانه')
@section('title_header','ثبت نام در سامانه')
@section('content')
    <form action="" method="post">
        @csrf
        <div class="form-group">
            <div class="col-md-12">
                <input type="username" class="form-control form-control-lg english"
                       name="username" value="" id="inputEmail" placeholder="نام کاربری"
                       autocomplete="off" autofocus>
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-12">
                <input type="email" class="form-control form-control-lg" chars="email" name="email"
                       placeholder="ایمیل">
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-12">
                <input type="text" class="form-control form-control-lg" name="mobile" chars="mobile"
                       id="inputPass" placeholder="شماره تماس">
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-12">
                <input type="text" class="form-control form-control-lg" name="reagent" chars="nationalcode"
                       id="inputPass" placeholder="کد ملی معرف">
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-12">
                <fieldset class="form-group position-relative">
                    <select class="form-control form-control-xl" id="xLargeSelect" name="class">
                        <option value="1">مغازه دار</option>
                        <option value="2">ویزیتور</option>
                    </select>
                </fieldset>
            </div>
        </div>
        <div class="form-group">
            <div class="text-center col-md-12">
                <button id="register" type="button"
                        class="btn btn-danger px-4 py-2 text-uppercase white font-small-4 box-shadow-2 border-0 btn-block btn-loading">
                    ثبت نام
                </button>
            </div>
        </div>
    </form>
    <div class="card-footer grey darken-1">
        <div class="text-center">آیا حساب کاربری دارید؟ <a href="{{asset('login')}}"><b>ورود</b></a></div>
    </div>
@endsection
@section('script')
    <script src="{{ asset('/js/functions/storeAjax.js') }}"></script>
    <script>
        $(document).ready(function () {
            $("#register").click(function (e) {
                e.preventDefault();
                storeAjax($(this), 'POST', captionButton = "ثبت نام", false, 0);
            });
        });

    </script>
@endsection
