@extends('master_page')

@section('title_browser') عمده فروش @endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header">عمده فروش</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title-wrap bar-success">
                            
                        </div>
                        <p class="mb-0"></p>
                    </div>
                    <div class="card-body">
                        <div class="card-block">
                            <form action='{{ (empty($collections)) ? route('wholesaler.store') : asset('wholesaler/'.$collections->coll_id) }}'
                                  method='POST' enctype="multipart/form-data">
                                {{ csrf_field() }}
                                {!! (!empty($collections)) ? "<input name='_method' type='hidden' value='PATCH'>" : ''  !!}
                                @include('sample.sample_form_collection')
                                <div class='row'>
                                    <div class='col-md-12'>
                                        <div class='form-group'>
                                            <label></label>
                                            <button class='btn btn-success btn-loading'>
                                            ثبت
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection