<div class="row">
    <div class="col-xl-4 col-lg-6 col-md-6 col-12">
        <div class="card bg-white">
            <div class="card-body">
                <div class="card-block pt-2 pb-0">
                    <div class="media">
                        <div class="media-body white text-left">
                            <h4 class="font-medium-5 card-title mb-0">{{number_format($sum_year[0]['sum_sale']).' '.setting("financial_unit")}}</h4>
                            <span class="grey darken-1">مجموع خرید در سال جاری</span>
                        </div>
                        <div class="media-right text-right">
                            <i class="icon-cup font-large-1 primary"></i>
                        </div>
                    </div>
                </div>
                <div id="Widget-line-chart" class="height-150 lineChartWidget WidgetlineChart mb-2">
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-4 col-lg-6 col-md-6 col-12">
        <div class="card bg-white">
            <div class="card-body">
                <div class="card-block pt-2 pb-0">
                    <div class="media">
                        <div class="media-body white text-left">
                            <h4 class="font-medium-5 card-title mb-0">{{number_format($sum_month[0]['sum_sale']).' '.setting("financial_unit")}}</h4>
                            <span class="grey darken-1">مجموع خرید در ماه جاری</span>
                        </div>
                        <div class="media-right text-right">
                            <i class="icon-wallet font-large-1 warning"></i>
                        </div>
                    </div>
                </div>
                <div id="Widget-line-chart1" class="height-150 lineChartWidget WidgetlineChart1 mb-2">
                </div>

            </div>
        </div>
    </div>
    <div class="col-xl-4 col-lg-6 col-md-6 col-12">
        <div class="card bg-white">
            <div class="card-body">
                <div class="card-block pt-2 pb-0">
                    <div class="media">
                        <div class="media-body white text-left">
                            <h4 class="font-medium-5 card-title mb-0">{{number_format($sum_week[0]['sum_sale']).' '.setting("financial_unit")}}</h4>
                            <span class="grey darken-1">مجموع خرید در هفته جاری</span>
                        </div>
                        <div class="media-right text-right">
                            <i class="icon-basket-loaded font-large-1 success"></i>
                        </div>
                    </div>
                </div>
                <div id="Widget-line-chart2" class="height-150 lineChartWidget WidgetlineChart2 mb-2">
                </div>
            </div>
        </div>
    </div>
</div>
<!--Statistics cards Ends-->

<!--Line with Area Chart 1 Starts-->
<div class="row match-height">
    <div class="col-xl-8 col-lg-12 col-12">
        <div class="card">
            <div class="card-header">
                <div class="card-title-wrap bar-success">
                    <h4 class="card-title">خرید سال</h4>
                </div>
            </div>
            <div class="card-body">
                <div class="card-block">
                    <div id="line-area-dashboard" class="height-300 lineArea1 lineArea1Shadow">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-4 col-lg-12 col-12">
        <div class="card">
            <div class="card-header">
                <div class="card-title-wrap bar-danger">
                    <h4 class="card-title">فعالیت های امروز</h4>
                </div>
            </div>
            <div class="card-body">
                <div class="col-12 timeline-left" id="activity">
                    <div class="timeline">
                        <ul class="list-unstyled base-timeline activity-timeline">
                            <li class="">
                                <div class="timeline-icon bg-dark">
                                    {{$today_activities[0]['sadereh']}}
                                </div>
                                <div class="act-time">امروز</div>
                                <div class="base-timeline-info">
                                    <a href="#" class="text-uppercase text-dark">فاکتور های صادره</a>
                                </div>
                                <small class="text-muted">
                                     
                                </small>
                            </li>
                            <li class="">
                                <div class="timeline-icon bg-primary">
                                    {{$today_activities[0]['companies']}}
                                </div>
                                <div class="act-time">امروز</div>
                                <div class="base-timeline-info">
                                    <a href="#" class="text-uppercase text-primary">تعداد شرکت های سفارش کالا</a>
                                </div>
                                <small class="text-muted">
                                     
                                </small>
                            </li>
                            <li class="">
                                <div class="timeline-icon bg-success">
                                    {{$today_activities[0]['not_payed']}}
                                </div>
                                <div class="act-time">امروز</div>
                                <div class="base-timeline-info">
                                    <a href="#" class="text-uppercase text-success">فاکتور های پرداخت شده</a>
                                </div>
                                <small class="text-muted">
                                     
                                </small>
                            </li>
                            <li class="">
                                <div class="timeline-icon bg-danger">
                                    {{$today_activities[0]['payed']}}
                                </div>
                                <div class="act-time">امروز</div>
                                <div class="base-timeline-info">
                                    <a href="#" class="text-uppercase text-danger">فاکتور های پرداخت نشده</a>
                                </div>
                                <small class="text-muted">
                                     
                                </small>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Line with Area Chart 1 Ends-->
<div class="row match-height">
    <div class="col-xl-4 col-lg-12 col-12">
        <div class="card">
            <div class="card-header">
                <div class="card-title-wrap bar-warning">
                    <h4 class="card-title">حراجی</h4>
                </div>
            </div>
            <div class="card-body">
                <div class="card-block">
                    <div id="carousel-example-caption" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <?php
                            $i = 0;
                            ?>
                            @foreach($haraji_products as $row)
                                <li data-target="#carousel-example-caption" data-slide-to="{{$i}}" class="active"></li>
                                <?php
                                $i++;
                                ?>
                            @endforeach
                        </ol>
                        <div class="carousel-inner" role="listbox">
                            <?php
                            $i = 0;
                            ?>
                            @foreach($haraji_products as $row)
                                <div class="carousel-item {{($i==0)? 'active':'' }}">
                                    <img src="{{getFile($row['file_path'],'product')}}" alt="First slide"
                                         style="width: fit-content; height: 320px">
                                    <div class="percent-wrapper">
                                        <div class="percent">%{{withoutZeros($row['dis'])}}</div>
                                    </div>
                                    <div class="carousel-caption">
                                        <p class="bg-info">{{$row['prod_name']}}</p>

                                    </div>
                                </div>
                                <?php
                                $i++
                                ?>
                            @endforeach
                        </div>
                        <a class="carousel-control-prev" href="#carousel-example-caption" role="button"
                           data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">قبلی</span>
                        </a>
                        <a class="carousel-control-next" href="#carousel-example-caption" role="button"
                           data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">بعدی</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-8" id="recent-sales">
        <div class="card">
            <div class="card-header">
                <div class="card-title-wrap bar-primary">
                    <h4 class="card-title">فروشندگان برتر
                    </h4>
                </div>
                <a class="heading-elements-toggle">
                    <i class="la la-ellipsis-v font-medium-3"></i>
                </a>
            </div>
            <div class="card-content mt-1">
                <div id="canvas-holder">
                    <canvas id="chart-area"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row match-height">
    <div class="col-xl-12 col-lg-12 col-12">
        <div class="card">
            <div class="card-header">
                <div class="card-title-wrap bar-warning">
                    <h4 class="card-title">یادداشت</h4>
                </div>
            </div>
            <div class="card-body">

                <div class="col-md-12">
                    <fieldset class="form-group">
                                       <textarea class="form-control" id="notes"
                                                 rows="10">{{(!empty(getNotesDashboard())?getNotesDashboard()->st_value:'')}}</textarea>
                    </fieldset>
                    <div class="form-actions">
                        <button type="button" class="btn btn-success btn-loading" id="createNotes">ذخیره
                        </button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@section('script')
    <script src="{{ asset('/theme/js/chartist.js') }}"></script>
    <script src="{{ asset('/theme/vendors/js/chart.min.js') }}"></script>
    <script src="{{asset("theme/js/dashboard-ecommerce.js")}}"></script>
    <script src="{{asset('theme/vendors/js/chartist.min.js')}}"></script>
    <script src="{{asset('theme/js/chart/Chart.js')}}"></script>
    <script src="{{asset('theme/js/chart/Chart.min.js')}}"></script>
    <script src="{{asset('theme/js/chart/utils.js')}}"></script>
    <script>
        distributed_labels = ["1", "2", "3", "4", "5", "6", "7"];
        distributed_series = ["1", "2", "3", "4", "5", "6", "7"];
        label_dashboard = [<?php
            foreach ($sum_by_date as $date):
                $date = explode('/', $date['mounth']);
                switch ($date[1]) {
                    case "1";
                        $month = "فروردین";
                        break;
                    case "2";
                        $month = "اردیبهشت";
                        break;
                    case "3";
                        $month = "خرداد";
                        break;
                    case "4";
                        $month = "تیر";
                        break;
                    case "5";
                        $month = "مرداد";
                        break;
                    case "6";
                        $month = "شهریور";
                        break;
                    case "7";
                        $month = "مهر";
                        break;
                    case "8";
                        $month = "آبان";
                        break;
                    case "9";
                        $month = "آذر";
                        break;
                    case "10";
                        $month = "دی";
                        break;
                    case "11";
                        $month = "بهمن";
                        break;
                    case "12";
                        $month = "اسفند";
                        break;
                }
                echo "'$month'" . ",";
            endforeach ?>];
        series_dashboard = [1,
            @foreach($sum_by_date as $sum)
            {{$sum['sum_sale'].','}}
            @endforeach];
        $(document).ready(function () {
            $("#createNotes").click(function (e) {
                var data = $("#notes").val();
                $.ajax({
                    type: "POST",
                    url: '{{asset('/notes-dashboard')}}',
                    data: {value: data},
                    success: function (data) {
                        console.log(JSON.parse(data));
                        resultResponse(JSON.parse(data), true, 2000, true);
                    },
                    error: function (data) {
                        errorForms(data);
                        $(elementClick).html("ثبت")
                    }
                });
            });
        });


        var config = {
            type: 'pie',
            data: {
                datasets: [{
                    data: [
                        <?php
                        foreach ($bartarinha as $batar):
                            $price = withoutZeros($batar['price']);
                            echo "'$price'" . ',';
                        endforeach
                        ?>
                    ],
                    backgroundColor: [
                        window.chartColors.red,
                        window.chartColors.orange,
                        window.chartColors.yellow,
                        window.chartColors.green,
                        window.chartColors.blue,
                    ],
                    label: 'Dataset 1'
                }],
                labels: [
                    <?php
                    foreach ($bartarinha as $batar):
                        $name = $batar['coll_name'];
                        echo "'$name'" . ',';
                    endforeach
                    ?>
                ]
            },
            options: {
                responsive: true
            }
        };

        window.onload = function () {
            var ctx = document.getElementById('chart-area').getContext('2d');
            window.myPie = new Chart(ctx, config);
        };
    </script>
@endsection
