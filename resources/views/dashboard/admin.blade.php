<section class="basic-elements">
    <div class="row">
        <div class="card-body" style="padding-bottom: 0px;">
            <div class="row">
                <div class="col-md-12">
                    <div class="row match-height">
                        <div class="col-xl-3 col-lg-6 col-12">
                            <div class="card gradient-orange-amber">
                                <div class="card-body">
                                    <div class="px-3 py-3">
                                        <div class="media">
                                            <div class="align-center">
                                                <i class="icon-users text-white font-large-2 float-left"></i>
                                            </div>
                                            <div class="media-body text-white text-right">
                                                <h3 class="text-white">{{countUsers()}}</h3>
                                                <span>مجموع خورده فروش و ویزیتور</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-6 col-12">
                            <div class="card gradient-light-blue-cyan">
                                <div class="card-body">
                                    <div class="px-3 py-3">
                                        <div class="media">
                                            <div class="align-center">
                                                <i class="icon-doc text-white font-large-2 float-left"></i>
                                            </div>
                                            <div class="media-body text-white text-right">
                                                <h3 class="text-white">{{countFactor()}}</h3>
                                                <span>مجموع فاکتور های ایجاد شده</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-6 col-12">
                            <div class="card gradient-red-pink">
                                <div class="card-body">
                                    <div class="px-3 py-3">
                                        <div class="media">
                                            <div class="align-center">
                                                <i class="icon-home text-white font-large-2 float-left"></i>
                                            </div>
                                            <div class="media-body text-white text-right">
                                                <h3 class="text-white">{{countCompany()}}</h3>
                                                <span>مجموع شرکت و عمده فروش</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-6 col-12">
                            <div class="card gradient-mint">
                                <div class="card-body">
                                    <div class="px-3 py-3">
                                        <div class="media">
                                            <div class="align-center">
                                                <i class="icon-graph text-white font-large-2 float-left"></i>
                                            </div>
                                            <div class="media-body text-white text-right">
                                                <h3 class="text-white">528,000</h3>
                                                <span>مجموع پورسانت ها</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section id="drag-area">
        <div class="card-body" style="padding-top: 0px">

            <div class="row" id="card-drag-area">

                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title-wrap bar-warning">
                                <h4 class="card-title">یادداشت ها</h4>
                            </div>
                        </div>
                        <div class="card-body">

                            <div class="col-md-12">
                                <fieldset class="form-group">
                                    <textarea class="form-control" id="notes"
                                              rows="12">{{(!empty(getNotesDashboard())?getNotesDashboard()->st_value:'')}}</textarea>
                                </fieldset>
                                <div class="form-actions">
                                    <button type="button" class="btn btn-success btn-loading" id="createNotes">ذخیره
                                    </button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title-wrap bar-warning">
                                <h4 class="card-title">اطلاعات سرور</h4>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row" style="margin-right: 0px">
                                <div class="js-gauge js-gauge--1 gauge"></div>
                                <div class="js-gauge js-gauge--2 gauge"></div>
                                <div class="js-gauge js-gauge--3 gauge"></div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title-wrap bar-warning">
                                <h4 class="card-title">5 شرکت برتر</h4>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="card-body">
                                <table class="table table-striped table-bordered sourced-data dataTable">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>رتبه</th>
                                        <th>نام کالا</th>
                                        <th>عکس</th>
                                        <th>میزان خرید</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <tr>
                                        <td>1</td>
                                        <td>
                                            <button type="button" class="btn btn-sm btn-outline-success round">1
                                            </button>
                                        </td>
                                        <td>فروشگاه صدف</td>
                                        <td>
                                        <span class="avatar avatar-xs">
                                                <img class="box-shadow-2"
                                                     src="{{getFile($pic=null,'logo')}}"
                                                     alt="avatar">
                                            </span>
                                        </td>
                                        <td>50000</td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>
                                            <button type="button" class="btn btn-sm btn-outline-info round">1
                                            </button>
                                        </td>
                                        <td>فروشگاه صدف</td>
                                        <td>
                                        <span class="avatar avatar-xs">
                                                <img class="box-shadow-2"
                                                     src="{{getFile($pic=null,'logo')}}"
                                                     alt="avatar">
                                            </span>
                                        </td>
                                        <td>50000</td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>
                                            <button type="button" class="btn btn-sm btn-outline-warning round">1
                                            </button>
                                        </td>
                                        <td>فروشگاه صدف</td>
                                        <td>
                                        <span class="avatar avatar-xs">
                                                <img class="box-shadow-2"
                                                     src="{{getFile($pic=null,'logo')}}"
                                                     alt="avatar">
                                            </span>
                                        </td>
                                        <td>50000</td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>
                                            <button type="button" class="btn btn-sm btn-outline-danger round">1
                                            </button>
                                        </td>
                                        <td>فروشگاه صدف</td>
                                        <td>
                                        <span class="avatar avatar-xs">
                                                <img class="box-shadow-2"
                                                     src="{{getFile($pic=null,'logo')}}"
                                                     alt="avatar">
                                            </span>
                                        </td>
                                        <td>50000</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title-wrap bar-warning">
                                <h4 class="card-title">5 مغازه دار برتر</h4>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="card-body">
                                <table class="table table-striped table-bordered sourced-data dataTable">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>رتبه</th>
                                        <th>نام فروشگاه</th>
                                        <th>تاریخ ثبت</th>
                                        <th>لوگو</th>
                                        <th>میزان خرید</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <tr>
                                        <td>1</td>
                                        <td>
                                            <button type="button" class="btn btn-sm btn-outline-success round">1
                                            </button>
                                        </td>
                                        <td>فروشگاه صدف</td>
                                        <td>1398/11/25</td>
                                        <td>
                                        <span class="avatar avatar-xs">
                                                <img class="box-shadow-2"
                                                     src="{{getFile($pic=null,'logo')}}"
                                                     alt="avatar">
                                            </span>
                                        </td>
                                        <td>50000</td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>
                                            <button type="button" class="btn btn-sm btn-outline-info round">1
                                            </button>
                                        </td>
                                        <td>فروشگاه صدف</td>
                                        <td>1398/11/25</td>
                                        <td>
                                        <span class="avatar avatar-xs">
                                                <img class="box-shadow-2"
                                                     src="{{getFile($pic=null,'logo')}}"
                                                     alt="avatar">
                                            </span>
                                        </td>
                                        <td>50000</td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>
                                            <button type="button" class="btn btn-sm btn-outline-warning round">1
                                            </button>
                                        </td>
                                        <td>فروشگاه صدف</td>
                                        <td>1398/11/25</td>
                                        <td>
                                        <span class="avatar avatar-xs">
                                                <img class="box-shadow-2"
                                                     src="{{getFile($pic=null,'logo')}}"
                                                     alt="avatar">
                                            </span>
                                        </td>
                                        <td>50000</td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>
                                            <button type="button" class="btn btn-sm btn-outline-danger round">1
                                            </button>
                                        </td>
                                        <td>فروشگاه صدف</td>
                                        <td>1398/11/25</td>
                                        <td>
                                        <span class="avatar avatar-xs">
                                                <img class="box-shadow-2"
                                                     src="{{getFile($pic=null,'logo')}}"
                                                     alt="avatar">
                                            </span>
                                        </td>
                                        <td>50000</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    </div>
</section>

@section('script')

    <script src="{{asset('js/functions/storeAjax.js')}}"></script>

    <script>
        $(document).ready(function () {
            $("#createNotes").click(function (e) {
                var data = $("#notes").val();
                $.ajax({
                    type: "POST",
                    url: '{{asset('/notes-dashboard')}}',
                    data: {value: data},
                    success: function (data) {
                        console.log(JSON.parse(data));
                        resultResponse(JSON.parse(data), true, 2000, true);
                    },
                    error: function (data) {
                        errorForms(data);
                        $(elementClick).html("ثبت")
                    }
                });
            });
        });


    </script>
@endsection
