<section class="basic-elements">
    <div class="row">
        <div class="card-body" style="padding-bottom: 0px;">
            <div class="row">
                <div class="col-md-8">
                    <div class="card-body">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                @if(count(getAdvertising('562'))!=0)
                                    <?php
                                    $i = 0;
                                    ?>
                                    @foreach(getAdvertising('562') as $value)
                                        @if($value->adm_station_id==562)
                                            <li data-target="#carousel-example-generic" data-slide-to="{{$i}}"
                                                class="active"></li>
                                            <?php $i++ ?>
                                        @endif
                                    @endforeach
                                @else
                                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                @endif
                            </ol>
                            <div class="carousel-inner slider" role="listbox">
                                @if(count(getAdvertising('562'))!=0)
                                    <?php
                                    $i = 0;
                                    ?>
                                    @foreach(getAdvertising('562') as $value)
                                        @if($value->adm_station_id==562)
                                            <div class="carousel-item {{($i==0)? 'active':'' }}">
                                                <a href="{{$value->adm_link}}"> <img
                                                        src="{{getFile($value->ur_path)}}"
                                                        alt="First slide" style="width: 640%; height: 200px"></a>
                                            </div>
                                        @endif
                                        <?php
                                        $i++
                                        ?>
                                    @endforeach
                                @else
                                    <div class="carousel-item active">
                                        <img
                                            src="{{asset('default-image/advertising.jpg')}}"
                                            alt="First slide" style="width: 700%; height: 200px">
                                    </div>
                                @endif
                            </div>
                            <a class="carousel-control-prev" href="#carousel-example-generic" role="button"
                               data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">قبلی</span>
                            </a>
                            <a class="carousel-control-next" href="#carousel-example-generic" role="button"
                               data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">بعدی</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card-body">
                        <div id="carousel-example-genericc" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                @if(count(getAdvertising('563'))!=0)
                                    <?php
                                    $i = 0;
                                    ?>
                                    @foreach(getAdvertising('563') as $value)
                                        @if($value->adm_station_id==563)
                                            <li data-target="#carousel-example-genericc" data-slide-to="{{$i}}"
                                                class="active"></li>
                                            <?php $i++ ?>
                                        @endif
                                    @endforeach
                                @else
                                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                @endif
                            </ol>
                            <div class="carousel-inner slider" role="listbox">
                                @if(count(getAdvertising('563'))!=0)
                                    <?php
                                    $i = 0;
                                    ?>
                                    @foreach(getAdvertising('563') as $value)
                                        @if($value->adm_station_id==563)
                                            <div class="carousel-item {{($i==0)? 'active':'' }}">
                                                <a href="{{$value->adm_link}}"> <img
                                                        src="{{getFile($value->ur_path)}}"
                                                        alt="First slide" style="width: 285%; height: 200px"></a>
                                            </div>
                                        @endif
                                        <?php
                                        $i++
                                        ?>
                                    @endforeach
                                @else
                                    <div class="carousel-item active">
                                        <img
                                            src="{{asset('default-image/advertising-small.jpg')}}"
                                            alt="First slide" style="width: 500%; height: 200px">
                                    </div>
                                @endif
                            </div>
                            <a class="carousel-control-prev" href="#carousel-example-genericc" role="button"
                               data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">قبلی</span>
                            </a>
                            <a class="carousel-control-next" href="#carousel-example-genericc" role="button"
                               data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">بعدی</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card-body">
                        <?php
                        $alert= array('info','red','warning','secondary');
                        $i=0;
                        ?>
                        @foreach(getNewsItems() as $row)
                            <?php
                            $random_alert= array_rand($alert);
                            ?>
                            <div class="alert alert-<?= $alert[$random_alert] ?> fade show" role="alert" data-toggle="modal" data-target="#large<?= $i ?>">
                                {!! $row->ann_title !!}
                            </div>
                            <div class="modal fade text-left" id="large<?=$i?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel17" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel17">{!! $row->ann_title !!}</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            {!! $row->ann_body !!}
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">بستن</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php $i++?>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <section id="drag-area">
            <div class="card-body" style="padding-top: 0px">

                <div class="row" id="card-drag-area">

                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title-wrap bar-warning">
                                    <h4 class="card-title">یادداشت ها</h4>
                                </div>
                            </div>
                            <div class="card-body">

                                <div class="col-md-12">
                                    <fieldset class="form-group">
                                       <textarea class="form-control" id="notes"
                                                 rows="12">{{(!empty(getNotesDashboard())?getNotesDashboard()->st_value:'')}}</textarea>
                                    </fieldset>
                                    <div class="form-actions">
                                        <button type="button" class="btn btn-success btn-loading" id="createNotes">ذخیره
                                        </button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title-wrap bar-warning">
                                    <h4 class="card-title">5 شرکت برتر</h4>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="card-body">
                                    <table class="table table-striped table-bordered sourced-data dataTable">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>رتبه</th>
                                            <th>نام کالا</th>
                                            <th>عکس</th>
                                            <th>میزان خرید</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        <tr>
                                            <td>1</td>
                                            <td>
                                                <button type="button" class="btn btn-sm btn-outline-success round">1
                                                </button>
                                            </td>
                                            <td>فروشگاه صدف</td>
                                            <td>
                                        <span class="avatar avatar-xs">
                                                <img class="box-shadow-2"
                                                     src="http://www.irannaz.com/images/2017/07/150114907675023-irannaz-com.jpg"
                                                     alt="avatar">
                                            </span>
                                            </td>
                                            <td>50000</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>
                                                <button type="button" class="btn btn-sm btn-outline-info round">1
                                                </button>
                                            </td>
                                            <td>فروشگاه صدف</td>
                                            <td>
                                        <span class="avatar avatar-xs">
                                                <img class="box-shadow-2"
                                                     src="http://www.irannaz.com/images/2017/07/150114907675023-irannaz-com.jpg"
                                                     alt="avatar">
                                            </span>
                                            </td>
                                            <td>50000</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>
                                                <button type="button" class="btn btn-sm btn-outline-warning round">1
                                                </button>
                                            </td>
                                            <td>فروشگاه صدف</td>
                                            <td>
                                        <span class="avatar avatar-xs">
                                                <img class="box-shadow-2"
                                                     src="http://www.irannaz.com/images/2017/07/150114907675023-irannaz-com.jpg"
                                                     alt="avatar">
                                            </span>
                                            </td>
                                            <td>50000</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>
                                                <button type="button" class="btn btn-sm btn-outline-danger round">1
                                                </button>
                                            </td>
                                            <td>فروشگاه صدف</td>
                                            <td>
                                        <span class="avatar avatar-xs">
                                                <img class="box-shadow-2"
                                                     src="http://www.irannaz.com/images/2017/07/150114907675023-irannaz-com.jpg"
                                                     alt="avatar">
                                            </span>
                                            </td>
                                            <td>50000</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title-wrap bar-warning">
                                    <h4 class="card-title">5 مغازه دار برتر</h4>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="card-body">
                                    <table class="table table-striped table-bordered sourced-data dataTable">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>رتبه</th>
                                            <th>نام فروشگاه</th>
                                            <th>تاریخ ثبت</th>
                                            <th>لوگو</th>
                                            <th>میزان خرید</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        <tr>
                                            <td>1</td>
                                            <td>
                                                <button type="button" class="btn btn-sm btn-outline-success round">1
                                                </button>
                                            </td>
                                            <td>فروشگاه صدف</td>
                                            <td>1398/11/25</td>
                                            <td>
                                        <span class="avatar avatar-xs">
                                                <img class="box-shadow-2"
                                                     src="http://www.irannaz.com/images/2017/07/150114907675023-irannaz-com.jpg"
                                                     alt="avatar">
                                            </span>
                                            </td>
                                            <td>50000</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>
                                                <button type="button" class="btn btn-sm btn-outline-info round">1
                                                </button>
                                            </td>
                                            <td>فروشگاه صدف</td>
                                            <td>1398/11/25</td>
                                            <td>
                                        <span class="avatar avatar-xs">
                                                <img class="box-shadow-2"
                                                     src="http://www.irannaz.com/images/2017/07/150114907675023-irannaz-com.jpg"
                                                     alt="avatar">
                                            </span>
                                            </td>
                                            <td>50000</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>
                                                <button type="button" class="btn btn-sm btn-outline-warning round">1
                                                </button>
                                            </td>
                                            <td>فروشگاه صدف</td>
                                            <td>1398/11/25</td>
                                            <td>
                                        <span class="avatar avatar-xs">
                                                <img class="box-shadow-2"
                                                     src="http://www.irannaz.com/images/2017/07/150114907675023-irannaz-com.jpg"
                                                     alt="avatar">
                                            </span>
                                            </td>
                                            <td>50000</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>
                                                <button type="button" class="btn btn-sm btn-outline-danger round">1
                                                </button>
                                            </td>
                                            <td>فروشگاه صدف</td>
                                            <td>1398/11/25</td>
                                            <td>
                                        <span class="avatar avatar-xs">
                                                <img class="box-shadow-2"
                                                     src="http://www.irannaz.com/images/2017/07/150114907675023-irannaz-com.jpg"
                                                     alt="avatar">
                                            </span>
                                            </td>
                                            <td>50000</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</section>
@section('script')
    <script src="{{asset('theme/js/draggable.js')}}"></script>
    <script src="{{asset('theme/vendors/js/dragula.min.js')}}"></script>
    <script src="{{asset('theme/js/chartjs.js')}}"></script>
    <script src="{{asset('theme/vendors/js/chart.min.js')}}"></script>
    <script src="{{asset('js/functions/storeAjax.js')}}"></script>
    <script>
        $(document).ready(function () {
            $("#createNotes").click(function (e) {
                var data=$("#notes").val();
                $.ajax({
                    type: "POST",
                    url: '{{asset('/notes-dashboard')}}',
                    data: {value:data},
                    success: function (data) {
                        console.log(JSON.parse(data));
                        resultResponse(JSON.parse(data), true, 2000, true);
                    },
                    error: function (data) {
                        errorForms(data);
                        $(elementClick).html("ثبت")
                    }
                });
            });
        });
    </script>
@endsection
