@extends('master_page')
@section('title_browser',$title)
@section('style')
    <style>
        /*table, th, td {*/
        /*    border: 1px solid #000000 !important;*/
        /*    color: #000000 !important;*/
        /*    font-weight: bolder;*/
        /*}*/



        /*.btn-sm, .input-sm {*/
        /*    height: 30px !important;*/
        /*}*/

        /*.input-sm {*/
        /*    padding-right: 5px;*/
        /*    width: 40px !important;*/
        /*}*/
    </style>
@endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-body">
                            <div class="card-block">
                                <div class="row">
                                    <div class="col-sm-12 show" style="display: none">
                                        <h2>فیلترها:</h2>
                                        <h6 style="line-height: 30px !important;font-weight: bold !important;text-align: justify !important;">
                                            <?=$title?>
                                            <span class="pull-left"><?='چاپ توسط: ' . user("username") . '/ تاریخ چاپ: ' . jdate()->format("Y/m/d | H:i:s")?></span>
                                        </h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                @foreach($headers as $header)
                                                    <th>{{$header}}</th>
                                                @endforeach
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            $i = 0;
                                            ?>
                                            @if(count($records))
                                                @foreach($records as $record)
                                                    <?php
                                                    $keys = array_keys($record);
                                                    $i++;
                                                    ?>
                                                    <tr>
                                                        @foreach($record as $key => $item)
                                                            <?php
                                                            if (in_array($key, $sum)) {
                                                                $temp = str_replace(',', '', $item);
                                                                if (!isset($sums[$key]))
                                                                    $sums[$key] = 0;
                                                                $sums[$key] += $temp;
                                                            }
                                                            ?>
                                                            <td>{{ $item }}</td>
                                                        @endforeach
                                                    </tr>
                                                @endforeach
                                            @else
                                                <td colspan="<?=count($headers)?>" style="text-align: center">{{ config('first_config.message.empty_table') }}</td>
                                            @endif
                                            @if(isset($keys))
                                                <tr>
                                                    @foreach($keys as $r)
                                                        @if($loop->index == 0)
                                                            <td>جمع</td>
                                                        @elseif(in_array($r,$sum))
                                                            <td>{{number_format($sums[$r])}}</td>
                                                            {{--@elseif($r == getPrevKeyInArray($keys,))--}}
                                                        @else
                                                            <td></td>
                                                        @endif
                                                    @endforeach
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
@endsection