@extends('master_page')
@section('title_browser','انتخاب دسته بندی')
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header">انتخاب دسته بندی</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <form action='{{url()->current()}}' method='POST'>
                                {{ csrf_field() }}
                                {!! (!empty($selected)) ? "<input name='_method' type='hidden' value='POST'>" : ''  !!}
                                @include('sample.sample_category_select')
                                <div class='row'>
                                    <div class='col-md-12'>
                                        <div class='form-group'>
                                        <a href="{{ asset($url) }}" class="btn btn-danger">بازگشت</a>
                                            <button type="button" id="addSelectedCategories" class='btn btn-success btn-loading'>ثبت</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('/js/functions/storeAjax.js') }}"></script>
    <script src="{{asset('/js/select2WithCheckbox.js')}}"></script>
    <script>
        $(document).on('click', '#addSelectedCategories', function () {
            storeAjax($(this), 'POST', captionButton = "ثبت", progress = false, reloadPage = false, TimeOutActionNextStore = 0,false)
        });
    </script>
@endsection