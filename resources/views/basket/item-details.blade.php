@extends('master_page')
<?php
$items = $result;
$title = "فاکتور خرید ";
?>
@section('title_browser',$title)
@section('style')
    <style>
        .btn-sm, .input-sm {
            height: 30px !important;
        }

        .input-sm {
            padding-right: 5px;
            width: 40px !important;
        }
    </style>
@endsection
@section('main_content')
    <section id="basic-form-layouts" class="basket-items">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header"><?=$title?></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="container">
                            <table class="table">
                                <tr>
                                    <th colspan="5"><h4 class="pull-right">مشخصات فروشنده</h4>
                                    </th>
                                </tr>
                                <tr>
                                    <td>نام شرکت:</td>
                                    <td>{{$items[array_keys($items)[0]]['coll_name']}}</td>
                                    <td>تلفن:</td>
                                    <td>{{$person_collection->prsn_phone1}}</td>
                                    <td rowspan="2" style="width: 100px;height: 100px"><img class="rounded-circle"
                                                                                            style="width: 100px;height: 100px"
                                                                                            src="{{ getFile(($items[array_keys($items)[0]]['logo'] != "") ? $items[array_keys($items)[0]]['logo'] : 'nologo.png') }}">
                                    </td>
                                </tr>
                                <tr>
                                    <td>آدرس:</td>
                                    <td colspan="3">{{$items[array_keys($items)[0]]['coll_address']}}</td>
                                </tr>
                            </table>
                        </div>
                        <p class="mb-0"></p>
                    </div>
                    <form method="post">
                        @csrf
                        <div class="card-body">
                            <div class="card-block">
                                <table
                                    class="table table-striped table-inverse table-bordered table-hover table-factor">
                                    <thead>
                                    <th>ردیف</th>
                                    <th>محصول</th>
                                    <th>مقدار</th>
                                    <th>واحد</th>
                                    <th>قیمت واحد</th>
                                    <th>قیمت کل</th>
                                    <th>تخفیف</th>
                                    <th>مبلغ کل پس از کسر تخفیف</th>
                                    <th>مالیات بر ارزش افزوده</th>
                                    <th>قیمت کل + مالیات بر ارزش افزوده</th>
                                    <th>عملیات</th>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $i = $sum_one = $sum_total = $sum_discount = $sum_total_menhaye_discount = $sum_maliat = $sum_total_bealave_maliat = 0;
                                    if($items)
                                    {
                                    foreach ($items as $item)
                                    {
                                    $one = $total = $discount = $total_menhaye_discount = $maliat = $total_bealave_maliat = 0;
                                    $i++;

                                    if ($item['fd_ooe'] == 0) {
                                        $sum_one += $one = $item['fd_price_buy'];
                                        $sum_total += $total = $one * $item['fd_count'];
                                        $sum_discount += $discount = floor(calcPercent($total, $item['fd_discount']));
                                        $sum_total_menhaye_discount += $total_menhaye_discount = $total - $discount;
                                        $sum_maliat += $maliat = ($item['fd_arzesh_afzoodeh'] == 1) ? floor(calcPercent($total_menhaye_discount, $tax)) : 0;
                                        $sum_total_bealave_maliat += $total_bealave_maliat = $total_menhaye_discount + $maliat;
                                    } else
                                        $one = "اشانتیون";
                                    $factor_discount = withoutZeros(floor(calcPercent($sum_total_bealave_maliat, $items[array_keys($items)[0]]['fm_festival_discount'])));
                                    $financial_unit = setting("financial_unit");
                                    ?>
                                    <tr style="{{($item['fd_ooe'] == 1) ? 'color:red': ''}}">
                                        <td>{{$i}}</td>
                                        <?php
                                        if ($item['fd_ooe'] == 0)
                                            $txt = $item['prod_name'];
                                        else if ($item['fd_ooe_id'] == -1)
                                            $txt = "اشانتیون-فاکتور ".$item['prod_name'];
                                        else
                                            $txt = "اشانتیون-محصول " . $item['prod_name'];
                                        ?>
                                        <td><b><?=$txt?></b></td>
                                        <td style="width: 200px">
                                            <?php
                                            if ($item['fd_ooe'] == 0)
                                            {
                                            ?>
                                            <div class="input-group prod-counter col-sm-offset-2">
                                                <div class="input-group-prepend">
                                                    <a class="btn btn-success prod-counter-plus btn-sm"><i
                                                            class="fa fa-plus"></i></a>
                                                </div>
                                                <input type="text" class="prod-count input-sm"
                                                       value="{{$item['fd_count']}}" style="width: 20px">
                                                <div class="input-group-prepend">
                                                    <a class="btn btn-danger prod-counter-minus btn-sm"><i
                                                            class="fa fa-minus"></i></a>
                                                </div>
                                                <div class="input-group-prepend">
                                                    <a class="btn btn-primary btn-accept btn-sm btn-loading-sm"
                                                       data-id="{{$item['cp_id']}}"><i class="fa fa-check"></i></a>
                                                </div>
                                            </div>
                                            <?php
                                            }
                                            else {
                                                echo $item['fd_count'];
                                            }
                                            ?>
                                        </td>
                                        <td>{{$item['bas_value']}}</td>
                                        <td>
                                            {{is_numeric($one) ? number_format($one) : $one}}
                                        </td>
                                        <td>{{number_format($total)}}</td>
                                        <td>{{number_format($discount)}}</td>
                                        <td>{{number_format($total_menhaye_discount)}}</td>
                                        <td>{{number_format($maliat)}}</td>
                                        <td>{{number_format($total_bealave_maliat)}}</td>
                                        <td>
                                            <a class="danger" data-method="post"
                                               data-confirm="اطمینان از حذف این مورد دارید؟"
                                               data-action="{{asset('delete-product-from-factor').'/'.$item['cp_id']}}"
                                               title="حذف"><i
                                                    class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                    <tr>
                                        <td colspan="4">مجموع</td>
                                        <td>{{number_format($sum_one)}}</td>
                                        <td>{{number_format($sum_total)}}</td>
                                        <td>{{number_format($sum_discount)}}</td>
                                        <td>{{number_format($sum_total_menhaye_discount)}}</td>
                                        <td>{{number_format($sum_maliat)}}</td>
                                        <td>{{number_format($sum_total_bealave_maliat)}}</td>
                                        <td>
                                            -
                                        </td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                    <tr>
                                        <td colspan="7" rowspan="4" style="vertical-align: middle;font-size: 16px"><b>کل
                                                مبلغ فاکتور به
                                                حروف:</b> {{numtoword($sum_total_bealave_maliat - $factor_discount).' '.$financial_unit}}
                                        </td>
                                        <td>کل مبلغ فاکتور</td>
                                        <td colspan="2"><b
                                                style="font-size:16px">{{number_format($sum_total_bealave_maliat)}}</b>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>تخفیف فاکتور
                                            ({{withoutZeros($items[array_keys($items)[0]]['fm_festival_discount'])}}%)
                                        </td>
                                        <td colspan="2">{{number_format($factor_discount)}}</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>هزینه ارسال

                                        </td>
                                        <td colspan="2">0</td>
                                        <td></td>
                                    </tr>
                                    <tr class="success">

                                        <td>مبلغ قابل پرداخت</td>
                                        <td colspan="2"><b
                                                style="font-size:16px">{{number_format($sum_total_bealave_maliat - $factor_discount)}}</b> {{$financial_unit}}
                                        </td>
                                        <td></td>
                                    </tr>
                                    </tbody>
                                </table>
                                @if($product_return_permit)
                                    <table class="table table-bordered table-marjooee" style="text-align: center">
                                        <tr>
                                            <th colspan="5"><h4 class="pull-right">کالاهای مرجوعی</h4>
                                                <a class="btn btn-sm pull-left btn-success" id="btn-add-marjooee"
                                                   title="افزودن"><i
                                                        class="fa fa-plus"></i></a>
                                            </th>
                                        </tr>
                                        <tr>
                                            <th>محصول</th>
                                            <th>تعداد</th>
                                            <th>قیمت</th>
                                            <th>علت</th>
                                            <th>عملیات</th>
                                        </tr>
                                        @if($factor_returns)
                                            @foreach($factor_returns as $return)
                                                <tr>
                                                    <td class="selectProduct">
                                                        <div class="input-group">
                                                            <button type="button"
                                                                    class="btn btn-info showProduct btn-block">{{$return->prod_name}}                                                            </button>
                                                            <input type="hidden" name="ret_fd_id[]"
                                                                   value="{{$return->fd_id}}">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" name="ret_count[]"
                                                               value="{{$return->fr_count}}">
                                                    </td>
                                                    <td>
                                                        <label class="form-control price_buy"
                                                               style="min-width: 150px">{{number_format(withoutZeros($return->fd_price_buy))}}</label>
                                                    </td>
                                                    <td>
                                                        <select class="form-control" name="ret_reason[]">
                                                            <option value="0">انتخاب کنید...</option>
                                                            @foreach($return_reasons as $reason)
                                                                <option
                                                                    value="{{$reason->crr_id}}" {{($return->fr_reason_id == $reason->crr_id) ? "selected" : ''}}>{{$reason->bas_value}}</option>
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-danger btn-sm deleteForm">
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif

                                    </table>
                                @endif
                                <div class="row">
                                    <div class="col-sm-12">
                                        <textarea name="description" class="form-control"
                                                  placeholder="توضیحات را اینجا بنویسید">{{$items[array_keys($items)[0]]['fm_description']}}</textarea>
                                    </div>
                                </div>
                                @if(($sum_total_bealave_maliat - $factor_discount) < $min_amount = setting('min_factor_amount',$items[array_keys($items)[0]]['coll_id'],0))
                                    <div class="row" style="margin-top: 20px">
                                        <div class="col-sm-12">
                                            <div class="alert alert-danger text-white">امکان ثبت فاکتور وجود ندارد، مبلغ
                                                فاکتور کمتر از حداقل مبلغ مجاز این شرکت می باشد! حداقل مبلغ
                                                فاکتور {{number_format($min_amount) . ' '.$financial_unit}} می باشد.
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    <div class="row" style="margin-top: 20px">
                                        <div class="col-sm-12">
                                            <div class="pull-left">
                                                <a class="btn btn-primary" href="{{asset("product-list")}}">افزودن
                                                    محصولات دیگر</a>
                                                <a class="btn btn-success btn-loading" id="btnSaveFactor">ثبت</a>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <table>
        <tr id="marjooeeForm" class="card border-right my-4 border-primary border-3" style="display: none">
            <td class="selectProduct">
                <div class="input-group">
                    <button type="button" class="btn btn-info showProduct btn-block">محصول
                    </button>
                    <input type="hidden" name="ret_fd_id[]">
                </div>
            </td>
            <td>
                <div class="input-group">
                    <select name="measure_unit[]" class="form-control" style="display: none">
                        <option value="0">عدد</option>
                    </select>
                    <input type="text" class="form-control" name="ret_count[]">
                </div>
            </td>
            <td>
                <label class="form-control price_buy" style="min-width: 150px"></label>
            </td>
            <td>
                <select class="form-control" name="ret_reason[]">
                    <option value="">انتخاب کنید...</option>
                    @foreach($return_reasons as $reason)
                        <option value="{{$reason->crr_id}}">{{$reason->bas_value}}</option>
                    @endforeach
                </select>
            </td>
            <td>
                <button type="button" class="btn btn-danger btn-sm deleteForm">
                    <i class="fa fa-trash"></i>
                </button>
            </td>
        </tr>
    </table>
@endsection
@section('script')
    <script src="{{asset('/js/functions/storeAjax.js')}}"></script>
    <script>
        function customeFunctionAfterSelectRow(node) {
            trClick.closest('tr').find("input[name^='ret_count']").val($(node).data('count'));
            if ($(node).data('count_per') > 1) {
                trClick.closest('tr').find("select[name^='measure_unit']").append(new Option($(node).data('measure_label'), "1")).show();
            }
            trClick.closest('tr').find("label.price_buy").html($(node).data('price'));
            var last_modal = createdModals.pop();
            $('#' + last_modal).modal('toggle');
            trClick.closest('.table-marjooee').find("input[name^='ret_fd_id']").each(function () {
                if ($(this).val() == $(node).data('id')) {
                    alert('این محصول قبلا انتخاب شده است!');
                    trClick.closest('tr').remove();
                }
            });
        }

        $(document).ready(function () {
            $(document).on('click', '.factor-master', function () {
                var id = $(this).data('id');
                showData('{{asset('/factor/detail-list')}}' + '/' + id);
            });
            $(document).on('click', '.showProduct', function () {
                showData('{{asset('/popup-factors').'/'.$items[array_keys($items)[0]]['coll_id']}}');
                trClick = $(this);
            });
            $(document).on("click", "#btnSaveFactor", function (e) {
                e.preventDefault();
                storeAjax($(this), 'POST', 'ثبت', false, 0);
            });
            $(document).on("click", ".deleteForm", function () {
                if (confirm('اطمینان از حذف این مورد دارید؟') == false)
                    return false;
                $(this).closest("tr").remove();
            });
            $(document).on("click", "#btn-add-marjooee", function () {
                var elem = $(".table-marjooee tr:last");
                var clone = $("#marjooeeForm").clone();
                clone.counter('محصول');
                $(elem).after("<tr>" + clone.html() + "<tr>");
                $(".table-marjooee tr:last").css("display", 'block');
            });
            $(document).on("click", '.btn-accept', function () {
                var elem = $(this).closest(".prod-counter");
                var count = parseInt($(elem).find("input").val());
                var id = $(this).data('id');
                $.post("{{asset('update-count')}}", {id: id, count: count}, function (data) {
                    var result = JSON.parse(data);
                    if (result.status == 100) {
                        location.reload();
                    } else {
                        toastr.error(result.msg, "نا موفق");
                    }
                });
            });

            function loadItems() {
                $.get("{{asset('load-basket-items').'/'.$id}}", function (data) {
                    var result = JSON.parse(data);
                    $(".basket-items").html(result.FormatHtml);
                });
            }

            $(document).on("click", ".prod-counter-plus", function () {
                var elem = $(this).closest(".prod-counter");
                var count = parseInt($(elem).find("input").val()) + 1;
                $(elem).find("input").attr('value', count);
            });
            $(document).on("keyup", '.prod-count', function () {
                if ($(this).val() == 0 || $(this).val() == '')
                    $(this).val(1);
            });
            $(document).on("keydown", '.prod-count', function (e) {
                var elem = $(this).closest(".prod-counter");
                if (e.keyCode == 13) {
                    $(elem).find('.btn-accept').click();
                }
            });
            $(document).on("click", ".prod-counter-minus", function () {
                var elem = $(this).closest(".prod-counter");
                var count = parseInt($(elem).find("input").val());
                if (count >= 2) {
                    $(elem).find("input").attr('value', count - 1);
                }
            });
        });
    </script>
@append
