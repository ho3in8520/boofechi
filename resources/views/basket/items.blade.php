@extends('master_page')
<?php
$title = "سبد خرید";
?>
@section('title_browser',$title)
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header"><?=$title?></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="container">

                        </div>
                        <p class="mb-0"></p>
                    </div>
                    <form method="GET" action="{{ asset('/company') }}">
                        <div class="card-body">
                            <div class="card-block">
                                <table style="text-align: center !important"
                                       class="table table-striped table-inverse table-bordered table-hover">
                                    <thead>
                                    <th>ردیف</th>
                                    <th>لوگو</th>
                                    <th>شرکت</th>
                                    <th>تعداد محصولات</th>
                                    <th>مبلغ</th>
                                    <th>عملیات</th>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $items = getBasketItems();
                                    $sum = $count = $i = 0;
                                    if($items)
                                    {
                                    foreach ($items as $item)
                                    {
                                    $i++;
                                    $sum += withoutZeros($item['price']);
                                    $count += $item['cnt'];
                                    ?>
                                    <tr>
                                        <td class='clickable-row' data-href='{{asset('basket-item-details').'/'.$item['fm_id']}}'>{{$i}}</td>
                                        <td class='clickable-row' data-href='{{asset('basket-item-details').'/'.$item['fm_id']}}'><img class="rounded-circle"
                                                 style="width: 50px;height: 50px"
                                                 src="{{ getFile(($item['logo'] != "") ? $item['logo'] : 'nologo.png') }}"></td>
                                        <td class='clickable-row' data-href='{{asset('basket-item-details').'/'.$item['fm_id']}}'><b>{{$item['coll_name']}}</b></td>
                                        <td class='clickable-row' data-href='{{asset('basket-item-details').'/'.$item['fm_id']}}'>{{$item['cnt']}}</td>
                                        <td class='clickable-row' data-href='{{asset('basket-item-details').'/'.$item['fm_id']}}'>{{number_format(withoutZeros($item['price']))}}</td>
                                        <td>
                                            <a href="{{asset('basket-item-details').'/'.$item['fm_id']}}"><i
                                                        class="fa fa-eye"></i></a>
                                            <a class="danger" data-method="post" data-confirm="اطمینان از حذف فاکتور مورد نظر دارید؟"
                                               data-action="{{asset('delete-factor-from-basket').'/'.$item['fm_id']}}"><i
                                                        class="fa fa-trash"></i></a>
                                        </td>

                                    </tr>
                                    <?php
                                    }
                                    }
                                    else{
                                    ?>
                                    <tr>
                                        <td colspan="6">سبد خرید شما خالی است</td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <td>-</td>
                                        <td>-</td>
                                        <td><b>جمع</b></td>
                                        <td>{{$count}}</td>
                                        <td>{{number_format($sum)}}</td>
                                        <td></td>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

