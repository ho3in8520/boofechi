@extends('master_page')
<?php
$title = "لیست کارخانه ها";
?>
@section('title_browser',$title)
@section('style')@endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header"><?=$title?></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="container">

                        </div>
                        <p class="mb-0"></p>
                    </div>
                    <form method="GET" action="{{ asset('/factory') }}">
                    <div class="card-body">
                        <div class="card-block">
                            <div class="col-md-12 table-responsive">
                                <div class="summary">{!!getSummary($data) !!}</div>
                                <table class="table table-striped table-bordered sourced-data dataTable">
                                    <thead>
                                    <tr>
                                        <th>ردیف</th>
                                        <th>نام کارخانه</th>
                                        <th>شناسه</th>
                                        <th>استان</th>
                                        <th>شهر</th>
                                        <th>عملیات</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @include('sample.sample_collections_filter_form')
                                    @if($data->count() != 0)
                                        @foreach($data as $row)
                                            <tr>
                                                <td>{{index($data,$loop)}}</td>
                                                <td>{{$row->coll_name}}</td>
                                                <td>{{$row->coll_id}}</td>
                                                <td>{{$row->state_name}}</td>
                                                <td>{{$row->city_name}}</td>
                                                <td></td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="7" style="text-align: center;">
                                                {{ config('first_config.message.empty_table') }}
                                            </td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        {{ $data->appends(request()->query())->links() }}
                    </div>
                    </form>
                </div>>
            </div>
        </div>
    </section>
@endsection
@section('script')@endsection
