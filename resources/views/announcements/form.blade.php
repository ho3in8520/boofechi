@extends('master_page')

@section('title_browser') ارسال اطلاعیه@endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header">ارسال اطلاعیه</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                    </div>
                    <div class="card-body">
                        <div class="card-block">
                            <form
                                action='{{ (empty($announcement)) ? route('announcement.store') : route('announcement.update',$announcement->ann_id) }}'
                                method='POST'>
                                {{ csrf_field() }}
                                {!! (!empty($announcement)) ? "<input name='_method' type='hidden' value='PATCH'>" : ''  !!}
                                @include('sample.sample_form_announce')
                                <div class='row'>
                                    <div class='col-md-10'>
                                        <div class='form-group'>
                                            <label></label>
                                            <button type="button" id="saveAnnounced"
                                                    class='btn btn-success btn-loading'>
                                                ثبت
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{asset('/js/functions/storeAjax.js')}}"></script>
    <script>
        $(document).ready(function () {
            $(document).on('click', '#saveAnnounced', function () {
                var elementClick = $(this);
                startConfig();
                var data = $(this).closest("form").serializeArray();
                if ($('input[name=type]:checked').val() == undefined) {
                    var type = ""
                } else {
                    var type = $('input[name=type]:checked').val()
                }
                data.push({
                    "name": "ann_body",
                    "value": CKEDITOR.instances.editor.getData()
                });
                data.push({
                    "name": "type",
                    "value": type
                });
                // alert(CKEDITOR.instances.editor.getData());
                console.log(data);
                $.ajax({
                    type: "POST",
                    url: '{{asset('/announcement')}}',
                    data: data,
                    success: function (data) {
                        console.log(JSON.parse(data));
                        resultResponse(JSON.parse(data), true, 2000, true);
                    },
                    error: function (data) {
                        errorForms(data);
                        $(elementClick).html("ثبت")
                    }
                });

            });
        });

    </script>
@append
