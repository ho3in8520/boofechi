@extends('master_page')
<?php
$title = "لیست اطلاعیه ها";
?>
@section('title_browser',$title)
@section('style')@endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header"><?=$title?></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="container">

                        </div>
                        <p class="mb-0"></p>
                    </div>
                    <form method="GET" action="{{ asset('/announcements') }}">
                        <div class="card-body">
                            <div class="card-block">
                                <div class="col-md-12 table-responsive">
                                    <div class="summary">{!!getSummary($data) !!}</div>
                                    @csrf
                                    <table class="table table-striped table-bordered sourced-data dataTable">
                                        <thead>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>عنوان اطلاعیه</th>
                                            <th>تعداد بازدید</th>
                                            <th>ایجاد کننده</th>
                                            <th>تاریخ ایجاد</th>
                                            <th>عملیات</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>#</td>
                                            <td><input type="text" class="form-control" name="title"
                                                       value="{{request('title','')}}"></td>
                                            <td></td>
                                            <td><select class="form-control"
                                                        name="user_id">{!! customForeach(collectionUsers(),'user_id','username',request('user_id','')) !!}</select>
                                            </td>
                                            <td></td>
                                            <td>
                                                <button type="submit" class="btn-loading btn btn-primary search-ajax">
                                                    اعمال فیلتر
                                                    <i class="ft-thumbs-up position-right"></i>
                                                </button>
                                            </td>
                                        </tr>
                                        @if($data->count() != 0)
                                            @foreach($data as $row)
                                                <tr>
                                                    <td>{{index($data ,$loop)}}</td>
                                                    <td>{{$row->ann_title}}</td>
                                                    <td>{{$row->ann_view_count}}</td>
                                                    <td>{{$row->user->username}}</td>
                                                    <td>{{jdate_from_gregorian($row->created_at)}}</td>
                                                    <td>
                                                        @can('edit_announcements')
                                                        <a href="{{route('announcement.edit',$row->ann_id)}}"
                                                           title="ویرایش"><i class="fa fa-pencil"></i> </a>
                                                        @endcan
                                                            @can('delete_announcements')
                                                            <a href="{{route('announcement.destroy',$row->ann_id)}}" class="delete-item"
                                                          data-id="{{$row->ann_id}}" title="حذف"><i class="fa fa-trash"></i> </a>
                                                    @endcan
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="6" style="text-align: center;">
                                                    {{ config('first_config.message.empty_table') }}
                                                </td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="card-footer">
                        {{ $data->appends($_GET)->links() }}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{asset('js/functions/deleteAjax.js')}}"></script>
    <script>
        $(document).ready(function () {
            $(".delete-item").click(function (e) {
                var txt= '{{config('first_config.message.confirm-delete')}}';
                e.preventDefault();
                deleteAjax($(this),$(this).attr('href'),$(this).data('id'));
            });
        });
    </script>
@endsection
