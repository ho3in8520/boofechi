@extends('master_page')
<?php
$title = "فاکتورهای وارده";
?>
@section('title_browser',$title)
@section('style')@endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header"><?=$title?></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                    </div>
                    <div class="card-body">
                        <form method="GET" action="{{url()->current()}}">
                            <div class="card-block">
                                <div class="col-md-12 table-responsive">
                                    <div class="summary">{!!getSummary($factors) !!}</div>
                                    <table class="table table-striped table-bordered sourced-data dataTable">
                                        <thead>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>خریدار</th>
                                            <th>تاریخ</th>
                                            <th>شماره فاکتور</th>
                                            <th>مبلغ({{setting('financial_unit')}})</th>
                                            <th>روش پرداخت</th>
                                            <th>وضیعت</th>
                                            <th>عملیات</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <input type="text" class="form-control"
                                                       value="{{ request()->query('collection') }}" name="collection">
                                            </td>
                                            <td>
                                                <input type="text" class="form-control datePicker"
                                                       value="{{ request()->query('factor_date') }}" name="factor_date">
                                            </td>
                                            <td>
                                                <input type="text" class="form-control"
                                                       value="{{ request()->query('factor_no') }}" name="factor_no">
                                            </td>
                                            <td>-</td>
                                            <td>-</td>
                                            <td>-</td>
                                            <td>
                                                <button class="btn btn-primary btn-loading search-ajax"> اعمال فیلتر <i
                                                            class="ft-thumbs-up position-right"></i></button>
                                            </td>
                                        </tr>
                                        @if($factors->count() != 0)
                                            @foreach($factors as $factor)
                                                <tr data-id="{{ $factor->fm_id }}" class="factor-master"
                                                    style="font-weight: {{($factor->far_is_viewed == 0) ? '900':'normal'}}">
                                                    <td>{{index($factors,$loop)}}</td>
                                                    <td>{{$factor->coll_name.'/ '.$factor->prsn_phone1}}</td>
                                                    <td>{{$factor->fm_date}}</td>
                                                    <td>{{$factor->fm_no}}</td>
                                                    <td>{{number_format($factor->fm_amount)}}</td>
                                                    <td>{{$factor->payment_method}}</td>
                                                    <td>
                                                        @if(isset($factor_statuses[$factor->fm_status]))
                                                            <span class="badge text-white" style="background-color: {{$factor_statuses[$factor->fm_status]['color']}}">
                                                                {{$factor_statuses[$factor->fm_status]['value']}}
                                                           </span>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <a href="{{asset('factor/view-input').'/'.$factor->fm_id}}">
                                                            <i class="fa fa-eye"></i>
                                                        </a>
                                                        <a href="{{asset('factor/print-factor').'/'.$factor->fm_id}}">
                                                            <i class="fa fa-print"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="8" style="text-align: center;">
                                                    {{ config('first_config.message.empty_table') }}
                                                </td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                    {!! $factors->appends(request()->query())->render() !!}
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection