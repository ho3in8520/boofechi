@extends('master_page')
<?php
$title = "پرداخت فاکتور شماره " . $factor->fm_no;
?>
@section('title_browser',$title)
@section('main_content')
    <section id="basic-form-layouts" class="basket-items">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header"><?=$title?></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form method="post">
                        @csrf
                        <div class="card-body">
                            <div class="card-block">
                                <table class="table table-bordered" style="text-align: center">
                                    <tr>
                                        <th colspan="5"><h4 class="pull-right">تخفیف روش پرداخت</h4>
                                            <input type="hidden" name="selected_payment_method">
                                        </th>
                                    </tr>
                                    <tr>
                                        <th>انتخاب کنید</th>
                                        <th>روش پرداخت</th>
                                        <th>درصد تخفیف</th>
                                        <th>توضیحات</th>
                                        <th>مبلغ نهایی({{setting('financial_unit')}})</th>
                                    </tr>
                                    @if($payment_methods)
                                        @foreach($payment_methods as $method)
                                            <tr data-amount="{{$factor->fm_amount - floor(calcPercent($factor->fm_amount,$method->cpm_percent)) - $factor->fm_talab_az_ghabl}}"
                                                data-code="{{$method->bas_id}}"
                                                data-user="{{withoutZeros(user('amount'))}}">
                                                <td><input type="radio" name="payment_type" value="{{$method->cpm_id}}">
                                                </td>
                                                <td>{{$method->bas_value}}</td>
                                                <td>%{{withoutZeros($method->cpm_percent)}}</td>
                                                <td>{{$method->cpm_description}}</td>
                                                <td>{{number_format($factor->fm_amount - floor(calcPercent($factor->fm_amount,$method->cpm_percent)) - $factor->fm_talab_az_ghabl)}}</td>
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <td colspan="4">مبلغ قابل پرداخت</td>
                                            <td colspan="1" style="background-color: #28D094;color: #ffffff"
                                                id="payable">0
                                            </td>
                                        </tr>
                                    @else
                                        <tr>
                                            <td colspan="5">
                                                فاقد روش های پرداخت
                                            </td>
                                        </tr>
                                    @endif
                                </table>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="pull-right payment_gateway" style="display: none">
                                            <label>درگاه بانکی</label>
                                            <select class="form-control" name="online_gate">
                                                <option value="0">انتخاب کنید...</option>
                                                @foreach($collection_online_gates as $online_gate)
                                                    <option
                                                        value="{{$online_gate->og_id}}">{{$online_gate->og_title}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="pull-left">
                                            <div id="msg"></div>
                                            <div style="float: left">
                                                <a class="payment"></a>
                                                <a class="btn btn-primary"
                                                   href="{{asset('factor/update-factor').'/'.$id}}">ویرایش
                                                    فاکتور</a>
                                                <a class="btn btn-success btn-loading" id="btnSaveFactor">ثبت</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{asset('/js/functions/storeAjax.js')}}"></script>
    <script>
        $(document).ready(function () {
            var amount = 0;
            $(document).on("click", "#btnSaveFactor", function (e) {
                var elem = $(this);
                e.preventDefault();
                if ($("input[name=payment_type]:checked").val())
                    if (confirm("پرداخت مبلغ " + number_format(amount) + " مورد تایید است؟") == false) {
                        $(elem).html("ثبت");
                        return false;
                    }
                storeAjax($(this), 'POST', 'ثبت', false, 0);
            });
            $("input[name=payment_type]").click(function (e) {
                if ($(this).is(":checked")) {
                    if ($(this).closest("tr").data("code") == 50) {
                        $(".payment_gateway").show();
                    } else {
                        $(".payment_gateway").hide();
                    }
                    $(this).closest('table').find("tr").css("background-color", "#ffffff");
                    $(this).closest("tr").css("background-color", "#dedcdc");
                    amount = $(this).closest("tr").data('amount');
                    user_amount = $(this).closest("tr").data('user');
                    $("#payable").html(number_format(amount));
                    if ($(this).closest("tr").data("code") == 53) {
                        if (amount >{{user('amount')}}) {
                            var payment = amount - user_amount;
                            $("#btnSaveFactor").hide();
                            $("#msg").html("<p class='msg' style='color: red'>مبلغ فاکتور بیشتر از موجودی کیف پول شما است لطفا کیف پول\n" +
                                "خود را شارژ کنید</p>")
                            $(".payment").html('<button type="button" data-factorId="{{$id}}" class="btn btn-info wallet"\n' +
                                'data-amount="' + payment + '">' +
                                'شارژ کیف پول' +
                                '</button>')
                        }
                    } else {
                        $(".msg").hide();
                        $(".btn-info").hide();
                        $("#btnSaveFactor").show();
                    }
                }
            });
        });
    </script>
@append
