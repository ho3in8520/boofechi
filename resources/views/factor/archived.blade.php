@extends('master_page')
<?php
$title = "بایگانی";
$statuses = config('first_config.factor-statuses');
?>
@section('title_browser',$title)
@section('style')@endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header"><?=$title?></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                    </div>
                    <div class="card-body">
                        <form method="GET" action="{{url()->current()}}">
                            <div class="card-block">
                                <div class="col-md-12 table-responsive">
                                    <div class="summary">{!!getSummary($factors) !!}</div>
                                    <table class="table table-striped table-bordered sourced-data dataTable">
                                        <thead>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>خریدار</th>
                                            <th>کاربر عامل</th>
                                            <th>تاریخ</th>
                                            <th>شماره فاکتور</th>
                                            <th>مبلغ({{setting('financial_unit')}})</th>
                                            <th>وضعیت فاکتور</th>
                                            <th>عملیات</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <input type="text" class="form-control"
                                                       value="{{ request()->query('collection') }}" name="collection">
                                            </td>
                                            <td>
                                                <input type="text" class="form-control"
                                                       value="{{ request()->query('agent') }}" name="agent">
                                            </td>
                                            <td>
                                                <input type="text" class="form-control datePicker"
                                                       value="{{ request()->query('factor_date') }}" name="factor_date">
                                            </td>
                                            <td>
                                                <input type="text" class="form-control"
                                                       value="{{ request()->query('factor_no') }}" name="factor_no">
                                            </td>
                                            <td></td>
                                            <td>
                                                <select class="form-control" name="factor_status">
                                                    <option value="">انتخاب کنید...</option>
                                                    @foreach($statuses as $key => $status)
                                                        <option value="{{$key}}">{{$status['value']}}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>
                                                <button class="btn btn-primary btn-loading search-ajax"> اعمال فیلتر <i
                                                            class="ft-thumbs-up position-right"></i></button>
                                            </td>
                                        </tr>
                                        @if($factors->count() != 0)
                                            @foreach($factors as $factor)
                                                <tr data-id="{{ $factor->fm_id }}" class="factor-master">
                                                    <td>{{index($factors,$loop)}}</td>
                                                    <td>{{$factor->coll_name}}</td>
                                                    <td>{{$factor->username}}</td>
                                                    <td>{{$factor->fm_date}}</td>
                                                    <td>{{$factor->fm_no}}</td>
                                                    <td>{{number_format($factor->fm_amount)}}</td>
                                                    <td>
                                                        @if(isset($statuses[$factor->fm_status]))
                                                            <span class="badge text-white" style="background-color: {{$statuses[$factor->fm_status]['color']}}">
                                                                {{$statuses[$factor->fm_status]['value']}}
                                                           </span>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if($mode==1)
                                                            <a title="مشاهده فاکتور"
                                                               href="{{asset('factor/view-input').'/'.$factor->fm_id}}">
                                                                <i class="fa fa-eye"></i>
                                                            </a>
                                                            @if($factor->fm_is_payed == 0)
                                                                <a title="پرداخت فاکتور"
                                                                   href="{{asset('factor/payment').'/'.$factor->fm_id}}">
                                                                    <i class="fa fa-money"></i>
                                                                </a>
                                                            @endif
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="8" style="text-align: center;">
                                                    {{ config('first_config.message.empty_table') }}
                                                </td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                    {!! $factors->appends(request()->query())->render() !!}
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection