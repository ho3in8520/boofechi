@extends('master_page')
<?php
$title = "فاکتورهای صادره";
?>
@section('title_browser',$title)
@section('style')@endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header"><?=$title?></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                    </div>
                    <div class="card-body">
                        <form method="GET" action="{{url()->current()}}">
                            <div class="card-block">
                                <div class="col-md-12 table-responsive">
                                    <div class="summary">{!!getSummary($factors) !!}</div>
                                    <table class="table table-striped table-bordered sourced-data dataTable">
                                        <thead>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>فروشنده</th>
                                            <th>تاریخ</th>
                                            <th>شماره فاکتور</th>
                                            <th>مبلغ({{setting('financial_unit')}})</th>
                                            <th>وضعیت پرداخت</th>
                                            <th>روش پرداخت</th>
                                            <th>عملیات</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <input type="text" class="form-control"
                                                       value="{{ request()->query('collection') }}" name="collection">
                                            </td>
                                            <td>
                                                <input type="text" class="form-control datePicker"
                                                       value="{{ request()->query('factor_date') }}" name="factor_date">
                                            </td>
                                            <td>
                                                <input type="text" class="form-control"
                                                       value="{{ request()->query('factor_no') }}" name="factor_no">
                                            </td>
                                            <td></td>
                                            <td>
                                                <select class="form-control" name="factor_payment">
                                                    <option value="1">انتخاب کنید...</option>
                                                    <option value="2" {{ request()->query('factor_payment') == 2 ? 'selected' : '' }}>
                                                        پرداخت شده
                                                    </option>
                                                    <option value="3" {{ request()->query('factor_payment') == 3 ? 'selected' : '' }}>
                                                        پرداخت نشده
                                                    </option>
                                                </select>
                                            </td>
                                            <td></td>
                                            <td>
                                                <button class="btn btn-primary btn-loading search-ajax"> اعمال فیلتر <i
                                                            class="ft-thumbs-up position-right"></i></button>
                                            </td>
                                        </tr>
                                        @if($factors->count() != 0)
                                            @foreach($factors as $factor)
                                                <tr data-id="{{ $factor->fm_id }}" class="factor-master">
                                                    <td>{{index($factors,$loop)}}</td>
                                                    <td>{{$factor->coll_name}}</td>
                                                    <td>{{$factor->fm_date}}</td>
                                                    <td>{{$factor->fm_no}}</td>
                                                    <td>{{number_format($factor->fm_amount)}}</td>
                                                    <td>{{$factor->fm_is_payed == 1 ? 'پرداخت شده' : 'پرداخت نشده'}}</td>
                                                    <td>{{$factor->payment_method}}</td>
                                                    <td>
                                                        @if($mode==1)
                                                            <a title="مشاهده فاکتور"
                                                               href="{{asset('factor/view-output').'/'.$factor->fm_id}}">
                                                                <i class="fa fa-eye"></i>
                                                            </a>
                                                            @if($factor->fm_is_payed == 0)
                                                                <a title="پرداخت فاکتور"
                                                                   href="{{asset('factor/payment').'/'.$factor->fm_id}}">
                                                                    <i class="fa fa-money-bill-alt"></i>
                                                                </a>
                                                            @endif
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="8" style="text-align: center;">
                                                    {{ config('first_config.message.empty_table') }}
                                                </td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                    {!! $factors->appends(request()->query())->render() !!}
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
