@extends('master_page')
<?php
        $fm_no = @$details[0]->fm_no;
$title = "اقلام شماره فاکتور " . $fm_no;
?>
@section('title_browser',$title)
@section('style')@endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header"><?=$title?></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                    </div>
                    <div class="card-body">
                        <form method="GET">
                            <div class="card-block">
                                <div class="col-md-12 table-responsive">
                                    <div class="summary">{{count($details) }} مورد.</div>
                                    <table class="table table-striped table-bordered sourced-data dataTable">
                                        <thead>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>کالا</th>
                                            <th>قیمت</th>
                                            <th>تعداد</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(count($details) != 0)
                                            <?php
                                            $i = 0;
                                            ?>
                                            @foreach($details as $detail)
                                                <?php
                                                $i++;
                                                ?>
                                                <tr data-count="{{withoutZeros($detail->fd_count)}}"
                                                    data-price="{{withoutZeros($detail->fd_price_buy)}}"
                                                    data-name="{{$detail->prod_name}}" data-id="{{$detail->fd_id}}"
                                                    data-count_per="{{$detail->cp_count_per}}"
                                                    data-measure_label="{{$detail->bas_value}}"
                                                    class="popup-selectable">
                                                    <td>{{$i}}</td>
                                                    <td>{{$detail->prod_name}}</td>
                                                    <td>{{withoutZeros($detail->fd_price_buy)}}</td>
                                                    <td>{{withoutZeros($detail->fd_count)}}</td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="7" style="text-align: center;">
                                                    {{ config('first_config.message.empty_table_return_products') }}
                                                </td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
