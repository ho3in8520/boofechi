@extends('master_page')
<?php
$title = "پرینت فاکتور";
?>
@section('title_browser',$title)
@section('style')
    <link href="https://www.panel.boofechi.com/printfont/stylesheet.css" rel="stylesheet" type="text/css">
    <style>
        .btn-sm, .input-sm {
            height: 30px !important;
        }
        table, tr, td,th,thead,tbody,.table,.print-body table , .print-body tr,.print-body td,.print-body thead,.print-body tbody,.print-body th{
            border: 1px solid #000000 !important;
            font-weight: bolder;
            color: #000000 !important;
            font-family: BYekan !important;
        }
        h4,b,label,.input-group,.form-control{
            font-weight: bolder;
            color: #000000 !important;
            font-family: BYekan !important;
        }
        @media print {
            .app-sidebar {
                display: none !important;
                visibility: hidden;
            }
        }
        .input-sm {
            padding-right: 5px;
            width: 40px !important;
        }
    </style>
@endsection
@section('main_content')
    <section id="basic-form-layouts" class="basket-items">
        <div class="row">
            <div class="col-sm-12 not-show">
                <h2 class="content-header"><?=$title?>
                    <div class="pull-left"><a class="btn btn-primary btn-print btn-block btn-loading" data-title="پرینت فاکتور" data-action="{{url()->current()}}"><i class="fa fa-print"></i></a></div>
                </h2>
            </div>
            <div class="col-sm-12 show" style="display: none">
                <h6>
                    <?=$title . " شماره ".$fm_no.'/ توسط '.user("username").'/ تاریخ '.jdate()->format("Y/m/d | H:i:s")?>
                </h6>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {!! $html !!}
            </div>
        </div>
    </section>
@endsection