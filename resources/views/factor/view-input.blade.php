@extends('master_page')
<?php
$items = $result;
$title = "جزئیات فاکتور";
?>
@section('title_browser',$title)
@section('style')
    <style>
        .btn-sm, .input-sm {
            height: 30px !important;
        }

        .input-sm {
            padding-right: 5px;
            width: 40px !important;
        }
    </style>
@endsection
@section('main_content')
    <section id="basic-form-layouts" class="basket-items">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header"><?=$title?></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="container">
                            <table class="table">
                                <tr>
                                    <th colspan="5"><h4 class="pull-right">مشخصات خریدار</h4>
                                    </th>
                                </tr>
                                <tr>
                                    <td>نام شرکت:</td>
                                    <td>{{$items[array_keys($items)[0]]['coll_name']}}</td>
                                    <td>تلفن:</td>
                                    <td>{{$person_collection->prsn_phone1}}</td>
                                    <td rowspan="2" style="width: 100px;height: 100px"><img class="rounded-circle"
                                                                                            style="width: 100px;height: 100px"
                                                                                            src="{{ getFile(($items[array_keys($items)[0]]['logo'] != "") ? $items[array_keys($items)[0]]['logo'] : 'nologo.png') }}">
                                    </td>
                                </tr>
                                <tr>
                                    <td>آدرس:</td>
                                    <td colspan="3">{{$items[array_keys($items)[0]]['coll_address']}}</td>
                                </tr>
                                <tr>
                                    <td>شماره فاکتور:</td>
                                    <td>{{$items[array_keys($items)[0]]['fm_no']}}</td>
                                    <td>زمان ثبت فاکتور:</td>
                                    <td colspan="2">{{jdate($items[array_keys($items)[0]]['fm_created_at'])->format('Y/m/d ساعت H:i:s')}}</td>
                                </tr>
                                <tr>
                                    <td>توضیحات:</td>
                                    <td colspan="3">{{$items[array_keys($items)[0]]['fm_description']}}</td>
                                </tr>
                            </table>
                        </div>
                        <p class="mb-0"></p>
                    </div>
                    <form method="post">
                        @csrf
                        <div class="card-body">
                            <div class="card-block">
                                <table style="text-align: center !important"
                                       class="table table-striped table-inverse table-bordered table-hover table-factor">
                                    <thead>
                                    <th>ردیف</th>
                                    <th>محصول</th>
                                    <th>مقدار</th>
                                    <th>واحد</th>
                                    <th>وزن</th>
                                    <th>قیمت واحد</th>
                                    <th>قیمت کل</th>
                                    <th>تخفیف</th>
                                    <th>مبلغ کل پس از کسر تخفیف</th>
                                    <th>مالیات بر ارزش افزوده</th>
                                    <th>قیمت کل + مالیات بر ارزش افزوده</th>
                                    </thead>
                                    <tbody>
                                    <tbody>
                                        <?php
                                        $i = $sum_one = $sum_total = $sum_discount = $sum_total_menhaye_discount = $sum_maliat = $sum_total_bealave_maliat = 0;
                                        if($items)
                                        {
                                        foreach ($items as $item)
                                        {
                                        $one = $total = $discount = $total_menhaye_discount = $maliat = $total_bealave_maliat = 0;
                                        $i++;

                                        if($item['fd_ooe'] == 0)
                                        {
                                            $sum_one += $one =  $item['fd_price_buy'];
                                            $sum_total += $total =  $one * $item['fd_count'];
                                            $sum_discount += $discount =  floor(calcPercent($total, $item['fd_discount']));
                                            $sum_total_menhaye_discount += $total_menhaye_discount =  $total - $discount;
                                            $sum_maliat += $maliat =  ($item['fd_arzesh_afzoodeh'] == 1) ? floor(calcPercent($total_menhaye_discount, $tax)) : 0;
                                            $sum_total_bealave_maliat += $total_bealave_maliat =  $total_menhaye_discount + $maliat;
                                        }
                                        else
                                            $one = "اشانتیون";
                                        $factor_discount = withoutZeros(floor(calcPercent($sum_total_bealave_maliat,$items[array_keys($items)[0]]['fm_festival_discount'])));
                                        $financial_unit = setting("financial_unit");
                                        ?>
                                        <tr style="{{($item['fd_ooe'] == 1) ? 'color:red': ''}}">
                                            <td>{{$i}}</td>
                                            <?php
                                            if ($item['fd_ooe'] == 0)
                                                $txt = $item['prod_name'];
                                            else if ($item['fd_ooe_id'] == -1)
                                                $txt = "اشانتیون-فاکتور";
                                            else
                                                $txt = "اشانتیون-محصول";
                                            ?>
                                            <td><b><?=$txt?></b></td>
                                            <td style="width: 200px">
                                                <div class="input-group prod-counter col-sm-offset-2">
                                                    <lable class="form-control prod-count input-sm"
                                                           style="width: 20px">{{$item['fd_count']}}</lable>
                                                </div>
                                            </td>
                                            <td>{{$item['bas_value']}}</td>
                                            <td style="width: 100px">
                                                <div class="input-group prod-counter col-sm-offset-2">
                                                    <?php
                                                    $status = null;
                                                    if ($item['bas_id'] != 34) {
                                                        $status = 'disabled';
                                                    }
                                                    ?>
                                                    <input type="text" class="prod-count input-sm" chars="float" name="fd_amount"
                                                           value="{{round($item['fd_count'],3)}}" style="width: 20px" {{$status}}>

                                                    <div class="input-group-prepend">
                                                        <button type="button" class="btn btn-primary btn-accept btn-sm btn-loading-sm"
                                                                data-bas-id="{{ $item['bas_id'] }}"
                                                                data-id="{{$item['fd_id']}}"
                                                                data-price="{{$one}}" {{ ($item['cp_id']) }} {{$status}}><i
                                                                class="fa fa-check"></i></button>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                {{is_numeric($one) ? number_format($one) : $one}}
                                            </td>
                                            <td>{{number_format($total)}}</td>
                                            <td>{{number_format($discount)}}</td>
                                            <td>{{number_format($total_menhaye_discount)}}</td>
                                            <td>{{number_format($maliat)}}</td>
                                            <td>{{number_format($total_bealave_maliat)}}</td>
                                        </tr>
                                        <?php
                                        }
                                        ?>
                                        <tr>
                                            <td colspan="5">مجموع</td>
                                            <td>{{number_format($sum_one)}}</td>
                                            <td>{{number_format($sum_total)}}</td>
                                            <td>{{number_format($sum_discount)}}</td>
                                            <td>{{number_format($sum_total_menhaye_discount)}}</td>
                                            <td>{{number_format($sum_maliat)}}</td>
                                            <td>{{number_format($sum_total_bealave_maliat)}}</td>
                                        </tr>
                                        <?php
                                        }
                                        ?>
                                        @if($items[array_keys($items)[0]]['fm_is_payed'] == 1)
                                            <?php
                                            $kham = floor(calcPercent(($sum_total_bealave_maliat - $factor_discount),$items[array_keys($items)[0]]['fm_payment_method_percent']));
                                            ?>
                                        @endif
                                        <tr>
                                            <td colspan="7" rowspan="3" style="vertical-align: middle;font-size: 16px"><b>کل مبلغ به حروف:</b> {{numtoword($sum_total_bealave_maliat - $factor_discount - $kham).' '.$financial_unit}}</td>
                                            <td>کل مبلغ فاکتور</td>
                                            <td colspan="3"><b style="font-size:16px">{{number_format($sum_total_bealave_maliat)}}</b></td>
                                        </tr>
                                        <tr>
                                            <td>تخفیف فاکتور
                                                ({{withoutZeros($items[array_keys($items)[0]]['fm_festival_discount'])}}%)
                                            </td>
                                            <td colspan="3">{{number_format($factor_discount)}}</td>
                                        </tr>
                                        <tr class="success">
                                            <td>مبلغ قابل پرداخت</td>
                                            <td colspan="3"><b style="font-size:16px">{{number_format($sum_total_bealave_maliat - $factor_discount)}}</b> {{$financial_unit}}</td>
                                        </tr>
                                        @if($items[array_keys($items)[0]]['fm_is_payed'] == 1)
                                            <tr>
                                                <td colspan="7"></td>
                                                <td>تخفیف روش پرداخت
                                                    ({{withoutZeros($items[array_keys($items)[0]]['fm_payment_method_percent'])}}%)
                                                </td>
                                                <td colspan="3">{{number_format($kham)}}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="7"></td>
                                                <td>مبلغ پرداخت شده</td>
                                                <td colspan="3"><b
                                                            style="font-size:16px">{{number_format($sum_total_bealave_maliat - $factor_discount - $kham)}}</b> {{$financial_unit}}
                                                </td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                                <table class="table table-bordered table-marjooee" style="text-align: center">
                                    @if(count($factor_returns) > 0)
                                        @if(in_array(0,array_column($factor_returns,'fr_type')))
                                            <tr>
                                                <th colspan="5"><h4 class="pull-right">کالاهای مرجوعی از طرف خریدار</h4>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>محصول</th>
                                                <th>تعداد</th>
                                                <th>قیمت</th>
                                                <th>علت</th>
                                                <th>عملیات</th>
                                            </tr>
                                        @endif
                                        @foreach($factor_returns as $return)
                                            @if($return['fr_type'] == 0)
                                                <tr data-id="{{$return['fr_id']}}">
                                                    <td class="selectProduct">
                                                        {{$return['prod_name']}}
                                                    </td>
                                                    <td>

                                                        <div class="input-group prod-counter col-sm-offset-2">
                                                            <div class="input-group-prepend">
                                                                <a class="btn btn-success prod-counter-plus btn-sm"><i
                                                                        class="fa fa-plus"></i></a>
                                                            </div>
                                                            <input type="text" class="prod-count input-sm" name="ret_count"
                                                                   value="{{withoutZeros($return['fr_count'])}}" style="width: 20px">
                                                            <div class="input-group-prepend">
                                                                <a class="btn btn-danger prod-counter-minus btn-sm"><i
                                                                        class="fa fa-minus"></i></a>
                                                            </div>
                                                        </div>


{{--                                                        <input type="text" class="form-control" name="ret_count"--}}
{{--                                                               value="{{withoutZeros($return['fr_count'])}}">--}}
                                                    </td>
                                                    <td>
                                                        <label class="form-control price_buy"
                                                               style="min-width: 150px">{{number_format(withoutZeros($return['fd_price_buy']))}}</label>
                                                    </td>
                                                    <td>
                                                        <lable class="frm-control">{{$return['reason']}}</lable>
                                                    </td>
                                                    <td>
                                                        {{--agar taeed nahaee ya laghv nashode factor--}}
                                                        @if($items[array_keys($items)[0]]['fm_status'] != 2 && $items[array_keys($items)[0]]['fm_status'] != 3 && user("user_id") == $is_viewed_user_id)
                                                            @if($return['fr_is_accepted']== 0)
                                                                <button title="تایید" type="button"
                                                                        class="btn btn-success btn-sm acceptForm">
                                                                    <i class="fa fa-check"></i>
                                                                </button>
                                                            @else
                                                                <button title="عدم تایید" type="button"
                                                                        class="btn btn-danger btn-sm deacceptForm">
                                                                    <i class="fa fa-remove"></i>
                                                                </button>
                                                            @endif
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    @endif
                                    @if(can('last-accept-factor') && $items[array_keys($items)[0]]['fm_self_collection_id'] != collection_id())
                                        @if($items[array_keys($items)[0]]['fm_status'] != 2 && $items[array_keys($items)[0]]['fm_status'] != 3)
                                            <tr>
                                                <th colspan="5"><h4 class="pull-right">کالاهای مرجوعی از طرف
                                                        فروشنده</h4><a class="btn btn-sm pull-left btn-success"
                                                                       id="btn-add-marjooee"
                                                                       title="افزودن"><i
                                                                class="fa fa-plus"></i></a>

                                                </th>
                                            </tr>
                                            <tr>
                                                <th>محصول</th>
                                                <th>تعداد</th>
                                                <th>قیمت</th>
                                                <th>علت</th>
                                                <th>عملیات</th>
                                            </tr>
                                        @endif
                                    @endif
                                </table>
                                @if($items[array_keys($items)[0]]['fm_self_collection_id'] != collection_id())
                                    @if($items[array_keys($items)[0]]['fm_status'] != 2 && $items[array_keys($items)[0]]['fm_status'] != 3 && user("user_id") == $is_viewed_user_id)
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>عملیات</label>
                                                    <select class="form-control" name="operation">
                                                        <option value>انتخاب کنید...</option>
                                                        @foreach($factor_actions as $key => $value)
                                                            @if(can($value['permission']))
                                                                <option value="{{$key}}">{{$value['value']}}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>ارسال به</label>
                                                    <select class="form-control" name="send_to">
                                                        {!! customForeach($collection_roles,'rol_id','rol_label') !!}
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <button class="btn btn-primary btn-loading btn-block" id="btnDoAction"
                                                            style="margin-top: 32px">
                                                        ثبت
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <button type="button" class="btn btn-danger btn-loading bt-block" id="dontRead"
                                                            style="margin-top: 32px">
                                                        خوانده نشده
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <table>
        <tr id="marjooeeForm" class="card border-right my-4 border-primary border-3" style="display: none">
            <td class="selectProduct">
                <div class="input-group">
                    <button type="button" class="btn btn-info showProduct btn-block">محصول
                    </button>
                    <input type="hidden" name="ret_fd_id[]">
                </div>
            </td>
            <td>
                <input type="text" class="form-control" name="ret_count[]">
            </td>
            <td>
                <label class="form-control price_buy" style="min-width: 150px"></label>
            </td>
            <td>
                <select class="form-control" name="ret_reason[]">
                    <option value="">انتخاب کنید...</option>
                    @foreach($return_reasons as $reason)
                        <option value="{{$reason->crr_id}}">{{$reason->bas_value}}</option>
                    @endforeach
                </select>
            </td>
            <td>
                <button type="button" class="btn btn-danger btn-sm deleteForm">
                    <i class="fa fa-trash"></i>
                </button>
            </td>
        </tr>
    </table>
@endsection
@section('script')
    <script src="{{asset('/js/functions/storeAjax.js')}}"></script>
    <script>
        function customeFunctionAfterSelectRow(node) {
            trClick.closest('tr').find("input[name^='ret_count']").val($(node).data('count'));
            trClick.closest('tr').find("label.price_buy").html($(node).data('price'));
        }
        $(document).ready(function () {
            $(document).on("click", "#btnDoAction", function (e) {
                e.preventDefault();
                storeAjax($(this), 'POST', 'اعمال', false, 0);
            });
            $(document).on("click", "#dontRead", function (e) {
                e.preventDefault();
                $.post("/factor/dont-read",{id:{{$id}}},function (data) {
                    var result = JSON.parse(data);
                    if(result.status == 100)
                        window.location.href = result.route;
                });
            });
            $(document).on("click", ".prod-counter-plus", function () {
                var elem = $(this).closest(".prod-counter");
                var count = parseInt($(elem).find("input").val()) + 1;
                $(elem).find("input").attr('value', count);
            });
            $(document).on("click", ".prod-counter-minus", function () {
                var elem = $(this).closest(".prod-counter");
                var count = parseInt($(elem).find("input").val());
                if (count >= 2) {
                    $(elem).find("input").attr('value', count - 1);
                }
            });
            $(document).on('click', '.factor-master', function () {
                var id = $(this).data('id');
                showData('{{asset('/factor/detail-list')}}' + '/' + id);
            });
            $(document).on('click', '.showProduct', function () {
                showData('{{asset('/output-factors').'/'.$items[array_keys($items)[0]]['fm_collection_id']}}');
                trClick = $(this);
            });
            $(document).on("click", "#btn-add-marjooee", function () {
                var elem = $(".table-marjooee tr:last");
                var clone = $("#marjooeeForm").clone();
                clone.counter('محصول');
                $(elem).after("<tr>" + clone.html() + "<tr>");
                $(".table-marjooee tr:last").css("display", 'block');
            });
            $(document).on("click", ".deleteForm", function () {
                if (confirm('اطمینان از حذف این مورد دارید؟') == false)
                    return false;
                $(this).closest("tr").remove();
            });
            $(document).on("click", ".acceptForm", function () {
                var element = $(this);
                if (confirm('اطمینان از تایید این مورد دارید؟') == false)
                    return false;
                var count = $(element).closest("tr").find("input[name=ret_count]").val();
                var id = $(element).closest("tr").data('id');
                $.post("{{asset('factor/accept-return')}}", {id: id, count: count}, function (data) {
                    var result = JSON.parse(data);
                    if (result.status == 100) {
                        toastr.success(result.msg, "موفق");
                        var parent = $(element).parent();
                        $(element).remove();
                        parent.html('<button title="عدم تایید" type="button" class="btn btn-danger btn-sm deacceptForm"><i class="fa fa-remove"></i></button>');
                    }
                });
            });
            $(document).on("click", ".deacceptForm", function () {
                var element = $(this);
                if (confirm('اطمینان از عدم تایید این مورد دارید؟') == false)
                    return false;
                var id = $(element).closest("tr").data('id');
                $.post("{{asset('factor/deaccept-return')}}", {id: id}, function (data) {
                    var result = JSON.parse(data);
                    if (result.status == 100) {
                        toastr.success(result.msg, "موفق");
                        var parent = $(element).parent();
                        $(element).remove();
                        parent.html('<button title="تایید" type="button" class="btn btn-success btn-sm acceptForm"><i class="fa fa-check"></i></button>');
                    }
                });
            });
            $(document).on("click", ".btn-accept", function () {
                var elem = $(this).closest(".prod-counter");
                var value = $(elem).find("input").val();
                var id = $(this).data('id');
                var bas_id = $(this).data('bas-id');

                $.post("{{asset('factor/update-amount')}}", {id: id, amount: value, basId:bas_id}, function (data) {
                    var result = JSON.parse(data)

                    if (result.status == 100) {
                        toastr.success(result.msg, "عملیات با موفقیت انجام گردید");
                        location.reload();
                    } else {
                        toastr.error(result.msg, "نا موفق");
                    }
                });
            })
        });
    </script>
@append
