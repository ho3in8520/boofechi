@extends('master_page')

@section('title_browser') آپلود فایل @endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-sm-12">

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title-wrap bar-success">
                            <h4 class="card-title" id="basic-layout-form">آپلود فایل</h4>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="card-block">
                            <form action="{{ (empty($uploadRoutes) ? route('upload_route.store') : asset('upload_route')) }}"
                                  method="POST" enctype="multipart/form-data">
@include('sample.sample_form_upload')

                                <div class="row">
                                    <div class="col-md-12">
                                        <a href="{{ asset($url) }}" class="btn btn-danger">بازگشت</a>
                                        <button class="btn btn-success" id="uploadFile" type="button">ثبت
                                        </button>
                                        <button class="btn btn-primary addFormUpload" type="button">
                                            افزودن فایل جدید
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('/js/functions/storeAjax.js') }}"></script>
    <script>

        $(document).on('click', '#uploadFile', function () {
            var elementClick = $(this);
            $('.ajaxMessage').html("");
            var has_error = 0;

            $('select').each(function () {
                if ($(this).val() == "") {
                    has_error = 1;
                    var errorHtml = "<div class='help-block text-danger'>نوع فایل را مشخص کنید!</div>";
                    showHtmlValidation($(this), errorHtml);
                }
            });
            
            if (has_error == 0) {
                storeAjax($(this), 'POST', "ثبت", true, 0, 2000);

            }else {
                toastr.error("خطاهای فرم را برطرف نمائید!", "خطا");
            }
        });

    </script>
@append
