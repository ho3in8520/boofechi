@extends("master_page")
<?php
$title = "انتساب نقش";
?>
@section("title_browser")
    <?=$title?>
@endsection
@section("style")@endsection
@section("main_content")
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title-wrap bar-success">
                            <h4 class="card-title"><?=$title?></h4>
                        </div>
                        <p class="mb-0"></p>
                    </div>
                    <div class="card-body">
                        <div class="card-block">
                            <form action="{{route('set-role-to-class')}}" method="POST">
                                {{ csrf_field() }}
                                {!! (!empty($selected_role)) ? "<input name='_method' type='hidden' value='PATCH'>" : ""  !!}
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>نوع کسب و کار</label>
                                            <select class="form-control" name="senf" onchange="showUser(this.value)">
                                                {!! customForeach($senfs,'bas_id','bas_value') !!}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        @foreach($panel_types as $panel_type)
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>{{$panel_type['bas_value']}}</label>
                                                    <select class="form-control"
                                                            data-rol="{{$panel_type['bas_id']}}"
                                                            name="panel[{{$panel_type['bas_id']}}]">
                                                        <option value="0">انتخاب
                                                            کنید...
                                                        </option>
                                                        @foreach($roles as $role)
                                                            <option
                                                                value='{{$role['rol_id']}}'>{{$role['rol_label']}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group" style="margin-top: 30px">
                                            <button type="button" id="savePanelRole"
                                                    class="btn btn-success btn-loading">ثبت
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section("script")
    <script src="{{asset('js/functions/storeAjax.js')}}"></script>
    <script>
        function showUser(str) {
            var response = '';
            $.ajax({
                type: 'post',
                url: "{{asset('/role/get-role-to-class')}}",
                data: {
                    q: str,
                },
                async: false,
                success: function (text) {
                    $('select[name^=panel]').each(function (index) {
                        var elem = $(this);
                        var role = $(elem).data('rol');
                        elem.val(in_array(text.response,role));
                    });
                }
            });
        }
        function in_array(array ,id){
            var check = 0;
            array.forEach(function(data) {
                if (id == data.clr_type_id){
                    check = data.rol_id;
                }
            })
            return check;
        }
        $(document).ready(function () {
            $(document).on("change", ".mySelectionBox", function () {
                var element = $(this);
                console.log($(element).closest("select").data('rol'));
                // alert($(this));
            });
            $("#savePanelRole").click(function (e) {
                e.preventDefault();
                storeAjax($(this), 'POST');
            });
        });
    </script>
@endsection
