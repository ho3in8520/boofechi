@extends('master_page')
<?php
$title = "لیست نقش ها";
?>
@section('title_browser',$title)
@section('style')@endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header"><?=$title?></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="container">

                        </div>
                        <p class="mb-0"></p>
                    </div>
                    <form method="GET" action="{{ asset('/role') }}">
                        <div class="card-body">
                            <div class="card-block">
                                @can('create_role')
                                    <div class="col-md-12">
                                        <div class="pull-left">
                                            <a href="{{route('role.create')}}" class="btn btn-success">ایجاد نقش
                                                جدید</a>
                                        </div>
                                    </div>
                                @endcan
                                <div class="col-md-12 table-responsive">
                                    <div class="summary">{!!getSummary($data) !!}</div>
                                    @csrf
                                    <table class="table table-striped table-bordered sourced-data dataTable">
                                        <thead>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>عنوان فارسی</th>
                                            <th>عنوان انگلیسی</th>
                                            <th>عملیات</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>#</td>
                                            <td><input type="text" class="form-control" name="rol_label"
                                                       value="{{request('rol_label','')}}"></td>
                                            <td><input type="text" class="form-control" name="rol_name"
                                                       value="{{request('rol_name','')}}"></td>
                                            <td>
                                                <button type="submit" class="btn-loading btn btn-primary">
                                                    اعمال فیلتر
                                                    <i class="ft-thumbs-up position-right"></i>
                                                </button>
                                            </td>
                                        </tr>
                                        @if($data->count() != 0)
                                            @foreach($data as $row)
                                                <tr>
                                                    <td>{{index($data ,$loop)}}</td>
                                                    <td>{{$row->rol_label}}</td>
                                                    <td>{{$row->rol_name}}</td>
                                                    <td>
                                                        @can('edit_role')
                                                            <a href="{{route('role.edit',$row->rol_id)}}"
                                                               title="ویرایش"><i class="fa fa-pencil-alt"></i> </a>
                                                        @endcan
                                                        @can('delete_role')
                                                            <a data-action="{{asset('role/delete').'/'.$row->rol_id}}"
                                                               data-confirm="اطمینان از حذف این مورد دارید؟"
                                                               data-method="post"
                                                               class="delete-item"
                                                               data-id="{{$row->rol_id}}" title="حذف"><i
                                                                        class="fa fa-trash"></i> </a>
                                                        @endcan
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="6" style="text-align: center;">
                                                    {{ config('first_config.message.empty_table') }}
                                                </td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="card-footer">
                        {{ $data->appends($_GET)->links() }}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{asset('js/functions/deleteAjax.js')}}"></script>
    <script>
        $(document).ready(function () {
            {{--$(".delete-item").click(function (e) {--}}
            {{--var txt= '{{config('first_config.message.confirm-delete')}}';--}}
            {{--e.preventDefault();--}}
            {{--deleteAjax($(this),$(this).attr('href'),$(this).data('id'));--}}
            {{--});--}}
        });
    </script>
@endsection
