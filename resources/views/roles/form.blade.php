@extends("master_page")
<?php
if (isset($rolPermissions))
    $title = "ویرایش نقش";
else {
    $rolPermissions = [];
    $title = "ثبت نقش";
}
?>
@section("title_browser")
    <?=$title?>
@endsection
@section("style")@endsection
@section("main_content")
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title-wrap bar-success">
                            <h4 class="card-title" id="basic-layout-form"><?=$title?></h4>
                        </div>
                        <p class="mb-0"></p>
                    </div>
                    <div class="card-body">
                        <div class="card-block">
                            <form action="{{ (empty($role) ? route("role.store") : asset("role/".$role->rol_id)) }}"
                                  method="POST">
                                {{ csrf_field() }}
                                {!! (!empty($role)) ? "<input name='_method' type='hidden' value='PATCH'>" : ""  !!}
                                <input type="hidden" name="rol_id" value="{{ (!empty($role) ? $role->rol_id : 0) }}">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>عنوان نقش(فارسی)</label>
                                            <input type="text"
                                                   class="form-control {{ $errors->has("roles.rol_label") ? "error-border" : "" }}"
                                                   name="roles[rol_label]"
                                                   value="{{ old('roles.rol_label',!empty($role) ?  $role->rol_label : '') }}">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>عنوان نقش(انگلیسی)</label>
                                            <input type="text"
                                                   chars="string|50"
                                                   class="form-control {{ $errors->has("roles.rol_name") ? "error-border" : "" }}"
                                                   name="roles[rol_name]"
                                                   value="{{ old('roles.rol_name',!empty($role) ? $role->rol_name : '') }}">
                                        </div>
                                    </div>
                                    @if(in_array(user('panel_type'),[39,46]))
                                        <div class="col-md-3" style="margin-top: 30px">
                                            <div class="form-group">
                                                <label>دریافت کننده تیکت باشد</label>
                                                <input type="checkbox"
                                                       name="roles[receive_tickets]"
                                                       class="switchery form-control activator"
                                                       data-size="xs"
                                                       data-switchery="true"
                                                       @if(!empty($role) && $role->rol_receive_tickets == 1)
                                                       checked
                                                    @endif
                                                >
                                            </div>
                                        </div>
                                    @endif
                                </div>
                                <div class="row" {{--style="display: none"--}}>
                                    <div class="col-12">
                                        <?php
                                        array_multisort(array_map(function ($element) {
                                            return $element['perm_parent_id'];
                                        }, $perms), SORT_ASC, $perms);
                                        ?>
                                        <div class="col-xl-12 col-lg-12">
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="card-title-wrap bar-primary">
                                                        <h4 class="card-title">دسترسی ها</h4>
                                                    </div>
                                                </div>
                                                <div class="card-body">
                                                    <div class="card-block">
                                                        <div class="card collapse-icon accordion-icon-rotate left">
                                                            <div>
                                                                <?php
                                                                showAllPermissions($perms, $rolPermissions);
                                                                ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <button type="button" id="saveRole" class="btn btn-success btn-loading">ثبت
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section("script")
    <script src="{{asset('js/functions/storeAjax.js')}}"></script>
    <script>
        $(document).ready(function () {
//            $(document).on("click",'.selectAll',function (e) {
//                e.preventDefault();
//                alert();
//            });
            $("#saveRole").click(function (e) {
                e.preventDefault();
                storeAjax($(this), 'POST');
            });
        });
    </script>
@endsection

