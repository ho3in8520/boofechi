@extends('master_page')
<?php
$title = "لیست دسته بندی ها";
?>
@section('title_browser',$title)
@section('style')@endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header"><?=$title?></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{--<div class="container">--}}
                            {{--@include('sample.sample_collections_filter_form')--}}
                        {{--</div>--}}
                        {{--<p class="mb-0"></p>--}}
                    </div>
                    <div class="card-body">
                        <div class="card-block">
                            <div class="col-md-12 table-responsive">
                                <div class="summary">{!!\App\Component\Tools::getSummary($data) !!}</div>
                                <table class="table table-striped table-bordered sourced-data dataTable">
                                    <thead>
                                    <tr>
                                        <th>ردیف</th>
                                        <th>نام</th>
                                        <th>والد</th>
                                        <th>نوع</th>
                                        <th>وضعیت</th>
                                        <th>عملیات</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($data)
                                        @foreach($data as $row)
                                            <tr>
                                                <td>{{($data->currentPage() - 1) * $data->perPage() + 1 + $loop->index}}</td>
                                                <td>{{$row->cat_name}}</td>
                                                <td>{{$row->cat_parent_name}}</td>
                                                <td>{{$row->cat_type}}</td>
                                                <td>{{$row->cat_status}}</td>
                                                <td></td>
                                            </tr>
                                        @endforeach
                                    @else
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        {{ $data->appends(request()->query())->links() }}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')@endsection
