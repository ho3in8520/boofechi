@extends('master_page')
@section('title_browser') {{ 'ثبت موجودی' }} @endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header">{{ 'ثبت موجودی' }}</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <form
                                action='{{ (empty($product)) ? route('inventory-factors.store') : route('inventory-factors.update') }}'
                                method='POST'>
                                {{ csrf_field() }}
                                {!! (!empty($product)) ? "<input name='_method' type='hidden' value='PATCH'>" : ''  !!}
                                <div class="row formProduct">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <button type="button" class="btn btn-info showProduct btn-block"
                                                    style="margin-top: 30px">{{ __('validation.attributes')['product_id'] }}</button>
                                            <input type="hidden" name="product_id[]">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>{{ __('validation.attributes')['product_count'] }}</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="count_per[]" disabled>
                                                <input type="text" class="form-control col-2" name="product_count[]">
                                                <input type="text" class="form-control computing" name="computing[]"
                                                       disabled>
                                                <input type="hidden" class="form-control" name="count_product[]">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>{{ __('validation.attributes')['product_price_buy'] }}</label>
                                            <input type="text" class="form-control" name="product_price_buy[]">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>{{ __('validation.attributes')['product_price_consumer'] }}</label>
                                            <input type="text" class="form-control" name="product_price_consumer[]">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group" style="margin-top: 30px">
                                            <button type="button" class="btn btn-danger deleteForm">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                            <button type="button" class="btn btn-primary addForm">
                                                <i class="fa fa-plus"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class='row'>
                                    <div class='col-md-12'>
                                        <div class='form-group'>
                                            <button type="button" class='btn btn-success btn-loading' id="save">ثبت
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script src="{{ asset('/js/functions/storeAjax.js') }}"></script>
    <script>
        function customeFunctionAfterSelectRow(node) {
            trClick.closest('.formProduct').find("input[name^='product_price_consumer']").val($(node).data('pricec'));
            trClick.closest('.formProduct').find("input[name^='product_price_buy']").val($(node).data('priceb'));
            trClick.closest('.formProduct').find("input[name^='count_per']").attr("data-count_per",$(node).data('count_per'));
            if ($(node).data('count_per') > 1)
                trClick.closest('.formProduct').find("input[name^='count_per']").val($(node).data('count_per') + ' عدد در هر ' + $(node).data('packaging_type'));
            else
                trClick.closest('.formProduct').find("input[name^='count_per']").val('عدد');
            trClick.closest('.formProduct').closest("form").find("input[name^='product_id']").each(function () {
                if ($(this).val() == $(node).data('id')) {
                    alert('این محصول قبلا انتخاب شده است!');
                    trClick.closest('.formProduct').remove();
                }
            });

            trClick.closest('.formProduct').find("input[name^='product_count']").val(1);
            trClick.closest('.formProduct').find("input[name^='computing']").val('جمع کل : ' + 1 * $(node).data('count_per') + 'عدد');
            trClick.closest('.formProduct').find("input[name^='count_product']").val(1 * $(node).data('count_per'));
        }
        $(document).on("keyup","input[name^='product_count']",function () {
            var count = $(this).val();
            var count_per = $(this).closest('.formProduct').find("input[name^='count_per']").attr("data-count_per");
            if (count == 0)
                $(this).closest('.formProduct').find("input[name^='computing']").val('');
            else if (count_per > 1) {
                $(this).closest('.formProduct').find("input[name^='computing']").val('جمع کل : ' + count * count_per + 'عدد');
                $(this).closest('.formProduct').find("input[name^='count_product']").val(count * count_per);
            } else {
                $(this).closest('.formProduct').find("input[name^='computing']").val(count + ' عدد ');
                $(this).closest('.formProduct').find("input[name^='count_product']").val(count);
            }
        });
        $(document).on('click', '.deleteForm', function () {
            var elementClick = $(this);
            if (confirm('آیا از حذف فرم اطمینان دارید ؟'))
                if ($(elementClick).closest('.formProduct'))
                    $(elementClick).closest('.formProduct').remove();
                else
                    alert('حذف امکان پذیر نمی باشد');
        });
        $(document).on('click', '.addForm', function () {
            var formHtml = $(this).closest('div.formProduct').clone();
            formHtml.counter('محصول');
            $(this).closest('div.formProduct').after("<div class='row formProduct'>" + formHtml.html() + "</div>");
        });
        $(document).on('click', '#save', function () {
            storeAjax($(this), 'POST')
        });
        $(document).on('click', '.showProduct', function () {
            showData('{{asset("/product")}}');
            trClick = $(this);
        });
    </script>
@endsection
