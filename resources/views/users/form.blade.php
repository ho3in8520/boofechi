@extends('master_page')

@section('title_browser') کاربر @endsection
@section('style')@endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header"> کاربر </h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title-wrap bar-success">

                        </div>
                        <p class="mb-0"></p>
                    </div>
                    <div class="card-body">
                        <div class="card-block">
                            <form action="{{ (empty($user) ? route('user.store') : asset('user/'.$user->user_id) ) }}"
                                  method="post">
                                {{ csrf_field() }}
                                {!! (!empty($user)) ? "<input name='_method' type='hidden' value='PATCH'>" : ''  !!}
                                <input type="hidden" name="user_id" value="{{ empty($user) ? 0 : $user->user_id }}">
                                <input type="hidden" name="prsn_id"
                                       value="{{ request()->has('prsn_id') ? request()->query('prsn_id') : 0  }}">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>{{ __('validation.attributes')['users.username'] }}</label>
                                            <input type="text"
                                                   class="form-control @error('users.username') error-border @enderror "
                                                   name="users[username]"
                                                   value="{{old('users.username',!empty($user) ? $user->username  : '') }}">
                                            @error('users.username')
                                            <div class="help-block text-danger">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>{{  __('validation.attributes')['password'] }}</label>
                                            <input type="password"
                                                   class="form-control @error('password') error-border @enderror"
                                                   name="password">

                                            @error('password')
                                            <div class="help-block text-danger">
                                                {{ $errors->first('password') }}
                                            </div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>{{  __('validation.attributes')['password_confirmation'] }}</label>
                                            <input type="password"
                                                   class="form-control @error('password_confirmation') error-border @enderror"
                                                   name="password_confirmation">

                                            @error('password_confirmation')
                                            <div class="help-block text-danger">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>{{  __('validation.attributes')['users.email'] }}</label>
                                            <input type="text"
                                                   class="form-control @error('users.email') error-border @enderror"
                                                   name="users[email]"
                                                   value="{{ old('users.email',empty($user) ? '' : $user->email) }}">

                                            @error('users.email')
                                            <div class="help-block text-danger">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>{{  __('validation.attributes')['users.is_active'] }}</label>
                                            <input type="checkbox" name="users[is_active]"
                                                   {{ old('users.is_active',empty($user) ? '' : $user->is_active) == 1 ? "checked" : "" }} value="1">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <a href="{{ asset($url) }}" class="btn btn-danger">بازگشت</a>
                                            <button class="btn btn-success btn-loading">ثبت</button>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')@endsection

