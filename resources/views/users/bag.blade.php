@extends('master_page')
<?php
$type_array = [
    ["id" => "52", "name" => "فاکتور حضوری"],
    ["id" => "51", "name" => "فاکتور چکی"],
    ["id" => "1", "name" => "فاکتور از نوع کیف پول"],
    ["id" => "2", "name" => "کسر از کیف پول/افزودن به کیف پول"],
    ["id" => "3", "name" => "فاکتورهای سامانه"],
    ["id" => "4", "name" => "کالای مرجوعی"],
    ["id" => "5", "name" => "لغو فاکتور"],
    ["id" => "6", "name" => "پرداخت پورسانت"],
    ["id" => "7", "name" => "درخواست تسویه"]
];
?>
@section('title_browser') تراکنش های مالی @endsection
@section('style')
    <style>
        .select2-container {
            min-width: 460px;
        }

        .select2-results__option {
            padding-right: 20px;
            vertical-align: middle;
        }

        /*.select2-selection__clear{*/
        /*float: left !important;*/
        /*z-index: 99999999999 !important;*/
        /*}*/

        .select2-results__option:before {
            content: "";
            display: inline-block;
            position: relative;
            height: 20px;
            width: 20px;
            border: 2px solid #e9e9e9;
            border-radius: 4px;
            background-color: #fff;
            margin-right: 20px;
            vertical-align: middle;
        }

        .select2-selection__choice {
            background-color: #ffffff;
        }

        .select2-results__message:before {
            content: "";
            border: 0;
        }

        .select2-results__option[aria-selected=true]:before {
            font-family: fontAwesome;
            content: "\f00c";
            color: #fff;
            background-color: #28D094;
            border: 0;
            display: inline;
            padding-left: 3px;
        }

        .select2-container--default .select2-results__option[aria-selected=true] {
            background-color: #fff;
        }

        .select2-container--default .select2-results__option--highlighted[aria-selected] {
            /*background-color: #eaeaeb;*/
            color: #272727;
        }

        .select2-selection--multiple {
            height: auto !important;
        }

        .select2-container--default .select2-selection--multiple {
            margin-bottom: 10px;
        }

        .select2-container--default.select2-container--open.select2-container--below .select2-selection--multiple {
            border-radius: 4px;
        }

        .select2-container--default.select2-container--focus .select2-selection--multiple {
            border-color: #28D094;
            border-width: 2px;
        }

        .select2-container--default .select2-selection--multiple {
            border-width: 2px;
        }

        .select2-container--open .select2-dropdown--below {

            border-radius: 6px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.5);

        }

        .select2-selection .select2-selection--multiple:after {
            content: 'hhghgh';
        }

        /* select with icons badges single*/
        .select-icon .select2-selection__placeholder .badge {
            display: none;
        }

        .select-icon .placeholder {
            display: none;
        }

        .select-icon .select2-results__option:before,
        .select-icon .select2-results__option[aria-selected=true]:before {
            display: none !important;
            /* content: "" !important; */
        }

        .select-icon .select2-search--dropdown {
            display: none;
        }
    </style>
@endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title-wrap bar-success">
                            <h4 class="card-title" id="basic-layout-form">تراکنش های مالی</h4>
                        </div>
                        <p class="mb-0"></p>
                    </div>
                    <div class="card-body">
                        <div class="card-block">
                            <form method="GET">
                                <div class="row">
                                    <div class="col-md-12 table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th>ردیف</th>
                                                <th>زمان</th>
                                                <th>مبلغ به ({{setting('financial_unit')}})</th>
                                                <th>توضیحات</th>
                                                <th>افزایش/کاهش</th>
                                                <th>نوع تراکنش</th>
                                                <th>عملیات</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>#</td>
                                                <td>

                                                </td>
                                                <td>

                                                </td>
                                                <td>
                                                    <input type="text" name="description" class="form-control" value="{{request()->get("description")}}">
                                                </td>
                                                <td>
                                                    <select class="form-control" name="statusTransacts">
                                                        <option value="0" {{request()->get("statusTransacts") == 0 ? "selected" : ""}}>همه</option>
                                                        <option value="1" {{request()->get("statusTransacts") == 1 ? "selected" : ""}}>افزایش</option>
                                                        <option value="2" {{request()->get("statusTransacts") == 2 ? "selected" : ""}}>کاهش</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control" name="typeTransacts">
                                                        {!! customForeach($type_array,'id','name',request()->get('typeTransacts')) !!}
                                                    </select>
                                                </td>
                                                <td>
                                                    <button type="submit" class="btn-loading btn btn-primary">اعمال
                                                        فیلتر
                                                        <i class="ft-thumbs-up position-right"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            <tr>
                                            @if(count($result) != 0)
                                                @foreach($result as $record)
                                                    <tr>
                                                        <td>{{ (request()->get('page',1) - 1) * 20 + $loop->iteration  }}</td>
                                                        <td>{{ jdate($record->bt_time)->format('Y/m/d H:i:s') }}</td>
                                                        <td>{{ number_format(withoutZeros($record->bt_amount)) }}</td>
                                                        <td>{{ $record->bt_description }}</td>
                                                        <td><?php
                                                            if (!in_array($record->bt_type, [52, 51]))
                                                                echo ($record->bt_status == 0) ? "<span class='badge text-white' style='background-color: #FF4961'>کاهش</span>" : "<span class='badge text-white' style='background-color: #28D094'>افزایش</span>";
                                                            else
                                                                echo "-";
                                                            ?></td>
                                                        <td><?php
                                                            foreach ($type_array as $key => $val) {
                                                                if ($val['id'] === $record->bt_type) {
                                                                    echo $val['name'];
                                                                    break;
                                                                }
                                                            }
                                                            ?>
                                                        </td>
                                                        <td>
                                                            -
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="7" style="text-align: center">
                                                        {{ config('first_config.message.empty_table') }}
                                                    </td>
                                                </tr>
                                            @endif

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

