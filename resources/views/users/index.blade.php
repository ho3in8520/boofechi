@extends('master_page')

@section('title_browser') لیست کاربران @endsection
@section('style')
    <style>
        .select2-container {
            min-width: 460px;
        }

        .select2-results__option {
            padding-right: 20px;
            vertical-align: middle;
        }

        /*.select2-selection__clear{*/
        /*float: left !important;*/
        /*z-index: 99999999999 !important;*/
        /*}*/

        .select2-results__option:before {
            content: "";
            display: inline-block;
            position: relative;
            height: 20px;
            width: 20px;
            border: 2px solid #e9e9e9;
            border-radius: 4px;
            background-color: #fff;
            margin-right: 20px;
            vertical-align: middle;
        }

        .select2-selection__choice {
            background-color: #ffffff;
        }

        .select2-results__message:before{
            content: "";
            border: 0;
        }

        .select2-results__option[aria-selected=true]:before {
            font-family: fontAwesome;
            content: "\f00c";
            color: #fff;
            background-color: #28D094;
            border: 0;
            display: inline;
            padding-left: 3px;
        }

        .select2-container--default .select2-results__option[aria-selected=true] {
            background-color: #fff;
        }

        .select2-container--default .select2-results__option--highlighted[aria-selected] {
            /*background-color: #eaeaeb;*/
            color: #272727;
        }

        .select2-selection--multiple {
            height: auto !important;
        }

        .select2-container--default .select2-selection--multiple {
            margin-bottom: 10px;
        }

        .select2-container--default.select2-container--open.select2-container--below .select2-selection--multiple {
            border-radius: 4px;
        }

        .select2-container--default.select2-container--focus .select2-selection--multiple {
            border-color: #28D094;
            border-width: 2px;
        }

        .select2-container--default .select2-selection--multiple {
            border-width: 2px;
        }

        .select2-container--open .select2-dropdown--below {

            border-radius: 6px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.5);

        }

        .select2-selection .select2-selection--multiple:after {
            content: 'hhghgh';
        }

        /* select with icons badges single*/
        .select-icon .select2-selection__placeholder .badge {
            display: none;
        }

        .select-icon .placeholder {
            display: none;
        }

        .select-icon .select2-results__option:before,
        .select-icon .select2-results__option[aria-selected=true]:before {
            display: none !important;
            /* content: "" !important; */
        }

        .select-icon .select2-search--dropdown {
            display: none;
        }
    </style>
@endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title-wrap bar-success">
                            <h4 class="card-title" id="basic-layout-form">لیست کاربران</h4>
                        </div>
                        <p class="mb-0"></p>
                    </div>
                    <div class="card-body">
                        <div class="card-block">
                            <form method="GET">
                                <div class="row">
                                    <div class="col-md-12 table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th>ردیف</th>
                                                <th>نام و نام خانوادگی</th>
                                                <th>نام کاربری</th>
                                                <th>نام مجموعه</th>
                                                <th>نوع مجموعه</th>
                                                <th>فعالیت</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>#</td>
                                                <td>
                                                    <input type="text" class="form-control" name="full_name"
                                                           value="{{ request()->query('full_name') }}">
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="username"
                                                           value="{{ request()->query('username') }}">
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="coll_name"
                                                           value="{{ request()->query('coll_name') }}">
                                                </td>
                                                <td>
                                                    <select class="form-control" name="typeCollection">
                                                        {!! customForeach($collections,'bas_id','bas_value',request()->query('typeCollection')) !!}
                                                    </select>
                                                </td>
                                                <td>
                                                    <button type="submit" class="btn-loading btn btn-primary">اعمال
                                                        فیلتر
                                                        <i class="ft-thumbs-up position-right"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            <tr>
                                            @if($users->count() != 0)
                                                @foreach($users as $user)
                                                    <tr>
                                                        <td>{{ ($users->currentPage() - 1) * $users->perPage() + $loop->iteration  }}</td>
                                                        <td>{{ $user->full_name }}</td>
                                                        <td>{{ $user->username }}</td>
                                                        <td>{{ $user->coll_name }}</td>
                                                        <td>{{ $user->typeCollection }}</td>
                                                        <td>
                                                            <a class="updateRole" data-id="{{ $user->user_id }}">
                                                                <i class="fa fa-pencil-square-o"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="6" style="text-align: center">
                                                        {{ config('first_config.message.empty_table') }}
                                                    </td>
                                                </tr>
                                            @endif

                                            </tbody>
                                        </table>
                                        {!! $users->appends(request()->query())->render() !!}
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        selectOptionRole = "" +
            "       <select id='selectRole' multiple style='width:200px !important'  class='form-control select2'>" +
            "       </select>" +
            "       <br>" +
            "       <br>" +
            "       <button id='setRole' class='btn btn-success' type='button'>ثبت</button>";
        RlId = "";
        $(document).on('click', '.updateRole', function () {
            id = $(this).attr('data-id');
            RlId = id;
            showModal(" تغییر  نقش", selectOptionRole, "", "direction: rtl");
            $(".select2").select2({
                dir: 'rtl',
            });
            $.get('/getRole', {id: id}, function (response) {
                $('#selectRole').html(JSON.parse(response).roleHtml);
            });
        });
        $(document).on('click', '#setRole', function () {
            var rol = ($(this).closest('.modal-body').find('select').val())
            $.post('/updateRole/' + RlId, {rol: rol}, function (response) {
                var result = JSON.parse(response);
                alert(result.msg);
            });
        })
    </script>
@endsection

