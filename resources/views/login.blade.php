<!DOCTYPE html>
<html lang="en" class="loading">
<head>
    <title>@yield('title')</title>
    @include('layout.header')
</head>
<body data-col="1-column" class=" 1-column  blank-page blank-page">
<div class="wrapper">
    <section id="login">
        <div class="container-fluid">
            <div class="row full-height-vh">
                <div class="col-12 d-flex align-items-center justify-content-center gradient-aqua-marine">
                    <div class="card px-2 py-2 box-shadow-2 width-400">
                        <div class="card-header text-center">
                            <h4 class="text-uppercase text-bold-400 grey darken-1">@yield('title_header')</h4>
                        </div>
                        <div class="card-body">
                            <div class="card-block">
                                @yield('content')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@include('layout.footer')
@yield('script')
</body>
</html>
