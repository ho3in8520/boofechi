@extends('master_page')
@section('title_browser')داشبورد@endsection
@section('style')
    <link rel="stylesheet" type="text/css" href="{{asset('theme/vendors/css/dragula.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset("theme/vendors/css/prism.min.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("theme/vendors/css/chartist.min.css")}}">
    <style>
        .slider {
            border-radius: 10px;
        }
        .alert-news {
            position : relative;
            padding : 0.75rem 1.25rem;
            margin-bottom : 1rem;
            border : 1px solid transparent;
            border-radius : 0.25rem;
        }
        .alert-info-news {
            border-color : #94d5ff !important;
            background-color : #8fd3ff !important;
            color : #053858 !important;
        }
    </style>
@endsection
@section('main_content')
    @if(!empty($result['msg_complet']))
        {!! $result['msg_complet'] !!}
    @endif
    @if(user('panel_type')==42 || user('panel_type')==43)
        @if(announcementSystem()['payment']!=0)
        @else
            <div class="row">
                <div class="col-md-12">
                    <div class="card-body" style="padding-bottom:0px">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            @if(announcementSystem()['payment']==0)
                               <a href="{{route('payment-methods')}}" style="color: white"><u> روش های پرداخت</u> انتخاب نشده است</a>
                                <br>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @endif
    @php
        $panel_type = user('panel_type');
        $dashboard = null;
        switch ($panel_type) {
            case "46":
                    $dashboard = 'dashboard.agent';
                break;
            case "45":
                    $dashboard = 'dashboard.visitor';
                break;
            case "44":
                    $dashboard = 'dashboard.shopkeeper';
                break;
                case "43":
                    $dashboard = 'dashboard.wholesaler';
                break;
                case "42":
                    $dashboard = 'dashboard.company_factory';
                break;
                case "41":
                    $dashboard = 'dashboard.company_factory';
                break;
                case "40":
                    $dashboard = 'dashboard.company_factory';
                break;
                case "39":
                 $dashboard = 'dashboard.admin';
                break;
        }
    @endphp
    @include($dashboard)

@endsection
@section('script')

    <script src="{{asset('js/functions/storeAjax.js')}}"></script>

@append
