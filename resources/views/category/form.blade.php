@extends('master_page')
<?php
$categoryRequest = new \App\Http\Requests\CategoryRequest();
?>
@section('title_browser') {{ (empty($old_cat)) ? 'ثبت دسته بندی' : 'ویرایش دسته بندی' }} @endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header">{{ (empty($old_cat)) ? 'ثبت دسته بندی' : 'ویرایش دسته بندی' }}</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <form action='{{ (empty($old_cat)) ? route('category.store') : route('category.update',$old_cat->cat_id) }}'
                                  method='POST'>
                                {{ csrf_field() }}
                                {!! (!empty($old_cat)) ? "<input name='_method' type='hidden' value='PATCH'>" : ''  !!}
                                <div class='row'>
                                    <div class='col-md-3'>
                                        <div class='form-group'>
                                            {!! ($categoryRequest->rules()['category.cat_senf_id'][0] == 'required') ? "<span class='text-danger'>*</span>" : '' !!}
                                            <label>صنف</label>
                                            <select
                                                    class='form-control {{ $errors->has('category.cat_senf_id') ? 'error-border' : '' }}'
                                                    name='category[cat_senf_id]'>
                                                {!!customForeach($senfs,'bas_id','bas_value',old('cat_senf_id',isset($old_cat) ? $old_cat->cat_senf_id : 0)) !!}
                                            </select>

                                            @if($errors->has('category.cat_senf_id'))
                                                <div class='help-block text-danger'>
                                                    {{ $errors->first('category.cat_senf_id') }}
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class='col-md-2'>
                                        <div class='form-group'>
                                            {!! ($categoryRequest->rules()['category.cat_name'][0] == 'required') ? "<span class='text-danger'>*</span>" : '' !!}
                                            <label>نام دسته بندی</label>
                                            <input type='text'
                                                   class='form-control {{ $errors->has('category.cat_name') ? 'error-border' : '' }}'
                                                   name='category[cat_name]'
                                                   value="{{ old('category.cat_name',empty($old_cat) ?  '' : $old_cat->cat_name) }}"
                                                   autofocus>

                                            @if($errors->has('category.cat_name'))
                                                <div class='help-block text-danger'>
                                                    {{ $errors->first('category.cat_name') }}
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class='col-md-3'>
                                        <div class='form-group'>
                                            {!! ($categoryRequest->rules()['category.cat_parent_id'][0] == 'required') ? "<span class='text-danger'>*</span>" : '' !!}
                                            <label>دسته والد</label>
                                            <select
                                                    class='form-control category {{ $errors->has('category.cat_parent_id') ? 'error-border' : '' }}'
                                                    name='category[cat_parent_id]'>
                                                <option value="0">انتخاب کنید...</option>
                                                {!!createCombobox($categories,0,[old('category.cat_parent_id',isset($old_cat) ? $old_cat->cat_parent_id : 0)]) !!}
                                            </select>

                                            @if($errors->has('category.cat_parent_id'))
                                                <div class='help-block text-danger'>
                                                    {{ $errors->first('category.cat_parent_id') }}
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class='col-md-2'>
                                        <div class='form-group'>
                                            {!! ($categoryRequest->rules()['category.cat_type'][0] == 'required') ? "<span class='text-danger'>*</span>" : '' !!}
                                            <label>نوع</label>
                                            <select
                                                    class='form-control {{ $errors->has('category.cat_type') ? 'error-border' : '' }}'
                                                    name='category[cat_type]'>
                                                <option value="0" {{(isset($old_cat) && $old_cat->cat_type == 0) ? "selected" : ''}}>
                                                    دسته بندی
                                                </option>
                                                <option value="1" {{(isset($old_cat) && $old_cat->cat_type == 1) ? "selected" : ''}}>
                                                    لاین
                                                </option>
                                            </select>

                                            @if($errors->has('category.cat_type'))
                                                <div class='help-block text-danger'>
                                                    {{ $errors->first('category.cat_type') }}
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class='col-md-2'>
                                        <div class='form-group'>
                                            <input type="checkbox"
                                                   name="category[cat_status]"
                                                   {{(isset($old_cat) && $old_cat->cat_status == 1) ? "checked" : ''}} checked>
                                            <label for="staffing">فعال/غیرفعال</label>
                                        </div>
                                    </div>
                                </div>
                                <div class='row'>
                                    <div class='col-md-12'>
                                        <div class='form-group'>
                                            <label></label>
                                            <button class='btn btn-success btn-loading'>ثبت</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            $("select[name='category[cat_senf_id]']").change(function () {
                $.post('{{asset('category/get-categories-by-senf-id').'/'}}' + $(this).val(), function (data) {
                    $("select[name='category[cat_parent_id]']").html(data);
                });
            });
        });
    </script>
@endsection