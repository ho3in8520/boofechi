@extends('master_page')
<?php
$categoryRequest = new \App\Http\Requests\CategoryRequest();
?>
@section('title_browser') {{ (empty($category)) ? 'ثبت دسته بندی' : 'ویرایش دسته بندی' }} @endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header">{{ (empty($category)) ? 'ثبت دسته بندی' : 'ویرایش دسته بندی' }}</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <form action='{{ (empty($category)) ? route('category.store') : route('category.update') }}'
                                  method='POST'>
                                {{ csrf_field() }}
                                {!! (!empty($category)) ? "<input name='_method' type='hidden' value='PATCH'>" : ''  !!}
                                <div class='row'>
                                    <div class='col-md-2'>
                                        <div class='form-group'>
                                            {!! ($categoryRequest->rules()['category.cat_name'][0] == 'required') ? "<span class='text-danger'>*</span>" : '' !!}
                                            <label>نام دسته بندی</label>
                                            <input type='text'
                                                   class='form-control {{ $errors->has('category.cat_name') ? 'error-border' : '' }}'
                                                   name='category[cat_name]'
                                                   value="{{ old('category.cat_name',empty($category) ?  '' : $category->cat_name) }}">

                                            @if($errors->has('category.cat_name'))
                                                <div class='help-block text-danger'>
                                                    {{ $errors->first('category.cat_name') }}
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class='col-md-2'>
                                        <div class='form-group'>
                                            {!! ($categoryRequest->rules()['category.cat_parent_id'][0] == 'required') ? "<span class='text-danger'>*</span>" : '' !!}
                                            <label>دسته والد</label>
                                            <select
                                                    class='form-control select2 {{ $errors->has('category.cat_parent_id') ? 'error-border' : '' }}'
                                                    name='category[cat_parent_id]'>
                                                {!!\App\Component\Tools::customForeach($categories,'cat_id','cat_name',old('category.cat_parent_id',empty($category) ?  '' : $category->cat_parent_id)) !!}
                                            </select>

                                            @if($errors->has('category.cat_parent_id'))
                                                <div class='help-block text-danger'>
                                                    {{ $errors->first('category.cat_parent_id') }}
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class='col-md-2'>
                                        <div class='form-group'>
                                            {!! ($categoryRequest->rules()['category.cat_type'][0] == 'required') ? "<span class='text-danger'>*</span>" : '' !!}
                                            <label>نوع</label>
                                            <select
                                                    class='form-control select2 {{ $errors->has('category.cat_type') ? 'error-border' : '' }}'
                                                    name='category[cat_type]'>
                                                {!!\App\Component\Tools::customForeach($types,'bas_id','bas_value',old('category.cat_type',empty($category) ?  '' : $category->cat_parent_id)) !!}
                                            </select>

                                            @if($errors->has('category.cat_type'))
                                                <div class='help-block text-danger'>
                                                    {{ $errors->first('category.cat_type') }}
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class='col-md-2'>
                                        <div class='form-group'>
                                            <input type="checkbox" name="category[cat_status]">
                                            <label for="staffing">فعال/غیرفعال</label>
                                        </div>
                                    </div>
                                </div>
                                <div class='row'>
                                    <div class='col-md-12'>
                                        <div class='form-group'>
                                            <label></label>
                                            <button class='btn btn-success btn-loading'>ثبت</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection