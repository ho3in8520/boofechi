@extends('master_page')
<?php
$title = "لیست دسته بندی ها";
?>
@section('title_browser',$title)
@section('style')@endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header"><?=$title?></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{--<div class="container">--}}
                        {{--@include('sample.sample_collections_filter_form')--}}
                        {{--</div>--}}
                        {{--<p class="mb-0"></p>--}}
                    </div>
                    <div class="card-body">
                        <div class="card-block">
                            <div class="col-md-12 table-responsive">
                                <div class="summary">{!!getSummary($data) !!}</div>
                                <form method="get">
                                    <table class="table table-striped table-bordered sourced-data dataTable">
                                        <thead>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>صنف</th>
                                            <th>نام</th>
                                            <th>والد</th>
                                            <th>نوع</th>
                                            <th>وضعیت</th>
                                            <th>عملیات</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>#</td>
                                            <td><select
                                                        class='form-control'
                                                        name='cat_senf_id'>
                                                    <option value="0">انتخاب کنید...</option>
                                                    {!!createCombobox($senfs,0,request('cat_senf_id','')) !!}
                                                </select></td>
                                            <td><input type="text" class="form-control" name="cat_name"
                                                       value="{{request('cat_name','')}}"></td>
                                            <td><select
                                                        class='form-control category'
                                                        name='cat_parent_id'>
                                                    <option value="0">انتخاب کنید...</option>
                                                    {!!createCombobox($categories,0,request('cat_parent_id','')) !!}
                                                </select></td>
                                            <td>
                                                <select
                                                        class='form-control'
                                                        name='cat_type'>
                                                    <option value="">
                                                        همه
                                                    </option>
                                                    <option value="0" {{(old('cat_type') == 0) ? "selected" : ''}}>
                                                        دسته بندی
                                                    </option>
                                                    <option value="1" {{(old('cat_type') == 1) ? "selected" : ''}}>
                                                        لاین
                                                    </option>
                                                </select>
                                            </td>
                                            <td></td>
                                            <td>
                                                <button type="submit" class="btn-loading btn btn-primary">
                                                    اعمال فیلتر
                                                    <i class="ft-thumbs-up position-right"></i>
                                                </button>
                                            </td>
                                        </tr>
                                        @if($data)
                                            @foreach($data as $row)
                                                <tr>
                                                    <td>{{index($data,$loop)}}</td>
                                                    <td>{{$row->senf}}</td>
                                                    <td>{{$row->cat_name}}</td>
                                                    <td>{{$row->cat_parent_name}}</td>
                                                    <td>{{$row->cat_type}}</td>
                                                    <td>{{$row->cat_status}}</td>
                                                    <td>
                                                        @can('edit_category')
                                                            <a href="{{route('category.edit',$row->cat_id)}}"
                                                               title="ویرایش"><i
                                                                        class="fa fa-pencil"></i></a>
                                                        @endcan
                                                        @can('delete_category')
                                                            <a href="{{route('category.destroy',$row->cat_id)}}"
                                                               class="delete-item"
                                                               data-id="{{$row->cat_id}}" title="حذف"><i
                                                                        class="fa fa-trash"></i> </a>
                                                        @endcan
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                        @endif
                                        </tbody>
                                    </table>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        {{ $data->appends($_GET)->links() }}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{asset('js/functions/deleteAjax')}}"></script>
    <script>
        $(document).ready(function () {
            $(".delete-item").click(function (e) {
                e.preventDefault();
                deleteAjax($(this), $(this).attr('href'), $(this).data('id'));
            });
        });
    </script>

@endsection
