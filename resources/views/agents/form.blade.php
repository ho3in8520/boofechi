@extends('master_page')
@section('title_browser') ثبت نماینده @endsection
@section('style')
    <link rel="stylesheet" type="text/css" href="{{asset("/theme/vendors/css/sweetalert2.min.css")}}">
@endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header">ثبت نماینده</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title-wrap bar-success">

                        </div>
                        <p class="mb-0"></p>
                    </div>
                    <div class="card-body">
                        <div class="card-block">
                            <form action="" method="post">
                                <h5>** احرازهویت</h5>
                                <hr>
                                @include('sample.sample_form_identity')
                                <h5>** مشخصات کسب و کار</h5>
                                <hr>
                                @include('sample.sample_form_job')
                                <h5>** مدارک کسب و کار</h5>
                                <hr>
                                @if($upload_find)
                                    <div class="col-md-12 table-responsive">
                                        <table class="table table-striped table-bordered sourced-data dataTable">
                                            <thead>
                                            <tr>
                                                <th>ردیف</th>
                                                <th>نمایش مدرک</th>
                                                <th>نوع مدرک</th>
                                                <th>تاریخ ثبت</th>
                                                <th>عملیات</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if($upload_find->count() != 0)
                                                @foreach($upload_find as $row)
                                                    <tr data-image="{{getFile($row->ur_path)}}">
                                                        <td>{{$loop->index +1}}</td>
                                                        <td><a class="show"> نمایش</a></td>
                                                        <td>{{$row->bas_value}}</td>
                                                        <td>{{jdate_from_gregorian($row->created_at)}}</td>
                                                        <td style="text-align: center">
                                                            <a class="primary"
                                                               data-action="{{asset('/delete-agentUpload'.'/'.$row->ur_id)}}"
                                                               data-confirm="اطمینان از حذف این مورد دارید؟"
                                                               data-method="delete">
                                                                <i class="fa fa-trash"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="7" style="text-align: center;">
                                                        {{ config('first_config.message.empty_table') }}
                                                    </td>
                                                </tr>

                                                <tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                @endif
                                @include('sample.sample_form_upload')
                                <div class="row">
                                    <div class="col-md-12">
                                        <button class="btn btn-primary addFormUpload" type="button">
                                            افزودن فایل جدید
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                </div>
                                <button type="button" class="btn btn-success btn-loading" id="create">ثبت</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('/js/functions/storeAjax.js') }}"></script>
    <script src="{{asset("/theme/vendors/js/sweetalert2.min.js")}}"></script>

    <script>
        $(document).ready(function () {
            $(document).on("click", ".show", function () {
                var element = $(this);
                swal({
                    imageUrl: $(element).closest("tr").data("image"),
                    imageWidth: 400,
                    imageHeight: 200,
                    imageAlt: 'Custom image',
                    animation: false
                });
            });


            $(document).on('click', '#create', function () {
                storeAjax($(this), 'POST', captionButton = "ثبت", progress = false, reloadPage = true, TimeOutActionNextStore = 2000, true)
            });
        });
    </script>
@append