@extends('master_page')
<?php
$title = "لیست نماینده ها";
?>
@section('title_browser',$title)
@section('style')
    <link rel="stylesheet" type="text/css" href="{{asset("/theme/vendors/css/sweetalert2.min.css")}}">
@endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header"><?=$title?></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="container">

                        </div>
                        <p class="mb-0"></p>
                    </div>
                    <form method="get" action="">
                        <div class="card-body">
                            <div class="card-block">
                                <div class="col-md-12 table-responsive">
                                    <table class="table table-striped table-bordered sourced-data dataTable">
                                        <thead>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>نام نماینده</th>
                                            <th>نام کاربری</th>
                                            <th>نام دامنه</th>
                                            <th>تاریخ ثبت</th>
                                            <th>وضعیت</th>
                                            <th>عملیات</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>#</td>
                                            <td>
                                                <input type="text" name="name" class="form-control"
                                                       value="{{request()->query('name')}}">
                                            </td>
                                            <td>
                                                <input type="text" name="username" class="form-control"
                                                       value="{{request()->query('username')}}">
                                            </td>
                                            <td>
                                                <input type="text" name="domain" class="form-control"
                                                       value="{{request()->query('domain')}}">
                                            </td>

                                            <td>
                                                <input type="text" name="date_from" class="form-control datePicker"
                                                       value="{{request()->query('date_from')}}">
                                            </td>
                                            <td>
                                                <select class="form-control" name="status">
                                                    <option value="">انتخاب کنید...</option>
                                                    <option value="0" {{request()->query('status') == '0' ? 'selected' : ''}}>
                                                        غیر فعال
                                                    </option>
                                                    <option value="1" {{request()->query('status') == '1' ? 'selected' : ''}}>
                                                        فعال
                                                    </option>
                                                </select>
                                            </td>

                                            <td>
                                                <button type="submit"
                                                        class="btn-loading btn btn-primary search-ajax">
                                                    اعمال فیلتر <i class="ft-thumbs-up position-right"></i>
                                                </button>
                                            </td>
                                        </tr>
                                        @if($result->count() != 0)
                                            @foreach($result as $row)
                                                <tr>
                                                    <td>{{index($result,$loop)}}</td>
                                                    <td>{{$row->prsn_name}} {{$row->prsn_family}}</td>
                                                    <td>{{$row->username}}</td>
                                                    <td>{{$row->ag_domain}}</td>
                                                    <td>{{jdate_from_gregorian($row->created_at,('Y/m/d'))}}</td>
                                                    <td>  @if($row->is_active==1)
                                                            {{'فعال'}}
                                                        @else
                                                            {{'غیرفعال'}}
                                                        @endif
                                                    </td>
                                                    <td style="text-align: center" data-id="{{($row->user_id)}}">
                                                        <a class=""
                                                           href="{{asset('edit-agent'.'/'.$row->user_id)}}"
                                                           data-original-title="" title="ویرایش">
                                                            <i class="fa fa-pencil"></i>
                                                        </a>
                                                        <a class="changePass" href="#"
                                                           data-original-title="" title="تغییر رمز عبور">
                                                            <i class="fa fa-lock"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="7" style="text-align: center;">
                                                    {{ config('first_config.message.empty_table') }}
                                                </td>
                                            </tr>
                                        @endif
                                        <tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        {!! $result->appends(request()->query())->render() !!}
                    </form>
                    <div class="card-footer">
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('/js/functions/storeAjax.js') }}"></script>
    <script src="{{asset("/theme/vendors/js/sweetalert2.min.js")}}"></script>

    <script>
        $(document).ready(function () {
            $(document).on("click", ".changePass", function () {
                var element = $(this);
                swal({
                    title: "رمزعبور",
                    text: "رمزعبور جدید را وارد کنید:",
                    input: "password",
                    showCancelButton: true,
                    animation: "slide-from-top",
                    confirmButtonText: 'ثبت',
                    cancelButtonText: "انصراف",
                    required: true,
                }).then(function (inputValue) {
                    if (inputValue === false) return false;
                    if (inputValue === "") {
                        swal("خطا!", "فیلد رمز عبور نمی تواند خالی باشد!", "error")
                        return false
                    }
                    var id = $(element).closest("td").data('id');
                    $.post("{{asset('/change-agentpass')}}", {id: id, password: inputValue}, function (data) {
                        var result = JSON.parse(data);
                        if (result.status == 100) {
                            toastr.success(result.msg, "موفق");
                            setTimeout(function () {
                                2000
                            });
                        }
                    });
                }).done();
            });
        });
    </script>
@endsection