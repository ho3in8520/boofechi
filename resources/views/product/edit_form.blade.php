@extends('master_page')
<?php
$productRequest = new \App\Http\Requests\ProductRequest();
?>
@section('style')
    <style>
        .formDiscount {
            display: none;
        }

        #deleteImage {
            position: relative;
        }

        #deleteImageUpload {
            font-size: 35px;
            margin-left: 10px;
            z-index: 1;
            cursor: pointer;
            position: absolute;
        }

        .img-container {
            direction: ltr !important;
        }

        .cropper-container {
            direction: ltr !important;
        }

        .cropper-wrap-box, .cropper-canvas {
            direction: ltr !important;
        }

        .cropper-crop-box {
            direction: ltr !important;
        }

        .cropper-view-box {
            direction: ltr !important;
        }

        .preview {
            overflow: hidden;
            width: 160px;
            height: 160px;
            margin: 10px;
            border: 1px solid red;
            direction: ltr !important;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/plugin/cropper/cropper.css') }}">

@endsection
@section('title_browser') {{ (empty($product)) ? 'ثبت محصول' : 'ویرایش محصول' }} @endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header">{{ (empty($product)) ? 'ثبت محصول' : 'ویرایش محصول' }}</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <form
                                action='{{route('product.update',$product->cp_id) }}'
                                method='post' enctype="multipart/form-data">
                                @method('patch')
                                {{ csrf_field() }}
                                <input type="hidden" name="backUrl" value="{{ $backUrl }}">
                                <div class='row'>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <div id="deleteImage">
                                            </div>
                                            <input accept="image/jpeg" name="file" type="file" id="imgInp"
                                                   style="display: none">
                                            <img title="آپلود عکس" style="cursor:pointer;" id="blah"
                                                 src="{{getFile($product->image,'product')}}"
                                                 alt="your image" width="150px" height="150px">

                                        </div>
                                    </div>
                                </div>
                                <div class='row'>
                                    <div class='col-md-3'>
                                        <div class='form-group'>
                                            {!! ($productRequest->rules()['product.prod_name'][0] == 'required') ? "<span class='text-danger'>*</span>" : '' !!}
                                            <label>{{ __('validation.attributes') ['product.prod_name']}}</label>
                                            <input type='text'
                                                   class='numberFormat form-control'
                                                   name='product[prod_name]'
                                                   value="{{ old('product.prod_name',empty($product) ?  '' : $product->prod_name) }}">
                                        </div>
                                    </div>
                                    <div class='col-md-3'>
                                        <div class='form-group'>
                                            {!! ($productRequest->rules()['product.prod_category_id'][0] == 'required') ? "<span class='text-danger'>*</span>" : '' !!}
                                            <label>{{ __('validation.attributes' )['product.prod_category_id']}}</label>
                                            <select class='form-control category'
                                                    name='product[prod_category_id]'>
                                                {!! createCombobox($categories,0,$product->prod_category_id) !!}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <?php $packaging_type = \App\Baseinfo::whereBasType('packaging_type')->where('bas_parent_id', '<>', '0')->get() ?>
                                        <div class="form-group">
                                            {!! ($productRequest->rules()['collProduct.cp_packaging_type'][0] == 'required') ? "<span class='text-danger'>*</span>" : '' !!}
                                            <label>{{ __('validation.attributes')['collProduct.cp_packaging_type'] }}</label>
                                            <select id="uni_measurement" class="form-control select2"
                                                    name="collProduct[cp_packaging_type]">
                                                {!! customForeach($packaging_type,'bas_id','bas_value',@$product->cp_packaging_type) !!}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <?php $weight_unit = \App\Baseinfo::whereBasType('measure_unit')->where('bas_parent_id', '<>', '0')->get() ?>
                                        <div class="form-group">
                                            {!! ($productRequest->rules()['collProduct.cp_measurement_unit_id'][0] == 'required') ? "<span class='text-danger'>*</span>" : '' !!}
                                            <label>{{ __('validation.attributes')['collProduct.cp_measurement_unit_id'] }}</label>
                                            <select id="uni_measurement" class="form-control select2"
                                                    name="collProduct[cp_measurement_unit_id]">
                                                {!! customForeach($weight_unit,'bas_id','bas_value',@$product->cp_measurement_unit_id) !!}
                                            </select>
                                        </div>
                                    </div>
                                    @if(empty($product))
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label id="counter">تعداد</label>
                                            <input type="text" class="form-control" name="collProduct[cp_count_per]"
                                                   value="{{ old('collProduct.cp_count_per',empty($product) ?  '' : $product->cp_count_per) }}">
                                        </div>
                                    </div>
                                        @endif
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label id="counter">تعداد</label>
                                            <input type="text" class="form-control" name="collProduct[cp_count_per]"
                                                   value="{{ empty($product) ?  '' : $product->cp_count_per }}">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            {!! ($productRequest->rules()['collProduct.cp_arzesh_afzoodeh'][0] == 'required') ? "<span class='text-danger'>*</span>" : '' !!}
                                            <input type="checkbox"
                                                   name="collProduct[cp_arzesh_afzoodeh]"
                                                   value="1" @if(@$product->cp_arzesh_afzoodeh==1)
                                                   checked
                                            @else
                                                @endif
                                            >
                                            <label>{{ __('validation.attributes')['collProduct.cp_arzesh_afzoodeh'] }}</label>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <input type="checkbox" name="collProduct[cp_sale_once]"@if(@$product->cp_sale_once==1)
                                            checked
                                            @else
                                                @endif
                                            >
                                            <label>{{ __('validation.attributes')['collProduct.sale_once'] }}</label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>{{ __('validation.attributes')['collProduct.cp_default_price_buy'] }}</label>
                                            (<label id="with_arzesh_afzoodeh">0</label>)
                                            <input type="text" class="form-control numeric"
                                                   value="{{old('collProduct.cp_default_price_buy',empty($product)?0:withoutZeros($product->cp_default_price_buy))}}"
                                                   name="collProduct[cp_default_price_buy]">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>{{ __('validation.attributes')['collProduct.cp_default_price_consumer'] }}</label>
                                            <input type="text" class="form-control numeric"
                                                   value="{{old('collProduct.cp_default_price_consumer',empty($product)?0:withoutZeros($product->cp_default_price_consumer))}}"
                                                   name="collProduct[cp_default_price_consumer]">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label style="font-size: 16px !important" id="counter">کد محصول حسابداری</label>
                                            <input type="text" class="form-control" name="collProduct[cp_code_hesabdari_api]"
                                                   value="{{ empty($product) ?  '' : $product->cp_code_hesabdari_api }}">
                                        </div>
                                    </div>
                                </div>

                                <h3>وزن و ابعاد</h3>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-3">
                                        {!! ($productRequest->rules()['collProduct.cp_weight_of_each'][0] == 'required') ? "<span class='text-danger'>*</span>" : '' !!}
                                        <label>{{ __('validation.attributes')['collProduct.cp_weight_of_each'] }}</label>
                                        <div class="input-group">
                                            <input type="text"
                                                   class="form-control"
                                                   name="collProduct[cp_weight_of_each]"
                                                   value="{{old('collProduct.cp_weight_of_each',empty($product)?'0':withoutZeros($product->cp_weight_of_each))}}">
                                            <select name="collProduct[cp_weight_unit]"
                                                    class="form-control methodSelectProduct">
                                                {!! customForeach($weight_units,'bas_id','bas_value',@$product->cp_weight_unit) !!}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label>{{ __('validation.attributes')['collProduct.cp_dimension_length'] }}</label>
                                        <div class="input-group">
                                            <input type="text"
                                                   class="form-control"
                                                   name="collProduct[cp_dimension_length]"
                                                   value="{{old('collProduct.cp_dimension_length',empty($product)?'':number_format($product->cp_dimension_length))}}"
                                                   placeholder="طول">
                                            <input type="text"
                                                   class="form-control"
                                                   name="collProduct[cp_dimension_width]"
                                                   value="{{old('collProduct.cp_dimension_width',empty($product)?'':number_format($product->cp_dimension_width))}}"
                                                   placeholder="عرض">
                                            <select name="collProduct[cp_width_unit]"
                                                    class="form-control methodSelectProduct">
                                                {!! customForeach($width_unit,'bas_id','bas_value',@$product->cp_width_unit) !!}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <h3>تخفیف/اشانتیون</h3>
                                <hr>
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>هیچکدام</label>
                                            <input class="discount" checked type="radio" name="type" value="3">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>تخفیف</label>
                                            <input class="discount" type="radio" name="type" value="1"
                                                   @if($product->have_discount > 0)
                                                   checked
                                            @else
                                                @endif>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>اشانتیون</label>
                                            <input class="discount" id="eshant" type="radio" name="type" value="2"
                                                   @if($product->have_eshant == -1)
                                                   checked
                                            @else
                                                @endif>
                                        </div>
                                    </div>
                                </div>
                                <div class="showEshant">
                                    <div class="row showFormDiscount DiscountForm"
                                         @if($product->have_discount > 0) @else style="display: none" @endif>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                {!! ($productRequest->rules()['pc_discount_percent.*'][0] == 'required') ? "<span class='text-danger'>*</span>" : '' !!}
                                                <label>درصد تخفیف</label>
                                                <input type="text" class="form-control"
                                                       name="pc_discount_percent[]"
                                                       value="{{old('pc_discount_percent.*',empty($product)?'':@$eshant->pc_discount_percent)}}">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>تاریخ از</label>
                                                <input type="text" class="form-control datePicker"
                                                       name="pc_date_from_dis[]"
                                                       value="{{old('pc_date_from_dis.*',empty(@$eshant)?'': jdate_from_gregorian(@$eshant->pc_date_from,'Y/m/d'))}}">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>تاریخ تا</label>
                                                <input type="text" class="form-control datePicker"
                                                       name="pc_date_to_dis[]"
                                                       value="{{old('pc_date_to_dis.*',empty(@$eshant)?'': jdate_from_gregorian(@$eshant->pc_date_to,'Y/m/d'))}}">

                                            </div>
                                        </div>
                                    </div>
                                    <div class="row eshantionForm"
                                         @if($product->have_eshant == -1) @else style="display: none" @endif>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                {!! ($productRequest->rules()['pc_buy_count.*'][0] == 'required') ? "<span class='text-danger'>*</span>" : '' !!}
                                                <label>تعداد خرید</label>
                                                <input type="text" class="form-control" name="pc_buy_count[]"
                                                       value="{{old('pc_buy_count.*',empty(@$eshant)?'':withoutZeros(@$eshant->pc_buy_count))}}">

                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                {!! ($productRequest->rules()['pc_free_count.*'][0] == 'required') ? "<span class='text-danger'>*</span>" : '' !!}
                                                <label>تعداد اشانت</label>
                                                <input type="text" class="form-control" name="pc_free_count[]"
                                                       value="{{old('pc_free_count.*',empty(@$eshant)?'':withoutZeros(@$eshant->pc_free_count))}}">
                                            </div>
                                        </div>
                                        <div class="col-md-3 selectProduct">
                                            <label>انتخاب محصول</label>
                                            <div class="input-group">
                                                <select name="pc_method_select_product[]"
                                                        class="form-control methodSelectProduct">
                                                    @if(@$eshant->pc_free_collection_product_id==$product->cp_id)
                                                        <option value="1" selected>محصول جاری</option>
                                                        <option value="2">محصول دیگر</option>
                                                    @elseif(@$eshant->pc_free_collection_product_id!=0)
                                                        <option value="1">محصول جاری</option>
                                                        <option value="2" selected>محصول دیگر</option>
                                                    @else
                                                        <option value="1" selected>محصول جاری</option>
                                                        <option value="2">محصول دیگر</option>
                                                    @endif

                                                </select>
                                                @if(!@$eshant->eshant)
                                                    <button type="button" class="btn btn-info showProduct" data-url="{{asset('/product')}}"
                                                            style="display: none">محصول
                                                    </button>
                                                    <input type="hidden" name="pc_free_collection_product_id[]">
                                                @elseif(@$eshant->eshant!=$product->prod_name)

                                                        <button type="button"
                                                                class="btn btn-info showProduct">
                                                            {{empty(@$eshant)?'محصول':@$eshant->eshant}}
                                                        </button>

                                                    <input type="hidden" name="pc_free_collection_product_id[]"
                                                           value="{{old('pc_free_count.*',empty(@$eshant)?'':@$eshant->pc_free_collection_product_id)}}">
                                                @else
                                                    <button type="button" class="btn btn-info showProduct"
                                                            style="display: none">محصول
                                                    </button>
                                                    <input type="hidden" name="pc_free_collection_product_id[]">
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <label>تاریخ</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control datePicker" name="pc_date_from[]"
                                                       value="{{old('pc_date_from.*',empty(@$eshant)?'': jdate_from_gregorian(@$eshant->pc_date_from,'Y/m/d'))}}"
                                                       placeholder="تاریخ از">
                                                <input type="text" class="form-control datePicker" name="pc_date_to[]"
                                                       value="{{old('pc_date_to.*',empty(@$eshant)?'': jdate_from_gregorian(@$eshant->pc_date_to,'Y/m/d'))}}"
                                                       placeholder="تاریخ تا">
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <label>محاسبه مازاد</label>
                                            <input type="checkbox" style="margin-top: 0px !important;"
                                                   name="pc_overflow[]" @if(@$eshant->pc_overflow==1)
                                                   checked
                                            @else
                                                @endif>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="input-group">
                                                <button type="button" class="btn btn-danger btn-sm deleteForm">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                                <button type="button" class="btn btn-primary btn-sm addForm">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <button id="createProduct" class="btn btn-success btn-loading" type="button">
                                            ثبت
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel">برش تصویر</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="img-container">
                        <img id="image" src="" alt="Picture" style="max-width: 100%">
                        <input type="hidden" id="dataGetter">
                    </div>
                </div>
                <div class="preview"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">انصراف</button>
                    <button type="button" class="btn btn-success" id="crop">ثبت</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('/js/functions/storeAjax.js') }}"></script>
    <script src="{{ asset('theme/plugin/cropper/cropper.js') }}"></script>
    <script src="{{ asset('page/product/edit.js') }}"></script>
@endsection
