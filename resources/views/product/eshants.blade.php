@extends('master_page')
<?php
$title = "اشانت های محصول: " . $prod_name;
?>
@section('title_browser') {{$title}} @endsection
@section('main_content')
    <section class="basic-elements">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header">{{$title}}</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title-wrap bar-success">
                            <h4 class="card-title mb-0">افزودن اشانت</h4>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="px-3">
                            <form class="form" id="formEshant" method="post" action="{{url()->current()}}">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-12">


                                            <div class="row showFormDiscount">
                                                <div class="col-md-2">
                                                    <div class="form-group">

                                                        <label>تعداد خرید</label>
                                                        <input type="text" class="form-control" name="pc_buy_count"
                                                               autocomplete="off">

                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">

                                                        <label>تعداد اشانت</label>
                                                        <input type="text" class="form-control" name="pc_free_count"
                                                               autocomplete="off">
                                                    </div>
                                                </div>
                                                <div class="col-md-3 selectProduct">
                                                    <label>انتخاب محصول</label>
                                                    <div class="input-group">
                                                        <select name="pc_method_select_product"
                                                                class="form-control methodSelectProduct">
                                                            <option value="1">محصول جاری</option>
                                                            <option value="2">محصول دیگر</option>
                                                        </select>
                                                        <button type="button" class="btn btn-info showProduct"
                                                                style="display: none;">محصول
                                                        </button>
                                                        <input type="hidden" name="pc_free_collection_product_id">
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <label>تاریخ</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control datePicker"
                                                               name="pc_date_from" placeholder="تاریخ از"
                                                               autocomplete="off">
                                                        <input type="text" class="form-control datePicker"
                                                               name="pc_date_to" placeholder="تاریخ تا"
                                                               autocomplete="off">
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <label>محاسبه مازاد</label>
                                                    <input type="checkbox" class="form-control switchery" data-size="xs"
                                                           data-color="danger" name="pc_overflow" checked=""
                                                           data-switchery="true" style="display: none;">
                                                </div>

                                            </div>


                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <button id="createEshant" name="create_basic_info"
                                                    class="btn btn-success btn-loading" type="button">
                                                ثبت
                                            </button>
                                        </div>
                                    </div>
                                    <div class="row form-actions">
                                        <div class="col-12 table-responsive">
                                            <table class="table table-striped table-bordered sourced-data dataTable"
                                                   style="text-align: center">
                                                <thead>
                                                <tr>
                                                    <th>ردیف</th>
                                                    <th>تعداد خرید</th>
                                                    <th>تعداد اشانت</th>
                                                    <th>محصول</th>
                                                    <th>تاریخ</th>
                                                    <th>عملیات</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($eshants as $eshant)
                                                    <?php
                                                    if ($eshant['pc_date_from'] == 0 && $eshant['pc_date_to'] == 0)
                                                        $time = "نامحدود";
                                                    else if ($eshant['pc_date_from'] == 0)
                                                        $time = "از " . '~' . ' تا ' . jdate($eshant['pc_date_to'])->format('Y/m/d');
                                                    else if ($eshant['pc_date_to'] == 0)
                                                        $time = "از " . jdate($eshant['pc_date_from'])->format('Y/m/d') . ' تا ' . "~";
                                                    else
                                                        $time = "از " . jdate($eshant['pc_date_from'])->format('Y/m/d') . ' تا ' . jdate($eshant['pc_date_to'])->format('Y/m/d');
                                                    ?>
                                                    <tr data-id="{{$eshant['pc_id']}}"
                                                        data-free-product-id="{{$eshant['pc_free_collection_product_id']}}"
                                                        data-date-from="{{jdate($eshant['pc_date_from'])->format('Y/m/d')}}"
                                                        data-date-to="{{jdate($eshant['pc_date_to'])->format('Y/m/d')}}"
                                                        data-overflow="{{$eshant['pc_overflow']}}"
                                                    >
                                                        <td>{{$loop->index + 1}}</td>
                                                        <td class="pc_buy_count">{{$eshant['pc_buy_count']}}</td>
                                                        <td class="pc_free_count">{{withoutZeros($eshant['pc_free_count'])}}</td>
                                                        <td class="prod_name">{{$eshant['prod_name']}}</td>
                                                        <td class="">{{$time}}</td>
                                                        <td style="text-align: center">
                                                            <a class="success p-0 editEshant"
                                                               data-action="{{asset('product/edit-eshant').'/'.$eshant['pc_id']}}">
                                                                <i class="fa fa-pencil font-medium-3 mr-2"></i>
                                                            </a>
                                                            <a class="danger p-0"
                                                               data-action="{{asset('product/delete-eshant').'/'.$eshant['pc_id']}}"
                                                               data-confirm="اطمینان از حذف این مورد دارید؟"
                                                               data-method="post">
                                                                <i class="fa fa-trash-o font-medium-3 mr-2"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
@section('script')
    <script src="{{ asset('/js/functions/storeAjax.js') }}"></script>
    <script>
        $(document).ready(function () {
            $(document).on("click", '.editEshant', function () {
                var tr = $(this).closest('tr');
                $('#formEshant').attr('action', $(this).data('action'));
                $('#formEshant').find('input[name=pc_buy_count]').val(tr.find(".pc_buy_count").html());
                $('#formEshant').find('input[name=pc_free_count]').val(tr.find(".pc_free_count").html());
                if ({{$id}} == tr.data("free-product-id")
                )
                $('#formEshant').find('select[name=pc_method_select_product]').val(1).change();
                else
                {
                    $('#formEshant').find('select[name=pc_method_select_product]').val(2).change();
                    $('#formEshant').find('.showProduct').html(tr.find('.prod_name').html());
                }
                $('#formEshant').find('input[name=pc_free_collection_product_id]').val(tr.data("free-product-id"));
                $('#formEshant').find('input[name=pc_date_from]').val(tr.data("date-from"));
                $('#formEshant').find('input[name=pc_date_to]').val(tr.data("date-to"));
                if (tr.data("overflow") == 1)
                    $('#formEshant').find('input[name=pc_overflow]').attr("checked", 'checked');
                else
                    $('#formEshant').find('input[name=pc_overflow]').removeAttr("checked");
                $('<input>').attr({
                    type: 'hidden',
                    value: tr.data("id"),
                    name: 'id'
                }).appendTo('#formEshant');
            });

            $(document).on("change", '.methodSelectProduct', function () {
                var row = $(this).closest("div.selectProduct");
                if ($(this).val() == 2) {
                    $(row).find('.showProduct').show();
                }
                else {
                    $(row).find('.showProduct').hide();
                }
            });
            $(document).on('click', '.showProduct', function () {
                showData('{{asset('/product')}}');
                trClick = $(this);
            });
            $(document).on('click', '#createEshant', function (e) {
                storeAjax($(this), 'POST', captionButton = "ثبت", progress = false, reloadPage = true, TimeOutActionNextStore = 2000)

            });
        });
    </script>
@append