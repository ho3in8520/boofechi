@extends('master_page')
<?php
$title = "جزئیات محصول";
$arzesh_afzoode_array = [0 => 'ندارد', 1 => 'دارد'];
?>
@section('title_browser',$title)
@section('style')@endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header"><?=$title?></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title-wrap bar-warning">
                            <div class="card-title">اطلاعات محصول</div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="card-block">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                                    <ul class="no-list-style">
                                        <li class="mb-2">
                                            <span class="text-bold-500"><h4 class="primary"><i
                                                            class="ft-home font-small-3"></i> {{$product->prod_name}}</h4></span>
                                            <span class="line-height-2 display-block overflow-hidden">قیمت خرید: {{number_format($product->price_buy). ' '. setting('financial_unit')}}</span>
                                            <span class="line-height-2 display-block overflow-hidden">قیمت مصرف کننده: {{number_format($product->price_consumer) . ' ' . setting('financial_unit')}}</span>
                                            @if($product->cp_count_per > 1)
                                                <span class="line-height-2 display-block overflow-hidden">تعداد در هر {{' '. $product->packaging_type}}: {{$product->cp_count_per}}</span>
                                            @endif
                                            <span class="line-height-2 display-block overflow-hidden">موجودی: {{withoutZeros($product->count).' '.$product->bas_value}}</span>
                                            <span class="line-height-2 display-block overflow-hidden">توضیحات: {{($product->prod_description == "") ? 'ندارد' : $product->prod_description}}</span>
                                            <span class="line-height-2 display-block overflow-hidden">دسته بندی: {{$product->category}}</span>
                                            <span class="line-height-2 display-block overflow-hidden">ارزش افزوده: {{$arzesh_afzoode_array[$product->cp_arzesh_afzoodeh]}}</span>
                                            <span class="text-bold-500"><h4 class="primary"><i
                                                            class="ft-home font-small-3"></i> وزن و ابعاد</h4></span>
                                            <span class="line-height-2 display-block overflow-hidden">وزن هر عدد: {{withoutZeros($product->cp_weight_of_each)}}</span>
                                            <span class="line-height-2 display-block overflow-hidden">طول هر عدد: {{withoutZeros($product->cp_dimension_length)}}</span>
                                            <span class="line-height-2 display-block overflow-hidden">عرض هر عدد: {{withoutZeros($product->cp_dimension_width)}}</span>
                                            @if($product->have_discount > 0)
                                                <span class="line-height-2 display-block overflow-hidden">درصد تخفیف: {{withoutZeros($product->have_discount)}}</span>
                                            @endif
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                                    <ul class="no-list-style">
                                        <li class="mb-2">
                                            تصویر:
                                            <br>
                                            <img class="img img-border img-responsive" style="width: 300px"
                                                 src="{{getFile($product->imgProduct,'product')}}">
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if($product->have_discount == -1)
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title-wrap bar-success">
                                <div class="card-title">اطلاعات اشانت</div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="card-block">
                                <div class="row">
                                    {!! $str !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </section>
@endsection
@section('script')@endsection
