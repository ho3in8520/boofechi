@extends('master_page')
<?php
$title = "لیست محصول ها";
?>
@section('title_browser',$title)
@section('style')@endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header"><?=$title?></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                    </div>
                    <div class="card-body">
                        <form method="GET" action="{{ asset('/product') }}">
                            <div class="card-block">
                                <div class="col-md-12 table-responsive">
                                    <div class="summary">{!!getSummary($products) !!}</div>
                                    <table class="table table-striped table-bordered sourced-data dataTable">
                                        <thead>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>نام محصول</th>
                                            <th>نام دسته</th>
                                            <th>تعداد</th>
                                            <th>قیمت</th>
                                            <th>تخفیف</th>
                                            <th>اشانتیون</th>
                                            <th>عملیات</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <input type="text" class="form-control"
                                                       value="{{ request()->query('prod_name') }}" name="prod_name">
                                            </td>
                                            <td>
                                                <input type="text" class="form-control"
                                                       value="{{ request()->query('cat_name') }}" name="cat_name">
                                            </td>
                                            <td>
                                                <div class="input-group">
                                                    <input type="text" class="form-control"
                                                           value="{{ request()->query('cp_default_count_from') }}"
                                                           name="cp_default_count_from" placeholder="از">
                                                    <input type="text" class="form-control"
                                                           value="{{ request()->query('cp_default_count_to') }}"
                                                           name="cp_default_count_to" placeholder="تا">
                                                </div>
                                            </td>
                                            <td>

                                            </td>
                                            <td>
                                                <input class="form-control"
                                                       value="{{ request()->query('have_discount') }}"
                                                       name="have_discount" placeholder="درصد تخفیف">
                                            </td>
                                            <td>
                                                <select class="form-control"
                                                        value="{{ request()->query('have_eshant') }}"
                                                        name="have_eshant">
                                                    <option value>انتخاب نمایید</option>
                                                    <option value="1">دارد</option>
                                                    <option value="0">ندارد</option>
                                                </select>
                                            </td>
                                            <td>
                                                <button type="submit" class="btn-loading btn btn-primary search-ajax">
                                                    اعمال فیلتر
                                                    <i class="ft-thumbs-up position-right"></i>
                                                </button>
                                            </td>
                                        </tr>
                                        @if($products->count() != 0)
                                            @foreach($products as $product)
                                                <tr data-name="{{ $product->prod_name }}" class="popup-selectable {{ (!empty($closestDate) && $closestDate->cp_id==$product->cp_id)?'table-info':'' }}"
                                                    data-id="{{ $product->cp_id }}"
                                                    data-pricec="{{ withoutZeros($product->cp_default_price_consumer) }}"
                                                    data-priceb="{{ withoutZeros($product->cp_default_price_buy) }}"
                                                    data-weight_unit="{{ $product->bas_value }}"
                                                    data-packaging_type="{{ $product->packaging_type }}"
                                                    data-count_per="{{ $product->cp_count_per }}">
                                                    <td>{{index($products,$loop)}}</td>
                                                    <td>{{$product->prod_name}}</td>
                                                    <td>{{$product->cat_name}}</td>
                                                    <td>{{withoutZeros($product->cp_default_count).' '.$product->bas_value}}</td>
                                                    <td>{{number_format(withoutZeros($product->cp_default_price_consumer))}}</td>
                                                    <td>{!! ($product->have_discount > 0) ? $product->have_discount.' %' : '<i class="fa fa-remove"></i>'  !!}</td>
                                                    <td>{!! ($product->have_eshant == -1) ? '<i class="fa fa-check"></i>' : '<i class="fa fa-remove"></i>'  !!}</td>
                                                    <td>
                                                        @if(!request()->ajax())
                                                            @can('edit_product')
                                                                <a title="ویرایش محصول"
                                                                   href="{{asset('product/'.$product->cp_id.'/edit')}}">
                                                                    <i class="fa fa-pencil-alt"></i>
                                                                </a>
                                                            @endcan
                                                            @can('product_eshant')
                                                                <a title="اشانت"
                                                                   href="{{asset('product/eshant').'/'.$product->cp_id}}">
                                                                    <i class="fa fa-list"></i>
                                                                </a>
                                                            @endcan
                                                            @can('delete_product')
                                                                <a data-confirm="اطمینان از حذف این محصول دارید" data-action="{{asset('product/delete').'/'.$product->cp_id}}" data-id="1" data-method="post">
                                                                    <i class="fa fa-trash"></i>
                                                                </a>
                                                            @endcan
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="7" style="text-align: center;">
                                                    {{ config('first_config.message.empty_table') }}
                                                </td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                    {!! $products->appends(request()->query())->render() !!}
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')@endsection
