@extends('master_page')
<?php
$productRequest = new \App\Http\Requests\ProductRequest();

?>
@section('style')
    <style>
        .formDiscount {
            display: none;
        }

        #deleteImage {
            position: relative;
        }

        #deleteImageUpload {
            font-size: 35px;
            margin-left: 10px;
            z-index: 1;
            cursor: pointer;
            position: absolute;
        }

        .img-container {
            direction: ltr !important;
        }

        .cropper-container {
            direction: ltr !important;
        }

        .cropper-wrap-box, .cropper-canvas {
            direction: ltr !important;
        }

        .cropper-crop-box {
            direction: ltr !important;
        }

        .cropper-view-box {
            direction: ltr !important;
        }

        .preview {
            overflow: hidden;
            width: 160px;
            height: 160px;
            margin: 10px;
            border: 1px solid red;
            direction: ltr !important;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/plugin/cropper/cropper.css') }}">

@endsection
@section('title_browser') {{ (empty($product)) ? 'ثبت محصول' : 'ویرایش محصول' }} @endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header">{{ (empty($product)) ? 'ثبت محصول' : 'ویرایش محصول' }}</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <form
                                action='{{ (empty($product)) ? route('product.store') : asset('product/'.$product->cp_id.'/update') }}'
                                method='POST' enctype="multipart/form-data">
                                <input type="hidden" id="maliat" value="{{setting('tax')}}">
                                {{ csrf_field() }}
                                {!! (!empty($product)) ? "<input name='_method' type='hidden' value='PATCH'>" : ''  !!}

                                <div class='row'>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <div id="deleteImage">
                                            </div>
                                            <input accept="image/jpeg" name="file" type="file" id="imgInp"
                                                   style="display: none">
                                            <img title="آپلود عکس" style="cursor:pointer;" id="blah"
                                                 src="{{ asset('/images/image.jpg') }}"
                                                 alt="your image" width="150px" height="150px">

                                        </div>
                                    </div>
                                </div>
                                <div class='row'>
                                    <div class='col-md-3'>
                                        <div class='form-group'>
                                            {!! ($productRequest->rules()['product.prod_name'][0] == 'required') ? "<span class='text-danger'>*</span>" : '' !!}
                                            <label>{{ __('validation.attributes') ['product.prod_name']}}</label>
                                            <input type='text'
                                                   class='numberFormat form-control'
                                                   name='product[prod_name]'
                                                   value="{{ empty($product) ?  '' : $product->prod_name }}">
                                        </div>
                                    </div>
                                    <div class='col-md-3'>
                                        <div class='form-group'>
                                            {!! ($productRequest->rules()['product.prod_category_id'][0] == 'required') ? "<span class='text-danger'>*</span>" : '' !!}
                                            <label>{{ __('validation.attributes' )['product.prod_category_id']}}</label>
                                            <select class='form-control category'
                                                    name='product[prod_category_id]'>
                                                {!! createCombobox($categories,0,0,true) !!}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <?php $measures = \App\Baseinfo::whereBasType('packaging_type')->where('bas_parent_id', '<>', '0')->get() ?>
                                        <div class="form-group">
                                            {!! ($productRequest->rules()['collProduct.cp_packaging_type'][0] == 'required') ? "<span class='text-danger'>*</span>" : '' !!}
                                            <label>{{ __('validation.attributes')['collProduct.cp_packaging_type'] }}</label>
                                            <select id="uni_measurement" class="form-control select2"
                                                    name="collProduct[cp_packaging_type]">
                                                {!! customForeach($measures,'bas_id','bas_value',@$product->cp_packaging_type) !!}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <?php $weight_unit = \App\Baseinfo::whereBasType('measure_unit')->where('bas_parent_id', '<>', '0')->get() ?>
                                        <div class="form-group">
                                            {!! ($productRequest->rules()['collProduct.cp_measurement_unit_id'][0] == 'required') ? "<span class='text-danger'>*</span>" : '' !!}
                                            <label>{{ __('validation.attributes')['collProduct.cp_measurement_unit_id'] }}</label>
                                            <select id="uni_measurement" class="form-control select2"
                                                    name="collProduct[cp_measurement_unit_id]">
                                                {!! customForeach($weight_unit,'bas_id','bas_value',@$product->cp_measurement_unit_id) !!}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label id="counter">تعداد</label>
                                            <input type="text" class="form-control" name="collProduct[cp_count_per]"
                                                   value="{{ empty($product) ?  '' : $product->cp_count_per }}">
                                        </div>
                                    </div>


                                    <div class="col-md-2">
                                        <div class="form-group">
                                            {!! ($productRequest->rules()['collProduct.cp_arzesh_afzoodeh'][0] == 'required') ? "<span class='text-danger'>*</span>" : '' !!}
                                            <input type="checkbox"
                                                   name="collProduct[cp_arzesh_afzoodeh]"
                                                   value="1" @if(@$product->cp_arzesh_afzoodeh==1)
                                                   checked
                                            @else
                                                @endif
                                            >
                                            <label>{{ __('validation.attributes')['collProduct.cp_arzesh_afzoodeh'] }}</label>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <input type="checkbox" name="collProduct[cp_sale_once]"@if(@$product->cp_sale_once==1)
                                            checked
                                            @else
                                                @endif
                                            >
                                            <label>{{ __('validation.attributes')['collProduct.sale_once'] }}</label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>{{ __('validation.attributes')['collProduct.cp_default_price_buy'] }}</label>
                                            (<label id="with_arzesh_afzoodeh">0</label>)
                                            <input type="text" class="form-control numeric"
                                                   value="{{empty($product)?0:withoutZeros($product->cp_default_price_buy)}}"
                                                   name="collProduct[cp_default_price_buy]">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>{{ __('validation.attributes')['collProduct.cp_default_price_consumer'] }}</label>
                                            <input type="text" class="form-control numeric"
                                                   value="{{empty($product)?0:withoutZeros($product->cp_default_price_consumer)}}"
                                                   name="collProduct[cp_default_price_consumer]">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label style="font-size: 16px !important" id="counter">کد محصول حسابداری</label>
                                            <input type="text" class="form-control" name="collProduct[cp_code_hesabdari_api]"
                                                   value="{{ empty($product) ?  '' : $product->cp_code_hesabdari_api }}">
                                        </div>
                                    </div>
                                </div>

                                <h3>وزن و ابعاد</h3>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-3">
                                        {!! ($productRequest->rules()['collProduct.cp_weight_of_each'][0] == 'required') ? "<span class='text-danger'>*</span>" : '' !!}
                                        <label>{{ __('validation.attributes')['collProduct.cp_weight_of_each'] }}</label>
                                        <div class="input-group">
                                            <input type="text"
                                                   class="form-control"
                                                   name="collProduct[cp_weight_of_each]"
                                                   value="{{empty($product)?'0':withoutZeros($product->cp_weight_of_each)}}">
                                            <select name="collProduct[cp_weight_unit]"
                                                    class="form-control methodSelectProduct">
                                                {!! customForeach($weight_units,'bas_id','bas_value',@$product->cp_weight_unit) !!}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label>{{ __('validation.attributes')['collProduct.cp_dimension_length'] }}</label>
                                        <div class="input-group">
                                            <input type="text"
                                                   class="form-control"
                                                   name="collProduct[cp_dimension_length]"
                                                   value="{{empty($product)?'':number_format($product->cp_dimension_length)}}"
                                                   placeholder="طول">
                                            <input type="text"
                                                   class="form-control"
                                                   name="collProduct[cp_dimension_width]"
                                                   value="{{empty($product)?'':number_format($product->cp_dimension_width)}}"
                                                   placeholder="عرض">
                                            <select name="collProduct[cp_width_unit]"
                                                    class="form-control methodSelectProduct">
                                                {!! customForeach($width_unit,'bas_id','bas_value',@$product->cp_width_unit) !!}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <h3>تخفیف/اشانتیون</h3>
                                <hr>
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>هیچکدام</label>
                                            <input class="discount" checked type="radio" name="type" value="3">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>تخفیف</label>
                                            <input class="discount" type="radio" name="type" value="1"
                                                   @if(@$product->have_discount > 0)
                                                   checked
                                            @else
                                                @endif>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>اشانتیون</label>
                                            <input class="discount" id="eshant" type="radio" name="type" value="2">
                                        </div>
                                    </div>
                                </div>
                                <div class="row showFormDiscount">
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button id="createProduct" class="btn btn-success btn-loading" type="button">
                                            ثبت
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div @if(@$product->have_discount > 0)
         @else
         style="display: none"
    @endif>
        <div class="DiscountForm">
            <div class="col-md-3">
                <div class="form-group">
                    {!! ($productRequest->rules()['pc_discount_percent.*'][0] == 'required') ? "<span class='text-danger'>*</span>" : '' !!}
                    <label>درصد تخفیف</label>
                    <input type="text" class="form-control"
                           name="pc_discount_percent[]">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>تاریخ از</label>
                    <input type="text" class="form-control datePicker" name="pc_date_from_dis[]">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>تاریخ تا</label>
                    <input type="text" class="form-control datePicker" name="pc_date_to_dis[]">

                </div>
            </div>
        </div>
        <div class="eshantionForm card border-right my-4 border-primary border-3"  style="display: none">
            <div class="col-md-2">
                <div class="form-group">
                    {!! ($productRequest->rules()['pc_buy_count.*'][0] == 'required') ? "<span class='text-danger'>*</span>" : '' !!}
                    <label>تعداد خرید</label>
                    <input type="text" class="form-control" name="pc_buy_count[]">

                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    {!! ($productRequest->rules()['pc_free_count.*'][0] == 'required') ? "<span class='text-danger'>*</span>" : '' !!}
                    <label>تعداد اشانت</label>
                    <input type="text" class="form-control" name="pc_free_count[]">
                </div>
            </div>
            <div class="col-md-3 selectProduct">
                <label>انتخاب محصول</label>
                <div class="input-group">
                    <select name="pc_method_select_product[]" class="form-control methodSelectProduct">
                        <option value="1">محصول جاری</option>
                        <option value="2">محصول دیگر</option>
                    </select>
                    <button type="button" class="btn btn-info showProduct" data-url="{{asset('/product')}}" style="display: none;">محصول
                    </button>
                    <input type="hidden" name="pc_free_collection_product_id[]">
                </div>
            </div>
            <div class="col-sm-4">
                <label>تاریخ</label>
                <div class="input-group">
                    <input type="text" class="form-control datePicker" name="pc_date_from[]" placeholder="تاریخ از">
                    <input type="text" class="form-control datePicker" name="pc_date_to[]" placeholder="تاریخ تا">
                </div>
            </div>
            <div class="col-sm-2">
                <label>محاسبه مازاد</label>
                <input type="checkbox" style="margin-top: 0px !important;"
                       name="pc_overflow[]" checked>
            </div>
            <div class="col-md-2">
                <div class="input-group">
                    <button type="button" class="btn btn-danger btn-sm deleteForm">
                        <i class="fa fa-trash"></i>
                    </button>
                    <button type="button" class="btn btn-primary btn-sm addForm">
                        <i class="fa fa-plus"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel">برش تصویر</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="img-container">
                        <img id="image" src="" alt="Picture" style="max-width: 100%">
                        <input type="hidden" id="dataGetter">
                    </div>
                </div>
                <div class="preview"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">انصراف</button>
                    <button type="button" class="btn btn-success" id="crop">ثبت</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('/js/functions/storeAjax.js') }}"></script>
    <script src="{{ asset('theme/plugin/cropper/cropper.js') }}"></script>
    <script src="{{ asset('page/product/form.js') }}"></script>
@endsection
