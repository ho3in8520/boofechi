@extends('master_page')
<?php
$title = "سفارش محصول";
?>
@section('title_browser',$title)
@section('style')
    <link href="{{asset('css/bootstrap-slider.css')}}" rel="stylesheet">
    <style>

        .thumbnail, .thumbnail .img-fluid {
            width: 100%;
            height: 150px !important;
        }

        .modal-body .content-header {
            display: none;
        }

        .slider-horizontal {
            width: 60% !important;
        }

        #nav-senfs .dropdown:hover .dropdown-menu {
            display: block;
        }

        #navbarSenfs ul li.dropdown {
            position: initial !important;
        }

        #nav-senfs.navbar {
            position: relative;
            left: auto !important;
            top: auto !important;
            right: auto !important;
            bottom: auto !important;
        }

        #nav-senfs .dropdown-menu {
            right: 30px !important;
            margin-top: 0 !important;
            text-align: right;
            width: 50%;
        }

        div.outer {
            position: relative;
        }

        div.outer a {
            position: absolute;
            right: 0;
            bottom: 0;
        }
        .links{
            margin-right: 50%;
            padding-bottom: 20px;
        }
    </style>
@endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <nav id="nav-senfs" class="navbar navbar-expand-lg navbar-light bg-default"
                 style="margin-top: -20px;width: 100% !important;">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSenfs"
                        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSenfs">
                    <div class="col-sm-9">
                        <ul class="navbar-nav mr-auto">
                            @if(!empty($senfs))
                                @foreach($senfs as $key => $senf)
                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            {{$key}}
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <div class="container">
                                                <div class="row">
                                                    @foreach($senf as $child)
                                                        @if($child['cat_parent_id'] == 0)
                                                            <div class="col-md-6">
                                                                <a class="nav-link"
                                                                   href="{{route('product-list').'/'.slg($key).'/'.slg($child['cat_name'])}}">{{$child['cat_name']}}</a>
                                                            </div>
                                                        @endif
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                            @else
                                <li class="nav-item">
                                    <a class="nav-link" href="#">هیچ صنفی وجود ندارد</a>
                                </li>
                            @endif
                        </ul>
                    </div>
                    <div class="input-group col-sm-3 pull-left mt-2">
                        <input id="search-box" class="form-control mr-sm-2" name="term" type="search"
                               placeholder="نام محصول/دسته بندی"
                               value="{{request()->query('q')}}"
                               aria-label="Search">
                        <span class="input-group-append">
                            <a id="search" class="btn btn-outline-light btn-primary" type="submit"><i
                                    class="fa fa-search"></i>
                            </a>
                        </span>
                    </div>
                </div>
            </nav>
        </div>
        <div class="row">
            <div class="col-md-9">
                <div class="row products" style="padding: 10px;">
                    @if(!empty($products))
                        @foreach($products as $product)
                            <div class="col-6 col-lg-4 col-md-6">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="thumbnail">
                                            <img class="card-img-top img-fluid"
                                                 src="{{ getFile(($product['file_path'] != "") ? $product['file_path'] : 'default.jpg') }}"
                                                 alt="Card image cap">
                                            @if($product['cp_default_count'] == 0)
                                                <div class="caption-wrapper">
                                                    <div class="caption">ناموجود</div>
                                                </div>
                                            @elseif($product['have_discount'] > 0)
                                                <div class="percent-wrapper">
                                                    <div class="percent">%{{withoutZeros($product['have_discount'])}}</div>
                                                </div>
                                            @elseif($product['have_discount'] == -1)
                                                <div class="eshant-wrapper">
                                                    <div class="eshant">اشانت</div>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="card-block">
                                            <p class="card-title">{{$product['prod_name']}}</p>
                                            <p class="card-text"></p>
                                            <p>
                                                <span>موجودی:</span>
                                                {{number_format(withoutZeros($product['cp_default_count']))}}
                                            </p>
                                            <p class="price">
                                                <span class="tag tag-default">قیمت:</span>
                                                <?php
                                                if ($product['have_discount'] > 0) {
                                                    $price = "<span class='danger' style='text-decoration: line-through'>" . number_format($product['price'])
                                                        . "</span> <span class='success'>" . number_format($product['price'] * (100 - $product['have_discount']) / 100) . "</span>";
                                                } else
                                                    $price = number_format($product['price']);
                                                ?>
                                                @if($product['cp_default_count'] > 0)
                                                    {!! $price !!} {{setting('financial_unit')}}
                                                @else
                                                    نامشخص
                                                @endif
                                            </p>
                                            @if ($product['cp_default_count'] > 0)
                                                <div class="input-group">
                                                    <select name="measure_unit" class="form-control measure_unit">
                                                        <option value="1">{{$product['bas_value']}}</option>
                                                        @if($product['cp_sale_once']==1)
                                                            <option value="0">تکی</option>
                                                        @endif
                                                    </select>
                                                    <input class="form-control" name="count_product"
                                                           placeholder="تعداد">
                                                </div>
                                                    @endif
                                                    @if($product['cp_default_count'] > 0)
                                                <div class="input-group">
                                                        <a class="btn btn-success btn-add-to-basket"
                                                           data-href="{{route('add-to-basket')}}"
                                                           data-id="{{$product['prod_id']}}" title="افزودن به سبد خرید"><i
                                                                class="fa fa-shopping-basket"></i></a>
                                                    @else
                                                        <div class="input-group">
                                                        <a class="btn btn-danger" title="ناموجود"><i
                                                                class="fa fa-shopping-basket"></i></a>
                                                    @endif
                                                    <a class="btn btn-primary product-detail"
                                                       data-href="{{route('product.show',$product['prod_id'])}}"
                                                       title="جزئیات محصول"><i
                                                            class="fa fa-eye"></i></a>
                                                    @if($product['have_discount'] == -1)
                                                        <a class="btn btn-info eshant-detail"
                                                           data-href="{{route('eshant-detail',$product['prod_id'])}}"
                                                           data-id="{{$product['prod_id']}}"
                                                           title="جزئیات اشانتیون"><i
                                                                class="fa fa-info"></i></a>
                                                    @endif
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    @else

                        @if($senf == '' || $cat == '')
                            <div class="alert alert-warning col-sm-12"
                                 style="margin-right: -10px;color:#FFFFFF !important;">لطفا یک دسته بندی انتخاب کنید.
                            </div>
                        @else
                            <div class="alert alert-danger col-sm-12"
                                 style="margin-right: -10px;color:#FFFFFF !important;">موردی یافت نشد.
                            </div>
                        @endif

                    @endif
                </div>
                            <div class="row links">
                                {!! $pagination->appends(request()->query())->links() !!}
                            </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <div class="card" id="filters" style="width: 100% !important;padding: 10px">
                        <div class="card-body" style="min-height: 300px">
                            <h6 class="form-section">
                                <i class="fa fa-check"></i> موجود یا ناموجود/تخفیف/اشانت</h6>
                            <div>
                                <input type="checkbox" name="mojod" class="switchery" value="1"
                                       {{(request('mojod') == 1) ? 'checked' : ''}} data-size="xs"
                                       data-color="primary">
                                موجود بودن کالا
                                <br>
                                <input type="checkbox" name="takhfif" class="switchery" value="1"
                                       {{(request('takhfif') == 1) ? 'checked' : ''}} data-size="xs"
                                       data-color="primary">
                                دارای تخفیف
                                <br>
                                <input type="checkbox" name="eshant" class="switchery" value="1"
                                       {{(request('eshant') == 1) ? 'checked' : ''}} data-size="xs"
                                       data-color="primary">
                                دارای اشانت
                            </div>
                            <h6 class="form-section" style="margin-top:20px">
                                <i class="fa fa-dollar"></i> <span id="slide-result"> قیمت از
                                    @if(request()->has('price_range') && request('price_range') != "")
                                        <?php
                                        list($from, $to) = explode(',', \request('price_range'));
                                        echo number_format($from) . ' تا ' . number_format($to);
                                        ?>
                                    @else
                                        0 تا {{$max_price}}
                                    @endif

                                </span>
                            </h6>
                            <div style="display: inline">
                                <b>0</b> <input name="price_range" id="ex1" type="text" class="span2" value=""
                                                data-slider-min="0"
                                                data-slider-max="{{$max_price}}" data-slider-step="100"
                                                data-slider-value="[{{request('price_range','0,'.$max_price)}}]"/>
                                <b>{{number_format($max_price)}}</b>
                            </div>
                            <h6 class="form-section" style="margin-top:20px">
                                <i class="fa  fa-list"></i> دسته بندی</h6>
                            <div>
                                @foreach($sub_cats as $item)
                                    @if($item['cat_parent_id'] != 0)
                                        <input type="checkbox" class="switchery sub_cat" value="{{$item['cat_id']}}"
                                               data-size="xs"
                                               {{(isset($_GET['sub_cats']) && in_array($item['cat_id'],$_GET['sub_cats'])) ? "checked" : ''}} data-color="primary"
                                               name="filter_sub_cat_{{$item['cat_id']}}">{{$item['cat_name']}}
                                    @endif
                                @endforeach
                            </div>
                            <h6 class="form-section" style="margin-top:20px">
                                <i class="fa fa-building"></i> شرکت ها</h6>
                            <div>
                                @foreach($companies as $value)
                                    <input type="checkbox" class="switchery company" data-size="xs"
                                           data-color="primary"
                                           {{(isset($_GET['companies']) && in_array($value->coll_id,$_GET['companies'])) ? "checked" : ''}}
                                           name="filter_companies_{{$value->coll_id}}"
                                           value="{{$value->coll_id}}">{{$value->coll_name}}<br/>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
@endsection
@section('script')
    <script src="{{asset('js/bootstrap-slider.js')}}"></script>
    <script>
        $(document).ready(function () {

            $(document).find("data-toggle[dropdown]").click(function (e) {
                e.preventDefault();
            });

            var slider = new Slider("#ex1", {
                min: 0, max: {{$max_price}}, width: '60%', tooltip: 'always', focus: true
            });
            slider.refresh({useCurrentValue: true});
            var x = $("#ex1").on("slide", function (slideEvt) {
                $("#slide-result").html("قیمت از " + slideEvt.value[0] + " تا " + slideEvt.value[1]);
            });
            x.on('slideStop', function (val) {
                generateUrl();
            });

            $(".sub_cat").change(function () {
                generateUrl();
            });

            $("input:checkbox[name=mojod]").change(function () {
                generateUrl();
            });

            $("input:checkbox[name=eshant]").change(function () {
                generateUrl();
            });

            $("input:checkbox[name=takhfif]").change(function () {
                generateUrl();
            });

            $(".company").change(function () {
                generateUrl();
            });

            $("#search").click(function () {
                generateUrl();
            });

            $(document).on("click", ".eshant-detail", function () {
                var id = $(this).data('id');
                showData($(this).data('href'), {}, true);
            });

            $(document).on("click", ".product-detail", function () {
                var id = $(this).data('id');
                showData($(this).data('href'), {}, true);
            });

            $(document).on("click", ".btn-add-to-basket", function () {
                busy = true;
                var element = $(this);
                $(element).beginLoading();
                var count = $(this).closest('.card-block').find("input[name^='count_product']").val();
                var type = $(this).closest('.card-block').find("select[name^='measure_unit']").val();
                addToBasket($(this).data('href'), {id: $(this).data('id'), count: count, type: type, mode: 1}, element);
            });

            $(window).scroll(function () {
                //fixDiv($('#filters').parent(), 90);
            });

            function fixDiv(div, scroll_number) {
                var $cache = div;
                if ($(window).scrollTop() > scroll_number)
                    $cache.css({
                        'z-index': 9999,
                        'position': 'fixed',
                        'width': $(div).width() + 'px !important'
                    });
                else
                    $cache.css({
                        'position': 'relative',
                        'width': $(div).width() + 'px !important'
                    });
            }

            function generateUrl() {
                $(".links").html('');
                $(".products").html('<p class="col-12"><i class="fa fa-spinner fa-spin"></i></p>');
                var query = '';
                var sub_cats = $("input:checkbox[name^='filter_sub_cat_']:checked").map(function () {
                    return $(this).val();
                }).get();

                var companies = $("input:checkbox[name^='filter_companies_']:checked").map(function () {
                    return $(this).val();
                }).get();

                var q = $("#search-box").val();
                var price_range = $("input[name=price_range]").val();
                var mojod = ($("input:checkbox[name=mojod]").is(':checked')) ? 1 : 0;
                var takhfif = ($("input:checkbox[name=takhfif]").is(':checked')) ? 1 : 0;
                var eshant = ($("input:checkbox[name=eshant]").is(':checked')) ? 1 : 0;
                query = $.param({
                    sub_cats: sub_cats,
                    companies: companies,
                    q: q,
                    price_range: price_range,
                    mojod: mojod,
                    takhfif: takhfif,
                    eshant: eshant
                });
                changeUrl('{{url()->current()}}?' + query);
            }

            function changeUrl(url) {
                $.get(url, function (data) {
                    var result = JSON.parse(data);
                    $(".products").html(result.data);
                    $(".links").html(result.links);
                });
                return window.history.pushState("object or string", "Title", url);
            }
        });
    </script>
@endsection
