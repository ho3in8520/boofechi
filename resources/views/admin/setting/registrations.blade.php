@extends('master_page')
<?php
$title = "مدیریت کاربران ثبت نام شده";
?>
@section('style')
    <link rel="stylesheet" type="text/css" href="{{asset("/theme/vendors/css/sweetalert2.min.css")}}">
@endsection
@section('title_browser',$title)
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header"><?=$title?></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">

                        <p class="mb-0"></p>
                    </div>
                    <div class="card-body">
                        <div class="card-block">
                            <div class="summary">{!! getSummary($registrations)!!}</div>
                            <form action="" method="get">
                                <table class="table table-striped table-bordered sourced-data dataTable">
                                    <thead>
                                    <tr>
                                        <th>ردیف</th>
                                        <th>نوع پنل</th>
                                        <th>کاربر</th>
                                        <th>فرم</th>
                                        <th>وضعیت</th>
                                        <th>عملیات</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th>#</th>
                                        <th><select class="form-control" name="panel_type">
                                                <option value="0">انتخاب کنید...</option>
                                                <option value="44" {{request()->query('panel_type') == 44 ? 'selected' : ''}}>
                                                    مغازه دار
                                                </option>
                                                <option value="45" {{request()->query('panel_type') == 45 ? 'selected' : ''}}>
                                                    ویزیتور
                                                </option>
                                            </select></th>
                                        <th><input type="text" class="form-control" name="user"
                                                   value="{{request()->query('user')}}"></th>
                                        <th><select class="form-control" name="form">
                                                <option value="0">انتخاب کنید...</option>
                                                <option value="authentication" {{request()->query('form') == 'authentication' ? 'selected' : ''}}>
                                                    احراز هویت
                                                </option>
                                                <option value="business-form" {{request()->query('form') == 'business-form' ? 'selected' : ''}}>
                                                    اطلاعات تکمیلی
                                                </option>
                                                <option value="upload_route" {{request()->query('form') == 'upload_route' ? 'selected' : ''}}>
                                                    آپلود
                                                </option>
                                            </select></th>
                                        <th><select class="form-control" name="status">
                                                <option value="0">انتخاب کنید...</option>
                                                <option value="1" {{request()->query('status') == 1 ? 'selected' : ''}}>
                                                    در انتظار بررسی
                                                </option>
                                                <option value="2" {{request()->query('status') == 2 ? 'selected' : ''}}>
                                                    تایید شده
                                                </option>
                                                <option value="3" {{request()->query('status') == 3 ? 'selected' : ''}}>
                                                    تایید نشده
                                                </option>
                                            </select></th>
                                        <th>
                                            <button type="submit" class="btn-loading btn btn-primary search-ajax">
                                                اعمال فیلتر
                                                <i class="ft-thumbs-up position-right"></i>
                                            </button>
                                        </th>
                                    </tr>
                                    @foreach($registrations as $row)
                                        <?php
                                        $status = "در انتظار بررسی";
                                        if ($row->pwc_accepted == 1)
                                            $status = "تایید شده";
                                        if ($row->pwc_accepted == 2)
                                            $status = "تایید نشده";
                                        ?>
                                        <tr data-id="{{$row->pwc_id}}">
                                            <td>{{index($registrations ,$loop)}}</td>
                                            <td>{{$row->bas_value}}</td>
                                            <td>{{$row->name}}</td>
                                            <td>{{$row->pw_form_address}}</td>
                                            <td class="status_lable">{{$status}}</td>
                                            <td>
                                                <button title="مشاهده اطلاعات" type="button"
                                                        class="btn btn-primary btn-sm viewForm">
                                                    <i class="fa fa-eye"></i>
                                                </button>
                                                @if(in_array($row->pwc_accepted,[0,2]))
                                                    <button title="تایید" type="button"
                                                            class="btn btn-success btn-sm acceptForm">
                                                        <i class="fa fa-check"></i>
                                                    </button>
                                                @endif
                                                @if(in_array($row->pwc_accepted,[0,1]))
                                                    <button title="عدم تایید" type="button"
                                                            class="btn btn-danger btn-sm deacceptForm">
                                                        <i class="fa fa-times"></i>
                                                    </button>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </form>
                            {!! $registrations->appends(request()->query())->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{asset("/theme/vendors/js/sweetalert2.min.js")}}"></script>
    <script>
        $(document).ready(function () {
            $(document).on("click", ".viewForm", function () {
                var element = $(this);
                var id = $(element).closest("tr").data('id');
                var buttons = {
                    0: {
                        title: 'ثبت',
                        data: {
                            id: id
                        },
                        class: 'btn btn-success saveChanges'
                    },
                };
                showData('{{asset('admin/get-form')}}', {id: id}, true, "#basic-form-layouts", buttons);
                /*$.post("{{asset('admin/get-form')}}", {id: id}, function (data) {
                 var result = JSON.parse(data);
                 if (result.status == 100) {

                 }
                 });*/
            });
            $(document).on("click", ".saveChanges", function (e) {
                var element = $(this);
                var data = $(element).closest(".modal-content").find("form").serializeArray();
                data.push({
                    'name': "id",
                    'value': $(element).data('id')
                });
                $.post($(element).closest(".modal-content").find("form").attr("action"), data, function (data) {
                    var result = JSON.parse(data);
                    if (result.status == 100)
                        toastr.success(result.msg, "موفق");
                    else
                        toastr.error(result.msg, "ناموفق");
                });
            });
            $(document).on("click", ".acceptForm", function () {
                var element = $(this);
                if (confirm('اطمینان از تایید این مورد دارید؟') == false)
                    return false;
                var id = $(element).closest("tr").data('id');
                $.post("{{asset('admin/form/accept')}}", {id: id}, function (data) {
                    var result = JSON.parse(data);
                    if (result.status == 100) {
                        toastr.success(result.msg, "موفق");
                        var parent = $(element).parent();
                        $(element).remove();
                        parent.closest("tr").find(".status_lable").html("تایید شده");
                        parent.html('<button title="مشاهده اطلاعات" type="button" class="btn btn-primary btn-sm viewForm"><i class="fa fa-eye"></i></button> <button title="عدم تایید" type="button" class="btn btn-danger btn-sm deacceptForm"><i class="fa fa-remove"></i></button>');
                    }
                });
            });
            $(document).on("click", ".deacceptForm", function () {
                var element = $(this);
                swal({
                    title: "علت",
                    text: "علت تایید نشدن:",
                    input: "textarea",
                    showCancelButton: true,
                    animation: "slide-from-top",
                    inputPlaceholder: "",
                    confirmButtonText: 'ثبت',
                    cancelButtonText: "انصراف",
                }).then(function (inputValue) {
                    if (inputValue === false) return false;
                    var id = $(element).closest("tr").data('id');
                    $.post("{{asset('admin/form/deaccept')}}", {id: id, reason: inputValue}, function (data) {
                        var result = JSON.parse(data);
                        if (result.status == 100) {
                            toastr.success(result.msg, "موفق");
                            var parent = $(element).parent();
                            $(element).remove();
                            parent.closest("tr").find(".status_lable").html("تایید نشده");
                            parent.html('<button title="مشاهده اطلاعات" type="button" class="btn btn-primary btn-sm viewForm"><i class="fa fa-eye"></i></button> <button title="تایید" type="button" class="btn btn-success btn-sm acceptForm"><i class="fa fa-check"></i></button>');
                        }
                    });
                }).done();
            });
        });
    </script>
@endsection
