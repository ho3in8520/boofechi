@extends('master_page')
@section('title_browser') اطلاعات پایه @endsection
@section('main_content')
    <section class="basic-elements">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header">اطلاعات پایه</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">

                    </div>
                    <div class="card-body">
                        <div class="px-3">
                            <form class="form" method="post" action="" enctype="multipart/form-data">
                                @if(!empty($old_base))
                                    @method('PATCH')
                                @endif
                                    <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-3 col-12">
                                            <div class="form-group">
                                                <label for="text">عنوان</label><span class="required red">*</span>
                                                <input type="text" class="form-control" name="BasicInfo[title]" value="{{formValue('BasicInfo.title',$old_base,'bas_value')}}">
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-12">
                                            <label for="text">دسته</label><span class="required red">*</span>
                                            <fieldset class="form-group position-relative">
                                                <select class="form-control select2" name="BasicInfo[category]" id="DefaultSelect">
                                                    {!! customForeach($types,'bas_id','bas_value',$category) !!}

                                                </select>
                                            </fieldset>
                                        </div>
                                        <div class="col-md-2 col-3">
                                        <div class="form-group" style="margin-top: 35px">
                                            <label for="checkbox">فعال/غیرفعال</label>
                                            <input type="checkbox"  name="BasicInfo[status]" id="status" value="1" class="switchery form-control"  data-size="xs" data-switchery="true"
                                                   @if(!empty($old_base) && $old_base->bas_status==1)
                                                   checked="checked"
                                                   @elseif(!empty($old_base) && $old_base->bas_status==0)
                                                           @else
                                                   checked="checked"
                                                    @endif
                                                   style="display: none;">
                                            <input id='testNameHidden' type='hidden' value='0' name='BasicInfo[status]'>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <button id="createBaiscInfo" name="create_basic_info" class="btn btn-success btn-loading" type="button">
                                                ثبت
                                            </button>
                                        </div>
                                    </div>
                                    <div class="row form-actions">
                                        <div class="col-12 table-responsive">
                                            <div class="summary">{!!getSummary($baseinfos) !!}</div>
                                            <table class="table table-striped table-bordered sourced-data dataTable">
                                                <thead>
                                                <tr>
                                                    <th>ردیف</th>
                                                    <th>عنوان</th>
                                                    <th>دسته</th>
                                                    <th>وضعیت</th>
                                                    <th>عملیات</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($baseinfos as $row)
                                                   <?php if($row->bas_status==1)
                                                        $status='فعال';
                                                   elseif($row->bas_status==0)
                                                        $status='غیر فعال';
                                                   ?>
                                                        <tr>
                                                            <td>{{index($baseinfos ,$loop)}}</td>
                                                            <td>{{$row->bas_value}}</td>
                                                            <td>
                                                               {{$row->parent}}
                                                            </td>
                                                            <td>{{$status}}</td>

                                                                <td style="text-align: center">
                                                                <a class="" href="{{asset('/admin/basic-info/'.$row->bas_id)}}" data-original-title="" title="">
                                                                    <i class="fa fa-pencil-alt"></i>
                                                                </a>
                                                                    <a class="primary" data-action="{{asset('/admin/delete-basic').'/'.$row->bas_id}}" data-confirm="اطمینان از حذف این مورد دارید؟" data-method="delete">
                                                                        <i class="fa fa-trash"></i>
                                                                    </a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        {{ $baseinfos->appends(request()->query())->links() }}
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
@section('script')
    <script src="{{ asset('/js/functions/storeAjax.js') }}"></script>
<script>

    $(document).ready(function () {
        $("#createBaiscInfo").click(function (e) {
            if(document.getElementById("status").checked) {
                document.getElementById('testNameHidden').disabled = true;
            }
            e.preventDefault();
            storeAjax($(this), 'post');
        });
    });
    </script>
@append
