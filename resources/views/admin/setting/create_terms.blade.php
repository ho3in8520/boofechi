@extends('master_page')
@section('title_browser') قوانین سامانه @endsection
@section('main_content')
    <section class="basic-elements">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header">قوانین سامانه</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                    </div>
                    <div class="card-body">
                        <div class="px-3">
                            <form class="form" method="post" action="" enctype="multipart/form-data">
                                @if($olds)

                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-12">
                                                <textarea rows="6" id="editor" class="form-control"
                                                          name="terms">{{formValue('terms',$olds,'pt_terms')}}</textarea>
                                        </div>

                                                <fieldset class="form-group position-relative">
                                                    <select class="form-control panel_type" hidden name="panel_type"
                                                            id="DefaultSelect">
                                                        {!! customForeach($bas_info,'bas_id','bas_value',$olds['pt_panel_id']) !!}
                                                    </select>
                                                </fieldset>

                                            <div class="col-md-5" style="margin-top: 30px">
                                                <div class="form-group">
                                                    <label for="checkbox">اجبار به تایید همه کاربران</label>
                                                    <input id='testNameHidden' type='hidden' value='0'
                                                           name='wizard[status]'>
                                                    <input type="checkbox" name="forced" id="status" value="1"
                                                           class="switchery form-control"
                                                           data-size="xs"
                                                           data-switchery="true"
                                                           style="display: none;">
                                                </div>
                                            </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <button id="createTerms" name="create_basic_info"
                                                    class="btn btn-success" type="button">
                                                ثبت
                                            </button>
                                        </div>
                                    </div>

                                    <div class="row form-actions">
                                        @endif
                                        <div class="col-12 table-responsive">
                                            <div class="summary"></div>
                                            <table class="table table-striped table-bordered sourced-data dataTable">
                                                <thead>
                                                <tr>
                                                    <th>ردیف</th>
                                                    <th>قوانین پنل</th>
                                                    <th>عملیات</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($fin_terms as $terms)
                                                    <tr>
                                                        <td>{{$loop->index +1}}</td>
                                                        <td style="text-align: center">{{$terms->bas_value}}</td>

                                                        <td style="text-align: center">
                                                            <a class=""
                                                               href="{{asset('admin/'.'create-terms'.'/'.$terms->pt_id)}}"
                                                               data-original-title="" title="">
                                                                <i class="fa fa-pencil"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
@section('script')
    <script src="{{ asset('/js/functions/storeAjax.js') }}"></script>
    <script src="{{asset('theme/ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('theme/ckeditor/adapters/jquery.js')}}"></script>
    <script>
        var editor = CKEDITOR.replace('editor', {
// Load the German interface.
            language: 'fa',
        });
        $(document).ready(function () {
            $("#createTerms").click(function (e) {
                if ($('#status').is(":checked")) {
                    var chekbox = $("#status").val()
                } else {
                    var chekbox = ''
                }
                $.ajax({
                    type: "POST",
                    url: '{{route('create-terms')}}',
                    data: {
                        terms: editor.getData(),
                        panel_type: $('select[name=panel_type]').val(),
                        forced: chekbox
                    },
                    success: function (html) {
                        var result = JSON.parse(html);
                        if (result.status == 100)
                            toastr.success(result.msg, 'موفق');
                        location.reload();
                    }
                });

            });
        });
    </script>
@append