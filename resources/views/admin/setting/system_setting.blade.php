@extends('master_page')
@section('title_browser') تنظیمات سامانه@endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header">تنظیمات سامانه</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form method="post" action="">
                        <div class="card-body">
                            <div class="card-block">
                                <h5>** واحد پولی</h5>
                                <hr>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="col-md-10">
                                            <label>واحد پولی</label>
                                            <select class="form-control" name="financial">
                                                {!! customForeach($financial,'bas_value','bas_value',$financial_unit) !!}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label>مالیات</label>
                                        <input class="form-control" name="taxes" placeholder="درصد"
                                               value="{{$tax ?? ''}}"
                                               style="width: 100px">

                                    </div>
                                </div>
                                <div class="card-block"></div>
                                <h5>**تنظیمات شرکت</h5>
                                <hr>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="col-md-11">
                                            <label>نقش پیشفرض دریافت کننده فاکتور</label>
                                            <select class="form-control select2" name="role_factor_receiver">
                                                {!! customForeach($role,'rol_id','rol_label',$old_role) !!}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-block"></div>
                                <h5>** تنظیمات عمومی</h5>
                                <hr>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>زمان منقضی شدن فاکتور های باز</label>
                                        <input class="form-control" value="{{$basket_life_time ?? ''}}"
                                               name="timeFactor"
                                               placeholder="دقیقه" style="width: 100px">

                                    </div>
                                    <div class="col-md-5">
                                        <label>زمان منقضی شدن خرید ویزیتور برای مغازه دار</label>
                                        <input class="form-control"
                                               value="{{$purchase_for_subscription_life_time ?? ''}}"
                                               name="timeFactorVisitor" placeholder="دقیقه" style="width: 100px">
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <label>فرجه محاسبه پورسانت</label>
                                        <input class="form-control" value="{{$forge_calc_porsant ?? ''}}"
                                               name="forgeCalcPorsant" placeholder="به روز" style="width: 100px">
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <label>تایم منقضی کد ویزیتور</label>
                                        <input class="form-control" value="{{$expire_time_visitor ?? ''}}"
                                               name="expireTimeVisitor" placeholder="به دقیقه" style="width: 100px">
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label>مبلغ بلوکه کیف پول</label>
                                        <input class="form-control" value="{{$checkout_request ?? ''}}"
                                               name="checkoutRequest" placeholder="5000" style="width: 100px">
                                    </div>

                                </div>
                                <div class="card-block"></div>
                                <h5>** پیامک ها</h5>
                                <hr>
                                <label>فعال | غیر فعال</label>
                                <div class="col-md-2">
                                    <input type="checkbox" class="switchery" data-size="xs" name="checkActive[sms]"
                                        {{(!empty($send_sms)?'checked':'')}}
                                    >
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>ارسال رمز عبور</label>
                                        <textarea rows="3" class='form-control'
                                                  name='sms[sendPassword]'>{{$sms_sendPassword ?? ''}}</textarea>
                                    </div>
                                    <div class="col-md-4">
                                        <label>خوش آمد گویی</label>
                                        <textarea rows="3" class='form-control'
                                                  name='sms[welcome]'>{{$sms_welcome ?? ''}}</textarea>
                                    </div>
                                    <div class="col-md-4">
                                        <label>فراموشی رمزعبور</label>
                                        <textarea rows="3" class='form-control'
                                                  name='sms[forgetPassword]'>{{$sms_forgetPassword ?? ''}}</textarea>
                                    </div>
                                    <div class="col-md-4">
                                        <label>فاکتور خرید</label>
                                        <textarea rows="3" class='form-control'
                                                  name='sms[factorBuy]'>{{$sms_factorBuy ?? ''}}</textarea>
                                    </div>
                                    <div class="col-md-4">
                                        <label>خرید ویزیتور برای مغازه دار</label>
                                        <textarea rows="3" class='form-control'
                                                  name='sms[sms_visitorBuy]'>{{$sms_visitorBuy ?? ''}}</textarea>
                                    </div>
                                    <div class="col-md-4">
                                        <label>کد تاییدیه تغییر شماره موبایل</label>
                                        <textarea rows="3" class='form-control'
                                                  name='sms[sms_changeMobile]'>{{$sms_changeMobile ?? ''}}</textarea>
                                    </div>
                                    <div class="col-md-4">
                                        <label>لغو فاکتور توسط شرکت</label>
                                        <textarea rows="3" class='form-control'
                                                  name='sms[sms_cancel_factor]'>{{$sms_cancel_factor ?? ''}}</textarea>
                                    </div>

                                </div>
                                <div class="card-block"></div>
                                <h5>** ایمیل ها</h5>
                                <hr>
                                <label>فعال | غیر فعال</label>
                                <div class="col-md-2">
                                    <input type="checkbox" class="switchery" data-size="xs" name="checkActive[email]"
                                        {{(!empty($send_email)?'checked':'')}}
                                    >
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>ارسال رمز عبور</label>
                                        <textarea rows="3" class='form-control'
                                                  name='email[sendPassword]'>{{$email_sendPassword ?? ''}}</textarea>
                                    </div>
                                    <div class="col-md-4">
                                        <label>خوش آمد گویی</label>
                                        <textarea rows="3" class='form-control'
                                                  name='email[welcome]'>{{$email_welcome ?? ''}}</textarea>
                                    </div>
                                    <div class="col-md-4">
                                        <label>فراموشی رمزعبور</label>
                                        <textarea rows="3" class='form-control'
                                                  name='email[forgetPassword]'>{{$email_forgetPassword ?? ''}}</textarea>
                                    </div>
                                    <div class="col-md-4">
                                        <label>فاکتور خرید</label>
                                        <textarea rows="3" class='form-control'
                                                  name='email[factorBuy]'>{{$email_factorBuy ?? ''}}</textarea>
                                    </div>
                                    <div class="col-md-4">
                                        <label>لینک تایید تغییر ایمیل</label>
                                        <textarea rows="3" class='form-control'
                                                  name='email[email_changeEmail]'>{{$email_changeEmail ?? ''}}</textarea>
                                    </div>
                                </div>
                                <button type="button" class="btn btn-success btn-loading" id="create"
                                        style="margin-top: 50px">ثبت
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('/js/functions/storeAjax.js') }}"></script>
    <script>
        $(document).ready(function () {
            $(document).on('click', '#create', function () {
                storeAjax($(this), 'POST', captionButton = "ثبت", progress = false, reloadPage = true, TimeOutActionNextStore = 2000, true)
            });
        });
    </script>
@endsection
