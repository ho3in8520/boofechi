@extends('master_page')
<?php
$title = "چیدمان منوها";
?>
@section('title_browser',$title)
@section('style')
    <link rel="stylesheet" href="{{asset('/css/menu/all.css')}}">
    <link rel="stylesheet" href="{{asset('/css/menu/fontawesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('/css/menu/bootstrap-iconpicker.min.css')}}">
    <style>
        .buttons {
            float: left;
            position: absolute;
            left: 5px
        }

        div:has > span.txt {
            margin-right: 10px !important;
        }

        .list-group-item div {
            padding-right: 15px !important;
        }
    </style>
@endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header"><?=$title?></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">

                        <p class="mb-0"></p>
                    </div>
                    <div class="card-body">
                        <div class="card-block">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="panel panel-primary">
                                        <div class="panel-body">
                                            <form id="frmEdit" class="form-horizontal">
                                                <div class="form-group">
                                                    <label for="text">عنوان لینک</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control item-menu" name="text"
                                                               id="text" placeholder="عنوان لینک">
                                                        <div class="input-group-append">
                                                            <button type="button" id="myEditor_icon"
                                                                    class="btn btn-outline-secondary iconpicker dropdown-toggle"
                                                                    data-original-title="" title=""
                                                                    aria-describedby="popover172882">
                                                                <i class="fas"></i>
                                                                <input type="hidden" value="fas fa-home">
                                                                <span class="caret"></span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <input type="hidden" name="icon" class="item-menu">
                                                </div>
                                                <div class="form-group">
                                                    <label for="href">URL</label>
                                                    <input type="text" style="direction: ltr !important;"
                                                           class="form-control item-menu" id="href" name="href"
                                                           placeholder="URL">
                                                </div>
                                                <div class="form-group">
                                                    <label for="target">مقصد لینک</label>
                                                    <select name="target" id="target" class="form-control item-menu">
                                                        <option value="_self">صفحه جاری</option>
                                                        <option value="_blank">صفحه خالی</option>
                                                        <option value="_top">پنجره جدید</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="title">توضیحات</label>
                                                    <input type="text" name="title" class="form-control item-menu"
                                                           id="title" placeholder="توضیحات">
                                                </div>
                                            </form>
                                            <div class="panel-footer">
                                                <button type="button" id="btnUpdate" class="btn btn-primary" disabled><i
                                                            class="fas fa-sync-alt"></i> ویرایش
                                                </button>
                                                <button type="button" id="btnAdd" class="btn btn-success"><i
                                                            class="fas fa-plus"></i> افزودن
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="card">
                                        <div class="card-body">
                                            <ul id="myEditor" class="sortableLists list-group">
                                                <?php
                                                $items = createHtmlMenu($menus);
                                                ?>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="mr-4">
                                                <button id="btnOutput" type="button"
                                                        class="btn btn-success btn-loading"><i
                                                            class="fas fa-check-square"></i> ذخیره
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="col-md-6"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{asset('/js/menu/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('/js/menu/jquery-menu-editor.min.js')}}"></script>
    <script src="{{asset('/js/menu/fontawesome5-3-1.min.js')}}"></script>
    <script src="{{asset('/js/menu/bootstrap-iconpicker.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            var arrayjson = {!! $items !!};
            var iconPickerOptions = {searchText: "جستجو...", labelHeader: "{0}/{1}"};
            var sortableListOptions = {
                placeholderCss: {'background-color': "#cccccc"}
            };

            var editor = new MenuEditor('myEditor', {listOptions: sortableListOptions, iconPicker: iconPickerOptions});
            editor.setForm($('#frmEdit'));
            editor.setUpdateButton($('#btnUpdate'));
            editor.setData(arrayjson);

            $('#btnOutput').on('click', function () {
                var str = editor.getString();
                $.post("{{url('/admin/save-menu')}}", {mnujson: str}, function (data) {
                    var result = JSON.parse(data);
                    console.log(result);
                    if (result.status == 100)
                        window.location.reload();
                });
            });

            $("#btnUpdate").click(function () {
                editor.update();
            });

            $('#btnAdd').click(function () {
                editor.add();
            });

            $('[data-toggle="tooltip"]').tooltip();

        });
    </script>
@endsection
