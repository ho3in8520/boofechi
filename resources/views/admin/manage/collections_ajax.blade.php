@extends('master_page')
@section('title_browser') لیست مجموعه ها @endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-12">
                <h2 class="content-header">لیست مجموعه ها</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <form action="{{asset('admin/collections-ajax')}}" method="post">
                                <div class="row justify-content-end">
                                    @can('add_collection')
                                        @if(!request()->ajax())
                                            <div class="col-md-2">
                                                <button class="btn btn-success btn-block" id="register" type="button">
                                                    ایجاد
                                                    مجموعه
                                                </button>
                                            </div>
                                        @endif
                                    @endcan
                                </div>
                                <div class="summary">{!!getSummary($result) !!}</div>
                                <table class="table table-striped table-bordered sourced-data dataTable">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>نام مجموعه</th>
                                        <th>نوع مجموعه</th>
                                        <th>تاریخ ثبت</th>
                                        <th>تلفن</th>
                                        <th>وضعیت پنل</th>
                                        <th>عملیات</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>-</td>
                                        <td>
                                            <input type="text" name="name" class="form-control"
                                                   value="{{ request()->query('name') }}"
                                                   placeholder="نام مجموعه">
                                        </td>
                                        <td>
                                            <select name="type" class="form-control">
                                                <option value="0" {{request()->query('type') == "" ? 'selected' : ''}}>
                                                    انتخاب کنید...
                                                </option>
                                                <option value="44" {{request()->query('type') == 44 ? 'selected' : ''}}>
                                                    فروشگاه
                                                </option>
                                                <option value="45" {{request()->query('type') == 45 ? 'selected' : ''}}>
                                                    ویزیتور
                                                </option>
                                                <option value="43" {{request()->query('type') == 43 ? 'selected' : ''}}>
                                                    عمده فروش
                                                </option>
                                                <option value="42" {{request()->query('type') == 42 ? 'selected' : ''}}>
                                                    شرکت
                                                </option>
                                                <option value="41" {{request()->query('type') == 41 ? 'selected' : ''}}>
                                                    کارخانه
                                                </option>
                                            </select>
                                        </td>
                                        <td>
                                            <div class="input-group">
                                                <input type="text" class="form-control datePicker"
                                                       name="date_from" placeholder="تاریخ از"
                                                       autocomplete="off"
                                                       value="{{request()->query('date_from')}}">
                                                <input type="text" class="form-control datePicker"
                                                       name="date_to" placeholder="تاریخ تا"
                                                       autocomplete="off"
                                                       value="{{request()->query('date_to')}}">
                                            </div>
                                        </td>
                                        <td>
                                            <input type="text" name="phone" class="form-control"
                                                   value="{{ request()->query('phone') }}"
                                                   placeholder="تلفن">
                                        </td>
                                        <td>
                                            <select name="status" class="form-control">
                                                <option value="0" {{request()->query('status') == "" ? 'selected' : ''}}>
                                                    انتخاب کنید...
                                                </option>
                                                <option value="1" {{request()->query('status') == 1 ? 'selected' : ''}}>
                                                    فعال
                                                </option>
                                                <option value="2" {{request()->query('status') == 2 ? 'selected' : ''}}>
                                                    غیرفعال
                                                </option>


                                            </select>
                                        </td>
                                        <td>
                                            <button type="submit"
                                                    class="btn-loading btn btn-primary search-ajax">
                                                اعمال فیلتر <i class="ft-thumbs-up position-right"></i>
                                            </button>
                                        </td>
                                    </tr>

                                    @foreach($result as $row)
                                        <?php
                                        if ($row->is_active == 1) {
                                            $status = '<span class="badge text-white badge-success">فعال</span>';
                                        } else {
                                            $status = '<span class="badge text-white badge-danger">غیر فعال</span>';
                                        }
                                        ?>

                                        <tr data-percent="{{withoutZeros($row->porsant_percent)}}"
                                            data-id="{{$row->coll_id}}"
                                            data-name="{{ $row->coll_name }}" class="popup-selectable">
                                            <td>{{index($result,$loop)}}</td>
                                            <td>{{$row->coll_name}}</td>
                                            <td>
                                                @if($row->panel_type==44)
                                                    {{'فروشگاه'}}
                                                @elseif($row->panel_type==45)
                                                    {{'ویزیتور'}}
                                                @elseif($row->panel_type==42)
                                                    {{'شرکت'}}
                                                @elseif($row->panel_type==41)
                                                    {{'کارخانه'}}
                                                @elseif($row->panel_type==46)
                                                    {{'نماینده'}}
                                                @elseif($row->panel_type==43)
                                                    {{'عمده فروش'}}
                                                @elseif($row->panel_type==39)
                                                    {{'ادمین سامانه'}}
                                                @endif
                                            </td>
                                            <td>{{jdate_from_gregorian($row->created_at)}}</td>
                                            <td>{{$row->prsn_phone1}}</td>
                                            <td>{!! $status !!}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {!! $result->appends(request()->query())->render() !!}

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
