@extends('master_page')
@section('title_browser') {{isset($result)? 'ویرایش تراکنش' : 'ثبت تراکنش'}} @endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <div class="card-header">
                            </div>
                            <div class="card-body">
                                <div class="px-3">
                                    <form method="post"
                                          action="{{isset($result)? asset('admin/create-transaction/'.$id) : asset('admin/edit-transaction')}}"
                                          enctype="multipart/form-data">
                                        <div class="form-body">
                                            <div class="row">
                                                <div class="form-group">
                                                    <div class="col-md-10">
                                                        <input type="text" id="currency" class="form-control" name="amount" chars="float"
                                                               placeholder="مبلغ">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-10">
                                                        <input type="text" class="form-control datePicker" name="date" chars="date"
                                                               placeholder="تاریخ">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-10">
                                                        <input type="text" class="form-control" name="issueTracking" chars="int"
                                                               placeholder="شماره پیگیری">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <select class="form-control" name="transactionType">
                                                            {!! customForeach($transaction,'bas_id','bas_value') !!}
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <button type="button"
                                                            class="btn btn-info collection">{{isset($result)? $result->coll_name :'کاربر'}}
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions">
                                            <button type="button" class="btn btn-success btn-loading createAdvertising"
                                                    id="createAdvertising">
                                                ثبت
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('/js/functions/storeAjax.js') }}"></script>
    <script>
        $(document).ready(function () {
            $(document).on('click', '.createAdvertising', function (e) {
                storeAjax($(this), 'POST', captionButton = "ثبت", progress = false, reloadPage = true, TimeOutActionNextStore = 2000)

            });

            $(document).on('click', '.collection', function () {
                showData('{{asset('/admin/search_user_transaction')}}');
                trClick = $(this);
            });
            var format = function(num){
                var str = num.toString(), parts = false, output = [], i = 1, formatted = null;
                if(str.indexOf(".") > 0) {
                    parts = str.split(".");
                    str = parts[0];
                }
                str = str.split("").reverse();
                for(var j = 0, len = str.length; j < len; j++) {
                    if(str[j] != ",") {
                        output.push(str[j]);
                        if(i%3 == 0 && j < (len - 1)) {
                            output.push(",");
                        }
                        i++;
                    }
                }
                formatted = output.reverse().join("");
                return( formatted + ((parts) ? "." + parts[1].substr(0, 2) : ""));
            };
            $(function(){
                $("#currency").keyup(function(e){
                    $(this).val(format($(this).val()));
                });
            });
        });
    </script>
@append
