@extends('master_page')
@section('main_content')
    <section id="clear-toasts">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <div class="row mt-1">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <img src="{{getFile($details->avatar)}}"
                                         class="rounded-circle" style="height: 300px
                                          ; width: 300px;">
                                </div>
                                <div class="col-lg-6 col-md-4 col-sm-6" style="margin-top: 50px">
                                    <h5>
                                        @if($details->panel_type==44)
                                            فروشگاه : {{$details->coll_name}}
                                        @elseif($details->panel_type==45)
                                            ویزیتور : {{$details->coll_name}}
                                        @elseif($details->panel_type==42)
                                            شرکت : {{$details->coll_name}}
                                        @elseif($details->panel_type==41)
                                            کارخانه : {{$details->coll_name}}
                                        @elseif($details->panel_type==46)
                                            نماینده : {{$details->coll_name}}
                                        @elseif($details->panel_type==43)
                                            عمده فروش : {{$details->coll_name}}
                                        @elseif($details->panel_type==38)
                                            ادمین سامانه
                                        @endif

                                    </h5>
                                    <p class="user-panel-status"><strong>وضعیت پنل : </strong>
                                        @if($details->is_active == 0)
                                            <span class="badge badge-danger text-white">غیر فعال</span>
                                        @else
                                            <span class="badge badge-success text-white">فعال</span>
                                        @endif
                                    </p>
                                    <p><strong>آدرس : </strong>{{$details->prsn_address}}
                                    </p>
                                    <p><strong>شماره تماس : </strong>{{$details->prsn_mobile1}}</p>
                                    <p><strong>ایمیل : </strong>{{$details->email}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('/js/functions/storeAjax.js') }}"></script>
    <script>
        $(document).ready(function () {
            $(document).on('click', '#send_sendMessage', function (e) {
                storeAjax($(this), 'POST')
            });
            $(document).on('click', '#send_report', function (e) {
                storeAjax($(this), 'POST')
            });
        });
    </script>
@endsection