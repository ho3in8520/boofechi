@extends('master_page')
<?php
$title = "لیست شکایات";
?>
@section('title_browser',$title)
@section('style')@endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header"><?=$title?></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                    </div>
                    <div class="card-body">
                        <div class="card-block">
                            <form action="{{asset('admin/reports')}}" method="get">
                                <table class="table table-striped table-bordered sourced-data dataTable">
                                    <thead>
                                    <tr>
                                        <th>ردیف</th>
                                        <th>گزارش دهنده</th>
                                        <th>متخلف</th>
                                        <th>عنوان</th>
                                        <th>تاریخ</th>
                                        <th>عملیات</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th>-</th>
                                        <th><input class="form-control" name="sender"
                                                   value="{{request()->query('sender')}}"></th>
                                        <th><input class="form-control" name="receiver"
                                                   value="{{request()->query('receiver')}}"></th>
                                        <th><input class="form-control" name="message"
                                                   value="{{request()->query('message')}}"></th>
                                        <th>
                                            <div class="input-group">
                                                <input class="form-control datePicker" placeholder="از"
                                                       type="text" name="date_from"
                                                       value="{{request()->query('date_from')}}">
                                                <input class="form-control datePicker" placeholder="تا"
                                                       type="text" name="date_to"
                                                       value="{{request()->query('date_to')}}">
                                            </div>
                                        </th>
                                        <th>
                                            <button class="btn btn-primary btn-loading search-ajax"> اعمال فیلتر
                                                <i
                                                        class="ft-thumbs-up position-right"></i></button>
                                        </th>
                                    </tr>
                                    @if($reports->count() != 0)
                                        @foreach($reports->items() as $report)
                                            <tr>
                                                <td>{{index($reports,$loop)}}</td>
                                                <td>{{$report->prsn_name}}</td>
                                                <td>{{$report->coll_name}}</td>
                                                <td>{{mb_substr($report->msg_message,0,50).'...'}}</td>
                                                <td>{{jdate($report->msg_created_at)->format("Y/m/d H:i:s")}}</td>
                                                <td>
                                                    <a class="viewReport" data-id="{{$report->msg_id}}" title="مشاهده">
                                                        <i class="fa fa-eye"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="8" style="text-align: center;">
                                                {{ config('first_config.message.empty_table') }}
                                            </td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                                <input type='hidden' name='tab' value='{{request()->query('tab')}}'>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{asset('/js/functions/popup.js')}}"></script>
    <script>
        $(document).on("click", '.viewReport', function () {
            var id = $(this).data("id");
            var buttons = {
                0: {
                    title: 'ثبت اخطار برای متخلف',
                    data: {
                        id: id
                    },
                    class: 'btn btn-warning addViolation'
                },
            };
            showData(APP_URL + '/admin/view-report/' + id, {}, true, "#basic-form-layouts", buttons);
        });
        $(document).on("click", '.addViolation', function () {
            var id = $(this).attr("data-id");
            if(confirm('اطمینان از ثبت اخطار دارید؟') == false)
                return false;
            $.post(APP_URL + '/admin/add-violation/' + id, function (data) {
                var result = JSON.parse(data);
                if (result.status == 100)
                    toastr.success(result.msg, 'موفق');
                else
                    toastr.error(result.msg, 'ناموفق');
            });
        });
    </script>
@endsection