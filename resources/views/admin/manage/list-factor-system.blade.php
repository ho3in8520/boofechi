@extends('master_page')
@section('title_browser') مدیریت فاکتور های سامانه @endsection
@section('style')
    <link rel="stylesheet" type="text/css" href="{{asset("/theme/vendors/css/sweetalert2.min.css")}}">

    <style>

        .name-company {
            margin-left: auto;
            margin-right: 150px;
        }

        .bold {
            font-weight: bold;
        }

        a {
            color: #757575;
        }

        a:hover {
            color: #757575;
        }

    </style>

@endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-12">
                <h2 class="content-header">مدیریت فاکتور های سامانه</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <form action="" method="get">
                                <div class="summary">{!!getSummary($result) !!}</div>
                                <table class="table table-striped table-bordered sourced-data dataTable">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>کاربر</th>
                                        <th>کاربر ایجاد کننده</th>
                                        <th>تاریخ ثبت</th>
                                        <th>مبلغ کل فاکتور</th>
                                        <th>وضعیت فاکتور</th>
                                        <th>وضعیت پرداخت</th>
                                        <th>عملیات</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>-</td>
                                        <td>
                                            <input type="text" name="name" class="form-control"
                                                   value="{{ request()->query('name') }}"
                                                   placeholder="نام کاربر">
                                        </td>
                                        <td>
                                            <input type="text" name="userCreate" class="form-control"
                                                   value="{{ request()->query('userCreate') }}"
                                                   placeholder="کاربر ایجاد کننده">
                                        </td>
                                        <td>
                                            <div class="input-group">
                                                <input type="text" class="form-control datePicker"
                                                       name="date" placeholder="تاریخ"
                                                       autocomplete="off"
                                                       value="{{ request()->query('date') }}">

                                            </div>
                                        </td>
                                        <td>
                                            <input type="text" name="amount" class="form-control"
                                                   value="{{ request()->query('amount') }}"
                                                   placeholder="نام ویزیتور">
                                        </td>
                                        <td>
                                            <select name="mode" class="form-control">
                                                <option value="0" {{request()->query('mode') == "" ? 'selected' : ''}}>
                                                    انتخاب کنید...
                                                </option>
                                                <option value="1" {{request()->query('mode') == 1 ? 'selected' : ''}}>
                                                    صادر نشده
                                                </option>
                                                <option value="2" {{request()->query('mode') == 2 ? 'selected' : ''}}>
                                                    صادر شده
                                                </option>


                                            </select>
                                        </td>
                                        <td>
                                            <select name="is_payed" class="form-control">
                                                <option value="0" {{request()->query('is_payed') == "" ? 'selected' : ''}}>
                                                    انتخاب کنید...
                                                </option>
                                                <option value="1" {{request()->query('is_payed') == 1 ? 'selected' : ''}}>
                                                    پرداخت نشده
                                                </option>
                                                <option value="2" {{request()->query('is_payed') == 2 ? 'selected' : ''}}>
                                                    پرداخت شده
                                                </option>


                                            </select>
                                        </td>
                                        <td>
                                            <button type="submit"
                                                    class="btn-loading btn btn-primary search-ajax">
                                                اعمال فیلتر <i class="ft-thumbs-up position-right"></i>
                                            </button>
                                        </td>
                                    </tr>


                                    @foreach($result as $row)
                                        <?php
                                        $sum=$row->sfm_amount-$row->sfm_discount
                                        ?>
                                        <tr>
                                            <td>{{index($result ,$loop)}}</td>
                                            <td>{{$row->coll_name}}</td>
                                            <td>{{$row->username}}</td>
                                            <td>{{jdate($row->sfm_created_at)->format("Y/m/d")}}</td>
                                            <td>{{number_format(withoutZeros($sum))}}</td>
                                            <td>
                                                @if($row->sfm_mode==0)
                                                    <span class="badge text-white badge-info">صادر نشده</span>
                                                @else
                                                    <span class="badge text-white badge-success">صادر شده</span>
                                                @endif
                                            </td>
                                            <td>
                                                @if($row->sfm_is_payed==0 && $row->sfm_is_force==1)
                                                    <span class="badge text-white badge-danger">پرداخت فوری</span>
                                                @elseif($row->sfm_is_payed==0)
                                                    <span class="badge text-white badge-warning">پرداخت نشده</span>
                                                @else
                                                    <span class="badge text-white badge-success">پرداخت شده</span>
                                                @endif
                                            </td>
                                            <td>
                                                @if($row->sfm_mode!=1)
                                                    <a title="ویرایش" href="{{asset('admin/edit-system-factor'.'/'.$row->sfm_id)}}">
                                                        <i class="fa fa-pencil"></i>
                                                    </a>
                                                    <a title="ارسال"
                                                       data-action="{{asset('admin/list-system-factor').'/'.$row->sfm_id}}"
                                                       data-confirm="اطمینان از ارسال این مورد دارید؟"
                                                       data-method="post">
                                                        <i class="fa fa-send"></i>
                                                    </a>
                                                @else
                                                    <a title="برگشت فاکتور"
                                                       data-action="{{asset('admin/return-system-factor').'/'.$row->sfm_id}}"
                                                       data-confirm="اطمینان از ارسال این مورد دارید؟"
                                                       data-method="post">
                                                        <i class="fa fa-undo"></i>
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {!! $result->appends(request()->query())->render() !!}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('/js/functions/storeAjax.js') }}"></script>
    <script src="{{asset("/theme/vendors/js/sweetalert2.min.js")}}"></script>

    <script>
        $(document).ready(function () {

            $(document).on('click', '#view', function (e) {
                storeAjax($(this), 'POST', captionButton = "خواندن", progress = false, reloadPage = true, TimeOutActionNextStore = 2000)

            });
            $(document).on("click", ".commission", function () {
                var element = $(this);
                swal({
                    title: "پورسانت",
                    text: "درصد پورسانت را وارد کنید:",
                    input: "text",
                    showCancelButton: true,
                    animation: "slide-from-top",
                    inputValue: $(element).closest("tr").data("percent"),
                    confirmButtonText: 'ثبت',
                    cancelButtonText: "انصراف",
                }).then(function (inputValue) {
                    if (inputValue === false) return false;
                    var id = $(element).closest("tr").data('id');
                    $.post("{{asset('/visitor-list')}}", {id: id, percent: inputValue}, function (data) {
                        var result = JSON.parse(data);
                        if (result.status == 100) {
                            toastr.success(result.msg, "موفق");
                            setTimeout(function () {
                                location.reload();
                            }, 2000);
                        }
                    });
                }).done();
            });
        });
    </script>
@append