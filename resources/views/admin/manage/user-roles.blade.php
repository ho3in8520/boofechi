@extends('master_page')
@section('style')@endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <div class="col-sm-12">
                                <form method="GET" action="{{url()->current()}}">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <select class="form-control" name="role">
                                                {!! customForeach($my_roles,'rol_id','rol_label',0,false) !!}
                                            </select>
                                        </div>
                                        <div class="col-sm-4">
                                            <a class="btn btn-success addRoleToUser">ثبت</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-12 table-responsive">
                                <table class="table table-striped table-bordered sourced-data dataTable">
                                    <thead>
                                    <tr>
                                        <th>ردیف</th>
                                        <th>عنوان فارسی</th>
                                        <th>عنوان انگلیسی</th>
                                        <th>عملیات</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($roles) != 0)
                                        <?php
                                        $i = 0;
                                        ?>
                                        @foreach($roles as $role)
                                            <?php
                                            $i++;
                                            ?>
                                            <tr data-id="<?=$role->ur_rol_id?>">
                                                <td>{{$i}}</td>
                                                <td>{{$role->rol_label}}</td>
                                                <td>{{$role->rol_name}}</td>
                                                <td>
                                                    <a data-confirm="اطمینان از حذف این محصول دارید" data-action="{{asset('admin/delete-role').'/'.$role->ur_rol_id}}" data-id="{{$id}}" data-method="post">
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="8" style="text-align: center;">
                                                {{ config('first_config.message.empty_table') }}
                                            </td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
