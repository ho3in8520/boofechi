@extends('master_page')
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <div class="col-md-12 table-responsive">
                                <form method="GET" action="{{asset('admin/support-tickets').'/'.$id}}">
                                    <div class="summary">{!!getSummary($tickets) !!}</div>
                                    <table class="table table-striped table-bordered sourced-data dataTable">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>شماره تیکت</th>
                                            <th>عنوان</th>
                                            <th>بخش</th>
                                            <th>تاریخ</th>
                                            <th>آخرین وضعیت</th>
                                            <th>عملیات</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>#</td>
                                            <td><input class="form-control" type="text" name="ticket_no"
                                                       value="{{request()->query('ticket_no')}}"></td>
                                            <td><input class="form-control" type="text" name="ticket_subject"
                                                       value="{{request()->query('ticket_subject')}}"></td>
                                            <td><select class="form-control" name="ticket_department">
                                                    {!! customForeach($departments,'rol_id','rol_label',request()->query('ticket_department')) !!}
                                                </select></td>
                                            <td><div class="input-group">
                                                    <input type="text" class="form-control datePicker" name="date_from" placeholder="از" value="{{request()->query('date_from')}}">
                                                    <input type="text" class="form-control datePicker" name="date_to" placeholder="تا" value="{{request()->query('date_to')}}">
                                                </div></td>
                                            <td>-</td>
                                            <td>
                                                <button type="submit" class="btn-loading btn btn-primary search-ajax">
                                                    اعمال فیلتر
                                                    <i class="ft-thumbs-up position-right"></i>
                                                </button>
                                            </td>
                                        </tr>
                                        @foreach($tickets as $ticket)
                                            <tr data-id="{{$ticket->t_id}}">
                                                <td>{{index($tickets,$loop)}}</td>
                                                <td>{{$ticket->t_id}}</td>
                                                <td>{{$ticket->t_subject}}</td>
                                                <td>{{$ticket->rol_label}}</td>
                                                <td>{{jdate_from_gregorian($ticket->t_created_at)}}</td>
                                                <td>
                                                        <span class="badge text-white"
                                                              style="background-color: {{$statuses[$ticket->t_status]['color']}}">{{$statuses[$ticket->t_status]['value']}}</span>
                                                </td>
                                                <td>
                                                    <a class="viewTicket" title="مشاهده">
                                                        <i class="fa fa-eye"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    {!! $tickets->appends(request()->query())->render() !!}
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection