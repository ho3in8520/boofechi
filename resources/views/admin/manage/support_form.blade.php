@extends('master_page')
@section('title_browser') فرم پشتیبانی @endsection
@section('style')
    <link rel="stylesheet" type="text/css" href="{{asset("/theme/vendors/css/sweetalert2.min.css")}}">
    <style>
        li.active > a {
            color: #28D094 !important;
        }

        li.skew-link {
            width: 120px;
            margin-left: -30px;
        }

        .skew-link {
            display: inline-block;
            transform: rotate(-60deg);
            margin-top: 20px;
            border-top: 2px solid white;
            margin-right: -40px;
        }

        .nav {
            padding-right: 20px;
        }

        #link-navs {
            height: 80px;
            background-color: slategray;
        }

        #link-navs a {
            color: white;
            width: 100px !important;
        }

        /*span {*/
        /*color: #fff;*/
        /*display: block;*/
        /*text-decoration: none;*/
        /*padding: 10px 30px;*/
        /*transition: 0.3s;*/
        /*transform: skew(20deg);*/
        /*}*/
    </style>
@endsection
@section('main_content')
    <section id="clear-toasts">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header">
                    فرم پشتیبانی
                </h2>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="nav-tabs-custom">
                                        <ul class="nav nav-tabs" id="link-navs">
                                            <li class="skew-link active">
                                                <a class="btn" id="information"
                                                   data-action="{{asset('admin/user-profile').'/'.$id}}"
                                                   href="#information"
                                                   data-toggle="tab"
                                                   aria-expanded="true">
                                                    <i class="fa fa-user"></i> مشخصات
                                                </a>
                                            </li>
                                            <li class="skew-link">
                                                <a class="btn"
                                                   data-action="{{asset('admin/user-documents').'/'.$id}}"
                                                   href="#documents" data-toggle="tab"
                                                   aria-expanded="true">
                                                    <i class="fa fa-file"></i> مدارک
                                                </a>
                                            </li>
                                            <li class="skew-link">
                                                <a class="btn"
                                                   data-action="{{asset('admin/support-tickets').'/'.$id}}"
                                                   href="#tickets" data-toggle="tab"
                                                   aria-expanded="true">
                                                    <i class="fa fa-ticket"></i> تیکت ها
                                                </a>
                                            </li>
                                            @if(findUser($id,'panel_type') == 'ویزیتور')
                                                <li class="skew-link">
                                                    <a class="btn"
                                                       data-action="{{asset('admin/support-subscriptions').'/'.$id}}"
                                                       href="#activity" data-toggle="tab"
                                                       aria-expanded="true">
                                                        <i class="fa fa-ticket"></i>زیر مجموعه
                                                    </a>
                                                </li>
                                            @endif
                                            <li class="skew-link">
                                                <a class="btn"
                                                   data-action="{{asset('admin/factors').'/'.$id}}"
                                                   href="#activity" data-toggle="tab"
                                                   aria-expanded="true">
                                                    <i class="fa fa-ticket"></i>فاکتورها
                                                </a>
                                            </li>
                                            <li class="skew-link">
                                                <a class="btn"
                                                   data-action="{{asset('admin/violations-log').'/'.$id}}"
                                                   href="#activity" data-toggle="tab"
                                                   aria-expanded="true">
                                                    <i class="fa fa-ticket"></i>گزارشات
                                                </a>
                                            </li>
                                            @if(findUser($id,'panel_type') == 'ویزیتور')
                                                <li class="skew-link">
                                                    <a class="btn"
                                                       data-action="{{asset('admin/porsant').'/'.$id}}"
                                                       href="#activity" data-toggle="tab"
                                                       aria-expanded="true">
                                                        <i class="fa fa-ticket"></i>پورسانت
                                                    </a>
                                                </li>
                                            @endif
                                            <li class="skew-link">
                                                <a class="btn"
                                                   data-action="{{asset("admin/user-announcements").'/'.$id}}"
                                                   href="#activity" data-toggle="tab"
                                                   aria-expanded="true">
                                                    <i class="fa fa-ticket"></i>اطلاعیه ها
                                                </a>
                                            </li>
                                            <li class="skew-link">
                                                <a class="btn"
                                                   data-action="{{asset("admin/user-roles").'/'.$id}}"
                                                   href="#activity" data-toggle="tab"
                                                   aria-expanded="true">
                                                    <i class="fa fa-ticket"></i>نقش ها
                                                </a>
                                            </li>
                                            @can('manage_wallet')
                                            <li class="skew-link">
                                                <a class="btn"
                                                   data-action="{{asset("admin/wallet").'/'.$id}}"
                                                   href="#activity" data-toggle="tab"
                                                   aria-expanded="true">
                                                    <i class="fa fa-ticket"></i>کیف پول
                                                </a>
                                            </li>
                                            @endcan
                                            @if(in_array(findUser($user->user_id,'panel_type','id'),[41,42,43]))
                                                <li class="skew-link">
                                                    <a class="btn"
                                                       data-action="{{asset("admin/user-setting").'/'.$id}}"
                                                       href="#activity" data-toggle="tab"
                                                       aria-expanded="true">
                                                        <i class="fa fa-ticket"></i>تنظیمات
                                                    </a>
                                                </li>
                                            @endif
                                        </ul>
                                        <div>
                                            <div class="col-sm-12 row"
                                                 style="margin-right: 0;padding: 5px;color: white;background-color: {{getBarColor(findUser($user->user_id,'panel_type','id'))}}">
                                                <div class="col-sm-2">نمایندگی:</div>
                                                <div class="col-sm-2">{{findUser($user->user_id,'agent')}}</div>
                                                <div class="col-sm-2">نام کاربری:</div>
                                                <div class="col-sm-3"
                                                     style="direction: ltr !important;">{{$user->username.'('.$user->code.')'}}</div>
                                                <div class="col-sm-1">نوع پنل:</div>
                                                <div class="col-sm-2">{{findUser($user->user_id,'panel_type')}}</div>
                                            </div>
                                        </div>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="admin-support-form-loaded-data-holder">
                                                <i class="fa fa-spinner fa-spin"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div style="color: orange"></div>
@endsection
@section('script')
    <script src="{{asset("/theme/vendors/js/sweetalert2.min.js")}}"></script>
    <script src="{{ asset('/js/functions/storeAjax.js') }}"></script>
    <script src="{{ asset('/js/functions/popup.js') }}"></script>
    <script src="{{ asset('/js/functions/supportForm.js') }}"></script>
    <script>
        $(document).ready(function () {

            $.get($("#information").data("action"), function (data) {
                var result = JSON.parse(data);
                $("#admin-support-form-loaded-data-holder").html(result.FormatHtml);
            });

            $(".skew-link a").click(function (e) {
                e.preventDefault();
                var elem = $(this);
                $("#admin-support-form-loaded-data-holder").html('<i class="fa fa-spinner fa-spin"></i>');
//                $(".tab-content").hide();
//                $(".tab-pane[id='" + ($(elem).attr("href")).substring(1) + "']").parent().show();
                $("li.active").removeClass("active");
                $(elem).closest("li").addClass("active");
                $.get($(elem).data("action"), function (data) {
                    var result = JSON.parse(data);
                    $("#admin-support-form-loaded-data-holder").html(result.FormatHtml);
                });
            });

            $(document).on("click", ".password", function () {
                var element = $(this);
                swal({
                    title: "تغییر رمزعبور پنل کاربر",
                    text: "رمزعبور جدید را وارد کنید:",
                    input: "password",
                    showCancelButton: true,
                    animation: "slide-from-top",
                    confirmButtonText: 'ثبت',
                    cancelButtonText: "انصراف",
                }).then(function (inputValue) {
                    if (inputValue === false) return false;
                    var id = $(element).closest("button").data('id');
                    $.post("{{asset('/admin/change-password')}}", {id: id, password: inputValue}, function (data) {
                        var result = JSON.parse(data);
                        if (result.status == 100) {
                            toastr.success(result.msg, "موفق");
                            setTimeout(function () {
                            });
                        }
                    });
                }).done();
            });

            $(document).on("click", ".wallet", function () {
                var element = $(this);
                swal({
                    title: "افزایش | کاهش کیف پول",
                    input: 'select',
                    inputOptions: {
                        1: 'افزایش',
                        0: 'کاهش',
                    },
                    html:
                        '<div class="row justify-content-center">' +
                        '<div class="col-md-3 col-sm-3">' +
                        '<input id="amount" onkeyup="if (/\\D/g.test(this.value)) this.value = this.value.replace(/\\D/g,\'\');" class="form-control" placeholder="مبلغ">' +
                        '</div>' +
                        '</div>' +
                        '<div class="row justify-content-center" style="margin-top: 20px">' +
                        '<div class="col-md-5 col-sm-5">' +
                        '<textarea id="description" class="form-control" placeholder="توضیحات"></textarea>' +
                        '</div>' +
                        '</div>',
                    showCancelButton: true,
                    animation: "slide-from-top",
                    confirmButtonText: 'ثبت',
                    cancelButtonText: "انصراف",
                    inputPlaceholder: 'انتخاب کنید...',
                    inputValidator: function (value) {
                        return new Promise(function (resolve, reject) {
                            if (value !== '' && $('#amount').val() !== '') {
                                resolve();
                            } else {
                                reject('فیلد ها الزامی هستن');
                            }
                        });
                    }
                }).then(function (inputValue) {
                    if (inputValue === false) return false;
                    var id = $(element).closest("button").data('id');
                    var amount = $('#amount').val();
                    var description = $('#description').val();
                    $.post("{{asset('admin/update-wallet')}}", {
                        id: id,
                        type: inputValue,
                        amount: amount,
                        description: description
                    }, function (data) {
                        var result = JSON.parse(data);
                        if (result.status == 100) {
                            toastr.success(result.msg, "موفق");
                            setTimeout(function () {
                            });
                        }else if(result.status == 300) {
                            toastr.error(result.msg, "مبلغ کیف پول کاربر کمتر از حد مشخص شده است");
                        }else if(result.status == 500) {
                            toastr.error(result.msg, "خطا در ثبت اطلاعات!");
                        }
                    });
                }).done();
            });
            $(document).on('click', '.login-panel', function () {
                $.post("{{ route("admin.login.panel.user") }}", {
                    id: $(this).data('id'),
                }, function (data) {
                    var result = JSON.parse(data);
                    if (result.status == 100) {
                        window.location.href = "{{ asset('/') }}";
                    }
                });
            });
        });
    </script>
@endsection
