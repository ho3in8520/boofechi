@extends('master_page')
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <form method="post" action="{{asset('admin/support-subscriptions').'/'.$id}}">
                                <table class="table table-striped table-bordered sourced-data dataTable text-center">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>آواتار</th>
                                        <th>نام مغازه دار یا کد</th>
                                        <th>استان/شهر</th>
                                        <th>تاریخ عضویت</th>
                                        <th>عملیات</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>#</td>
                                        <td>-</td>
                                        <td><input type="text" name="name" class="form-control"
                                                   value="{{request()->query('name')}}"></td>
                                        <td>
                                            <div class="input-group">
                                                <select class="form-control state" name="state">
                                                    {!! customForeach($states,'c_id','c_name',request()->query('state')) !!}
                                                </select>
                                                <select class="form-control city" name="city">
                                                    {!! customForeach($cities,'c_id','c_name',request()->query('city')) !!}
                                                </select>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="input-group">
                                                <input type="text" class="form-control datePicker"
                                                       name="date_from" placeholder="تاریخ از"
                                                       autocomplete="off" value="{{request()->query('date_from')}}">
                                                <input type="text" class="form-control datePicker"
                                                       name="date_to" placeholder="تاریخ تا"
                                                       autocomplete="off" value="{{request()->query('date_to')}}">
                                            </div>
                                        </td>
                                        <td>
                                            <button type="submit" class="btn-loading btn btn-primary search-ajax">
                                                اعمال فیلتر
                                                <i class="ft-thumbs-up position-right"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    @if($subscription->count())
                                        @foreach($subscription as $row)
                                            <tr data-code="{{$row->code}}" class="popup-selectable" data-id="{{$row->user_id}}" data-status="{{$row->is_active}}">
                                                <td>{{index($subscription,$loop)}}</td>
                                                <td><img class="rounded-circle" src="{{getFile($row->avatar)}}" style="width: 50px;height: 50px"></td>
                                                <td>{{$row->name}}</td>
                                                <td>{{$row->state.'/'.$row->city}}</td>
                                                <td>{{jdate_from_gregorian($row->created_at,"Y/m/d")}}</td>
                                                <td>
                                                    <a class="showUser" title="مشاهده"><i class="fa fa-eye"></i></a>
                                                    <a class="showPermissions" title="پرمیژن ها"><i class="fa fa-star"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="5">
                                                {{ config('first_config.message.empty_table') }}
                                            </td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection