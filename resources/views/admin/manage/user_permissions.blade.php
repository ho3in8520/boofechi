@extends('master_page')
@section('main_content')
    <section id="clear-toasts">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <div class="row mt-1">
                                <div class="col-12">
                                    @foreach($permissions as $permission)
                                        <input style="margin-right: 10px" readonly="readonly" data-color="#E91E63" type="checkbox" checked>{{$permission}}
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('/js/functions/storeAjax.js') }}"></script>
    <script>
        $(document).ready(function () {
            $(document).on('click', '#send_sendMessage', function (e) {
                storeAjax($(this), 'POST')
            });
            $(document).on('click', '#send_report', function (e) {
                storeAjax($(this), 'POST')
            });
        });
    </script>
@endsection