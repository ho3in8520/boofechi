@extends('master_page')
@section('title_browser') لیست تراکنش های سامانه @endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-12">
                <h2 class="content-header">لیست تراکنش ها</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <form action="" method="get">
                                <div class="summary">{!!getSummary($result) !!}</div>
                                <table class="table table-striped table-bordered sourced-data dataTable">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>نام مجموعه</th>
                                        <th>مبلغ</th>
                                        <th>تاریخ</th>
                                        <th>نوع تراکنش</th>
                                        <th>شماره پیگیری</th>
                                        <th>کاربر ایجاد کننده</th>
                                        <th>عملیات</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>-</td>
                                        <td>
                                            <input type="text" name="collection" class="form-control"
                                                   value="{{ request()->query('collection') }}"
                                                   placeholder="نام مجموعه">
                                        </td>
                                        <td>
                                            <input type="text" name="amount" class="form-control"
                                                   value="{{ request()->query('amount') }}"
                                                   placeholder="مبلغ">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control datePicker"
                                                   name="date" placeholder="تاریخ"
                                                   autocomplete="off"
                                                   value="{{ request()->query('date') }}">
                                        </td>
                                        <td>
                                            <select name="transactionType" class="form-control">
                                                <option value="" {{request()->query('mode') == "" ? 'selected' : ''}}>
                                                    انتخاب کنید...
                                                </option>
                                                @foreach($type_request as $value)
                                                <option
                                                    value="{{$value->bas_id}}" {{request()->query('mode') == $value->bas_id ? 'selected' : ''}}>
                                                    {{$value->bas_value}}
                                                </option>
                                                    @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <input type="text" name="issueTracking" class="form-control"
                                                   value="{{ request()->query('issueTracking') }}"
                                                   placeholder="شماره پیگیری">
                                        </td>
                                        <td>
                                            <input type="text" name="userCreate" class="form-control"
                                                   value="{{ request()->query('userCreate') }}"
                                                   placeholder="کاربر ایجاد کننده">
                                        </td>
                                        <td>
                                            <button type="submit"
                                                    class="btn-loading btn btn-primary search-ajax">
                                                اعمال فیلتر <i class="ft-thumbs-up position-right"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    @if(count($result)>0)
                                    @foreach($result as $row)
                                    <tr>
                                        <td>{{index($result,$loop)}}</td>
                                        <td>{{$row->coll_name}}</td>
                                        <td>{{number_format($row->dp_amount)}}</td>
                                        <td>{{jdate_from_gregorian($row->dp_date,'Y/m/d')}}</td>
                                        <td>{{$row->bas_value}}</td>
                                        <td>{{$row->dp_tracking_code}}</td>
                                        <td>{{$row->prsn_name.' '.$row->prsn_family}}</td>
                                        @can('manage_Transaction')

                                        @endcan
                                    </tr>
                                        @endforeach
                                    @else
                                    <tr>
                                        <td colspan="7" style="text-align: center;">
                                            {{ config('first_config.message.empty_table') }}
                                        </td>
                                    </tr>
                                        @endif
                                    </tbody>
                                </table>
                                {!! $result->appends(request()->query())->render() !!}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('/js/functions/storeAjax.js') }}"></script>
    <script>
        $(document).ready(function () {

            $(document).on('click', '#view', function (e) {
                storeAjax($(this), 'POST', captionButton = "خواندن", progress = false, reloadPage = true, TimeOutActionNextStore = 2000)

            });
        });

    </script>
@append
