@extends('master_page')
@section('title_browser') لیست مجموعه ها @endsection
@section('style')
    <style>

        .name-company {
            margin-left: auto;
            margin-right: 150px;
        }

        .bold {
            font-weight: bold;
        }

        a {
            color: #757575;
        }

        a:hover {
            color: #757575;
        }

    </style>

@endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-12">
                <h2 class="content-header">لیست مجموعه ها</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <form action="{{asset('admin/collections')}}" method="post">
                                <div class="row justify-content-end">
                                    @can('add_collection')
                                        <div class="col-md-2">
                                            <button class="btn btn-success btn-block" id="register" type="button">
                                                ایجاد
                                                مجموعه
                                            </button>
                                        </div>
                                    @endcan
                                </div>
                                <div class="summary">{!!getSummary($result) !!}</div>
                                <table class="table table-striped table-bordered sourced-data dataTable">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>نام مجموعه</th>
                                        <th>نوع مجموعه</th>
                                        <th>تاریخ ثبت</th>
                                        <th>تلفن</th>
                                        <th>وضعیت پنل</th>
                                        <th>عملیات</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>-</td>
                                        <td>
                                            <input type="text" name="name" class="form-control"
                                                   value="{{ request()->query('name') }}"
                                                   placeholder="نام مجموعه">
                                        </td>
                                        <td>
                                            <select name="type" class="form-control">
                                                <option value="0" {{request()->query('type') == "" ? 'selected' : ''}}>
                                                    انتخاب کنید...
                                                </option>
                                                <option value="44" {{request()->query('type') == 44 ? 'selected' : ''}}>
                                                    فروشگاه
                                                </option>
                                                <option value="45" {{request()->query('type') == 45 ? 'selected' : ''}}>
                                                    ویزیتور
                                                </option>
                                                <option value="43" {{request()->query('type') == 43 ? 'selected' : ''}}>
                                                    عمده فروش
                                                </option>
                                                <option value="42" {{request()->query('type') == 42 ? 'selected' : ''}}>
                                                    شرکت
                                                </option>
                                                <option value="41" {{request()->query('type') == 41 ? 'selected' : ''}}>
                                                    کارخانه
                                                </option>
                                            </select>
                                        </td>
                                        <td>
                                            <div class="input-group">
                                                <input type="text" class="form-control datePicker"
                                                       name="date_from" placeholder="تاریخ از"
                                                       autocomplete="off"
                                                       value="{{request()->query('date_from')}}">
                                                <input type="text" class="form-control datePicker"
                                                       name="date_to" placeholder="تاریخ تا"
                                                       autocomplete="off"
                                                       value="{{request()->query('date_to')}}">
                                            </div>
                                        </td>
                                        <td>
                                            <input type="text" name="phone" class="form-control"
                                                   value="{{ request()->query('phone') }}"
                                                   placeholder="تلفن">
                                        </td>
                                        <td>
                                            <select name="status" class="form-control">
                                                <option
                                                    value="0" {{request()->query('status') == "" ? 'selected' : ''}}>
                                                    انتخاب کنید...
                                                </option>
                                                <option value="1" {{request()->query('status') == 1 ? 'selected' : ''}}>
                                                    فعال
                                                </option>
                                                <option value="2" {{request()->query('status') == 2 ? 'selected' : ''}}>
                                                    غیرفعال
                                                </option>
                                            </select>
                                        </td>
                                        <td>
                                            <button type="submit"
                                                    class="btn-loading btn btn-primary search-ajax">
                                                اعمال فیلتر <i class="ft-thumbs-up position-right"></i>
                                            </button>
                                        </td>
                                    </tr>

                                    @foreach($result as $row)
                                        <?php
                                        if ($row->is_active == 1) {
                                            $status = '<span class="badge text-white badge-success">فعال</span>';
                                        } else {
                                            $status = '<span class="badge text-white badge-danger">غیر فعال</span>';
                                        }
                                        ?>

                                        <tr data-percent="{{withoutZeros($row->porsant_percent)}}"
                                            data-id="{{$row->coll_id}}"
                                            data-user-id="{{$row->user_id}}"
                                            data-name="{{ $row->coll_name }}" class="popup-selectable">
                                            <td>{{index($result,$loop)}}</td>
                                            <td>{{$row->coll_name}}</td>
                                            <td>
                                                @if($row->panel_type==44)
                                                    {{'فروشگاه'}}
                                                @elseif($row->panel_type==45)
                                                    {{'ویزیتور'}}
                                                @elseif($row->panel_type==42)
                                                    {{'شرکت'}}
                                                @elseif($row->panel_type==41)
                                                    {{'کارخانه'}}
                                                @elseif($row->panel_type==46)
                                                    {{'نماینده'}}
                                                @elseif($row->panel_type==43)
                                                    {{'عمده فروش'}}
                                                @elseif($row->panel_type==39)
                                                    {{'ادمین سامانه'}}
                                                @endif
                                            </td>
                                            <td>{{jdate_from_gregorian($row->created_at)}}</td>
                                            <td>{{$row->prsn_phone1}}</td>
                                            <td>{!! $status !!}</td>
                                            <td>
                                                @can('edit_collection')
                                                    @if($row->panel_type!=39)
                                                        <a title="ویرایش"
                                                           href="
                                                  @if($row->panel_type==42)
                                                           {{asset('/company/'.$row->coll_id.'/'.'edit')}}
                                                           @elseif($row->panel_type==43)
                                                           {{asset('/wholesaler/'.$row->coll_id.'/'.'edit')}}
                                                           @elseif($row->panel_type==41)
                                                           {{asset('/factory/'.$row->coll_id.'/'.'edit')}}
                                                           @elseif($row->panel_type==46)
                                                           {{asset('/edit-agent/'.$row->user_id)}}
                                                           @elseif($row->panel_type==44)
                                                           {{asset('/admin/edit-shopkeeper/'.$row->user_id)}}
                                                           @elseif($row->panel_type==45)
                                                           {{asset('/admin/edit-visitor/'.$row->user_id)}}
                                                           @endif
                                                               ">

                                                            <i class="fa fa-pencil-alt"></i>
                                                        </a>
                                                    @endif
                                                @endcan
                                                @if(in_array($row->panel_type,[44,45]))
                                                    @if(can('block_collection') && $row->is_active==0)
                                                        <a title="فعال کردن کاربر"
                                                           data-confirm="اطمینان برای فعال کردن این کاربر دارید؟"
                                                           data-method="post"
                                                           data-action="{{asset('/admin/is-active'.'/'.$row->user_id)}}">
                                                            <i class="fa fa-check"></i>
                                                        </a>
                                                    @elseif(can('unblock_collection') && $row->is_active==1)
                                                        <a title="غیرفعال کاربر"
                                                           data-confirm="اطمینان برای غیرفعال کردن این کاربر دارید؟"
                                                           data-method="post"
                                                           data-action="{{asset('/admin/is-active'.'/'.$row->user_id)}}">
                                                            <i class="fa fa-ban"></i>
                                                        </a>
                                                    @endif
                                                @endif
                                                @if( can('edit_porsant') && $row->panel_type==45)
                                                    <a title="پورسانت" class="commission">
                                                        <i class="fa fa-dollar-sign"></i>
                                                    </a>
                                                @endif
                                                    @if( can('porsant_collection') && in_array($row->panel_type,[41,42,43]))
                                                    <a title="پورسانت" href="{{asset("/admin/porsant-collection/$row->coll_id")}}">
                                                        <i class="fa fa-dollar-sign"></i>
                                                    </a>
                                                @endif
                                                @if(can('area-support') && in_array($row->panel_type,[42,43]))
                                                    <a title="نقاط تحت پوشش"
                                                       href="{{asset('area-support'.'/'.$row->coll_id)}}">
                                                        <i class="fa fa-map-marker"></i>
                                                    </a>
                                                @endif
                                                @if(!in_array($row->panel_type,[44,45]))
                                                    @if(can('block_collection') && $row->coll_hide == 0)
                                                        <a title="غیر فعال کردن کاربر"
                                                           data-confirm="اطمینان از غیر فعال کردن این شرکت دارید؟"
                                                           data-action="{{asset('/admin/block-collection').'/'.$row->coll_id}}"
                                                           data-method="post">
                                                            <i class="fa fa-ban"></i>
                                                        </a>
                                                    @endif
                                                    @if(can('unblock_collection') && $row->coll_hide == 1)
                                                        <a title="فعال کردن کاربر"
                                                           data-confirm="اطمینان از فعال کردن این شرکت دارید؟"
                                                           data-action="{{asset('/admin/unblock-collection').'/'.$row->coll_id}}"
                                                           data-method="post">
                                                            <i class="fa fa-check"></i>
                                                        </a>
                                                    @endif
                                                @endif
                                                @if(can('design_print') && $row->panel_type!=44 && $row->panel_type!=45 && $row->panel_type!=39)
                                                    <a title="طراحی فاکتور"
                                                       href="{{asset('/admin/design-print/'.$row->coll_id)}}">
                                                        <i class="fa fa-print"></i>
                                                    </a>
                                                @endif
                                                {{--@can('change_password_user')--}}
                                                    {{--<a title="تغییر رمزعبور" class="password">--}}
                                                        {{--<i class="fa fa-key"></i>--}}
                                                    {{--</a>--}}
                                                {{--@endcan--}}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {!! $result->appends(request()->query())->render() !!}

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('/js/functions/storeAjax.js') }}"></script>
    <script>
        $(document).ready(function () {

            $(document).on("click", "#register", function () {
                var element = $(this);
                swal({
                    title: 'انتخاب نوع مجموعه',
                    input: 'select',
                    inputOptions: {

                        "1": "عمده فروش",
                        "2": "شرکت",
                        "3": "کارخانه",
                        "4": "نماینده",

                    },
                    confirmButtonText: 'ثبت',
                    cancelButtonText: "انصراف",
                    showCancelButton: true,
                }).then(function (inputValue) {
                    if (inputValue === '1') {
                        window.location.replace("{{asset('/wholesaler/create')}}");
                    }
                    if (inputValue === '2') {
                        window.location.replace("{{asset('/company/create')}}");
                    }
                    if (inputValue === '3') {
                        window.location.replace("{{asset('/factory/create')}}");
                    }
                    if (inputValue === '4') {
                        window.location.replace("{{asset('/agent/register-agent')}}");
                    }

                });

            });

            $(document).on("click", ".commission", function () {
                var element = $(this);
                swal({
                    title: "پورسانت",
                    text: "درصد پورسانت را وارد کنید:",
                    input: "text",
                    showCancelButton: true,
                    animation: "slide-from-top",
                    inputValue: $(element).closest("tr").data("percent"),
                    confirmButtonText: 'ثبت',
                    cancelButtonText: "انصراف",
                }).then(function (inputValue) {
                    if (inputValue === false) return false;
                    var id = $(element).closest("tr").data('user-id');
                    $.post("{{asset('/admin/edit-porsant')}}", {id: id, percent: inputValue}, function (data) {
                        var result = JSON.parse(data);
                        if (result.status == 100) {
                            toastr.success(result.msg, "موفق");
                            setTimeout(function () {
                                location.reload();
                            }, 2000);
                        }
                    });
                }).done();
            });

            {{--$(document).on("click", ".password", function () {--}}
                {{--var element = $(this);--}}
                {{--swal({--}}
                    {{--title: "تغییر رمزعبور پنل کاربر",--}}
                    {{--text: "رمزعبور جدید را وارد کنید:",--}}
                    {{--input: "password",--}}
                    {{--showCancelButton: true,--}}
                    {{--animation: "slide-from-top",--}}
                    {{--confirmButtonText: 'ثبت',--}}
                    {{--cancelButtonText: "انصراف",--}}
                {{--}).then(function (inputValue) {--}}
                    {{--if (inputValue === false) return false;--}}
                    {{--var id = $(element).closest("tr").data('id');--}}
                    {{--$.post("{{asset('/admin/change-password')}}", {id: id, password: inputValue}, function (data) {--}}
                        {{--var result = JSON.parse(data);--}}
                        {{--if (result.status == 100) {--}}
                            {{--toastr.success(result.msg, "موفق");--}}
                            {{--setTimeout(function () {--}}
                            {{--});--}}
                        {{--}--}}
                    {{--});--}}
                {{--}).done();--}}
            {{--});--}}
        });
    </script>
@append
