@extends('master_page')
<?php
$title = "ثبت تراکنش های سامانه";
?>
@section('title_browser',$title)
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <div class="card-header">
                            </div>
                            <div class="card-body">
                                <div class="px-3">
                                    <form method="post"
                                          action=""
                                          enctype="multipart/form-data">
                                        <div class="form-body">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label>نوع درخواست</label>
                                                    <select class="form-control" name="type_request">
                                                        {!! customForeach($type_request,'bas_id','bas_value') !!}
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <label>نوع پرداخت</label>
                                                    <select class="form-control" name="type_payment">
                                                        {!! customForeach($type_payment,'bas_id','bas_value') !!}
                                                    </select>
                                                </div>
                                                <div class="col-md-3 form-group">
                                                    <button type="button"
                                                            class="btn btn-info collection" style="margin-top: 31px; width: 100%">مجموعه
                                                    </button>
                                                    <input type="hidden" name="collection_id"
                                                           value="{{isset($master)? $master->adm_collection_id :''}}">
                                                </div>
                                                <div class="col-md-3">
                                                    <label>مبلغ پرداخت</label>
                                                    <input class="form-control" name="amount">
                                                </div>
                                                <div class="col-md-3">
                                                    <label>تاریخ و ساعت پرداخت</label>
                                                    <div class="input-group">
                                                    <input class="form-control datePicker" name="date">
                                                    <input type="text" name="time" class="form-control"
                                                           value=""
                                                           placeholder="ساعت" chars="time">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <label>شماره پیگیری</label>
                                                    <input class="form-control" name="number_follow">
                                                </div>
                                                <div class="col-md-5">
                                                    <label>توضیحات</label>
                                                    <textarea class="form-control" name="description" rows="2"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions">
                                            <button type="button" class="btn btn-success btn-loading create-transaction">
                                                ثبت
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('/js/functions/storeAjax.js') }}"></script>
    <script>
        $(document).ready(function () {
            $(document).on('click', '.create-transaction', function (e) {
                storeAjax($(this), 'POST', captionButton = "ثبت", progress = false, reloadPage = true, TimeOutActionNextStore = 2000)

            });

            $(document).on('click', '.collection', function () {
                showData('{{asset('/admin/collections-ajax')}}');
                trClick = $(this);
            });
        });
    </script>
@endsection