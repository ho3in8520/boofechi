@extends('master_page')
<?php $title = 'تاریخچه کیف پول'?>
@section('title_browser') {{$title}} @endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-12">
                <h2 class="content-header">{{$title}}</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <form action="" method="get">
                                <div class="row">

                                    <div class="col-md-3">
                                        <label>نوع تراکنش</label>
                                        <select class="form-control" name="transaction_type">
                                            <option value="" {{request()->query('transaction_type') == '' ? 'selected' : ''}}>انتخاب کنید...</option>
                                            <option value="1" {{request()->query('transaction_type') == 1 ? 'selected' : ''}}>خرید فاکتور</option>
                                            <option value="2" {{request()->query('transaction_type') == 2 ? 'selected' : ''}}>شارژ کیف پول</option>
                                            <option value="3" {{request()->query('transaction_type') == 2 ? 'selected' : ''}}>فاکتور های سامانه</option>
                                            <option value="4" {{request()->query('transaction_type') == 3 ? 'selected' : ''}}>کالای مرجوعی</option>
                                            <option value="5" {{request()->query('transaction_type') == 3 ? 'selected' : ''}}>لغو فاکتور</option>
                                            <option value="6" {{request()->query('transaction_type') == 3 ? 'selected' : ''}}>پرداخت پورسانت</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label>نوع پرداخت</label>
                                        <select class="form-control" name="payment_type">
                                            <option value="" {{request()->query('payment_type') == '' ? 'selected' : ''}}>انتخاب کنید...</option>
                                            <option value="1" {{request()->query('payment_type') == 1 ? 'selected' : ''}}>نقدی</option>
                                            <option value="2" {{request()->query('payment_type') == 2 ? 'selected' : ''}}>کیف پول</option>
                                            <option value="3" {{request()->query('payment_type') == 3 ? 'selected' : ''}}>چک</option>
                                            <option value="4" {{request()->query('payment_type') == 4 ? 'selected' : ''}}>آنلاین</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label>تاریخ</label>
                                        <div class="input-group">
                                            <input type="text" name="date_from" placeholder="از" class="form-control datePicker" value="{{request()->query('date_from')}}">
                                            <input type="text" name="date_to" placeholder="تا" class="form-control datePicker" value="{{request()->query('date_to')}}">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <label>مبلغ</label>
                                        <div class="input-group">
                                            <input type="text" name="price_from" placeholder="از" class="form-control" value="{{request()->query('price_from')}}">
                                            <input type="text" name="price_to" placeholder="تا" class="form-control" value="{{request()->query('price_to')}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 30px">

                                    <div class="col-md-4">
                                        <label>توضیحات</label>
                                        <input type="text" name="description" class="form-control"
                                               value="{{request()->query('description')}}">
                                    </div>
                                    <div class="col-md-3">
                                        <label>وضعیت</label>
                                        <select class="form-control" name="status">
                                            <option value="" {{request()->query('status') == '' ? 'selected' : ''}}>انتخاب کنید...</option>
                                            <option value="1" {{request()->query('status') == 1 ? 'selected' : ''}}>افزایشی</option>
                                            <option value="2" {{request()->query('status') == 2 ? 'selected' : ''}}>کاهشی</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <label>کاربر</label>
                                        <input type="text" class="form-control" name="user" value="{{request()->query('user')}}">
                                    </div>
                                    <div class="col-md-2">
                                        <label>کاربر انجام دهنده</label>
                                        <input type="text" class="form-control" name="user_creator" value="{{request()->query('user_creator')}}">
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <div class="text-center">
                                        <button type="button" id="search" class="btn btn-info search-ajax btn-loading">
                                            جستجو
                                        </button>

                                        <strong>تعداد رکورد یافت شده</strong>
                                        (<span class="warning">15</span>)


                                    </div>
                                </div>

                            </form>
                            <table class="table table-striped table-bordered sourced-data dataTable">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>نوع تراکنش</th>
                                    <th>نوع پرداخت</th>
                                    <th>تاریخ</th>
                                    <th>مبلغ</th>
                                    <th>کاربر</th>
                                    <th>کاربر انجام دهنده</th>
                                    <th>توضیحات</th>
                                    <th>وضعیت</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>1</td>
                                    <td>1</td>
                                    <td>1</td>
                                    <td>1</td>
                                    <td>1</td>
                                    <td>1</td>
                                    <td>1</td>
                                    <td>1</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).bind('keypress', function (e) {
            if (e.keyCode == 13) {
                $('#search').trigger('click');
            }
        });

    </script>
@endsection
