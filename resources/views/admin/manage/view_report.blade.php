@extends('master_page')
@section('style')@endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>استان و شهر گزارش دهنده:</label>
                                        <input class="form-control" readonly="readonly"
                                               value="{{$report->state.'/'.$report->city}}">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>نوع پنل گزارش دهنده:</label>
                                        <input class="form-control" readonly="readonly" value="{{$report->bas_value}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>نام گزارش دهنده:</label>
                                        <input class="form-control" readonly="readonly" value="{{$report->prsn_name}}">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>تاریخ و ساعت:</label>
                                        <input class="form-control" readonly="readonly"
                                               value="{{jdate($report->msg_created_at)->format("Y/m/d | H:i:s")}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>نوع پنل متخلف:</label>
                                        <input class="form-control" readonly="readonly" value="{{$report->prsn_name}}">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>نام و کد متخلف:</label>
                                        <input class="form-control" readonly="readonly"
                                               value="{{$report->receiver_name}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>متن گزارش:</label>
                                        <textarea rows="6" class="form-control"
                                                  readonly="readonly">{{$report->msg_message}}</textarea>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>فایل:</label>
                                        <label class="form-control">
                                            @if($report->report_file != '')
                                                <a href="{{getFile($report->report_file)}}" target="_blank">دانلود</a>
                                            @endif
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection