@extends('master_page')
@section('main_content')
    <section id="clear-toasts">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <div class="row mt-1">
                                <div class="col-lg-5 col-md-5 col-sm-12">
                                    <img src="{{getFile($details->avatar,'avatar')}}"
                                         class="rounded-circle" style="height: 200px
                                          ; width: 200px;">
                                </div>
                                <div class="col-lg-6 col-md-4 col-sm-6" style="margin-top: 50px">
                                    <h4>
                                        @if($details->panel_type==44)
                                            فروشگاه : {{$details->coll_name}}
                                        @elseif($details->panel_type==45)
                                            ویزیتور : {{$details->coll_name}}
                                        @elseif($details->panel_type==42)
                                            شرکت : {{$details->coll_name}}
                                        @elseif($details->panel_type==41)
                                            کارخانه : {{$details->coll_name}}
                                        @elseif($details->panel_type==46)
                                            نماینده : {{$details->coll_name}}
                                        @elseif($details->panel_type==43)
                                            عمده فروش : {{$details->coll_name}}
                                        @elseif($details->panel_type==38)
                                            ادمین سامانه
                                        @endif

                                    </h4>
                                    <p class="user-panel-status"><strong>وضعیت پنل : </strong>
                                        @if($details->is_active == 0)
                                            <span class="badge badge-danger text-white">غیر فعال</span>
                                        @else
                                            <span class="badge badge-success text-white">فعال</span>
                                        @endif
                                    </p>
                                </div>
                            </div>
                            <div class="row justify-content-end">
                                @can('change_password_user')
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-primary password"
                                                data-id="{{$id}}">
                                            تغییر رمز کاربر
                                        </button>
                                        @endcan
                                        <button type="button" class="btn btn-info blockUser" data-id="{{$id}}"
                                                data-status="{{$details->is_active}}">بلاک کردن/ باز کردن
                                        </button>
                                        @can('login_panel_user')
                                            <button type="button" class="btn btn-info login-panel"
                                                    data-id="{{$details->user_id}}">ورود به پنل کاربر
                                            </button>
                                        @endcan
                                    </div>


                            </div>
                            <div class="card-block"></div>
                            <div class="col-md-12">
                                <h5>** اطلاعات فردی</h5>
                                <hr>
                                <div class="row">
                                    <div class="col-md-3 form-group">
                                        <label>نام و نام خانوادگی</label>
                                        <input class="form-control" value="{{$details->prsn_name}}" disabled>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label>شماره تماس</label>
                                        <input class="form-control" value="{{$details->prsn_mobile1}}" disabled>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label>استان</label>
                                        <input class="form-control" value="{{$details->state}}" disabled>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label>شهر</label>
                                        <input class="form-control" value="{{$details->city}}" disabled>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label>ایمیل</label>
                                        <input class="form-control" value="{{$details->email}}" disabled>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label>آدرس</label>
                                        <textarea class="form-control" disabled>{{$details->prsn_address}}</textarea>
                                    </div>

                                </div>
                            </div>
                            <div class="card-block"></div>
                            <div class="col-md-12">
                                <h5>** اطلاعات کسب و کار</h5>
                                <hr>
                                <div class="row">
                                    <div class="col-md-3 form-group">
                                        <label>نام مجموعه</label>
                                        <input class="form-control" value="{{$collection->coll_name}}" disabled>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label>کد اقتصادی</label>
                                        <input class="form-control" value="{{$collection->coll_economic_code}}" disabled>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label>شناسه ملی</label>
                                        <input class="form-control" value="{{$collection->coll_national_number}}" disabled>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label>وب سایت</label>
                                        <input class="form-control" value="{{$collection->coll_site_address}}" disabled>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label>کد پستی</label>
                                        <input class="form-control" value="{{$collection->coll_post_code}}" disabled>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label>استان</label>
                                        <input class="form-control" value="{{$collection->state}}" disabled>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label>شهر</label>
                                        <input class="form-control" value="{{$collection->city}}" disabled>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label>آدرس</label>
                                        <textarea class="form-control" disabled>{{$collection->coll_address}}</textarea>
                                    </div>

                                    <div class="col-md-6 form-group">
                                        <label>صنف</label>
                                        <select class="form-control select2" multiple disabled>
                                            @foreach($classes as $row)
                                                <option>{{$row->bas_value}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>
                            </div>
                            <div class="card-block"></div>
                            <div class="col-md-12">
                                <h5>** نقش</h5>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6">
                                        <select class="form-control select2" multiple disabled>
                                            @foreach($role as $row)
                                                <option>{{$row->rol_label}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            @if(findUser($id,'panel_type','id')!=44 && findUser($id,'panel_type','id')!=45)
                            <div class="card-block"></div>
                            <div class="col-md-12">
                                <h5>** نقاط تحت پوشش</h5>
                                <hr>
                                <div class="row">
                                    <div class="col-md-5">
                                        <label>استان</label>
                                        <select class="form-control select2" multiple disabled>
                                            @foreach($area_support_state as $val)
                                                <option>{{$val->c_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-5">
                                        <label>شهر</label>
                                        <select class="form-control select2" multiple disabled>
                                            @foreach($area_support_city as $val)
                                                <option>{{$val->c_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="card-block"></div>
                            <div class="col-md-12">
                                <h5>**دسته بندی ها</h5>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6 form-group">
                                        <select class="form-control select2" multiple disabled>
                                            @foreach($category as $row)
                                                <option>{{$row->cat_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                           @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
