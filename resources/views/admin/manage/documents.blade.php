@extends('master_page')
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <div class="summary">{!! getSummary($files)!!}</div>
                            <table class="table table-striped table-bordered sourced-data dataTable">
                                <thead>
                                <tr>
                                    <th>ردیف</th>
                                    <th>نوع فایل</th>
                                    <th>توضیحات</th>
                                    <th>وضعیت</th>
                                    <th>دانلود</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($files as $row)
                                    <?php
                                    $status = "تایید نشده";
                                    if ($row->ur_status)
                                        $status = "تایید شده";
                                    ?>
                                    <tr data-id="{{$row->ur_id}}">
                                        <td>{{index($files ,$loop)}}</td>
                                        <td>{{$row->bas_value}}</td>
                                        <td>{{$row->ur_description}}</td>
                                        <td class="status_lable">{{$status}}</td>
                                        <td>
                                            <a href="{{getFile($row->ur_path)}}" target="_blank">
                                                دانلود فایل
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {!! $files->appends(request()->query())->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            $(document).on("click", ".acceptForm", function () {
                var element = $(this);
                if (confirm('اطمینان از تایید این مورد دارید؟') == false)
                    return false;
                var id = $(element).closest("tr").data('id');
                $.post("{{asset('admin/files/accept')}}", {id: id}, function (data) {
                    var result = JSON.parse(data);
                    if (result.status == 100) {
                        toastr.success(result.msg, "موفق");
                        var parent = $(element).parent();
                        $(element).remove();
                        parent.closest("tr").find(".status_lable").html("تایید شده");
                        parent.html('<button title="عدم تایید" type="button" class="btn btn-danger btn-sm deacceptForm"><i class="fa fa-remove"></i></button>');
                    }
                });
            });
            $(document).on("click", ".deacceptForm", function () {
                var element = $(this);
                if (confirm('اطمینان از عدم تایید این مورد دارید؟') == false)
                    return false;
                var id = $(element).closest("tr").data('id');
                $.post("{{asset('admin/files/deaccept')}}", {id: id}, function (data) {
                    var result = JSON.parse(data);
                    if (result.status == 100) {
                        toastr.success(result.msg, "موفق");
                        var parent = $(element).parent();
                        $(element).remove();
                        parent.closest("tr").find(".status_lable").html("تایید نشده");
                        parent.html('<button title="تایید" type="button" class="btn btn-success btn-sm acceptForm"><i class="fa fa-check"></i></button>');
                    }
                });
            });
        });
    </script>
@endsection