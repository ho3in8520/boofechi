@extends('master_page')
@section('title_browser') مدیریت فاکتور های سامانه @endsection
@section('main_content')


    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-12">
                <h2 class="content-header">مدیریت فاکتور های سامانه</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <form action="" method="post">
                                <div class="row justify-content-end" style="margin-left: 20px">
                                    <button type="button" class="btn-loading btn btn-info" id="view">
                                        خواندن
                                    </button>
                                </div>
                                <div class="summary">{!! getSummary($result) !!}</div>
                                <table class="table table-striped table-bordered sourced-data dataTable">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>#</th>
                                        <th>نام و نام خانوادگی</th>
                                        <th>نام مجموعه</th>
                                        <th>نوع پنل</th>
                                        <th>تاریخ درخواست</th>
                                        <th>مبلغ</th>
                                        <th>عملیات</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>
                                            <div class="custom-control custom-checkbox m-0">
                                                <input type="checkbox" class="custom-control-input checkAll"
                                                       id="item0">
                                                <label class="custom-control-label" for="item0"></label>
                                            </div>
                                        </td>
                                        <td>-</td>
                                        <td>
                                            <input type="text" name="name" class="form-control"
                                                   value="{{ request()->query('name') }}"
                                                   placeholder="نام و نام خانوادگی">
                                        </td>
                                        <td>
                                            <input type="text" name="coll_name" class="form-control"
                                                   value="{{ request()->query('coll_name') }}"
                                                   placeholder="نام مجموعه">
                                        </td>

                                        <td>
                                            <select name="panel_type" class="form-control">
                                                <option
                                                    value="" {{request()->query('panel_type') == "" ? 'selected' : ''}}>
                                                    انتخاب کنید...
                                                </option>
                                                <option
                                                    value="45" {{request()->query('panel_type') == 45 ? 'selected' : ''}}>
                                                    فروشگاه
                                                </option>
                                                <option
                                                    value="46" {{request()->query('panel_type') == 46 ? 'selected' : ''}}>
                                                    ویزیتور
                                                </option>
                                                <option
                                                    value="43" {{request()->query('panel_type') == 43 ? 'selected' : ''}}>
                                                    عمده فروش
                                                </option>
                                                <option
                                                    value="42" {{request()->query('panel_type') == 42 ? 'selected' : ''}}>
                                                    شرکت
                                                </option>
                                                <option
                                                    value="41" {{request()->query('panel_type') == 41 ? 'selected' : ''}}>
                                                    کارخانه
                                                </option>


                                            </select>
                                        </td>
                                        <td>
                                            <input type="text" name="date" class="form-control datePicker"
                                                   value="{{ request()->query('date') }}"
                                                   placeholder="تاریخ">
                                        </td>
                                        <td>
                                            <input type="text" name="amount" class="form-control"
                                                   value="{{ request()->query('amount') }}"
                                                   placeholder="مبلغ">
                                        </td>
                                        <td>
                                            <button type="submit"
                                                    class="btn-loading btn btn-primary search-ajax">
                                                اعمال فیلتر <i class="ft-thumbs-up position-right"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    @foreach($result as $row)
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox m-0">
                                                    <input type="checkbox" name="view[{{$row->rc_id}}]"
                                                           class="custom-control-input checkbox"
                                                           id="item{{$row->rc_id}}">
                                                    <label class="custom-control-label"
                                                           for="item{{$row->rc_id}}"></label>
                                                </div>

                                            </td>
                                            <td>{{index($result,$loop)}}</td>
                                            <td>{{$row->prsn_name.' '.$row->prsn_family}}</td>
                                            <td>{{$row->coll_name}}</td>
                                            <td>
                                                @php
                                                    switch ($row->panel_type){
                                                        case 44:
                                                            echo "مغازه دار";
                                                            break;
                                                        case 45:
                                                            echo "ویزیتور";
                                                            break;
                                                        case 43:
                                                            echo "عمده فروش";
                                                            break;
                                                        case 42:
                                                            echo "شرکت";
                                                            break;
                                                            case 41:
                                                            echo "کارخانه";
                                                            break;
                                                            case 39:
                                                            echo " ادمین سامانه";
                                                            break;
                                                    }
                                                @endphp
                                            </td>
                                            <td>{{jdate_from_gregorian($row->rc_created_at,'Y/m/d | H:i:s')}}</td>
                                            <td>{{number_format($row->rc_price)}}</td>
                                            <td></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {!! $result->appends(request()->query())->render() !!}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('/js/functions/storeAjax.js') }}"></script>

    <script>
        $(document).ready(function () {

            $(document).on('click', '#view', function (e) {
                storeAjax($(this), 'POST', captionButton = "خواندن", progress = false, reloadPage = true, TimeOutActionNextStore = 2000)

            });

            $(".checkAll").on('change', function () {
                $(".checkbox").prop('checked', $(this).is(":checked"));
            });
        });
    </script>
@append
