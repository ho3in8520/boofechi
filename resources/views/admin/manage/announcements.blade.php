@extends('master_page')
@section('style')
    <style>

        .name-company {
            margin-left: auto;
            margin-right: 150px;
        }

        .bold {
            font-weight: bold;
        }

        a {
            color: #757575;
        }

        a:hover {
            color: #757575;
        }

    </style>

@endsection
@section('main_content')

    <div class="content-wrapper" id="basic-form-layouts">
        <div class="container-fluid"><!--Extended Table starts-->
            <section>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="card-block">
                                    <div class="summary">{!!getSummary($result) !!}</div>
                                    <form action="{{asset('announcement-list')}}" method="get">
                                        <table class="table table-striped table-bordered sourced-data dataTable">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>عنوان</th>
                                                <th>ارسال کننده</th>
                                                <th>تاریخ</th>
                                                <th>عملیات</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>-</td>
                                                <td>
                                                    <select class="form-control state" name="status">
                                                        <option value="" {{ request()->query('status') == "" ? 'selected' : '' }}>
                                                            انتخاب کنید...
                                                        </option>
                                                        <option value="1" {{ request()->query('status') == 1 ? 'selected' : '' }}>
                                                            خوانده شده
                                                        </option>
                                                        <option value="2" {{ request()->query('status') == 2 ? 'selected' : '' }}>
                                                            خوانده نشده
                                                        </option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <input type="text" name="companyName" class="form-control"
                                                           value="{{ request()->query('companyName') }}"
                                                           placeholder="نام شرکت">
                                                </td>
                                                <td>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control datePicker"
                                                               name="no_date_from" placeholder="تاریخ از"
                                                               autocomplete="off"
                                                               value="{{request()->query('no_date_from')}}">
                                                        <input type="text" class="form-control datePicker"
                                                               name="no_date_to" placeholder="تاریخ تا"
                                                               autocomplete="off"
                                                               value="{{request()->query('no_date_to')}}">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button type="submit"
                                                            class="btn-loading btn btn-primary search-ajax">
                                                        اعمال فیلتر <i class="ft-thumbs-up position-right"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            @foreach($result as $value)
                                                <?php
                                                $class = '';

                                                if ($value->is_viewed == 0)
                                                    $class = 'bold'
                                                ?>

                                                <tr>
                                                    <td class="{!! $class !!}">{{index($result ,$loop)}}</td>
                                                    <td class="{!!  $class !!}"><a
                                                                href="{{asset('announcement-form'.'/'.$value->ann_id)}}">{{$value->ann_title}} </a>
                                                    </td>

                                                    <td class="{!! $class !!}"><a
                                                                href="{{asset('announcement-form'.'/'.$value->ann_id)}}">{{$value->coll_name}}</a>
                                                    </td>
                                                    <td class="{!!  $class !!}"><a
                                                                href="{{asset('announcement-form'.'/'.$value->ann_id)}}">{{jdate_from_gregorian($value->created_at,'Y/m/d')}}</a>
                                                    </td>
                                                    <form method="post" action="">
                                                        <td>
                                                            <a title="مشاهده"
                                                               target="_blank" href="{{asset('announcement-form'.'/'.$value->ann_id)}}">
                                                                <i class="fa fa-eye"></i>
                                                            </a>
                                                        </td>
                                                    </form>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                        {!! $result->appends(request()->query())->render() !!}
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection