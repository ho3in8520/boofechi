@extends('master_page')
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <form action="{{asset('admin/user-setting').'/'.$id}}" method="post">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>دریافت کننده فاکتور</label>
                                            <select class="form-control" name="factor_receiver">
                                                {!! customForeach($self_roles,'rol_id','rol_label',$array_setting['factor_receiver']) !!}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>نقش اصلی</label>
                                            <select class="form-control" name="role">
                                                {!! customForeach($roles,'rol_id','rol_label',$default_role_admin_set->rol_id) !!}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <a class="btn btn-success" id="save-user-setting" data-id="{{$id}}">ثبت
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection