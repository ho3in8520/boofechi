@extends('master_page')
@section('title_browser') لیست تبلیغات سامانه @endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-12">
                <h2 class="content-header">لیست تبلیغات</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <form action="" method="get">
                                <div class="summary">{!!getSummary($result) !!}</div>
                                <table class="table table-striped table-bordered sourced-data dataTable">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>نوع مجموعه</th>
                                        <th>تاریخ نمایش</th>
                                        <th>کاربر ثبت کننده</th>
                                        <th>جایگاه</th>
                                        <th>عملیات</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>-</td>
                                        <td>
                                            <input type="text" name="name" class="form-control"
                                                   value="{{ request()->query('name') }}"
                                                   placeholder="نام مجموعه">
                                        </td>
                                        <td>
                                            <div class="input-group">
                                                <input type="text" class="form-control datePicker"
                                                       name="date_from" placeholder="تاریخ از"
                                                       autocomplete="off"
                                                       value="{{ request()->query('date_from') }}">
                                                <input type="text" class="form-control datePicker"
                                                       name="date_to" placeholder="تاریخ تا"
                                                       autocomplete="off"
                                                       value="{{ request()->query('date_to') }}">

                                            </div>
                                        </td>
                                        <td>
                                            <input type="text" name="nameCreator" class="form-control"
                                                   value="{{ request()->query('amount') }}"
                                                   placeholder="نام کاربر">
                                        </td>
                                        <td>
                                            <select name="station" class="form-control">
                                                <option value="" {{request()->query('mode') == "" ? 'selected' : ''}}>
                                                    انتخاب کنید...
                                                </option>
                                                <option value="562" {{request()->query('mode') == 562 ? 'selected' : ''}}>
                                                    سایز بزرگ200*700
                                                </option>
                                                <option value="563" {{request()->query('mode') == 563 ? 'selected' : ''}}>
                                                    سایز بزرگ200*500
                                                </option>


                                            </select>
                                        </td>

                                        <td>
                                            <button type="submit"
                                                    class="btn-loading btn btn-primary search-ajax">
                                                اعمال فیلتر <i class="ft-thumbs-up position-right"></i>
                                            </button>
                                        </td>
                                    </tr>

                                    @foreach($result as $row)
                                        <tr>
                                            <td>{{index($result ,$loop)}}</td>
                                            <td>{{$row->coll_name}}</td>
                                            <td>از ({{jdate_from_gregorian($row->adm_start_at,'Y/d/m')}}) ----
                                                تا({{jdate_from_gregorian($row->adm_expire_at,'Y/d/m')}})
                                            </td>

                                            <td>{{$row->prsn_name}} {{$row->prsn_family}}</td>
                                            <td>{{$row->bas_value}}</td>

                                            <td><a href="{{asset('admin/edit-advertising'.'/'.$row->adm_id)}}"
                                                   title="ویرایش">
                                                    <i class="fa fa-pencil-alt"></i>
                                                </a></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {!! $result->appends(request()->query())->render() !!}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('/js/functions/storeAjax.js') }}"></script>
    <script src="{{asset("/theme/vendors/js/sweetalert2.min.js")}}"></script>

    <script>
        $(document).ready(function () {

            $(document).on('click', '#view', function (e) {
                storeAjax($(this), 'POST', captionButton = "خواندن", progress = false, reloadPage = true, TimeOutActionNextStore = 2000)

            });
            $(document).on("click", ".commission", function () {
                var element = $(this);
                swal({
                    title: "پورسانت",
                    text: "درصد پورسانت را وارد کنید:",
                    input: "text",
                    showCancelButton: true,
                    animation: "slide-from-top",
                    inputValue: $(element).closest("tr").data("percent"),
                    confirmButtonText: 'ثبت',
                    cancelButtonText: "انصراف",
                }).then(function (inputValue) {
                    if (inputValue === false) return false;
                    var id = $(element).closest("tr").data('id');
                    $.post("{{asset('/visitor-list')}}", {id: id, percent: inputValue}, function (data) {
                        var result = JSON.parse(data);
                        if (result.status == 100) {
                            toastr.success(result.msg, "موفق");
                            setTimeout(function () {
                                location.reload();
                            }, 2000);
                        }
                    });
                }).done();
            });
        });
    </script>
@append
