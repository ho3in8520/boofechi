@extends('master_page')
@section('style')@endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                    </div>
                    <div class="card-body">
                        <div class="card-block">
                            <ul class="nav nav-tabs report-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link {{(request()->query('tab','porsant') == 'porsant') ? 'active' : ''}}"
                                       id="home-tab" data-toggle="tab" href="#porsant" role="tab"
                                       aria-controls="home" aria-selected="true">پورسانت پرداختی به ادمین</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link {{(request()->query('tab') == 'requests') ? 'active' : ''}}"
                                       id="profile-tab" data-toggle="tab" href="#requests" role="tab"
                                       aria-controls="profile" aria-selected="false">درخواست ها</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link {{(request()->query('tab') == 'violations') ? 'active' : ''}}"
                                       id="contact-tab" data-toggle="tab" href="#violations" role="tab"
                                       aria-controls="contact" aria-selected="false">تخلفات</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade {{(request()->query('tab','porsant') == 'porsant') ? 'show active' : ''}}"
                                     id="porsant" role="tabpanel"
                                     aria-labelledby="home-tab">sdfsdf
                                </div>
                                <div class="tab-pane fade {{(request()->query('tab') == 'requests') ? 'show active' : ''}}" id="requests" role="tabpanel" aria-labelledby="profile-tab">
                                    fffff
                                </div>
                                <div class="tab-pane fade {{(request()->query('tab') == 'violations') ? 'show active' : ''}}" id="violations" role="tabpanel"
                                     aria-labelledby="contact-tab">
                                    <form action="{{asset('admin/reports').'/'.$id}}" method="get">
                                        <table class="table table-striped table-bordered sourced-data dataTable">
                                            <thead>
                                            <tr>
                                                <th>ردیف</th>
                                                <th>تاریخ</th>
                                                <th>علت</th>
                                                <th>نوع</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <th>-</th>
                                                <th>
                                                    <div class="input-group">
                                                        <input class="form-control datePicker" placeholder="از"
                                                               type="text" name="date_from"
                                                               value="{{request()->query('date_from')}}">
                                                        <input class="form-control datePicker" placeholder="تا"
                                                               type="text" name="date_to"
                                                               value="{{request()->query('date_to')}}">
                                                    </div>
                                                </th>
                                                <th><input class="form-control"
                                                           type="text" name="reason"
                                                           value="{{request()->query('reason')}}"></th>
                                                <th>
                                                    <button class="btn btn-primary btn-loading search-ajax"> اعمال فیلتر
                                                        <i
                                                                class="ft-thumbs-up position-right"></i></button>
                                                </th>
                                            </tr>
                                            @if($reports->count() != 0)
                                                @foreach($reports->items() as $report)
                                                    <tr>
                                                        <td>{{index($reports,$loop)}}</td>
                                                        <td>{{jdate($report->vl_time)->format("Y/m/d H:i:s")}}</td>
                                                        <td>{{$report->vl_reason}}</td>
                                                        <td>
                                                            <span class="badge badge-danger">{{$report->vl_type == 1 ? 'بلاک شده' : 'دریافت اخطار'}}</span>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="8" style="text-align: center;">
                                                        {{ config('first_config.message.empty_table') }}
                                                    </td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                        <input type='hidden' name='tab' value='{{request()->query('tab')}}'>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection