@extends('master_page')
@section('title_browser') {{isset($result)? 'ویرایش تبلیغات' : 'ثبت تبلیغات'}} @endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <div class="card-header">
                            </div>
                            <div class="card-body">
                                <div class="px-3">
                                    <form method="post"
                                          action="{{isset($result)? asset('admin/edit-advertising/'.$id) : asset('admin/create-advertising')}}"
                                          enctype="multipart/form-data">
                                        <div class="form-body">
                                            <div class="form-group row">
                                                <div class="col-md-5">
                                                    <div class="input-group">
                                                        <button type="button"
                                                                class="btn btn-info collection">{{isset($master)? $master->coll_name :'مجموعه'}}
                                                        </button>
                                                        <input type="hidden" name="collection_id"
                                                               value="{{isset($master)? $master->adm_collection_id :''}}">
                                                        <input type="text" class="form-control datePicker"
                                                               name="date_from" placeholder="تاریخ از"
                                                               autocomplete="off" chars="jdate|10"
                                                               value="{{isset($master)? jdate_from_gregorian($master->adm_start_at,'Y/d/m') :''}}">
                                                        <input type="text" class="form-control datePicker"
                                                               name="date_to" placeholder="تاریخ تا"
                                                               autocomplete="off" chars="jdate|10"
                                                               value="{{isset($master)? jdate_from_gregorian($master->adm_expire_at,'Y/d/m') :''}}">
                                                        <input type="text" name="time" class="form-control"
                                                               value="{{isset($master)? jdate_from_gregorian($master->adm_expire_at,'h:i:s') :''}}"
                                                               placeholder="ساعت" chars="time">
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <label>نوع پنل</label>
                                                <div class="col-md-5">
                                                    <select class="form-control select2" name="panel[]" multiple
                                                            style="height: 100px">
                                                        {!! customForeach($panel_type,'bas_id','bas_value',(!empty($panel_type_old)?$panel_type_old:null)) !!}
                                                    </select>
                                                    <input type="hidden" name="panel_type" value="1">
                                                </div>
                                            </div>
                                            <br>
                                            <br>
                                            <div class="row">
                                                <label>صنف</label>
                                                <div class="col-md-5">
                                                    <select class="form-control select2" name="rol[]" multiple
                                                            style="height: 100px">
                                                        {!! customForeach($rol,'bas_id','bas_value',(!empty($senf)?$senf:null)) !!}
                                                    </select>
                                                    <input type="hidden" name="senf" value="1">
                                                </div>
                                            </div>
                                            <br>
                                            <br>
                                            <div class="form-group row">
                                                <label>جایگاه اسلایدر</label>
                                                <div class="col-md-5">
                                                    <select class="form-control" name="station">
                                                        {!! customForeach($advertising,'bas_id','bas_value',(!empty($master->adm_station_id)?$master->adm_station_id:null)) !!}
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label>لینک بنر</label>
                                                <div class="col-md-5">
                                                    <input class="form-control"
                                                           value="{{isset($master)? $master->adm_link:''}}" name="link">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label>توضیحات</label>
                                                <div class="col-md-5">
                                                    <textarea class="form-control" rows="3"
                                                              name="description">{{isset($master) ? $master->adm_description: ''}}</textarea>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                @include('sample.sample_form_area_support')
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-md-9">
                                                    <input type="file" name="image" id="file" style="display: none">
                                                    <label class="btn btn-warning" for="file">انتخاب تصویر</label>
                                                    <br>
                                                    <b id="show-name-upload" style="color: green"></b>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions">
                                            <button type="button" class="btn btn-success btn-loading createAdvertising"
                                                    id="createAdvertising">
                                                ثبت
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('/js/functions/storeAjax.js') }}"></script>
    <script>
        $(document).ready(function () {
            $(document).on('click', '.createAdvertising', function (e) {
                storeAjax($(this), 'POST', captionButton = "ثبت", progress = false, reloadPage = true, TimeOutActionNextStore = 2000)

            });

            $(document).on('click', '.collection', function () {
                showData('{{asset('/admin/collections-ajax')}}');
                trClick = $(this);
                alert(this)
            });
        });
    </script>
@append
