@extends('master_page')
<?php $title = 'جستجوی کاربران'?>
@section('title_browser') {{$title}} @endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-12">
                <h2 class="content-header">{{$title}}</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <form action="{{asset('/admin/search_user_transaction')}}" method="get">
                                <div class="row">

                                    <div class="col-md-4">
                                        <label>نام مجموعه</label>
                                        <input type="text" name="name" class="form-control col-md-10"
                                               value="{{request()->query('name')}}">
                                    </div>
                                    <div class="col-md-4">
                                        <label>کد پستی</label>
                                        <input type="text" name="post" class="form-control col-md-10"
                                               value="{{request()->query('post')}}">
                                    </div>
                                    <div class="col-md-4">
                                        <label>کد ملی</label>
                                        <input type="text" name="nationalCode" class="form-control col-md-10"
                                               value="{{request()->query('nationalCode')}}">
                                    </div>

                                </div>
                                <div class="row" style="margin-top: 30px">

                                    <div class="col-md-4">
                                        <label>شماره موبایل</label>
                                        <input type="text" name="mobile" class="form-control col-md-10"
                                               value="{{request()->query('mobile')}}">
                                    </div>
                                    <div class="col-md-4">
                                        <label>نام کاربری</label>
                                        <input type="text" name="username" class="form-control col-md-10"
                                               value="{{request()->query('username')}}">
                                    </div>
                                    <div class="col-md-4">
                                        <label>نوع پنل</label>
                                        <select name="panel_type" class="form-control col-md-10">
                                            <option
                                                value="0" {{request()->query('panel_type') == "" ? 'selected' : ''}}>
                                                انتخاب کنید...
                                            </option>
                                            <option
                                                value="44" {{request()->query('panel_type') == 44 ? 'selected' : ''}}>
                                                فروشگاه
                                            </option>
                                            <option
                                                value="45" {{request()->query('panel_type') == 45 ? 'selected' : ''}}>
                                                ویزیتور
                                            </option>
                                            <option
                                                value="43" {{request()->query('panel_type') == 43 ? 'selected' : ''}}>
                                                عمده فروش
                                            </option>
                                            <option
                                                value="42" {{request()->query('panel_type') == 42 ? 'selected' : ''}}>
                                                شرکت
                                            </option>
                                            <option
                                                value="41" {{request()->query('panel_type') == 41 ? 'selected' : ''}}>
                                                کارخانه
                                            </option>
                                            <option
                                                value="46" {{request()->query('panel_type') == 46 ? 'selected' : ''}}>
                                                نماینده
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <div class="text-center">
                                        <button type="button" id="search" class="btn btn-info search-ajax btn-loading">
                                            جستجو
                                        </button>

                                        <strong>تعداد کاربران یافت شده</strong>
                                        (<span class="warning">{{ $result->total() }}</span>)


                                    </div>
                                </div>

                            </form>
                            <table class="table table-striped table-bordered sourced-data dataTable">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>نام مجموعه</th>
                                    <th>نوع مجموعه</th>
                                    <th>نام کاربری</th>
                                    <th>کد ملی</th>
                                    <th>موبایل</th>
                                    <th>کد پستی</th>
                                    <th>عملیات</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($result as $row)
                                    <?php
                                    if ($row->is_active == 1) {
                                        $status = '<span class="badge text-white badge-success">فعال</span>';
                                    } else {
                                        $status = '<span class="badge text-white badge-danger">غیر فعال</span>';
                                    }
                                    ?>

                                    <tr data-id="{{$row->user_id}}"
                                         data-name="{{ $row->username }}"
                                        class="popup-selectable">
                                        <td>{{index($result,$loop)}}</td>
                                        <td>{{$row->coll_name}}</td>
                                        <td>
                                            @if($row->panel_type==44)
                                                {{'فروشگاه'}}
                                            @elseif($row->panel_type==45)
                                                {{'ویزیتور'}}
                                            @elseif($row->panel_type==42)
                                                {{'شرکت'}}
                                            @elseif($row->panel_type==41)
                                                {{'کارخانه'}}
                                            @elseif($row->panel_type==46)
                                                {{'نماینده'}}
                                            @elseif($row->panel_type==43)
                                                {{'عمده فروش'}}
                                            @elseif($row->panel_type==39)
                                                {{'ادمین سامانه'}}
                                            @endif
                                        </td>
                                        <td>{{$row->username}}</td>
                                        <td>{{$row->prsn_national_code}}</td>
                                        <td>{{$row->prsn_mobile1}}</td>
                                        <td>{{$row->prsn_post_code}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {!! $result->appends(request()->query())->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

