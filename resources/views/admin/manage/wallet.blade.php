@extends('master_page')
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <div class="col-sm-12">
                                <form method="post" action="">
                                    <div class="row">
                                        <h4>موجودی کیف پول : <b
                                                style="color: green">{{number_format(withoutZeros($find_amount->amount))}}</b></h4>
                                        <div class="col-md-12">
                                            <div class="pull-left">
                                                <button type="button" data-id="{{$id}}" class="btn btn-info wallet">
                                                    افزایش | کاهش
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="row">
                                <div class="col-md-12 table-responsive">
                                    <table class="table table-striped table-bordered sourced-data dataTable">
                                        <thead>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>افزایش اعتبار توسط</th>
                                            <th>تاریخ</th>
                                            <th>وضعیت</th>
                                            <th>مبلغ</th>
                                            <th>توضیحات</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(!count($result)==0)
                                            @foreach($result as $row)
                                                <tr>
                                                    <td>{{index($result,$loop)}}</td>
                                                    <td>{{$row->prsn_name.' '.$row->prsn_family.' ('.$row->username.')'}}</td>
                                                    <td>{{jdate_from_gregorian($row->bt_time,'Y/d/m H:i:s')}}</td>
                                                    <td><span class="badge text-white bg-{{($row->bt_status == 1) ? 'green' : 'red'}}">{{($row->bt_status == 1) ? 'افزایش' : 'کسر'}}</span></td>
                                                    <td>{{number_format(withoutZeros($row->bt_amount))}}</td>
                                                    <td>{{$row->bt_description}}</td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="7" style="text-align: center;">
                                                    {{ config('first_config.message.empty_table') }}
                                                </td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                    {!! $result->appends(request()->query())->render() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
