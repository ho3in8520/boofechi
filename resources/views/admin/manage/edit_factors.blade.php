@extends('master_page')
<?php
$title = "ویرایش فاکتور سامانه";
?>
@section('title_browser',$title)
@section('main_content')
    <section id="basic-form-layouts" class="basket-items">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header"><?=$title?></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="container">
                            <table class="table">
                                <tr>
                                    <th colspan="5"><h4 class="pull-right">مشخصات خریدار</h4>
                                    </th>
                                </tr>
                                <tr>
                                    <td>نام شرکت:</td>
                                    <td>{{$collection->prsn_job}}</td>
                                    <td>تلفن:</td>
                                    <td>{{$collection->prsn_phone1}}</td>
                                    <td rowspan="2" style="width: 100px;height: 100px">
                                        <img class="rounded-circle"
                                             style="width: 100px;height: 100px"
                                             src="{{getFile(($img) ? $img->ur_path: '')}}">
                                    </td>
                                </tr>
                                <tr>
                                    <td>آدرس:</td>
                                    <td colspan="3">{{$collection->coll_address}}</td>
                                </tr>
                            </table>
                        </div>
                        <p class="mb-0"></p>
                    </div>
                    <form method="post" action="">
                        @csrf
                        <div class="card-body">
                            <div class="card-block">
                                <table style="text-align: center !important"
                                       class="table table-striped table-inverse table-bordered table-hover">
                                    <thead>
                                    <th>ردیف</th>
                                    <th>عنوان</th>
                                    <th>قیمت</th>
                                    <th>قیمت کل</th>
                                    <th>عملیات</th>
                                    </thead>
                                    <tbody>
                                    @php
                                        $sum = 0;
                                        $i = 0;
                                    @endphp
                                    @foreach($detail as $row)
                                        @php
                                            $sum += $row->sfd_request_amount;
                                        @endphp
                                    <tr>
                                        <td>1</td>
                                        <td>
                                            <input type="text" class="form-control"
                                                   name="title[{{$i}}]" value="{{$row->sfd_request_title}}">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control price amount"
                                                   name="price[{{$i}}]" value="{{WithoutZeros($row->sfd_request_amount)}}">
                                        </td>
                                        <td>
                                            <label class="from-control total">{{number_format(WithoutZeros($row->sfd_request_amount))}}</label>
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-danger deleteForm">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                            <button type="button" class="btn btn-primary addForm">
                                                <i class="fa fa-plus"></i>
                                            </button>
                                        </td>
                                    </tr>
                                        <?php
                                            $i++;
                                        ?>
                                    @endforeach
                                    <tr class="">
                                        <td colspan="2"></td>
                                        <td>جمع کل</td>
                                        <td id="totalAmount"></td>
                                        <td></td>
                                    </tr>
                                    <tr class="success">
                                        <td colspan="2"></td>
                                        <td>تخفیف</td>
                                        <td><input class="form-control price discount" value="{{WithoutZeros($find_user->sfm_discount)}}" name="dicount"></td>
                                        <td></td>
                                    </tr>
                                    <tr class="success">
                                        <td colspan="2"></td>
                                        <td>قابل پرداخت</td>
                                        <td id="payable"></td>
                                        <td></td>
                                    </tr>
                                    </tbody>
                                </table>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <textarea name="description" class="form-control"
                                                  placeholder="توضیحات را اینجا بنویسید">{{$find_user->sfm_description}}</textarea>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 20px">
                                    <div class="col-sm-12">
                                        <div class="pull-left">
                                            <button type="button" class="btn btn-success"
                                                    id="btnSaveFactor">ثبت
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{asset('/js/functions/storeAjax.js')}}"></script>
    <script>
        counter = {{$i - 1}};
        $(document).on("keyup", ".amount", function () {
            var elem = $(this);
            var price = $(elem).val();
            // var sumAmounts = 0;
            calcPayable();
            // $("#totalAmount").html(numberFormat(sumAmounts));
            $(elem).closest("tr").find('.total').html(numberFormat(price));
            alert(sumAmounts)
        });

        $(document).on("keyup", ".discount", function () {
            calcPayable();
        });


        function calcPayable() {
            var sumAmounts = 0;
            $(".amount").each(function (key, value) {
                if (parseInt($(this).val()) > 0)
                    sumAmounts += parseInt($(this).val());
            });
            var discount = "0";
            if (parseInt($(".discount").val()) > 0)
                discount = $(".discount").val();

            $("#payable").html(numberFormat(sumAmounts - discount));
            $("#totalAmount").html(numberFormat(sumAmounts));
        }

        $('.price').keypress(function (e) {
            if (isNaN(this.value + "" + String.fromCharCode(e.charCode))) return false;
        })
            .on("cut copy paste", function (e) {
                e.preventDefault();
            });

        $(document).on('click', '.addForm', function () {
            var formHtml = $(this).closest('tr').clone();
            formHtml.counter('');
            formHtml.find(".total").html("0");
            $(this).closest('tr').after("<tr>" + formHtml.html() + "</tr>");
        });

        $(document).on('click', '.deleteForm', function () {
            var elementClick = $(this);
            if (confirm('آیا از حذف فرم اطمینان دارید ؟'))
                if ($(elementClick).closest('tr'))
                    $(elementClick).closest('tr').remove();
                else
                    alert('حذف امکان پذیر نمی باشد');
        });

        $(document).on('click', '#btnSaveFactor', function (e) {
            var retVal = confirm("آیا مطمئن هستین برای ثبت");
            if (retVal == true) {
                storeAjax($(this), 'POST', captionButton = "ثبت", progress = false, reloadPage = false, TimeOutActionNextStore = 2000)
                return true;
            } else {
                return false;
            }
        });
    </script>
@append