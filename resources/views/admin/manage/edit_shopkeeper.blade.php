<?php
$states = getAreaSupport(1);
$cities = getAreaSupport(2, null, null, request('state', null));
$states_coll = getAreaSupport(1);
$cities_coll = getAreaSupport(2, null, null, request('state_coll', null));
$title='ویرایش خورده فروش'
?>
@extends('master_page')
@section('title_browser') {{$title}}@endsection
@section('style')
    <style>
        .formDiscount {
            display: none;
        }

        #deleteImage {
            position: relative;
        }

        #deleteImageUpload {
            font-size: 35px;
            margin-left: 10px;
            z-index: 1;
            cursor: pointer;
            position: absolute;
        }
    </style>
@endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header">{{$title}}</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title-wrap bar-success">

                        </div>
                        <p class="mb-0"></p>
                    </div>
                    <div class="card-body">
                        <div class="card-block">
                            <form
                                action=''
                                method='POST' enctype="multipart/form-data">
                                @include("sample.sample_edit_visitor_shopkeeper")
                                <div class="row">
                                    <div class="card-block"></div>
                                    <div class='col-md-12'>
                                        <div class='form-group'>
                                            <label></label>
                                            <button type="button" id="update" class='btn btn-success btn-loading'>
                                                ثبت
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('/js/functions/storeAjax.js') }}"></script>
    <script>

        $(document).ready(function () {

            $(document).on('click', '#update', function (e) {
                storeAjax($(this), 'POST', captionButton = "ثبت", progress = false, reloadPage = true, TimeOutActionNextStore = 2000)

            });
        });
    </script>
@append
