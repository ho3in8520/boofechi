<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>
        طراحی فاکتور | {{$collection->coll_name}}
    </title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <script src="{{asset('/js/editor/codeFlask.min.js')}}"></script>
    <style>
        .test {
            background: #eee;
        }
        pre {
            background-color: transparent !important;
            border: none !important;
        }
        .codeflask{
            width: 80% !important;
        }
        .btn-success{font-family:tahoma;color:#fff;background-color:#28a745;border-color:#28a745}.btn-success:hover{color:#fff;background-color:#218838;border-color:#1e7e34}
        .btn{display:inline-block;font-weight:400;color:#212529;text-align:center;vertical-align:middle;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;background-color:transparent;border:1px solid transparent;padding:.375rem .75rem;font-size:1rem;line-height:1.5;border-radius:.25rem;transition:color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out}@media (prefers-reduced-motion:reduce){.btn{transition:none}}.btn:hover{color:#212529;text-decoration:none}
    </style>
</head>
<body>
<form method="post">
    @csrf
    <div class="editor">{!! $factor_html !!}</div>
    <div style="display: block;margin: auto;width: 20%;float: right;direction: rtl;position: fixed;right: 0">
        <div style="width: 100%;text-align: center">
            <h4 style="font-family: Tahoma">کلید های قابل استفاده</h4>
        </div>
        <div style="width: 100%">
            <ul style="color: red;font-family: tahoma">
                <li>%seller_corporate% : نام شرکت</li>
                <li>%seller_tel% : تلفن شرکت</li>
                <li>%seller_logo% : لوگو</li>
                <li>%seller_address% : آدرس شرکت</li>
                <li>%buyer_corporate% : نام خریدار</li>
                <li>%buyer_tel% : تلفن خریدار</li>
                <li>%buyer_address% : آدرس خریدار</li>
                <li>%factor_number% : شماره فاکتور</li>
                <li>%time% : زمان خرید</li>
                <li>%description% : توضیحات فاکتور</li>
                <li>%products% : محصولات</li>
                <li>%discount_percent% : درصد تخفیف فاکتور</li>
                <li>%discount_amount% : مبلغ تخفیف فاکتور</li>
                <li>%payable% : مبلغ قابل پرداخت</li>
                <li>%payable_string% : مبلغ قابل پرداخت به حروف</li>
                <li>%payment_method_percent% : درصد تخفیف روش پرداخت</li>
                <li>%payment_method_discount_amount% : مبلغ تخفیف روش پرداخت</li>
                <li>%payed_amount% : مبلغ پرداخت شده</li>
                <li>%returns% : مرجوعی ها</li>
            </ul>
        </div>
        <button type="submit" class="btn-success" style="width: 100%;text-align: center;">ثبت فرم</button>
    </div>
</form>
<script>
    const flask = new CodeFlask('.editor', {
        language: 'html',
        lineNumbers: true,
        areaId: 'thing1',
        areaName: 'print_html',
        ariaLabelledby: 'header1',
        handleTabs: true
    });

    window['flask'] = flask;
</script>
</body>
</html>
