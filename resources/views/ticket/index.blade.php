@extends('master_page')
<?php
$title = 'تیکت ها';
if (\Request::route()->getName() == 'my-tickets')
    $title = 'تیکت های من'
?>
@section('title_browser'){{$title}}@endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header"><?=$title?></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                    </div>
                    <div class="card-body">
                        <div class="card-block">
                            @if(can('create_ticket') && \Request::route()->getName() == 'my-tickets')
                                <div class="col-md-12">
                                    <div class="pull-left">
                                        <a href="{{route('ticket.create')}}" class="btn btn-success">ارسال تیکت جدید</a>
                                    </div>
                                </div>
                            @endif
                            <div class="col-md-12 table-responsive">
                                <form method="GET" action="{{asset('/ticket')}}">
                                    <div class="summary">{!!getSummary($tickets) !!}</div>
                                    <table class="table table-striped table-bordered sourced-data dataTable">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>شماره تیکت</th>
                                            <th>عنوان</th>
                                            <th>بخش</th>
                                            <th>تاریخ</th>
                                            <th>آخرین وضعیت</th>
                                            <th>عملیات</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>#</td>
                                            <td><input class="form-control" type="text" name="ticket_no"
                                                       value="{{request()->query('ticket_no')}}"></td>
                                            <td><input class="form-control" type="text" name="ticket_subject"
                                                       value="{{request()->query('ticket_subject')}}"></td>
                                            <td><select class="form-control" name="ticket_department">
                                                    {!! customForeach($departments,'rol_id','rol_label',request()->query('ticket_department')) !!}
                                                </select></td>
                                            <td>-</td>
                                            <td>-</td>
                                            <td>
                                                <button type="submit" class="btn-loading btn btn-primary search-ajax">
                                                    اعمال فیلتر
                                                    <i class="ft-thumbs-up position-right"></i>
                                                </button>

                                            </td>
                                        </tr>

                                        @foreach($tickets as $ticket)
                                            <tr>
                                                <td>{{index($tickets,$loop)}}</td>
                                                <td>{{$ticket->t_id}}</td>
                                                <td>{{$ticket->t_subject}}</td>
                                                <td>{{$ticket->rol_label}}</td>
                                                <td>{{jdate_from_gregorian($ticket->t_created_at)}}</td>
                                                <td>
                                                        <span class="badge text-white"
                                                              style="background-color: {{$statuses[$ticket->t_status]['color']}}">{{$statuses[$ticket->t_status]['value']}}</span>
                                                </td>
                                                <td>
                                                    @can('view_ticket')
                                                    <?php
                                                    if (\Request::route()->getName() == 'my-tickets')
                                                    {
                                                    ?>
                                                    <a title="مشاهده"
                                                       href="{{asset('show-ticket').'/'.$ticket->t_id}}">
                                                        <i class="fa fa-eye"></i>
                                                    </a>
                                                    <?php
                                                    }
                                                    else{
                                                    ?>
                                                    <a title="مشاهده"
                                                       href="{{route('ticket.show',[$ticket->t_id])}}">
                                                        <i class="fa fa-eye"></i>
                                                    </a>
                                                    <?php
                                                    }
                                                    ?>
                                                        @endcan
                                                        @if(user('panel_type')==38 || user('panel_type')==40)
                                                            @can('create_factor')
                                                                <a title="صدور فاکتور"
                                                                   href="{{asset('admin/system-factor'.'/'.$ticket->t_id)}}">
                                                                    <i class="fa fa-file"></i>
                                                                </a>
                                                            @endcan
                                                        @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    {!! $tickets->appends(request()->query())->render() !!}
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection