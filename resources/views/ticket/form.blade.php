@extends('master_page')
<?php $title = 'ارسال تیکت'?>
@section('title_browser'){{$title}}@endsection
@section('main_content')
    <section id="horizontal-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header">{{$title}}</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                    </div>
                    <div class="card-body">
                        <div class="px-3">
                            <form method="post" action="{{route('ticket.store')}}" enctype="multipart/form-data">
                                <div class="form-body">
                                    <div class="form-group row">
                                        <div class="col-md-3">
                                            <select id="projectinput6" name="ticket[section]" class="form-control">
                                                {!! customForeach($departments,'rol_id','rol_label') !!}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <input type="text" id="projectinput5" class="form-control" name="ticket[title]"
                                                   placeholder="عنوان">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-9">
                                            <textarea id="projectinput9" rows="5" class="form-control"
                                                      placeholder="متن تیکت"
                                                      name="ticket[text]"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-9">
                                                <input type="file" name="file" id="file" style="display: none">
                                            <label class="btn btn-warning" for="file">آپلود فایل</label>
                                            <br>
                                            <b id="show-name-upload" style="color: green"></b>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <button type="button" class="btn btn-success btn-loading" id="createTicket">
                                         ارسال تیکت
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('/js/functions/storeAjax.js') }}"></script>
    <script>
    $(document).ready(function(){
            $('input[type="file"]').change(function(e){
                var fileName = e.target.files[0].name;
                $('#show-name-upload').html(fileName)
            });
        $(document).on('click', '#createTicket', function (e) {
            storeAjax($(this), 'POST')

        });
        });
    </script>

    @endsection