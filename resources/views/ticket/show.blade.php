@extends('master_page')
<?php

$title = 'مشاهده تیکت شماره ' . $id;
?>
@section('title_browser'){{$title}}@endsection
@section('main_content')
    <section id="horizontal-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header">{{$title}}</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                    </div>
                    <div class="card-body">
                        <div class="px-3">
                            @foreach($result as $answer)
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <?php
                                            $name = (($ticket->t_creator_user_id == $answer->t_creator_user_id) ? "<b style='font-weight: bold;color:blue'>" . "مشتری" .' '.$answer->prsn_name. "</b>" : "<b style='font-weight: bold;color:red'>" .'پشتیبان'.' '. $answer->prsn_name . "</b>");
                                            ?>
                                            <label>{!! $name." (".jdate_from_gregorian($answer->t_created_at) !!})
                                                @if($answer->t_file!= null)
                                                    <a href="{{getFile($answer->t_file)}}" class="btn btn-info btn-sm" style="margin-top: 10px" >فایل ضمیمه</a>
                                                @endif
                                            </label>
                                            <textarea class="form-control" readonly
                                                      rows="6">{{$answer->t_description}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <div class="row">
                                @if(can('answer_ticket') && ($ticket->t_creator_user_id == user('user_id') or $ticket->t_status != 4))
                                    <div class="col-sm-6">
                                        <h3>پاسخ به تیکت:</h3>
                                        <form method="post" action="{{asset('ticket/answer').'/'.$id}}"
                                              enctype="multipart/form-data">
                                            <div class="form-body">
                                                <div class="form-group row">
                                                    <div class="col-md-12">
                                            <textarea id="projectinput9" rows="5" class="form-control"
                                                      placeholder="پاسخ"
                                                      name="ticket[text]"></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-9">
                                                        <input type="file" name="file" id="file" style="display: none">
                                                        <label class="btn btn-warning" for="file">آپلود فایل</label>
                                                        <br>
                                                        <b id="show-name-upload" style="color: green"></b>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-actions">
                                                <button type="button" class="btn btn-success btn-loading"
                                                        id="createTicket">
                                                    ارسال تیکت
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                @endif
                                @if(can('referral_ticket') && ($ticket->t_creator_user_id == user('user_id') or $ticket->t_status != 4))
                                    <div class="col-sm-6">
                                        <h3>ارجاع تیکت:</h3>
                                        <form method="post" action="{{asset('ticket/referral').'/'.$id}}"
                                              enctype="multipart/form-data">
                                            <div class="form-body">
                                                <div class="form-group row">
                                                    <div class="col-md-6">
                                                        <select id="projectinput6" name="ticket[section]"
                                                                class="form-control">
                                                            {!! customForeach($departments,'rol_id','rol_label') !!}
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-actions">
                                                <button type="button" class="btn btn-primary btn-loading"
                                                        id="referTicket">
                                                    ارجاع تیکت
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('/js/functions/storeAjax.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('input[type="file"]').change(function (e) {
                var fileName = e.target.files[0].name;
                $('#show-name-upload').html(fileName)
            });
            $(document).on('click', '#createTicket', function (e) {
                storeAjax($(this), 'POST', "ثبت", false, 1);

            });
            $(document).on('click', '#referTicket', function (e) {
                storeAjax($(this), 'POST', "ارجاع", false, 0);

            });
        });
    </script>

@endsection
