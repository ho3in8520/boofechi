@extends('master_page')
<?php $title = 'تسویه پورسانت'?>
@section('title_browser'){{$title}}@endsection
@section('main_content')
    <section id="horizontal-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header">{{$title}}</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                    </div>
                    <div class="card-body">
                        <div class="px-3">
                            <div class="card-body">
                                <div class="card-block">
                                    <div class="card-text">
                                        <blockquote class="blockquote border-left-primary border-left-3 pl-2">
                                            <p><b>توضیحات ادمین</b></p>
                                            <strong style="color: red">کمتر از 100 هزار تومان واریز نمی گردد</strong>
                                        </blockquote>
                                    </div>
                                </div>
                            </div>
                            <form method="post" action="" enctype="multipart/form-data">
                                <div class="form-body">
                                    <div class="form-group row">
                                        <div class="col-md-3">
                                            <p><strong>شماره کارت : </strong>0000-0000-0000-0000</p>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <strong style="color: green"><strong style="color: #757575">مبلغ کل پورسانت : </strong>100,000,000</strong>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label>مبلغ تسویه :</label>
                                        <div class="col-md-2">
                                            <input class="form-control number" onkeyup="javascript:this.value=number_format(this.value);" name="amount" type="text">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <button type="button" class="btn btn-success btn-loading" id="create_poursantCleanse">
                                        ثبت
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('/js/functions/storeAjax.js') }}"></script>
    <script>
        $(document).ready(function(){
            $(document).on('click', '#create_poursantCleanse', function (e) {
                storeAjax($(this), 'POST', captionButton = "ثبت", progress = false, reloadPage = false, TimeOutActionNextStore = 2000)

            });
        });
    </script>

@endsection