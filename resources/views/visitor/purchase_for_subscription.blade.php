@extends('master_page')
<?php $title = 'خرید برای مغازه دار'?>
@section('title_browser'){{$title}}@endsection
@section('main_content')
    <section id="horizontal-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header">{{$title}}</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <form method="post" action="" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label>کد مغازه دار:</label>
                                        <div class="input-group">
                                            <input class="form-control" name="code" id="code">
                                            <a class="btn btn-primary selectShopkeeper">
                                                <i class="fa fa-search"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div>
                                    <button type="button" class="btn btn-success btn-loading" id="send">ارسال کد
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('/js/functions/storeAjax.js') }}"></script>
    <script>
        function customeFunctionAfterSelectRow(elementClick) {
            $("#code").val($(elementClick).data("code"));
        }
        $(document).ready(function () {
            $(".selectShopkeeper").click(function () {
                showData('{{asset('/subscription')}}');
            });

            $(document).on("click", "#send", function () {
                var code = $("#code").val();
                if (confirm('اطمینان از ارسال کد تایید برای کد کاربری ' + code + " دارید؟") == false)
                    return false
                else
                    storeAjax($(this), 'POST','ثبت',0,false);
            })
        });

        $(document).bind('keypress', function(e) {
            if(e.keyCode==13){
                $('#send').trigger('click');
            }
        });

    </script>
@endsection
