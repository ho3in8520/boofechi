@extends('master_page')
<?php $title = 'ورود کد تایید'?>
@section('title_browser'){{$title}}@endsection
@section('main_content')
    <section id="horizontal-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header">{{$title}}</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <form method="post" action="" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label>کد تایید:</label>
                                        <input class="form-control" name="otp" id="code">
                                    </div>
                                </div>
                                <br>
                                <div>
                                    <button type="button" class="btn btn-success btn-loading" id="check">بررسی کد
                                    </button>
                                    <a class="btn btn-primary btn-loading"  href="{{asset('/purchase-for-subscription')}}">برگشت</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('/js/functions/storeAjax.js') }}"></script>
    <script>
        function customeFunctionAfterSelectRow(elementClick) {
            $("#code").val($(elementClick).data("code"));
        }
        $(document).ready(function () {
            $(".selectShopkeeper").click(function () {
                showData('{{asset('/subscription')}}');
            });

            $(document).on("click", "#check", function () {
                var code = $("#code").val();
                storeAjax($(this), 'POST', 'ثبت', 0, false);
            })
        });
    </script>
@endsection