@extends('master_page')
<?php $title = 'پورسانت ها'?>
@section('title_browser'){{$title}}@endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header">{{$title}}</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <form method="post" action="">
                                <div class="summary">{!!getSummary($porsant) !!}</div>
                                <table class="table table-striped table-bordered sourced-data dataTable text-center">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>نام مغازه دار یا کد</th>
                                        <th>شماره فاکتور</th>
                                        <th>تاریخ فاکتور</th>
                                        <th>مبلغ فاکتور</th>
                                        <th>پورسانت دریافتی</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>#</td>
                                        <td><input type="text" name="name" class="form-control"
                                                   value="{{request()->query('name')}}"></td>
                                        <td>
                                            <input type="text" name="factor_number" class="form-control"
                                                   value="{{request()->query('factor_number')}}">
                                        </td>
                                        <td>
                                            <div class="input-group">
                                                <input type="text" class="form-control datePicker"
                                                       name="date_from" placeholder="تاریخ از"
                                                       autocomplete="off" value="{{request()->query('date_from')}}">
                                                <input type="text" class="form-control datePicker"
                                                       name="date_to" placeholder="تاریخ تا"
                                                       autocomplete="off" value="{{request()->query('date_to')}}">
                                            </div>
                                        </td>
                                        <td></td>
                                        <td>
                                            <button type="submit" class="btn-loading btn btn-primary search-ajax">
                                                اعمال فیلتر
                                                <i class="ft-thumbs-up position-right"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    @foreach($porsant as $row)
                                        <tr>
                                            <td>{{index($porsant,$loop)}}</td>
                                            <td>{{$row->name}}</td>
                                            <td>{{$row->fm_no}}</td>
                                            <td>{{jdate($row->fm_created_at)->format("Y/m/d")}}</td>
                                            <td>{{number_format($row->fm_amount)}}</td>
                                            <td>{{number_format($row->porsant)}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {!! $porsant->appends(request()->query())->render() !!}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection