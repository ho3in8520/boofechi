@extends('master_page')
@section('title_browser') لیست ویزیتور ها @endsection
@section('style')
    <link rel="stylesheet" type="text/css" href="{{asset("/theme/vendors/css/sweetalert2.min.css")}}">

    <style>

        .name-company {
            margin-left: auto;
            margin-right: 150px;
        }

        .bold {
            font-weight: bold;
        }

        a {
            color: #757575;
        }

        a:hover {
            color: #757575;
        }

    </style>

@endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-12">
                <h2 class="content-header">لیست ویزیتور ها</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <form action="" method="get">
                                <table class="table table-striped table-bordered sourced-data dataTable">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>نام ویزیتور</th>
                                        <th>تاریخ عضویت</th>
                                        <th>تعداد زیرمجموعه</th>
                                        <th>درصد پورسانت</th>
                                        <th>عملیات</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>-</td>
                                        <td>
                                            <input type="text" name="nameVisitor" class="form-control"
                                                   value="{{ request()->query('nameVisitor') }}"
                                                   placeholder="نام ویزیتور">
                                        </td>
                                        <td>
                                            <div class="input-group">
                                                <input type="text" class="form-control datePicker"
                                                       name="date_from" placeholder="تاریخ از"
                                                       autocomplete="off"
                                                       value="{{request()->query('date_from')}}">
                                                <input type="text" class="form-control datePicker"
                                                       name="date_to" placeholder="تاریخ تا"
                                                       autocomplete="off"
                                                       value="{{request()->query('date_to')}}">
                                            </div>
                                        </td>
                                        <td>
                                            <input type="text" name="subset" class="form-control"
                                                   value="{{ request()->query('subset') }}"
                                                   placeholder="تعداد زیر مجموعه">
                                        </td>
                                        <td>
                                            <div class="input-group">
                                                <input type="text" class="form-control"
                                                       name="porsant_from" placeholder="پورسانت از"
                                                       autocomplete="off"
                                                       value="{{request()->query('porsant_from')}}">
                                                <input type="text" class="form-control"
                                                       name="porsant_to" placeholder="پورسانت تا"
                                                       autocomplete="off"
                                                       value="{{request()->query('porsant_to')}}">
                                            </div>
                                        </td>
                                        <td>
                                            <button type="submit"
                                                    class="btn-loading btn btn-primary search-ajax">
                                                اعمال فیلتر <i class="ft-thumbs-up position-right"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    @foreach($result as $value)
                                        <tr data-percent="{{withoutZeros($value->porsant_percent)}}"
                                            data-id="{{$value->user_id}}">
                                            <td>{{index($result ,$loop)}}</td>
                                            <td>{{$value->prsn_name}}</td>
                                            <td>{{jdate_from_gregorian($value->created_at,"Y/m/d")}}</td>
                                            <td>{{$value->sub_users}}</td>
                                            <td>{{withoutZeros($value->porsant_percent)}}</td>
                                            <td><a type="button" title="ویرایش درصد پورسانت"
                                                   class="btn btn-info commission btn-sm">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {!! $result->appends(request()->query())->render() !!}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('/js/functions/storeAjax.js') }}"></script>
    <script src="{{asset("/theme/vendors/js/sweetalert2.min.js")}}"></script>

    <script>
        $(document).ready(function () {

            $(document).on('click', '#view', function (e) {
                storeAjax($(this), 'POST', captionButton = "خواندن", progress = false, reloadPage = true, TimeOutActionNextStore = 2000)

            });
            $(document).on("click", ".commission", function () {
                var element = $(this);
                swal({
                    title: "پورسانت",
                    text: "درصد پورسانت را وارد کنید:",
                    input: "text",
                    showCancelButton: true,
                    animation: "slide-from-top",
                    inputValue: $(element).closest("tr").data("percent"),
                    confirmButtonText: 'ثبت',
                    cancelButtonText: "انصراف",
                }).then(function (inputValue) {
                    if (inputValue === false) return false;
                    var id = $(element).closest("tr").data('id');
                    $.post("{{asset('/visitor-list')}}", {id: id, percent: inputValue}, function (data) {
                        var result = JSON.parse(data);
                        if (result.status == 100) {
                            toastr.success(result.msg, "موفق");
                            setTimeout(function () {
                                location.reload();
                            }, 2000);
                        }
                    });
                }).done();
            });
        });
    </script>
@append