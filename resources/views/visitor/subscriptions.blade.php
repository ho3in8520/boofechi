@extends('master_page')
<?php $title = 'زیر مجموعه'?>
@section('title_browser'){{$title}}@endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header">{{$title}}</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <form method="post" action="{{asset('subscription')}}">
                                <table class="table table-striped table-bordered sourced-data dataTable text-center">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>نام مغازه دار یا کد</th>
                                        <th>استان/شهر</th>
                                        <th>تاریخ عضویت</th>
                                        <th>عملیات</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>#</td>
                                        <td><input type="text" name="name" class="form-control"
                                                   value="{{request()->query('name')}}"></td>
                                        <td>
                                            <div class="input-group">
                                                <select class="form-control state" name="state">
                                                    {!! customForeach($states,'c_id','c_name',request()->query('state')) !!}
                                                </select>
                                                <select class="form-control city" name="city">
                                                    {!! customForeach($cities,'c_id','c_name',request()->query('city')) !!}
                                                </select>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="input-group">
                                                <input type="text" class="form-control datePicker"
                                                       name="date_from" placeholder="تاریخ از"
                                                       autocomplete="off" value="{{request()->query('date_from')}}">
                                                <input type="text" class="form-control datePicker"
                                                       name="date_to" placeholder="تاریخ تا"
                                                       autocomplete="off" value="{{request()->query('date_to')}}">
                                            </div>
                                        </td>
                                        <td>
                                            <button type="submit" class="btn-loading btn btn-primary search-ajax">
                                                اعمال فیلتر
                                                <i class="ft-thumbs-up position-right"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    @if($subscription->count())
                                        @foreach($subscription as $row)
                                            <tr data-code="{{$row->code}}" class="popup-selectable">
                                                <td>{{index($subscription,$loop)}}</td>
                                                <td>{{$row->name}}</td>
                                                <td>{{$row->state.'/'.$row->city}}</td>
                                                <td>{{jdate_from_gregorian($row->created_at,"Y/m/d")}}</td>
                                                <td>-</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="5">
                                                {{ config('first_config.message.empty_table') }}
                                            </td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection