@extends('master_page')
@section('title_browser') پیام ها @endsection
@section('style')
    <style>

        .name-company {
            margin-left: auto;
            margin-right: 150px;
        }

        .bold {
            font-weight: bold;
        }

        a {
            color: #757575;
        }

        a:hover {
            color: #757575;
        }

    </style>

@endsection
@section('main_content')

    <div class="content-wrapper" id="basic-form-layouts">
        <div class="container-fluid"><!--Extended Table starts-->
            <div class="row">
                <div class="col-12">
                    <h2 class="content-header">پیام ها</h2>
                </div>
            </div>
            <section>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="card-block">
                                    <div class="summary">{!!getSummary($result) !!}</div>
                                    <form action="" method="get">
                                        <div class="row justify-content-end" style="margin-left: 20px">
                                            <button type="button"
                                                    class="btn-loading btn btn-info" id="view">
                                                خواندن
                                            </button>
                                        </div>
                                        <table class="table table-striped table-bordered sourced-data dataTable">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>#</th>
                                                <th>عنوان</th>
                                                <th>ارسال کننده</th>
                                                <th>تاریخ</th>
                                                <th>اقدامات</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>-</td>
                                                <td>
                                                    <div class="custom-control custom-checkbox m-0">
                                                        <input type="checkbox" class="custom-control-input checkAll"
                                                               id="item0">
                                                        <label class="custom-control-label" for="item0"></label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <select class="form-control state" name="status">
                                                        <option value="" {{ request()->query('status') == "" ? 'selected' : '' }}>
                                                            انتخاب کنید...
                                                        </option>
                                                        <option value="2" {{ request()->query('status') == 2 ? 'selected' : '' }}>
                                                            خوانده شده
                                                        </option>
                                                        <option value="1" {{ request()->query('status') == 1 ? 'selected' : '' }}>
                                                            خوانده نشده
                                                        </option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <input type="text" name="senderName" class="form-control"
                                                           value="{{ request()->query('senderName') }}"
                                                           placeholder="نام ارسال کننده">
                                                </td>
                                                <td>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control datePicker"
                                                               name="date_from" placeholder="تاریخ از"
                                                               autocomplete="off"
                                                               value="{{request()->query('date_from')}}">
                                                        <input type="text" class="form-control datePicker"
                                                               name="date_to" placeholder="تاریخ تا"
                                                               autocomplete="off"
                                                               value="{{request()->query('date_to')}}">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button type="submit"
                                                            class="btn-loading btn btn-primary search-ajax">
                                                        اعمال فیلتر <i class="ft-thumbs-up position-right"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            @foreach($result as $value)
                                                <?php
                                                $class = '';

                                                if ($value->msg_is_read == 0)
                                                    $class = 'bold'
                                                ?>

                                                <tr>
                                                    <td class="{!! $class !!}">{{index($result ,$loop)}}</td>
                                                    <td>
                                                        @if($value->msg_is_read == 0)
                                                            <div class="custom-control custom-checkbox m-0">
                                                                <input type="checkbox" name="view[{{$value->msg_id}}]"
                                                                       class="custom-control-input checkbox"
                                                                       id="item{{$value->msg_id}}">
                                                                <label class="custom-control-label"
                                                                       for="item{{$value->msg_id}}"></label>
                                                            </div>
                                                        @else
                                                            -
                                                        @endif
                                                    </td>

                                                    <td class="{!!  $class !!}"><a
                                                                href="{{asset('view-massage'.'/'.$value->msg_id)}}">{{$value->msg_title}} </a>
                                                    </td>

                                                    <td class="{!! $class !!}"><a
                                                                href="{{asset('view-massage'.'/'.$value->msg_id)}}">{{$value->coll_name}}</a>
                                                    </td>
                                                    <td class="{!!  $class !!}"><a
                                                                href="{{asset('view-massage'.'/'.$value->msg_id)}}">{{jdate_from_gregorian($value->msg_created_at,'Y/m/d')}}</a>
                                                    </td>
                                                    <form method="post" action="{{route('list-massage')}}">
                                                        <td>
                                                            <a title="مشاهده"
                                                               href="{{asset('view-massage'.'/'.$value->msg_id)}}">
                                                                <i class="fa fa-eye"></i>
                                                            </a>
                                                        </td>
                                                    </form>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                        {!! $result->appends(request()->query())->render() !!}
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>


@endsection
@section('script')
    <script src="{{ asset('/js/functions/storeAjax.js') }}"></script>
    <script>
        $(document).ready(function () {

            $(".checkAll").on('change', function () {
                $(".checkbox").prop('checked', $(this).is(":checked"));
            });

            $(document).on('click', '#view', function (e) {
                storeAjax($(this), 'POST', captionButton = "خواندن", progress = false, reloadPage = true, TimeOutActionNextStore = 2000)

            });
        });
    </script>
@append