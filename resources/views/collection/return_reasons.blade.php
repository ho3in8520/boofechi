@extends('master_page')
<?php $title = 'علت های مرجوعی'?>
@section('title_browser',$title)
@section('main_content')
    <section class="basic-elements">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header">{{$title}}</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="px-3">
                            <form class="form" method="post" action="">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="card-header">
                                        </div>
                                        <div class="col-12 table-responsive">
                                            <div class="return">
                                                <table
                                                    class="table table-striped table-bordered sourced-data dataTable">
                                                    <thead>
                                                    <tr>
                                                        <th>ردیف</th>
                                                        <th>علت های مرجوعی</th>
                                                        <th>افزودن به موجودی انبار</th>
                                                        <th>فعال/غیر فعال</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                    $i=0;
                                                    ?>
                                                    @foreach($return_reasons as $key=>$return_reason)



                                                        <tr class="text-center">
                                                            <td>{{$loop->index + 1}}</td>
                                                            <td>{{$return_reason['bas_value']}}</td>
                                                            <td>
                                                                <div class="custom-control custom-checkbox m-0">
                                                                    <input type="checkbox" class="custom-control-input" id="return{{$i}}"
                                                                       name="is_return[{{$return_reason['bas_id']}}]"
                                                                       value="{{$return_reason['bas_id']}}"
                                                                <?php
                                                                    if (isset($old_reasons[$key])) {
                                                                        if ($old_reasons[$key]['crr_bas_id'] == $return_reason['bas_id']) {
                                                                            if (isset($old_reasons[$key]) && $old_reasons[$key]['crr_return_to_inventory'] == true)
                                                                                echo "checked";
                                                                            else {
                                                                                if (!isset($old_reasons[$key]) || $old_reasons[$key]['crr_status'] != true)
                                                                                    echo "disabled";
                                                                            }
                                                                        }
                                                                    } else
                                                                        echo "disabled";
                                                                    ?>>
                                                                    <label class="custom-control-label" for="return{{$i}}"></label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="custom-control custom-checkbox m-0">
                                                                    <input type="checkbox" class="custom-control-input active" id="active{{$i}}"
                                                                           name="active[{{$return_reason['bas_id']}}]"
                                                                       value="{{$return_reason['bas_id']}}"
                                                            {{--style="display: none;"--}}
                                                               <?php
                                                                if (isset($old_reasons[$key])) {
                                                                    if ($old_reasons[$key]['crr_bas_id'] == $return_reason['bas_id'] && $old_reasons[$key]['crr_status'] == true)
                                                                        echo "checked";
                                                                }
                                                                ?>
                                                                >
                                                                    <label class="custom-control-label" for="active{{$i}}"></label>
                                                                </div>
                                                            </td>

                                                        </tr>
                                                        <?php
                                                        $i++
                                                        ?>
                                                    @endforeach

                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button id="updateReturn_reason"
                                                            class="btn btn-success btn-loading" type="button">
                                                        ثبت
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
@section('script')
    <script src="{{ asset('/js/functions/storeAjax.js') }}"></script>
    <script>

        $(document).ready(function () {
            $("#updateReturn_reason").click(function (e) {
                e.preventDefault();
                storeAjax($(this), 'POST', 'ثبت', false, 1);
            });


            $(document).on('change', '.active', function () {
                var is_return = $(this).closest('tr').find("input[name^='is_return']");
                var active = $(this).closest('tr').find("input[name^='active']")
                if (!active.prop("checked")) {
                    is_return.attr("checked", false);
                    is_return.attr("disabled", true);
                } else {
                    is_return.removeAttr("disabled");
                }


            });


        });
    </script>
@endsection
