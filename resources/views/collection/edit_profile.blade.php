@extends('master_page')
<?php $title = 'ویرایش پروفایل' ?>
@section('title_browser') {{$title}} @endsection
@section('style')
    <style>
        .formDiscount {
            display: none;
        }

        #deleteImage {
            position: relative;

        }

        #deleteImageUpload {
            font-size: 35px;
            margin-left: 10px;
            z-index: 1;
            cursor: pointer;
            position: absolute;
            margin-top: 20px;
        }

        .line-end {
            border-top: 1px solid #D1D5EA;
            padding: 10px 0;
            margin-top: 20px;
        }

    </style>
@endsection
@section('main_content')
    <section class="basic-elements">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header">{{$title}}</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="px-3">
                            <form class="form" method="post" action="" enctype="multipart/form-data" id="userform"
                                  name="userform">
                                {{ csrf_field() }}
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <div id="deleteImage">
                                                </div>
                                                <img title="آپلود عکس" style="cursor:pointer; margin-top: 20px"
                                                     id="blah"
                                                     src="@if(getAvatar())
                                                     {{ getFile(getAvatar()) }}
                                                     @else
                                                     {{ asset('/images/image.jpg') }}
                                                     @endif
                                                         "
                                                     alt="your image" width="150px" height="150px">
                                                <input accept="image/jpeg" name="avatar" type="file"
                                                       id="imgInp" style="display: none">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4 col-12">
                                            <div class="form-group">
                                                <label for="text">نام</label>
                                                <input type="text" class="form-control" name="edit_profile[name]"
                                                       value="{{$result->prsn_name}}"
                                                       disabled="disabled">
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="form-group">
                                                <label for="text">نام خانوادگی</label>
                                                <input type="text" class="form-control" name="edit_profile[family]"
                                                       value="{{$result->prsn_family}}"
                                                       disabled="disabled">
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="form-group">
                                                <label for="text">کدملی</label>
                                                <input type="text" class="form-control"
                                                       value="{{$result->prsn_national_code}}"
                                                       name="edit-profile[national-code]" disabled="disabled">
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="form-group">
                                                <label for="text">تلفن همراه</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="edit_profile[mobail]"
                                                           value="{{$result->prsn_mobile1}}"
                                                           disabled="disabled">
                                                    <button type="button" class="btn btn-warning editPassword">ویرایش
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="form-group">
                                                <label for="text">ایمیل</label>
                                                <div class="input-group">
                                                    <input type="email" class="form-control" name="edit_profile[email]"
                                                           value="{{$result->email}}"
                                                           disabled="disabled">
                                                    <button type="button" class="btn btn-warning editEmail">ویرایش
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="form-group">
                                                <label for="text">رمزعبور فعلی</label>
                                                <input type="text" class="form-control" name="old_password">
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="form-group">
                                                <label for="text">رمزعبور جدید</label>
                                                <input type="password" class="form-control password" id="password"
                                                       name="password">
                                                <div class="message"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="form-group">
                                                <label for="text">تایید رمزعبور جدید</label>
                                                <input type="password" class="form-control password" id="verifyPassword"
                                                       name="verifyPassword">
                                                <div class="message"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="form-group">
                                                <label for="text">شماره شبا</label>
                                                <input type="text" class="form-control" name="sheba"
                                                       value="{{(isset($sheba))?$sheba:''}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <div class="text-right">
                                            <button type="button" id="make_change"
                                                    class="btn btn-success btn-loading">ثبت
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('/js/functions/storeAjax.js') }}"></script>
    <script>
        $(document).ready(function () {
            function previewImage(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#blah').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#imgInp").change(function () {
                previewImage(this);
                if ($(this).val() != "")
                    $("#deleteImage").html("<i id='deleteImageUpload' title=\"حذف عکس\" class=\"fa fa-trash text-danger\"></i>")
            });

            $(document).on('click', '#blah', function () {
                $("#imgInp").click();
            });
            $(document).on('click', '#deleteImageUpload', function () {
                $("#imgInp").val('');
                $("#blah").attr('src', "@if($avatar){{ getFile($avatar->ur_path) }}@else {{ asset('/images/image.jpg') }} @endif");
                $("#deleteImage").html("");
            });


            $(document).on("click", ".editPassword", function () {
                var element = $(this);
                swal({
                    title: 'تغییر شماره همراه؟',
                    text: "اطمینان از تغییر شماره همراه خود دارید؟",
                    icon: 'warning',
                    animation: "slide-from-top",
                    showCancelButton: true,
                    confirmButtonText: 'ثبت',
                    cancelButtonText: "انصراف",
                }).then((result) => {
                    if (result) {
                        $.post("{{asset('/change-mobile')}}", {type: 1}, function (data) {
                                var result = JSON.parse(data);
                                if (result.status == 100) {
                                    toastr.success(result.msg, "موفق");
                                    swal({
                                        title: "کد تاییدیه",
                                        text: "کد فعال سازی به شماره موبایل فعلی شما ارسال گردید لطفا کد را داخل کادر زیر وارد نمایید.:",
                                        input: "text",
                                        showCancelButton: true,
                                        animation: "slide-from-top",
                                        confirmButtonText: 'ثبت',
                                        cancelButtonText: "انصراف",
                                    }).then(
                                        function (inputValue) {
                                            if (inputValue === false) return false;
                                            $.post("{{asset('/change-mobile')}}", {
                                                code: inputValue,
                                                type: 2
                                            }, function (data) {
                                                var result = JSON.parse(data);
                                                if (result.status == 100) {
                                                    toastr.success(result.msg, "موفق");
                                                    if (result) {
                                                        swal({
                                                            title: "شماره همراه جدید",
                                                            text: "لطفا شماره همراه جدید خود را وارد کنید:",
                                                            input: "text",
                                                            showCancelButton: true,
                                                            animation: "slide-from-top",
                                                            confirmButtonText: 'ثبت',
                                                            cancelButtonText: "انصراف",
                                                        }).then(
                                                            function (inputValue) {
                                                                if (inputValue === false) return false;
                                                                $.post("{{asset('/change-mobile')}}", {
                                                                    mobileNew: inputValue,
                                                                    type: 3
                                                                }, function (data) {
                                                                    var result = JSON.parse(data);
                                                                    if (result.status == 100) {
                                                                        toastr.success(result.msg, "موفق");
                                                                        swal({
                                                                            title: "کد تاییدیه",
                                                                            text: "کد فعال سازی به شماره موبایل جدید شما ارسال گردید لطفل کد را داخل کادر زیر وارد نمایید.",
                                                                            input: "text",
                                                                            showCancelButton: true,
                                                                            animation: "slide-from-top",
                                                                            confirmButtonText: 'ثبت',
                                                                            cancelButtonText: "انصراف",
                                                                        }).then(
                                                                            function (inputValue) {
                                                                                if (inputValue === false) return false;
                                                                                $.post("{{asset('/change-mobile')}}", {
                                                                                        codeNew: inputValue,
                                                                                        type: 4
                                                                                    }, function (data) {
                                                                                        var result = JSON.parse(data);
                                                                                        if (result.status == 100) {
                                                                                            toastr.success(result.msg, "موفق");
                                                                                        } else {
                                                                                            toastr.error(result.msg, "ناموفق");
                                                                                        }
                                                                                    }
                                                                                )
                                                                            })
                                                                    } else {
                                                                        toastr.error(result.msg, "ناموفق");
                                                                    }
                                                                }).done();
                                                            })
                                                    }
                                                } else {
                                                    if (result.status == 300 || result.status == 500) {
                                                        toastr.error(result.msg, "ناموفق");
                                                    }
                                                }
                                            }).done();
                                        });
                                } else {
                                    toastr.error(result.msg, "ناموفق");
                                }
                            }
                        ).done();
                    }
                    ;
                });
            });

            $(document).on("click", ".editEmail", function () {
                var element = $(this);
                swal({
                    title: 'تغییر ایمیل؟',
                    text: "اطمینان از تغییر ایمیل خود دارید؟",
                    icon: 'warning',
                    animation: "slide-from-top",
                    showCancelButton: true,
                    confirmButtonText: 'ثبت',
                    cancelButtonText: "انصراف",
                }).then((result) => {
                    if (result) {
                        $.post("{{asset('/change-email')}}", {type: 1}, function (data) {
                                var result = JSON.parse(data);
                                if (result.status == 100) {
                                    toastr.success(result.msg, "موفق");
                                    swal({
                                        title: "کد تاییدیه",
                                        text: "کد فعال سازی به ایمیل فعلی شما ارسال گردید لطفا کد را داخل کادر زیر وارد نمایید.:",
                                        input: "text",
                                        showCancelButton: true,
                                        animation: "slide-from-top",
                                        confirmButtonText: 'ثبت',
                                        cancelButtonText: "انصراف",
                                    }).then(
                                        function (inputValue) {
                                            if (inputValue === false) return false;
                                            $.post("{{asset('/change-email')}}", {
                                                code: inputValue,
                                                type: 2
                                            }, function (data) {
                                                var result = JSON.parse(data);
                                                if (result.status == 100) {
                                                    toastr.success(result.msg, "موفق");
                                                    if (result) {
                                                        swal({
                                                            title: "ایمیل جدید",
                                                            text: "لطفا ایمیل جدید خود را وارد کنید:",
                                                            input: "text",
                                                            showCancelButton: true,
                                                            animation: "slide-from-top",
                                                            confirmButtonText: 'ثبت',
                                                            cancelButtonText: "انصراف",
                                                        }).then(
                                                            function (inputValue) {
                                                                if (inputValue === false) return false;
                                                                $.post("{{asset('/change-email')}}", {
                                                                    emailNew: inputValue,
                                                                    type: 3
                                                                }, function (data) {
                                                                    var result = JSON.parse(data);
                                                                    if (result.status == 100) {
                                                                        toastr.success(result.msg, "موفق");
                                                                        swal({
                                                                            title: "کد تاییدیه",
                                                                            text: "کد فعال سازی به ایمیل جدید شما ارسال گردید لطفل کد را داخل کادر زیر وارد نمایید.",
                                                                            input: "text",
                                                                            showCancelButton: true,
                                                                            animation: "slide-from-top",
                                                                            confirmButtonText: 'ثبت',
                                                                            cancelButtonText: "انصراف",
                                                                        }).then(
                                                                            function (inputValue) {
                                                                                if (inputValue === false) return false;
                                                                                $.post("{{asset('/change-email')}}", {
                                                                                        codeNew: inputValue,
                                                                                        type: 4
                                                                                    }, function (data) {
                                                                                        var result = JSON.parse(data);
                                                                                        if (result.status == 100) {
                                                                                            toastr.success(result.msg, "موفق");
                                                                                        } else {
                                                                                            toastr.error(result.msg, "ناموفق");
                                                                                        }
                                                                                    }
                                                                                )
                                                                            })
                                                                    } else {
                                                                        toastr.error(result.msg, "ناموفق");
                                                                    }
                                                                }).done();
                                                            })
                                                    }
                                                } else {
                                                    if (result.status == 300 || result.status == 500) {
                                                        toastr.error(result.msg, "ناموفق");
                                                    }
                                                }
                                            }).done();
                                        });
                                } else {
                                    toastr.error(result.msg, "ناموفق");
                                }
                            }
                        ).done();
                    }
                    ;
                });
            });
            $(document).on("click", ".editEmadil", function () {
                var element = $(this);
                swal({
                    title: "ویرایش ایمیل",
                    text: "ایمیل  خود را وارد کنید:",
                    input: "text",
                    showCancelButton: true,
                    animation: "slide-from-top",
                    confirmButtonText: 'ثبت',
                    cancelButtonText: "انصراف",
                }).then(
                    function (inputValue) {
                        if (inputValue === false) return false;
                        $.post("{{asset('/change-email')}}", {email: inputValue, type: 1}, function (data) {
                            var result = JSON.parse(data);
                            if (result.status == 100) {
                                toastr.success(result.msg, "موفق");
                            } else if (result.status == 500) {
                                toastr.error(result.msg, "خطا");
                            }
                        });
                    }).done();
            });
        });
        $(document).on('click', '#make_change', function (e) {
            var password = $('#password').val();
            var verifyPassword = $('#verifyPassword').val();
            if (password == verifyPassword) {
                storeAjax($(this), 'POST', captionButton = "ثبت", progress = false, reloadPage = true, TimeOutActionNextStore = 2000)

            } else {
                $('.message').html("<div class='help-block text-danger'>رمزعبور با یکدیگر مطابقت ندارد</div>");
                $('.password').css({'border-color': '#d2322d'});

            }
        });


    </script>
@endsection

