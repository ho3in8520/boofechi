@extends('master_page')
@section('title_browser') ثبت نام مغازه دار @endsection
@section('main_content')

    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header">اطلاعات پایه</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title-wrap bar-success">

                        </div>
                        <p class="mb-0"></p>
                    </div>
                    <div class="card-body">
                        <div class="card-block">
                            <form method="post" action="{{asset('authentication')}}">

                                @include("sample.sample_register_shopkeeper")

                                @if(!request()->ajax())
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label></label>
                                                <button class="btn btn-success btn-loading" id="RegisterShopkeeper">
                                                    ثبت
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('/js/functions/storeAjax.js') }}"></script>
    <script>

        $(document).ready(function () {
            $("#RegisterShopkeeper").click(function (e) {
                e.preventDefault();
                storeAjax($(this), 'POST', 'ثبت', false, 1);
            });
        });
    </script>
@endsection
