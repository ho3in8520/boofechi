@extends('master_page')
@section('title_browser') {{ (empty($category)) ? 'نقاط تحت پوشش' : 'ویرایش نقاط تحت پوشش' }} @endsection
<?php
$states = editCompanyAreaSupport(1);
$cities = editCompanyAreaSupport(2, null, null, request('state'));

?>
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header">{{ (empty($category)) ? 'نقاط تحت پوشش' : 'ویرایش نقاط تحت پوشش' }}</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <form action='' method='POST'>
                                {{ csrf_field() }}
                                {!! (!empty($category)) ? "<input name='_method' type='hidden' value='PATCH'>" : ''  !!}
                                <div class='row'>
                                    <div class='col-md-3'>
                                        <div class='form-group'>
                                            <label>استان</label>
                                            <select name="state" class="form-control state">
                                                {!!customForeach($states, "c_id", "c_name",request()->query('state')) !!}
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-3'>
                                        <div class='form-group'>
                                            <label> شهر <a title="برای انتخاب چندتایی کلید ctrl را نگه دارید!"><i
                                                            class="fa fa-question-circle"></i></a></label>
                                            <select name="city" class="form-control city" multiple="multiple">
                                                {!!customForeach($cities, "c_id", "c_name",request()->query('city'))!!}
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-1'>
                                        <div class='form-group'>
                                            <a id="addToSelectedList" class="btn btn-success"
                                               style="margin-top: 30px">افزودن
                                            </a>
                                            <a id="removeToSelectedList" class="btn btn-danger"> حذف 
                                            </a>
                                        </div>
                                    </div>
                                    <div class='col-md-4'>
                                        <div class='form-group'>
                                            <label>شهرهای انتخاب شده</label>
                                            <textarea id="selected_cities" class="form-control" rows="4"
                                                      readonly="readonly"></textarea>
                                            <input type="hidden" name="selected_cities" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class='row'>
                                    <div class='col-md-12'>
                                        <div class='form-group'>
                                            @if(isset($url))
                                                <a href="{{ asset($url) }}" class="btn btn-danger">بازگشت</a>
                                            @endif
                                            <button type="button" id="saveAreaSupports"
                                                    class='btn btn-success btn-loading'>ثبت
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{asset('js/functions/storeAjax.js')}}"></script>
    <script>
        $(document).ready(function () {
            $("#saveAreaSupports").click(function (e) {
                e.preventDefault();
                storeAjax($(this), 'POST', captionButton = "ثبت", progress = false, reloadPage = true, TimeOutActionNextStore = 2000)
            });
        });
        $(document).ready(function () {
            var array_vals = <?=$temp?>;
            concatAreasSupport(array_vals);
            $(document).on("change", ".state", function () {
                setTimeout(function () {
                    if (typeof array_vals[$(".state option:selected").text()] !== 'undefined') {
                        $.each(array_vals[$(".state option:selected").text()]['ids'], function (key, value) {
                            $('.city option[value=' + value + ']').attr('selected', true);
                        })
                    }
                }, 600);
            });
            function concatAreasSupport(arr) {
                ret_arr = {};
                var text = "";
                var ids = "";
                $.each(arr, function (key, value) {
                    text += key + "=>";
                    $.each(value['ids'], function (k, v) {
                        ids += v + ",";
                    });
                    $.each(value['text'], function (k, v) {
                        text += v + ",";
                    });
                    text = text.replace(/,+$/, '') + "\n";
                });
                ids = ids.replace(/,+$/, '') + "\n";
                $("textarea#selected_cities").val(text);
                $("input[name=selected_cities]").val(ids);
            }

            $("#addToSelectedList").click(function () {
                if ($(".state option:selected").val()) {
                    array_vals[$(".state option:selected").text()] = [];
                    var array_temp_val = [];
                    var array_temp_txt = [];
                    $(".city option:selected").each(function () {
                        if (jQuery.inArray($(this).val(), array_vals[$(".state option:selected").text()]) === -1) {
                            if ($(this).val()) {
                                array_temp_val.push($(this).val());
                                array_temp_txt.push($(this).text());
                            }
                        }
                    });
                    if (array_temp_txt != "") {
                        array_vals[$(".state option:selected").text()]['ids'] = array_temp_val;
                        array_vals[$(".state option:selected").text()]['text'] = array_temp_txt;
                    }
                    else
                        delete array_vals[$(".state option:selected").text()];
                    concatAreasSupport(array_vals);
                }
            });
            $("#removeToSelectedList").click(function () {
                array_vals={};
                $("#selected_cities").val('');
                $("input[name='selected_cities']").val('');
            });
        });
    </script>
@append
