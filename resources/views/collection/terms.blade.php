@extends('master_page')
@section('title_browser') قوانین سامانه @endsection
@section('style')
    <style>
        .box {
            width: 1000px;
            height: 400px;
            background: #F0F0F0;
            overflow-y: scroll;
            padding: 10px;
        }
        .scroll-example {
            overflow: auto;
            scrollbar-width: none; /* Firefox */
            -ms-overflow-style: none; /* IE 10+ */
        }

        .scroll-example::-webkit-scrollbar {
            width: 0px;
            background: transparent; /* Chrome/Safari/Webkit */
        }

    </style>
@endsection
@section('main_content')

    <section class="basic-elements">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                    </div>
                    <div class="card-body">
                        <div class="px-3">
                            <form class="form" method="post" action="">
                                <div class="form-body">
                                    <div class="row col-md-12">
                                        <div class="box scroll-example">
                                            {!! $result->pt_terms !!}
                                        </div>
                                    </div>
                                    <div class="form-group " style="margin-top: 20px">
                                        <div class="row justify-content-center">
                                            <div class="col-md-1">

                                                <button id="createRules"
                                                        class="btn btn-success" type="button" disabled>
                                                    ثبت
                                                </button>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="custom-control custom-checkbox m-0">
                                                    <input type="checkbox" class="custom-control-input" id="item1">
                                                    <label class="custom-control-label" for="item1">قوانین را با دقت
                                                        مطالعه کردم و با آن موافق هستم</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('/js/functions/storeAjax.js') }}"></script>
    <script>
        $(document).ready(function () {
            $(document).on('click', '#createRules', function (e) {
                storeAjax($(this), 'POST', captionButton = "ثبت", progress = false, reloadPage = false, TimeOutActionNextStore = 2000)

            });
            $("#item1").click(function () {
                $("#createRules").attr("disabled", !this.checked);
            });
        });
    </script>
@endsection
