@extends('master_page')
<?php $title = 'گزارش فاکتور ها'?>
@section('title_browser'){{$title}}@endsection
@section('main_content')
    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <h2 class="content-header">{{$title}}</h2>
                </div>
            </div>
            <section id="extended">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="card-block">
                                    <form method="post" action="">
                                        <div class="row">
                                            <div class="col-md-3 ml-md-auto">
                                                <div class="form-group">
                                                    <input type="text" name="companyName" class="form-control"
                                                           placeholder="نام شرکت">
                                                </div>
                                            </div>
                                            <div class="col-md-3 ml-md-auto">
                                                <div class="form-group">
                                                    <input type="text" name="productName" class="form-control"
                                                           placeholder="نام کالا">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <input type="text" class="form-control datePicker"
                                                           name="in_date_from[]" placeholder="تاریخ از"
                                                           autocomplete="off">
                                                    <input type="text" class="form-control datePicker"
                                                           name="in_date_to[]" placeholder="تاریخ تا"
                                                           autocomplete="off">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 ml-md-auto">
                                                <div class="form-group">
                                                    <input type="text" name="amount" class="form-control"
                                                           placeholder="مبلغ">
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group mt-1">
                                                    <label for="switcherySize13"
                                                           class="font-medium-2 text-bold-600 mr-1">تایید شده</label>
                                                    <input type="checkbox" id="switcherySize13" name="accepted"
                                                           class="switchery" data-size="xs" data-color="warning"
                                                           data-switchery="true" style="display: none;">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group mt-1">
                                                    <label for="switcherySize13"
                                                           class="font-medium-2 text-bold-600 mr-1">بازگشت از
                                                        شرکت</label>
                                                    <input type="checkbox" id="switcherySize13" name="recursive"
                                                           class="switchery" data-size="xs" data-color="warning"
                                                           data-switchery="true" style="display: none;">
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group mt-1">
                                                    <label for="switcherySize13"
                                                           class="font-medium-2 text-bold-600 mr-1">حذف شده</label>
                                                    <input type="checkbox" id="switcherySize13" name="deleted"
                                                           class="switchery" data-size="xs" data-color="warning"
                                                           data-switchery="true" style="display: none;">
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group mt-1">
                                                    <button type="submit" class="btn btn-primary search-ajax">
                                                        اعمال فیلتر
                                                        <i class="ft-thumbs-up position-right"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <table class="table table-responsive-md text-center">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>شرکت</th>
                                            <th>شماره فاکتور</th>
                                            <th>مبلغ</th>
                                            <th>تاریخ</th>
                                            <th>وضعیت</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>کاله</td>
                                            <td>102563</td>
                                            <td>500000</td>
                                            <td>1398/5/12</td>
                                            <td style="color:green">تایید شده</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>میهن</td>
                                            <td>10453</td>
                                            <td>900000</td>
                                            <td>1398/5/12</td>
                                            <td style="color:orange">بازگشت از شرکت</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>صدف</td>
                                            <td>3454321</td>
                                            <td>400000</td>
                                            <td>1398/5/12</td>
                                            <td style="color:red">حذف شده</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection