@extends('master_page')
@section('title_browser') تنظیمات @endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <form action="" method="post">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>دریافت کننده فاکتور</label>
                                            <select class="form-control" name="factor_receiver">
                                                {!! customForeach($role,'rol_id','rol_label',@$factor->st_value) !!}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-10">
                                            <label> حداقل مبلغ فاکتور</label>
                                            <input type="text" class="form-control" chars="int"
                                                   value="{{!empty($min_factor_amount)?$min_factor_amount->st_value:''}}"
                                                   name="min_factor_amount" placeholder="50000">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-10">
                                            <label>توکن</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="token" value="{{ !empty($token)?$token:'' }}" disabled>
                                                <div class="input-group-append">
                                                    <button type="button" class="btn btn-success" id="createToken" data-toggle="tooltip"
                                                            data-placement="top" title=""
                                                            data-original-title="ساخت توکن جدید" aria-label="Create">
                                                        <i class="fa fa-plus"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-info" id="copyToken" aria-label="Copy"
                                                            data-toggle="tooltip" data-placement="left" title=""
                                                            data-original-title="کپی توکن">
                                                        <i class="icon-docs"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <label>قابلیت مرجوعی</label>

                                        <div class="form-group">
                                            <input type="checkbox"
                                                   name="returned"
                                                   class="switchery form-control activator"
                                                   data-size="xs"
                                                   data-switchery="true"
                                                   style="display: none;"{{(isset($returned->st_value) && $returned->st_value==0)?'':'checked'}}>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <button type="button" class="btn btn-success" id="save-user-setting">ثبت
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('/js/functions/storeAjax.js') }}"></script>
    <script>
        $(document).ready(function () {
            $(document).on('click', '#save-user-setting', function (e) {
                storeAjax($(this), 'POST', captionButton = "ثبت", progress = false, reloadPage = true, TimeOutActionNextStore = 2000)
            });
            $("#createToken").click(function () {
                $.ajax({
                    url: '/create-token',
                    type: 'POST',
                    data: {},
                    success: function (data) {
                        var result=JSON.parse(data);
                        console.log(result);
                        if(result.status==100){
                            toastr.success(result.msg, "موفق", {showMethod: "slideDown", hideMethod: "slideUp", timeOut: 3000});
                            setTimeout(function() {
                                location.reload();
                            }, 3000);
                        }else if(result.status==500){
                            toastr.error(result.msg, "خطا", {showMethod: "slideDown", hideMethod: "slideUp", timeOut: 3000});
                        }
                    }
                });
            });
            $( '#copyToken' ).click( function()
            {
                var clipboardText = "";
                clipboardText = $( '#token' ).val();
                copyToClipboard( clipboardText );
                alert("توکن کپی شد")

            });
        });
    </script>
@endsection
