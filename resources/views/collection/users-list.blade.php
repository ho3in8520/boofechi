@extends('master_page')
<?php $title = 'لیست کاربران'?>
@section('title_browser'){{$title}}@endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div>
            <div class="row">
                <div class="col-12">
                    <h2 class="content-header">{{$title}}</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{asset('users-list')}}" method="get">
                                <div class="card-block">
                                    <table class="table table-bordered text-center">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>آواتار</th>
                                            <th>نام کاربری</th>
                                            <th>نام</th>
                                            <th>نام خانوادگی</th>
                                            <th>نقش</th>
                                            <th>شماره موبایل</th>
                                            <th>عملیات</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>-</td>
                                            <td>-</td>
                                            <td><input name="username" class="form-control"
                                                       value="{{ request()->query('username') }}" autofocus=""></td>
                                            <td><input name="name" class="form-control"
                                                       value="{{ request()->query('name') }}"></td>
                                            <td><input name="family" class="form-control"
                                                       value="{{ request()->query('family') }}"></td>
                                            <td><input name="rol" class="form-control"
                                                       value="{{ request()->query('rol') }}"></td>
                                            <td><input name="mobile" class="form-control"
                                                       value="{{ request()->query('mobile') }}"></td>
                                            <td>
                                                <button type="submit" class="btn-loading btn btn-primary search-ajax">
                                                    فیلتر <i class="ft-thumbs-up position-right"></i>
                                                </button>
                                            </td>
                                        </tr>
                                        @foreach($users as $user)
                                            <tr data-id="{{$user->user_id}}" data-wallet="{{number_format(withoutZeros($user->amount))}}">
                                                <td>{{index($users,$loop)}}</td>
                                                <td>
                                                    <img class="rounded-circle"
                                                         style="width: 50px;height: 50px"
                                                         src="{{ getFile(($user->avatar != "") ? $user->avatar : 'nologo.png') }}">
                                                </td>
                                                <td>{{$user->username}}</td>
                                                <td>{{$user->prsn_name}}</td>
                                                <td>{{$user->prsn_family}}</td>
                                                <td>{{$user->rol_label}}</td>
                                                <td>{{$user->prsn_mobile1}}</td>
                                                <td>
                                                    @can('delete_user')
                                                        <a class="primary"
                                                           data-action="{{asset('/delete-user').'/'.$user->user_id}}"
                                                           data-confirm="اطمینان از حذف این مورد دارید؟"
                                                           data-method="delete">
                                                            <i class="fa fa-trash"></i>
                                                        </a>
                                                    @endcan
                                                    @can('edit_user')
                                                        <a href="{{asset('/update-user').'/'.$user->user_id}}">
                                                            <i class="fa fa-pencil-alt"></i>
                                                        </a>
                                                    @endcan
                                                        <a class="wallet-user">
                                                            <i class="fa fa-dollar-sign"></i>
                                                        </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).on("click", ".wallet-user", function () {
            var element = $(this);
            var wallet = $(element).closest("tr").data("wallet");
            swal({
                title: "افزایش | کاهش کیف پول",
                input: 'select',
                inputOptions: {
                    1: 'افزایش',
                    2: 'کاهش',
                },
                html:
                    '<div class="row justify-content-center">' +
                    '<div class="col-md-12 col-sm-12">' +
                    '<h6 style="color:green">موجودی کیف پول کاربر : '+wallet+' </h6>' +
                    '</div>' +
                    '<hr>' +
                    '<div class="col-md-3 col-sm-3">' +
                    '<input id="amount" onkeyup="if (/\\D/g.test(this.value)) this.value = this.value.replace(/\\D/g,\'\');" class="form-control" placeholder="مبلغ">' +
                    '</div>' +
                    '</div>' ,
                showCancelButton: true,
                animation: "slide-from-top",
                confirmButtonText: 'ثبت',
                cancelButtonText: "انصراف",
                inputPlaceholder: 'انتخاب کنید...',
                inputValidator: function (value) {
                    return new Promise(function (resolve, reject) {
                        if (value !== '' && $('#amount').val() !== '') {
                            resolve();
                        } else {
                            reject('فیلد ها الزامی هستن');
                        }
                    });
                }
            }).then(function (inputValue) {
                if (inputValue === false) return false;
                var id = $(element).closest("tr").data('id');
                var amount = $('#amount').val();
                $.post("{{asset('update-wallet')}}", {
                    user_id: id,
                    type: inputValue,
                    amount: amount,
                }, function (data) {
                    var result = JSON.parse(data);
                    if (result.status == 100) {
                        toastr.success(result.msg, "موفق");
                        setTimeout(function () {
                        });
                    }else if(result.status == 300) {
                        toastr.error(result.msg, "مبلغ کیف پول کاربر کمتر از حد مشخص شده است");
                    }else if(result.status == 500) {
                        toastr.error(result.msg, "خطا در ثبت اطلاعات!");
                    }
                });
            }).done();
        });
    </script>
    @endsection
