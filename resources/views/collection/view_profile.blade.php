@extends('master_page')

@section('title_browser') پروفایل کاربر @endsection

@section('main_content')

    <section id="clear-toasts">

        <div class="row">

            <div class="col-12">

                <div class="card">

                    <div class="card-body">

                        <div class="card-block">

                            <div class="row mt-1">

                                <div class="col-lg-6 col-md-6 col-sm-12">

                                    <img src="{{getFile($result->logo,"logo")}}"

                                         class="rounded-circle" style="height: 300px

                                          ; width: 300px;">

                                </div>

                                <div class="col-lg-6 col-md-4 col-sm-6" style="margin-top: 50px">

                                    <h5>

                                        @if($result->panel_type==44)

                                            فروشگاه : {{$result->coll_name}}

                                        @elseif($result->panel_type==45)

                                            ویزیتور : {{$result->coll_name}}

                                        @elseif($result->panel_type==42)

                                            شرکت : {{$result->coll_name}}

                                        @elseif($result->panel_type==41)

                                            کارخانه : {{$result->coll_name}}

                                        @elseif($result->panel_type==46)

                                            نماینده : {{$result->coll_name}}

                                        @elseif($result->panel_type==43)

                                            عمده فروش : {{$result->coll_name}}

                                        @elseif($result->panel_type==39)

                                            ادمین سامانه

                                        @endif



                                    </h5>

                                    <p><strong>آدرس : </strong>{{$result->prsn_address}}

                                    </p>

                                    <p><strong>شماره تماس : </strong>{{$result->prsn_phone1}}</p>

                                    <p><strong>ایمیل : </strong>{{$result->email}}</p>

                                    <div class="form-actions">

                                        <div class="text-right">

                                            <button type="button" data-toggle="modal" data-target="#sendMessage"

                                                    class="btn btn-info mr-1">ارسال پیام

                                            </button>

                                            <button type="button" data-toggle="modal" data-target="#Report"

                                                    class="btn btn-warning">گزارش به ادمین

                                            </button>



                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

        </div>

    </section>

    <div class="modal fade text-left" id="sendMessage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel17"

         aria-hidden="true">

        <div class="modal-dialog modal-xs" role="document">

            <div class="modal-content">

                <div class="modal-header">

                    <h4 class="modal-title" id="myModalLabel17">ارسال پیام به کاربر</h4>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                        <span aria-hidden="true">&times;</span>

                    </button>

                </div>

                <form method="post" action="{{asset('send-message').'/'.$result->coll_id}}" enctype="multipart/form-data">

                    <div class="modal-body">

                        <div class="col-md-8">

                            <fieldset class="form-group position-relative">

                                <input type="text" name="title_sendMessage" class="form-control" id="Default"

                                       placeholder="عنوان پبام">

                            </fieldset>

                        </div>

                        <div class="col-md-12">

                            <fieldset class="form-group position-relative">

                                <textarea name="text_sendMessage" class="form-control"

                                          placeholder="متن پیام..."></textarea>

                            </fieldset>

                        </div>

                        <div class="col-md-4">

                            <fieldset class="form-group position-relative">

                                <label for="file-upload" class="btn mr-1 btn-secondary">آپلود تصویر</label>

                                <input type="file" name="file_sendMessage" id="file-upload" accept="image/jpeg"

                                       style="display: none">

                            </fieldset>

                        </div>



                    </div>

                    <div class="modal-footer">

                        <button type="button" id="send_sendMessage" class="btn btn-primary">ارسال پیام</button>

                        <button type="button" class="btn btn-danger" data-dismiss="modal">انصراف

                        </button>

                    </div>

                </form>

            </div>

        </div>

    </div>

    <div class="modal fade text-left" id="Report" tabindex="-1" role="dialog" aria-labelledby="myModalLabel17"

         aria-hidden="true">

        <div class="modal-dialog modal-xs" role="document">

            <div class="modal-content">

                <div class="modal-header">

                    <h4 class="modal-title" id="myModalLabel17">گزارش به ادمین</h4>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                        <span aria-hidden="true">&times;</span>

                    </button>

                </div>

                <form method="post" action="{{asset('report-to-admin').'/'.$id}}" enctype="multipart/form-data">

                    <div class="modal-body">

                        <div class="col-md-12">

                            <fieldset class="form-group position-relative">

                                <textarea name="text_report" class="form-control" placeholder="متن گزارش..."></textarea>

                            </fieldset>

                        </div>

                        <div class="col-md-4">

                            <fieldset class="form-group position-relative">

                                <label for="file1-upload" class="btn mr-1 btn-secondary">آپلود تصویر</label>

                                <input type="file" name="file_report" id="file1-upload" accept="image/jpeg"

                                       style="display: none">

                            </fieldset>

                        </div>

                    </div>

                    <div class="modal-footer">

                        <button type="button" id="send_report" class="btn btn-primary">ارسال گزارش</button>

                        <button type="button" class="btn btn-danger" data-dismiss="modal">انصراف

                        </button>

                    </div>

                </form>

            </div>

        </div>

    </div>

@endsection

@section('script')

    <script src="{{ asset('/js/functions/storeAjax.js') }}"></script>

    <script>

        $(document).ready(function () {

            $(document).on('click', '#send_sendMessage', function (e) {

                storeAjax($(this), 'POST')

            });

            $(document).on('click', '#send_report', function (e) {

                storeAjax($(this), 'POST')

            });

        });

    </script>

@endsection