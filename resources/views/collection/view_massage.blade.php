@extends('master_page')

@section('title_browser') پیام ها @endsection

@section('style')

    <style>

        .date_margin {

            margin: 30px;

        }

    </style>

@endsection

@section('main_content')

    @if($result)

        <section id="user-area">

            <div class="row">

                <div class="col-xl-3 col-lg-12">

                    <div class="card mb-4">

                        <div class="card-body">

                            <form method="post" action="">

                                <div class="card-block">

                                    <div class="align-self-center halfway-fab text-center">

                                        <img src="{{getFile($result->logo,"logo")}}"

                                             class="rounded-circle img-border gradient-summer width-100"

                                             alt="Card image">

                                    </div>

                                    <div class="text-center">

                                        <a href="#"><span

                                                    class="font-medium-2 text-uppercase">{{$result->coll_name}}</span></a>

                                    </div>

                                    <div class="row">

                                        <div class="col-md-12">

                                            <ul class="no-list-style pr-0 text-center date_margin">

                                                <li class="mb-2">

                                            <span class="text-bold-500 primary"><i

                                                        class="icon-calendar font-small-3"></i> تاریخ ارسال :</span>

                                                    <span class="display-block overflow-hidden">{{jdate_from_gregorian($result->msg_created_at,"Y/m/d | H:i")}}</span>

                                                </li>

                                            </ul>

                                        </div>

                                    </div>

                                </div>

                            </form>

                        </div>

                    </div>

                </div>

                <div class="col-xl-9 col-lg-12">

                    <!--About div starts-->

                    <div id="about">

                        <div class="row">

                            <div class="col-sm-12">

                                <div class="card">

                                    <div class="card-header">

                                        <div class="card-title-wrap bar-info">

                                            <div class="card-title"><b>{{$result->msg_title}}</b></div>

                                        </div>

                                    </div>

                                    <div class="card-body">

                                        <div class="card-block">

                                            <div class="mb-3">

                                            <span class="display-block overflow-hidden"><p class="textalign">

                                         {{$result->msg_message}}</p>

                                    </span></div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </section>

    @else

        <div class="card px-3 py-2 box-shadow-2">

            <div class="card-body">

                <div class="card-block">

                    <div class="error-container">

                        <div class="no-border">

                            <div class="text-center text-bold-700 grey darken-2 mt-5"

                                 style="font-size: 11rem; margin-bottom: 4rem;">404

                            </div>

                        </div>

                        <div class="error-body">



                            <div class="py-2 justify-content-center">

                                <div class="text-center">

                                    <h4>موردی برای نمایش یافت نشد</h4>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    @endif

@endsection