@extends('master_page')
<?php
$title = "روش های پرداخت آنلاین";
?>
@section('title_browser') {{$title}}@endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header"><?=$title?></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <form id="gate-form" action="{{url()->current()}}" method="post">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <select class="form-control" id="gate" name="gate">
                                                {!! customForeach($online_gates,'og_id','og_title') !!}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                </div>
                            </form>
                            <form action="{{url()->current()}}" method="get">
                                <table class="table table-responsive-md text-center table-bordered">
                                    <thead>
                                    <tr>
                                        <th>ردیف</th>
                                        <th>نام</th>
                                        <th>وضعیت</th>
                                        <th>عملیات</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td></td>
                                        <td><input class="form-control" name="title"
                                                   value="{{request()->query('title')}}"></td>
                                        <td>
                                            <select class="form-control" name="status">
                                                <option value="1" {{request()->query('status') == 1 ? "selected" : ''}}>
                                                    انتخاب کنید...
                                                </option>
                                                <option value="2" {{request()->query('status') == 2 ? "selected" : ''}}>
                                                    فعال
                                                </option>
                                                <option value="3" {{request()->query('status') == 3 ? "selected" : ''}}>
                                                    غیر فعال
                                                </option>
                                            </select>
                                        </td>
                                        <td>
                                            <button type="submit" class="btn-loading btn btn-primary search-ajax">
                                                اعمال فیلتر
                                                <i class="ft-thumbs-up position-right"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    <?php
                                    $i = 0;
                                    ?>
                                    @if(count($collection_online_gates) > 0)
                                        @foreach($collection_online_gates as $gate)
                                            <?php
                                            $i++;
                                            ?>
                                            <tr data-id="{{$gate->cog_id}}" data-og="{{$gate->og_id}}" data-param1="{{$gate->cog_param1}}" data-param2="{{$gate->cog_param2}}" data-param3="{{$gate->cog_param3}}">
                                                <td>{{$i}}</td>
                                                <td>{{$gate->og_title}}</td>
                                                <td class="status">
                                                    <span class="text-white badge badge-{{($gate->cog_status == 1) ? "success" : "danger"}}">{{($gate->cog_status == 1) ? "فعال" : "غیرفعال"}}</span>
                                                </td>
                                                <td>
                                                    <a title="ویرایش" class="editGate"><i class="fa fa-pencil"></i></a>
                                                    @if($gate->cog_status == 0)
                                                        <a title="فعال کردن" class="changeStatus" data-status="1"><i
                                                                    class="fa fa-check"></i></a>
                                                    @else
                                                        <a title="غیر فعال کردن" class="changeStatus" data-status="0"><i
                                                                    class="fa fa-remove"></i></a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="4" style="text-align: center;">
                                                {{ config('first_config.message.empty_table') }}
                                            </td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form" style="display: none">
            <div class="gate2">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>ترمینال</label>
                        <input type="text" class="form-control" name="terminal">
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>نام کاربری</label>
                        <input type="text" class="form-control" name="username">
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>کلمه عبور</label>
                        <input type="text" class="form-control" name="password">
                    </div>
                </div>
            </div>
            <div class="gate3">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>کد پذیرنده</label>
                        <input type="text" class="form-control" name="merchant">
                    </div>
                </div>
            </div>
            <div class="gate4">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>ترمینال</label>
                        <input type="text" class="form-control" name="terminal">
                    </div>
                </div>
            </div>
            <div class="row" id="gate-form-button">
                <div class="col-sm-3">
                    <button type="button" class="btn btn-success btn-loading addGate">ثبت</button>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('/js/functions/storeAjax.js') }}"></script>
    <script>
        $(document).ready(function () {
            $(document).on("change", "#gate", function () {
                var id = $(this).val();
                $("#gate-form").find(".row:nth-child(2)").html($(".gate" + id).html());
                $("#gate-form").find(".row").slice(2).remove();
                $("#gate-form").find(".row:nth-child(2)").after("<div class='row'>" + $("#gate-form-button").html() + "</div>");
            });
            $(document).on("click", ".addGate", function () {
                storeAjax($(this), 'POST', 'ثبت');
            });
            $(document).on("click", ".editGate", function () {
                var id = $(this).closest("tr").data("id");
                var gate_id = $(this).closest("tr").data("og");
                var param1 = $(this).closest("tr").data("param1");
                var param2 = $(this).closest("tr").data("param2");
                var param3 = $(this).closest("tr").data("param3");
                $("select[name=gate]").val(gate_id);
                var html = $(".gate" + gate_id).clone();
                html.find("input:eq(0)").attr("value",param1);
                html.find("input:eq(1)").attr("value",param2);
                html.find("input:eq(2)").attr("value",param3);
                $("#gate-form").find(".row:nth-child(2)").html(html.html());
                $("#gate-form").find(".row:nth-child(2)").append("<input type='hidden' value='"+id+"' name='cog_id'>");
                $("#gate-form").find(".row").slice(2).remove();
                $("#gate-form").find(".row:nth-child(2)").after("<div class='row'>" + $("#gate-form-button").html() + "</div>");

            });
            $(document).on("click",".changeStatus",function(){
                var elem = $(this);
                var id = $(elem).closest("tr").data("id");
                $.post("{{asset('change-status-gate').'/'}}"+id,{"status":$(elem).data("status")},function(data){
                   var result = JSON.parse(data);
                   if(result.status == 100)
                   {
                       toastr.success(result.msg,"موفق");
                       if($(elem).data("status") == 0)
                       {
                           $(elem).data("status",1);
                           $(elem).html('<i class="fa fa-check"></i>');
                           $(elem).closest("tr").find("td.status").html('<span class="text-white badge badge-danger">غیر فعال</span>');
                       }
                       else
                       {
                           $(elem).html('<i class="fa fa-remove"></i>');
                           $(elem).data("status",0);
                           $(elem).closest("tr").find("td.status").html('<span class="text-white badge badge-success">فعال</span>');
                       }
                   }
                   else
                       toastr.success(result.msg,"ناموفق");
                });
            });

        });
    </script>
@endsection