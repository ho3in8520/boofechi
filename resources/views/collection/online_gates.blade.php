@extends('master_page')
<?php $title = 'درگاه های بانکی'?>
@section('title_browser',$title)
@section('main_content')
    <section class="basic-elements">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header">{{$title}}</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="px-3">
                            <form class="form" method="post" action="" enctype="multipart/form-data">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="card-header">
                                        </div>
                                        <div class="col-12 table-responsive">

                                            <table class="table table-striped table-bordered sourced-data dataTable">
                                                <thead>
                                                <tr>
                                                    <th>ردیف</th>
                                                    <th>نام درگاه</th>
                                                    <th>فعال/غیرفعال</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($online_gates as $row)
                                                    <tr class="text-center">
                                                        <td>{{$loop->index + 1}}</td>
                                                        <td>{{$row['og_title']}}</td>
                                                        <td>
                                                            <input type="checkbox" id="switcherySize13"
                                                                   name="active[{{$row['og_id']}}]"
                                                                   class="switchery" data-size="xs"
                                                                   data-switchery=""
                                                                   value="{{$row['cog_id']}}"
                                                                   style="display: none;"
                                                                   @if($row['cog_status']==true)
                                                                   checked
                                                                    @endif
                                                            >
                                                        </td>
                                                        @endforeach
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button id="updateReturn_reason"
                                                            class="btn btn-success btn-loading" type="button">
                                                        ثبت
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
@section('script')
    <script src="{{ asset('/js/functions/storeAjax.js') }}"></script>
    <script>

        $(document).ready(function () {
            $("#updateReturn_reason").click(function (e) {
                e.preventDefault();
                storeAjax($(this), 'POST','ثبت',false,1);
            });
        });
    </script>
@endsection