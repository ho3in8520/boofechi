@extends('master_page')
@section('title_browser') فاکتور های سامانه @endsection
@section('style')
    <link rel="stylesheet" type="text/css" href="{{asset("/theme/vendors/css/sweetalert2.min.css")}}">

    <style>

        .name-company {
            margin-left: auto;
            margin-right: 150px;
        }

        .bold {
            font-weight: bold;
        }

        a {
            color: #757575;
        }

        a:hover {
            color: #757575;
        }

    </style>

@endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-12">
                <h2 class="content-header">فاکتور های سامانه</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <form action="" method="get">
                                <div class="summary">{!!getSummary($result) !!}</div>
                                <table class="table table-striped table-bordered sourced-data dataTable">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>کاربر ایجاد کننده</th>
                                        <th>تاریخ ثبت</th>
                                        <th>مبلغ({{setting('financial_unit')}})</th>
                                        <th>وضعیت پرداخت</th>
                                        <th>عملیات</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>-</td>
                                        <td>
                                            <input type="text" name="user-create" class="form-control"
                                                   value="{{ request()->query('user-create') }}"
                                                   placeholder="کاربر ایجاد کننده">
                                        </td>
                                        <td>
                                            <div class="input-group">
                                                <input type="text" class="form-control datePicker"
                                                       name="date" placeholder="تاریخ"
                                                       autocomplete="off"
                                                       value="{{ request()->query('date') }}">

                                            </div>
                                        </td>
                                        <td>
                                            <input type="text" name="amount" class="form-control"
                                                   value="{{ request()->query('amount') }}"
                                                   placeholder="مبلغ">
                                        </td>
                                        <td>
                                            <select name="is_payed" class="form-control">
                                                <option value="0" {{request()->query('is_payed') == "" ? 'selected' : ''}}>
                                                    انتخاب کنید...
                                                </option>
                                                <option value="1" {{request()->query('is_payed') == 1 ? 'selected' : ''}}>
                                                    پرداخت نشده
                                                </option>
                                                <option value="2" {{request()->query('is_payed') == 2 ? 'selected' : ''}}>
                                                    پرداخت شده
                                                </option>


                                            </select>
                                        </td>
                                        <td>
                                            <button type="submit"
                                                    class="btn-loading btn btn-primary search-ajax">
                                                اعمال فیلتر <i class="ft-thumbs-up position-right"></i>
                                            </button>
                                        </td>
                                    </tr>


                                    @foreach($result as $row)
                                        <?php
                                        $sum = $row->sfm_amount - $row->sfm_discount
                                        ?>
                                        <tr data-id="{{$row->sfm_id}}">
                                            <td>{{index($result ,$loop)}}</td>
                                            <td>{{$row->prsn_name}} {{$row->prsn_family}}</td>
                                            <td>{{jdate_from_gregorian($row->sfm_created_at,'Y/m/d')}}</td>
                                            <td>{{number_format(withoutZeros($sum))}}</td>
                                            <td>
                                                @if($row->sfm_is_payed==0 && $row->sfm_is_force==1)
                                                    <span class="badge text-white badge-danger">پرداخت فوری</span>
                                                @elseif($row->sfm_is_payed==0)
                                                    <span class="badge text-white badge-warning">پرداخت نشده</span>
                                                @else
                                                    <span class="badge text-white badge-success">پرداخت شده</span>
                                                @endif
                                            </td>
                                            <td>

                                                <a href="{{asset('view-system-factor'.'/'.$row->sfm_id)}}"
                                                   title="نمایش"><i class="fa fa-eye"></i> </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {!! $result->appends(request()->query())->render() !!}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('/js/functions/storeAjax.js') }}"></script>
    <script src="{{asset("/theme/vendors/js/sweetalert2.min.js")}}"></script>

    <script>
        $(document).ready(function () {

            $(document).on('click', '#view', function (e) {
                storeAjax($(this), 'POST', captionButton = "خواندن", progress = false, reloadPage = true, TimeOutActionNextStore = 2000)

            });

        });

    </script>
@append
