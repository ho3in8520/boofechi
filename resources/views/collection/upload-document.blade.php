@extends('master_page')

@section('title_browser') آپلود فایل @endsection
@section('style')@endsection
@section('main_content')
    <div class="row">
        <div class="col-sm-12">
            <h2 class="content-header">آپلود فایل</h2>
        </div>
    </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <form action="" method="POST" enctype="multipart/form-data">
                                <div class="row sample">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>انتخاب فایل</label>
                                            <br>
                                            <div class="deleteImage">
                                            </div>
                                            <input type="file" style="display: none" class="form-control imgInp"
                                                   name="ur_url[]">
                                            <img class="blah" title="آپلود عکس" src="{{ asset('/images/image.jpg') }}"
                                                 alt="your image" width="100px" height="100px">

                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <?php $types = \App\Baseinfo::whereBasType('typeUpload')->where('bas_parent_id', '<>', '0')->get() ?>
                                        <div class="form-group">
                                            <label>نوع فایل</label>
                                            <select class="form-control" name="ur_type_id[]">
                                                {!! customForeach($types,'bas_id','bas_value') !!}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>شرح</label>
                                            <textarea class="form-control"
                                                      name="ur_description[]"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="addForm">

                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <a href="{{ asset($url) }}" class="btn btn-danger">بازگشت</a>
                                        <button class="btn btn-success btn-loading" id="uploadFile" type="button">ثبت
                                        </button>
                                        <button class="btn btn-primary addFormUpload" type="button">
                                            افزودن فرم جدید
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('/js/functions/storeAjax.js') }}"></script>
    <script>
        $(document).on('click', '#uploadFile', function () {
            $('.ajaxMessage').html("");
            storeAjax($(this), 'POST', "بعدی", true, 0, 2000)
        });
        $(document).on('click', '.addFormUpload', function () {
            $('.ajaxMessage').html("");
            var htmlForm = $(".sample:last").clone();
            $(".help-block").remove();
            $(".addForm").append(htmlForm);
            $(".sample:last").find(".help-block").remove();
            $(".sample:last").find("textarea,input").val("");
            $(".imgInp:last").val('');
            $(".blah:last").attr('src', "{{ asset('/images/image.jpg') }}")
            $(".deleteImage:last").html("");
            $("textarea,select,input").css({
                "border-width": "1px",
                "border-color": "#b8b894",
                "border-style": "solid"
            });
        });
        elementClick = "";
        $(document).on('click', '.blah', function () {
            elementClick = $(this);
            $(elementClick).closest('.form-group').find(".imgInp").click();
        });
        $(document).on('change','.imgInp',function(){
            previewImage(this);
            $(elementClick).closest('.form-group').find(".deleteImage").html("<i id='deleteImageUpload' title=\"حذف عکس\" class=\"fa fa-trash text-danger\"></i>");
        });
        function previewImage(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $(elementClick).closest('.form-group').find('.blah').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }


        $(document).on('click', '#deleteImageUpload', function () {
            $(this).closest(".form-group").find(".imgInp").val('');
            $(this).closest(".form-group").find(".blah").attr('src', '/images/image.jpg');
            $(this).closest(".form-group").find(".deleteImage").html("");
        });
    </script>
@endsection
