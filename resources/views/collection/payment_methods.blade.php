@extends('master_page')
@section('title_browser') روش های پرداخت @endsection
@section('main_content')
    <section class="basic-elements">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header">روش های پرداخت</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="px-3">
                            <form class="form" method="post" action="">
                                <div class="form-body">

                                    @foreach($methods as $method)
                                        @foreach($old_pays as $old_pay)
                                            <?
                                            if ($method['bas_id'] == $old_pay['cpm_bas_id']) {
                                                $percent = $old_pay['cpm_percent'];
                                                $description = $old_pay['cpm_description'];
                                                $status = $old_pay['cpm_status'];
                                                }
                                            ?>
                                        @endforeach

                                        <div class="row">
                                            <div class="col-md-2">
                                                <div class="form-group" style="margin-top: 35px">
                                                    <input value="{{$method['bas_id']}}" type="checkbox"
                                                           name="is_active[{{$method['bas_id']}}]"
                                                           class="switchery form-control activator"
                                                           data-size="xs"
                                                           data-switchery="true" style="display: none;"
                                                           @if(@$status==true)
                                                           checked
                                                    @endif
                                                    ">
                                                    {{$method['bas_value']}}
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group" style="margin-top: 30px">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control"
                                                               value="{{ empty($percent) ?  '0' : withoutZeros($percent) }}"
                                                               name="percent[{{$method['bas_id']}}]" placeholder="تخفیف"
                                                                {{(@$status == false) ? 'disabled' : ''}} chars="english">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text" id="basic-addon2">%</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-8">
                                                <div class="form-group" style="margin-top: 30px">
                                                    <input type="text" class="form-control"
                                                           name="description[{{$method['bas_id']}}]"
                                                           placeholder="توضیحات"
                                                           {{(@$status == false) ? 'disabled' : ''}} value="{{ empty($description) ?  '' : $description }}">
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                    <div class="row">
                                        <div class="col-md-12">
                                            <button id="createPayment"
                                                    class="btn btn-success btn-loading" type="button">
                                                ثبت
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('/js/functions/storeAjax.js') }}"></script>
    <script>
        $(document).ready(function () {
            $(document).on('change', '.activator', function () {
                var val = $(this).prop('value');
                var elem = $(this).closest('.row');
                if ($(this).prop('checked')) {
                    $(elem).find("input").prop("disabled", false)
                } else {
                    $(elem).find("input").prop("disabled", true);
                    $(elem).find("input");
                }
            });
            $(document).on('click', '#createPayment', function (e) {
                storeAjax($(this), 'POST', captionButton = "ثبت", progress = false, reloadPage = true, TimeOutActionNextStore = 2000)

            });
        });
    </script>
@endsection
