@extends('master_page')
<?php
$i = 0;
$title = 'تخفیف بازه ای'?>
@section('title_browser') {{$title}} @endsection
@section('main_content')
    <section class="basic-elements">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header">{{$title}}</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="px-3">
                            <form class="form" id="formEshant" method="post" action="">
                                <div class="form-body">

                                    @if($return)
                                        @foreach($return as $value)
                                            <div class="row festivalDiscounts">
                                                <div class="col-sm-3">
                                                    <label>تاریخ</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control datePicker"
                                                               name="date_from[{{$i}}]"
                                                               placeholder="تاریخ از"
                                                               autocomplete="off"
                                                               value="{{jdate($value['cfc_date_from'])->format('Y/m/d')}}">
                                                        <input type="text" class="form-control datePicker"
                                                               name="date_to[{{$i}}]"
                                                               placeholder="تاریخ تا"
                                                               autocomplete="off"
                                                               value="{{jdate($value['cfc_date_to'])->format('Y/m/d')}}">
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>قیمت</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control"
                                                               name="price_from[{{$i}}]"
                                                               placeholder="قیمت از"
                                                               autocomplete="off" value="{{$value['cfc_buy_from']}}">
                                                        <input type="text" class="form-control"
                                                               name="price_to[{{$i}}]"
                                                               placeholder="قیمت تا"
                                                               autocomplete="off" value="{{$value['cfc_buy_to']}}">
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <label>درصد</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control"
                                                               name="percentage[{{$i}}]"
                                                               placeholder="%8" aria-describedby="basic-addon2"
                                                               value="{{WithoutZeros($value['cfc_discount_percent'])}}">
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group" style="margin-top: 30px">
                                                        <a type="button" class="btn btn-danger deleteForm"
                                                                data-action="{{asset('festival-discounts').'/'.$value['cfc_id']}}"
                                                                data-confirm="اطمینان از حذف این مورد دارید؟"
                                                                data-method="delete">
                                                            <i class="fa fa-trash"></i>
                                                        </a>
                                                        <button type="button" class="btn btn-primary addForm">
                                                            <i class="fa fa-plus"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                            $i++;
                                            ?>
                                        @endforeach
                                    @else
                                        <div class="row festivalDiscounts">
                                            <div class="col-sm-3">
                                                <label>تاریخ</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control datePicker"
                                                           name="date_from[{{$i}}]"
                                                           placeholder="تاریخ از"
                                                           autocomplete="off"
                                                           value="">
                                                    <input type="text" class="form-control datePicker"
                                                           name="date_to[{{$i}}]"
                                                           placeholder="تاریخ تا"
                                                           autocomplete="off"
                                                           value="">
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <label>قیمت</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control"
                                                           name="price_from[{{$i}}]"
                                                           placeholder="قیمت از"
                                                           autocomplete="off" value="">
                                                    <input type="text" class="form-control"
                                                           name="price_to[{{$i}}]"
                                                           placeholder="قیمت تا"
                                                           autocomplete="off" value="">
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <label>درصد</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control"
                                                           name="percentage[{{$i}}]"
                                                           placeholder="%8" aria-describedby="basic-addon2"
                                                           value="">
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group" style="margin-top: 30px">
                                                    <button type="button" class="btn btn-danger deleteForm">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-primary addForm">
                                                        <i class="fa fa-plus"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button id="createDiscount" name="create_basic_info"
                                                class="btn btn-success btn-loading" type="button">
                                            ثبت
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('/js/functions/storeAjax.js') }}"></script>
    <script>
        counter = {{$i-1}};
        $(document).on('click', '.deleteForm', function () {
            var elementClick = $(this);
                if ($(elementClick).closest('.festivalDiscounts'))
                    $(elementClick).closest('.festivalDiscounts').remove();
                else
                    alert('حذف امکان پذیر نمی باشد');
        });
        $(document).on('click', '#createDiscount', function () {
            var err = false;
            var element = $(this);
            $(element).html("<i class='fa fa-spin fa-spinner'></i> منتظر بمانید...");
            var arr = [];
            $('input[name^=date_from]').each(function (k, v) {
                $(this).css('border-color', '#A6A9AE');
                arr[k] = {'from': $(this).val(), 'to': ''};
            });
            $('input[name^=date_to]').each(function (k, v) {
                $(this).css('border-color', '#A6A9AE');
                arr[k]['to'] = $(this).val();
            });
            $('input[name^=date_from]').each(function (index) {
                var elem = $(this);
                var date = $(elem).val();
                $(arr).each(function (key, value) {
                    if (index != key) {
                        if (date >= value.from && date < value.to) {
                            $(elem).css('border-color', 'red');
                            err = true;
                        }
                    }
                });
            });
            $('input[name^=date_to]').each(function (index) {
                var elem = $(this);
                var date = $(elem).val();
                $(arr).each(function (key, value) {
                    if (index != key) {
                        if (date > value.from && date <= value.to) {
                            $(elem).css('border-color', 'red');
                            err = true;
                        }
                    }
                });
            });

            $('input[name^=date_to]').each(function (index) {
                var elem = $(this);
                var date = parseInt($(elem).val());
                $(arr).each(function (key, value) {
                    if (index != key) {
                        if (date > value.from && date <= value.to) {
                            $(elem).css('border-color', 'red');
                            err = true;
                        }
                    }
                });
            });

            var err = false;
            var element = $(this);
            $(element).html("<i class='fa fa-spin fa-spinner'></i> منتظر بمانید...");
            var arr = [];
            $('input[name^=price_from]').each(function (k, v) {
                $(this).css('border-color', '#A6A9AE');
                arr[k] = {'from': parseInt($(this).val()), 'to': ''};
            });
            $('input[name^=price_to]').each(function (k, v) {
                $(this).css('border-color', '#A6A9AE');
                arr[k]['to'] = parseInt($(this).val());
            });
            $('input[name^=price_from]').each(function (index) {
                var elem = $(this);
                var date = parseInt($(elem).val());
                $(arr).each(function (key, value) {
                    if (index != key) {
                        if (date >= value.from && date < value.to) {
                            $(elem).css('border-color', 'red');
                            err = true;
                        }
                    }
                });
            });
            $('input[name^=price_to]').each(function (index) {
                var elem = $(this);
                var date = parseInt($(elem).val());
                $(arr).each(function (key, value) {
                    if (index != key) {
                        if (date > value.from && date <= value.to) {
                            $(elem).css('border-color', 'red');
                            err = true;
                        }
                    }
                });
            });
            $('input[name^=price_to]').each(function (index) {
                var elem = $(this);
                var date = parseInt($(elem).val());
                $(arr).each(function (key, value) {
                    if (index != key) {
                        if (date > value.from && date <= value.to) {
                            $(elem).css('border-color', 'red');
                            err = true;
                        }
                    }
                });
            });

            if (err == false) {
                storeAjax($(this), 'post');
            } else {
                $(element).html('ثبت');
            }
        });

        $(document).on('click', '.addForm', function () {
            var formHtml = $(this).closest('div.festivalDiscounts').clone();
            formHtml.counter('');
            $(this).closest('div.festivalDiscounts').after("<div class='row festivalDiscounts'>" + formHtml.html() + "</div>");
        });

    </script>

@endsection

