@extends('master_page')
<?php
$title = 'اشانت بر اساس فاکتور';
$i = 0;
?>
@section('title_browser') {{$title}} @endsection
@section('main_content')

    <section class="basic-elements">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header">{{$title}}</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="px-3">
                            <form class="form" method="post" action="">
                                <div class="form-body">
                                    @if($return)
                                        @foreach($return as $value)
                                            <div class="row festivalDiscounts">
                                                <div class="col-md-12">
                                                    <div class="row showFormDiscount">
                                                        <div class="col-sm-2">
                                                            <label>قیمت از</label>
                                                            <div class="input-group">
                                                                <input type="text" class="form-control"
                                                                       name="price_from[{{$i}}]" placeholder="قیمت از"
                                                                       value="{{$value['cfc_buy_from']}}">
                                                                <input type="text" class="form-control"
                                                                       name="price_to[{{$i}}]" placeholder="قیمت تا"
                                                                       value="{{$value['cfc_buy_to']}}">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <label>تاریخ</label>
                                                            <div class="input-group">
                                                                <input type="text" class="form-control datePicker"
                                                                       name="pc_date_from[]"
                                                                       value="{{jdate_from_gregorian($value['cfc_date_from'],'Y/m/d')}}"
                                                                       placeholder="تاریخ از">
                                                                <input type="text" class="form-control datePicker"
                                                                       name="pc_date_to[]"
                                                                       value="{{jdate_from_gregorian($value['cfc_date_to'],'Y/m/d')}}"
                                                                       placeholder="تاریخ تا">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <label>تعداد</label>
                                                            <div class="input-group">
                                                                <select class="form-control" name="type[{{$i}}]">
                                                                    @if($value['cp_count_per']>1)
                                                                        <option value="0">تعداد</option>
                                                                        <option
                                                                            value="1">{{$value['bas_value']}}</option>
                                                                    @else
                                                                        <option value="0">تعداد</option>
                                                                    @endif
                                                                </select>
                                                                <input type="text" class="form-control" name="count[{{$i}}]"
                                                                       value="{{number_format($value['cfc_free_count'])}}"
                                                                       placeholder="تعداد">
                                                                <input type="hidden" name="count_per[{{$i}}]" value="{{$value['cp_count_per']}}">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <label>محاسبه مازاد</label>
                                                            <input type="checkbox" style="margin-top: 40px !important;"
                                                                   name="pc_overflow[]" {{($value['cfc_overflow']==true)?'checked':''}}>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <div class="form-group">
                                                                <button type="button" name="product"
                                                                        class="btn btn-info showProduct form-control"
                                                                        style="margin-top: 31px">{{(!empty($value)?$value['prod_name']:'محصول')}}
                                                                </button>
                                                                <input type="hidden"
                                                                       name="pc_free_collection_product_id[{{$i}}]"
                                                                       value="{{$value['cfc_free_collection_product_id']}}">
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2">
                                                            <div class="form-group" style="margin-top: 30px">
                                                                <a type="button" class="btn btn-danger deleteForm"
                                                                   data-action="{{asset('factor-eshant').'/'.$value['cfc_id']}}"
                                                                   data-confirm="اطمینان از حذف این مورد دارید؟"
                                                                   data-method="delete">
                                                                    <i class="fa fa-trash"></i>
                                                                </a>
                                                                <button type="button" class="btn btn-primary addForm"
                                                                        style="width: 35px;height: 35px">
                                                                    <i class="fa fa-plus"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                            $i++;
                                            ?>
                                        @endforeach
                                    @else
                                        <div class="row festivalDiscounts">
                                            <div class="col-md-12">
                                                <div class="row showFormDiscount">
                                                    <div class="col-sm-2">
                                                        <label>قیمت از</label>
                                                        <div class="input-group">
                                                            <input type="text" class="form-control"
                                                                   name="price_from[]" placeholder="قیمت از">
                                                            <input type="text" class="form-control"
                                                                   name="price_to[]" placeholder="قیمت تا">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>تاریخ</label>
                                                        <div class="input-group">
                                                            <input type="text" class="form-control datePicker"
                                                                   name="pc_date_from[]" placeholder="تاریخ از">
                                                            <input type="text" class="form-control datePicker"
                                                                   name="pc_date_to[]" placeholder="تاریخ تا">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>تعداد</label>
                                                        <div class="input-group">
                                                            <select class="form-control" name="type[]">
                                                            </select>
                                                            <input type="text" class="form-control" name="count[]"
                                                                   placeholder="تعداد">
                                                            <input type="hidden" name="count_per[]">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>محاسبه مازاد</label>
                                                        <input type="checkbox" style="margin-top: 40px !important;"
                                                               name="pc_overflow[]" checked>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <div class="form-group">
                                                            <button type="button" name="product"
                                                                    class="btn btn-info showProduct form-control"
                                                                    style="margin-top: 31px">محصول
                                                            </button>
                                                            <input type="hidden" name="pc_free_collection_product_id[]">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-2">
                                                        <div class="form-group" style="margin-top: 30px">
                                                            <button type="button" class="btn btn-danger deleteForm"
                                                                    style="width: 35px;height: 35px">
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                            <button type="button" class="btn btn-primary addForm"
                                                                    style="width: 35px;height: 35px">
                                                                <i class="fa fa-plus"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="row">
                                        <div class="col-md-12">
                                            <button id="createEshant" name="create_basic_info"
                                                    class="btn btn-success" type="button">
                                                ثبت
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('/js/functions/storeAjax.js') }}"></script>

    <script>
        counter = {{$i-1}};

        function customeFunctionAfterSelectRow(node) {
            trClick.closest('.festivalDiscounts').find("input[name^='count_per']").val($(node).data('count_per'));
            if ($(node).data('count_per') > 1) {
                var optionElementReference = new Option($(node).data('weight_unit'), "1")
                trClick.closest('.festivalDiscounts').find("select[name^='type']").html('<option value="0">تعداد</option>').append(optionElementReference);
            } else
                trClick.closest('.festivalDiscounts').find("select[name^='type']").html('<option value="0">تعداد</option>');



        }


        $(document).on('click', '.deleteForm', function () {
            var elementClick = $(this);
            if ($(elementClick).closest('.festivalDiscounts'))
                $(elementClick).closest('.festivalDiscounts').remove();
            else
                alert('حذف امکان پذیر نمی باشد');
        });
        $(document).on('click', '#createEshant', function () {
            var err = false;
            var element = $(this);
            $(element).html("<i class='fa fa-spin fa-spinner'></i> منتظر بمانید...");
            var arr = [];
            $('input[name^=price_from]').each(function (k, v) {
                $(this).css('border-color', '#A6A9AE');
                arr[k] = {'from': parseInt($(this).val()), 'to': ''};
            });
            $('input[name^=price_to]').each(function (k, v) {
                $(this).css('border-color', '#A6A9AE');
                arr[k]['to'] = parseInt($(this).val());
            });
            $('input[name^=price_from]').each(function (index) {
                var elem = $(this);
                var date = parseInt($(elem).val());
                $(arr).each(function (key, value) {
                    if (index != key) {
                        if (date >= value.from && date < value.to) {
                            $(elem).css('border-color', 'red');
                            err = true;
                        }
                    }
                });
            });
            $('input[name^=price_to]').each(function (index) {
                var elem = $(this);
                var date = parseInt($(elem).val());
                $(arr).each(function (key, value) {
                    if (index != key) {
                        if (date > value.from && date <= value.to) {
                            $(elem).css('border-color', 'red');
                            err = true;
                        }
                    }
                });
            });

            if (err == false) {
                storeAjax($(this), 'post');
            } else {
                $(element).html('ثبت');
            }
        });


        $(document).on('click', '.addForm', function () {
            var formHtml = $(this).closest('div.festivalDiscounts').clone();
            formHtml.counter('محصول');
            $(this).closest('div.festivalDiscounts').after("<div class='row festivalDiscounts'>" + formHtml.html() + "</div>");
        });
        $(document).on('click', '.showProduct', function () {
            showData('{{asset('/product')}}');
            trClick = $(this);
        });

    </script>

@append







