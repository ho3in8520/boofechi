@extends('master_page')
@section('title_browser') اطلاعات پایه @endsection
@section('main_content')
    <section class="basic-elements">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header">اطلاعات پایه</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">

                    </div>
                    <div class="card-body">
                        <div class="px-3">
                            <form class="form" method="post" action="" enctype="multipart/form-data">
                                @if(!empty($update_wiz))
                                    @method('PATCH')
                                @endif
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-4">
                                            <div class="col-md-10 col-12">
                                                <label for="text">نوع پنل</label><span class="required red">*</span>
                                                <fieldset class="form-group position-relative">
                                                    <select class="form-control" name="wizard[type]"
                                                            id="DefaultSelect">
                                                        {!! customForeach($bas_info,'bas_id','bas_value',$type) !!}
                                                    </select>
                                                </fieldset>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-md-5 col-5">
                                                    <div class="form-group">
                                                        <label for="text">آدرس url</label><span
                                                                class="required red">*</span>
                                                        <input type="text" class="form-control" name="wizard[address]"
                                                               value="{{formValue('wizard.address',$update_wiz,'pw_form_address')}}">
                                                    </div>
                                                </div>
                                                <div class="col-md-5 col-5">
                                                    <div class="form-group" style="margin-top: 35px">
                                                        <label for="checkbox">فعال/غیرفعال</label>
                                                        <input type="checkbox" name="wizard[status]" id="status"
                                                               value="1" class="switchery form-control" data-size="xs"
                                                               data-switchery="true"
                                                               @if(!empty($update_wiz) && $update_wiz->pw_status==true)
                                                               checked="checked"
                                                               @elseif(!empty($update_wiz) && $update_wiz->pw_status==false)
                                                               @else
                                                               checked="checked"
                                                               @endif
                                                               style="display: none;">
                                                        <input id='testNameHidden' type='hidden' value='0'
                                                               name='wizard[status]'>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <button id="createBaiscInfo" name="create_basic_info"
                                                    class="btn btn-success btn-loading" type="button">
                                                ثبت
                                            </button>
                                        </div>
                                    </div>
                                    <div class="row form-actions">
                                        <div class="col-12 table-responsive">
                                            <div class="summary"></div>
                                            <table class="table table-striped table-bordered sourced-data dataTable">
                                                <thead>
                                                <tr>
                                                    <th>ردیف</th>
                                                    <th>نوع پنل</th>
                                                    <th>آدرس</th>
                                                    <th>وضعیت</th>
                                                    <th>عملیات</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                    @foreach($wizards as $row)
                                        <?php
                                        if ($row->pw_status==true){
                                            $status='فعال';
                                        }elseif($row->pw_status==false){
                                            $status='غیرفعال';
                                        }
                                        ?>
                                                <tr>
                                                    <td>{{index($wizards ,$loop)}}</td>
                                                    <td>{{$row->bas_value}}</td>
                                                    <td>{{$row->pw_form_address}}</td>
                                                    <td style="text-align: center">{{$status}}</td>

                                                    <td style="text-align: center">
                                                        <a class="" href="{{asset('wizard-setting/'.$row->pw_id)}}" data-original-title="" title="">
                                                            <i class="fa fa-pencil"></i>
                                                        </a>
                                                        <a class="primary" data-action="{{asset('wizard-setting/'.$row->pw_id)}}"
                                                           data-confirm="اطمینان از حذف این مورد دارید؟"
                                                           data-method="delete">
                                                            <i class="fa fa-trash"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                        @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        {{ $wizards->appends(request()->query())->links() }}
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
@section('script')
    <script src="{{ asset('/js/functions/storeAjax.js') }}"></script>
    <script>

        $(document).ready(function () {
            $("#createBaiscInfo").click(function (e) {
                if(document.getElementById("status").checked) {
                    document.getElementById('testNameHidden').disabled = true;
                }
                e.preventDefault();
                storeAjax($(this), 'post');
            });
        });
    </script>
@append