@extends('master_page')
@section('title_browser') ثبت نام ویزیتور @endsection
<?php
$states = \App\City::where('c_parent_id', 0)->get()->toArray();
if (!empty($visitor) && $visitor->prsn_id != "") {
    $cities = \App\City::where('c_parent_id', $visitor->coll_state_id)->get();
} else if (old('collections.coll_state_id') != "")
    $cities = \App\City::whereCParentId(old('collections.coll_state_id'))->get();
else
    $cities = [];
?>
@section('style')


@endsection
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header">اطلاعات کسب و کار</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title-wrap bar-success">

                        </div>
                        <p class="mb-0"></p>
                    </div>
                    <div class="card-body">
                        <div class="card-block">
                            <form method="post" action="{{asset('/visitor/business-form')}}">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <span class="text-danger">*</span>
                                            <label>شغل</label>
                                            <input type="text" class="form-control " name="visitor[job]"
                                                   value="{{formValue('visitor.job',$visitor,'prsn_job')}}"
                                                   autocomplete="off">

                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <span class="text-danger">*</span>
                                            <label>استان</label>
                                            <select class="form-control state" name="visitor[state]">
                                                {!! customForeach($states,'c_id','c_name',old('collections.coll_state_id',empty($visitor) ? '' : $visitor->coll_state_id)) !!}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <span class="text-danger">*</span>
                                            <label>شهر</label>
                                            <select class="form-control city" name="visitor[city]">
                                                @if(empty($visitor))
                                                    <option value="">انتخاب کنید...</option>
                                                @else
                                                    {!! customForeach($cities,'c_id','c_name',old('collections.coll_city_id',empty($visitor) ? '' : $visitor->coll_city_id)) !!}
                                                @endif
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <span class="text-danger">*</span>
                                            <label>آدرس محل کار</label>
                                            <span class="ft-help-circle" data-toggle="tooltip" data-placement="top"
                                                  title="اجناس خریداری شده به آدرس ذکر شده ارسال می شود">
                                            </span>
                                            <textarea class="form-control "
                                                      name="visitor[address]">{{formValue('visitor.address',$visitor,'coll_address')}}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <span class="text-danger">*</span>
                                        <label>صنف</label>
                                        <select class="form-control select2" name="visitor_class[]" multiple
                                                style="">
                                            {!! customForeach($typeCollections,'bas_id','bas_value',$old_senfs) !!}

                                        </select>
                                        <input type="hidden" name="typeClass" value="1">
                                        @error('typeClass')
                                        <div class="help-block text-danger">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                @if(!request()->ajax())
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label></label>
                                                <button class="btn btn-success btn-loading" id="RegisterShopkeeper">
                                                    ثبت
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('/js/functions/storeAjax.js') }}"></script>
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });

        $(document).ready(function () {
            $("#RegisterShopkeeper").click(function (e) {
                e.preventDefault();
                storeAjax($(this), 'POST', 'ثبت', false, 1);
            });
        });
    </script>
@append