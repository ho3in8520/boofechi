@extends('master_page')
<?php
$title = "جزئیات فاکتور";
?>
@section('title_browser',$title)
@section('style')
    <link rel="stylesheet" type="text/css" href="{{asset("/theme/vendors/css/sweetalert2.min.css")}}">

    <style>
        .btn-sm, .input-sm {
            height: 30px !important;
        }

        .input-sm {
            padding-right: 5px;
            width: 40px !important;
        }

    </style>
@endsection
@section('main_content')
    <section id="basic-form-layouts" class="basket-items">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header"><?=$title?></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="container">
                            <table class="table">
                                <tr>
                                    <th colspan="5"><h4 class="pull-right">مشخصات فروشنده</h4>
                                    </th>
                                </tr>
                                <tr>
                                    <td>نام شرکت:</td>
                                    <td>{{$seller->prsn_job}}</td>
                                    <td>تلفن:</td>
                                    <td>{{$seller->prsn_phone1}}</td>
                                    <td rowspan="2" style="width: 100px;height: 100px"><img class="rounded-circle"
                                                                                            style="width: 100px;height: 100px"
                                                                                            src="{{getFile(($logo) ? $logo->ur_path: $logo=null,'logo')}}">
                                    </td>
                                </tr>
                                <tr>
                                    <td>آدرس:</td>
                                    <td colspan="3">{{$seller->coll_address}}</td>
                                </tr>
                                <tr>
                                    <td>زمان ثبت فاکتور:</td>
                                    <td colspan="2">{{jdate_from_gregorian($master->sfm_created_at)}}</td>
                                </tr>
                                <tr>
                                    <td>توضیحات:</td>
                                    <td colspan="3">{{$master->sfm_description}}</td>
                                </tr>
                            </table>
                        </div>
                        <p class="mb-0"></p>
                    </div>
                    <form method="post">
                        @csrf
                        <div class="card-body">
                            <div class="card-block">
                                <table style="text-align: center !important"
                                       class="table table-striped table-inverse table-bordered table-hover">
                                    <thead>
                                    <th>ردیف</th>
                                    <th>عنوان سرویس</th>
                                    <th>قیمت کل</th>
                                    </thead>
                                    <tbody>
                                    @php
                                        $sum = 0;
                                    @endphp
                                    @foreach($detail as $row)
                                        @php
                                            $sum += $row->sfd_request_amount;
                                        @endphp
                                        <tr>
                                            <td>{{$loop->index+1}}</td>
                                            <td><b>{{$row->sfd_request_title}}</b></td>
                                            <td><input type="hidden" class="amount"
                                                       value="{{$row->sfd_request_amount}}">{{number_format(WithoutZeros($row->sfd_request_amount))}}
                                            </td>

                                        </tr>
                                    @endforeach
                                    <?php
                                    ?>
                                    <tr>
                                        <td colspan="1"></td>
                                        <td>جمع کل</td>
                                        <td id="totalAmount">0</td>
                                    </tr>

                                    <tr>
                                        <td colspan="1"></td>
                                        <td>تخفیف</td>
                                        <td>{{number_format(WithoutZeros($master->sfm_discount))}}</td>
                                    </tr>
                                    <tr class="success">
                                        <td colspan="1"></td>
                                        <td>قابل پرداخت</td>
                                        <td>{{number_format(WithoutZeros($master->sfm_amount-$master->sfm_discount))}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                                @if($master->sfm_is_payed==0)
                                    @if($master->sfm_amount-$master->sfm_discount > user('amount'))
                                        <h6 style="color: red">مبلغ فاکتور بیشتر از موجودی کیف پول شما است لطفا کیف پول
                                            خود را شارژ کنید</h6>
                                        <button type="button" class="btn btn-info wallet"
                                                data-amount="{{$master->sfm_amount-$master->sfm_discount-user('amount')}}">
                                            شارژ کیف پول
                                        </button>
                                    @else
                                        <input type="hidden" name="factor_id" value="{{$id}}">
                                        <button type="button" class="btn btn-info payment">
                                            پرداخت فاکتور
                                        </button>
                                    @endif
                                @else
                                    <div class="btn btn-success">
                                        پرداخت شده
                                    </div>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{asset("/theme/vendors/js/sweetalert2.min.js")}}"></script>
    <script src="{{asset('/js/functions/storeAjax.js')}}"></script>
    <script>
        $(document).ready(function () {
            $(document).on('click', '.payment', function () {
            storeAjax($(this), 'POST', 'ثبت', false, 0);
        });
        });

    </script>
@endsection
