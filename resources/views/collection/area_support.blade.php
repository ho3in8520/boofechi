@extends('master_page')
@section('title_browser') {{ (empty($category)) ? 'نقاط تحت پوشش' : 'ویرایش نقاط تحت پوشش' }} @endsection
<?php
$states = getAreaSupport(1);
$cities = getAreaSupport(2, null, null, request('state', null));
?>
@section('main_content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header">{{ (empty($category)) ? 'نقاط تحت پوشش' : 'ویرایش نقاط تحت پوشش' }}</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <form action='' method='POST'>
                                {{ csrf_field() }}
                                {!! (!empty($category)) ? "<input name='_method' type='hidden' value='PATCH'>" : ''  !!}
                                <div class='row'>
                                    @include('sample.sample_form_area_support')
                                </div>
                                <div class='row'>
                                    <div class='col-md-12'>
                                        <div class='form-group'>
                                            @if(isset($url))
                                            <a href="{{ asset($url) }}" class="btn btn-danger">بازگشت</a>
                                        @endif
                                            <button type="button" id="saveAreaSupports"
                                                    class='btn btn-success btn-loading'>ثبت
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{asset('js/functions/storeAjax.js')}}"></script>
    <script>
        $(document).ready(function () {
            $("#saveAreaSupports").click(function (e) {
                e.preventDefault();
                storeAjax($(this), 'POST', captionButton = "ثبت", progress = false, reloadPage = false, TimeOutActionNextStore = 0,false);
            });
        });
    </script>
@append