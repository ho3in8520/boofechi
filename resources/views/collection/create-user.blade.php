@extends('master_page')
<?php
$title = "ثبت کاربر جدید";
if (isset($old_user))
    $title = "ویرایش کاربر";
?>
@section('title_browser')<?=$title?> @endsection

@section('main_content')

    <section class="basic-elements">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header"><?=$title?></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title-wrap bar-success">
                            <h4 class="card-title mb-0">مشخصات کاربر</h4>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="px-3">

                            <form class="form" method="post" action="" enctype="multipart/form-data">
                                @if(isset($old_user))
                                    @method('PATCH')
                                @endif
                                @include("sample.sample_collection_create_user")
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@append