
<!DOCTYPE html>
<html lang="en">

<head>
    <title> به سایت بوفه چی خوش آمدید</title>
    <!-- for-mobile-apps -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Digital Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />

    <script>
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>

    <!-- css files -->
    <link href="{{asset("theme-test/css/bootstrap.css")}}" rel='stylesheet' type='text/css' /><!-- bootstrap css -->
    <link href="{{asset("theme-test/css/style.css")}}" rel='stylesheet' type='text/css' /><!-- custom css -->
    <link href="{{asset("theme-test/css/font-awesome.min.css")}}" rel="stylesheet"><!-- fontawesome css -->
    <link href="{{asset("theme-test/fonts/vazirFont.css")}}" rel="stylesheet"><!-- fontawesome css -->
    <!-- //css files -->
<style>
    .enamad {
        background-color: whitesmoke;
        position: absolute;
    }
</style>
    <!-- google fonts -->
    <link
        href="//fonts.googleapis.com/css?family=Cabin:400,400i,500,500i,600,600i,700,700i&amp;subset=latin-ext,vietnamese"
        rel="stylesheet">
    <!-- //google fonts -->

</head>

<body dir="rtl">


<!-- //header -->
<header class="py-4">
    <div class="container">
        <div id="logo">
            <h1> <a href="index.html"><span class="fa fa-cloud" aria-hidden="true"></span> boofechi</a></h1>
        </div>
        <!-- nav -->
        <nav class="d-lg-flex">

            <label for="drop" class="toggle"><span class="fa fa-bars" aria-hidden="true"></span></label>
            <input type="checkbox" id="drop" />
            <ul class="menu mt-2 ml-auto" dir="ltr">
                <li class=""><a href="index">صفحه اصلی</a></li>
                <li class=""><a href="index#about">درباره ما</a></li>

                <li class=""><a href="rules">قوانین</a></li>
                <li class=""><a href="complaint">ثبت شکایات</a></li>
                <li class=""><a href="contact">تماس باما</a></li>
            </ul>
            <div class="login-icon ml-lg-2">
                <a class="user" href="{{route('login')}}"> ورود</a>
            </div>
        </nav>
        <div class="clear"></div>
        <!-- //nav -->
    </div>
</header>
<!-- //header -->

<!-- banner -->
<div class="banner" id="home">
    <div class="container">
        <div class="row banner-text">
            <div class="slider-info col-lg-6">
                <div class="banner-info-grid mt-lg-5 text-right">
                    <h2>به وب سایت بوفه چی خوش آمدید</h2>
                    <p>بوفه چی با واسطه بودن بین شرکت های تولیدی/عمده فروشان و خرده فروش ها به صورت انلاین به آنها کمک میکند تا در معاملات روزانه خود با یکدیگر در ارتباط باشند </p>
                </div>
                <a class="btn mr-2 text-capitalize" href="{{route('login')}}">ورود به سامانه </a>
                <a class="btn text-capitalize" href="{{route('register')}}">ثبت نام  </a>
            </div>
            <div class="col-lg-6 col-md-8 mt-lg-0 mt-sm-5 mt-3 banner-image text-lg-center">
                <img src="{{asset("theme-test/images/bannerpng.png")}}" alt="" class="img-fluid" />
            </div>
        </div>
    </div>
</div>
<!-- //banner -->

<!-- about -->
<section class="about py-5" id="about">
    <div class="container py-lg-5 py-sm-3">
        <div class="row">
            <div class="col-lg-3 about-left">
                <h3 class="heading mb-lg-5 mb-4">درباره ما</h3>
            </div>
            <div class="col-lg-5 col-md-7 about-text">
                <h3>به وب سایت بوفه چی خوش آمدید</h3>
                <p class="mt-4">سامانه بوفه چی توسط شرکت فن آوران هیوا پارتاک طراحی شده است</p>
                <p class="mt-3">در سایت بوفه چی شرکت ها و تولیدکنندگان میتوانند محصولات خود را به خرده فروشان به صورت انلاین ارائه نمایند</p>
                <p class="mt-2">در این سامانه ما قصد داریم با واسطه بین شرکت های تولیدی و عمده فروشان به صورت انلاین و بدونه هیچ دغدغه  اجناس خودر را به خرده فروشان بفروشند</p>
            </div>
            <div class="col-lg-4 col-md-5 about-img">
                <img src="{{asset("theme-test/images/1.png")}}" alt="" class="img-fluid" />
            </div>
        </div>
    </div>
</section>
<!-- //about -->

<!-- why choose us -->
<section class="choose py-5" id="services">
    <div class="container py-md-3">
        <h3 class="heading mb-5"> آنچه ما ارائه می دهیم</h3>
        <div class="feature-grids row">
            <div class="col-lg-3 col-sm-6">
                <div class="f1 icon1 p-4">
                    <div class="icon">
                        <span class="fa fa-bandcamp"></span>
                    </div>
                    <h3 class="my-3">‍پنل کارخانه</h3>
                    <p>کارخانه و تولیدی های میتوانند با استفاده به پنل که در اختیار دارند اجناس خودرابه نماینده های فروش خود در سراسر کشور ارا|ئه نماید.</p>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 mt-sm-0 mt-4">
                <div class="f1 icon2 p-4">
                    <div class="icon">
                        <span class="fa fa-codepen"></span>
                    </div>
                    <h3 class="my-3">شرکت های پخش</h3>
                    <p>شرکت های پخش و یا نماینده های تولید کننده میتوانند با استفاده از پنلی که در اختیار دارند اجناس خود را به خرده فروشان ارائه دهند</p>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 mt-lg-0 mt-4">
                <div class="f1 icon3 p-4">
                    <div class="icon">
                        <span class="fa fa-btc"></span>
                    </div>
                    <h3 class="my-3">ویزیتور</h3>
                    <p>افرادی که مایل به همکاری و درامدزایی هستند میتوانند بصورت ویزیتور ثبت نام کرده و خرده فروشان را زیر مجموعه خود قرار دهند و از خرید انها پورسانت دریافت کنند</p>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 mt-lg-0 mt-4">
                <div class="f1 icon4 p-4">
                    <div class="icon">
                        <span class="fa fa-cloud"></span>
                    </div>
                    <h3 class="my-3">خرده فروش</h3>
                    <p>خرده فروش ها میتوانند بدونه هیچ مشکل و دغدغه ای برای سفارش ، با استفاده از پنلی که در اختیار دارند اجناس مورد نظر خود را از شرکت های داخل سامانه سفارش دهند</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- //why choose us -->

<!-- Offered Services -->
<section class="process py-5" id="process">
    <div class="container py-md-3">
        <h3 class="heading mb-5">مراحل فعالیت سامانه</h3>

        <h3 class="heading mb-5"></h3>
        <div class="row process-grids">
            <div class="col-lg-3 col-md-6 my-lg-4 w3pvt-ab position-relative">
                <div class="">
                    <img src="{{asset("theme-test/images/1.png")}}" alt="" class="img-fluid">
                </div>
                <h4 class="feed-title my-md-3 mb-3">کارخانه جات</h4>
                <p>کارخانه ها و شرکت های تولید کننده جهت ثبت نام میتوانند با استفاده از فرم ثبت نام اولیه اطلاعات کسب و کار خود را ارسال نمایند و منتظر تماس از طرف بوفه چی باشند </p>
            </div>
            <div class="col-lg-3 col-md-6 my-lg-4 mt-md-0 mt-4 w3pvt-ab position-relative">
                <div class="">
                    <img src="{{asset("theme-test/images/2.png")}}" alt="" class="img-fluid">
                </div>
                <h4 class="feed-title my-md-3 mb-3">شرکت ها و نماینده های پخش</h4>
                <p> شرکت ها و نماینده های فروش جهت ثبت نام میتوانند با استفاده از فرم ثبت نام اولیه اطلاعات کسب و کار خود را ارسال نمایند و منتظر تماس از طرف بوفه چی باشند </p>
            </div>
            <div class="col-lg-3 col-md-6 my-lg-4 mt-sm-5 mt-4 w3pvt-ab position-relative">
                <div class="">
                    <img src="{{asset("theme-test/images/1.png")}}" alt="" class="img-fluid">
                </div>
                <h4 class="feed-title my-md-3 mb-3">ویزیتور</h4>
                <p>ویزیتور ها با زیرمجموعه گرفتن و فروش اجناس میتوانند طبق درصد تعیین شده توسط سامانه ، مستقیما از بوفه چی پورسانت دریافت نمایند </p>
            </div>
            <div class="col-lg-3 col-md-6 mt-lg-4 mt-sm-5 mt-4 w3pvt-ab">
                <div class="">
                    <img src="{{asset("theme-test/images/2.png")}}" alt="" class="img-fluid">
                </div>
                <h4 class="feed-title my-md-3 mb-3">خرده فروشان</h4>
                <p>مغازه داران و خرده فروشان میتوانند از این پس بدون دغدغه و مشکلات کار با ویزیتور ها و زمان و ثبت سفارشات اشتباه توسط ویزیتورها  مستقیما درخواست و سفارش خود را توسط بوفه چی به مدیران فروش شرکت ها و پخش ها  اعلام نمایند</p>
            </div>
        </div>
    </div>
</section>
<!-- Offered Services -->

<!-- video -->
<section class="video py-5" id="video">
    <div class="container">
        <div class="inner-sec-w3ls py-lg-5 py-3">
            <div class="row">
                <div class="col-lg-6 video-right px-lg-5 px-2">
                    <h3 class="tittle-w3"> وب سایت بوفه چی</h3>
                    <p class="mt-4">
                        بوفه چی با واسطه بودن بین شرکت های تولیدی/عمده فروشان و خرده فروش ها به صورت انلاین به آنها کمک میکند تا در معاملات روزانه خود با یکدیگر در ارتباط باشند
                    </p>

                </div>
                <div class="col-lg-6 video-img mt-lg-0 mt-4">
                    <img src="{{asset("theme-test/images/banner.png")}}" alt="" class="img-fluid">
                </div>
            </div>
        </div>
    </div>
</section>

<footer class="footer-emp-w3ls py-5" dir="rtl">
    <div class="container pt-lg-3">
        <div class="row footer-top">
            <div class="col-lg-4 col-sm-6 footer-grid-wthree">
                <h4 class="footer-title text-uppercase mb-4">ما کی هستیم</h4>
                <div class="contact-info">
                    <p>
                        بوفه چی در مهر ماه 1396 استارت برنامه نویسی خورد که با فعالیت های شبانه روزی همکاران توانستیم در آذر ماه 1398 شروع به کار کند واولین وب سایت بازاریابی انلاین میباشد که توانستیم پل ارتباطی بین تولید کنندگان و خرده فروشان باشیم.
                    </p>
                    <h4 class="mt-3">مورد اعتماد بیش از 1000+ نفر</h4>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 footer-grid-wthree mt-lg-0 mt-sm-5 mt-4">
                <h3 class="footer-title text-uppercase mb-4">تماس باما</h3>
                <div class="contact-info">
                    <div class="footer-style-w3ls">
                        <h4 class="mb-2"> <span class="fa mr-1 fa-map-marker"></span> آدرس</h4>
                        <p>اصفهان-تیران و کرون-شهرک الغدیر</p>
                    </div>
                    <div class="footer-style-w3ls my-3">
                        <h4 class="mb-2"><span class="fa mr-1 fa-phone"></span> شماره تماس</h4>
                        <p>03142345208</p>
                    </div>
                    <div class="footer-style-w3ls">
                        <h4 class="mb-2"><span class="fa mr-1 fa-envelope-open"></span> ایمیل</h4>
                        <p><a href="mailto:info@boofechi.com">info@boofechi.com</a></p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 footer-grid-wthree mt-lg-0 mt-sm-5 mt-4">
                <h3 class="footer-title text-uppercase mb-4">مجوز ها</h3>
                <div class="enamad">
                <a target="_blank" href="https://trustseal.enamad.ir/?id=138270&amp;Code=pbW8BpnnEAVPRDvsBkA2"><img src="https://Trustseal.eNamad.ir/logo.aspx?id=138270&amp;Code=pbW8BpnnEAVPRDvsBkA2" alt="" style="cursor:pointer" id="pbW8BpnnEAVPRDvsBkA2"></a>
                </div>
            </div>

        </div>
    </div>
</footer>
<!-- //footer -->

<!-- copyright -->
<div class="copy-right-top border-top">
    <p class="copy-right text-center py-4">طراحی توسط
        <a href="http://hivapartak.ir/"> شرکت فن آوران هیوا ‍پارتک </a>
    </p>
</div>
<!-- //copyright -->

<!-- move top -->
<div class="move-top text-right">
    <a href="#home" class="move-top">
        <span class="fa fa-angle-up  mb-3" aria-hidden="true"></span>
    </a>
</div>
<!-- move top -->

<!-- popup -->
<div id="popup1" class="popup-effect">
    <div class="popup">
        <img src="images/banner.png" alt="Popup Image" class="img-fluid" />
        <p class="mt-4 ">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
            laudantium, totam rem aperiam, eaque ipsa quae ab illo quasi architecto beatae vitae dicta
            sunt explicabo.</p>
        <a class="close" href="#">&times;</a>
    </div>
</div>
<!-- //popup -->

<!-- popup -->
<div id="popup2" class="popup-effect">
    <div class="popup">
        <iframe src="https://player.vimeo.com/video/188673754"></iframe>
        <p class="mt-4 ">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
            laudantium, totam rem aperiam, eaque ipsa quae ab illo quasi architecto beatae vitae dicta
            sunt explicabo.</p>
        <a class="close" href="#">&times;</a>
    </div>
</div>
<!-- //popup -->



</body>

</html>
