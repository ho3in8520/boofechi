@extends('master_page')
@section('title_browser')سیستم@endsection
@section('style')
    <link rel="stylesheet" type="text/css" href="{{asset('theme/vendors/css/dragula.min.css')}}">
    <style>
        .slider {
            border-radius: 10px;
        }
    </style>
@endsection
@section('main_content')
    @if(!empty($msg_complet))
        {!! $msg_complet !!}
    @endif
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="content-header">پیغام سیستم</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                    </div>
                    <div class="card-body">
                        <div class="card-block">
                            <div class="row">
                                <div class="card-body" style="margin-bottom:0px">
                                    <div class="alert alert-{{$status}} alert-dismissible fade show text-white"
                                         role="alert">
                                        {{$message}}
                                        <br>
                                        <?php
                                        if($status == "success")
                                        echo "شماره پیگیری پرداخت شما: ". $gateway->transact_retrival_ref_no;
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection