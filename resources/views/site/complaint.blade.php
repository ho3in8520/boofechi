<!--
author: W3layouts
author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="en">
<head>
    <title>ثبت شکایات</title>
    <!-- for-mobile-apps -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Digital Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />

    <script>
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>

    <!-- css files -->
    <!-- css files -->
    <link href="{{asset("theme-test/css/bootstrap.css")}}" rel='stylesheet' type='text/css' /><!-- bootstrap css -->
    <link href="{{asset("theme-test/css/style.css")}}" rel='stylesheet' type='text/css' /><!-- custom css -->
    <link href="{{asset("theme-test/css/font-awesome.min.css")}}" rel="stylesheet"><!-- fontawesome css -->
    <link href="{{asset("theme-test/fonts/vazirFont.css")}}" rel="stylesheet"><!-- fontawesome css -->
    <!-- //css files -->
    <!-- //css files -->

    <!-- google fonts -->
    <link href="//fonts.googleapis.com/css?family=Cabin:400,400i,500,500i,600,600i,700,700i&amp;subset=latin-ext,vietnamese" rel="stylesheet">
    <!-- //google fonts -->

</head>
<body dir="rtl">

<!-- //header -->
<header class="py-4">
    <div class="container">
        <div id="logo">
            <h1> <a href="index.html"><span class="fa fa-cloud" aria-hidden="true"></span> boofechi</a></h1>
        </div>
        <!-- nav -->
        <nav class="d-lg-flex">

            <label for="drop" class="toggle"><span class="fa fa-bars" aria-hidden="true"></span></label>
            <input type="checkbox" id="drop" />
            <ul class="menu mt-2 ml-auto" dir="ltr">
                <li class=""><a href="index">صفحه اصلی</a></li>
                <li class=""><a href="index#about">درباره ما</a></li>

                <li class=""><a href="rules">قوانین</a></li>
                <li class=""><a href="complaint">ثبت شکایات</a></li>
                <li class=""><a href="contact">تماس باما</a></li>
            </ul>
            <div class="login-icon ml-lg-2">
                <a class="user" href="{{route('login')}}"> ورود</a>
            </div>
        </nav>
        <div class="clear"></div>
        <!-- //nav -->
    </div>
</header>
<!-- //header -->

<!-- banner -->
<div class="banner inner-banner" id="home">
    <div class="container">

    </div>
</div>
<!-- //banner -->

<!-- Contact page -->
<section class="contact py-5 my-lg-5">
    <div class="container">
        <h2 class="heading mb-sm-5 mb-4">ثبت شکایات</h2>
        <div class="row contact_information">


            <div class="col-lg-6 col-md-6">
                <div class="mt-5 information">
                    <h4 class="text-uppercase mb-4"><span class="fa fa-comments"></span> ارتباطات </h4>
                    <p class="mb-3 text-capitalize">برای ثبت شکایات خود لطفا به ما ایمیل بزنید<a href="mailto:info@boofechi.com">info@boofechi.com</a></p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- //Contact page -->

<!-- footer -->
<footer class="footer-emp-w3ls py-5" dir="rtl">
    <div class="container pt-lg-3">
        <div class="row footer-top">
            <div class="col-lg-4 col-sm-6 footer-grid-wthree">
                <h4 class="footer-title text-uppercase mb-4">ما کی هستیم</h4>
                <div class="contact-info">
                    <p>
                        بوفه چی در مهر ماه 1396 استارت برنامه نویسی خورد که با فعالیت های شبانه روزی همکاران توانستیم در آذر ماه 1398 شروع به کار کند واولین وب سایت بازاریابی انلاین میباشد که توانستیم پل ارتباطی بین تولید کنندگان و خرده فروشان باشیم.
                    </p>
                    <h4 class="mt-3">مورد اعتماد بیش از 1000+ نفر</h4>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 footer-grid-wthree mt-lg-0 mt-sm-5 mt-4">
                <h3 class="footer-title text-uppercase mb-4">تماس باما</h3>
                <div class="contact-info">
                    <div class="footer-style-w3ls">
                        <h4 class="mb-2"> <span class="fa mr-1 fa-map-marker"></span> آدرس</h4>
                        <p>اصفهان-تیران و کرون-شهرک الغدیر</p>
                    </div>
                    <div class="footer-style-w3ls my-3">
                        <h4 class="mb-2"><span class="fa mr-1 fa-phone"></span> شماره تماس</h4>
                        <p>03142345208</p>
                    </div>
                    <div class="footer-style-w3ls">
                        <h4 class="mb-2"><span class="fa mr-1 fa-envelope-open"></span> ایمیل</h4>
                        <p><a href="mailto:info@boofechi.com">info@boofechi.com</a></p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</footer>
<!-- //footer -->
<!-- copyright -->
<div class="copy-right-top border-top">
    <p class="copy-right text-center py-4">طراحی توسط
        <a href="http://hivapartak.ir/"> شرکت فن آوران هیوا ‍پارتک </a>
    </p>
</div>
<!-- //copyright -->

<!-- move top -->
<div class="move-top text-right">
    <a href="#home" class="move-top">
        <span class="fa fa-angle-up  mb-3" aria-hidden="true"></span>
    </a>
</div>
<!-- move top -->

</body>
</html>
