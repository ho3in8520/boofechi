<!DOCTYPE html>
<html lang="en" class="loading">
<head>
    <title>@yield('title_browser')</title>
    <meta name="csrf" content="{{ csrf_token() }}"/>
    @include('layout.header')
    @yield('style')
</head>
<body data-col="2-columns" class=" 2-columns ">
@include('sweet::alert')
<div class="mdl"></div>
<div class="wrapper">
    <div data-active-color="white" data-background-color="{{getPanelColor()}}"
         data-image="{{ asset(getPanelBg()) }}"
         class="app-sidebar">
        <div class="sidebar-header">
            <div class="logo clearfix"><a href="" class="logo-text" style="text-align: center">
                    <div class="logo-img"></div><span class="text" style="margin-right: -20px">{{getPanelName()}}<br><small>({{user('code')}})</small></span></a><a id="sidebarToggle" href="javascript:;" class="nav-toggle d-none d-sm-none d-md-none d-lg-block"><i data-toggle="expanded" class="ft-disc toggle-icon"></i></a><a id="sidebarClose" href="javascript:;" class="nav-close d-block d-md-block d-lg-none d-xl-none"><i class="ft-circle"></i></a></div>
        </div>
        <div class="sidebar-content">
            <div class="nav-container">
                @include('layout.right_menu')
            </div>
        </div>
        <div class="sidebar-background"></div>
    </div>

    <nav class="navbar navbar-expand-lg navbar-light bg-faded">
        <div class="container-fluid">
            <div class="navbar-header">
                <div class="navbar-header">
                    <button type="button" data-toggle="collapse" class="navbar-toggle d-lg-none float-right"><span
                                class="sr-only">تغییر ناوبری </span><span class="icon-bar"></span><span
                                class="icon-bar"></span><span class="icon-bar"></span></button>
                    <span class="d-lg-none navbar-right navbar-collapse-toggle"><a class="open-navbar-container"><i
                                    class="ft-more-vertical"></i></a></span><a id="navbar-fullscreen"
                                                                               href="javascript:;"
                                                                               class="ml-2 display-inline-block apptogglefullscreen "><i
                                class="ft-maximize blue-grey darken-4 toggleClass"></i>
                        <p class="d-none">تمام صفحه</p></a>
                    <div class="dropdown mr-2 display-inline-block">
                        <label class="">{!! getPurchasingUserId() !!}</label>
                    </div>
                </div>
            </div>
            @include('layout.top_menu')
        </div>
    </nav>

    <div class="main-panel">
        <div class="main-content">
            <div class="content-wrapper">
                <div class="container-fluid">
                    <?php $tools = new \App\Component\Tools(); ?>
                    <div class="ajaxMessage">
                        {!! $tools->session_message('success','Success') !!}
                        {!! $tools->session_message('error','Error') !!}
                        {!! $tools->session_message('warning','Warning') !!}
                    </div>
                    @yield('main_content')
                </div>
            </div>
        </div>
        <footer class="footer footer-static footer-light">

        </footer>

    </div>
</div>

<script>
    var counter = 0;
</script>
@include('layout.setting_theme')
@include('layout.footer')
<script>
    $(document).ready(function () {
        APP_URL = {!! json_encode(url('/')) !!}
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf"]').attr('content')
            }
        });
        $(document).on("change", "select.state", function () {
            var id = $(this).val();
            @if(Request::route()->getName()=="area-support-company")
                $.get("{{route('get-cities-edit-company')}}", {id: id}, function (data) {
                    $("select.city").html(JSON.parse(data));
                })

            @else
            $.get("{{route('get-cities')}}", {id: id}, function (data) {
                $("select.city").html(JSON.parse(data));
            })@endif
        });
        $(document).on("click", "table.table-eshants tbody tr", function (e) {
            e.preventDefault();
            addToBasket($(this).data('href'), {id: $(this).data('id'), count: $(this).data('count')});
        });
    });
    function loadBasket(){
        $.get("{{asset('get-basket-top-menu')}}",function(data){
            var result = JSON.parse(data);
            if(result.status == 100) {
                $(".dropdown_basket").show();
                $(".basket_count").html(result.count);
                $(".table-basket-items-top-menu").html(result.basket);
            }
        });
    }
    function addToBasket(url, data, element) {
        var OH = "<i class='fa fa-shopping-basket'></i>";
        $.get(url, data, function (d) {
            var result = JSON.parse(d);
            if (result.status == 100) {
                toastr.success(result.msg, "موفق");
                if(result.count > 0) {
                    $(element).html(OH);
                    loadBasket();
                }
            }
            else {
                toastr.error(result.msg, "ناموفق");
            }
        });
    }
</script>
@yield('script')
</body>
</html>
