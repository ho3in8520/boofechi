@section('style')
    <style>
        .formDiscount {
            display: none;
        }

        #deleteImage {
            position: relative;
        }

        #deleteImageUpload {
            font-size: 35px;
            margin-left: 10px;
            z-index: 1;
            cursor: pointer;
            position: absolute;
        }

        .line-end {
            border-top: 1px solid #D1D5EA;
            padding: 10px 0;
            margin-top: 20px;
        }

    </style>
@endsection

<div class="form-body">
    <div class="row">
        <div class="col-md-2">
            <div class="form-group">
                <div id="deleteImage">
                </div>
                <img title="آپلود عکس" style="cursor:pointer;" id="blah"
                     src="{{ asset('/images/image.jpg') }}"
                     alt="your image" width="150px" height="150px">
                <input accept="image/jpeg" name="create-user[image]" type="file" id="imgInp" style="display: none">

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4 col-12">
            <div class="form-group">
                <label for="text">نام</label><span class="required red">*</span>
                <input type="text" class="form-control" name="create-user[name]"
                       value="{{formValue('name',@$old_person,'prsn_name')}}">
            </div>
        </div>
        <div class="col-md-4 col-12">
            <div class="form-group">
                <label for="text">نام خانوادگی</label><span
                        class="required red">*</span>
                <input type="text" class="form-control" name="create-user[last-name]"
                       value="{{formValue('last-name',@$old_person,'prsn_family')}}">
            </div>
        </div>
        <div class="col-md-4 col-12">
            <div class="form-group">
                <label for="text">کدملی</label><span class="required red">*</span>
                <input type="text" class="form-control"
                       name="create-user[national-code]"
                       value="{{formValue('national-code',@$old_person,'prsn_national_code')}}" maxlength="10">
            </div>
        </div>
        <div class="col-md-4 col-12">
            <div class="form-group">
                <label for="text">تلفن همراه</label><span class="required red">*</span>
                <input type="text" class="form-control" name="create-user[mobile]"
                       chars="mobile|11"
                       value="{{formValue('mobile',@$old_person,'prsn_mobile1')}}" maxlength="11">
            </div>
        </div>
        <div class="col-md-4 col-12">
            <div class="form-group">
                <label for="text">نام کاربری</label><span class="required red">*</span>
                @if(isset($old_user))
                    <label class="form-control" style="text-transform: none">{{formValue('username',@$old_user)}}</label>
                @else
                    <input type="text" class="form-control" name="create-user[username]">
                @endif
            </div>
        </div>
        <div class="col-md-4 col-12">
            <div class="form-group">
                <label for="text">رمزعبور</label>
                @if(!isset($old_user))
                    <span class="required red">*</span>
                @endif
                <input type="text" class="form-control" name="create-user[password]">
            </div>
        </div>
        <div class="col-md-4 col-12">
            <div class="form-group">
                <label for="text">نقش</label><span class="required red">*</span>
                <select
                        class='form-control'
                        name='create-user[role]'>
                    {!! customForeach($roles,'rol_id','rol_label',formValue('role',@$old_role,'ur_rol_id')) !!}
                </select>
            </div>
        </div>
    </div>
    <div class="line-end"></div>
    <div class="card-header">
        <div class="card-title-wrap bar-warning">
            <h4 class="card-title mb-0">نقاط تحت پوشش</h4>
        </div>
    </div>
    <div class="row">
        @include("sample.sample_form_area_support")
    </div>
    <div class="line-end"></div>
    <div class="card-header">
        <div class="card-title-wrap bar-primary">
            <h4 class="card-title mb-0">دسته بندی</h4>
        </div>
    </div>
    <div class="row">
        @include("sample.sample_category_select")
    </div>
    <div class="form-actions left">
        <button type="button" id="createuser" class="btn btn-success btn-loading">ثبت </button>
    </div>
</div>
@section('script')
    <script src="{{ asset('/js/functions/storeAjax.js') }}"></script>
    <script>
        function previewImage(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#imgInp").change(function () {
            previewImage(this);
            if ($(this).val() != "")
                $("#deleteImage").html("<i id='deleteImageUpload' title=\"حذف عکس\" class=\"fa fa-trash text-danger\"></i>")
        });

        $(document).on('click', '#blah', function () {
            $("#imgInp").click();
        });
        $(document).on('click', '#deleteImageUpload', function () {
            $("#imgInp").val('');
            $("#blah").attr('src', "{{ asset('/images/image.jpg') }}");
            $("#deleteImage").html("");
        });

        $(document).on('click', '#createuser', function (e) {
            storeAjax($(this), 'POST')

        });
    </script>
@append
