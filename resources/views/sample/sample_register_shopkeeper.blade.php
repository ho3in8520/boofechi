<?php
$states = \App\City::where('c_parent_id', 0)->get()->toArray();
if (!empty($oldForm) && $oldForm->prsn_id != "") {
    $cities = \App\City::where('c_parent_id', $oldForm->prsn_state_id)->get();
} else if (old('shop_keeper.state') != "")
    $cities = \App\City::whereCParentId(old('shop_keeper.state'))->get();
else
    $cities = [];
?>
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <span class="text-danger">*</span>
                                            <label>نام</label>
                                            <input type="text" class="form-control " name="shop_keeper[name]"
                                                   value="{{formValue('shop_keeper.name',$oldForm,'prsn_name')}}" autocomplete="off">

                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <span class="text-danger">*</span>
                                            <label>نام خانوادگی</label>
                                            <input type="text" class="form-control " name="shop_keeper[last_name]" value="{{formValue('shop_keeper.last_name',$oldForm,'prsn_family')}}"
                                                   autocomplete="off">

                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <span class="text-danger">*</span>
                                            <label>نام پدر</label>
                                            <input type="text" class="form-control " name="shop_keeper[name_father]"
                                                   value="{{formValue('shop_keeper.name_father',$oldForm,'prsn_father_name')}}" autocomplete="off">

                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <span class="text-danger">*</span>
                                            <label>تاریخ تولد</label>
                                            <input type="text" class="form-control datePicker" name="shop_keeper[date_of_birth]"
                                                   maxlength="10" value="{{formValue('shop_keeper.date_of_birth',$oldForm,'prsn_birthday')}}" autocomplete="off">


                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <span class="text-danger">*</span>
                                            <label>شماره ملی</label>
                                            <input type="text" class="form-control " name="shop_keeper[national_code]"
                                                   maxlength="10" value="{{formValue('shop_keeper.national_code',$oldForm,'prsn_national_code')}}" autocomplete="off">

                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <span class="text-danger">*</span>
                                            <label>کد پستی</label>
                                            <input type="text" class="form-control " name="shop_keeper[postal_code]"
                                                   maxlength="10" value="{{formValue('shop_keeper.postal_code',$oldForm,'prsn_post_code')}}" autocomplete="off">

                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <span class="text-danger">*</span>
                                            <label>استان</label>
                                            <select class="form-control state" name="shop_keeper[state]" >
                                                {!! customForeach($states,'c_id','c_name',old('shop_keeper.state',empty($oldForm) ? '' : $oldForm->prsn_state_id)) !!}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <span class="text-danger">*</span>
                                            <label>شهر</label>
                                            <select class="form-control city" name="shop_keeper[city]" >
                                                @if(empty($oldForm))
                                                    <option value="">انتخاب کنید...</option>
                                                @else
                                                    {!! customForeach($cities,'c_id','c_name',old('shop_keeper.city',empty($oldForm) ? '' : $oldForm->prsn_city_id)) !!}
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>کد معرف</label>
                                            <input type="text" class="form-control " name="shop_keeper[reagent_id]"
                                                   maxlength="10" value="{{formValue('shop_keeper.reagent_id',$oldForm,'reagent_id')}}" autocomplete="off">

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <span class="text-danger">*</span>
                                            <label>آدرس</label>
                                            <textarea class="form-control" name="shop_keeper[address]">{{formValue('shop_keeper.address',$oldForm,'prsn_address')}}</textarea>

                                        </div>
                                    </div>
                                </div>


