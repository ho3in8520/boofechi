<?php
$persons = new \App\Http\Requests\AgentRequest();
?>
<div class="row">
    <div class="col-md-2">
        <div class="form-group">
            {!! $persons->rules()['persons.prsn_name'][0] == "required" ? "<span class='text-danger'>*</span>" :  "" !!}
            <label>نام</label>
            <input type="text"
                   class="form-control {{ ($errors->has('persons.prsn_name') ? 'error-border' : '') }}"
                   name="persons[prsn_name]" maxlength=""
                   value="{{ formValue('persons.prsn_name',@$person,'prsn_name') }}">

            @if($errors->has('persons.prsn_name'))
                <div class="help-block text-danger">
                    {{ $errors->first('persons.prsn_name') }}
                </div>
            @endif
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            {!! $persons->rules()['persons.prsn_family'][0] == "required" ? "<span class='text-danger'>*</span>" :  "" !!}
            <label>نام خانوادگی</label>
            <input type="text"
                   class="form-control {{ ($errors->has('persons.prsn_family') ? 'error-border' : '') }}"
                   name="persons[prsn_family]" maxlength=""
                   value="{{ formValue('persons.prsn_family',@$person,'prsn_family') }}">

            @if($errors->has('persons.prsn_family'))
                <div class="help-block text-danger">
                    {{ $errors->first('persons.prsn_family') }}
                </div>
            @endif
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            {!! $persons->rules()['persons.prsn_national_code'][0] == "required" ? "<span class='text-danger'>*</span>" :  "" !!}
            <label>شماره ملی</label>
            <input type="text"
                   class="form-control {{ ($errors->has('persons.prsn_national_code') ? 'error-border' : '') }}"
                   name="persons[prsn_national_code]" maxlength=""
                   value="{{ formValue('persons.prsn_national_code',@$person,'prsn_national_code') }}">

            @if($errors->has('persons.prsn_national_code'))
                <div class="help-block text-danger">
                    {{ $errors->first('persons.prsn_national_code') }}
                </div>
            @endif
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            {!! $persons->rules()['persons.prsn_father_name'][0] == "required" ? "<span class='text-danger'>*</span>" :  "" !!}
            <label>نام پدر</label>
            <input type="text"
                   class="form-control {{ $errors->has('persons.prsn_father_name') ? 'error-border' : '' }}"
                   name="persons[prsn_father_name]" maxlength=""
                   value="{{ formValue('persons.prsn_father_name',@$person,'prsn_father_name') }}">

            @if($errors->has('persons.prsn_father_name'))
                <div class="help-block text-danger">
                    {{ $errors->first('persons.prsn_father_nameprsn_birthday') }}
                </div>
            @endif
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            {!! $persons->rules()['persons.prsn_birthday'][0] == "required" ? "<span class='text-danger'>*</span>" :  "" !!}
            <label>تاریخ تولد</label>
            <input type="text"
                   class="form-control datePicker{{ $errors->has('persons.prsn_birthday') ? 'error-border' : '' }}"
                   name="persons[prsn_birthday]" maxlength=""
                   value="{{ formValue('persons.prsn_birthday',@$person,'prsn_birthday') }}">

            @if($errors->has('persons.prsn_birthday'))
                <div class="help-block text-danger">
                    {{ $errors->first('persons.prsn_birthday') }}
                </div>
            @endif
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            {!! $persons->rules()['persons.prsn_certificate_code'][0] == "required" ? "<span class='text-danger'>*</span>" :  "" !!}
            <label>شماره شناسنامه</label>
            <input type="text"
                   class="form-control {{ $errors->has('persons.prsn_certificate_code') ? 'error-border' : '' }}"
                   name="persons[prsn_certificate_code]" maxlength=""
                   value="{{ formValue('persons.prsn_certificate_code',@$person,'prsn_certificate_code') }}">

            @if($errors->has('persons.prsn_certificate_code'))
                <div class="help-block text-danger">
                    {{ $errors->first('persons.prsn_certificate_code') }}
                </div>
            @endif
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            {!! $persons->rules()['persons.prsn_serial_certificate_code'][0] == "required" ? "<span class='text-danger'>*</span>" :  "" !!}
            <label>سریال شناسنامه</label>
            <input type="text"
                   class="form-control {{ $errors->has('persons.prsn_serial_certificate_code') ? 'error-border' : '' }}"
                   name="persons[prsn_serial_certificate_code]" value="{{ formValue('persons.prsn_serial_certificate_code',@$person,'prsn_serial_certificate_code') }}">

            @if($errors->has('persons.prsn_serial_certificate_code'))
                <div class="help-block text-danger">
                    {{ $errors->first('persons.prsn_serial_certificate_code') }}
                </div>
            @endif
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            {!! $persons->rules()['persons.prsn_phone1'][0] == "required" ? "<span class='text-danger'>*</span>" :  "" !!}
            <label>شماره تماس</label>
            <input type="text"
                   class="form-control {{ $errors->has('persons.prsn_phone1') ? 'error-border' : '' }}"
                   name="persons[prsn_phone1]" value="{{ formValue('persons.prsn_phone1',@$person,'prsn_phone1') }}">

            @if($errors->has('persons.prsn_phone1'))
                <div class="help-block text-danger">
                    {{ $errors->first('persons.prsn_phone1') }}
                </div>
            @endif
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            {!! $persons->rules()['persons.prsn_mobile1'][0] == "required" ? "<span class='text-danger'>*</span>" :  "" !!}
            <label>شماره همراه</label>
            <input type="text"
                   class="form-control {{ $errors->has('persons.prsn_mobile1') ? 'error-border' : '' }}"
                   name="persons[prsn_mobile1]" value="{{ formValue('persons.prsn_mobile1',@$person,'prsn_mobile1') }}">

            @if($errors->has('persons.prsn_mobile1'))
                <div class="help-block text-danger">
                    {{ $errors->first('persons.prsn_mobile1') }}
                </div>
            @endif
        </div>
    </div>
    <div class='col-md-2'>
        <div class='form-group'>
            {!! $persons->rules()['collections.coll_email'][0] == "required" ? "<span class='text-danger'>*</span>" :  "" !!}
            <label>ایمیل</label>
            <input type='text' class='form-control {{ $errors->has('collections.coll_email') ? 'error-border' : '' }}'
                   name='collections[coll_email]'
                   value="{{ old('collections.coll_email',empty($collections) ? '' : $collections->email) }}">

            @if($errors->has('collections.coll_email'))
                <div class='help-block text-danger'>
                    {{ $errors->first('collections.coll_email') }}
                </div>
            @endif
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            {!! $persons->rules()['persons.prsn_gender_id'][0] == "required" ? "<span class='text-danger'>*</span>" :  "" !!}
            <label>جنسیت</label>
            <select class="form-control  {{ $errors->has('persons.prsn_gender_id') ? 'error-border' : '' }}"
                    name="persons[prsn_gender_id]">
                {!! customForeach($gender,'bas_id','bas_value',@$person->prsn_gender_id) !!}
            </select>

            @if($errors->has('persons.prsn_gender_id'))
                <div class="help-block text-danger">
                    {{ $errors->first('persons.prsn_gender_id') }}
                </div>
            @endif
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            {!! $persons->rules()['persons.prsn_post_code'][0] == "required" ? "<span class='text-danger'>*</span>" :  "" !!}
            <label>کدپستی </label>
            <input type="text"
                   class="form-control {{ $errors->has('persons.prsn_post_code') ? 'error-border' : '' }}"
                   name="persons[prsn_post_code]" value="{{ formValue('persons.prsn_post_code',@$person,'prsn_post_code') }}">

            @if($errors->has('persons.prsn_post_code'))
                <div class="help-block text-danger">
                    {{ $errors->first('persons.prsn_post_code') }}
                </div>
            @endif
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            {!! $persons->rules()['persons.prsn_address'][0] == "required" ? "<span class='text-danger'>*</span>" :  "" !!}
            <label>آدرس محل سکونت</label>
            <textarea class="form-control {{ $errors->has('persons.prsn_address') ? 'error-border' : '' }}"
                      name="persons[prsn_address]">{{ old('persons.prsn_address',empty($persons) ? '' : @$person->prsn_address) }}</textarea>

            @if($errors->has('persons.prsn_address'))
                <div class="help-block text-danger">
                    {{ $errors->first('persons.prsn_address') }}
                </div>
            @endif
        </div>
    </div>
</div>