<div class='row'>
    <div class="col-md-2">
        <div class="form-group">
            <div id="deleteImage">
            </div>
            <input accept="image/jpeg" name="file" type="file" id="imgInp"
                   style="display: none">
            <img title="آپلود عکس" style="cursor:pointer;" id="blah"
                 src="{{(isset($avatar))? getFile($avatar->ur_path) : asset('/images/image.jpg')}}"
                 alt="your image" width="150px" height="150px">

        </div>
    </div>
</div>
<h5>** اطلاعات فردی</h5>
<hr>
<div class='row'>
    <div class='col-md-2'>
        <div class='form-group'>
            <label>نام</label>
            <input type='text'
                   class='form-control'
                   name='persons[prsn_name]'
                   value="{{ old('persons.prsn_name',empty($persons) ?  '' : $persons->prsn_name) }}">
        </div>
    </div>
    <div class='col-md-2'>
        <div class='form-group'>
            <label>نام خانوادگی</label>
            <input type='text'
                   class='form-control'
                   name='persons[prsn_family]'
                   value="{{ old('persons.prsn_family',empty($persons) ? '' : $persons->prsn_family) }}">
        </div>
    </div>
    <div class='col-md-2'>
        <div class='form-group'>
            <label>نام پدر</label>
            <input type='text'
                   class='form-control'
                   name='persons[prsn_father_name]'
                   value="{{ old('persons.prsn_father_name',empty($persons) ? '' : $persons->prsn_father_name) }}">
        </div>
    </div>
    <div class='col-md-2'>
        <div class='form-group'>
            <label>تاریخ تولد</label>
            <input type='text'
                   class='form-control datePicker'
                   name='persons[prsn_birthday]' maxlength="10"
                   value="{{ old('persons.prsn_birthday',empty($persons) ? '' : $persons->prsn_birthday) }}">
        </div>
    </div>
    <div class='col-md-2'>
        <div class='form-group'>
            <label>شماره ملی</label>
            <input type='text'
                   class='form-control'
                   name='persons[prsn_national_code]' maxlength="11"
                   value="{{ old('persons.prsn_national_code',empty($persons) ? '' : $persons->prsn_national_code) }}">
        </div>
    </div>
    <div class='col-md-2'>
        <div class='form-group'>
            <label>کد پستی</label>
            <input type='text'
                   class='form-control'
                   name='persons[prsn_post_code]' maxlength="11"
                   value="{{ old('persons.prsn_post_code',empty($persons) ? '' : $persons->prsn_post_code) }}">
        </div>
    </div>

    <div class="col-md-2">
        <div class="form-group">
            <label>استان</label>
            <select
                class='form-control state'
                name='persons[prsn_state_id]' id='state'>
                {!! customForeach($states,'c_id','c_name',old('persons.prsn_state_id',empty($persons) ? '' : $persons->prsn_state_id)) !!}
            </select>

            @if($errors->has('collections.coll_state_id'))
                <div class='help-block text-danger'>
                    {{ $errors->first('collections.coll_state_id') }}
                </div>
            @endif
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <?php
            if (!empty($persons) && $persons->prsn_state_id != "")
                $cities = \App\City::where('c_parent_id', $persons->prsn_state_id)->get();
            elseif (old('persons.prsn_state_id') != "")
                $cities = \App\City::whereCParentId(old('persons.prsn_state_id'))->get();
            else
                $cities = [];
            ?>
            <label>شهر</label>
            <select
                class='form-control city'
                name='persons[prsn_city_id]' id="city">
                <option value>انتخاب نمایید</option>
                @foreach($cities as $city)
                    <option
                        value="{{ $city->c_id }}" {{ old('persons.prsn_city_id', (!empty($persons)) ? $persons->prsn_city_id : '') == $city->c_id ? "selected" : ""  }}>{{ $city->c_name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>آدرس</label>
            <textarea
                class="form-control"
                name="persons[prsn_address]">{{ old('persons.prsn_address',empty($persons) ? '' : $persons->prsn_address) }}</textarea>
        </div>
    </div>
</div>

<h5>** اطلاعات کسب و کار</h5>
<hr>
<div class='row'>
    <div class='col-md-2'>
        <div class='form-group'>
            <label>شغل</label>
            <input type='text'
                   class='form-control'
                   name='persons[prsn_job]' maxlength="11"
                   value="{{ old('persons.prsn_job',empty($persons) ? '' : $persons->prsn_job) }}">
        </div>
    </div>
    <div class='col-md-2'>
        <div class='form-group'>
            <label>نام فروشگاه</label>
            <input type='text'
                   class='form-control'
                   name='collection[coll_name]'
                   value="{{ old('collection.coll_name',empty($collection) ? '' : $collection->coll_name) }}">
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <label>استان</label>
            <select
                class='form-control state_coll'
                name='collection[coll_state_id]' id='state1'>
                {!! customForeach($states_coll,'c_id','c_name',old('collection.coll_state_id',empty($collection) ? '' : $collection->coll_state_id)) !!}
            </select>

        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <?php
            if (!empty($collection) && $collection->coll_state_id != "")
                $cities_coll = \App\City::where('c_parent_id', $collection->coll_state_id)->get();
            elseif (old('collection.coll_state_id') != "")
                $cities_coll = \App\City::whereCParentId(old('collection.coll_state_id'))->get();
            else
                $cities_coll = [];
            ?>
            <label>شهر</label>
            <select
                class='form-control city_coll'
                name='collection[coll_city_id]' id="city">
                <option value>انتخاب نمایید</option>
                @foreach($cities_coll as $city)
                    <option
                        value="{{ $city->c_id }}" {{ old('collection.coll_city_id', (!empty($collection)) ? $collection->coll_city_id : '') == $city->c_id ? "selected" : ""  }}>{{ $city->c_name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>آدرس محل کار</label>
            <textarea
                class="form-control"
                name="collection[coll_address]">{{ old('collection.coll_address',empty($collection) ? '' : $collection->coll_address) }}</textarea>
        </div>
    </div>
    <div class="col-md-5">
        <span class="text-danger">*</span>
        <label>صنف</label>
        <?php
        $typeCollections = \App\Baseinfo::whereBasType('type_class')->where('bas_parent_id', '<>', '0')->get();
        ?>
        <select class="form-control select2" name="shop_keeper_class[]" multiple>
            {!! customForeach($typeCollections,'bas_id','bas_value',$old_senfs) !!}
        </select>
<input type="hidden" name="class">
    </div>
</div>
@section('script')
    <script>
        $(document).ready(function () {

            $("#imgInp").change(function () {
                previewImage(this);
                if ($(this).val() != "")
                    $("#deleteImage").html("<i id='deleteImageUpload' title=\"حذف عکس\" class=\"fa fa-trash text-danger\"></i>")
            });
            $(document).on('click', '#blah', function () {
                $("#imgInp").click();
            });
            $(document).on('click', '#deleteImageUpload', function () {
                $("#imgInp").val('');
                $("#blah").attr('src', '{{asset('/images/image.jpg')}}');
                $("#deleteImage").html("");
            });

            $(document).on("change", "select.state_coll", function () {
                var id = $(this).val();
                $.get("{{route('get-cities')}}", {id: id}, function (data) {
                    $("select.city_coll").html(JSON.parse(data));
                })
            });
        });
    </script>
    @append
