@section('style')
    <style>
        .formDiscount {
            display: none;
        }

        #deleteImage {
            position: relative;
        }

        #deleteImageUpload {
            font-size: 35px;
            margin-left: 10px;
            z-index: 1;
            cursor: pointer;
            position: absolute;
        }

        .line-end {
            border-top: 1px solid #D1D5EA;
            padding: 10px 0;
            margin-top: 20px;
        }

    </style>
@endsection

{{ csrf_field() }}
{!! (!empty($uploadRoutes)) ? "<input name='_method' type='hidden' value='PATCH'>" : ''  !!}
<input type="hidden" name="prsn_id" value="{{ request()->query('prsn_id') }}">

    @if(empty($upload))
        <div class="row sample">
    <div class="col-md-3">
        <div class="form-group">
            <label>انتخاب فایل</label>
            <br>
            <div id="deleteImage" class="deleteImage">
            </div>
            <img title="آپلود فایل" style="cursor:pointer;" class="blah"
                 src="{{ asset('/images/image.jpg') }}"
                 alt="your image" width="150px" height="150px">
            <input name="ur_url[]" type="file" id="imgInp" class="imgInp" style="display: none">


        </div>
    </div>
    <div class="col-md-3">
        <?php $types = \App\Baseinfo::whereBasType('typeUpload')->where('bas_parent_id', '<>', '0')->get() ?>
        <div class="form-group">
            <label>نوع فایل</label>
            <select class="form-control" name="ur_type_id[]">
                {!! customForeach($types,'bas_id','bas_value') !!}
            </select>
        </div>
    </div>
    <div class="col-md-5">
        <div class="form-group">
            <label>شرح</label>
            <textarea class="form-control"
                      name="ur_description[]"></textarea>
        </div>
    </div>
    <div class="col-md-1">
        <div class="form-group">
           <a href="#" class="deleteForm btn btn-danger" style="margin-top: 30px">
               <i class="fa fa-trash"></i>
           </a>
        </div>
    </div>
    </div>
        @else
        @foreach($upload as $row)
            <div class="row sample">
        <div class="col-md-3">
            <div class="form-group">
                <label>انتخاب فایل</label>
                <br>
                <div id="deleteImage" class="deleteImage">
                </div>
                <img title="آپلود فایل" style="cursor:pointer;" class="blah"
                     src="{{ getFile($row->ur_path)}}"
                     alt="your image" width="150px" height="150px">
                <input name="ur_url[]" type="file" id="imgInp" class="imgInp" style="display: none">


            </div>
        </div>
        <div class="col-md-3">
            <?php $types = \App\Baseinfo::whereBasType('typeUpload')->where('bas_parent_id', '<>', '0')->get() ?>
            <div class="form-group">
                <label>نوع فایل</label>
                <select class="form-control" name="ur_type_id[]">
                    {!! customForeach($types,'bas_id','bas_value',$row->bas_id) !!}
                </select>
            </div>
        </div>
        <div class="col-md-5">
            <div class="form-group">
                <label>شرح</label>
                <textarea class="form-control"
                          name="ur_description[]">{{$row->ur_description}}</textarea>
            </div>
        </div>
        <div class="col-md-1">
            <div class="form-group">
                <a href="#" class="deleteForm btn btn-danger" style="margin-top: 30px">
                    <i class="fa fa-trash"></i>
                </a>
            </div>
        </div>
        </div>
        @endforeach
        @endif
<div class="addForm">

</div>



@section('script')
    <script>

        $(document).on('click', '.addFormUpload', function () {
            $('.ajaxMessage').html("");

            var htmlForm = $(".sample:last").clone();
            $(".help-block").remove();
            $(".addForm").append(htmlForm);
            $(".sample:last").find(".help-block").remove();
            $(".sample:last").find("textarea,input").val("");
            $(".imgInp:last").val('');
            $(".blah:last").attr('src', "{{ asset('/images/image.jpg') }}")
            $(".deleteImage:last").html("");
            $("textarea,select,input").css({
                "border-width": "1px",
                "border-color": "#b8b894",
                "border-style": "solid"
            });
        });

        function previewImage(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $(input).closest(".sample").find('.blah').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $(document).on('change','.imgInp',function () {
            previewImage(this);
            if ($(this).val() != "")
                $(this).closest(".sample").find(".deleteImage").html("<i id='deleteImageUpload' title=\"حذف عکس\" class=\"fa fa-trash text-danger deleteImageUpload\"></i>")
        });

        $(document).on('click', '.blah', function () {
            $(this).closest(".sample").find(".imgInp").click();
        });
        $(document).on('click', '.deleteImageUpload', function () {
            var parent = $(this).closest(".sample");
            $(parent).find(".imgInp").val('');
            $(parent).find(".blah").attr('src', "{{ asset('/images/image.jpg') }}");
            $(parent).find(".deleteImage").html("");
        });
        $(document).on('click', '.deleteForm', function () {
            var parent = $(this).closest(".sample");
            $(parent).remove();
        });
    </script>

@endsection
