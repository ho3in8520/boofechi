<?php
$statuses = \App\Baseinfo::where("bas_type", "condition-panel-collection")->where("bas_parent_id", "!=", 0)->get();
$states = \App\City::where("c_parent_id", 0)->get();
$cities = \App\City::where("c_parent_id", request()->query('state'))->get();
?>
<tr>
    <td></td>
    <td>
        <input type="text" name="name" class="form-control"
               value="{{request()->query('name')}}">
    </td>
    <td>
        <input type="text" name="id" class="form-control"
               value="{{request()->query('id')}}">
    </td>
    <td>
        <select name="state" class="form-control state">
            {!!customForeach($states, "c_id", "c_name",request()->query('state')) !!}
        </select>
    </td>
    <td>
        <select name="city" class="form-control city">
            {!!customForeach($cities, "c_id", "c_name",request()->query('city'))!!}
        </select>
    </td>
    <td>
        <button type="submit" class="btn-loading btn btn-primary search-ajax">اعمال فیلتر
            <i class="ft-thumbs-up position-right"></i>
        </button>
    </td>
</tr>