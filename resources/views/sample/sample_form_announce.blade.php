<?php
$announcementRequest = new \App\Http\Requests\AnnoucementRequest();
?>
<div class="form-group row">
    <label class="col-md-1 label-control"
           for="projectinput1">{{ __('validation.attributes')['announcement.ann_title'] }} : </label>
    <div class="col-md-7">
        <input type='text' class='form-control title {{ $errors->has('announcement.ann_title') ? 'error-border' : '' }}'
               name='announcement[ann_title]'
               value="{{ old('announcement.ann_body',empty($announcement) ?  '' : $announcement->ann_title) }}">

        @if($errors->has('announcement.ann_title'))
            <div class='help-block text-danger'>
                {{ $errors->first('announcement.ann_title') }}
            </div>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-md-1 label-control" for="projectinput1">{{ __('validation.attributes')['announcement.ann_body'] }}
        : </label>
    <div class="col-md-10">
        <textarea rows="6" id="editor"
                  class='form-control ann_body {{ $errors->has('announcement.ann_body') ? 'error-border' : '' }}'
                  name='ann_body'>{{old('announcement.ann_body',empty($announcement) ?  '' : $announcement->ann_body) }}</textarea>

        @if($errors->has('announcement.ann_body'))
            <div class='help-block text-danger'>
                {{ $errors->first('announcement.ann_body') }}
            </div>
        @endif
    </div>
</div>
@if(user('panel_type')==39)
    <div class="row">
        <h4>نوع ارسال :</h4>
    </div>
    <div class="row">
        <div class="col-md-2">
            <input type="radio" class="check" name="type" value="news">
            <label>اخبار</label>
        </div>
        <div class="form-group col-md-2">
            <input type="radio" class="check" name="type" value="ann">
            <label>اطلاعیه</label>
        </div>
    </div>
@endif
<div class="row">
    <h4>ارسال به:</h4>
</div>
<div class="row">
    <input type="text" name="selected_type" style="display: none;">
</div>
<div class="row">
    @foreach($types as $type)
        <div class="col-sm-2">
            <input type="checkbox"
                   name="role_person[{{$type['bas_id']}}]" class="check"
                   value="{{$type['bas_id']}}" {{(in_array($type['bas_id'],$selected_types)) ? "checked" : ''}}> {{$type['bas_value']}}
        </div>
    @endforeach
</div>
<div class="form-group row mt-3">
    @include('sample.sample_form_area_support')
</div>
@section('script')
    {{--<script src="{{asset('js/functions/storeAjax.js')}}"></script>--}}
    <script src="{{asset('theme/ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('theme/ckeditor/adapters/jquery.js')}}"></script>
    <script>
        $(document).ready(function () {
            //     $("#saveAnnounced").click(function (e) {
            //         e.preventDefault();
            //         storeAjax($(this), 'POST');
            //     });
        });
        CKEDITOR.replace('editor', {
// Load the German interface.
            language: 'fa'
        });
    </script>
@append
