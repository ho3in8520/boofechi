<?php
if (\Illuminate\Support\Facades\Request::is('factory/*'))
    $collection = new \App\Http\Requests\FactoryRequest();
elseif (\Illuminate\Support\Facades\Request::is('company/*'))
    $collection = new \App\Http\Requests\CompanyRequest();
elseif (\Illuminate\Support\Facades\Request::is('wholesaler/*'))
    $collection = new \App\Http\Requests\WholesalerRequest();
?>
@section('style')
    <style>
        .formDiscount {
            display: none;
        }

        #deleteImage {
            position: relative;
        }

        #deleteImageUpload {
            font-size: 35px;
            margin-left: 10px;
            z-index: 1;
            cursor: pointer;
            position: absolute;
        }
    </style>
@append
<div class='row'>
    <div class="col-md-2">
        <div class="form-group">
            <div id="deleteImage">
            </div>
            <input accept="image/jpeg" name="file" type="file" id="imgInp"
                   style="display: none">
            <img title="آپلود عکس" style="cursor:pointer;" id="blah"
                 src="{{(isset($logo))? getFile($logo->ur_path) : asset('/images/image.jpg')}}"
                 alt="your image" width="150px" height="150px">

        </div>
    </div>
</div>
<div class='row'>
    <input type="hidden" name="coll_id" value="{{ (!empty($collections) ? $collections->coll_id : "0") }}">
    <input type="hidden" name="prsn_id" value="{{ (!empty($persons) ? $persons->prsn_id : "0") }}">
    <div class='col-md-2'>
        <div class='form-group'>
            {!! in_array('required',$collection->rules()['collections.coll_name']) ? "<span class='text-danger'>*</span>" : '' !!}
            <label>{{ __('validation.attributes')['collections.coll_name'] }}</label>
            <input type='text' class='form-control {{ $errors->has('collections.coll_name') ? 'error-border' : '' }}'
                   name='collections[coll_name]'
                   value="{{ old('collections.coll_name',empty($collections) ?  '' : $collections->coll_name) }}">

            @if($errors->has('collections.coll_name'))
                <div class='help-block text-danger'>
                    {{ $errors->first('collections.coll_name') }}
                </div>
            @endif
        </div>
    </div>
    <div class='col-md-2'>
        <div class='form-group'>
            {!! ($collection->rules()['persons.prsn_name'][0] == 'required') ? "<span class='text-danger'>*</span>" : '' !!}
            <label>{{ __('validation.attributes')['persons.prsn_name'] }} (نام مدیر)</label>
            <input type='text' class='form-control {{ $errors->has('persons.prsn_name') ? 'error-border' : '' }}'
                   name='persons[prsn_name]'
                   value="{{ old('persons.prsn_name',empty($persons) ? '' : $persons->prsn_name) }}">

            @if($errors->has('persons.prsn_name'))
                <div class='help-block text-danger'>
                    {{ $errors->first('persons.prsn_name') }}
                </div>
            @endif
        </div>
    </div>
    <div class='col-md-2'>
        <div class='form-group'>
            {!! ($collection->rules()['persons.prsn_family'][0] == 'required') ? "<span class='text-danger'>*</span>" : '' !!}
            <label>{{ __('validation.attributes')['persons.prsn_family'] }}</label>
            <input type='text' class='form-control {{ $errors->has('persons.prsn_family') ? 'error-border' : '' }}'
                   name='persons[prsn_family]'
                   value="{{ old('persons.prsn_family',empty($persons) ? '' : $persons->prsn_family) }}">

            @if($errors->has('persons.prsn_family'))
                <div class='help-block text-danger'>
                    {{ $errors->first('persons.prsn_family') }}
                </div>
            @endif
        </div>
    </div>
    <div class='col-md-2'>
        <div class='form-group'>
            {!! ($collection->rules()['persons.prsn_national_code'][0] == 'required') ? "<span class='text-danger'>*</span>" : '' !!}
            <label>{{ __('validation.attributes')['persons.prsn_national_code'] }}</label>
            <input type='text'
                   class='form-control {{ $errors->has('persons.prsn_national_code') ? 'error-border' : '' }}'
                   name='persons[prsn_national_code]' maxlength="10"
                   value="{{ old('persons.prsn_national_code',empty($persons) ? '' : $persons->prsn_national_code) }}">


            @if($errors->has('persons.prsn_national_code'))
                <div class='help-block text-danger'>
                    {{ $errors->first('persons.prsn_national_code') }}
                </div>
            @endif
        </div>
    </div>
    <div class='col-md-2'>
        <div class='form-group'>
            {!! ($collection->rules()['persons.prsn_mobile1'][0] == 'required') ? "<span class='text-danger'>*</span>" : '' !!}
            <label>{{ __('validation.attributes')['persons.prsn_mobile1'] }}</label>
            <input type='text' class='form-control {{ $errors->has('persons.prsn_mobile1') ? 'error-border' : '' }}'
                   name='persons[prsn_mobile1]' maxlength="11"
                   value="{{ old('persons.prsn_mobile1',empty($persons) ? '' : $persons->prsn_mobile1) }}">

            @if($errors->has('persons.prsn_mobile1'))
                <div class='help-block text-danger'>
                    {{ $errors->first('persons.prsn_mobile1') }}
                </div>
            @endif
        </div>
    </div>
    <div class='col-md-2'>
        <div class='form-group'>
            {!! ($collection->rules()['persons.prsn_phone1'][0] == 'required') ? "<span class='text-danger'>*</span>" : '' !!}
            <label>{{ __('validation.attributes')['persons.prsn_phone1'] }}</label>
            <input type='text' class='form-control {{ $errors->has('persons.prsn_phone1') ? 'error-border' : '' }}'
                   name='persons[prsn_phone1]' maxlength="11"
                   value="{{ old('persons.prsn_phone1',empty($persons) ? '' : $persons->prsn_phone1) }}">

            @if($errors->has('persons.prsn_phone1'))
                <div class='help-block text-danger'>
                    {{ $errors->first('persons.prsn_phone1') }}
                </div>
            @endif
        </div>
    </div>
    <div class='col-md-2'>
        <div class='form-group'>
            {!! ($collection->rules()['user.reagent_id'][0] == 'required') ? "<span class='text-danger'>*</span>" : '' !!}
            <label>{{ __('validation.attributes')['user.reagent_id'] }}</label>
            <input type='text'
                   class='form-control {{ $errors->has('user.reagent_id') ? 'error-border' : '' }}'
                   name='user[reagent_id]'
                   value="{{ old('user.reagent_id',empty($collections) ? '' : $collections->coll_reagent_code_id) }}">

            @if($errors->has('user.reagent_id'))
                <div class='help-block text-danger'>
                    {{ $errors->first('user.reagent_id') }}
                </div>
            @endif
        </div>
    </div>
    <div class='col-md-2'>
        <div class='form-group'>
            {!! ($collection->rules()['collections.coll_economic_code'][0] == 'required') ? "<span class='text-danger'>*</span>" : '' !!}
            <label>{{ __('validation.attributes')['collections.coll_economic_code'] }}</label>
            <input type='text'
                   class='form-control {{ $errors->has('collections.coll_economic_code') ? 'error-border' : '' }}'
                   name='collections[coll_economic_code]'
                   value="{{ old('collections.coll_economic_code',empty($collections) ? '' : $collections->coll_economic_code) }}">

            @if($errors->has('collections.coll_economic_code'))
                <div class='help-block text-danger'>
                    {{ $errors->first('collections.coll_economic_code') }}
                </div>
            @endif
        </div>
    </div>
    <div class='col-md-2'>
        <div class='form-group'>
            {!! ($collection->rules()['collections.coll_national_number'][0] == 'required') ? "<span class='text-danger'>*</span>" : '' !!}
            <label>{{ __('validation.attributes')['collections.coll_national_number'] }}</label>
            <input type='text'
                   class='form-control {{ $errors->has('collections.coll_national_number') ? 'error-border' : '' }}'
                   name='collections[coll_national_number]' maxlength="10"
                   value="{{ old('collections.coll_national_number',empty($collections) ? '' : $collections->coll_national_number) }}">

            @if($errors->has('collections.coll_national_number'))
                <div class='help-block text-danger'>
                    {{ $errors->first('collections.coll_national_number') }}
                </div>
            @endif
        </div>
    </div>
    <div class='col-md-2'>
        <div class='form-group'>
            {!! ($collection->rules()['collections.coll_post_code'][0] == 'required') ? "<span class='text-danger'>*</span>" : '' !!}
            <label>{{ __('validation.attributes')['collections.coll_post_code'] }}</label>
            <input type='text'
                   class='form-control {{ $errors->has('collections.coll_post_code') ? 'error-border' : '' }}'
                   name='collections[coll_post_code]' maxlength="10"
                   value="{{ old('collections.coll_post_code',empty($collections) ? '' : $collections->coll_post_code) }}">

            @if($errors->has('collections.coll_post_code'))
                <div class='help-block text-danger'>
                    {{ $errors->first('collections.coll_post_code') }}
                </div>
            @endif
        </div>
    </div>
    <div class='col-md-2'>
        <div class='form-group'>
            {!! ($collection->rules()['collections.coll_site_address'][0] == 'required') ? "<span class='text-danger'>*</span>" : '' !!}
            <label>{{ __('validation.attributes')['collections.coll_site_address'] }}</label>
            <input type='text'
                   class='form-control {{ $errors->has('collections.coll_site_address') ? 'error-border' : '' }}'
                   name='collections[coll_site_address]'
                   value="{{ old('collections.coll_site_address',empty($collections) ? '' : $collections->coll_site_address) }}">

            @if($errors->has('collections.coll_site_address'))
                <div class='help-block text-danger'>
                    {{ $errors->first('collections.coll_site_address') }}
                </div>
            @endif
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            {!! ($collection->rules()['collections.coll_site_address'][0] == 'required') ? "<span class='text-danger'>*</span>" : '' !!}
            <label>{{ __('validation.attributes')['collections.coll_state_id'] }}</label>
            <select class='form-control state select2 {{ $errors->has('collections.coll_state_id') ? 'error-border' : '' }}'
                    name='collections[coll_state_id]' id='state'>
                {!! customForeach($states,'c_id','c_name',old('collections.coll_state_id',empty($collections) ? '' : $collections->coll_state_id)) !!}
            </select>

            @if($errors->has('collections.coll_state_id'))
                <div class='help-block text-danger'>
                    {{ $errors->first('collections.coll_state_id') }}
                </div>
            @endif
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <?php
            if (!empty($collections) && $collections->coll_state_id != "")
                $cities = \App\City::where('c_parent_id', $collections->coll_state_id)->get();
            elseif (old('collections.coll_state_id') != "")
                $cities = \App\City::whereCParentId(old('collections.coll_state_id'))->get();
            else
                $cities = [];
            ?>
            {!! ($collection->rules()['collections.coll_city_id'][0] == 'required') ? "<span class='text-danger'>*</span>" : '' !!}
            <label>{{ __('validation.attributes')['collections.coll_city_id'] }}</label>
            <select class='form-control city select2 {{ $errors->has('collections.coll_city_id') ? 'error-border' : '' }}'
                    name='collections[coll_city_id]' id="city">
                <option value>انتخاب نمایید</option>
                @foreach($cities as $city)
                    <option value="{{ $city->c_id }}" {{ old('collections.coll_city_id', (!empty($collections)) ? $collections->coll_city_id : '') == $city->c_id ? "selected" : ""  }}>{{ $city->c_name }}</option>
                @endforeach
            </select>

            @if($errors->has('collections.coll_city_id'))
                <div class='help-block text-danger'>
                    {{ $errors->first('collections.coll_city_id') }}
                </div>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <span class="text-danger">*</span>
        <label>صنف</label>
        <?php
        $typeCollections = \App\Baseinfo::whereBasType('type_class')->where('bas_parent_id', '<>', '0')->get();
        ?>
        <select class="form-control select2" name="classType[]" multiple style="height: 100px">
            <option value>انتخاب نمایید</option>
            @foreach($typeCollections as $type)
                @if((is_array(old('classType'))) && in_array($type->bas_id,old('classType')))
                    <?php $selected = "selected" ?>
                @elseif(!empty($collectionClass) && in_array($type->bas_id,$collectionClass))
                    <?php $selected = "selected" ?>
                @else
                    <?php $selected = "" ?>
                @endif
                <option value="{{ $type->bas_id  }}" {{  $selected  }}>{{ $type->bas_value }}</option>
            @endforeach
        </select>
        <input type="hidden" name="typeClass" value="1">
        @error('typeClass')
        <div class="help-block text-danger">
            {{ $message }}
        </div>
        @enderror
    </div>
    <div class="col-md-12">
        <div class="form-group">
            {!! ($collection->rules()['collections.coll_address'][0] == 'required') ? "<span class='text-danger'>*</span>" : '' !!}
            <label>{{ __('validation.attributes')['collections.coll_address'] }}</label>
            <textarea class="form-control {{ $errors->has('collections.coll_address') ? 'error-border' : '' }}"
                      name="collections[coll_address]">{{ old('collections.coll_address',empty($collections) ? '' : $collections->coll_address) }}</textarea>

            @if($errors->has('collections.coll_address'))
                <div class="help-block text-danger">
                    {{ $errors->first('collections.coll_address') }}
                </div>
            @endif
        </div>
    </div>
</div>
@section('script')
    <script>
        $("#imgInp").change(function () {
            previewImage(this);
            if ($(this).val() != "")
                $("#deleteImage").html("<i id='deleteImageUpload' title=\"حذف عکس\" class=\"fa fa-trash text-danger\"></i>")
        });
        $(document).on('click', '#blah', function () {
            $("#imgInp").click();
        });
        $(document).on('click', '#deleteImageUpload', function () {
            $("#imgInp").val('');
            $("#blah").attr('src', '{{asset('/images/image.jpg')}}');
            $("#deleteImage").html("");
        });
    </script>
@append
