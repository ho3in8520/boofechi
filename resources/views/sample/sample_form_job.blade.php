<?php
$collection = new \App\Http\Requests\AgentRequest();

$states = \App\City::where('c_parent_id', 0)->get()->toArray();
if (!empty($collections) && $collections->prsn_id != "") {
    $cities = \App\City::where('c_parent_id', $collections->coll_state_id)->get();
} else if (old('collections.coll_state_id') != "")
    $cities = \App\City::whereCParentId(old('collections.coll_state_id'))->get();
else
    $cities = [];

?>
<div class="row">
    <div class='col-md-2'>
        <div class='form-group'>
            {!! $collection->rules()['collections.coll_name'][0] == "required" ? "<span class='text-danger'>*</span>" : "" !!}
            <label>نام مجموعه </label>
            <input type='text'
                   class='form-control {{ $errors->has('collections.coll_name') ? 'error-border' : '' }}'
                   name='collections[coll_name]'
                   value="{{ old('collections.coll_name',empty($collections) ?  '' : $collections->coll_name) }}">

            @if($errors->has('collections.coll_name'))
                <div class='help-block text-danger'>
                    {{ $errors->first('collections.coll_name') }}
                </div>
            @endif
        </div>
    </div>
    <div class='col-md-2'>
        <div class='form-group'>
            <label>کد اقتصادی</label>
            <input type='text'
                   class='form-control {{ $errors->has('collections.coll_economic_code') ? 'error-border' : '' }}'
                   name='collections[coll_economic_code]'
                   value="{{ old('collections.coll_economic_code',empty($collections) ? '' : $collections->coll_economic_code) }}">

            @if($errors->has('collections.coll_economic_code'))
                <div class='help-block text-danger'>
                    {{ $errors->first('collections.coll_economic_code') }}
                </div>
            @endif
        </div>
    </div>
    <div class='col-md-2'>
        <div class='form-group'>
            <label>شناسه ملی</label>
            <input type='text'
                   class='form-control {{ $errors->has('collections.coll_national_number') ? 'error-border' : '' }}'
                   name='collections[coll_national_number]'
                   value="{{ old('collections.coll_national_number',empty($collections) ? '' : $collections->coll_national_number) }}">

            @if($errors->has('collections.coll_national_number'))
                <div class='help-block text-danger'>
                    {{ $errors->first('collections.coll_national_number') }}
                </div>
            @endif
        </div>
    </div>
    <div class='col-md-2'>
        <div class='form-group'>
            <label>نوع شرکت</label>
            <select class='form-control {{ $errors->has('collections.coll_type_id') ? 'error-border' : '' }}'
                    name='collections[coll_type_id]'>
                {!! customForeach($company,'bas_id','bas_value',old('collections.coll_state_id',empty($collections) ? '' : $collections->coll_type)) !!}
            </select>

            @if($errors->has('collections.coll_type_id'))
                <div class="help-block text-danger">
                    {{ $errors->first('collections.coll_type_id') }}
                </div>
            @endif
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            {!! $collection->rules()['collections.coll_state_id'][0] == "required" ? "<span class='text-danger'>*</span>" : "" !!}
            <label>استان</label>
            <select class='form-control state {{ $errors->has('collections.coll_state_id') ? 'error-border' : '' }}'
                    name='collections[coll_state_id]' id='state'>
                {!! customForeach($states,'c_id','c_name',old('collections.coll_state_id',empty($collections) ? '' : $collections->coll_state_id)) !!}

                {{--<option value>انتخاب نمایید</option>--}}
                {{--@foreach($states as $st)--}}
                    {{--<option value="{{ $st->c_id }}" {{ old('collections.coll_state_id',empty($collections) ? '' : $collections->coll_state_id) == $st->c_id ? "selected" : "" }}>{{ $st->c_name }}</option>--}}
                {{--@endforeach--}}
            </select>

            @if($errors->has('collections.coll_state_id'))
                <div class='help-block text-danger'>
                    {{ $errors->first('collections.coll_state_id') }}
                </div>
            @endif
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            {!! $collection->rules()['collections.coll_city_id'][0] == "required" ? "<span class='text-danger'>*</span>" : "" !!}
            <label>شهر</label>
            <select class='form-control city {{ $errors->has('collections.coll_city_id') ? 'error-border' : '' }}'
                    name='collections[coll_city_id]' id="city">
                @if(empty($collections))
                    <option value="">انتخاب کنید...</option>
                @else
                    {!! customForeach($cities,'c_id','c_name',old('collections.coll_city_id',empty($collections) ? '' : $collections->coll_city_id)) !!}
                @endif
            {{--@foreach($cities as $city)--}}
                    {{--<option value="{{ $city->c_id }}" {{ old('collections.coll_city_id') == $city->c_id ? "selected" : ""  }}>{{ $city->c_name }}</option>--}}
                {{--@endforeach--}}
            </select>

            @if($errors->has('collections.coll_city_id'))
                <div class='help-block text-danger'>
                    {{ $errors->first('collections.coll_city_id') }}
                </div>
            @endif
        </div>
    </div>
    <div class='col-md-2'>
        <div class='form-group'>
            {!! $collection->rules()['collections.coll_post_code'][0] == "required" ? "<span class='text-danger'>*</span>" : "" !!}
            <label>کد پستی</label>
            <input type='text'
                   class='form-control {{ $errors->has('collections.coll_post_code') ? 'error-border' : '' }}'
                   name='collections[coll_post_code]'
                   value="{{ old('collections.coll_post_code',empty($collections) ? '' : $collections->coll_post_code) }}">

            @if($errors->has('collections.coll_post_code'))
                <div class='help-block text-danger'>
                    {{ $errors->first('collections.coll_post_code') }}
                </div>
            @endif
        </div>
    </div>
    <div class='col-md-2'>
        <div class='form-group'>
            {!! $collection->rules()['collections.coll_post_code'][0] == "required" ? "<span class='text-danger'>*</span>" : "" !!}
            <label>دامنه</label>
            <input type='text'
                   class='form-control {{ $errors->has('collections.coll_post_code') ? 'error-border' : '' }}'
                   name='collections[domain]'
                   value="{{ old('collections.domain',empty($domain) ? '' : $domain->ag_domain) }}">

            @if($errors->has('collections.coll_post_code'))
                <div class='help-block text-danger'>
                    {{ $errors->first('collections.coll_post_code') }}
                </div>
            @endif
        </div>
    </div>
    <div class='col-md-2'>
        <div class='form-group'>
            <span class='text-danger'>*</span>
            <label>نام کاربری</label>
            <input type='text'
                   class='form-control'
                   name='collections[username]'
                   value="{{ old('collections.username',empty($collections) ? '' : $collections->username) }}">
        </div>
    </div>
    @if(!$collections)
    <div class='col-md-2'>
        <div class='form-group'>
            <span class='text-danger'>*</span>
            <label>رمز عبور</label>
            <input type='password'
                   class='form-control'
                   name='collections[password]'
                   value="">
        </div>
    </div>
    @endif
    <div class='col-md-2'>
        <div class='form-group'>
            <span class='text-danger'>*</span>
            <label>نقش</label>
            <select class="form-control" name="collections[role]">
                {!! customForeach($roles,'rol_id','rol_label') !!}
            </select>
        </div>
    </div>
    <div class='col-md-12'>
        <div class='form-group'>
            {!! $collection->rules()['collections.coll_address'][0] == "required" ? "<span class='text-danger'>*</span>" : "" !!}
            <label>آدرس محل کسب و کار</label>
            <textarea class="form-control {{ $errors->has('collections.coll_address') ? 'error-border' : '' }}" name="collections[coll_address]">{{ formValue('collections.coll_address',$collections,'coll_address') }}</textarea>

            @if($errors->has('collections.coll_address'))
                <div class="help-block text-danger">
                    {{ $errors->first('collections.coll_address') }}
                </div>
            @endif
        </div>
    </div>
</div>
