<?php
if (!isset($collection_id)) {
    $categories = getCategories(collection_id());
} else {
    $categories = getCategoriesToSenf($collection_id);
}

//dd($categories);
?>
@section('style')
    <style>
        .select2-container {
            min-width: 400px;
        }

        .select2-results__option {
            padding-right: 20px;
            vertical-align: middle;
        }

        /*.select2-selection__clear{*/
        /*float: left !important;*/
        /*z-index: 99999999999 !important;*/
        /*}*/

        .select2-results__option:before {
            content: "";
            display: inline-block;
            position: relative;
            height: 20px;
            width: 20px;
            border: 2px solid #e9e9e9;
            border-radius: 4px;
            background-color: #fff;
            margin-right: 20px;
            vertical-align: middle;
        }

        .select2-selection__choice {
            background-color: #ffffff;
        }

        .select2-results__message:before {
            content: "";
            border: 0;
        }

        .select2-results__option[aria-selected=true]:before {
            font-family: fontAwesome;
            content: "\f00c";
            color: #fff;
            background-color: #28D094;
            border: 0;
            display: inline;
            padding-left: 3px;
        }

        .select2-container--default .select2-results__option[aria-selected=true] {
            background-color: #fff;
        }

        .select2-container--default .select2-results__option--highlighted[aria-selected] {
            /*background-color: #eaeaeb;*/
            color: #272727;
        }

        .select2-selection--multiple {
            height: auto !important;
        }

        .select2-container--default .select2-selection--multiple {
            margin-bottom: 10px;
        }

        .select2-container--default.select2-container--open.select2-container--below .select2-selection--multiple {
            border-radius: 4px;
        }

        .select2-container--default.select2-container--focus .select2-selection--multiple {
            border-color: #28D094;
            border-width: 2px;
        }

        .select2-container--default .select2-selection--multiple {
            border-width: 2px;
        }

        .select2-container--open .select2-dropdown--below {

            border-radius: 6px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.5);

        }

        .select2-selection .select2-selection--multiple:after {
            content: 'hhghgh';
        }

        /* select with icons badges single*/
        .select-icon .select2-selection__placeholder .badge {
            display: none;
        }

        .select-icon .placeholder {
            display: none;
        }

        .select-icon .select2-results__option:before,
        .select-icon .select2-results__option[aria-selected=true]:before {
            display: none !important;
            /* content: "" !important; */
        }

        .select-icon .select2-search--dropdown {
            display: none;
        }
    </style>
@append
<div class='row'>
    <div class='col-md-5'>
        <div>
            <label>دسته بندی
                ( انتخاب همه
                <input type="checkbox" class="selectAllSelect2">
                )
            </label>
            <select name="categories[]" class="select2WithCheckbox form-control select2" multiple="multiple">
                {!!createCombobox($categories,0,$selected) !!}
            </select>
            <input type="hidden" name="category" value="1">
        </div>
    </div>
</div>
