<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'نام کاربری/کلمه عبور نادرست است!',
    'throttle' => 'تعداد تلاش ناموفق شما به حد نصاب رسیده است، لطفا بعد از :seconds ثانیه مجددا تلاش فرمائید.',

];
