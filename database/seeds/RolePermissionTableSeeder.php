<?php

use Illuminate\Database\Seeder;

class RolePermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::insert("
        INSERT INTO role_permissions (role_permissions.rp_rol_id,role_permissions.rp_perm_id)
        SELECT 1,permissions.perm_id from permissions
        ");

        //must delete because it is for test
        $inv = new \App\Inventories();
        $inv->inv_name = "انبار پیش فرض";
        $inv->inv_collection_id = 1;
        $inv->save();

        $fc = new \App\FinancialCycle();
        $fc->fc_name = "دوره مالی پیش فرض";
        $fc->fc_collection_id = 1;
        $fc->fc_date_from = jdate()->format("Y/m/d");
        $fc->fc_date_to = 0;
        $fc->fc_status = 1;
        $fc->save();

        $role = new \App\Role();
        $role->rol_name = "kharobar";
        $role->rol_label = "شرکت های خواروبار فروشی";
        $role->rol_creator = 2;
        $role->save();

        $role = new \App\Role();
        $role->rol_name = "kharobar";
        $role->rol_label = "شرکت های خواروبار فروشی";
        $role->rol_creator = 2;
        $role->save();

        \Illuminate\Support\Facades\DB::table('collection_payment_methods')->insert([
            ['cpm_collection_id' => 1, 'cpm_bas_id' => 50, 'cpm_status' => 1],
            ['cpm_collection_id' => 1, 'cpm_bas_id' => 51, 'cpm_status' => 1],
            ['cpm_collection_id' => 1, 'cpm_bas_id' => 52, 'cpm_status' => 1]
        ]);

        \Illuminate\Support\Facades\DB::table('user_roles')->insert([
            ['ur_user_id' => 2, 'ur_rol_id' => $role->rol_id]
        ]);

        \Illuminate\Support\Facades\DB::table('class_roles')->insert([
            ['clr_class_id' => 47, 'clr_type_id' => 1, 'clr_role_id' => $role->rol_id],
            ['clr_class_id' => 47, 'clr_type_id' => 2, 'clr_role_id' => $role->rol_id],
            ['clr_class_id' => 47, 'clr_type_id' => 3, 'clr_role_id' => $role->rol_id]
        ]);
        //end must delete

    }
}
