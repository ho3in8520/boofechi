<?php

use Illuminate\Database\Seeder;

class SettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::insert("
        INSERT INTO setting (st_key,st_value,st_collection_id,st_user_id) VALUES 
        ('financial_unit',N'تومان',0,0),
        ('tax',cast(9 as nvarchar),0,0)        
        ");
    }
}
