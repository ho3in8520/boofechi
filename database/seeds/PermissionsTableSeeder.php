<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    public function url($route)
    {
        return str_replace(URL::to('/'), "", $route);
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission = [
            [
                'perm_name' => 'menu_collections',
                'perm_label' => "مدیریت مجموعه ها",
                'perm_status' => '1',
                'perm_fm_id' => '0',
                'perm_link' => 'menu',
                'perm_icon' => 'fa fa-home',
                'childs' => [
                    [
                        'perm_name' => 'list_collection',
                        'perm_label' => "لیست مجموعه ها",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => $this->url(route('collections')),
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'add_collection',
                        'perm_label' => "ثبت مجموعه",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => '',
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'edit_collection',
                        'perm_label' => "ویرایش مجموعه",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => '',
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'block_collection',
                        'perm_label' => "بلاک کردن مجموعه",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => '',
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'unblock_collection',
                        'perm_label' => "آنبلاک کردن مجموعه",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => '',
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'design_print',
                        'perm_label' => "طراحی فرم پرینت",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => '',
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'area-support',
                        'perm_label' => "نقاط تحت پوشش",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => '',
                        'perm_icon' => '',
                    ],
                ]
            ],
            //announcement
            [
                'perm_name' => 'menu_announcements',
                'perm_label' => "منو اطلاعیه",
                'perm_status' => '1',
                'perm_fm_id' => '0',
                'perm_link' => 'menu',
                'perm_icon' => '',
                'childs' => [
                    [
                        'perm_name' => 'list_announcements',
                        'perm_label' => "لیست اطلاعیه",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => $this->url(route('announcement-list')),
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'create_announcements',
                        'perm_label' => "ثبت اطلاعیه",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => $this->url(route('announcement.create')),
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'edit_announcements',
                        'perm_label' => "ویرایش اطلاعیه",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => '',
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'delete_announcements',
                        'perm_label' => "حذف اطلاعیه",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => '',
                        'perm_icon' => '',
                    ],
                ]
            ],
            //category
            [
                'perm_name' => 'menu_category',
                'perm_label' => "منو  دسته بندی",
                'perm_status' => '1',
                'perm_fm_id' => '0',
                'perm_link' => 'menu',
                'perm_icon' => '',
                'childs' => [
                    [
                        'perm_name' => 'list_category',
                        'perm_label' => "لیست  دسته بندی",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => $this->url(route('category.index')),
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'create_category',
                        'perm_label' => "ثبت  دسته بندی",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => $this->url(route('category.create')),
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'edit_category',
                        'perm_label' => "ویرایش  دسته بندی",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => '',
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'delete_category',
                        'perm_label' => "حذف  دسته بندی",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => '',
                        'perm_icon' => '',
                    ],
                ]
            ],
            //product
            [
                'perm_name' => 'menu_product',
                'perm_label' => "منو محصولات",
                'perm_status' => '1',
                'perm_fm_id' => '0',
                'perm_link' => 'menu',
                'perm_icon' => '',
                'childs' => [
                    [
                        'perm_name' => 'order_product',
                        'perm_label' => "سفارش محصول",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => $this->url(route('product-list')),
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'list_product',
                        'perm_label' => "لیست محصولات",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => $this->url(route('product.index')),
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'create_product',
                        'perm_label' => "ثبت محصولات",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => $this->url(route('product.create')),
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'edit_product',
                        'perm_label' => "ویرایش محصولات",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => '',
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'delete_product',
                        'perm_label' => "حذف محصولات",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => '',
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'increase_inventory',
                        'perm_label' => "ثبت موجودی محصولات",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => $this->url(route('inventory-factors.create')),
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'product_eshant',
                        'perm_label' => "ویرایش اشانت",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => '',
                        'perm_icon' => '',
                    ],
                ]
            ],
            //user
            [
                'perm_name' => 'menu_user',
                'perm_label' => "منو کاربر",
                'perm_status' => '1',
                'perm_fm_id' => '0',
                'perm_link' => 'menu',
                'perm_icon' => '',
                'childs' => [
                    [
                        'perm_name' => 'list_user',
                        'perm_label' => "لیست کاربران",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => $this->url(route('users-list')),
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'create_user',
                        'perm_label' => "ثبت کاربر",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => $this->url(route('create-user')),
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'edit_user',
                        'perm_label' => "ویرایش کاربر",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => '',
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'delete_user',
                        'perm_label' => "حذف کاربر",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => '',
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'support_form',
                        'perm_label' => "مدیریت ثبت نام ها",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => $this->url(route('search-user')),
                        'perm_icon' => '',
                    ],
                ]
            ],
            //role
            [
                'perm_name' => 'manu_role',
                'perm_label' => "منو نقش ها",
                'perm_status' => '1',
                'perm_fm_id' => '0',
                'perm_link' => 'menu',
                'perm_icon' => '',
                'childs' => [
                    [
                        'perm_name' => 'list_role',
                        'perm_label' => "لیست نقش ها",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => $this->url(route('role.index')),
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'create_role',
                        'perm_label' => "ایجاد نقش",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => $this->url(route('role.create')),
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'edit_role',
                        'perm_label' => "ویرایش نقش",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => '',
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'delete_role',
                        'perm_label' => "حذف نقش",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => '',
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'set_role_to_class',
                        'perm_label' => "انتساب نقش به اصناف",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => $this->url(route('set-role-to-class')),
                        'perm_icon' => '',
                    ],
                ]
            ],
            //setting
            [
                'perm_name' => 'menu_setting',
                'perm_label' => "منو تنظیمات",
                'perm_status' => '1',
                'perm_fm_id' => '0',
                'perm_link' => 'menu',
                'perm_icon' => '',
                'childs' => [
                    [
                        'perm_name' => 'admin-setting',
                        'perm_label' => "تنظیمات",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => $this->url(route('system-setting')),
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'return-reasons',
                        'perm_label' => "علت های مرجوعی",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => $this->url(route('return-reasons')),
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'payment-methods',
                        'perm_label' => "روش های پرداخت",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => $this->url(route('payment-methods')),
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'online-gates',
                        'perm_label' => "روش های پرداخت آنلاین",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => $this->url(route('online-gates')),
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'menu_setting',
                        'perm_label' => "تنظیمات منوها",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => $this->url(route('menu-setting')),
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'manage_baseinfos',
                        'perm_label' => "اطلاعات پایه",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => $this->url(route('basic-info')),
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'manage_terms',
                        'perm_label' => "قوانین سامانه",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => $this->url(route('create-terms')),
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'manage_avatars',
                        'perm_label' => "مدیریت آواتارها",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => $this->url(route('accept-uploaded-files')),
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'manage_registration',
                        'perm_label' => "مدیریت کاربران ثبت نام شده",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => $this->url(route('accept-registration')),
                        'perm_icon' => '',
                    ],
                ]
            ],
            //discounts
            [
                'perm_name' => 'discounts',
                'perm_label' => "تخفیفات",
                'perm_status' => '1',
                'perm_fm_id' => '0',
                'perm_link' => 'menu',
                'perm_icon' => '',
                'childs' => [
//                    [
//                        'perm_name' => 'payment_methods',
//                        'perm_label' => "تخفیف بر اساس روش پرداخت",
//                        'perm_status' => '1',
//                        'perm_fm_id' => '0',
//                        'perm_link' => '/payment-methods',
//                        'perm_icon' => '',
//                    ],
                    [
                        'perm_name' => 'festival_discounts',
                        'perm_label' => "تخفیف بازه ای",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => $this->url(route('festival-discounts')),
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'factor_eshant',
                        'perm_label' => "اشانت بر اساس فاکتور",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => $this->url(route('factor-eshant')),
                        'perm_icon' => '',
                    ],
                ]
            ],
            //factors
            [
                'perm_name' => 'menu-factors',
                'perm_label' => "مدیریت فاکتورها",
                'perm_status' => '1',
                'perm_fm_id' => '0',
                'perm_link' => 'menu',
                'perm_icon' => '',
                'childs' => [
                    [
                        'perm_name' => 'output-factors',
                        'perm_label' => "فاکتورهای صادره",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => $this->url(route('output-factors')),
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'input-factor',
                        'perm_label' => "فاکتورهای وارده ",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => $this->url(route('input-factors')),
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'system-factors',
                        'perm_label' => "فاکتورهای سامانه",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => $this->url(route('list-factor-system')),
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'manage_system_factor',
                        'perm_label' => "مدیریت فاکتورهای سامانه",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => $this->url(route('admin/list-system-factor')),
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'archive-factors',
                        'perm_label' => "بایگانی",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => $this->url(route('archived-factors')),
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'last-accept-factor',
                        'perm_label' => "تایید نهایی فاکتور",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => '',
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'first-accept-factor',
                        'perm_label' => "تایید اولیه فاکتور",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => '',
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'cancel-factor',
                        'perm_label' => "لغو فاکتور",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => '',
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'suspend-factor',
                        'perm_label' => "تعلیق فاکتور",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => '',
                        'perm_icon' => '',
                    ],
                ]
            ],
            //tickets
            [
                'perm_name' => 'menu-ticket',
                'perm_label' => "مدیریت تیکت ها",
                'perm_status' => '1',
                'perm_fm_id' => '0',
                'perm_link' => 'menu',
                'perm_icon' => '',
                'childs' => [
                    [
                        'perm_name' => 'list_tickets',
                        'perm_label' => "لیست تیکت ها",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => $this->url(route('ticket.index')),
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'my-tickets',
                        'perm_label' => "تیکت های من ",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => $this->url(route('my-tickets')),
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'create_ticket',
                        'perm_label' => "ارسال تیکت جدید",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => $this->url(route('ticket.create')),
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'referral_ticket',
                        'perm_label' => "ارجاع تیکت",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => '',
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'view_ticket',
                        'perm_label' => "مشاهده تیکت",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => '',
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'answer_ticket',
                        'perm_label' => "پاسخ به تیکت ها",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => '',
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'create_factor',
                        'perm_label' => "ایجاد فاکتور سیستم",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => '',
                        'perm_icon' => '',
                    ],
                ]
            ],
            //visitors
            [
                'perm_name' => 'menu-ticket',
                'perm_label' => "ویزیتور",
                'perm_status' => '1',
                'perm_fm_id' => '0',
                'perm_link' => 'menu',
                'perm_icon' => '',
                'childs' => [
                    [
                        'perm_name' => 'purchase_for_subscription',
                        'perm_label' => "خرید برای زیر مجموعه",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => $this->url(route('purchase-for-subscription')),
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'view_porsants',
                        'perm_label' => "مشاهده پورسانت های دریافتی",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => $this->url(route('porsant')),
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'edit_porsant',
                        'perm_label' => "ویرایش پورسانت",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => '',
                        'perm_icon' => '',
                    ],
                    [
                        'perm_name' => 'view_subscription',
                        'perm_label' => "مشاهده زیر مجموعه",
                        'perm_status' => '1',
                        'perm_fm_id' => '0',
                        'perm_link' => $this->url(route('subscription')),
                        'perm_icon' => '',
                    ],
                ]
            ],
        ];
        SeederHierarchySaveMenu($permission);

    }
}
