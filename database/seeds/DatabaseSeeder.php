<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            BaseinfosTableSeeder::class,
            CititiesTableSeeder::class,
            UsersTableSeeder::class,
            FackerValue::class,
            PermissionsTableSeeder::class,
            RolesTableSeeder::class,
            UserRoleTableSeeder::class,
            RolePermissionTableSeeder::class,
            SettingTableSeeder::class,
        ]);
    }
}
