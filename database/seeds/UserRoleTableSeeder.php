<?php

use Illuminate\Database\Seeder;

class UserRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::unprepared("SET IDENTITY_INSERT user_roles on");
        \Illuminate\Support\Facades\DB::insert("
        INSERT INTO user_roles (ur_id, ur_user_id, ur_rol_id) VALUES 
        (1, '2', '1')
        ");
        \Illuminate\Support\Facades\DB::unprepared("SET IDENTITY_INSERT user_roles off");
    }
}
