<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::unprepared("SET IDENTITY_INSERT roles on");
        \Illuminate\Support\Facades\DB::insert("
            INSERT INTO roles (rol_id, rol_name, rol_label,rol_creator) VALUES 
            ('1', 'programmer', N'programmer',2)
        ");
        \Illuminate\Support\Facades\DB::unprepared("SET IDENTITY_INSERT roles off");
    }
}
