<?php

use Illuminate\Database\Seeder;

class BaseinfosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::unprepared("SET IDENTITY_INSERT baseinfos on");
        $baseinfos = [
            [
                'bas_id' => '1',
                'bas_type' => 'unknown',
                'bas_value' => 'نامشخص',
                'bas_parent_id' => '0',
                'bas_status' => '0',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '2',
                'bas_value' => 'نوع اشخاص',
                'bas_type' => 'role_person',
                'bas_parent_id' => '0',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '3',
                'bas_value' => 'مدیر پنل',
                'bas_type' => 'role_person',
                'bas_parent_id' => '2',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '4',
                'bas_value' => 'مغازه دار',
                'bas_type' => 'role_person',
                'bas_parent_id' => '2',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '5',
                'bas_value' => 'ویزیتور',
                'bas_type' => 'role_person',
                'bas_parent_id' => '2',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '6',
                'bas_value' => 'مدیر شرکت',
                'bas_type' => 'role_person',
                'bas_parent_id' => '2',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '7',
                'bas_value' => 'حسابدار',
                'bas_type' => 'role_person',
                'bas_parent_id' => '2',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '8',
                'bas_value' => 'انبار دار',
                'bas_type' => 'role_person',
                'bas_parent_id' => '2',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '9',
                'bas_value' => 'مدیر فروش',
                'bas_type' => 'role_person',
                'bas_parent_id' => '2',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '10',
                'bas_value' => 'جنسیت',
                'bas_type' => 'gender',
                'bas_parent_id' => '0',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '11',
                'bas_value' => 'زن',
                'bas_type' => 'gender',
                'bas_parent_id' => '10',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '12',
                'bas_value' => 'مرد',
                'bas_type' => 'gender',
                'bas_parent_id' => '10',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '13',
                'bas_value' => 'نوع مجموعه',
                'bas_type' => 'role_collection',
                'bas_parent_id' => '0',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '14',
                'bas_value' => 'کارخانه',
                'bas_type' => 'role_collection',
                'bas_parent_id' => '13',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '15',
                'bas_value' => 'شرکت فروش',
                'bas_type' => 'role_collection',
                'bas_parent_id' => '13',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '16',
                'bas_value' => 'عمده فروش',
                'bas_type' => 'role_collection',
                'bas_parent_id' => '13',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '17',
                'bas_value' => 'مغازه دار',
                'bas_type' => 'role_collection',
                'bas_parent_id' => '13',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '18',
                'bas_value' => 'نوع شرکت',
                'bas_type' => 'type_collection',
                'bas_parent_id' => '0',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '19',
                'bas_value' => 'نوع کسب و کار',
                'bas_type' => 'type_class',
                'bas_parent_id' => '0',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '21',
                'bas_value' => 'وضعیت پنل مجموعه ها',
                'bas_type' => 'condition-panel-collection',
                'bas_parent_id' => '0',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '22',
                'bas_value' => 'غیرفعال',
                'bas_type' => 'condition-panel-collection',
                'bas_parent_id' => '21',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '23',
                'bas_value' => 'فعال',
                'bas_type' => 'condition-panel-collection',
                'bas_parent_id' => '21',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '24',
                'bas_value' => 'نوع فایل',
                'bas_type' => 'type_file',
                'bas_parent_id' => '0',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '25',
                'bas_value' => 'مدارک کسب و کار',
                'bas_type' => 'type_file',
                'bas_parent_id' => '24',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '26',
                'bas_value' => 'مدارک اهراز هویتی',
                'bas_type' => 'type_file',
                'bas_parent_id' => '24',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '27',
                'bas_value' => 'لوگو',
                'bas_type' => 'type_file',
                'bas_parent_id' => '24',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '28',
                'bas_value' => 'آواتار',
                'bas_type' => 'type_file',
                'bas_parent_id' => '24',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '29',
                'bas_value' => 'نوع دسته بندی',
                'bas_type' => 'category_type',
                'bas_parent_id' => '0',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '30',
                'bas_value' => 'اصلی',
                'bas_type' => 'category_type',
                'bas_parent_id' => '29',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '31',
                'bas_value' => 'فرعی',
                'bas_type' => 'category_type',
                'bas_parent_id' => '29',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '32',
                'bas_value' => 'واحد اندازه گیری',
                'bas_type' => 'measure_unit',
                'bas_parent_id' => '0',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '33',
                'bas_value' => 'متر',
                'bas_type' => 'measure_unit',
                'bas_parent_id' => '32',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '34',
                'bas_value' => 'کیلوگرم',
                'bas_type' => 'measure_unit',
                'bas_parent_id' => '32',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '35',
                'bas_value' => 'لیتر',
                'bas_type' => 'measure_unit',
                'bas_parent_id' => '32',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '36',
                'bas_value' => 'نوع آپلود',
                'bas_type' => 'typeUpload',
                'bas_parent_id' => '0',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '37',
                'bas_value' => 'مدارک هویتی',
                'bas_type' => 'typeUpload',
                'bas_parent_id' => '36',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '38',
                'bas_value' => 'مدارک کسب و کار',
                'bas_type' => 'typeUpload',
                'bas_parent_id' => '36',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '39',
                'bas_value' => 'مدیریت سامانه',
                'bas_type' => 'panel_type',
                'bas_parent_id' => '36',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '41',
                'bas_value' => 'کارخانه',
                'bas_type' => 'panel_type',
                'bas_parent_id' => '36',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '42',
                'bas_value' => 'شرکت',
                'bas_type' => 'panel_type',
                'bas_parent_id' => '36',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '43',
                'bas_value' => 'عمده فروش',
                'bas_type' => 'panel_type',
                'bas_parent_id' => '36',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '44',
                'bas_value' => 'مغازه دار',
                'bas_type' => 'panel_type',
                'bas_parent_id' => '36',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '45',
                'bas_value' => 'ویزیتور',
                'bas_type' => 'panel_type',
                'bas_parent_id' => '36',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '46',
                'bas_value' => 'نمایندگی',
                'bas_type' => 'panel_type',
                'bas_parent_id' => '36',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '47',
                'bas_value' => 'خواروبار فروشی',
                'bas_type' => 'type_class',
                'bas_parent_id' => '19',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '48',
                'bas_value' => 'کامپیوتری',
                'bas_type' => 'type_class',
                'bas_parent_id' => '19',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '49',
                'bas_value' => 'روش های پرداخت',
                'bas_type' => 'type_payment',
                'bas_parent_id' => '0',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '50',
                'bas_value' => 'آنلاین',
                'bas_type' => 'type_payment',
                'bas_parent_id' => '49',
                'bas_status' => '1',
                'bas_extra_value' => '1',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '51',
                'bas_value' => 'چک',
                'bas_type' => 'type_payment',
                'bas_parent_id' => '49',
                'bas_status' => '1',
                'bas_extra_value' => '2',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '52',
                'bas_value' => 'حضوری',
                'bas_type' => 'type_payment',
                'bas_parent_id' => '49',
                'bas_status' => '1',
                'bas_extra_value' => '3',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '53',
                'bas_value' => 'کیف پول',
                'bas_type' => 'type_payment',
                'bas_parent_id' => '49',
                'bas_status' => '1',
                'bas_extra_value' => '4',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '54',
                'bas_value' => 'علت های مرجویی',
                'bas_type' => 'return_reason',
                'bas_parent_id' => '0',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '55',
                'bas_value' => 'مخدوش بودن کالا',
                'bas_type' => 'return_reason',
                'bas_parent_id' => '54',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '56',
                'bas_value' => 'تاریخ مصرف گذشته',
                'bas_type' => 'return_reason',
                'bas_parent_id' => '54',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '57',
                'bas_value' => 'واحد وزن',
                'bas_type' => 'weight_unit',
                'bas_parent_id' => '0',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '58',
                'bas_value' => 'گرم',
                'bas_type' => 'weight_unit',
                'bas_parent_id' => '57',
                'bas_status' => '1',
                'bas_extra_value' => '/',
                'bas_extra_value1' => '1000',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '59',
                'bas_value' => 'کیلو',
                'bas_type' => 'weight_unit',
                'bas_parent_id' => '57',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '60',
                'bas_value' => 'تن',
                'bas_type' => 'weight_unit',
                'bas_parent_id' => '57',
                'bas_status' => '1',
                'bas_extra_value' => '*',
                'bas_extra_value1' => '1000',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '61',
                'bas_value' => 'واحد طول و عرض',
                'bas_type' => 'width_unit',
                'bas_parent_id' => '0',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '62',
                'bas_value' => 'اینچ',
                'bas_type' => 'width_unit',
                'bas_parent_id' => '61',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '63',
                'bas_value' => 'سانتی متر',
                'bas_type' => 'width_unit',
                'bas_parent_id' => '61',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
            [
                'bas_id' => '64',
                'bas_value' => 'متر',
                'bas_type' => 'width_unit',
                'bas_parent_id' => '61',
                'bas_status' => '1',
                'bas_extra_value' => '0',
                'bas_extra_value1' => '0',
                'bas_can_user_add' => '0',
            ],
        ];
        \App\Baseinfo::insert($baseinfos);

//        \Illuminate\Support\Facades\DB::insert("
//        INSERT INTO baseinfos (bas_id, bas_type, bas_value, bas_parent_id, bas_status, bas_extra_value, bas_can_user_add) VALUES
//
////        ('1', 'unknown', N'نامشخص', '0', '0', '0', '0')
////        ,('2', 'role_person', N'نوع اشخاص', '0', '1', '0', '0')
//        ,('3', 'role_person', N'مدیر پنل', '1', '1', '0', '0')
//        ,('4', 'role_person', N'مغازه دار', '1', '1', '0', '0')
//        ,('5', 'role_person', N'ویزیتور', '1', '1', '0', '0')
//        ,('6', 'role_person', N'مدیر شرکت', '1', '1', '0', '0')
//        ,('7', 'role_person', N'حسابدار', '1', '1', '0', '0')
//        ,('8', 'role_person', N'انبار دار', '1', '1', '0', '0')
//        ,('9', 'role_person', N'مدیر فروش', '0', '1', '0', '0')
//        ,('10', 'gender', N'جنسیت', '0', '1', '0', '0')
//        ,('11', 'gender', N'زن', '9', '1', '0', '0')
//        ,('12', 'gender', N'مرد', '9', '1', '0', '0')
//        ,('13', 'role_collection', N'نوع مجموعه', '0', '1', '0', '0')
//        , ('14', 'role_collection', N'کارخانه', '12', '1', '0', '0')
//        , ('15', 'role_collection', N'شرکت فروش', '12', '1', '0', '0')
//        , ('16', 'role_collection', N'عمده فروش', '12', '1', '0', '0')
//        , ('17', 'role_collection', N'مغازه دار', '12', '1', '0', '0')
//        ,('18', 'type_collection', N'نوع شرکت', '0', '1', '0', '0')
//        ,('19', 'type_class', N'نوع کسب و کار', '0', '1', '0', '0')
//        ,('20', 'condition-panel-collection', N'وضعیت پنل مجموعه ها', '0', '1', '0', '0')
//        ,('21', 'condition-panel-collection', N'غیرفعال', '0', '1', '0', '0')
//        ,('22', 'condition-panel-collection', N'فعال', '0', '1', '0', '0')
//        ,('23', 'type_file', N'نوع فایل', '0', '1', '0', '0')
//        ,('24', 'type_file', N'مدارک کسب و کار', '23', '1', '0', '1')
//        ,('25', 'type_file', N'مدارک اهراز هویتی', '0', '1', '0', '1')
//        ,('26', 'type_file', N'لوگو', '0', '1', '0', '1')
//        ,('27', 'type_file', N'آواتار', '0', '1', '0', '1')
//        ,('28', 'category_type', N'نوع دسته بندی', '0', '1', '0', '1')
//        ,('29', 'category_type', N'اصلی', '28', '1', '0', '1')
//        ,('30', 'category_type', N'فرعی ', '28', '1', '0', '1')
//        ,('31', 'measure_unit', N'واحد اندازه گیری', '0', '1', '0', '1')
//        ,('32', 'measure_unit', N'متر', '31', '1', '0', '1')
//        ,('33', 'measure_unit', N'کیلوگرم', '31', '1', '0', '1')
//        ,('34', 'measure_unit', N'لیتر', '31', '1', '0', '1')
//        ,('35', 'typeUpload', N'نوع آپلود', '0', '1', '0', '1')
//        ,('36', 'typeUpload', N'مدارک هویتی', '35', '1', '0', '1')
//        ,('37', 'typeUpload', N'مدارک کسب و کار', '35', '1', '0', '1')
//        ,('38', 'panel_type', N'مدیریت سامانه', '35', '1', '0', '1')
//        ,('39', 'panel_type', N'کارخانه', '35', '1', '0', '1')
//        ,('40', 'panel_type', N'شرکت', '35', '1', '0', '1')
//        ,('41', 'panel_type', N'عمده فروش', '35', '1', '0', '1')
//        ,('42', 'panel_type', N'مغازه دار', '35', '1', '0', '1')
//        ,('43', 'panel_type', N'ویزیتور', '35', '1', '0', '1')
//        ,('44', 'panel_type', N'نمایندگی', '35', '1', '0', '1')
//        ");
        \Illuminate\Support\Facades\DB::unprepared("SET IDENTITY_INSERT baseinfos off");

    }
}
