<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::unprepared("SET IDENTITY_INSERT users ON");

        \App\User::create([
            'user_id' => 1,
            'username' => 'unkhow',
            'password' => bcrypt('123321'),
            'code' => '73',
            'panel_type' => '38',
            'is_active' => '1',
        ]);
        \App\User::create([
            'user_id' => 2,
            'username' => 'admin',
            'password' => bcrypt('123321'),
            'code' => '73',
            'panel_type' => '38',
            'is_active' => '1',
        ]);

        \Illuminate\Support\Facades\DB::unprepared("SET IDENTITY_INSERT users off");
    }
}
