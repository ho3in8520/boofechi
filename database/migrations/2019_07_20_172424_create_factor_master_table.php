<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactorMasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factor_master', function (Blueprint $table) {
            $table->bigIncrements('fm_id');
            $table->string('fm_no', '50')->default('-');
            $table->string('fm_session_user', '255')->nullable();
            $table->char('fm_date', '10')->default('____/__/__');
            $table->unsignedInteger('fm_time')->default(0);
            $table->unsignedBigInteger('fm_collection_id')->default(0);
            $table->unsignedBigInteger('fm_self_collection_id')->default(0);
            $table->unsignedBigInteger('fm_creator_id')->default(1);
            $table->unsignedBigInteger('fm_porsant_user_id')->default(0);
            $table->unsignedBigInteger('fm_payment_method')->default(1);
            $table->unsignedBigInteger('fm_assigned_role_id')->default(0);
            $table->unsignedBigInteger('fm_assigned_user_id')->default(0);
            $table->float('fm_payment_method_discount')->default(0);
            $table->float('fm_festival_discount')->default(0);
            $table->tinyInteger('fm_status')->default(0);
            $table->integer('fm_created_at')->default(0);
            $table->float('fm_returns_sum')->default(0);
            $table->float('fm_amount')->default(0);
            $table->tinyInteger('fm_tax')->default(0);
            $table->string('fm_description')->nullable();
            $table->boolean('fm_mode')->comment('0 => open , 1 => closed')->default(0);
            $table->boolean('fm_calculated')->comment('0 => no , 1 => yes')->default(0);
            $table->boolean('fm_is_payed')->comment('0 => no , 1 => yes')->default(0);
            $table->timestamps();
            $table->engine = "InnoDB";
            $table->foreign('fm_collection_id')->references('coll_id')->on('collections');
            $table->foreign('fm_creator_id')->references('user_id')->on('users');
//            $table->foreign('fm_payment_method')->references('cpm_id')->on('collection_payment_methods');
        });
        Schema::create('factor_detail', function (Blueprint $table) {
            $table->bigIncrements('fd_id');
            $table->unsignedBigInteger('fd_master_id');
            $table->unsignedBigInteger('fd_product_id');
            $table->unsignedBigInteger('fd_count')->default(1);
            $table->float('fd_price_buy')->default(0);
            $table->float('fd_price_consumer')->default(0);
            $table->float('fd_discount')->default(0);
            $table->boolean('fd_arzesh_afzoodeh');
            $table->boolean('fd_ooe')->comment('ooe is: original or eshant | 0 => original,1 => eshant')->default(0);
            $table->unsignedBigInteger('fd_ooe_id')->comment('eshant for ...')->default(0);
            $table->engine = "InnoDB";
            $table->foreign('fd_master_id')->references('fm_id')->on('factor_master');
            $table->foreign('fd_product_id')->references('cp_id')->on('collection_products');
        });
        Schema::create('factor_returns', function (Blueprint $table) {
            $table->bigIncrements('fr_id');
            $table->unsignedBigInteger('fr_master_id');
            $table->unsignedBigInteger('fr_fd_id');
            $table->unsignedBigInteger('fr_count')->default(1);
            $table->unsignedBigInteger('fr_reason_id')->comment("from collection_return_reasons");
            $table->boolean('fr_is_accepted')->default(0);
            $table->boolean('fr_type')->default(0)->comment('0 =>sefaresh dehande zade,1 =>sefaresh girande zade');
            $table->float('fr_accepted_count')->default(0);
            $table->foreign('fr_master_id')->references('fm_id')->on('factor_master');
            $table->foreign('fr_reason_id')->references('crr_id')->on('collection_return_reasons');
            $table->foreign('fr_fd_id')->references('fd_id')->on('factor_detail');
            $table->engine = "InnoDB";
        });
        Schema::create('collection_factor_complimentary', function (Blueprint $table) {
            $table->bigIncrements('cfc_id');
            $table->unsignedBigInteger('cfc_collection_id')->default(1);
            $table->boolean('cfc_type')->comment('0 => discount, 1 => eshant');
            $table->integer('cfc_buy_from')->default(0);
            $table->integer('cfc_buy_to')->default(0);
            $table->float('cfc_discount_percent')->default(0);
            $table->unsignedBigInteger('cfc_free_collection_product_id')->default(1)->comment("from collection_products prod_id");
            $table->float('cfc_free_count')->default(0);
            $table->integer('cfc_date_from')->default(0)->comment('timestamp. if 0 then unlimited');
            $table->integer('cfc_date_to')->default(0)->comment('timestamp. if 0 then unlimited');
            $table->boolean('cfc_overflow')->default(1)->comment('1=> agar az marz oboor kard mohasebe shavad?');

            $table->foreign('cfc_collection_id')->references('coll_id')->on('collections');
            $table->engine = "InnoDB";
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collection_factor_complimentary');
        Schema::dropIfExists('factor_returns');
        Schema::dropIfExists('factor_detail');
        Schema::dropIfExists('factor_master');
    }
}
