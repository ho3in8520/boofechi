<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSystemFactorMaster extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_factor_master', function (Blueprint $table) {
            $table->bigIncrements('sfm_id');
            $table->integer('sfm_created_at');
            $table->unsignedBigInteger('sfm_collection_id');
            $table->unsignedBigInteger('sfm_creator_id');
            $table->string('sfm_description', 4000);
            $table->float('sfm_amount');
            $table->boolean('sfm_is_payed');
            $table->foreign('sfm_collection_id')->references('coll_id')->on('collections');
            $table->foreign('sfm_creator_id')->references('user_id')->on('users');
        });

        Schema::create('system_factor_detail', function (Blueprint $table) {
            $table->bigIncrements('sfd_id');
            $table->unsignedBigInteger('sfd_master_id');
            $table->string('sfd_request_title',2000);
            $table->float('sfd_request_amount');
            $table->float('sfd_request_discount');
            $table->foreign('sfd_master_id')->references('sfm_id')->on('system_factor_master');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_factor_detail');
        Schema::dropIfExists('system_factor_master');
    }
}