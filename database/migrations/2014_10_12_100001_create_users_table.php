<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('user_id');
            $table->string('username')->unique();
            $table->string('code', '100')->comment('format code : 125-state-1242-city-random(number)');
            $table->string('email', '190')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->boolean('is_active')->default(0);
            $table->float('amount')->default(0);
            $table->unsignedBigInteger('panel_type')->default(44);
            $table->string('password');
            $table->integer('otp');
            $table->rememberToken();
            $table->timestamps();
            $table->bigInteger('creator')->default(0);
            $table->engine = "InnoDB";
            $table->foreign('panel_type')->references('bas_id')->on('baseinfos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
