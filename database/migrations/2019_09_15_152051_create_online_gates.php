<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOnlineGates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('online_gates', function (Blueprint $table) {
            $table->bigIncrements('og_id');
            $table->string('og_title',100);
            $table->string('og_description',4000)->nullable();
            $table->string('og_request_method',500);
            $table->string('og_verify_method',500);
            $table->float('og_minimum_amount');
            $table->boolean('og_status');
        });
        Schema::create('collection_online_gates', function (Blueprint $table) {
            $table->bigIncrements('cog_id');
            $table->unsignedBigInteger('cog_gate_id');
            $table->unsignedBigInteger('cog_collection_id');
            $table->string('cog_param1',500);
            $table->string('cog_param2',500);
            $table->string('cog_param3',500);
            $table->boolean('cog_status');
            $table->foreign('cog_gate_id')->references('og_id')->on('online_gates');
            $table->foreign('cog_collection_id')->references('coll_id')->on('collections');
        });

        Schema::create('advertising_master', function (Blueprint $table) {
            $table->bigIncrements('adm_id');
            $table->unsignedBigInteger('adm_creator');
            $table->integer('adm_created_at');
            $table->integer('adm_start_at');
            $table->integer('adm_expire_at');
            $table->unsignedBigInteger('adm_collection_id');
            $table->string('adm_description',4000)->nullable(true);
            $table->unsignedBigInteger('adm_station_id');
            $table->foreign('adm_collection_id')->references('coll_id')->on('collections');
            $table->foreign('adm_creator')->references('user_id')->on('users');
            $table->foreign('adm_station_id')->references('bas_id')->on('baseinfos');
        });
        Schema::create('advertising_detail', function (Blueprint $table) {
            $table->bigIncrements('add_id');
            $table->unsignedBigInteger('add_master_id');
            $table->unsignedBigInteger('add_panel_type');
            $table->unsignedBigInteger('add_city_id');
            $table->foreign('add_master_id')->references('adm_id')->on('advertising_master');
            $table->foreign('add_panel_type')->references('bas_id')->on('baseinfos');
            $table->foreign('add_city_id')->references('c_id')->on('cities');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collection_online_gates');
        Schema::dropIfExists('online_gates');
    }
}
