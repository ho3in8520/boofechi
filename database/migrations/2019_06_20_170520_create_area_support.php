<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreaSupport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('area_support', function (Blueprint $table) {
            $table->bigIncrements('as_id');
            $table->unsignedBigInteger('as_collection_id')->default(1)->comment("from collections coll_id");
            $table->unsignedBigInteger('as_user_id')->default(1)->comment("from collections coll_id");
            $table->unsignedBigInteger('as_city_id')->default(1)->comment("from cities city_id");
            $table->foreign('as_collection_id')->references('coll_id')->on('collections');
            //category ha marboot be che useri ast?
            $table->foreign('as_user_id')->references('user_id')->on('users');
            $table->foreign('as_city_id')->references('c_id')->on('cities');
            $table->engine = "InnoDB";
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('area_support');
    }
}
