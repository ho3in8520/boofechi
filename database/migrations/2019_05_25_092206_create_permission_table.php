<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permissions', function (Blueprint $table) {
            $table->bigIncrements('perm_id');
            $table->string('perm_name');
            $table->string('perm_label');
            $table->boolean('perm_status')->comment('0 = deActive , 1 = Active');
            $table->integer('perm_parent_id')->default(0);
            $table->integer('perm_fm_id');
            $table->string('perm_link');
            $table->string('perm_icon');


            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permissions');
    }
}
