<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCollectionReturnReasons extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collection_return_reasons', function (Blueprint $table) {
            $table->bigIncrements('crr_id');
            $table->unsignedBigInteger('crr_collection_id')->default(1);
            $table->unsignedBigInteger('crr_bas_id')->default(1);//reason
            $table->boolean('crr_return_to_inventory')->default(0);
            $table->foreign('crr_collection_id')->references('coll_id')->on('collections');
            $table->foreign('crr_bas_id')->references('bas_id')->on('baseinfos');
            $table->engine = "InnoDB";
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collection_return_reasons');
    }
}
