<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonalityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('persons', function (Blueprint $table) {
            $table->bigIncrements('prsn_id');
            $table->string('prsn_name', '30')->nullable();
            $table->string('prsn_family', '30')->nullable();
            $table->unsignedBigInteger('prsn_user_id')->default(1)->comment('from users user_id');
            $table->unsignedBigInteger('prsn_gender_id')->default(1)->comment('from baseinfo gender');
            $table->string('prsn_father_name', '50')->nullable()->nullable();
            $table->string('prsn_birthday')->nullable();
            $table->string('prsn_national_code', '10')->nullable();
            $table->string('prsn_certificate_code', '15')->nullable();
            $table->string('prsn_serial_certificate_code', '15')->nullable();
            $table->string('prsn_mobile1', '11')->nullable();
            $table->string('prsn_mobile2', '11')->nullable();
            $table->string('prsn_phone1', '11')->nullable();
            $table->string('prsn_phone2', '11')->nullable();
            $table->string('prsn_post_code', '40')->nullable();
            $table->string('prsn_address', '1000')->nullable();
            $table->string('prsn_job', '100')->nullable();
            $table->unsignedBigInteger('prsn_state_id')->comment('from cities')->default(100000);
            $table->unsignedBigInteger('prsn_city_id')->comment('from cities')->default(100000);
            $table->boolean('prsn_status')->default(1)->comment('0 = deActive , 1 = Active');
            $table->boolean('prsn_is_deleted')->default('0');

            $table->foreign('prsn_gender_id')->references('bas_id')->on('baseinfos');
            $table->foreign('prsn_state_id')->references('c_id')->on('cities');
            $table->foreign('prsn_city_id')->references('c_id')->on('cities');

            $table->engine = 'InnoDB';
        });


        Schema::create('collections', function (Blueprint $table) {
            $table->bigIncrements('coll_id');
            $table->string('coll_name', '100')->nullable();
            $table->string('coll_economic_code', '30')->nullable();
            $table->string('coll_national_number', '30')->nullable();
            $table->boolean('coll_is_deleted')->default(0);
            $table->string('coll_email','190')->nullable();
            $table->string('coll_site_address','190')->nullable();
            $table->string('coll_post_code', '15')->nullable();
            $table->string('coll_address', '1000')->nullable();
            $table->unsignedBigInteger('coll_state_id')->default(100000);
            $table->unsignedBigInteger('coll_city_id')->default(100000);

            $table->foreign('coll_state_id')->references('c_id')->on('cities');
            $table->foreign('coll_city_id')->references('c_id')->on('cities');

            $table->engine = 'InnoDB';

        });


        Schema::create('collection_persons',function(Blueprint $table){
            $table->bigIncrements('cp_id');
            $table->unsignedBigInteger('cp_prsn_id');
            $table->unsignedBigInteger('cp_coll_id');

            $table->foreign('cp_prsn_id')->references('prsn_id')->on('persons');
            $table->foreign('cp_coll_id')->references('coll_id')->on('collections');

            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collection_persons');
        Schema::dropIfExists('persons');
        Schema::dropIfExists('collections');
    }
}
