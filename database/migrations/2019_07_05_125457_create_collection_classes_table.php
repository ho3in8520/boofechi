<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCollectionClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collection_classes', function (Blueprint $table) {
            $table->bigIncrements('coc_id');
            $table->unsignedBigInteger('coc_coll_id');
            $table->unsignedBigInteger('coc_typeClass_id')->default(1)->comment('from baseinfos type_class');
            $table->timestamps();

            $table->engine = 'InnoDB';

            $table->foreign('coc_coll_id')->references('coll_id')->on('collections');
            $table->foreign('coc_typeClass_id')->references('bas_id')->on('baseinfos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collection_classes');
    }
}
