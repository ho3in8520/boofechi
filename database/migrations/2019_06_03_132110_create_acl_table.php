<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAclTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->bigIncrements('rol_id');
            $table->string('rol_name');
            $table->string('rol_label');
            $table->unsignedBigInteger('rol_creator');
            $table->engine = 'InnoDB';
            $table->foreign('rol_creator')->references('user_id')->on('users');
        });

        Schema::create('role_permissions', function (Blueprint $table) {
            $table->bigIncrements('rp_id');
            $table->unsignedBigInteger('rp_rol_id');
            $table->unsignedBigInteger('rp_perm_id');


            $table->foreign('rp_rol_id')->references('rol_id')->on('roles')->onDelete('cascade');
            $table->foreign('rp_perm_id')->references('perm_id')->on('permissions')->onDelete('cascade');


            $table->engine = "InnoDB";
        });

        Schema::create('user_roles', function (Blueprint $table) {
            $table->bigIncrements('ur_id');
            $table->unsignedBigInteger('ur_user_id');
            $table->unsignedBigInteger('ur_rol_id');


            $table->foreign('ur_user_id')->references('user_id')->on('users')->onDelete('cascade');
            $table->foreign('ur_rol_id')->references('rol_id')->on('roles')->onDelete('cascade');


            $table->engine = "InnoDB";
        });

        Schema::create('class_roles', function (Blueprint $table) {
            $table->bigIncrements('clr_id');
            $table->unsignedBigInteger('clr_class_id');
            $table->unsignedBigInteger('clr_type_id')->comment('panel_type');
            $table->unsignedBigInteger('clr_role_id');
            $table->foreign('clr_class_id')->references('bas_id')->on('baseinfos');
            $table->foreign('clr_type_id')->references('bas_id')->on('baseinfos');
            $table->foreign('clr_role_id')->references('rol_id')->on('roles')->onDelete('cascade');
            $table->engine = "InnoDB";
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_roles');
        Schema::dropIfExists('class_roles');
        Schema::dropIfExists('collection_roles');
        Schema::dropIfExists('role_permissions');
        Schema::dropIfExists('roles');


    }
}
