<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCollectionPaymentMethods extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collection_payment_methods', function (Blueprint $table) {
            $table->bigIncrements('cpm_id');
            $table->unsignedBigInteger('cpm_collection_id')->default(1);
            $table->unsignedBigInteger('cpm_bas_id')->default(1);//payment_method
            $table->boolean('cpm_status')->comment("0=>disable , 1=>enable")->default(0);
            $table->float('cpm_percent')->default(0);
            $table->string('cpm_description',2000)->nullable();
            $table->foreign('cpm_collection_id')->references('coll_id')->on('collections');
            $table->foreign('cpm_bas_id')->references('bas_id')->on('baseinfos');
            $table->engine = "InnoDB";
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collection_payment_methods');
    }
}
