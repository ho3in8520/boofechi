<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUploadRouteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('upload_route', function (Blueprint $table) {
            $table->bigIncrements('ur_id');
            $table->unsignedBigInteger('ur_collection_id');
            $table->unsignedBigInteger('ur_fk_id')->nullable();
            $table->string('ur_path');
            $table->unsignedBigInteger('ur_type_id')->default(1)->comment('from baseinfos  type_file');
            $table->boolean('ur_status')->default(0)->comment('0 = deActive, 1 = Active');
            $table->boolean('ur_is_deleted')->default(0)->comment('0 = no, 1 = yes');
            $table->string('ur_table_name','500')->nullable();
            $table->string('ur_description','500')->nullable();
            $table->timestamps();
            $table->foreign('ur_collection_id')->references('coll_id')->on('collections');
            $table->foreign('ur_type_id')->references('bas_id')->on('baseinfos');

            $table->engine = "InnoDB";
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('upload_route');
    }
}
