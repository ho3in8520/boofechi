<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->bigIncrements('cat_id');
            $table->string('cat_name', '255');
            $table->boolean('cat_status')->default(1)->comment("0 = deActive, 1 = Active");
            $table->boolean('cat_type')->default(0)->comment("0 = category, 1 = line");
            $table->unsignedBigInteger('cat_senf_id')->default(1)->comment("from baseinfos type_class");
            $table->integer('cat_parent_id')->default(0);
            $table->engine = "InnoDB";
            $table->foreign('cat_senf_id')->references('bas_id')->on('baseinfos');
        });

        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('prod_id');
            $table->string('prod_name', '255');
            $table->longText('prod_description')->nullable();
            $table->unsignedBigInteger('prod_category_id')->default(1)->comment("from categories cat_id");
            $table->foreign('prod_category_id')->references('cat_id')->on('categories');
            $table->engine = "InnoDB";
        });

        Schema::create('collection_products', function (Blueprint $table) {
            $table->bigIncrements('cp_id');
            $table->string('cp_second_code', '50');
            $table->unsignedBigInteger('cp_collection_id')->default(1)->comment("from collections coll_id");
            $table->unsignedBigInteger('cp_product_id')->default(1)->comment("from products prod_id");
            $table->integer('cp_count_per')->default(0);
//            $table->float('cp_default_price_buy')->default(0);
            $table->float('cp_default_price_buy')->default(0);
            $table->float('cp_default_price_consumer')->default(0);
            $table->float('cp_default_count')->default(0);

            $table->boolean('cp_arzesh_afzoodeh')->default(0);

            $table->unsignedBigInteger('cp_measurement_unit_id')->default(1)->comment("from baseinfos bas_id");
            $table->float('cp_weight_of_each')->comment("vazn har adad")->default(0);
            $table->float('cp_dimension_length')->comment("tool")->default(0);
            $table->float('cp_dimension_width')->comment("arz")->default(0);
            $table->float('cp_weight_unit')->default(0);
            $table->float('cp_width_unit')->default(0);

            $table->foreign('cp_collection_id')->references('coll_id')->on('collections');
            $table->foreign('cp_product_id')->references('prod_id')->on('products');

            $table->timestamps();
            $table->engine = "InnoDB";
            $table->foreign('cp_measurement_unit_id')->references('bas_id')->on('baseinfos');
        });

        Schema::create('collection_categories', function (Blueprint $table) {
            $table->bigIncrements('cc_id');
            $table->unsignedBigInteger('cc_collection_id')->default(1);
            $table->unsignedBigInteger('cc_category_id')->default(1);
            $table->unsignedBigInteger('cc_user_id')->default(1);
            $table->engine = "InnoDB";
            $table->foreign('cc_user_id')->references('user_id')->on('users');
            $table->foreign('cc_collection_id')->references('coll_id')->on('collections');
            $table->foreign('cc_category_id')->references('cat_id')->on('categories');
        });

        Schema::create('product_complimentary', function (Blueprint $table) {
            $table->bigIncrements('pc_id');
            $table->unsignedBigInteger('pc_collection_product_id')->default(1)->comment("from collection_products cp_id");
            $table->boolean('pc_type')->comment('0 => discount, 1 => eshant');
            $table->float('pc_discount_percent')->default(0);
            $table->integer('pc_buy_count')->default(0);
            $table->float('pc_free_count')->default(0);
            $table->unsignedBigInteger('pc_free_collection_product_id')->default(1)->comment("from collection_products prod_id");
            $table->integer('pc_date_from')->default(0)->comment('timestamp. if 0 then unlimited');
            $table->integer('pc_date_to')->default(0)->comment('timestamp. if 0 then unlimited');
            $table->boolean('pc_overflow')->default(1)->comment('1=> agar az marz oboor kard mohasebe shavad?');

            $table->foreign('pc_collection_product_id')->references('cp_id')->on('collection_products');
            $table->engine = "InnoDB";
        });

        Schema::create('inventories', function (Blueprint $table) {
            $table->bigIncrements('inv_id');
            $table->string('inv_name', '255');
            $table->unsignedBigInteger('inv_collection_id')->default(1)->comment("from collections coll_id");

            $table->engine = "InnoDB";
            $table->foreign('inv_collection_id')->references('coll_id')->on('collections');
        });

        Schema::create('financial_cycles', function (Blueprint $table) {
            $table->bigIncrements('fc_id');
            $table->string('fc_name', '255');
            $table->string('fc_date_from', '10');
            $table->string('fc_date_to', '10');
            $table->boolean('fc_status')->default(1);
            $table->unsignedBigInteger('fc_collection_id')->default(1)->comment("from collections coll_id");

            $table->engine = "InnoDB";
            $table->foreign('fc_collection_id')->references('coll_id')->on('collections');
        });

        Schema::create('inventory_factor_master', function (Blueprint $table) {
            $table->bigIncrements('ifm_id');
            $table->string('ifm_no', '50')->default('-');
            $table->string('ifm_session_user', '255');
            $table->char('ifm_date', '10')->default('____/__/__');
            $table->unsignedBigInteger('ifm_inventory_id')->default(1)->comment("from inventories coll_id");
            $table->unsignedBigInteger('ifm_financial_cycle_id')->default(1)->comment("from financial_cycles coll_id");
            $table->unsignedBigInteger('ifm_user_id')->default(1)->comment("from collections coll_id");
            $table->boolean('ifm_type')->comment('0 => in , 1 => out')->default(0);
            $table->boolean('ifm_mode')->comment('0 => open , 1 => closed')->default(0);

            $table->timestamps();
            $table->engine = "InnoDB";
            $table->foreign('ifm_inventory_id')->references('inv_id')->on('inventories');
            $table->foreign('ifm_financial_cycle_id')->references('fc_id')->on('financial_cycles');
            $table->foreign('ifm_user_id')->references('user_id')->on('users');
        });

        Schema::create('inventory_factor_detail', function (Blueprint $table) {
            $table->bigIncrements('ifd_id');
            $table->unsignedBigInteger('ifd_factor_master_id')->default(1)->comment("from inventories inv_id");
            $table->unsignedBigInteger('ifd_collection_product_id')->default(1)->comment("from financial_cycles fc_id");
            $table->integer('ifd_count');
            $table->float('ifd_price_buy');
            $table->float('ifd_price_consumer');
            $table->engine = "InnoDB";
            $table->foreign('ifd_factor_master_id')->references('ifm_id')->on('inventory_factor_master');
            $table->foreign('ifd_collection_product_id')->references('cp_id')->on('collection_products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory_factor_detail');
        Schema::dropIfExists('inventory_factor_master');
        Schema::dropIfExists('financial_cycles');
        Schema::dropIfExists('collection_categories');
        Schema::dropIfExists('inventories');
        Schema::dropIfExists('product_complimentary');
        Schema::dropIfExists('collection_products');
        Schema::dropIfExists('products');
        Schema::dropIfExists('categories');
    }
}
