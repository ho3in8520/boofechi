<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePanelWizards extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('panel_wizards', function (Blueprint $table) {
            $table->bigIncrements('pw_id');
            $table->unsignedBigInteger('pw_panel_type_id');
            $table->string('pw_form_address');
            $table->boolean('pw_status')->default(1);
            $table->engine = "InnoDB";
            $table->foreign('pw_panel_type_id')->references('bas_id')->on('baseinfos')->comment('panel_type');
        });
        Schema::create('panel_wizard_completed', function (Blueprint $table) {
            $table->bigIncrements('pwc_id');
            $table->unsignedBigInteger('pwc_form_id');
            $table->unsignedBigInteger('pwc_user_id');
            $table->tinyInteger('pwc_accepted')->default(0);
            $table->string('pwc_reason', 1000)->nullable();
            $table->engine = "InnoDB";
            $table->timestamps();
            $table->foreign('pwc_form_id')->references('pw_id')->on('panel_wizards');
            $table->foreign('pwc_user_id')->references('user_id')->on('users');
            $table->unsignedBigInteger('updatetor_user_id');
        });

        Schema::create('factor_routing', function (Blueprint $table) {
            $table->bigIncrements('far_id');
            $table->boolean('far_is_viewed')->default(0);
            $table->unsignedBigInteger('far_user_viewed')->default(0);
            $table->unsignedBigInteger('far_role_id');
            $table->unsignedBigInteger('far_factor_id');
            $table->integer('far_created_at');
            $table->integer('far_viewed_at')->nullable();
            $table->engine = "InnoDB";
            $table->foreign('far_role_id')->references('rol_id')->on('roles');
            $table->foreign('far_factor_id')->references('fm_id')->on('factor_master');
        });

        Schema::create('collection_debt', function (Blueprint $table) {
            $table->bigIncrements('cd_id');
            $table->unsignedBigInteger('cd_source_collection_id');
            $table->unsignedBigInteger('cd_destination_collection_id');
            $table->float('cd_amount')->default(0);
            $table->engine = "InnoDB";
            $table->foreign('cd_source_collection_id')->references('coll_id')->on('collections');
            $table->foreign('cd_destination_collection_id')->references('coll_id')->on('collections');
        });

        Schema::create('panel_terms', function (Blueprint $table) {
            $table->bigIncrements('pt_id');
            $table->unsignedBigInteger('pt_panel_id');
            $table->string('pt_terms');
            $table->engine = "InnoDB";
            $table->foreign('pt_panel_id')->references('bas_id')->on('baseinfos');
        });

        Schema::create('tickets', function (Blueprint $table) {
            $table->bigIncrements('t_id');
            $table->unsignedBigInteger('t_parent_id')->default(0);
            $table->string('t_subject', 1000)->nullable();
            $table->string('t_description');
            $table->integer('t_created_at');
            $table->unsignedBigInteger('t_creator_user_id');
            $table->unsignedBigInteger('t_assigned_role_id')->default(0);
            $table->unsignedBigInteger('t_assigned_user_id')->default(0);
            $table->tinyInteger('t_status');
            $table->boolean('t_mode')->comment('0=>open,1=>closed');
            $table->engine = "InnoDB";
            $table->foreign('t_creator_user_id')->references('user_id')->on('users');
        });

        Schema::create('ticket_routing', function (Blueprint $table) {
            $table->bigIncrements('tr_id');
            $table->unsignedBigInteger('tr_ticket_id');
            $table->unsignedBigInteger('tr_role_id');
            $table->integer('tr_created_at');
            $table->boolean('tr_is_viewed')->default(0);
            $table->integer('tr_viewed_at')->default(0);
            $table->unsignedBigInteger('tr_viewed_user_id')->default(0);
            $table->engine = "InnoDB";
            $table->foreign('tr_ticket_id')->references('t_id')->on('tickets');
            $table->foreign('tr_role_id')->references('rol_id')->on('roles');
        });

        Schema::create('messages', function (Blueprint $table) {
            $table->bigIncrements('msg_id');
            $table->string('msg_title');
            $table->string('msg_message');
            $table->unsignedBigInteger('msg_sender_user_id');
            $table->unsignedBigInteger('msg_receiver_collection_id')->nullable();
            $table->integer('msg_created_at');
            $table->boolean('msg_is_read')->default(0);
            $table->boolean('msg_type')->default(0)->comment('0=>message 1=>report to admin');
            $table->unsignedBigInteger('msg_reader_user_id')->default(0);
            $table->integer('msg_read_at')->default(0);
            $table->engine = "InnoDB";
            $table->foreign('msg_sender_user_id')->references('user_id')->on('users');
        });

        Schema::create('collection_management_porsant', function (Blueprint $table) {
            $table->bigIncrements('cmp_id');
            $table->unsignedBigInteger('cmp_collection_id');
            $table->string('cmp_date_from', 10);
            $table->integer('cmp_date_to', 10);
            $table->float('cmp_price_from')->default(0);
            $table->float('cmp_price_to')->default(0);
            $table->float('cmp_percent')->default(0);
            $table->engine = "InnoDB";
            $table->foreign('cpm_collection_id')->references('coll_id')->on('collections');
        });

        Schema::create('agents', function (Blueprint $table) {
            $table->bigIncrements('ag_id');
            $table->string('ag_domain', 1000);
            $table->boolean('ag_status');
            $table->unsignedBigInteger('ag_parent_id')->default(0);
            $table->engine = "InnoDB";
        });

        Schema::create('announcement_viewed', function (Blueprint $table) {
            $table->bigIncrements('av_id');
            $table->unsignedBigInteger('av_ann_id');
            $table->unsignedBigInteger('av_user_id');
            $table->integer('av_time');
            $table->engine = "InnoDB";
            $table->foreign('av_ann_id')->references('ann_id')->on('announcements');
            $table->foreign('av_user_id')->references('user_id')->on('users');
        });

        Schema::create('transacts', function (Blueprint $table) {
            $table->bigIncrements('transact_id');
            $table->tinyInteger('transact_type')->comment('0=>charge kife pool , 1=> collection_factor payment ,2=> system_factor payment');
            $table->string('transact_refId', 50);
            $table->float('transact_amount');
            $table->unsignedBigInteger('transact_user_or_factor_id')->comment('if type is 0 user_id else if type is 1 or 2 factor_id');
            $table->integer('transact_time');
            $table->boolean('transact_status');
            $table->engine = "InnoDB";
        });

        Schema::create('violation_log', function (Blueprint $table) {
            $table->bigIncrements('vl_id');
            $table->unsignedBigInteger('vl_user_id');
            $table->tinyInteger('vl_type')->comment('0=>notification , 1=> block panel');
            $table->integer('vl_time');
            $table->engine = "InnoDB";
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collection_management_porsant');
        Schema::dropIfExists('ticket_routing');
        Schema::dropIfExists('tickets');
        Schema::dropIfExists('messages');
        Schema::dropIfExists('panel_terms');
        Schema::dropIfExists('collection_debt');
        Schema::dropIfExists('factor_routing');
        Schema::dropIfExists('panel_wizard_completed');
        Schema::dropIfExists('panel_wizards');
        Schema::dropIfExists('agents');
        Schema::dropIfExists('announcement_viewed');
        Schema::dropIfExists('transacts');
    }
}
