<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBaseinfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('baseinfos', function (Blueprint $table) {
            $table->bigIncrements('bas_id');
            $table->string('bas_type','100');
            $table->string('bas_value','100');
            $table->integer('bas_parent_id')->default(0);
            $table->boolean('bas_status')->default(1)->comment('0 = deActive , 1 = Active');
            $table->string('bas_extra_value','500')->default(0);
            $table->string('bas_extra_value1','500')->default(0);
            $table->boolean('bas_can_user_add')->default(0);
            $table->engine = "InnoDB";
        });

        Schema::create('cities',function(Blueprint $table){
            $table->bigIncrements('c_id');
            $table->string('c_name');
            $table->integer('c_parent_id')->default(0);
            $table->string('c_latitude','30')->nullable()->comment('with');
            $table->string('c_longitude','30')->nullable()->comment('height');

            $table->engine = "InnoDB";
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('baseinfos');
        Schema::dropIfExists('cities');
    }
}
