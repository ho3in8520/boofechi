<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Routines extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //add_announcement
        \Illuminate\Support\Facades\DB::unprepared('
            create procedure [dbo].[add_announcement](
               @mode tinyint, --mode = 1 => insert & mode = 2 --update
               @collection_id bigint = 0, -- if mode = 1
               @user_id bigint = 0, -- if mode = 1
               @title nvarchar(4000),
               @body nvarchar(max),
               @values nvarchar(max), --format: \'<Root><Data CT="1" TP="1"></Data></Root>\'  
               @timestmp datetime,
               @id bigint = 0 -- if mode = 2 
            )
            as 
            declare @already_added int;
             declare @start_date datetime
              declare @end_date datetime
             declare @return int
             declare @ann_id bigint
             declare @tbl_tmp table (CT bigint, TP bigint)
             declare @IXML int
            
             exec sp_xml_preparedocument @IXML output, @values
             insert into @tbl_tmp (CT, TP)
             select CT,TP
             from OPENXML(@IXML, \'/Root/Data\')   
             WITH (AN bigint, CT bigint,TP bigint)
            
             if @mode = 1
             begin
                 EXECUTE get_start_and_end_date \'2019-06-29 20:40:00\',\'str\',\'day\',0, @start_date OUTPUT
                 EXECUTE get_start_and_end_date \'2019-06-29 20:40:00\',\'end\',\'day\',0, @end_date OUTPUT
                 begin try
                    select @already_added = count(1) from announcement_limitation where anl_time between dbo.getTimeStmp(@start_date) and dbo.getTimeStmp(@end_date)
                        and anl_collection_id = @collection_id;
                        if @already_added = 0
                        begin
                            begin tran
                                insert into announcements(ann_title,ann_body,ann_collection_id,ann_user_id)
                                values(@title,@body,@collection_id,@user_id);
            
                                select @ann_id = SCOPE_IDENTITY()
            
                                insert into announcement_receivers (anr_announcement_id, anr_city_id, anr_type_id)
                                select @ann_id ,CT, TP
                                from @tbl_tmp	
                                group by CT, TP
            
                                insert into announcement_limitation(anl_collection_id,anl_time) values(@collection_id,dbo.getTimeStmp(@timestmp))
                            commit tran
                            select @return = 0
                        end
                        else
                          select @return = -1 --already added announcement
                 end try
              
                 begin catch
                   if @@TRANCOUNT > 0
                        rollback tran
                   select @return = -2
                 end catch
             end
             else if @mode = 2
             begin 
                begin try
                    begin tran
                        update announcements
                        set ann_title = @title,ann_body = @body, updated_at = @timestmp
                         where ann_id = @id
                        
                        delete from announcement_receivers where anr_announcement_id = @id
                        insert into announcement_receivers (anr_announcement_id, anr_city_id, anr_type_id)
                        select @id ,CT, TP
                        from @tbl_tmp	
                        group by CT, TP
                
                    commit tran
                    select @return = 0
                 end try
              
                 begin catch
                   if @@TRANCOUNT > 0
                        rollback tran
                   select @return = -2
                 end catch
             end
             select @return as result
        ');

        //add_product
        \Illuminate\Support\Facades\DB::unprepared('
           create PROCEDURE [dbo].[add_product](
                @name nvarchar(255),
                @description      nvarchar(max) = null,
                @category_id      BIGINT,
                @arzesh_afzoodeh  float,
                @measurement_unit BIGINT,
                @price_buy float,
                @price_consumer float,
                @weight           float,
                @tool             float,
                @arz              float,
                @user_id          BIGINT,
                @timestmp         datetime,
                @file_path         varchar(1000) = null,
                @cp_weight_unit float = 0,
                @cp_width_unit float = 0
            )
            as
            declare @return int 
            declare @prod_id bigint    
            declare @collection_id bigint    
            declare @cp_id bigint
        
            --if product not exist insert it and return id
        
            SELECT @prod_id = prod_id
            FROM products
            WHERE prod_name = LTRIM(RTRIM(@name))
                    AND prod_category_id = @category_id
            if @prod_id is null
            begin
                INSERT INTO products (prod_name, prod_description, prod_category_id)
                VALUES (LTRIM(RTRIM(@name)), @description, @category_id)
                SELECT @prod_id = scope_identity()
            end
                
            SELECT top 1 @collection_id = cp_coll_id
            FROM persons
                LEFT JOIN collection_persons ON cp_prsn_id = prsn_id
            WHERE prsn_user_id = @user_id;
            begin transaction
            begin try
                INSERT INTO collection_products (cp_second_code, cp_collection_id, cp_product_id, cp_default_price_buy,cp_default_price_consumer, cp_arzesh_afzoodeh, cp_measurement_unit_id, cp_weight_of_each, cp_dimension_length, cp_dimension_width, created_at, updated_at,cp_weight_unit,cp_width_unit)
                VALUES ((SELECT isnull(max(cast(cp.cp_second_code AS bigint)) , 99) + 1
                            FROM collection_products cp
                            WHERE cp.cp_collection_id = @collection_id), @collection_id, @prod_id, @price_buy,@price_consumer, @arzesh_afzoodeh,
                                                                        @measurement_unit, @weight, @tool, @arz, @timestmp, NULL,@cp_weight_unit,@cp_width_unit);
                select @return = scope_identity()
        
                insert into upload_route(ur_collection_id,ur_fk_id,ur_table_name,ur_is_deleted,ur_status,ur_path,created_at)
                values (@collection_id,@return,\'collection_products\',0,1,@file_path,getdate())
        
                commit;															
            end try
            begin catch
                rollback;
                select @return = -2 --has error
            end catch
        
            SELECT @return as result;      
        ');

        //add_role
        \Illuminate\Support\Facades\DB::unprepared('
            create procedure [dbo].[add_role](
               @mode tinyint, --mode = 1 => insert & mode = 2 --update
               @title_persian nvarchar(500),
               @title_english varchar(500),  
               @values nvarchar(max), --format: \'<Root><Data CT="1" TP="1"></Data></Root>\'  
               @id bigint = 0, -- if mode = 2 
               @creator bigint  = 0 -- if mode = 1
            )
            as 
             declare @return int
             declare @role_id bigint
             declare @tbl_tmp table (RL bigint)
             declare @IXML int
            
             exec sp_xml_preparedocument @IXML output, @values
             insert into @tbl_tmp (RL)
             select RL
             from OPENXML(@IXML, \'/Root/Data\')   
             WITH (AN bigint, RL bigint)
            
             if @mode = 1
             begin	
                 begin try
                        begin
                            begin tran
                                insert into roles(rol_label,rol_name,rol_creator)
                                values(@title_persian,@title_english,@creator);
            
                                select @role_id = SCOPE_IDENTITY()
            
                                insert into role_permissions(rp_rol_id,rp_perm_id)
                                select @role_id ,RL
                                from @tbl_tmp	
                                group by RL
            
                            commit tran
                            select @return = 0
                        end			
                 end try
              
                 begin catch
                   if @@TRANCOUNT > 0
                        rollback tran
                   select @return = -2
                 end catch
             end
             else if @mode = 2
             begin 
                begin try
                    begin tran
                        update roles
                        set rol_label = @title_persian,rol_name = @title_english
                         where rol_id = @id
                        
                        delete from role_permissions where rp_rol_id = @id
                        insert into role_permissions(rp_rol_id,rp_perm_id)
                        select @id ,RL
                        from @tbl_tmp	
                        group by RL
                
                    commit tran
                    select @return = 0
                 end try
              
                 begin catch
                   if @@TRANCOUNT > 0
                        rollback tran
                   select @return = -2
                 end catch
             end
             select @return as result
        ');

        //check_relation
        \Illuminate\Support\Facades\DB::unprepared('
            create PROCEDURE [dbo].[check_relation](
                @table_name VARCHAR(500),
                @id         BIGINT
            )
            as
                DECLARE @des_table VARCHAR(500);
                DECLARE @des_field VARCHAR(500);
                DECLARE @query_text varchar(max) = \'\'
                DECLARE @dest_table varchar(500)
                DECLARE @dest_field varchar(500)
                            
                DECLARE for_cursor CURSOR FOR
                    SELECT
                    tr_destination_table,
                    tr_destination_field
                    FROM table_relations
                    WHERE tr_source_table = @table_name AND tr_status = 1;
                            
                            
                OPEN for_cursor;
                FETCH NEXT FROM for_cursor INTO @dest_table, @dest_field
                WHILE @@FETCH_STATUS = 0  
                BEGIN  
                        SET @query_text = CONCAT(@query_text, \'select \', @dest_field, \' as ID from \', @dest_table,
                                            \' WHERE  \', @dest_field, \' = \', @id, \' union \');
                       FETCH NEXT FROM for_cursor INTO @dest_table, @dest_field
                END;
                CLOSE for_cursor;
                DEALLOCATE for_cursor;
                            
                --select @query_text as result
                if @query_text != \'\'      
                    begin      
                        SELECT @query_text = substring(@query_text, 1, len(@query_text) - 6)			
                        exec(@query_text)
                    end
                else
                    select 0 as ID
        ');

        //edit_product
        \Illuminate\Support\Facades\DB::unprepared('
            create PROCEDURE [dbo].[edit_product](
                @id              BIGINT,
                @name VARCHAR(255),
                @description varchar(max),
                @category_id     BIGINT, 
                @arzesh_afzoodeh float, 
                @measurement_unit BIGINT,
                @weight          float,
                @tool float,
                @arz float,
                @user_id BIGINT,
                @timestmp        DATETIME
            )
            as
                declare @prod_id bigint
                declare @collection_id bigint
                declare @return int
            
                --if product not exist insert it and return id
                SELECT @prod_id=prod_id
                FROM products
                WHERE prod_name = ltrim(rtrim(@name))
                AND prod_category_id = @category_id;
            
                IF @prod_id is null 
                    INSERT INTO products (prod_name, prod_description, prod_category_id)
                    VALUES (ltrim(rtrim(@name)), @description, @category_id);
                    SELECT @prod_id = SCOPE_IDENTITY()
                                
                SELECT top 1 @collection_id = cp_coll_id	
                FROM persons
                LEFT JOIN collection_persons ON cp_prsn_id = persons.prsn_id
                WHERE prsn_user_id = @user_id
            
                begin transaction
                begin try
                    UPDATE collection_products
                    SET cp_product_id  = @prod_id, cp_arzesh_afzoodeh = @arzesh_afzoodeh,
                    cp_measurement_unit_id = @measurement_unit, cp_weight_of_each = @weight, cp_dimension_length = @tool,
                    cp_dimension_width = @arz, updated_at = @timestmp
                    WHERE cp_id = @id
                    select @return = 0
                    commit
                end try
                    
                begin catch
                    rollback;
                    select @return = -2
                end catch
                SELECT @return AS result;

        ');

        //get_start_and_end_date
        \Illuminate\Support\Facades\DB::unprepared('
            create procedure [dbo].[get_start_and_end_date](
                @date datetime,
                @mode char(3) = \'str\',
                @type varchar(50) = \'day\',
                @count integer = 0,
                @last_time datetime OUTPUT 
            )
            as 
            begin
            declare @dt datetime;
            declare @stmt nvarchar(255)
            declare @test datetime
                if @mode = \'str\'
                    SET @dt = CONVERT(DATETIME, CONVERT(varchar(11),@date, 131  ) + \' 00:00:00\', 131 ) 
                else if @mode = \'end\'
                    SET @dt = CONVERT(DATETIME, CONVERT(varchar(11),@date, 131  ) + \' 23:59:59\', 131 )
                    
                set @stmt = concat(\'declare @dt datetime \',\'declare @count int \',\'declare @dtOutput datetime\')
                set @stmt = concat(@stmt,\' set @dt =  \'\'\' , format(@dt, \'yyyy-MM-dd HH:mm:ss.fff\') ,\'\'\'\')
                set @stmt = concat(@stmt,\' set @count = \' , @count)
                set @stmt = concat(@stmt,\' set @dtOutput = DATEADD(\',@type,\',@count,@dt)\')
                set @stmt = concat(@stmt , \'select @dtOutput as result\')
                CREATE TABLE #tmptbl
                (
                   dt datetime
                )
                INSERT INTO #tmptbl
                EXECUTE sp_executesql @stmt
                select @last_time = dt from #tmptbl
            end
        ');

        //inventory_factor
        \Illuminate\Support\Facades\DB::unprepared('
            create PROCEDURE [dbo].[inventory_factor](	
                @inv_id BIGINT,
                @user_id BIGINT, 
                @fc_id BIGINT,
                @user_session_id VARCHAR(255),
                @type            TINYINT,
                @collection_id   BIGINT,
                @shamsi_date     char(10),
                @created_at      datetime,
                @values nvarchar(max),
                @updated_at      datetime = null
                )
            as
                declare @return tinyint
                declare @ifm_id bigint
                
                begin tran 
                begin try
                    declare @tbl_tmp table (PRD bigint,PRB float,PRC float,CNT float)
                    declare @IXML int
            
                    exec sp_xml_preparedocument @IXML output, @values
                    insert into @tbl_tmp (PRD,PRB,PRC,CNT)
                    select PRD,PRB,PRC,CNT
                    from OPENXML(@IXML, \'/Root/Data\')   
                    WITH (PRD bigint,PRB float,PRC float,CNT float)
            
                    INSERT INTO inventory_factor_master (ifm_session_user, ifm_inventory_id, ifm_user_id, ifm_financial_cycle_id, ifm_type)
                        VALUES (@user_session_id, @inv_id, @user_id, @fc_id, @type);
                        SELECT @ifm_id = scope_identity()
            
                    INSERT INTO inventory_factor_detail (ifd_collection_product_id, ifd_factor_master_id, ifd_price_buy, ifd_price_consumer, ifd_count)
                    select PRD,@ifm_id ,PRB,PRC,CNT
                    from @tbl_tmp	
                    group by PRD,PRB,PRC,CNT
            
            
                    DECLARE @PRD bigint 
                    DECLARE @PRB float
                    DECLARE @PRC float 
                    DECLARE @CNT float
            
                    if @type = 0 -- agar factor vorodi hast count , price ro update kon
                    begin
                        DECLARE update_price_and_count_cursor CURSOR FAST_FORWARD
                        FOR select PRD,PRB,PRC,CNT
                            from @tbl_tmp
                        OPEN update_price_and_count_cursor
                        FETCH NEXT FROM update_price_and_count_cursor INTO @PRD,@PRB,@PRC,@CNT
                        WHILE @@FETCH_STATUS = 0
                        BEGIN
                            exec update_count_and_price @PRD,@PRB,@PRC,@CNT
                            FETCH NEXT FROM update_price_and_count_cursor INTO @PRD,@PRB,@PRC,@CNT
                        END
                        CLOSE update_price_and_count_cursor
                        DEALLOCATE update_price_and_count_cursor
                    end
                    else if @type = 1 -- agar factor khoroji hast count ro update kon
                    begin		
                        DECLARE update_price_and_count_cursor CURSOR FAST_FORWARD
                        FOR select PRD, CNT
                            from @tbl_tmp
                        OPEN update_price_and_count_cursor
                        FETCH NEXT FROM update_price_and_count_cursor INTO @PRD,@CNT
                        WHILE @@FETCH_STATUS = 0
                        BEGIN
                            exec update_count_and_price @prod_id = @PRD,  @count = @CNT
                            FETCH NEXT FROM update_price_and_count_cursor INTO @PRD,@CNT
                        END
                        CLOSE update_price_and_count_cursor
                        DEALLOCATE update_price_and_count_cursor
                    end
                    UPDATE inventory_factor_master
                    SET
                        ifm_mode = 1,
                        ifm_date = @shamsi_date,
                        ifm_type = @type,
                        ifm_no   = (
                            SELECT x.max_no
                            FROM (
                                    SELECT max(ifmaster.ifm_no) + 1 AS max_no
                                    FROM inventory_factor_master AS ifmaster
                                        LEFT JOIN inventories ON inv_id = ifmaster.ifm_inventory_id
                                        LEFT JOIN collections ON inv_collection_id = coll_id
                                    WHERE coll_id = @collection_id
                                    ) x
                        )
                    WHERE ifm_mode = 0
                            AND ifm_user_id = @user_id
                            AND ifm_session_user = @user_session_id;
                    commit tran;
                    select @return = 0
                end try
                begin catch
                    if @@TRANCOUNT > 0
                        rollback tran;
                    select @return = -2
                end catch
                select @return as result
        ');

        //set_role_to_senf
        \Illuminate\Support\Facades\DB::unprepared('
            create procedure [dbo].[set_role_to_senf](
               @senf_id bigint,
               @values nvarchar(max) --format: \'<Root><Data CT="1" TP="1"></Data></Root>\'  
            )
            as 
             declare @return int
             declare @tbl_tmp table (TP bigint,RL bigint)
             declare @IXML int
            
             exec sp_xml_preparedocument @IXML output, @values
             insert into @tbl_tmp (TP,RL)
             select TP,RL
             from OPENXML(@IXML, \'/Root/Data\')   
             WITH (TP bigint, RL bigint)
            
            begin try
                begin tran		
                    delete from class_roles where clr_class_id = @senf_id
            
                    insert into class_roles(clr_class_id,clr_type_id,clr_role_id)
                    select @senf_id,TP,RL
                    from @tbl_tmp	
                    group by TP,RL
            
                commit tran
                select @return = 0		
            end try
            begin catch
            if @@TRANCOUNT > 0
                rollback tran
            select @return = -2
            end catch
            select @return as result
        ');

        //show_products_product_list_count
        \Illuminate\Support\Facades\DB::unprepared('
            create procedure [dbo].[show_products_product_list_count](	
                @cat_name nvarchar(500),
                @senf nvarchar(500),
                @sub_cats varchar(max) = \'\', --root data
                @companies varchar(max) = \'\', --root data
                @q nvarchar(500) = \'\',
                @sort varchar(50) = \'prod_id\',
                @direction varchar(50) = \'asc\',
                @per_page int = 20,
                @price_from float = 0,
                @price_to float = 0,
                @page int = 1,
                @mojod tinyint = 0,
                @takhfif tinyint = 0,
                @eshant tinyint = 0
            )
            as 
                DECLARE @offset INT
                DECLARE @newsize INT
                DECLARE @sql NVARCHAR(MAX)
            
                IF(@page=0)
                  BEGIN
                    SET @offset = @page
                   END
                ELSE 
                  BEGIN
                    SET @offset = (@page - 1) * @per_page
                  END
                declare @IXML int
                 if @sub_cats != \'\'
                 begin
                    declare @tbl_tmp_sub_cats table (ID bigint)
                     exec sp_xml_preparedocument @IXML output, @sub_cats
                     insert into @tbl_tmp_sub_cats (ID)
                     select ID
                     from OPENXML(@IXML, \'/Root/Data\')   
                     WITH (ID bigint)
                 end 
            
                if @companies != \'\'
                begin
                    declare @tbl_tmp_companies table (ID bigint)
                    exec sp_xml_preparedocument @IXML output, @companies
                     insert into @tbl_tmp_companies (ID)
                     select ID
                     from OPENXML(@IXML, \'/Root/Data\')   
                     WITH (ID bigint)
                end
                ;with cte as
                (
                    select cat_id, cat_name,cat_parent_id
                    from categories
                    left join collection_categories on cc_category_id = cat_id
                    left join collections on cc_collection_id = coll_id
                    left join collection_classes on coc_coll_id = coll_id
                    left join baseinfos on bas_id = coc_typeClass_id
                    where cat_parent_id = 0 and cat_name = @cat_name and (@companies = \'\' or coll_id in (select ID from @tbl_tmp_companies))
                    and bas_value = @senf
                    union all
                    select cat.cat_id,cat.cat_name,cat.cat_parent_id
                    from cte join categories cat
                        on cat.cat_parent_id = cte.cat_id
                )
                select count(*) as cnt
                from
                (
                    select * from cte
                )t
                left join products on prod_category_id = cat_id
                left join collection_products on cp_product_id = prod_id
                left join collections on cp_collection_id = coll_id
                where prod_id is not null 
                and (@sub_cats = \'\' or cat_id in (select ID from @tbl_tmp_sub_cats))
                and (@q = \'\' or (prod_name like \'%\'+@q+\'%\' or cat_name like \'%\'+@q+\'%\' or coll_name like \'%\'+@q+\'%\'))
                and (@price_from = 0 or cp_default_price_consumer >= @price_from)
                and (@price_to = 0 or cp_default_price_consumer <= @price_to)	
                and (@mojod = 0 or  cp_default_count != 0)
                and (@takhfif = 0 or dbo.check_discount_or_eshant(cp_id,dbo.getTimeStmp(getdate())) > 0)
                and (@eshant = 0 or dbo.check_discount_or_eshant(cp_id,dbo.getTimeStmp(getdate())) = -1)
                group by cp_id
        ');

        //show_products_product_list_rows
        \Illuminate\Support\Facades\DB::unprepared('
            create procedure [dbo].[show_products_product_list_rows](	
                @cat_name nvarchar(500),
                @senf nvarchar(500),
                @sub_cats varchar(max) = \'\', --root data
                @companies varchar(max) = \'\', --root data
                @q nvarchar(500) = \'\',
                @sort varchar(50) = \'prod_id\',
                @direction varchar(50) = \'asc\',
                @per_page int = 20,
                @price_from float = 0,
                @price_to float = 0,
                @page int = 1,
                @mojod tinyint = 0,
                @takhfif tinyint = 0,
                @eshant tinyint = 0
            )
            as 
                DECLARE @offset INT
                DECLARE @newsize INT
                DECLARE @sql NVARCHAR(MAX)
            
                IF(@page=0)
                  BEGIN
                    SET @offset = @page
                   END
                ELSE 
                  BEGIN
                    SET @offset = (@page - 1) * @per_page
                  END
                declare @IXML int
                 if @sub_cats != \'\'
                 begin
                    declare @tbl_tmp_sub_cats table (ID bigint)
                     exec sp_xml_preparedocument @IXML output, @sub_cats
                     insert into @tbl_tmp_sub_cats (ID)
                     select ID
                     from OPENXML(@IXML, \'/Root/Data\')   
                     WITH (ID bigint)
                 end 
            
                if @companies != \'\'
                begin
                    declare @tbl_tmp_companies table (ID bigint)
                    exec sp_xml_preparedocument @IXML output, @companies
                     insert into @tbl_tmp_companies (ID)
                     select ID
                     from OPENXML(@IXML, \'/Root/Data\')   
                     WITH (ID bigint)
                end
                ;with cte as
                (
                    select cat_id, cat_name,cat_parent_id
                    from categories
                    left join collection_categories on cc_category_id = cat_id
                    left join collections on cc_collection_id = coll_id
                    left join collection_classes on coc_coll_id = coll_id
                    left join baseinfos on bas_id = coc_typeClass_id
                    where cat_parent_id = 0 and cat_name = @cat_name and (@companies = \'\' or coll_id in (select ID from @tbl_tmp_companies))
                    and bas_value = @senf
                    union all
                    select cat.cat_id,cat.cat_name,cat.cat_parent_id
                    from cte join categories cat
                        on cat.cat_parent_id = cte.cat_id
                )
                select 
                    cp_id as prod_id,
                     prod_name,prod_description,
                     cp_default_count,
                     cp_default_price_buy as price,
                     dbo.check_discount_or_eshant(cp_id,dbo.getTimeStmp(getdate())) as have_discount,bas_value,
                     (select top 1 ur_path from upload_route where ur_fk_id = cp_id and ur_table_name = \'collection_products\' order by ur_id desc) as file_path
                from
                (
                    select * from cte
                )t
                left join products on prod_category_id = cat_id
                left join collection_products on cp_product_id = prod_id
                left join collections on cp_collection_id = coll_id
                left join baseinfos on bas_id = cp_measurement_unit_id
                where prod_id is not null 
                and (@sub_cats = \'\' or cat_id in (select ID from @tbl_tmp_sub_cats))
                and (@q = \'\' or (prod_name like \'%\'+@q+\'%\' or cat_name like \'%\'+@q+\'%\' or coll_name like \'%\'+@q+\'%\'))
                and (@price_from = 0 or cp_default_price_consumer >= @price_from)
                and (@price_to = 0 or cp_default_price_consumer <= @price_to)
                and (@mojod = 0 or  cp_default_count != 0)
                and (@takhfif = 0 or dbo.check_discount_or_eshant(cp_id,dbo.getTimeStmp(getdate())) > 0)
                and (@eshant = 0 or dbo.check_discount_or_eshant(cp_id,dbo.getTimeStmp(getdate())) = -1)
                group by cp_id,prod_id ,prod_name,prod_description,cp_default_count,cp_default_price_buy,bas_value
                order by 
                    -- prod_id
                    CASE WHEN @sort = \'prod_id\'
                    AND @direction = \'asc\'
                    THEN prod_id END,
                    CASE WHEN @sort = \'prod_id\'
                    AND @direction = \'desc\'
                    THEN prod_id END DESC,
            
                    -- prod_name
                    CASE WHEN @sort = \'prod_name\'
                    AND @direction = \'asc\'
                    THEN prod_name END,
                    CASE WHEN @sort = \'prod_name\'
                    AND @direction = \'desc\'
                    THEN prod_name END DESC
                OFFSET @offset ROWS FETCH NEXT @per_page ROWS ONLY
        ');

        //show_senf_and_catergory_product_list
        \Illuminate\Support\Facades\DB::unprepared('
            create procedure [dbo].[show_senf_and_catergory_product_list](
                @user_id bigint
            )
            as 
                ;with cte as
                (
                    select cat_id, cat_name,cat_parent_id from categories
                    where cat_parent_id = 0
                    union all
                    select cat.cat_id,cat.cat_name,cat.cat_parent_id
                    from cte join categories cat
                        on cat.cat_parent_id = cte.cat_id
                )
                select dbo.get_full_path(cat_id) as full_path,cat_id,bas_value,cat_name,cat_parent_id
                from
                (
                    select * from cte
                )t
                left join collection_categories on cc_category_id = t.cat_id
                left join collections on coll_id = cc_collection_id
                left join collection_classes on coc_coll_id = coll_id
                left join baseinfos on bas_id = coc_typeClass_id
                where bas_type = \'type_class\'
                and (
                    bas_id in (
                        select clr_class_id from class_roles 
                        left join user_roles on ur_rol_id = clr_role_id  
                        where ur_user_id = @user_id
                    )
                )
                group by cat_id,bas_value,cat_name,cat_parent_id
        ');

        //update_count_and_price
        \Illuminate\Support\Facades\DB::unprepared('
            create procedure [dbo].[update_count_and_price](
                @prod_id BIGINT,
                @price_buy float = null,
                @price_consumer float = null,
                @count float
            )
            as begin   	
                if @price_consumer is not null
                    begin
                         update collection_products SET cp_default_count += @count,
                         cp_default_price_buy = @price_buy,
                         cp_default_price_consumer = @price_consumer
                         WHERE cp_id = @prod_id;
                    end
                else
                    begin		
                         update collection_products SET cp_default_count -= @count			
                         WHERE cp_id = @prod_id;
                    end
            END
        ');

        //check_discount_or_eshant
        \Illuminate\Support\Facades\DB::unprepared('
            create FUNCTION [dbo].[check_discount_or_eshant](@prod_id BIGINT, @now_timestamp INT)
            RETURNS float
            as begin
                 declare @discount float
                 declare @eshant float   
                 declare @return float
                        --percent => have_discount, -1 => have_eshant , -2 => nothing
                SELECT top 1
                    @discount = pc_discount_percent,
                    @eshant = pc_free_collection_product_id
                FROM product_complimentary
                WHERE pc_collection_product_id = @prod_id AND
                        (pc_date_to = 0 OR (pc_date_from <= @now_timestamp AND pc_date_to >= @now_timestamp)) order by pc_id desc;
                IF @discount is null OR @discount = 0
                    IF @eshant is null OR @eshant = 0
                        select @return = -2; -- nothing
                    ELSE
                        select @return = -1; --have_eshant			
                ELSE
                    select @return = @discount; --have_discount
                return @return
            END
        ');

        //get_full_path
        \Illuminate\Support\Facades\DB::unprepared('
            create FUNCTION [dbo].[get_full_path](@id BIGINT)
            RETURNS nvarchar(max)
           as BEGIN
            DECLARE @_parentId INT;
            DECLARE @_typeContent nvarchar(1000);
            DECLARE @full_path nvarchar(max);
        
            SET @full_path = \'\';
        
            WHILE @id IS NOT NULL 
            begin
                SELECT
                @_parentId = cat_parent_id,
                @_typeContent = cat_name
                FROM categories
                WHERE cat_id = @id
                IF (@@ROWCOUNT != 1)
                    break       
                            
                SET @full_path = CONCAT(@_typeContent, \'/\', @full_path);
                SET @id = @_parentId;
            END
            IF @full_path != \'\'
                set @full_path = substring(@full_path, 1, len(@full_path) - 1);
            return @full_path
            END
        ');

        //getTimeStmp
        \Illuminate\Support\Facades\DB::unprepared('
            create FUNCTION [dbo].[getTimeStmp] 
            ( 
                @dt DATETIME 
            ) 
            RETURNS BIGINT 
            AS 
            BEGIN 
                DECLARE @diff BIGINT 
                IF @dt >= \'20380119\' 
                BEGIN 
                    SET @diff = CONVERT(BIGINT, DATEDIFF(S, \'19700101\', \'20380119\')) 
                        + CONVERT(BIGINT, DATEDIFF(S, \'20380119\', @dt)) 
                END 
                ELSE 
                    SET @diff = DATEDIFF(S, \'19700101\', @dt) 
                RETURN @diff 
            END
        ');

        //factor_master_sp
        \Illuminate\Support\Facades\DB::unprepared('          
            create PROCEDURE [dbo].[factor_master_sp](	
                @mode tinyint,
                @collection_id BIGINT = null,
                @user_id BIGINT,
				--@user_session_id VARCHAR(255),
                @shamsi_date     char(10) = \'____/__/__\',
                @created_at      int = 0,
                @updated_at      int = null,
                @description nvarchar(max) = null,
				@payment_method bigint = null,
				@self_collection_id bigint = null,
				@return_reasons varchar(max) = null,
				@fm_id bigint = null out
            )
            as
                declare @return int
                if @mode = 0 --open factor
                begin
                    begin try
						select top 1 @fm_id = fm_id from factor_master
						where fm_collection_id = @collection_id and fm_creator_id = @user_id
						and fm_self_collection_id = @self_collection_id
						/*and fm_session_user = @user_session_id*/
						and fm_mode = 0

                        if @fm_id is null
						begin
							INSERT INTO factor_master (fm_collection_id,fm_creator_id,fm_self_collection_id)
								VALUES (@collection_id,@user_id/*,@user_session_id*/,@self_collection_id);
							SELECT @return = scope_identity()   
							set @fm_id = @return
						end
						else 
							set @return = @fm_id
                    end try
                    begin catch			
						print error_message()
                        select @return = -2
                    end catch
                end
                else if @mode = 1 --close factor
                begin					
					declare @percent float					
					begin try
					begin tran
						 exec factor_returns_sp @fm_id = @fm_id,@return_reasons = @return_reasons
						 declare @maliat int
						 select @maliat = convert(float,setting.st_value) from setting where st_key = \'tax\'
						 select @fm_id = fm_id from factor_master where fm_mode = 0
									AND fm_creator_id = @user_id and fm_collection_id = @collection_id
						  

						  --select @percent = cpm_percent from collection_payment_methods where cpm_collection_id = @collection_id and cpm_id = @payment_method
						  UPDATE factor_master
							SET
                            fm_mode = 1,
                            fm_date = @shamsi_date,			
							fm_payment_method =  @payment_method,
							fm_payment_method_percent =  0,
							fm_created_at = @created_at,
							fm_self_collection_id = @self_collection_id,
							fm_time = @created_at,
							fm_tax = @maliat,
							fm_description = @description,
							fm_amount = (select sum( 
													FLOOR(
													case fd_arzesh_afzoodeh
														when 1 then
														((fd_price_buy + dbo.calcPercent(fd_price_buy,@maliat)) - dbo.calcPercent((fd_price_buy + dbo.calcPercent(fd_price_buy,@maliat)),fd_discount)) * fd_count
														else 
														(fd_price_buy - dbo.calcPercent(fd_price_buy,fd_discount)) * fd_count
														end
													)
												) from factor_detail where fd_master_id = fm_id and fd_ooe = 0
										),
                            fm_no   = (
                                SELECT x.max_no + 1
                                FROM (
                                        SELECT isnull(max(CAST(fmaster.fm_no as bigint)) , 0) AS max_no
                                        FROM factor_master AS fmaster								
                                            LEFT JOIN collections ON fm_collection_id = coll_id											
                                     ) x
                            )
							WHERE fm_id = @fm_id
									AND fm_creator_id = @user_id and fm_mode = 0
									/*AND fm_session_user = @user_session_id;*/

							 declare @role_id bigint						
							 select @role_id = dbo.get_setting(\'factor_receiver\',@collection_id,@user_id)
							 insert into factor_routing (far_role_id,far_factor_id,far_created_at)
												 values(@role_id,@fm_id,@created_at)

							 --mohasebeye talab az ghabl
							  declare @fm_amount float 
							  declare @cd_amount float 
							  select @fm_amount = fm_amount,@collection_id = fm_collection_id from factor_master where fm_id = @fm_id
							  select @cd_amount = cd_amount from collection_debt where cd_source_collection_id = @collection_id and cd_destination_collection_id = @self_collection_id
							  if @cd_amount is not null
							  begin
								  if @cd_amount > @fm_amount 
									 set @cd_amount = @fm_amount
								  update factor_master set fm_talab_az_ghabl = @cd_amount where fm_id = @fm_id	
								  update collection_debt set cd_amount -= @cd_amount where cd_source_collection_id = @collection_id and cd_destination_collection_id = @self_collection_id
							  end
							 commit tran
							 select @return = 0
					
					end try
					begin catch
						if @@TRANCOUNT > 0
							rollback tran
							--print error_message()
						select @return = -2
					end catch

					select @return as result
                end                                                          
        ');

        //factor_detail_sp
        \Illuminate\Support\Facades\DB::unprepared('
            create PROCEDURE [dbo].[factor_detail_sp](	               
                @fm_id bigint,
                @prod_id bigint,
                @count int = 1,
                @add_type int = 1, --or 1 or -1
				@timestamp int
            )
            as
                declare @return int
				declare @fd_id bigint
                declare @arzesh_afzodeh bit
				declare @price_buy float
				declare @price_consumer float
				declare @have_eshant float
				declare @temp float
				
				update factor_master set fm_calculated = 0 where fm_id = @fm_id

				select @arzesh_afzodeh = cp_arzesh_afzoodeh,
					@price_buy = cp_default_price_buy,
					@price_consumer = cp_default_price_consumer
                    from collection_products where cp_id = @prod_id
				if @count > 0
					select @count = @add_type * @count
				select top 1 @fd_id = fd_id from factor_detail where fd_master_id = @fm_id and fd_product_id = @prod_id and fd_ooe = 0
				
				declare @maliat int
				select @maliat = convert(float,setting.st_value) from setting where st_key = \'tax\'

                if @fd_id is null --insert
                begin          
					set @temp = @price_buy
					/*if @arzesh_afzodeh = 1 
					begin
						set @temp += (@price_buy * @maliat)/100
					end*/			
					if @count>=0
					INSERT INTO factor_detail(fd_master_id,fd_product_id,fd_count,fd_arzesh_afzoodeh,fd_price_buy,fd_price_consumer)
						VALUES (@fm_id,@prod_id,1,@arzesh_afzodeh,@temp,@price_consumer);		
						select @return = 0     
                end
                else --update
                begin
					if @count = 0 or @count = -1
						UPDATE factor_detail
							SET fd_count += 1	
							WHERE fd_product_id = @prod_id AND fd_master_id = @fm_id and fd_ooe = 0
					else
						UPDATE factor_detail
							SET fd_count = @count	
							WHERE fd_product_id = @prod_id AND fd_master_id = @fm_id and fd_ooe = 0
                end

				--check eshant
				
				select @count = fd_count from factor_detail WHERE fd_product_id = @prod_id AND fd_master_id = @fm_id and fd_ooe = 0
			
				if @count <= 0 
				begin
					declare @count_item_in_factor_detail bigint
					delete from factor_detail WHERE fd_product_id = @prod_id AND fd_master_id = @fm_id
					select @count_item_in_factor_detail = count(1) from factor_detail where fd_master_id = @fm_id
					if @count_item_in_factor_detail = 0
						delete from factor_master WHERE fm_id = @fm_id
				end
				else
				begin
					select @have_eshant = dbo.check_discount_or_eshant(@prod_id,@timestamp)
					if @have_eshant = -1 --ehsnt darad
					begin 
						declare @pc_free_count int
						declare @pc_free_collection_product_id bigint
						declare @pc_overflow bit
						declare @pc_buy_count int
						declare @mazad int = 1
						declare @pc_price_buy float

						select top 1 @pc_free_count= pc_free_count,@pc_buy_count = pc_buy_count,@pc_free_collection_product_id = pc_free_collection_product_id,@pc_overflow=pc_overflow from product_complimentary 
						where pc_collection_product_id = @prod_id and pc_buy_count <= @count
						order by pc_buy_count desc
						select top 1 @pc_price_buy = cp_default_price_buy,@arzesh_afzodeh = cp_arzesh_afzoodeh from collection_products where cp_id = @pc_free_collection_product_id
						
						set @temp = @pc_price_buy
						if @arzesh_afzodeh is not null
							set @temp += (@price_buy * @maliat)/100

						delete from factor_detail where fd_ooe_id = @prod_id
						if @pc_free_count is not null
						begin
							if @pc_overflow = 1
								set @mazad = @count/@pc_buy_count
							set @pc_free_count = @pc_free_count * @mazad							
							INSERT INTO factor_detail(fd_master_id,fd_product_id,fd_ooe,fd_ooe_id,fd_count,fd_arzesh_afzoodeh,fd_price_buy)
								select @fm_id,@pc_free_collection_product_id,1,@prod_id,@pc_free_count,0,@temp	
						end
					end
					else if @have_eshant > 0 --discount darad
					begin
						UPDATE factor_detail
							SET fd_discount = @have_eshant
							WHERE fd_product_id = @prod_id AND fd_master_id = @fm_id and fd_ooe = 0
					end
				end
        ');

        //add_to_basket
        \Illuminate\Support\Facades\DB::unprepared('
            create PROCEDURE [dbo].[add_to_basket](	
                @add_type int = 1, --minus = -1 and plus = 1
                @user_id bigint,   
                @prod_id bigint,
                @count bigint = 0,
                @timestamp int,
				@self_collection_id bigint
            )
            as
                declare @return int
                declare @fm_id bigint
                declare @collection_id bigint
                begin try		
                    select top 1 @collection_id = cp_collection_id from collection_products 
                    where cp_id = @prod_id
            
                    if @collection_id is null
                        set @return = -1 -- mahsole eshtebah ast
                    else
                    begin
                        begin tran		
							exec factor_master_sp @mode = 0,@collection_id = @collection_id,@user_id = @user_id,@self_collection_id = @self_collection_id,@fm_id = @fm_id out
							exec factor_detail_sp @fm_id = @fm_id,@prod_id = @prod_id,@count = @count,@add_type = @add_type,@timestamp = @timestamp
                        commit tran
						set @return = 0
                     end
                end try
                begin catch
                    if @@TRANCOUNT > 0
                        rollback tran
                    set @return  = -2
                end catch
                select @return as result                          
        ');

        //get_basket_items
        \Illuminate\Support\Facades\DB::unprepared('
            create PROCEDURE [dbo].[get_basket_items](	   
                @user_id bigint,    
				@id bigint = null,
				@mode bit= 0
            )
            as   	
				if @id is null
				begin
					select coll_name,(select count(1) from factor_detail where fd_master_id = fm_id and fd_ooe = 0) as cnt,
					(((100 - fm_festival_discount) * (select sum(fd_count * (((100 - fd_discount) * fd_price_buy)/100)) from factor_detail where fd_master_id = fm_id and fd_ooe = 0))/100) as price,fm_id,
					(select top 1 ur_path from upload_route where ur_table_name = \'collections\' and ur_collection_id = coll_id order by ur_id desc) as logo
					from factor_master
					left join collections on coll_id = fm_collection_id
					where fm_creator_id = @user_id
					and fm_mode = 0
				end 
				else
					select cp_id,prod_name,fd_id,fd_count,bas_value,fd_ooe,fd_ooe_id,
					(((100 - fm_festival_discount) * (select sum(fd_count * (((100 - fd_discount) * fd_price_buy)/100)) from factor_detail where fd_master_id = fm_id and fd_ooe = 0))/100) as fm_amount
					,fd_price_buy,fd_price_consumer,fd_arzesh_afzoodeh,coll_id,fm_self_collection_id,
					(select top 1 ur_path from upload_route where ur_collection_id = coll_id and ur_table_name = \'collections\' order by ur_id desc) as logo
					,coll_name,coll_address,fd_discount,fm_calculated,fm_festival_discount from factor_detail
					left join collection_products on cp_id = fd_product_id
					left join baseinfos on bas_id = cp_measurement_unit_id
					left join factor_master on fd_master_id = fm_id 
					left join collections on coll_id = fm_collection_id
					left join products on cp_product_id = prod_id
					where fd_master_id = @id			
					and fm_mode = @mode		
					order by cp_id,fd_id,fd_ooe_id
        ');

        //delete_factor_from_basket
        \Illuminate\Support\Facades\DB::unprepared('
            create PROCEDURE [dbo].[delete_factor_from_basket](	   
                @user_id bigint,    
				@id bigint
            )
            as   	
			declare @return int
			begin try
				delete from factor_detail where fd_master_id = @id
				delete from factor_master where fm_id = @id  and fm_creator_id = @user_id
				and fm_mode = 0
				select @return = 0
			end try
			begin catch
				select @return = -2
			end catch
			select @return as result
        ');

        //delete_from_basket
        \Illuminate\Support\Facades\DB::unprepared('
        create PROCEDURE [dbo].[delete_from_basket](	   
            @user_id bigint,               
            @prod_id bigint	
        )
        as
            declare @return tinyint
            declare @fm_id bigint
            declare @collection_id bigint
            begin try		
                select top 1 @collection_id = cp_collection_id from collection_products 
                where cp_id = @prod_id
        
                if @collection_id = 0
                    set @return = -1 -- mahsole eshtebah ast
                else
                begin	
                    select @fm_id = fm_id from factor_master where fm_creator_id = @user_id and fm_collection_id = @collection_id
                    delete from factor_detail where fd_master_id = @fm_id and fd_product_id = @prod_id
                 end
            end try
            begin catch		
                set @return  = -2
            end catch
            select @return as result                  
        ');

        //factor_calculates
        \Illuminate\Support\Facades\DB::unprepared('
            create procedure [dbo].[factor_calculates](
                @fm_id bigint,
                @timestamp int
            )
            as
                declare @total float
                declare @collection_id bigint
                declare @free_count float
                declare @percent float
                declare @product_id bigint
                declare @overflow bit
                declare @temp float
                declare @cfc_buy_to float
                declare @arzesh_afzodeh bit
                declare @price_buy float
        
                delete from factor_detail where fd_master_id = @fm_id and fd_ooe_id  = -1
        
                select 
                @collection_id = (select top 1 fm_collection_id from factor_master where fm_id = fd_master_id),
                @total = sum(fd_count * (((100 - fd_discount) * fd_price_buy)/100)) 
                from factor_detail 
                where fd_master_id = @fm_id and fd_ooe = 0
                group by fd_master_id
        
                --calc discount
                select top 1 @percent = cfc_discount_percent,@overflow = cfc_overflow
                from collection_factor_complimentary 
                where cfc_collection_id = @collection_id 
                and ((cfc_buy_to = 0) or (@total between cfc_buy_from and cfc_buy_to) or (cfc_buy_to < @total))
                and ((cfc_date_to = 0) or (@timestamp between cfc_date_from and cfc_date_to))
                and cfc_type = 0
                order by cfc_id desc
                set @percent = isnull(@percent , 0)
            
                if @percent is not null
                begin
                    UPDATE factor_master set fm_festival_discount = @percent where fm_id = @fm_id
                end
                --end calc discount
        
                --calc eshant
                select top 1 @cfc_buy_to = cfc_buy_to, @free_count = cfc_free_count,@product_id = cfc_free_collection_product_id,@overflow = cfc_overflow
                from collection_factor_complimentary 
                where cfc_collection_id = @collection_id 
                and ((cfc_buy_to = 0) or (@total between cfc_buy_from and cfc_buy_to) or (cfc_buy_to < @total))
                and ((cfc_date_to = 0) or (@timestamp between cfc_date_from and cfc_date_to))
                and cfc_type = 1
                order by cfc_id desc
                
                if @free_count is not null
                begin		
                    set @temp = 1		
                    if @overflow = 1 and @total > @cfc_buy_to
                        set @temp = floor(@total/@cfc_buy_to)
                    set @free_count = @free_count * @temp
                    
                    if @product_id is not null
                    begin
                        select @arzesh_afzodeh = cp_arzesh_afzoodeh,
                        @price_buy = cp_default_price_buy
                        from collection_products where cp_id = @product_id
                        set @temp = @price_buy
                        if @arzesh_afzodeh = 1 
                        begin				
                            declare @maliat float
                            select @maliat = convert(float,setting.st_value) from setting where st_key = \'tax\'
                            set @temp += (@price_buy * @maliat)/100
                        end
                        INSERT INTO factor_detail(fd_master_id,fd_product_id,fd_ooe,fd_ooe_id,fd_count,fd_arzesh_afzoodeh,fd_price_buy)
                        select @fm_id,@product_id,1,-1,@free_count,0,@temp	
                    end
                end
                --end calc eshant
        
                UPDATE factor_master SET fm_calculated = 1 where fm_id = @fm_id
                select 0 as result
        ');

        //get_setting
        \Illuminate\Support\Facades\DB::unprepared('
            create function get_setting(@item nvarchar(1000),@collection_id bigint = 0,@user_id bigint=0)
            returns nvarchar(max)
            as 
            begin
                select @collection_id = isnull(@collection_id , 0),@user_id = isnull(@user_id , 0)
                declare @st_value nvarchar(max)
                select top 1 @st_value = [st_value] 
                    from [setting] 
                    where ([st_key] = @item) 
                    and (
                        ([st_collection_id] = @collection_id and [st_user_id] = @user_id) or 
                        ([st_collection_id] = 0 and [st_user_id] = @user_id) or
                        ([st_collection_id] = @collection_id and [st_user_id] = 0) or
                        ([st_collection_id] = 0 and [st_user_id] = 0)
                    )
                    order by st_user_id desc,st_collection_id desc
                 return @st_value;
            end 
        ');

        //factor_returns_sp
        \Illuminate\Support\Facades\DB::unprepared('
            create procedure [dbo].[factor_returns_sp]
            (
                @fm_id bigint,
                @return_reasons varchar(max) = null,
                @type bit = 0
            )
            as
                declare @tbl_tmp table (PR bigint, CN bigint,RS bigint)
                declare @IXML int
                declare @retCount int
            
                exec sp_xml_preparedocument @IXML output, @return_reasons
            
                insert into @tbl_tmp (PR, CN,RS)
                select PR, CN,RS
                from OPENXML(@IXML, \'/Root/Data\')   
                WITH (PR bigint, CN bigint,RS bigint)
                group by PR,CN,RS
            
                select @retCount = count(1) from @tbl_tmp
                if @retCount > 0
                begin
                    insert into factor_returns (fr_master_id, fr_fd_id, fr_count,fr_reason_id,fr_type)
                    select @fm_id ,PR, CN,RS,@type
                    from @tbl_tmp	
                end
        ');

        //factor_change_status
        \Illuminate\Support\Facades\DB::unprepared('
            create PROCEDURE [dbo].[factor_change_status](	        
                @created_at      int = 0,                
                @return_reasons varchar(max) = null,
                @fm_id bigint = null out,
                @status tinyint,
                @role_id bigint = null,
                @user_id bigint,
                @shamsi_date char(10)
            )
            as
                declare @return int
                declare @return_sum float
                declare @source_collection_id bigint
                declare @destination_collection_id bigint
                declare @payment_method bigint
                declare @talabAzGhabl float
                declare @amount float
            
                set @return_sum = 0
            
                select 
                    @payment_method = fm_payment_method,
                    @source_collection_id = fm_collection_id,
                    @destination_collection_id = fm_self_collection_id,
                    @talabAzGhabl = fm_talab_az_ghabl,
                    @amount  = fm_amount
                from factor_master
            
                select @payment_method = cpm_bas_id from collection_payment_methods where cpm_id = @payment_method
            
                begin try
                begin tran	
                        exec factor_returns_sp @fm_id = @fm_id,@return_reasons = @return_reasons,@type = 1
                        
                        if @status = 2 --last accept
                        begin
                            --1) add returns sum to fm_returns_sum and if payment method
                            -- is online or kife pool add returns sum to collection_debt table
                            select @return_sum = sum(fd_price_buy * fr_count) from factor_returns
                            left join factor_detail on fr_fd_id = fd_id 
                            where fr_master_id =  @fm_id
                            
                            if @payment_method in(50,53)
                            begin
                                update collection_debt set cd_amount += @return_sum  
                                where cd_source_collection_id = @source_collection_id and cd_destination_collection_id = @destination_collection_id
                                if @@ROWCOUNT = 0
                                    insert into collection_debt (cd_source_collection_id,cd_destination_collection_id,cd_amount)
                                    values(@source_collection_id,@destination_collection_id,@return_sum)
                            end
                            
                            --2) return to inventury if return reason is returnable
                            
                            declare @tbl_tmp table (PRD bigint,PRB float,PRC float,CNT float)               
                            insert into @tbl_tmp (PRD,PRB,PRC,CNT)
                            select fd_product_id,fd_price_buy,fd_price_consumer,fr_count from factor_returns
                            left join factor_detail on fd_id = fr_fd_id
                            left join collection_return_reasons on fr_reason_id = crr_id
                            where crr_return_to_inventory = 1
                        
                            declare @inv_id bigint
                            select @inv_id = inv_id from inventories where inv_collection_id = @source_collection_id
            
                            declare @fc_id bigint
                            select @fc_id = fc_id from financial_cycles where fc_collection_id = @source_collection_id
            
                            declare @ifm_id bigint
                            INSERT INTO inventory_factor_master (ifm_inventory_id, ifm_user_id, ifm_financial_cycle_id, ifm_type)
                                VALUES (@inv_id, @user_id, @fc_id, 0);
                                SELECT @ifm_id = scope_identity()				
                        
                            INSERT INTO inventory_factor_detail (ifd_collection_product_id, ifd_factor_master_id, ifd_price_buy, ifd_price_consumer, ifd_count)
                            select PRD,@ifm_id ,PRB,PRC,CNT
                            from @tbl_tmp	
                            group by PRD,PRB,PRC,CNT
            
                            --add mojodi and price to table collection_products
                            DECLARE @PRD bigint 
                            DECLARE @PRB float
                            DECLARE @PRC float 
                            DECLARE @CNT float
            
                            DECLARE update_price_and_count_cursor CURSOR FAST_FORWARD
                            FOR select PRD,PRB,PRC,CNT
                                from @tbl_tmp
                            OPEN update_price_and_count_cursor
                            FETCH NEXT FROM update_price_and_count_cursor INTO @PRD,@PRB,@PRC,@CNT
                            WHILE @@FETCH_STATUS = 0
                            BEGIN
                                exec update_count_and_price @PRD,@PRB,@PRC,@CNT
                                FETCH NEXT FROM update_price_and_count_cursor INTO @PRD,@PRB,@PRC,@CNT
                            END
                            CLOSE update_price_and_count_cursor
                            DEALLOCATE update_price_and_count_cursor
            
                            UPDATE inventory_factor_master
                            SET
                                ifm_mode = 1,
                                ifm_date = @shamsi_date,
                                ifm_type = 0,
                                ifm_no   = (
                                    SELECT x.max_no
                                    FROM (
                                            SELECT max(ifmaster.ifm_no) + 1 AS max_no
                                            FROM inventory_factor_master AS ifmaster
                                                LEFT JOIN inventories ON inv_id = ifmaster.ifm_inventory_id
                                                LEFT JOIN collections ON inv_collection_id = coll_id
                                            WHERE coll_id = @source_collection_id
                                            ) x
                                )
                            WHERE ifm_id = @ifm_id and ifm_mode = 0
                                    
            
                        end
                        else if @status = 3
                        begin
                            --if online or kife pool hast kole pool ro bargardoon
                            if @payment_method in(50,53)
                            begin
                                set @talabAzGhabl  = @amount
                            end
                            -- return talab az ghabl to collection_debt table
                            update collection_debt set cd_amount += @talabAzGhabl 
                            where cd_source_collection_id = @source_collection_id and cd_destination_collection_id = @destination_collection_id
                        end
            
                        UPDATE factor_master
                        SET          
                        fm_status = @status,		
                        fm_returns_sum = @return_sum
                        WHERE fm_id = @fm_id		
            
                        --age bayad send beshe
                        if @role_id is not null
                            insert into factor_routing (far_role_id,far_factor_id,far_created_at)
                                            values(@role_id,@fm_id,@created_at)
                        commit tran
                        select @return = 0
                --end tran
                end try
                begin catch
                    if @@TRANCOUNT > 0
                        rollback tran
                    select @return = -2
                end catch
            
                select @return as result                                      
        ');

        //factor_payment
        \Illuminate\Support\Facades\DB::unprepared('
            alter procedure factor_payment
            (
                @fm_id bigint,
                @payment_method bigint,
                @mode int,
                @user_id bigint = null
            )
            as
                declare @percent float
                declare @return float
                declare @bas_id bigint
            
                select @percent = cpm_percent,@bas_id = cpm_bas_id from collection_payment_methods where cpm_id = @payment_method
                if @percent is not null
                    update factor_master set fm_payment_method = @payment_method, fm_payment_method_percent = @percent
                    where fm_id = @fm_id
                if @mode = 1 --get amount of factor
                begin
                    select dbo.get_factor_amount(@fm_id,2) as amount,@bas_id as method
                end
                else if @mode = 2 -- payment factor
                begin
                    declare @has_error bit = 0
                    declare @amount float
                    if @bas_id = 53 --kife pool
                    begin
                        select @user_id = [user_id],@amount = amount from [users] where [user_id] = @user_id 			
                        if @amount < dbo.get_factor_amount(@fm_id,2)
                        begin
                            select -5 as result
                            return
                        end
                    end
            
                    begin try
                        begin tran				
                            update factor_master set fm_is_payed = 1 where fm_id = @fm_id
                            if @bas_id = 53 --kife pool
                            begin
                                update [users] set amount -= dbo.get_factor_amount(@fm_id,2) where [user_id] = @user_id
                            end
                            commit tran
                            select 0 as result				
                    end try
                    begin catch
                        if @@TRANCOUNT > 0
                            rollback tran
                        select -2 as result
                    end catch		
                end
        ');

        //get_factor_amount
        \Illuminate\Support\Facades\DB::unprepared('
            alter function get_factor_amount
            (
                @fm_id bigint,
                @mode bit -- mode = 1 => without payment_method_percent, mode = 2 => with payment_method_percent
            )
            returns float
            begin
                declare @festival_discount float
                declare @payment_discount float
                declare @factor_amount float
                declare @credit float
                declare @temp float
            
                select 
                    @festival_discount = fm_festival_discount,
                    @payment_discount=fm_payment_method_percent,
                    @factor_amount = fm_amount,
                    @credit = fm_talab_az_ghabl
                from factor_master where fm_id = @fm_id
                
                set @temp = ((100-@festival_discount)* @factor_amount)/100;
                set @temp -= @credit	
                if @mode = 2
                    set @temp = ((100-@payment_discount) * @temp)/100
                return  round(@temp,0)
            end
        ');

        //calcPercent
        \Illuminate\Support\Facades\DB::unprepared('
            create function calcPercent(
                @number float,
                @percent float
            )
            returns float
            as
            begin
                return ((@percent) * @number) / 100;
            end
        ');

//        \Illuminate\Support\Facades\DB::unprepared('');
//        \Illuminate\Support\Facades\DB::unprepared('');
//        \Illuminate\Support\Facades\DB::unprepared('');
//        \Illuminate\Support\Facades\DB::unprepared('');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $sql  = "DROP PROCEDURE IF EXISTS add_announcement;
                 DROP PROCEDURE IF EXISTS add_product;
                 DROP PROCEDURE IF EXISTS add_role;
                 DROP PROCEDURE IF EXISTS check_relation;
                 DROP PROCEDURE IF EXISTS edit_product;
                 DROP PROCEDURE IF EXISTS get_start_and_end_date;
                 DROP PROCEDURE IF EXISTS inventory_factor;
                 DROP PROCEDURE IF EXISTS set_role_to_senf;
                 DROP PROCEDURE IF EXISTS show_products_product_list_count;
                 DROP PROCEDURE IF EXISTS show_products_product_list_rows;
                 DROP PROCEDURE IF EXISTS show_senf_and_catergory_product_list;
                 DROP PROCEDURE IF EXISTS update_count_and_price;
                 DROP PROCEDURE IF EXISTS factor_master_sp;
                 DROP PROCEDURE IF EXISTS add_to_basket;
                 DROP PROCEDURE IF EXISTS get_basket_items;
                 DROP PROCEDURE IF EXISTS delete_factor_from_basket;
                 DROP PROCEDURE IF EXISTS factor_calculates;
                 DROP PROCEDURE IF EXISTS delete_from_basket;
                 DROP PROCEDURE IF EXISTS factor_returns_sp;
                 DROP PROCEDURE IF EXISTS factor_change_status;
                 DROP PROCEDURE IF EXISTS factor_payment;
                 DROP PROCEDURE IF EXISTS factor_detail_sp;";
        \Illuminate\Support\Facades\DB::connection()->getPdo()->exec($sql);

        $sql  = "DROP FUNCTION IF EXISTS check_discount_or_eshant;
                 DROP FUNCTION IF EXISTS get_full_path;
                 DROP FUNCTION IF EXISTS get_setting;
                 DROP FUNCTION IF EXISTS get_factor_amount;
                 DROP FUNCTION IF EXISTS calcPercent;
                 DROP FUNCTION IF EXISTS getTimeStmp;";
        \Illuminate\Support\Facades\DB::connection()->getPdo()->exec($sql);
    }
}
