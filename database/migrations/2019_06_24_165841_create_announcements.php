<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnnouncements extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('announcements', function (Blueprint $table) {
            $table->bigIncrements('ann_id');
            $table->string('ann_title', 500);
            $table->text('ann_body');
            $table->integer('ann_view_count')->default(0);
            $table->unsignedBigInteger('ann_user_id')->default(1)->comment("from users user_id");
            $table->unsignedBigInteger('ann_collection_id')->default(1)->comment("from collections coll_id");
            $table->foreign('ann_user_id')->references('user_id')->on('users');
            $table->foreign('ann_collection_id')->references('coll_id')->on('collections');
            $table->timestamps();
            $table->engine = "InnoDB";
        });

        Schema::create('announcement_receivers', function (Blueprint $table) {
            $table->bigIncrements('anr_id');
            $table->unsignedBigInteger('anr_announcement_id')->default(1)->comment("from cities c_id");
            $table->unsignedBigInteger('anr_city_id')->default(1)->comment("from cities c_id");
            $table->unsignedBigInteger('anr_type_id')->default(1)->comment("from baseinfos bas_id");
            $table->foreign('anr_announcement_id')->references('ann_id')->on('announcements');
            $table->foreign('anr_city_id')->references('c_id')->on('cities');
            $table->foreign('anr_type_id')->references('bas_id')->on('baseinfos');
            $table->engine = "InnoDB";
        });

        Schema::create('announcement_limitation', function (Blueprint $table) {
            $table->bigIncrements('anl_id');
            $table->unsignedBigInteger('anl_collection_id')->comment("from collections coll_id");
            $table->integer('anl_time');
            $table->engine = "InnoDB";
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('announcement_receivers');
        Schema::dropIfExists('announcement_limitation');
        Schema::dropIfExists('announcements');
    }
}
