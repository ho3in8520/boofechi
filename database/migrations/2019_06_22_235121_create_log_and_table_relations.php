<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogAndTableRelations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_relations', function (Blueprint $table) {
            $table->bigIncrements('tr_id');
            $table->string('tr_source_table');
            $table->string('tr_source_field');
            $table->string('tr_destination_table');
            $table->string('tr_destination_field');
            $table->boolean('tr_status')->default(1)->comment("0 => active ,1 => deActivve");
            $table->engine = "InnoDB";
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_relations');
    }
}
